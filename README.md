
# Transmorpher

Transmorpher is an environment for processing generic transformations on XML documents.

All the documentation can be found at [https://moex.gitlabpages.inria.fr/transmorpher/](https://moex.gitlabpages.inria.fr/transmorpher/)

# FlowComposer

FlowComposer was an interface for creating Transmorpher transformation flows.

# Credits

Transmorpher was a joint development of the [https://exmo.inria.fr](Exmo INRIA team) and the Fluxmedia startup.

----
<small>
[https://gitlab.inria.fr/moex/transmorpher](https://gitlab.inria.fr/moex/transmorpher)
</small>
