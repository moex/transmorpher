#!/bin/sh

TMDIR=

CPATH=${TMDIR}/lib/xerces.jar:${CLASSPATH}
CPATH=${TMDIR}/lib/xalan.jar:${CPATH}
CPATH=${TMDIR}/lib/sax2.jar:${CPATH}
#CPATH=${TMDIR}/lib/bsf.jar:${CPATH}
CPATH=${TMDIR}/lib/gnu-regexp.jar:${CPATH}

pwd

java -mx64m -classpath .:../classes:../resources:${CPATH} -Djava.compiler=NONE fr.fluxmedia.transmorpher.application.Transmorph $*

