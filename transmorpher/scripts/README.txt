J. Euzenat
27/02/2006

--------------------------------------------------------
This directory is meant to build Transmorpher.

In fact, everything is now in build.xml for ant.

Transmorpher is now simply run through:

$ java -jar lib/transmo.jar process.xml

--------------------------------------------------------
For compiling:

$ ant jar
	=> compile -> compiles modified files
	-> generate a new jar

$ ant compileall -> compile all the files

-------------------------------------------------------
For marking and releasing
(only works on a particular machine, hence certainly not
 under gforge anymore)

$ ant mark -Dversion=1-0-5
	=> prepare
		-> modify files for new version number
		=> jar
		=> javadoc
		-> replace everything
	-> rtags cvs repository

$ ant release -Dversion=1-0-5
	=> package
		=> image
			=> mark
			-> create transmo-version directory
			-> copy whole tree in it
		-> create file list
		-> create zip
		-> create targz
	=> extrajar -> prepare the jar of extraneous files
	-> modify web pages
	-> send mail about new release (all logs)

--------------------------------------------------------
For releasing as INRIA Software CD

$ ant cdrom -Dversion=1-0-5
    -> create the CDROM hierarchy
    -> copy and make HTML files
    -> prepare transmorpher and extra zip files
    -> build the cdrom.tar.gz file

$Id: README.txt,v 1.4 2006-02-27 20:03:58 euzenat Exp $
