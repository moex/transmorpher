/**
 * $Id: AllTests.java,v 1.2 2002-10-28 09:13:56 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** Unit test for the whole Transmorpher hierarchy
*/

package fr.fluxmedia ;

// import java classes
import junit.framework.*;

import fr.fluxmedia.transmorpher.Graph.GraphSuite;
import fr.fluxmedia.transmorpher.Utils.UtilsSuite;
import fr.fluxmedia.transmorpher.stdlib.StdlibSuite;
import fr.fluxmedia.transmorpher.TProcessFactory.TProcessFactorySuite;

//import fr.fluxmedia.transmorpher.File.FileSuite;

public class AllTests {

  public static TestSuite suite() {
    
    TestSuite suite;
    
    suite = new TestSuite("fr.fluxmedia.AllTests");
    
    suite.addTest(fr.fluxmedia.transmorpher.Graph.GraphSuite.suite());
    suite.addTest(fr.fluxmedia.transmorpher.Utils.UtilsSuite.suite());
    //    suite.addTest(fr.fluxmedia.transmorpher.File.FileSuite.suite());
    suite.addTest(fr.fluxmedia.transmorpher.stdlib.StdlibSuite.suite());
		suite.addTest(fr.fluxmedia.transmorpher.TProcessFactory.TProcessFactorySuite.suite());

    return suite;
  }
}

