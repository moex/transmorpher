package fr.fluxmedia.transmorpher.stdlib;

import junit.framework.TestCase;
// JUnitDoclet begin import
import fr.fluxmedia.transmorpher.stdlib.directoryIterator;
// JUnitDoclet end import

/**
* Generated by JUnitDoclet, a tool provided by
* ObjectFab GmbH under LGPL.
* Please see www.junitdoclet.org, www.gnu.org
* and www.objectfab.de for informations about
* the tool, the licence and the the authors.
*/


public class directoryIteratorTest
// JUnitDoclet begin extends_implements
extends TestCase
// JUnitDoclet end extends_implements
{
  // JUnitDoclet begin class
  fr.fluxmedia.transmorpher.stdlib.directoryIterator directoryiterator = null;
  // JUnitDoclet end class
  
  public directoryIteratorTest(String name) {
    // JUnitDoclet begin method directoryIteratorTest()
    super(name);
    // JUnitDoclet end method directoryIteratorTest()
  }
  
  public void setUp() throws Exception {
    // JUnitDoclet begin method setUp()
    super.setUp();
    directoryiterator = new fr.fluxmedia.transmorpher.stdlib.directoryIterator();
    // JUnitDoclet end method setUp()
  }
  
  public void tearDown() throws Exception {
    // JUnitDoclet begin method tearDown()
    directoryiterator = null;
    super.tearDown();
    // JUnitDoclet end method tearDown()
  }
  
  public void testGetName() throws Exception {
    // JUnitDoclet begin method testGetName()
    // JUnitDoclet end method testGetName()
  }
  
  public void testStaticInit() throws Exception {
    // JUnitDoclet begin method testStaticInit()
    // JUnitDoclet end method testStaticInit()
  }
  
  public void testInit() throws Exception {
    // JUnitDoclet begin method testInit()
    // JUnitDoclet end method testInit()
  }
  
  public void testHasMoreElements() throws Exception {
    // JUnitDoclet begin method testHasMoreElements()
    // JUnitDoclet end method testHasMoreElements()
  }
  
  public void testNextElement() throws Exception {
    // JUnitDoclet begin method testNextElement()
    // JUnitDoclet end method testNextElement()
  }
  
  
  
  /**
  * JUnitDoclet moves marker to this method, if there is not match
  * for them in the regenerated code and if the marker is not empty.
  * This way, no test gets lost when regenerating after renaming.
  * Method testVault is supposed to be empty.
  */
  public void testVault() throws Exception {
    // JUnitDoclet begin method testVault()
    // JUnitDoclet end method testVault()
  }
  
}
