package fr.fluxmedia.transmorpher.stdlib;

import junit.framework.TestCase;
// JUnitDoclet begin import
import fr.fluxmedia.transmorpher.stdlib.wrap;
// JUnitDoclet end import

/**
* Generated by JUnitDoclet, a tool provided by
* ObjectFab GmbH under LGPL.
* Please see www.junitdoclet.org, www.gnu.org
* and www.objectfab.de for informations about
* the tool, the licence and the the authors.
*/


public class wrapTest
// JUnitDoclet begin extends_implements
extends TestCase
// JUnitDoclet end extends_implements
{
  // JUnitDoclet begin class
  fr.fluxmedia.transmorpher.stdlib.wrap wrap = null;
  // JUnitDoclet end class
  
  public wrapTest(String name) {
    // JUnitDoclet begin method wrapTest()
    super(name);
    // JUnitDoclet end method wrapTest()
  }
  
  public void setUp() throws Exception {
    // JUnitDoclet begin method setUp()
    super.setUp();
    wrap = new fr.fluxmedia.transmorpher.stdlib.wrap();
    // JUnitDoclet end method setUp()
  }
  
  public void tearDown() throws Exception {
    // JUnitDoclet begin method tearDown()
    wrap = null;
    super.tearDown();
    // JUnitDoclet end method tearDown()
  }
  
  public void testIsLastHandler() throws Exception {
    // JUnitDoclet begin method testIsLastHandler()
    // JUnitDoclet end method testIsLastHandler()
  }
  
  public void testIsFirstHandler() throws Exception {
    // JUnitDoclet begin method testIsFirstHandler()
    // JUnitDoclet end method testIsFirstHandler()
  }
  
  
  
  /**
  * JUnitDoclet moves marker to this method, if there is not match
  * for them in the regenerated code and if the marker is not empty.
  * This way, no test gets lost when regenerating after renaming.
  * Method testVault is supposed to be empty.
  */
  public void testVault() throws Exception {
    // JUnitDoclet begin method testVault()
    // JUnitDoclet end method testVault()
  }
  
}
