package fr.fluxmedia.transmorpher.stdlib;

import junit.framework.TestSuite;


// JUnitDoclet begin import
// JUnitDoclet end import

/**
* Generated by JUnitDoclet, a tool provided by
* ObjectFab GmbH under LGPL.
* Please see www.junitdoclet.org, www.gnu.org
* and www.objectfab.de for informations about
* the tool, the licence and the the authors.
*/


public class StdlibSuite
// JUnitDoclet begin extends_implements
// JUnitDoclet end extends_implements
{
  // JUnitDoclet begin class
  // JUnitDoclet end class
  
  public static TestSuite suite() {
    
    TestSuite suite;
    
    suite = new TestSuite("fr.fluxmedia.transmorpher.stdlib");
    
    // suite.addTestSuite(fr.fluxmedia.transmorpher.stdlib.broadcastTest.class);
    // suite.addTestSuite(fr.fluxmedia.transmorpher.stdlib.wrapTest.class);
    // suite.addTestSuite(fr.fluxmedia.transmorpher.stdlib.concatTest.class);
    // suite.addTestSuite(fr.fluxmedia.transmorpher.stdlib.directoryIteratorTest.class);
    // suite.addTestSuite(fr.fluxmedia.transmorpher.stdlib.integerIteratorTest.class);
    // suite.addTestSuite(fr.fluxmedia.transmorpher.stdlib.stdoutTest.class);
    // suite.addTestSuite(fr.fluxmedia.transmorpher.stdlib.xsltTest.class);
    // suite.addTestSuite(fr.fluxmedia.transmorpher.stdlib.tmqTest.class);
    // suite.addTestSuite(fr.fluxmedia.transmorpher.stdlib.readfileTest.class);
    
    
    
    // JUnitDoclet begin method suite()
		suite.addTestSuite(fr.fluxmedia.transmorpher.stdlib.ReadfileTest.class);
    suite.addTestSuite(fr.fluxmedia.transmorpher.stdlib.BroadcastTest.class);
    suite.addTestSuite(fr.fluxmedia.transmorpher.stdlib.ConcatTest.class);
		suite.addTestSuite(fr.fluxmedia.transmorpher.stdlib.IntegerIteratorTest.class);
    suite.addTestSuite(fr.fluxmedia.transmorpher.stdlib.FileIteratorTest.class);
    suite.addTestSuite(fr.fluxmedia.transmorpher.stdlib.FixpointIteratorTest.class);
    suite.addTestSuite(fr.fluxmedia.transmorpher.stdlib.TestingDispatcherTest.class);
		suite.addTestSuite(fr.fluxmedia.transmorpher.stdlib.XsltTest.class);
		suite.addTestSuite(fr.fluxmedia.transmorpher.stdlib.WritefileTest.class);
		suite.addTestSuite(fr.fluxmedia.transmorpher.stdlib.StdoutTest.class);
		// JUnitDoclet end method suite()
    
    return suite;
  }
}
