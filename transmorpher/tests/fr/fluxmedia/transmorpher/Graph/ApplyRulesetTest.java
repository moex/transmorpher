package fr.fluxmedia.transmorpher.Graph;

import junit.framework.TestCase;
// JUnitDoclet begin import
import fr.fluxmedia.transmorpher.Graph.ApplyRuleset;
// JUnitDoclet end import

/**
* Generated by JUnitDoclet, a tool provided by
* ObjectFab GmbH under LGPL.
* Please see www.junitdoclet.org, www.gnu.org
* and www.objectfab.de for informations about
* the tool, the licence and the the authors.
*/


public class ApplyRulesetTest
// JUnitDoclet begin extends_implements
extends TestCase
// JUnitDoclet end extends_implements
{
  // JUnitDoclet begin class
  fr.fluxmedia.transmorpher.Graph.ApplyRuleset applyruleset = null;
  // JUnitDoclet end class
  
  public ApplyRulesetTest(String name) {
    // JUnitDoclet begin method ApplyRulesetTest()
    super(name);
    // JUnitDoclet end method ApplyRulesetTest()
  }
  
  public void setUp() throws Exception {
    // JUnitDoclet begin method setUp()
    super.setUp();
    applyruleset = new fr.fluxmedia.transmorpher.Graph.ApplyRuleset();
    // JUnitDoclet end method setUp()
  }
  
  public void tearDown() throws Exception {
    // JUnitDoclet begin method tearDown()
    applyruleset = null;
    super.tearDown();
    // JUnitDoclet end method tearDown()
  }
  
  public void testRetroNull() throws Exception {
    // JUnitDoclet begin method testRetroNull()
    // JUnitDoclet end method testRetroNull()
  }
  
  public void testSetGetStrategy() throws Exception {
    // JUnitDoclet begin method testSetGetStrategy()
    java.lang.String[] tests = {null, "", " ", "a", "A", "�", "�", "0123456789", "012345678901234567890", "\n"};
    
    for (int i = 0; i < tests.length; i++) {
      applyruleset.setStrategy(tests[i]);
      assertEquals(tests[i], applyruleset.getStrategy());
    }
    // JUnitDoclet end method testSetGetStrategy()
  }
  
  public void testGenerateXML() throws Exception {
    // JUnitDoclet begin method testGenerateXML()
    // JUnitDoclet end method testGenerateXML()
  }
  
  public void testCreateProcess() throws Exception {
    // JUnitDoclet begin method testCreateProcess()
    // JUnitDoclet end method testCreateProcess()
  }
  
  public void testSetUp() throws Exception {
    // JUnitDoclet begin method testSetUp()
    // JUnitDoclet end method testSetUp()
  }
  
  public void testGenerateJavaCode() throws Exception {
    // JUnitDoclet begin method testGenerateJavaCode()
    // JUnitDoclet end method testGenerateJavaCode()
  }
  
  
  
  /**
  * JUnitDoclet moves marker to this method, if there is not match
  * for them in the regenerated code and if the marker is not empty.
  * This way, no test gets lost when regenerating after renaming.
  * Method testVault is supposed to be empty.
  */
  public void testVault() throws Exception {
    // JUnitDoclet begin method testVault()
    // JUnitDoclet end method testVault()
  }
  
}
