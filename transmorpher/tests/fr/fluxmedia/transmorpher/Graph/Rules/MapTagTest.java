package fr.fluxmedia.transmorpher.Graph.Rules;

import junit.framework.TestCase;
// JUnitDoclet begin import
import fr.fluxmedia.transmorpher.Graph.Rules.MapTag;
// JUnitDoclet end import

/**
* Generated by JUnitDoclet, a tool provided by
* ObjectFab GmbH under LGPL.
* Please see www.junitdoclet.org, www.gnu.org
* and www.objectfab.de for informations about
* the tool, the licence and the the authors.
*/


public class MapTagTest
// JUnitDoclet begin extends_implements
extends TestCase
// JUnitDoclet end extends_implements
{
  // JUnitDoclet begin class
  fr.fluxmedia.transmorpher.Graph.Rules.MapTag maptag = null;
  // JUnitDoclet end class
  
  public MapTagTest(String name) {
    // JUnitDoclet begin method MapTagTest()
    super(name);
    // JUnitDoclet end method MapTagTest()
  }
  
  public void setUp() throws Exception {
    // JUnitDoclet begin method setUp()
    super.setUp();
    maptag = new fr.fluxmedia.transmorpher.Graph.Rules.MapTag("tag", "subs");
	assertTrue( maptag.match.equals("tag") );
	assertTrue( maptag.target.equals("subs") );
	assertTrue( maptag.context == null );
    maptag = new fr.fluxmedia.transmorpher.Graph.Rules.MapTag("tag", "subs", "path");
	assertTrue( maptag.match.equals("tag") );
	assertTrue( maptag.target.equals("subs") );
	assertTrue( maptag.context != null );
	assertTrue( maptag.context.equals("path") );
    // JUnitDoclet end method setUp()
  }
  
  public void tearDown() throws Exception {
    // JUnitDoclet begin method tearDown()
    maptag = null;
    super.tearDown();
    // JUnitDoclet end method tearDown()
  }
  
  public void testGenerateXML() throws Exception {
    // JUnitDoclet begin method testGenerateXML()
    // JUnitDoclet end method testGenerateXML()
  }
  
  public void testGenerateXSLTCode() throws Exception {
    // JUnitDoclet begin method testGenerateXSLTCode()
    // JUnitDoclet end method testGenerateXSLTCode()
  }
  
  public void testGenerateInsideXSLTCode() throws Exception {
    // JUnitDoclet begin method testGenerateInsideXSLTCode()
    // JUnitDoclet end method testGenerateInsideXSLTCode()
  }
  
  public void testGenerateJavaCode() throws Exception {
    // JUnitDoclet begin method testGenerateJavaCode()
    // JUnitDoclet end method testGenerateJavaCode()
  }
  
  
  
  /**
  * JUnitDoclet moves marker to this method, if there is not match
  * for them in the regenerated code and if the marker is not empty.
  * This way, no test gets lost when regenerating after renaming.
  * Method testVault is supposed to be empty.
  */
  public void testVault() throws Exception {
    // JUnitDoclet begin method testVault()
    // JUnitDoclet end method testVault()
  }
  
}
