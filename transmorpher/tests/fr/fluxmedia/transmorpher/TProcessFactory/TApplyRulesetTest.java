package fr.fluxmedia.transmorpher.TProcessFactory;

import junit.framework.TestCase;
// JUnitDoclet begin import
import fr.fluxmedia.transmorpher.TProcessFactory.TApplyRuleset;
// JUnitDoclet end import

/**
* Generated by JUnitDoclet, a tool provided by
* ObjectFab GmbH under LGPL.
* Please see www.junitdoclet.org, www.gnu.org
* and www.objectfab.de for informations about
* the tool, the licence and the the authors.
*/


public class TApplyRulesetTest
// JUnitDoclet begin extends_implements
extends TestCase
// JUnitDoclet end extends_implements
{
  // JUnitDoclet begin class
  fr.fluxmedia.transmorpher.TProcessFactory.TApplyRuleset tapplyruleset = null;
  // JUnitDoclet end class
  
  public TApplyRulesetTest(String name) {
    // JUnitDoclet begin method TApplyRulesetTest()
    super(name);
    // JUnitDoclet end method TApplyRulesetTest()
  }
  
  public void setUp() throws Exception {
    // JUnitDoclet begin method setUp()
    super.setUp();
    tapplyruleset = new fr.fluxmedia.transmorpher.TProcessFactory.TApplyRuleset();
    // JUnitDoclet end method setUp()
  }
  
  public void tearDown() throws Exception {
    // JUnitDoclet begin method tearDown()
    tapplyruleset = null;
    super.tearDown();
    // JUnitDoclet end method tearDown()
  }
  
  
  
  /**
  * JUnitDoclet moves marker to this method, if there is not match
  * for them in the regenerated code and if the marker is not empty.
  * This way, no test gets lost when regenerating after renaming.
  * Method testVault is supposed to be empty.
  */
  public void testVault() throws Exception {
    // JUnitDoclet begin method testVault()
    // JUnitDoclet end method testVault()
  }
  
}
