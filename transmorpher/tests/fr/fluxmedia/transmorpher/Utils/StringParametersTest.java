package fr.fluxmedia.transmorpher.Utils;

import junit.framework.TestCase;
// JUnitDoclet begin import
import fr.fluxmedia.transmorpher.Utils.StringParameters;
// JUnitDoclet end import

/**
* Generated by JUnitDoclet, a tool provided by
* ObjectFab GmbH under LGPL.
* Please see www.junitdoclet.org, www.gnu.org
* and www.objectfab.de for informations about
* the tool, the licence and the the authors.
*/


public class StringParametersTest
// JUnitDoclet begin extends_implements
extends TestCase
// JUnitDoclet end extends_implements
{
  // JUnitDoclet begin class
  fr.fluxmedia.transmorpher.Utils.StringParameters stringparameters = null;
  // JUnitDoclet end class
  
  public StringParametersTest(String name) {
    // JUnitDoclet begin method StringParametersTest()
    super(name);
    // JUnitDoclet end method StringParametersTest()
  }
  
  public void setUp() throws Exception {
    // JUnitDoclet begin method setUp()
    super.setUp();
    stringparameters = new fr.fluxmedia.transmorpher.Utils.StringParameters();
    // JUnitDoclet end method setUp()
  }
  
  public void tearDown() throws Exception {
    // JUnitDoclet begin method tearDown()
    stringparameters = null;
    super.tearDown();
    // JUnitDoclet end method tearDown()
  }
  
  public void testSetParameter() throws Exception {
    // JUnitDoclet begin method testSetParameter()
    // JUnitDoclet end method testSetParameter()
  }
  
  public void testGetStringParameter() throws Exception {
    // JUnitDoclet begin method testGetStringParameter()
    // JUnitDoclet end method testGetStringParameter()
  }
  
  
  
  /**
  * JUnitDoclet moves marker to this method, if there is not match
  * for them in the regenerated code and if the marker is not empty.
  * This way, no test gets lost when regenerating after renaming.
  * Method testVault is supposed to be empty.
  */
  public void testVault() throws Exception {
    // JUnitDoclet begin method testVault()
    // JUnitDoclet end method testVault()
  }
  
}
