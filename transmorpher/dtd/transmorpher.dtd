<?xml version="1.0" encoding="UTF-8" ?>
<!-- $Id: transmorpher.dtd,v 1.27 2003-01-15 12:47:22 triolet Exp $
 !
 ! Transmorpher
 ! 
 ! Copyright (C) 2001-2002 Fluxmedia and INRIA Rhone-Alpes.
 !
 ! http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 ! 
 ! This program is free software; you can redistribute it and/or
 ! modify it under the terms of the GNU General Public License
 ! as published by the Free Software Foundation; either version 2
 ! of the License, or (at your option) any later version.
 ! 
 ! This program is distributed in the hope that it will be useful,
 ! but WITHOUT ANY WARRANTY; without even the implied warranty of
 ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ! GNU General Public License for more details.
 ! 
 ! You should have received a copy of the GNU General Public License
 ! along with this program; if not, write to the Free Software
 ! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 -->

<!-- ############################################### -->
<!--                 Imported DTDs                   -->
<!-- ############################################### -->           

<!ENTITY % queryDTD SYSTEM "query.dtd">
%queryDTD; 

<!ENTITY % annotDTD SYSTEM "annot.dtd">
<!-- %annotDTD; -->

<!-- That of XSLT -->
<!-- XPath is used but does not require a DTD -->

<!-- ############################################### -->
<!--                 pseudo macros                   -->
<!-- ############################################### -->           

<!ENTITY % path "CDATA">		<!-- xpath: -->
<!ENTITY % uri "CDATA">			<!-- URI -->
<!ENTITY % param "CDATA">		<!-- channel specification -->
<!ENTITY % regexp "CDATA">		<!-- regular expression -->
<!ENTITY % key "CDATA">			<!-- an identifier -->

<!ENTITY % id "id %key; #REQUIRED">	<!-- a name for displaying -->
<!ENTITY % idop "id CDATA #IMPLIED">	<!-- an (optional) name for displaying -->
<!ENTITY % name "name ID #REQUIRED">	<!-- a name for identifying -->
<!ENTITY % ref "ref %key; #REQUIRED">	<!-- a reference to identified -->

<!ENTITY % type "type CDATA #REQUIRED">	<!-- type of a subtyped class -->

<!ENTITY % input "in %param; #IMPLIED">	<!-- input channels -->
<!ENTITY % output "out %param; #IMPLIED">	<!-- output channels -->
<!ENTITY % file "file %uri; #IMPLIED">	<!-- input file for compatibility -->

<!ENTITY % match "match CDATA #REQUIRED">
<!ENTITY % context "context %path; #IMPLIED">
<!ENTITY % target "target %key; #REQUIRED">
<!ENTITY % value "value CDATA #REQUIRED">

<!-- ############################################### -->
<!--          parameters				-->
<!-- ############################################### -->           


<!ELEMENT param ANY>
<!ATTLIST param name CDATA #REQUIRED
		select %path; #IMPLIED>

<!ELEMENT with-param ANY>
<!ATTLIST with-param name CDATA #REQUIRED
		select %path; #IMPLIED>

<!-- ############################################### -->
<!--          top-level: transmorpher                -->
<!-- ############################################### -->           

<!-- stylesheet -->


<!ELEMENT transmorpher ((include|import|defextern)*
				,(ruleset
                   		|process
                  		|query
                   		|annotations
                   		)*,(servlet|transformer|main)?)>
<!ATTLIST transmorpher
	version CDATA #REQUIRED
	%name;
        optimized (true|false) 'false'
	reloc CDATA #IMPLIED
	xmlns %uri; "http://transmorpher.fluxmedia.fr/1.0"
	xmlns:xsl %uri; #FIXED "http://www.w3.org/1999/XSL/Transform">

<!ELEMENT include EMPTY>
<!ATTLIST include href %uri; #REQUIRED>

<!ELEMENT import EMPTY>
<!ATTLIST import href %uri; #REQUIRED>

<!ELEMENT defextern EMPTY>
<!ATTLIST defextern name CDATA #REQUIRED
                    class CDATA #REQUIRED
                    default (true|false) 'false'
                    implements (external|query|dispatcher|generator|serializer|merger) 'external'
>

<!-- ############################################### -->
<!--                      ruleset                    -->
<!-- ############################################### -->           


<!ELEMENT ruleset (param*,(namespace|extends|maptag|modtag|addtag|remtag|flatten|
                       mapatt|rematt|resubst)*)>
<!-- add |xsl:template -->
<!ATTLIST ruleset %name;>

<!ELEMENT namespace EMPTY>
<!ATTLIST namespace
	%name;
	uri %uri; #REQUIRED>	

<!ELEMENT extends EMPTY>
<!ATTLIST extends
	%ref;>	

<!ELEMENT maptag EMPTY>
<!ATTLIST maptag
	%match;
	%target;
        %context;>

<!ELEMENT modtag (mapatt|rematt|addatt|defatt)*> 
<!ATTLIST modtag 
	%match;
		%target;
        %context;>

<!ELEMENT addtag ANY>
<!ATTLIST addtag
	%match;
        %context;>

<!ELEMENT remtag EMPTY>
<!ATTLIST remtag
	%match;
        %context;> 

<!ELEMENT flatten EMPTY> 
<!ATTLIST flatten 
	%match;> 

<!ELEMENT mapatt EMPTY>
<!ATTLIST mapatt
	%match;
	%target;
        %context;>

<!ELEMENT addatt EMPTY>
<!ATTLIST addatt
	%match;
	%value;>

<!ELEMENT rematt EMPTY>
<!ATTLIST rematt
	%match;
        %context;>

<!ELEMENT resubst EMPTY>
<!ATTLIST resubst
        %context;
	%match;
	source %regexp; #REQUIRED
	target %regexp; #REQUIRED
	apply (once|once-per-line|all) 'all'>

<!-- ############################################### -->
<!--                      process                    -->
<!-- ############################################### -->           

<!ENTITY % expr "repeat
			|generate
			|serialize
			|dispatch
			|merge
			|apply-process
			|apply-ruleset
			|apply-query
			|apply-external
			">

<!ELEMENT process (param*,(%expr;)*)>
<!ATTLIST process
	%name;
	%input;
	%output;>

<!-- Calls -->

<!ELEMENT repeat (with-param*,iterate*,(%expr;)*)>
<!ATTLIST repeat
	%id;
	times CDATA "*"
	%input;
	bin %param; #IMPLIED
	%output;
	bout %param; #IMPLIED
	test %param; #IMPLIED>

<!ELEMENT iterate (with-param*)>
<!ATTLIST iterate %type;
									name CDATA #IMPLIED
                  from CDATA #IMPLIED
                  to CDATA #IMPLIED
                  in CDATA #IMPLIED
                  incr CDATA #IMPLIED
                  filter CDATA #IMPLIED
                  context CDATA #IMPLIED
                  select CDATA #IMPLIED>

<!ELEMENT apply-external (with-param*)>
<!ATTLIST apply-external
	%id;
	%type; 
	%file;
	%input;
	%output;>

<!ELEMENT apply-ruleset (with-param*)>
<!ATTLIST apply-ruleset
	%id;
	%ref;
	strategy (topdown|bottomup) 'topdown'
	%input;
	%output;>

<!-- this should take either file or ref -->
<!ELEMENT apply-query (with-param*)> 
<!ATTLIST apply-query 
	%id;
	%type; 
	%file;
	%ref;
	%input;
	%output;>

<!ELEMENT apply-process (with-param*)>
<!ATTLIST apply-process
	%id;
	%ref;
	%input;
	%output;>

<!ELEMENT dispatch (with-param*)> 
<!ATTLIST dispatch 
	%idop;
	%type;
	%input;
	%output;>

<!ELEMENT merge (with-param*)> 
<!ATTLIST merge 
	%idop;
	%type;
	%output;
	%input;>

<!ELEMENT generate (with-param*)> 
<!ATTLIST generate 
	%idop;
	%type;
        %file;
	%input; 
	%output;>

<!ELEMENT serialize (with-param*)> 
<!ATTLIST serialize 
	%idop;
	%type;
	%file;
	%input;>

<!-- ############################################### -->
<!--                 The main process                -->
<!-- ############################################### -->           

<!ELEMENT main (param*,(%expr;)*)> 
<!ATTLIST main %name;> 
  
<!ELEMENT servlet (param*,(%expr;)*)> 
<!ATTLIST servlet %name;
		optimize (true|false) 'false'> 
  
<!ELEMENT transformer (param*,(%expr;)*)> 
<!ATTLIST transformer %name;
		optimize (true|false) 'false'> 



