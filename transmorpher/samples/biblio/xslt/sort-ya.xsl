<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->
<!-- $Id: sort-ya.xsl,v 1.5 2007-03-08 11:21:48 euzenat Exp $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://exmo.inrialpes.fr/papers">

<xsl:output
  method="xml"
  version="1.0"
  encoding="iso-8859-1"
  omit-xml-declaration="no"
  standalone="no"
  doctype-system="http://exmo.inrialpes.fr/papers/xml/bib.dtd"
  indent="yes"/> 

<!-- toplevel -->

<xsl:template match="/">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="bibliography">
<bibliography name="{@name}" version="{@version}" date="{@date}" author="{@author}">
	<xsl:apply-templates select="reference">
          <xsl:sort select="date/@year" order="descending" />
	  <xsl:sort select="(authors/p[1]/@last)|(editors/p[1]/@last)|(directors/p[1]/@last)"/>
	</xsl:apply-templates>
</bibliography>
</xsl:template>

<xsl:template match="*|@*|text()">
	<xsl:copy>
		<xsl:apply-templates select="*|@*|text()"/>
	</xsl:copy>
</xsl:template>

</xsl:stylesheet>
