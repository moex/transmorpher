<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->
<!-- $Id: form-ftype.xsl,v 1.3 2007-03-08 11:21:47 euzenat Exp $ -->

<xsl:stylesheet version="1.0"
  xmlns:datext="http://www.jclark.com/xt/java/java.util.Date"
  xmlns:datexa="xalan://java.util.Date"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers"
  xmlns:fo="http://www.w3.org/1999/XSL/Format">

<!-- toplevel -->

<xsl:include href="form-fo.xsl" />

<xsl:param name="format">html</xsl:param>
<xsl:param name="sort">ya</xsl:param>
<xsl:param name="layout"/>
<xsl:param name="filename"/>

<!-- article|book|inbook|incollection article|conference|inproceedings|proceedings|phdthesis|prospective|mastersthesis|booklet|manual|techreport|misc|unpublished -->


  <xsl:template match="bibliography">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">

      <!-- PAPIER A4 -->
      <fo:layout-master-set>
        <fo:simple-page-master master-name="A4"
           page-width="210mm"  page-height="297mm"
           margin-top="0.5in"  margin-bottom="0.5in"
           margin-left="0.5in" margin-right="0.5in">
          <fo:region-before extent="1.0in" />
          <fo:region-body margin-top="0.5in"
                          margin-bottom="0.5in"/>
          <fo:region-after extent="0.5in" />
        </fo:simple-page-master>
      </fo:layout-master-set>

      <fo:page-sequence master-reference="A4" initial-page-number="1">

        <!-- EN TETE -->
        <fo:static-content flow-name="xsl-region-before">
          <fo:block line-height="10pt">
            <fo:leader leader-length="19.5cm" leader-pattern="rule"
                       rule-thickness="1pt" color="black"/>
          </fo:block>
        </fo:static-content>

        <!-- PIED DE PAGE -->
        <fo:static-content flow-name="xsl-region-after">
          <fo:block text-align="left">
	  	<xsl:value-of select="@name"/><xsl:text> </xsl:text>
	  	(version <xsl:value-of select="@version"/>)
          </fo:block>
          <fo:block text-align="right">
            <fo:page-number/>
          </fo:block>
        </fo:static-content>

        <!-- CORPS -->
        <fo:flow flow-name="xsl-region-body" font-size="10pt"
		 font-family="Times" line-height="15pt"
                 text-align="justify" space-after="50pt">
          <fo:block font-size="14pt">
		    <xsl:value-of select="@name"/> (<xsl:value-of select="@date"/>)
	  </fo:block>


<!-- BEGINING -->
	<xsl:for-each select="reference">
		<xsl:if test="not(preceding-sibling::reference) or (preceding-sibling::reference[position()=1]/@type != ./@type)">
			<xsl:choose>
				<xsl:when test="@type='article'">
<fo:block text-align="center" font-weight="bold" space-before="12pt" font-size="12pt">Journal articles/Articles de revues</fo:block>
		                </xsl:when>
		                <xsl:when test="@type='book'">
<fo:block text-align="center" font-weight="bold" space-before="12pt" font-size="12pt">Books/Monographies</fo:block>
		                </xsl:when>
		                <xsl:when test="(@type='inbook' and preceding-sibling::reference[position()=1]/@type != 'incollection') or (@type='incollection' and preceding-sibling::reference[position()=1]/@type != 'inbook')">
<fo:block text-align="center" font-weight="bold" space-before="12pt" font-size="12pt">Book chapters and collected papers/Chapitres de livres</fo:block>
		                </xsl:when>
                		<xsl:when test="@type='collection'">
<fo:block text-align="center" font-weight="bold" space-before="12pt" font-size="12pt">Book coordination/Coordination d'ouvrages</fo:block>
		                </xsl:when>
                		<xsl:when test="@type='specialissue'">
<fo:block text-align="center" font-weight="bold" space-before="12pt" font-size="12pt">Journal special issue/�diteur invit�</fo:block>
		                </xsl:when>
                		<xsl:when test="(@type='proceedings' and preceding-sibling::reference[position()=1]/@type != 'serialproceedings') or (@type='serialproceedings' and preceding-sibling::reference[position()=1]/@type != 'proceedings')">
<fo:block text-align="center" font-weight="bold" space-before="12pt" font-size="12pt">Conference editing/Publication d'actes</fo:block>
		                </xsl:when>
		                <xsl:when test="@type='inproceedings'">
<fo:block text-align="center" font-weight="bold" space-before="12pt" font-size="12pt">Conference papers/Communication � des conf�rences</fo:block>
		                </xsl:when>
                		<xsl:when test="@type='phd'">
<fo:block text-align="center" font-weight="bold" space-before="12pt" font-size="12pt">PhD theses/Th�ses de doctorats et habilitations</fo:block>
                		</xsl:when>
               			 <xsl:when test="@type='prospective'">
<fo:block text-align="center" font-weight="bold" space-before="12pt" font-size="12pt">Prospective reports/Rapports de prospective</fo:block>
                		</xsl:when>
		                <xsl:when test="@type='lecturenotes'">
<fo:block text-align="center" font-weight="bold" space-before="12pt" font-size="12pt">Lecture notes/Notes de cours</fo:block>
		                </xsl:when>
		                <xsl:when test="@type='recension'">
<fo:block text-align="center" font-weight="bold" space-before="12pt" font-size="12pt">Reviews/Recensions</fo:block>
		                </xsl:when>
                		<xsl:when test="@type='techreport'">
<fo:block text-align="center" font-weight="bold" space-before="12pt" font-size="12pt">Technical reports/Rapports de recherche</fo:block>
                		</xsl:when>
                		<xsl:when test="@type='deliverable'">
<fo:block text-align="center" font-weight="bold" space-before="12pt" font-size="12pt">Deliverables/Rapports de contrats</fo:block>
                		</xsl:when>
               			 <xsl:when test="@type='mastersthesis'">
<fo:block text-align="center" font-weight="bold" space-before="12pt" font-size="12pt">Master theses/M�moires de DEA</fo:block>
                		</xsl:when>
               			 <xsl:when test="@type='bulletin'">
<fo:block text-align="center" font-weight="bold" space-before="12pt" font-size="12pt">Non reviewed articles/Articles non rapport�s</fo:block>
                		</xsl:when>
		                <xsl:when test="@type='motionpicture'">
<fo:block text-align="center" font-weight="bold" space-before="12pt" font-size="12pt">Motion pictures/Oeuvre audio-visuelle</fo:block>
                		</xsl:when>
		                <xsl:when test="(@type='booklet' and preceding-sibling::reference[position()=1]/@type != 'manual') or (@type='manual' and preceding-sibling::reference[position()=1]/@type != 'booklet')">
<fo:block text-align="center" font-weight="bold" space-before="12pt" font-size="12pt">Booklets and manuals/Manuels</fo:block>
 		               </xsl:when>
		                <xsl:when test="(@type='misc' and preceding-sibling::reference[position()=1]/@type != 'internalreport') or (@type='internalreport' and preceding-sibling::reference[position()=1]/@type != 'misc')">
<fo:block text-align="center" font-weight="bold" space-before="12pt" font-size="12pt">Miscellaneous/Divers documents publics</fo:block>
 		               </xsl:when>
                		<xsl:when test="@type='unpublished'">
<fo:block text-align="center" font-weight="bold" space-before="12pt" font-size="12pt">Unpublished/Textes non publi�s</fo:block>
                		</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:apply-templates select="."/>
	</xsl:for-each>

<!-- END -->
          <fo:block line-height="10pt">
            <fo:leader leader-length="15cm" leader-pattern="rule"
                       rule-thickness="1pt" color="black"/>
          </fo:block>
          <fo:block font-size="9pt" font-family="Times" line-height="9pt">
        <xsl:text>Generated by Transmorpher</xsl:text>
        <!-- for XT -->
	<xsl:if test="function-available('datext:to-string') and function-available('datext:new')">
		<xsl:text> on </xsl:text><xsl:value-of select="datext:to-string(datext:new())"/>
	</xsl:if>
        <!-- for Xalan -->
        <xsl:if test="function-available('datexa:toString')">
		<xsl:text> on </xsl:text><xsl:value-of select="datexa:toString(datexa:new())"/>
	</xsl:if>
	  	
          </fo:block>
        </fo:flow>
      </fo:page-sequence>

    </fo:root>
  </xsl:template>

</xsl:stylesheet>
