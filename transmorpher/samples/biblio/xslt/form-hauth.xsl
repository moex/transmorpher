<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->
<!-- $Id: form-hauth.xsl,v 1.6 2007-03-08 11:21:48 euzenat Exp $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers">

<xsl:import href="form-hh.xsl" />

<xsl:param name="format">html</xsl:param>             
<xsl:param name="sort">ay</xsl:param>               
<xsl:param name="abstrip">off</xsl:param>
<xsl:param name="layout">spare</xsl:param>
<xsl:param name="filename"/>

<!-- toplevel -->

<xsl:template match="bibliography">
<h1><xsl:value-of select="@name" /> (<xsl:value-of select="@date"/>)</h1>


<dl>
	<xsl:for-each select="reference">
          <xsl:variable name="author" select="(./authors/p[1]/@last)|(./editors/p[1]/@last)|(./directors/p[1]/@last)"/>
          <xsl:choose>
          <xsl:when test="not(preceding-sibling::reference)">
            <dt><h2><xsl:value-of select="$author" /><i> and others</i></h2></dt><dd></dd>
          </xsl:when>
          <xsl:otherwise>
            <xsl:variable name="last" select="(preceding-sibling::reference[position()=1]/authors/p[1]/@last)|(preceding-sibling::reference[position()=1]/editors/p[1]/@last)|(preceding-sibling::reference[position()=1]/directors/p[1]/@last)"/>
            <xsl:if test="not($author = $last)">
              <dt><h2><xsl:value-of select="$author" /><i> and others</i></h2></dt><dd></dd>
            </xsl:if>
              </xsl:otherwise>
            </xsl:choose>
		<xsl:apply-templates select="."/>
	</xsl:for-each>
</dl>
</xsl:template>

</xsl:stylesheet>
