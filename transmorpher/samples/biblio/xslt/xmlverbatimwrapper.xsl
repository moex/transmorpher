<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- $Id: xmlverbatimwrapper.xsl,v 1.5 2007-03-08 11:21:48 euzenat Exp $ -->
<!--
   XML to HTML Verbatim Formatter with Syntax Highlighting
   HTML wrapper
   Version 1.0.1
   GPL (c) Oliver Becker, 2000-06-19
   obecker@informatik.hu-berlin.de
-->

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

   <xsl:import href="xmlverbatim.xsl" />

   <xsl:output method="html"
               doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN" />

   <!-- CSS Stylesheet -->
   <xsl:param name="css-stylesheet" select="'http://barbara.inrialpes.fr/xslt/xmlverbatim.css'" />

   <!-- root -->
   <xsl:template match="/">
      <html>
         <head>
            <title>XML source view</title>
            <link rel="stylesheet" type="text/css" 
	          href="{$css-stylesheet}" />
         </head>
         <body class="xmlverb-default">
            <tt>
               <xsl:apply-templates select="." mode="xmlverb" />
            </tt>
         </body>
      </html>
      <xsl:text>&#xA;</xsl:text>
   </xsl:template>

</xsl:stylesheet>
