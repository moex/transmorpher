<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->
<!-- $Id: form-fyear.xsl,v 1.3 2007-03-08 11:21:47 euzenat Exp $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers">

<xsl:import href="form-fo.xsl" />

<xsl:param name="format"/>
<xsl:param name="sort">ya</xsl:param>               
<xsl:param name="layout"/>
<xsl:param name="filename"/>

<!-- toplevel -->

<xsl:template match="bibliography">
<h1><xsl:value-of select="@name" /> (<xsl:value-of select="@date"/>)</h1>


<dl>
	<xsl:for-each select="reference">
          <xsl:if test="not(preceding-sibling::reference) or (preceding-sibling::reference[position()=1]/date/@year != ./date/@year)">
            <dt><h2><xsl:value-of select="./date/@year" /></h2></dt><dd></dd>
		</xsl:if>
		<xsl:apply-templates select="."/>
	</xsl:for-each>
</dl>
</xsl:template>

</xsl:stylesheet>
