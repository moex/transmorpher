<?xml version="1.0" encoding="iso-8859-1"?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->
<!-- $Id: strip-abstract.xsl,v 1.3 2007-03-08 11:21:48 euzenat Exp $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://exmo.inrialpes.fr/papers"
  result-ns=""
  indent="yes">

<xsl:output
  method="xml"
  version="1.0"
  encoding="iso-8859-1"
  omit-xml-declaration="no"
  standalone="no"
  doctype-system="http://exmo.inrialpes.fr/papers/xml/bib.dtd"
  indent="yes"/> 

<!-- toplevel -->

<xsl:template match="/">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="bibliography">
<bibliography name="{@name}" version="{@version}" date="{@date}">
	<xsl:apply-templates select="reference"/>
</bibliography>
</xsl:template>

<xsl:template match="*|@*|text()">
	<xsl:if test="not(@status='hidden')">
		<xsl:copy>
			<xsl:apply-templates select="*|@*|text()"/>
		</xsl:copy>
	</xsl:if>
</xsl:template>

<xsl:template match="abstract|keywords|areas|comments|notes|softwares|contracts"/>

</xsl:stylesheet>
