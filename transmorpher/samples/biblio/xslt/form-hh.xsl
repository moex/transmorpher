<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>
<!-- $Id: form-hh.xsl,v 1.6 2007-03-08 11:21:48 euzenat Exp $ -->

<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->

<!-- This stylesheet defines the HTML formats for pages and reference.
     It is called by the other form-hXXXX.xsl stylesheet for adding the 
     grouping information by the main sorting key -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers">

<xsl:import href="common.xsl" />
<xsl:include href="form-exmo.xsl" />
<xsl:include href="form-je.xsl" />
<xsl:include href="form-spare.xsl" />

<xsl:template match="/">
  <xsl:choose>
    <!--xsl:when test="$layout and $layout!=''">
	<xsl:apply-templates select="." mode="$layout" />
    </xsl:when-->
    <xsl:when test="$layout='exmo'">
	<xsl:apply-templates select="." mode="exmo"/>
    </xsl:when>
    <xsl:when test="$layout='je'">
	<xsl:apply-templates select="." mode="je"/>
    </xsl:when>
    <xsl:otherwise>
        <!-- should even be better with a default mode -->
	<xsl:apply-templates select="." mode="spare">
	  <xsl:with-param name="filename"><xsl:value-of select="$filename"/></xsl:with-param>
	</xsl:apply-templates>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="reference" mode="linear">
  <xsl:choose>
    <xsl:when test="authors|directors|scenarists|interprets|voices">
      <xsl:apply-templates select="authors|directors|scenarists|interprets|voices" mode="linear">
        <xsl:with-param name="language" select="@language"/>
      </xsl:apply-templates>
    </xsl:when>
    <xsl:when test="editors">
      <xsl:apply-templates select="editors" mode="linear">
        <xsl:with-param name="language" select="@language"/>
      </xsl:apply-templates>
    </xsl:when>
  </xsl:choose>
  <xsl:call-template name="gettitle"/><xsl:text>, </xsl:text>
  <xsl:apply-templates select="." mode="line3"/>
</xsl:template>


<xsl:template match="reference" mode="line3">
	<xsl:choose>
	<xsl:when test="@type='article' or @type='specialissue' or @type='recension' or @type='bulletin'">
		<i><xsl:value-of select="journal/@name"/></i>
			<xsl:if
			test="@type='specialissue'"><xsl:text> </xsl:text><xsl:choose><xsl:when
			test="@language='fr'">(num�ro sp�cial)</xsl:when>
			      <xsl:otherwise>(special issue)</xsl:otherwise>
                  </xsl:choose></xsl:if>
			<xsl:if test="volume"><xsl:text> </xsl:text><xsl:value-of select="volume/text()"/></xsl:if>
			<xsl:if test="number">(<xsl:value-of select="number/text()"/>)</xsl:if>
			<xsl:if test="pages">:<xsl:value-of select="pages/text()"/></xsl:if>
			<xsl:text>, </xsl:text>
                        <xsl:value-of select="date/@year"/>
	</xsl:when>
	<xsl:when test="@type='unpublished'">
		Unpublished <xsl:if test="date/@year"><xsl:text>, </xsl:text><xsl:value-of select="date/@year"/></xsl:if>
		</xsl:when>
	<xsl:when test="@type='prospective' or @type='techreport' or @type='internalreport'">
		<xsl:choose>
			<xsl:when test="type"><xsl:value-of select="type/text()"/></xsl:when>
			<xsl:when test="@type='techreport'">Technical report</xsl:when>
			<xsl:when test="@type='techreport'">Prospective report</xsl:when>
			<xsl:when test="@type='internalreport'">Internal report</xsl:when>
		</xsl:choose>
		<xsl:if test="number"><xsl:text> </xsl:text><xsl:value-of select="number/text()"/></xsl:if>
		<xsl:text>, </xsl:text>
		<xsl:if test="institution">
			<xsl:value-of select="institution/@name"/><xsl:text>, </xsl:text>
			<xsl:if test="institution/location">
				<xsl:apply-templates select="institution/location"/><xsl:text>, </xsl:text>
			</xsl:if>
		</xsl:if>
                <xsl:if test="length and (length/@unit='p')">
                  <xsl:value-of select="length/text()"/><xsl:text>p., </xsl:text>
		</xsl:if>
		<xsl:if test="date/@month">
			<xsl:call-template name="codemonth">
                        	<xsl:with-param name="month" select="date/@month"/>
                		<xsl:with-param name="language" select="@language"/>
                              </xsl:call-template><xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="date/@year"/>
	</xsl:when>
	<xsl:when test="@type='deliverable'">
		<xsl:choose>
			<xsl:when test="type"><xsl:value-of select="type/text()"/></xsl:when>
			<xsl:otherwise>Deliverable</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="number"><xsl:text> </xsl:text><xsl:value-of select="number/text()"/></xsl:if>
		<xsl:text>, </xsl:text>
		<xsl:if test="contract">
                  <xsl:value-of select="contract/text()"/><xsl:text>, </xsl:text>
		</xsl:if>
		<xsl:if test="institution">
			<xsl:value-of select="institution/@name"/><xsl:text>, </xsl:text>
			<xsl:if test="institution/location">
				<xsl:apply-templates select="institution/location"/><xsl:text>, </xsl:text>
			</xsl:if>
		</xsl:if>
                <xsl:if test="length and (length/@unit='p')">
                  <xsl:value-of select="length/text()"/><xsl:text>p., </xsl:text>
		</xsl:if>
		<xsl:if test="date/@month">
			<xsl:call-template name="codemonth">
                        	<xsl:with-param name="month" select="date/@month"/>
                		<xsl:with-param name="language" select="@language"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:value-of select="date/@year"/>
	</xsl:when>
	<xsl:when test="@type='mastersthesis'">
		<xsl:choose>
			<xsl:when test="type"><xsl:value-of select="type/text()"/></xsl:when>
			<xsl:otherwise>Master thesis</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="number"><xsl:text> </xsl:text><xsl:value-of select="number/text()"/></xsl:if>
		<xsl:text>, </xsl:text>
			<xsl:if test="school">
				<xsl:value-of select="school/@name"/><xsl:text>, </xsl:text>
				<xsl:if test="school/location">
					<xsl:apply-templates select="school/location"/><xsl:text>, </xsl:text>
				</xsl:if>
			</xsl:if>
                <xsl:if test="length and (length/@unit='p')">
                  <xsl:value-of select="length/text()"/><xsl:text>p., </xsl:text>
		</xsl:if>
		<xsl:if test="date/@month">
			<xsl:call-template name="codemonth">
                        	<xsl:with-param name="month" select="date/@month"/>
                		<xsl:with-param name="language" select="@language"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:value-of select="date/@year"/>
	</xsl:when>
	<xsl:when test="@type='phd'">
		<xsl:choose>
			<xsl:when test="type"><xsl:value-of select="type/text()"/></xsl:when>
			<xsl:otherwise>PhD thesis</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="number"><xsl:text> </xsl:text><xsl:value-of select="number/text()"/></xsl:if>
		<xsl:text>, </xsl:text>
			<xsl:if test="school">
				<xsl:value-of select="school/@name"/><xsl:text>, </xsl:text>
				<xsl:if test="school/location">
					<xsl:apply-templates select="school/location"/><xsl:text>, </xsl:text>
				</xsl:if>
			</xsl:if>
                <xsl:if test="length and (length/@unit='p')">
                  <xsl:value-of select="length/text()"/><xsl:text>p., </xsl:text>
		</xsl:if>
		<xsl:if test="date/@month">
			<xsl:call-template name="codemonth">
                        	<xsl:with-param name="month" select="date/@month"/>
                		<xsl:with-param name="language" select="@language"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:value-of select="date/@year"/>
	</xsl:when>
	<!-- OR conference -->
	<xsl:when test="@type='proceedings'">
		<xsl:choose>
			<xsl:when test="publisher">
				<xsl:value-of select="publisher/@name"/><xsl:text>, </xsl:text>
				<xsl:if test="publisher/location">
					<xsl:apply-templates select="publisher/location"/><xsl:text>, </xsl:text>
				</xsl:if>
			</xsl:when>
			<xsl:when test="institution">
				<xsl:value-of select="institution/@name"/><xsl:text>, </xsl:text>
				<xsl:if test="institution/location">
					<xsl:apply-templates select="institution/location"/><xsl:text>, </xsl:text>
				</xsl:if>
			</xsl:when>
		</xsl:choose>
                <xsl:if test="length and (length/@unit='p')">
                  <xsl:value-of select="length/text()"/><xsl:text>p., </xsl:text>
		</xsl:if>
		<xsl:value-of select="date/@year"/>
	</xsl:when>
	<xsl:when test="@type='manual'">
		<xsl:choose>
			<xsl:when test="type"><xsl:value-of select="type/text()"/></xsl:when>
			<xsl:otherwise>Manual</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="edition"><xsl:text> (</xsl:text><xsl:value-of select="edition"/>
                  <xsl:choose><xsl:when test="@language='fr'">�d</xsl:when>
			      <xsl:otherwise>ed</xsl:otherwise>
                  </xsl:choose><xsl:text>.)</xsl:text>
                </xsl:if>
                <xsl:text>, </xsl:text>
		<xsl:if test="institution">
			<xsl:value-of select="institution/@name"/><xsl:text>, </xsl:text>
			<xsl:if test="institution/location">
				<xsl:apply-templates select="institution/location"/><xsl:text>, </xsl:text>
			</xsl:if>
		</xsl:if>
                <xsl:if test="length and (length/@unit='p')">
                  <xsl:value-of select="length/text()"/><xsl:text>p., </xsl:text>
		</xsl:if>
		<xsl:if test="date/@month">
			<xsl:call-template name="codemonth">
                        	<xsl:with-param name="month" select="date/@month"/>
                		<xsl:with-param name="language" select="@language"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:value-of select="date/@year"/>
	</xsl:when>
	<xsl:when test="@type='inproceedings'">
	  <xsl:text>in: </xsl:text>
          <xsl:if test="editors">
            <xsl:apply-templates select="editors" mode="linear">
              <xsl:with-param name="language" select="@language"/>
            </xsl:apply-templates>
          </xsl:if>
	  <xsl:call-template name="confname">
	    <xsl:with-param name="conf" select="conference"/>
	  </xsl:call-template>
	  <xsl:text>, </xsl:text>
	  <xsl:if test="conference/location">
	    <xsl:apply-templates select="conference/location"/><xsl:text>, </xsl:text>
	  </xsl:if>
	  <xsl:if test="in">
            <xsl:text>(</xsl:text><xsl:apply-templates select="in/reference" mode="linear"/><xsl:text>), </xsl:text>
	  </xsl:if>
	  <xsl:if test="red">
            <xsl:text>(rev. </xsl:text>
            <xsl:apply-templates select="red/reference" mode="linear"/><xsl:text>), </xsl:text>
	  </xsl:if>
	  <xsl:if test="pages">
	    <xsl:text>pp</xsl:text>
	    <xsl:value-of select="pages/text()"/>
	    <xsl:text>, </xsl:text>
	  </xsl:if>
          <xsl:if test="date/@month"><xsl:text>(</xsl:text>
            <xsl:if test="date/@day">
	      <xsl:value-of select="date/@day"/><xsl:text> </xsl:text>
	    </xsl:if>
	    <xsl:call-template name="codemonth">
              <xsl:with-param name="month" select="date/@month"/>
              <xsl:with-param name="language" select="@language"/>
	    </xsl:call-template>
	    <xsl:text>) </xsl:text>                                
          </xsl:if>
	  <xsl:value-of select="date/@year"/>
	</xsl:when>
	<xsl:when test="@type='incollection'">
			<xsl:text>in: </xsl:text>
                        <xsl:apply-templates select="in/reference" mode="linear"/>
                        <xsl:text>, </xsl:text>
			<xsl:if test="pages">
				<xsl:text>pp</xsl:text>
				<xsl:value-of select="pages/text()"/>
			</xsl:if>
	</xsl:when>
	<xsl:when test="@type='inbook'">
			<xsl:text>in: </xsl:text>
			<xsl:if test="chapter">
				<xsl:text>Chapter </xsl:text>
				<xsl:value-of select="chapter/text()"/>
				<xsl:text> of </xsl:text>
			</xsl:if>
                        <xsl:apply-templates select="in/reference" mode="linear"/>
                        <xsl:text>, </xsl:text>
			<xsl:if test="pages">
				<xsl:text>pp</xsl:text>
				<xsl:value-of select="pages/text()"/>
			</xsl:if>
	</xsl:when>
	<xsl:when test="@type='booklet'">
	</xsl:when>
	<xsl:when test="@type='book' or @type='collection'">
                  <xsl:if test="serie">
                    <xsl:if test="@language='fr'">Collection </xsl:if><xsl:value-of select="serie/text()"/><xsl:if test="@language='en'"> series</xsl:if>
                    <xsl:if test="number"><xsl:text> </xsl:text><xsl:value-of select="number/text()"/></xsl:if>
                    <xsl:text>, </xsl:text>
                  </xsl:if>
			<xsl:value-of select="publisher/@name"/>
			<xsl:text>, </xsl:text>
			<xsl:if test="publisher/location">
				<xsl:apply-templates select="publisher/location"/><xsl:text>, </xsl:text>
			</xsl:if>

                      <xsl:if test="length and (length/@unit='p')">
                        <xsl:value-of select="length/text()"/><xsl:text>p., </xsl:text>
                      </xsl:if>
			<xsl:if test="red">
                          <xsl:text>(rev. </xsl:text>
                          <xsl:apply-templates select="red/reference" mode="linear"/><xsl:text>), </xsl:text>
			</xsl:if>
			<xsl:if test="trad">
                          <xsl:text>(trad. </xsl:text><xsl:value-of select="trad/@language"/>
                          <xsl:apply-templates select="trad/reference" mode="linear"/><xsl:text>), </xsl:text>
			</xsl:if>
			<xsl:value-of select="date/@year"/>
	</xsl:when>
	<xsl:when test="@type='misc'">
		<xsl:if test="type"><xsl:value-of select="type/text()"/><xsl:text>, </xsl:text></xsl:if>
		<xsl:if test="institution">
			<xsl:value-of select="institution/@name"/><xsl:text>, </xsl:text>
			<xsl:if test="institution/location">
				<xsl:apply-templates select="institution/location"/><xsl:text>, </xsl:text>
			</xsl:if>
		</xsl:if>
                <xsl:if test="length and (length/@unit='p')">
                  <xsl:value-of select="length/text()"/><xsl:text>p., </xsl:text>
		</xsl:if>
		<xsl:if test="date/@month">
			<xsl:call-template name="codemonth">
                        	<xsl:with-param name="month" select="date/@month"/>
                		<xsl:with-param name="language" select="@language"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:value-of select="date/@year"/>
	</xsl:when>
	<xsl:when test="@type='lecturenotes'">
		<xsl:choose>
			<xsl:when test="type"><xsl:value-of select="type/text()"/></xsl:when>
			<xsl:when test="@language='fr'">Notes de cours</xsl:when>
			<xsl:when test="@language='en'">Lecture notes</xsl:when>
		</xsl:choose>
		<xsl:text>, </xsl:text>
		<xsl:if test="school">
			<xsl:value-of select="school/@name"/><xsl:text>, </xsl:text>
			<xsl:if test="school/location">
				<xsl:apply-templates select="school/location"/><xsl:text>, </xsl:text>
			</xsl:if>
		</xsl:if>
                <xsl:if test="length and (length/@unit='p')">
                  <xsl:value-of select="length/text()"/><xsl:text>p., </xsl:text>
		</xsl:if>
		<xsl:if test="date/@month">
			<xsl:call-template name="codemonth">
                        	<xsl:with-param name="month" select="date/@month"/>
                		<xsl:with-param name="language" select="@language"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:value-of select="date/@year"/>
	</xsl:when>
	<xsl:when test="@type='motionpicture'">
		<xsl:if test="institution">
			<xsl:value-of select="institution/@name"/><xsl:text>, </xsl:text>
			<xsl:if test="institution/location">
				<xsl:apply-templates select="institution/location"/><xsl:text>, </xsl:text>
			</xsl:if>
		</xsl:if>
                <xsl:if test="length and (length/@unit='mn')">
                  <xsl:value-of select="length/text()"/><xsl:text>mn, </xsl:text>
		</xsl:if>
		<xsl:if test="date/@month">
			<xsl:call-template name="codemonth">
                        	<xsl:with-param name="month" select="date/@month"/>
                		<xsl:with-param name="language" select="@language"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:value-of select="date/@year"/>
	</xsl:when>
	<xsl:when test="@type='serialproceedings'">
		<i><xsl:value-of select="journal/@name"/></i>
		<xsl:if test="volume"><xsl:text> </xsl:text><xsl:value-of select="volume/text()"/></xsl:if>
		<xsl:if test="number">(<xsl:value-of select="number/text()"/>)</xsl:if>
		<xsl:if test="pages">:<xsl:value-of select="pages/text()"/></xsl:if>
		<xsl:text>, </xsl:text>
                <xsl:value-of select="date/@year"/>
	</xsl:when>
	</xsl:choose>
</xsl:template>

<!-- displaying people an lists of people -->

<xsl:template match="p">
	<xsl:value-of select="@title"/><xsl:text> </xsl:text>
	<xsl:value-of select="@first"/><xsl:text> </xsl:text>
	<xsl:value-of select="@last"/>
 </xsl:template>

<xsl:template match="authors" mode="linear">
  <xsl:for-each select="p">
	<xsl:apply-templates select="."/><xsl:text>, </xsl:text>
  </xsl:for-each>
</xsl:template>

<xsl:template name="listpeople">
	<xsl:apply-templates select="p[1]"/>
	<xsl:for-each select="p[position()>1]">
		<xsl:text>, </xsl:text>
		<xsl:apply-templates select="."/>
	</xsl:for-each>
</xsl:template>

<!-- this should implement the complete xxx scheme -->
<xsl:template match="editors" mode="linear">
  <xsl:param name="language"/>
  <xsl:apply-templates select="p[1]"/>
  <xsl:for-each select="p[position()>1]">
    <xsl:text>, </xsl:text>
    <xsl:apply-templates select="."/>
  </xsl:for-each>
  <xsl:choose><xsl:when test="$language='fr'"> (�d</xsl:when>
    <xsl:when test="$language='de'"> (Hrsg</xsl:when>
    <xsl:otherwise> (ed</xsl:otherwise>
  </xsl:choose>
  <xsl:if test="p[2]">s</xsl:if>
  <xsl:text>), </xsl:text>
</xsl:template>
<xsl:template match="directors" mode="linear">
  <xsl:param name="language"/>
  <xsl:call-template name="listpeople"/>
  <xsl:choose><xsl:when test="$language='fr'"> (r�alisateur</xsl:when>
    <xsl:when test="$language='de'"> (regiseur</xsl:when>
    <xsl:otherwise> (director</xsl:otherwise>
  </xsl:choose>
  <xsl:if test="p[2]">s</xsl:if>
  <xsl:text>), </xsl:text>
</xsl:template>
<xsl:template match="scenarists" mode="linear">
  <xsl:param name="language"/>
  <xsl:call-template name="listpeople"/>
  <xsl:choose><xsl:when test="$language='fr'"> (sc�nariste</xsl:when>
    <xsl:when test="$language='de'"> (scenarist</xsl:when>
    <xsl:otherwise> (scenarist</xsl:otherwise>
  </xsl:choose>
  <xsl:if test="p[2]">s</xsl:if>
  <xsl:text>), </xsl:text>
</xsl:template>
<xsl:template match="interprets" mode="linear">
  <xsl:param name="language"/>
  <xsl:call-template name="listpeople"/>
  <xsl:choose><xsl:when test="$language='fr'"> (acteur</xsl:when>
    <xsl:when test="$language='de'"> (actor</xsl:when>
    <xsl:otherwise> (actor</xsl:otherwise>
  </xsl:choose>
  <xsl:if test="p[2]">s</xsl:if>
  <xsl:text>), </xsl:text>
</xsl:template>
<xsl:template match="voices" mode="linear">
  <xsl:param name="language"/>
  <xsl:call-template name="listpeople"/>
  <xsl:choose><xsl:when test="$language='fr'"> (voix</xsl:when>
    <xsl:when test="$language='de'"> (voice</xsl:when>
    <xsl:otherwise> (voice</xsl:otherwise>
  </xsl:choose>
  <xsl:if test="p[2]">s</xsl:if>
  <xsl:text>), </xsl:text>
</xsl:template>
<!-- displaying a link -->

<xsl:template match="li">
	<xsl:value-of select="text()"/>
</xsl:template>

<!-- displaying a location -->

<xsl:template match="location">
	<xsl:if test="@place"><xsl:value-of select="@place"/><xsl:text>, </xsl:text></xsl:if>
	<xsl:if test="@city"><xsl:value-of select="@city"/></xsl:if>
	<xsl:if test="@country"><xsl:text> (</xsl:text>
		<xsl:if test="@state"><xsl:value-of select="@state"/><xsl:text> </xsl:text></xsl:if>
		<xsl:value-of select="@country"/><xsl:text>)</xsl:text>
	</xsl:if>
</xsl:template>

<!-- displaying a link -->

<xsl:template match="links">
        <font size="-1">
		<xsl:apply-templates/>
        </font>
</xsl:template>

<!-- displaying an URL -->

<xsl:template match="url">
	<xsl:element name="a">
		<xsl:attribute name="href">
			<xsl:value-of select='@href'/>
		</xsl:attribute>
		<tt><xsl:value-of select='@href'/></tt>
	</xsl:element><br />
</xsl:template>

<!-- displaying the title of a reference -->
<xsl:template name="gettitle">
  <xsl:copy-of select="title/text()|title/*" />
  <xsl:if test="(@type='proceedings' or @type='serialproceedings') and conference">
    <xsl:text> (</xsl:text><xsl:call-template name="confname">
      <xsl:with-param name="conf" select="conference"/>
    </xsl:call-template><xsl:text>)</xsl:text>
  </xsl:if>
</xsl:template>

<!-- displaying the name of a conference -->
<xsl:template name="confname">
  <xsl:param name="conf"/>
  <xsl:choose>
    <xsl:when test="@language='fr'"><xsl:text>Actes </xsl:text></xsl:when>
    <xsl:otherwise><xsl:text>Proc. </xsl:text></xsl:otherwise>
  </xsl:choose>
  <xsl:if test="$conf/@issue">
    <xsl:value-of select="$conf/@issue"/><sup>
      <xsl:call-template name="coderank">
        <xsl:with-param name="num" select="$conf/@issue"/>
        <xsl:with-param name="language" select="@language"/>
    </xsl:call-template></sup>
  </xsl:if>
  <xsl:if test="$conf/@sponsor and @language='en'">
    <xsl:value-of select="$conf/@sponsor"/>
    <xsl:text> </xsl:text>
  </xsl:if>
  <xsl:choose>
    <xsl:when test="$conf/@type"><xsl:value-of select="$conf/@type"/></xsl:when>
    <xsl:otherwise><xsl:text>conference</xsl:text></xsl:otherwise>
  </xsl:choose>
  <xsl:text> </xsl:text>
  <xsl:if test="$conf/@sponsor and @language='fr'">
    <xsl:value-of select="$conf/@sponsor"/>
  </xsl:if>
  <xsl:if test="$conf/@name">
    <xsl:choose>
      <xsl:when test="@language='fr'"><xsl:text> sur </xsl:text></xsl:when>
      <xsl:otherwise><xsl:text> on </xsl:text></xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="$conf/@name"/>
  </xsl:if>
  <xsl:if test="$conf/@short">
    <xsl:text> (</xsl:text>
    <xsl:value-of select="$conf/@short"/>
    <xsl:text>)</xsl:text>
  </xsl:if>
</xsl:template>

<!-- The full format for HTML bibliographic entries -->

<xsl:template match="reference">
	<dt>
	<br />
	<hr />
        <a name="{@index}">
  <xsl:choose>
    <xsl:when test="authors|directors|scenarists|interprets|voices">
      <xsl:apply-templates select="authors|directors|scenarists|interprets|voices" mode="linear">
        <xsl:with-param name="language" select="@language"/>
      </xsl:apply-templates>
    </xsl:when>
    <xsl:when test="editors">
      <xsl:apply-templates select="editors" mode="linear">
        <xsl:with-param name="language" select="@language"/>
      </xsl:apply-templates>
    </xsl:when>
  </xsl:choose>
        </a>
	<b><xsl:text> </xsl:text><xsl:call-template name="gettitle"/></b><xsl:text>, </xsl:text>
        <xsl:apply-templates select="." mode="line3"/>
	<xsl:if test="award">
		<xsl:text> </xsl:text><i><xsl:value-of select="award/text()"/></i>
	</xsl:if>
	<br />
	<xsl:if test="note">
		<xsl:text> </xsl:text><i><xsl:value-of select="note/text()"/></i><br />
	</xsl:if>
	<!-- supressed until Servlet work again -->
	<!--a href="BibrefServlet?format=bib&amp;file={$filename}&amp;abstrip=false&amp;ref={@index}"><font size="-1">BibTeX</font></a><br /-->
	<xsl:apply-templates select="links"/>
	</dt>
	<dd>
	<xsl:if test="abstract">
		<i><xsl:copy-of select="abstract/text()|abstract/*"/><xsl:text> </xsl:text></i><br />
	</xsl:if>
	<xsl:if test="comments">
		<small><xsl:copy-of select="comments/text()|comments/*"/></small><br />
	</xsl:if>
	<xsl:if test="keywords">
		<font size="-1">
		<xsl:apply-templates select="keywords/li[1]"/>
		<xsl:for-each select="keywords/li[position()>1]">
			<xsl:text>, </xsl:text><xsl:apply-templates select="."/>
		</xsl:for-each>
		<xsl:text> </xsl:text></font><br />
	</xsl:if>
	<xsl:if test="areas">
		<xsl:apply-templates select="areas/li[1]"/>
		<xsl:for-each select="areas/li[position()>1]">
			<xsl:text>, </xsl:text><xsl:apply-templates select="."/>
		</xsl:for-each>
	</xsl:if>
	</dd>
</xsl:template>

<!-- template for a short presentation of the entries -->

<xsl:template match="reference" mode="short">
	<p>
        <a name="{@index}">
  <xsl:choose>
    <xsl:when test="authors|directors|scenarists|interprets|voices">
      <xsl:apply-templates select="authors|directors|scenarists|interprets|voices" mode="linear">
        <xsl:with-param name="language" select="@language"/>
      </xsl:apply-templates>
    </xsl:when>
    <xsl:when test="editors">
      <xsl:apply-templates select="editors" mode="linear">
        <xsl:with-param name="language" select="@language"/>
      </xsl:apply-templates>
    </xsl:when>
  </xsl:choose>
        </a>
	<b><a
        href="http://barbara.inrialpes.fr/servlets/BibrefServlet?format=html&amp;file=bibexmo.xml&amp;abstrip=false&amp;ref={@index}"><xsl:call-template name="gettitle"/></a></b><xsl:text>, </xsl:text>
        <xsl:apply-templates select="." mode="short3"/>
        </p>
</xsl:template>

<!-- By default, everything is copyied -->

<xsl:template match="*|@*|text()">
	<xsl:copy>
		<xsl:apply-templates select="*|@*|text()"/>
	</xsl:copy>
</xsl:template>

</xsl:stylesheet>
