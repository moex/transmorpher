<?xml version="1.0" encoding="iso-8859-1"?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->
<!-- $Id: sort-ty.xsl,v 1.4 2007-03-08 11:21:48 euzenat Exp $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://exmo.inrialpes.fr/papers">

<xsl:output
  method="xml"
  version="1.0"
  encoding="iso-8859-1"
  omit-xml-declaration="no"
  standalone="no"
  doctype-system="http://exmo.inrialpes.fr/papers/xml/bib.dtd"
  indent="yes"/> 

<!-- toplevel -->

<xsl:template match="/">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="bibliography">
<bibliography name="{@name}" version="{@version}" author="{@author}" date="{@date}">
	<xsl:apply-templates select="reference">
		<xsl:sort select="areas/li[1]/text()"/>
                <xsl:sort select="date/@year"/>
	</xsl:apply-templates>
</bibliography>
</xsl:template>

<xsl:template match="*|@*|text()">
	<xsl:copy>
		<xsl:apply-templates select="*|@*|text()"/>
	</xsl:copy>
</xsl:template>

</xsl:stylesheet>
