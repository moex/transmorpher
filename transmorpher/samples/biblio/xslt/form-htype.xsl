<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->
<!-- $Id: form-htype.xsl,v 1.7 2007-03-08 11:21:48 euzenat Exp $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers">

<!-- toplevel -->

<xsl:import href="form-hh.xsl" />

<xsl:param name="format">html</xsl:param>
<xsl:param name="sort">cya</xsl:param>
<xsl:param name="layout">spare</xsl:param>
<xsl:param name="abstrip">off</xsl:param>
<xsl:param name="filename"/>

<!-- article|book|inbook|incollection article|conference|inproceedings|proceedings|phdthesis|prospective|mastersthesis|booklet|manual|techreport|misc|unpublished -->

<xsl:template match="bibliography">
<h1><xsl:value-of select="@name" /> (<xsl:value-of select="@date"/>)</h1>


<dl>
	<xsl:for-each select="reference">
		<xsl:if test="not(preceding-sibling::reference) or (preceding-sibling::reference[position()=1]/@type != ./@type)">
			<xsl:choose>
				<xsl:when test="@type='article'">
<dt><h2>Journal articles/Articles de revues</h2></dt><dd></dd>
		                </xsl:when>
		                <xsl:when test="@type='book'">
<dt><h2>Books/Monographies</h2></dt><dd></dd>
		                </xsl:when>
		                <xsl:when test="(@type='inbook' and preceding-sibling::reference[position()=1]/@type != 'incollection') or (@type='incollection' and preceding-sibling::reference[position()=1]/@type != 'inbook')">
<dt><h2>Book chapters and collected papers/Chapitres de livres</h2></dt><dd></dd>
		                </xsl:when>
                		<xsl:when test="@type='collection'">
<dt><h2>Book coordination/Coordination d'ouvrages</h2></dt><dd></dd>
		                </xsl:when>
                		<xsl:when test="@type='specialissue'">
<dt><h2>Journal special issue/�diteur invit�</h2></dt><dd></dd>
		                </xsl:when>
                		<xsl:when test="(@type='proceedings' and preceding-sibling::reference[position()=1]/@type != 'serialproceedings') or (@type='serialproceedings' and preceding-sibling::reference[position()=1]/@type != 'proceedings')">
<dt><h2>Conference editing/Publication d'actes</h2></dt><dd></dd>
		                </xsl:when>
		                <xsl:when test="@type='inproceedings'">
<dt><h2>Conference papers/Communication � des conf�rences</h2></dt><dd></dd>
		                </xsl:when>
                		<xsl:when test="@type='phd'">
<dt><h2>PhD theses/Th�ses de doctorats et habilitations</h2></dt><dd></dd>
                		</xsl:when>
               			 <xsl:when test="@type='prospective'">
<dt><h2>Prospective reports/Rapports de prospective</h2></dt><dd></dd>
                		</xsl:when>
		                <xsl:when test="@type='lecturenotes'">
<dt><h2>Lecture notes/Notes de cours</h2></dt><dd></dd>
		                </xsl:when>
		                <xsl:when test="@type='recension'">
<dt><h2>Reviews/Recensions</h2></dt><dd></dd>
		                </xsl:when>
                		<xsl:when test="@type='techreport'">
<dt><h2>Technical reports/Rapports de recherche</h2></dt><dd></dd>
                		</xsl:when>
                		<xsl:when test="@type='deliverable'">
<dt><h2>Deliverables/Rapports de contrats</h2></dt><dd></dd>
                		</xsl:when>
               			 <xsl:when test="@type='mastersthesis'">
<dt><h2>Master theses/M�moires de DEA</h2></dt><dd></dd>
                		</xsl:when>
               			 <xsl:when test="@type='bulletin'">
<dt><h2>Non reviewed articles/Articles non rapport�s</h2></dt><dd></dd>
                		</xsl:when>
		                <xsl:when test="@type='motionpicture'">
<dt><h2>Motion pictures/Oeuvre audio-visuelle</h2></dt><dd></dd>
                		</xsl:when>
		                <xsl:when test="(@type='booklet' and preceding-sibling::reference[position()=1]/@type != 'manual') or (@type='manual' and preceding-sibling::reference[position()=1]/@type != 'booklet')">
<dt><h2>Booklets and manuals/Manuels</h2></dt><dd></dd>
 		               </xsl:when>
		                <xsl:when test="(@type='misc' and preceding-sibling::reference[position()=1]/@type != 'internalreport') or (@type='internalreport' and preceding-sibling::reference[position()=1]/@type != 'misc')">
<dt><h2>Miscellaneous/Divers documents publics</h2></dt><dd></dd>
 		               </xsl:when>
                		<xsl:when test="@type='unpublished'">
<dt><h2>Unpublished/Textes non publi�s</h2></dt><dd></dd>
                		</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:apply-templates select="."/>
	</xsl:for-each>
</dl>
</xsl:template>

</xsl:stylesheet>
