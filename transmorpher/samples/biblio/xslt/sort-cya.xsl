<?xml version="1.0" encoding="iso-8859-1"?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->
<!-- $Id: sort-cya.xsl,v 1.5 2007-03-08 11:21:48 euzenat Exp $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://exmo.inrialpes.fr/papers">

<xsl:output
  method="xml"
  version="1.0"
  encoding="iso-8859-1"
  omit-xml-declaration="no"
  standalone="no"
  doctype-system="http://exmo.inrialpes.fr/papers/xml/bib.dtd"
  indent="yes"/> 

<!-- toplevel -->

<xsl:template match="/">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="bibliography">
<bibliography name="{@name}" version="{@version}" author="{@author}" date="{@date}">
	<xsl:apply-templates select="reference">
		<!-- here is the trick provided by Oliver Becker -->
		<xsl:sort select="concat(
					substring('a', 1, number(@type='article')),
					substring('b', 1, number(@type='book')),
					substring('c', 1, number(@type='inbook' or @type='incollection')),
					substring('d', 1, number(@type='collection')),
					substring('e', 1, number(@type='specialissue')),
					substring('f', 1, number(@type='proceedings' or @type='serialproceedings')),
					substring('g', 1, number(@type='inproceedings')),
					substring('h', 1, number(@type='phd')),
					substring('i', 1, number(@type='prospective')),
					substring('j', 1, number(@type='lecturenotes')),
					substring('k', 1, number(@type='recension')),
					substring('l', 1, number(@type='techreport')),
					substring('m', 1, number(@type='deliverable')),
					substring('o', 1, number(@type='mastersthesis')),
					substring('q', 1, number(@type='bulletin')),
					substring('s', 1, number(@type='motionpicture')),
					substring('u', 1, number(@type='booklet' or @type='manual')),
					substring('v', 1, number(@type='misc' or @type='internalreport')),
					substring('z', 1, number(@type='unpublished' or @type='internalreport'))
				)" />
		<!-- xsl:sort select="@type"/ -->
                <xsl:sort select="date/@year"/>
		<xsl:sort select="(authors/p[1]/@last)|(editors/p[1]/@last)|(directors/p[1]/@last)"/>
	</xsl:apply-templates>
</bibliography>
</xsl:template>

<xsl:template match="*|@*|text()">
	<xsl:copy>
		<xsl:apply-templates select="*|@*|text()"/>
	</xsl:copy>
</xsl:template>

</xsl:stylesheet>
   
