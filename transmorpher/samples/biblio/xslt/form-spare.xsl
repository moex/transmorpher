<?xml version="1.0" encoding="iso-8859-1" standalone="yes" ?>
<!-- $Id: form-spare.xsl,v 1.3 2007-03-08 11:21:48 euzenat Exp $ -->

<!-- This stylesheet defines the HTML header format for the spare bibliography generation -->

<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:bib="http://www.inrialpes.fr/exmo/papers"
 xmlns:datext="http://www.jclark.com/xt/java/java.util.Date"
 xmlns:datexa="xalan://java.util.Date">

<xsl:output
  method="xml"
  encoding="iso-8859-1"
  omit-xml-declaration="no"
  standalone="yes"
  doctype-public="-//IETF//DTD HTML//EN"
  indent="yes"/> 

<xsl:template match="/" mode="spare">
  <xsl:param name="filename" />

<html>
	<head>
		<title><xsl:value-of select="$filename"/> bibliography</title>
	</head>
	<body bgcolor="ffffff">
	  [<a href="{$filename}-year.html">HTML per year</a>]
	  [<a href="{$filename}-type.html">HTML per type</a>]
	  [<a href="{$filename}-area.html">HTML per topic</a>]
	  [<a href="{$filename}-auth.html">HTML per authors</a>]
	  [<a href="{$filename}.pdf">PDF</a>]
	  <!--[<a href="{$filename}.ps">PostScript</a>]-->
	  [<a href="{$filename}.rtf">RTF</a>]
	  [<a href="{$filename}.bib">BibTeX</a>]
	  <!--[<a href="{$filename}.rdf">RDF</a>]-->
	  [<a href="{$filename}.xml">XML</a>]
	  <hr />
	  <p />
	    <xsl:apply-templates/>
	    <hr />
	    <small>
	      <a href="../notice.html">� INRIA Rh�ne-Alpes, 1999-2001,2003, 2005-2007</a>
	    </small>
	</body>
</html>

</xsl:template>

</xsl:stylesheet>
