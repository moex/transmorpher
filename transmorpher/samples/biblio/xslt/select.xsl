<?xml version="1.0" encoding="UTF-8"?>
<!-- $Id: select.xsl,v 1.8 2007-03-08 11:21:48 euzenat Exp $ -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
>

  <xsl:param name="param"></xsl:param>
  <xsl:param name="value"></xsl:param>
  <xsl:param name="ref"></xsl:param>
	 
  <xsl:template match="bibliography">
    <bibliography>
      <xsl:attribute name="date"><xsl:value-of select="@date"/></xsl:attribute>

      <xsl:if test="$ref!=''">
	<xsl:apply-templates select="reference"/>
      </xsl:if>
      <xsl:if test="$param='ref'">
	<xsl:apply-templates select="reference"/>
      </xsl:if>
      <xsl:if test="$param='areas'">
	<xsl:apply-templates select="reference/areas">
	  <xsl:sort select="../date/@year" order="descending"/>
	</xsl:apply-templates>
      </xsl:if>
      <xsl:if test="$param='auth'">
	<xsl:apply-templates select="reference/authors">
	  <xsl:sort select="../date/@year" order="descending"/>
	</xsl:apply-templates>				
      </xsl:if>
      <xsl:if test="$param='softwares'">
	<xsl:apply-templates select="reference/softwares">
	  <xsl:sort select="../date/@year" order="descending"/>
	</xsl:apply-templates>
      </xsl:if>
      <xsl:if test="$param='contracts'">
	<xsl:apply-templates select="reference/contracts">
	  <xsl:sort select="../date/@year" order="descending"/>
	</xsl:apply-templates>
      </xsl:if>
    </bibliography>
  </xsl:template>

  <xsl:template match="text()"/>

  <!-- Selection -->
	
  <xsl:template match="bibliography/reference">
    <xsl:if test="@index=$ref">
      <xsl:copy-of select="."/>
    </xsl:if>
    <xsl:if test="@index=$value">
      <xsl:copy-of select="."/>
    </xsl:if>
  </xsl:template>
	
  <xsl:template match="bibliography/reference/areas">
    <xsl:if test="li[contains(translate(text(),
		  'abcdefghijklmnopqrstuvwxz',
		  'ABCDEFGHIJKLMNOPQRSTUVWXYZ'),
		  translate($value,
		  'abcdefghijklmnopqrstuvwxz',
		  'ABCDEFGHIJKLMNOPQRSTUVWXYZ'))]">
      <xsl:copy-of select=".."/>
    </xsl:if>
  </xsl:template>
	
  <xsl:template match="bibliography/reference/authors">
    <xsl:if test="p[translate(@last,'abcdefghijklmnopqrstuvwxz',
		  'ABCDEFGHIJKLMNOPQRSTUVWXYZ')=translate($value,
		  'abcdefghijklmnopqrstuvwxz',
		  'ABCDEFGHIJKLMNOPQRSTUVWXYZ')]">
      <xsl:copy-of select=".."/>
    </xsl:if>
  </xsl:template>
	
  <xsl:template match="bibliography/reference/softwares">
    <xsl:if test="li[contains(translate(text(),
		  'abcdefghijklmnopqrstuvwxz',
		  'ABCDEFGHIJKLMNOPQRSTUVWXYZ'),
		  translate($value,
		  'abcdefghijklmnopqrstuvwxz',
		  'ABCDEFGHIJKLMNOPQRSTUVWXYZ'))]">
      <xsl:copy-of select=".."/>
    </xsl:if>
  </xsl:template>
	
  <xsl:template match="bibliography/reference/contracts">
    <xsl:if test="li[contains(translate(text(),
		  'abcdefghijklmnopqrstuvwxz',
		  'ABCDEFGHIJKLMNOPQRSTUVWXYZ'),
		  translate($value,
		  'abcdefghijklmnopqrstuvwxz',
		  'ABCDEFGHIJKLMNOPQRSTUVWXYZ'))]">
      <xsl:copy-of select=".."/>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
