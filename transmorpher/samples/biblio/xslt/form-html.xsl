<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->
<!-- $Id: form-html.xsl,v 1.5 2007-03-08 11:21:48 euzenat Exp $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers">

<xsl:import href="form-hh.xsl" />
<xsl:include href="form-exmo.xsl" />

<xsl:output
  method="html"
  encoding="iso-8859-1"
  omit-xml-declaration="no"
  standalone="no"
  doctype-public="-//IETF//DTD HTML//EN"
  indent="yes"/> 

<xsl:param name="format">html</xsl:param>             
<xsl:param name="sort">ya</xsl:param>               
<xsl:param name="layout"/>
<xsl:param name="filename"/>

<!-- toplevel -->

<xsl:template match="bibliography">
<h1><xsl:value-of select="@name" /> selection (<xsl:value-of select="@date"/>)</h1>


<dl>
	<xsl:apply-templates select="*"/>
</dl>
</xsl:template>

</xsl:stylesheet>
