<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->
<!-- $Id: form-harea.xsl,v 1.6 2007-03-08 11:21:48 euzenat Exp $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers">

<xsl:import href="form-hh.xsl" />

<xsl:param name="format">html</xsl:param>             
<xsl:param name="sort">ty</xsl:param>               
<xsl:param name="abstrip">off</xsl:param>
<xsl:param name="layout">spare</xsl:param>
<xsl:param name="filename"/>

<!-- toplevel -->

<xsl:template match="bibliography">
<h1><xsl:value-of select="@name" /> (<xsl:value-of select="@date"/>)</h1>


<dl>
	<xsl:for-each select="reference">
		<xsl:if test="not(preceding-sibling::reference) or (preceding-sibling::reference[position()=1]/areas/li[1]/text() != ./areas/li[1]/text())">
<dt><h2><xsl:value-of select="./areas/li[1]/text()" /><xsl:text> </xsl:text></h2></dt><dd></dd>
		</xsl:if>
		<xsl:apply-templates select="."/>
	</xsl:for-each>
</dl>
</xsl:template>

</xsl:stylesheet>
