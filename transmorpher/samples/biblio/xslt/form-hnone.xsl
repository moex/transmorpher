<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->
<!-- $Id: form-hnone.xsl,v 1.2 2007-03-08 11:21:48 euzenat Exp $ -->

<!-- HTML format that does not apply any presentation -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers">

<xsl:import href="form-hh.xsl" />

<xsl:param name="format">html</xsl:param>             
<xsl:param name="sort">ya</xsl:param>               
<xsl:param name="abstrip">off</xsl:param>
<xsl:param name="layout">spare</xsl:param>
<xsl:param name="filename"/>

<!-- toplevel -->

<xsl:template match="bibliography">
<h1><xsl:value-of select="@name" /> (<xsl:value-of select="@date"/>)</h1>
    <dl>
        <xsl:for-each select="reference">
		<xsl:apply-templates select="."/>
	</xsl:for-each>
    </dl>
</xsl:template>

</xsl:stylesheet>
