<?xml version="1.0" encoding="iso-8859-1" standalone="yes" ?>

<!-- $Id: common.xsl,v 1.4 2007-03-08 11:21:47 euzenat Exp $ -->



<xsl:stylesheet version="1.0"
  xml:space="default"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://exmo.inrialpes.fr/papers"
  xmlns:date="http://www.jclark.com/xt/java/java.util.Date"
  xmlns:java="http://xml.apache.org/xslt/java">

  <!-- utilities for decoding the dates and numbers -->

  <xsl:template name="codemonth">
    <xsl:param name="month"/>
    <xsl:param name="language"/>
    <xsl:choose>
      <xsl:when test="$language='fr'">
	<xsl:choose>
	  <xsl:when test="$month='1'"><xsl:text>janvier </xsl:text></xsl:when>
	  <xsl:when test="$month='2'"><xsl:text>f�vrier </xsl:text></xsl:when>
	  <xsl:when test="$month='3'"><xsl:text>mars </xsl:text></xsl:when>
	  <xsl:when test="$month='4'"><xsl:text>avril </xsl:text></xsl:when>
	  <xsl:when test="$month='5'"><xsl:text>mai </xsl:text></xsl:when>
	  <xsl:when test="$month='6'"><xsl:text>juin </xsl:text></xsl:when>
	  <xsl:when test="$month='7'"><xsl:text>juillet </xsl:text></xsl:when>
	  <xsl:when test="$month='8'"><xsl:text>ao�t </xsl:text></xsl:when>
	  <xsl:when test="$month='9'"><xsl:text>septembre </xsl:text></xsl:when>
	  <xsl:when test="$month='10'"><xsl:text>octobre </xsl:text></xsl:when>
	  <xsl:when test="$month='11'"><xsl:text>novembre </xsl:text></xsl:when>
	  <xsl:when test="$month='12'"><xsl:text>d�cembre </xsl:text></xsl:when>
	</xsl:choose>
      </xsl:when>
  <xsl:otherwise>
	<xsl:choose>
	  <xsl:when test="$month='1'"><xsl:text>January </xsl:text></xsl:when>
	  <xsl:when test="$month='2'"><xsl:text>February </xsl:text></xsl:when>
	  <xsl:when test="$month='3'"><xsl:text>March </xsl:text></xsl:when>
	  <xsl:when test="$month='4'"><xsl:text>April </xsl:text></xsl:when>
	  <xsl:when test="$month='5'"><xsl:text>May </xsl:text></xsl:when>
	  <xsl:when test="$month='6'"><xsl:text>June </xsl:text></xsl:when>
	  <xsl:when test="$month='7'"><xsl:text>July </xsl:text></xsl:when>
	  <xsl:when test="$month='8'"><xsl:text>August </xsl:text></xsl:when>
	  <xsl:when test="$month='9'"><xsl:text>September </xsl:text></xsl:when>
	  <xsl:when test="$month='10'"><xsl:text>October </xsl:text></xsl:when>
	  <xsl:when test="$month='11'"><xsl:text>November </xsl:text></xsl:when>
	  <xsl:when test="$month='12'"><xsl:text>December </xsl:text></xsl:when>
	</xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="coderank">
    <xsl:param name="num"/>
    <xsl:param name="language"/>
    <xsl:variable name="last" select="substring($num, string-length($num), 256)"/>
    <xsl:choose>
      <xsl:when test="$language='fr'">
	<xsl:choose>
	<xsl:when test="$last='1'">er </xsl:when>
	<xsl:otherwise>e </xsl:otherwise>
	</xsl:choose>
      </xsl:when>
      <xsl:otherwise>
	<xsl:choose>
	  <xsl:when test="substring($num, string-length($num)-1, string-length($num)-1)='1'">th </xsl:when>
	  <xsl:when test="$last='1'">st </xsl:when>
	  <xsl:when test="$last='2'">nd </xsl:when>
	  <xsl:when test="$last='3'">rd </xsl:when>
	  <xsl:otherwise>th </xsl:otherwise>
	</xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>

