<?xml version="1.0" encoding="iso-8859-1"?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output
  method="xml"
  version="1.0"
  encoding="iso-8859-1"
  omit-xml-declaration="no"
  standalone="yes"
  indent="yes"/> 

<!-- toplevel -->

<xsl:param name="nombre">Theodule</xsl:param>

<xsl:template match="/">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="*|@*|text()">
		<xsl:copy>
			<xsl:apply-templates select="*|@*|text()"/>
		</xsl:copy>
</xsl:template>

<xsl:template match="h1">
	<h1>Salut <xsl:value-of select="$nombre"/></h1>
</xsl:template>

</xsl:stylesheet>
