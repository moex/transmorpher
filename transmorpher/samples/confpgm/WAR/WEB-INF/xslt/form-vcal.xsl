<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>

<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->
<!-- $Id: form-vcal.xsl,v 1.1 2002-10-07 14:36:20 triolet Exp $ -->

<xsl:stylesheet version="1.0"
  xmlns:pgm="http://co4.inrialpes.fr/xml/pimlib/confpgm"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers">

<xsl:output
  method="text"
  encoding="iso-8859-1"
  omit-xml-declaration="yes"
  standalone="yes"
  indent="yes"/> 

<!-- toplevel -->

<xsl:template match="pgm:conference">
BEGIN:VCALENDAR
PRODID: Transmorpher-vCallib
TZ: <xsl:value-of select="@tz"/>
VERSION:1.0
	<xsl:apply-templates>
		<xsl:with-param name="name" select="@name"/>
		<xsl:with-param name="loc" select="@location"/>
	</xsl:apply-templates>
END:VCALENDAR
</xsl:template>

<xsl:template match="pgm:day">
	<xsl:param name="name"/>
	<xsl:param name="loc"/>
	<xsl:apply-templates>
		<xsl:with-param name="name" select="$name"/>
		<xsl:with-param name="loc" select="$loc"/>
		<xsl:with-param name="day"><xsl:value-of select="@year"/><xsl:number value="@month" format="01"/><xsl:number value="@day" format="01"/></xsl:with-param>
	</xsl:apply-templates>
</xsl:template>

<xsl:template match="pgm:slot">
	<xsl:param name="name"/>
	<xsl:param name="loc"/>
	<xsl:param name="day"/>
	<xsl:apply-templates>
		<xsl:with-param name="name" select="$name"/>
		<xsl:with-param name="loc" select="$loc"/>
		<xsl:with-param name="beg"><xsl:value-of select="$day"/>T<xsl:number value="substring-before(@begin,':')" format="01"/><xsl:number value="substring-after(@begin,':')" format="01"/>00</xsl:with-param>
		<xsl:with-param name="end"><xsl:value-of select="$day"/>T<xsl:number value="substring-before(@end,':')" format="01"/><xsl:number value="substring-after(@end,':')" format="01"/>00</xsl:with-param>
	</xsl:apply-templates>
</xsl:template>

<!-- I did not took the chair into account -->
<xsl:template match="pgm:session">
	<xsl:param name="name"/>
	<xsl:param name="loc"/>
	<xsl:param name="beg"/>
	<xsl:param name="end"/>
BEGIN:VEVENT
SUMMARY: <xsl:value-of select="$name"/><xsl:text> <!--/xsl:text>(<xsl:value-of select="$loc"/>)<xsl:text--> </xsl:text>
<xsl:choose>
  <xsl:when test="@type='InvitedTalk'"><xsl:value-of select="pgm:talk/pgm:title/text()"/> (Invited talk) <xsl:apply-templates select="pgm:talk/pgm:presenters/pgm:p"/></xsl:when>
  <xsl:when test="@type='Tutorial'"><xsl:value-of select="pgm:talk/pgm:title/text()"/> (Tutorial) <xsl:apply-templates select="pgm:talk/pgm:presenters"/></xsl:when>
  <xsl:when test="@type='CoffeeBreak'">Coffee break</xsl:when>
  <xsl:when test="@type='Panel'"><xsl:value-of select="pgm:discussion/pgm:title/text()"/> (panel) <xsl:apply-templates select="pgm:discussion/pgm:presenters"/></xsl:when>
  <xsl:when test="@type='Lunch'">Lunch</xsl:when>
  <xsl:when test="@type='PosterSession'">Poster session</xsl:when>
  <xsl:when test="(@type='Banquet') or (@type='SocialEvent') or (@type='Ceremony')"><xsl:value-of select="@title"/></xsl:when>
  <xsl:when test="@type='TechnicalSession'"><xsl:value-of select="@title"/></xsl:when>
</xsl:choose>
DTSTART: <xsl:value-of select="$beg"/>
DTEND: <xsl:value-of select="$end"/>
LOCATION: <xsl:value-of select="$loc"/>
<xsl:choose>
  <xsl:when test="@type='TechnicalSession'">
DESCRIPTION;ENCODING=QUOTED-PRINTABLE:<xsl:apply-templates select="pgm:talk"/>
</xsl:when>
</xsl:choose>
<!-- URL:
ATTACH;VALUE=URL:
-->
END:VEVENT
</xsl:template>

<xsl:template match="pgm:talk"><xsl:text>
    </xsl:text><xsl:value-of select="pgm:title/text()"/><xsl:text>=0D=0A=
      </xsl:text><xsl:apply-templates select="pgm:presenters" mode="qp"/>
</xsl:template>

<xsl:template match="pgm:presenters" mode="qp">
	<xsl:apply-templates select="."/><xsl:text>=0D=0A=</xsl:text>
</xsl:template>

<xsl:template match="pgm:presenters">
	<xsl:apply-templates select="pgm:p[1]"/>
	<xsl:for-each select="pgm:p[position()>1]">
		<xsl:text>, </xsl:text>
		<xsl:apply-templates select="."/>
	</xsl:for-each>
</xsl:template>

<xsl:template match="pgm:p">
<xsl:value-of select="@first"/><xsl:text> </xsl:text><xsl:value-of select="@last"/>
</xsl:template>
</xsl:stylesheet>

