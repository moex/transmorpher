<?xml version="1.0" encoding="iso-8859-1" standalone="yes" ?>
<!-- $Id: form-iswc.xsl,v 1.2 2002-11-08 09:46:50 triolet Exp $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers">

<xsl:param name="title"/>
<xsl:param name="filename"/>

<xsl:template match="/">
<!-- This stylesheet defines the HTML header format for the whole ISWC site -->
<html xsl:version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers"
  xmlns:date="http://www.jclark.com/xt/java/java.util.Date"
  xmlns:java="http://xml.apache.org/xslt/java"
  result-ns=""
  indent="yes">

<head><title>International Semantic Web Conference 2002</title>
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
   <meta name="description" content="international semantic web conference homepage" />
   <meta name="keywords" content="Semantic Web, semantic web conference, international, semantics, logic, knowledge representation, ontology, formal ontology, lexicons, Sardinia, Ontoweb, DAML, RDF, XML, Information Integration, artificial intelligence, AI" />
</head>
<body bgColor="#ffffff">
<!--

    <rdf:RDF xmlns:rdf  = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"

             xmlns:swrc = "http://www.semanticweb.org/ontologies/swrc-onto-2000-09-10.daml#">



       <swrc:Workshop rdf:about="tann:2001,semanticweb.org,ISWC2002">

          <swrc:date>9-12 June, 2002</swrc:date>

          <swrc:location>Sardinia, IT</swrc:location>

          <swrc:name>International Semantic Web Conference (ISWC)</swrc:name>

       </swrc:Workshop>



   </rdf:RDF>



--> 

<br/> <!-- this one is here for fixing the display (on NS4.7)-->

<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" bgcolor="#3366FF" height="74">

  <TR bgcolor="#CCCC33"> 

    <TD align="middle" width="100%" height="68"> 

      <div align="center"><FONT 

      color="#ffffff" size="+7">International Semantic Web Conference (ISWC)</FONT></div>

    </TD></TR>

</TABLE>



<p align="center">

<a href="">

<img alt=" [ Oval picture of sardinia ] " src="sardinia.jpg" border="0"

width="80%" /></a>

</p>



<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">

  <TR>

 

    <td valign="top" width="30%"> 

      <table cellspacing="0" cellpadding="0" width="100%" border="0">

        <tr> 
          <td colspan="2"> 
            <p>
						<a href="/ProcessProgram/ProcessProgram?filename=overview.html">Overview</a><br/>
            <a href="/ProcessProgram/ProcessProgram?filename=program.html">Detailled technical program</a><br/>
            <a href="/ProcessProgram/ProcessProgram?filename=program.vcs">vCal</a><br/> 
            <a href="/ProcessProgram/ProcessProgram?filename=overview.rdf">RDF/iCal</a><br/> 
            <a href="/ProcessProgram/ProcessProgram?filename=program.daml">DAML</a><br/> 
            <a href="/ProcessProgram/ProcessProgram?filename=program.xml">XML</a><br/> 
						 <br/></p>
            </td>
        </tr>
 
        <tr bgcolor="#CCCC33"> 

          <td align="middle" colspan="2"><b><font color="#ffffff">Further Information 

            </font></b></td>

        </tr>

        <tr> 

          <td colspan="2"> 

            <p>
            <a href="index.html">ISWC 2002</a><br/>
	    <a href="http://annotation.semanticweb.org/iswc/documents.html">Annotated abstracts</a><img src="logos/new.gif" width="32" height="16" /><br/>
	    <a href="overview.html">Program</a> (updated)<br/>
	    <a href="index.html#aaai">AAAI grant</a><img src="logos/new.gif" width="32" height="16" /><br/>
	    <a href="http://www.consultaumbria.com/ISWC2002"><b>Registration</b></a><br/>
            <a href="tutorials.html">Tutorials</a><br/>
	    <a href="venue.html">Venue information</a><br/>
            <a href="sponsors.html">Sponsors</a><br/>
            <a href="cfpapers.html">Call for papers</a><br/> 
            <a href="cftutorials.html">Call for tutorials</a><br/>
	    <a href="cfposters.html">Call for late-breaking topics</a><br/>
<br/></p>
            </td>

        </tr>

        <tr bgcolor="#CCCC33">

          <td align="middle" colspan="2"> <b><font color="#ffffff">Our Sponsors</font></b> 

          </td>

        </tr>

        <tr> 

          <td>See the <A href="sponsors.html">Sponsor chairs</A>

<table>

  <tr>

    <td><img src="logos/platinum.jpg" width="30" height="100" /><br/>

        <img src="logos/platinum.jpg" width="30" height="100" /><br/>

        <img src="logos/platinum.jpg" width="30" height="100" /></td>

    <td><center>

        <a href="http://www.networkinference.com" target="_ ">

           <img src="logos/netinf.jpg"  alt=" [ Network inference ]" border="0" width="250" align="middle" />

        </a><br/>

        <a href="http://www.ontoknowledge.org" target="_ ">

           <img src="logos/logo-otk.jpg"  alt=" [ OnToKnowledge ] " border="0" width="150" align="middle" />

        </a><br/>

        <a href="http://www.swi.psy.uva.nl/projects/ibrow/home.html" target="_ ">

           <img src="logos/ibrow-sml.jpg"  alt=" [ iBrow ] " border="0" width="200" align="middle" />

          </a>

    </center></td> 

  </tr>

  <tr> 

    <td><img src="logos/gold.jpg" width="30" height="80" /><br/>

        <img src="logos/gold.jpg" width="30" height="80" /><br/>

        <img src="logos/gold.jpg" width="30" height="80" /></td>

    <td><center>

        <a href="http://www.ontoprise.de" target="_ ">

           <img src="logos/ontoprise.gif"  alt=" [ Ontoprise ] " align="middle" width="250" border="0" />

        </a><br/>

        <a href="http://www.net.intap.or.jp/INTAP/" target="_ ">

           <img src="logos/INTAP-logo.jpg"  alt=" [ INTAP ] " align="middle" width="150" border="0" />

        </a><br/>

        <a href="http://www.rd.francetelecom.fr" target="_ ">

           <img src="logos/logo_ft.gif"  alt=" [ France Telecom R&amp;D ] " align="middle" width="250" border="0" />

        </a><br/>

        <a href="http://wonderweb.semanticweb.org" target="_ ">

           <img src="logos/logo-wonder.gif"  alt=" [ WonderWeb ] " align="middle" width="150" border="0" />

        </a>

    </center></td> 

  </tr>

  <tr>

    <td> <img src="logos/silver.jpg" width="30" height="60" /><br/>

         <img src="logos/silver.jpg" width="30" height="60" /><br/>

         <img src="logos/silver.jpg" width="30" height="60" />

    </td>

    <td><center>

      <a href="http://www.cognit.com/" target="_ ">

        <img src="logos/cognit.jpg" alt=" [ CognIT ] " align="middle" width="180" border="0" />

      </a><br/>

      <a href="http://www.sandsoft.com/" target="_ ">

        <img src="logos/sandpiper-logo.jpg" alt=" [ Sandpiper ] " align="middle" width="180" border="0" />

      </a><br/>

        <a href="http://www.nokia.com" target="_ ">

           <img src="logos/nokia.jpg"  alt=" [ Nokia ] " align="middle" width="150" border="0" />

        </a><br/>

      <!--a href="http://www.sandsoft.com/" target="_ ">

        <img src="logos/sandpiper-logo-name.jpg" alt=" [ Sandpiper ] " align="middle" width="180" border="0" />

      </a-->

    </center></td>

  </tr>

</table>

</td>

        </tr>





        <tr bgcolor="#CCCC33"> 

          <td align="middle" colspan="2"><b><font color="#ffffff">Chairs</font></b></td>

        </tr>

        <tr>

	    <td colspan="2">

<br/><p><dl><dt><b>General Chair</b></dt>

<dd>

      <a href="http://www.cs.umd.edu/~hendler">James Hendler</a> <br/>

      Maryland Information and Network Dynamics Lab<br/>

      University of Maryland<br/>

      College Park, MD 20742, USA<br/>

      <a href="mailto:hendler@cs.umd.edu">hendler@cs.umd.edu</a></dd>

<dt>

<b>Program Chair</b></dt>

<dd>



    <a href="http://www.cs.man.ac.uk/~horrocks">Ian Horrocks</a><br/>

    Department of Computer Science<br/>

    University of Manchester<br/>

    Oxford Road<br/>

    Manchester M13 9PL, ENGLAND UK<br/>

    <a href="mailto:horrocks@cs.man.ac.uk">horrocks@cs.man.ac.uk</a><br/>

</dd>

<dt>

<b>Organisation chair</b> </dt>

<dd>

      <a href="http://www.iasi.rm.cnr.it/iasi/personnel/missikoff.html">Michele Missikoff</a><br/>

      Istituto di Analisi dei Sistemi ed Informatica - CNR<br/>

      Viale Manzoni, 30<br/>

      00185 Roma - Italy<br/>

      <a href="mailto:missikof@iasi.rm.cnr.it">missikof@iasi.rm.cnr.it</a> </dd>

</dl></p>

	    </td></tr>

        <tr bgcolor="#CCCC33"> 

          <td align="middle" colspan="2"><b><font color="#ffffff">Contacts</font></b></td>

        </tr>

        <tr>

	    <td colspan="2">



<br/>

	    </td></tr>

      </table>

    </td>

    <TD vAlign="top" width="1%"> </TD>

    <TD vAlign="top" width="60%"> 

      <table cellspacing="0" cellpadding="0" width="100%" border="0">

        <tr> 

          <td> 

            <center>

              <h1>Program<xsl:value-of select="$title"/><br/>(ISWC2002)</h1>

               <p><b>June 9-12th, 2002 </b> </p>

              <p><b>Sardinia, Italia</b></p>

                 

              <table width="88%" border="0" height="83">

                <tr> 

                  <td width="28%"><b><a href="http://www.ontoweb.org"><img src="logos/ontoweb1.jpg" width="108" height="106" border="0" alt="OntoWeb" /></a></b></td>

                  <td width="42%"><b>supported in part by the <a href="http://www.ontoweb.org/">OntoWeb 

                    </a>network <br/><br/><br/><br/>in cooperation with the</b></td>

                  <td width="30%"><b><a href="http://www.ontoweb.org"><img src="logos/ist.jpg" width="119" height="91" border="0" alt="IST" /></a></b></td>

                </tr>

                <tr> 

                  <td colspan="3"><center><b><a href="http://www.daml.org/">DARPA 

                    <img align="middle" src="logos/logo-daml.jpg" width="120" border="0" /></a> Program</b> </center></td>

                </tr>

              </table></center>

    </td>

  </tr>

</table>





	<xsl:apply-templates/>

</TD></TR></TABLE>

<hr />

http://iswc.semanticweb.org/<xsl:value-of select="$filename"/>

<hr />

<address>

        <!-- for XT -->

	<xsl:if test="function-available('date:to-string') and function-available('date:new')">

		Last generated: <xsl:value-of select="date:to-string(date:new())"/>

	</xsl:if>

        <!-- for Xalan -->

        <xsl:if test="function-available('java:toString') and function-available('java:java.util.Date.new')">

		Last generated: <xsl:value-of select="java:toString(java:java.util.Date.new())"/>

	</xsl:if>

	</address>

	</body>

</html>

</xsl:template>
</xsl:stylesheet>

