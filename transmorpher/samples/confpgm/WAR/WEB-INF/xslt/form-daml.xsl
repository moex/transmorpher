<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>

<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->
<!-- $Id: form-daml.xsl,v 1.1 2002-10-07 14:36:20 triolet Exp $ -->

<xsl:stylesheet version="1.0"
  xmlns:pgm="http://co4.inrialpes.fr/xml/pimlib/confpgm"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:ical="http://ilrt.org/discovery/2001/06/schemas/ical-full/hybrid.rdf#"
  xmlns:time="http://daml.umbc.edu/ontologies/calendar-ont#">

<xsl:output
  method="xml"
  encoding="utf-8"
  omit-xml-declaration="no"
  indent="yes"/> 

<!-- toplevel -->

<xsl:template match="pgm:conference">
  <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
           xmlns:daml="http://www.daml.org/2000/10/daml-ont#"
           xmlns:time="http://daml.umbc.edu/ontologies/calendar-ont#"
           xmlns="http://daml.umbc.edu/ontologies/talk-ont#">
	<xsl:apply-templates>
		<xsl:with-param name="name" select="@name"/>
		<xsl:with-param name="loc" select="@location"/>
	</xsl:apply-templates>
  </rdf:RDF>
</xsl:template>

<xsl:template match="pgm:day">
	<xsl:param name="name"/>
	<xsl:param name="loc"/>
	<xsl:apply-templates>
		<xsl:with-param name="name" select="$name"/>
		<xsl:with-param name="loc" select="$loc"/>
		<xsl:with-param name="year"><xsl:number value="@year" format="01"/></xsl:with-param>
		<xsl:with-param name="month"><xsl:number value="@month" format="01"/></xsl:with-param>
		<xsl:with-param name="day"><xsl:number value="@day" format="01"/></xsl:with-param>
	</xsl:apply-templates>
</xsl:template>

<xsl:template match="pgm:slot">
	<xsl:param name="name"/>
	<xsl:param name="loc"/>
	<xsl:param name="year"/>
	<xsl:param name="month"/>
	<xsl:param name="day"/>
	<xsl:apply-templates>
		<xsl:with-param name="name" select="$name"/>
		<xsl:with-param name="loc" select="$loc"/>
		<xsl:with-param name="year" select="$year"/>
		<xsl:with-param name="month" select="$month"/>
		<xsl:with-param name="day" select="$day"/>
		<xsl:with-param name="hbeg"><xsl:number value="substring-before(@begin,':')" format="01"/>00</xsl:with-param>
                <xsl:with-param name="mbeg"><xsl:number value="substring-after(@begin,':')" format="01"/>00</xsl:with-param>
		<xsl:with-param name="hend"><xsl:number value="substring-before(@end,':')" format="01"/>00</xsl:with-param>
                <xsl:with-param name="mend"><xsl:number value="substring-after(@end,':')" format="01"/>00</xsl:with-param>
	</xsl:apply-templates>
</xsl:template>

<xsl:template match="pgm:session">
	<xsl:param name="name"/>
	<xsl:param name="loc"/>
	<xsl:param name="year"/>
	<xsl:param name="month"/>
	<xsl:param name="day"/>
	<xsl:param name="hbeg"/>
	<xsl:param name="hend"/>
	<xsl:param name="mbeg"/>
	<xsl:param name="mend"/>
  <xsl:if test="(@type='InvitedTalk') or (@type='TechnicalSession')">
	<xsl:apply-templates select="pgm:talk">
		<xsl:with-param name="name" select="$name"/>
		<xsl:with-param name="loc" select="$loc"/>
		<xsl:with-param name="year" select="$year"/>
		<xsl:with-param name="month" select="$month"/>
		<xsl:with-param name="day" select="$day"/>
		<xsl:with-param name="hbeg" select="$hbeg"/>
		<xsl:with-param name="mbeg" select="$mbeg"/>
		<xsl:with-param name="hend" select="$hend"/>
		<xsl:with-param name="mend" select="$mend"/>
	</xsl:apply-templates>
  </xsl:if>
</xsl:template>

<xsl:template match="pgm:talk">
	<xsl:param name="name"/>
	<xsl:param name="loc"/>
	<xsl:param name="year"/>
	<xsl:param name="month"/>
	<xsl:param name="day"/>
	<xsl:param name="hbeg"/>
	<xsl:param name="hend"/>
	<xsl:param name="mbeg"/>
	<xsl:param name="mend"/>
    <!-- The about is the #nb -->
    <rdf:Description rdf:about="#{@nb}">
       <Talk rdf:parseType="Resource">
          <Title><xsl:value-of select="pgm:title/text()"/></Title>
          <!-- the daml-uri is the this-file#nb -->
          <DAML-URI>#{@nb}</DAML-URI>
          <!-- Abstract /-->
          <BeginTime rdf:parseType="Resource">
            <time:Year><xsl:value-of select="$year"/></time:Year>
            <time:Month><xsl:value-of select="$month"/></time:Month>
            <time:Day><xsl:value-of select="$day"/></time:Day>
            <time:Hour><xsl:value-of select="$hbeg"/></time:Hour>
            <time:Minute><xsl:value-of select="$mbeg"/></time:Minute>
            <time:Second>00</time:Second>
         </BeginTime>
          <BeginTime rdf:parseType="Resource">
            <time:Year><xsl:value-of select="$year"/></time:Year>
            <time:Month><xsl:value-of select="$month"/></time:Month>
            <time:Day><xsl:value-of select="$day"/></time:Day>
            <time:Hour><xsl:value-of select="$hend"/></time:Hour>
            <time:Minute><xsl:value-of select="$mend"/></time:Minute>
            <time:Second>00</time:Second>
         </BeginTime>
         <!--Topic></Topic-->
         <!--Audience-->
         <DomainName>Semantic Web</DomainName>
         <Location rdf:parseType="Resource">
            <!-- This will require redesign out locations -->
            <Building>Grand Hotel Chia Laguna</Building>
            <State>Sardinia</State>
            <City>Baia Chia</City>
            <Country>Italy</Country>
         </Location>
         <xsl:apply-templates select="pgm:presenters/pgm:p"/>
       </Talk>
    </rdf:Description>
</xsl:template>

<xsl:template match="pgm:p">
         <Speaker rdf:parseType="Resource">
            <Name><xsl:value-of select="@first"/><xsl:text> </xsl:text><xsl:value-of select="@last"/></Name>
            <xsl:if test="@company!=''">
            <Organisation><xsl:value-of select="@company"/></Organisation>
            </xsl:if>
         </Speaker>
</xsl:template>

</xsl:stylesheet>

