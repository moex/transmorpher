<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>

<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->
<!-- $Id: form-html.xsl,v 1.1 2002-10-07 14:36:20 triolet Exp $ -->

<xsl:stylesheet version="1.0"
  xmlns:pgm="http://co4.inrialpes.fr/xml/pimlib/confpgm"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers">

<xsl:include href="form-iswc.xsl" />

<xsl:output
  method="html"
  encoding="iso-8859-1"
  omit-xml-declaration="no"
  standalone="no"
  doctype-public="-//IETF//DTD HTML//EN"
  indent="yes"/> 

<!-- toplevel -->

<xsl:template match="pgm:conference">
<h1><xsl:value-of select="@name"/> Schedule</h1>
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="pgm:day">
<h2><xsl:value-of select="@day"/>/<xsl:value-of select="@month"/>/<xsl:value-of select="@year"/></h2>
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="pgm:slot">
<h3><xsl:value-of select="@begin"/>-<xsl:value-of select="@end"/></h3>
	<xsl:apply-templates/>
</xsl:template>

<xsl:template name="title-session">
  <xsl:variable name="head" select="concat(
					substring('Invited talk', 1 div (@type='InvitedTalk')),
					substring('Coffee break', 1 div (@type='CoffeeBreak')),
					substring('Panel', 1 div (@type='Panel')),
					substring('Tutorial', 1 div (@type='Tutorial')),
					substring('Poster session', 1 div (@type='PosterSession')),
					substring('Lunch', 1 div (@type='Lunch')),
					substring('Technical session', 1 div (@type='TechnicalSession')),
					substring('', 1 div ((@type='Ceremony') or (@type='Banquet') or (@type='SocialEvent')))
				)"/>
  <xsl:if test="@room!=''"><xsl:text>[</xsl:text><xsl:value-of select="@room"/><xsl:text>] </xsl:text></xsl:if>
    <xsl:choose>
      <xsl:when test="@url!=''">
        <xsl:element name="a">
          <xsl:attribute name="href">
            <xsl:value-of select="@url"/>
          </xsl:attribute>  <xsl:value-of select="$head"/>
          <xsl:choose>
           <xsl:when test="@title!=''"><xsl:if test="$head!=''"><xsl:text>: </xsl:text></xsl:if><xsl:value-of select="@title"/></xsl:when>
           <xsl:when test="pgm:talk"><xsl:if test="$head!=''"><xsl:text>: </xsl:text></xsl:if><xsl:value-of select="pgm:talk/pgm:title/text()"/></xsl:when>
          </xsl:choose>
        </xsl:element>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$head"/>
        <xsl:choose>
          <xsl:when test="@title!=''"><xsl:if test="$head!=''"><xsl:text>: </xsl:text></xsl:if><xsl:value-of select="@title"/></xsl:when>
          <xsl:when test="pgm:talk"><xsl:if test="$head!=''"><xsl:text>: </xsl:text></xsl:if><xsl:value-of select="pgm:talk/pgm:title/text()"/></xsl:when>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:if test="pgm:chair">
      (<i>Chair: <xsl:apply-templates select="pgm:chair/pgm:p"/></i>)
    </xsl:if>
</xsl:template>

<xsl:template match="pgm:session">
<h4><xsl:call-template name="title-session"/></h4>
<xsl:choose>
  <xsl:when test="@type='InvitedTalk'">
	<xsl:apply-templates select="pgm:talk/pgm:presenters"/>
  </xsl:when>
  <xsl:when test="@type='Tutorial'">
	<xsl:apply-templates select="pgm:talk/pgm:presenters"/>
  </xsl:when>
  <xsl:when test="@type='Panel'">
	<xsl:apply-templates select="pgm:discussion/pgm:presenters"/>
  </xsl:when>
  <xsl:when test="@type='TechnicalSession'">
	<dl><xsl:apply-templates select="pgm:talk"/></dl>
  </xsl:when>
</xsl:choose>
</xsl:template>

<xsl:template match="pgm:talk">
<dt><xsl:choose><xsl:when test="@url!=''"><xsl:element name="a"><xsl:attribute name="href"><xsl:value-of select="@url"/></xsl:attribute><xsl:value-of select="pgm:title/text()"/></xsl:element></xsl:when><xsl:otherwise><xsl:value-of select="pgm:title/text()"/></xsl:otherwise></xsl:choose></dt>
<dd>
	<xsl:apply-templates select="pgm:presenters"/>
</dd>
</xsl:template>

<xsl:template match="pgm:presenters">
	<xsl:apply-templates select="pgm:p[1]"/>
	<xsl:for-each select="pgm:p[position()>1]">
		<xsl:text>, </xsl:text>
		<xsl:apply-templates select="."/>
	</xsl:for-each>
</xsl:template>

<xsl:template match="pgm:p">
<xsl:choose><xsl:when test="@url!=''"><xsl:element name="a"><xsl:attribute name="href"><xsl:value-of select="@url"/></xsl:attribute><xsl:value-of select="@first"/><xsl:text> </xsl:text><xsl:value-of select="@last"/></xsl:element></xsl:when><xsl:otherwise><xsl:value-of select="@first"/><xsl:text> </xsl:text><xsl:value-of select="@last"/></xsl:otherwise></xsl:choose>

<xsl:if test="@company!=''">
<xsl:text> (</xsl:text><xsl:value-of select="@company"/><xsl:text>)</xsl:text>
</xsl:if>
</xsl:template>
</xsl:stylesheet>

