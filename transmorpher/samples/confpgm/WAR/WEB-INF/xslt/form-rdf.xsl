<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>

<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->
<!-- $Id: form-rdf.xsl,v 1.1 2002-10-07 14:36:20 triolet Exp $ -->

<xsl:stylesheet version="1.0"
  xmlns:pgm="http://co4.inrialpes.fr/xml/pimlib/confpgm"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:ical="http://ilrt.org/discovery/2001/06/schemas/ical-full/hybrid.rdf#"
  xmlns:util="http://ilrt.org/discovery/2001/06/schemas/swws/index.rdf#">
<xsl:output
  method="xml"
  encoding="utf-8"
  omit-xml-declaration="no"
  indent="yes"/> 

<!-- toplevel -->

<xsl:template match="pgm:conference">
  <rdf:RDF xmlns:cal="http://www.w3.org/2000/09/calendar/alpha1"
           xmlns:dc="http://purl.org/dc/elements/1.1/"
           xmlns:foaf="http://xmlns.com/foaf/0.1/"
           xmlns:html="http://www.w3.org/1999/xhtml"
           xmlns:ical="http://ilrt.org/discovery/2001/06/schemas/ical-full/hybrid.rdf#"
           xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
           xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
           xmlns:util="http://ilrt.org/discovery/2001/06/schemas/swws/index.rdf#"
           xmlns:xlink="http://www.w3.org/1999/xlink">
    <ical:VCALENDAR rdf:about="">
      <dc:source rdf:resource="http://www.SemanticWeb.org/SWWS/program/" />
      <dc:description><xsl:value-of select="@name"/> schedule</dc:description>
      <dc:contributor></dc:contributor>
      <dc:date>2001-07-16</dc:date>
	<xsl:apply-templates>
		<xsl:with-param name="name" select="@name"/>
		<xsl:with-param name="loc" select="@location"/>
	</xsl:apply-templates>
    </ical:VCALENDAR>
  </rdf:RDF>
</xsl:template>

<xsl:template match="pgm:day">
	<xsl:param name="name"/>
	<xsl:param name="loc"/>
	<xsl:apply-templates>
		<xsl:with-param name="name" select="$name"/>
		<xsl:with-param name="loc" select="$loc"/>
		<xsl:with-param name="day"><xsl:value-of select="@year"/><xsl:number value="@month" format="01"/><xsl:number value="@day" format="01"/></xsl:with-param>
	</xsl:apply-templates>
</xsl:template>

<xsl:template match="pgm:slot">
	<xsl:param name="name"/>
	<xsl:param name="loc"/>
	<xsl:param name="day"/>
	<xsl:apply-templates>
		<xsl:with-param name="name" select="$name"/>
		<xsl:with-param name="loc" select="$loc"/>
		<xsl:with-param name="day" select="$day"/>
		<xsl:with-param name="hbeg"><xsl:number value="substring-before(@begin,':')" format="01"/>00</xsl:with-param>
                <xsl:with-param name="mbeg"><xsl:number value="substring-after(@begin,':')" format="01"/>00</xsl:with-param>
		<xsl:with-param name="hend"><xsl:number value="substring-before(@end,':')" format="01"/>00</xsl:with-param>
                <xsl:with-param name="mend"><xsl:number value="substring-after(@end,':')" format="01"/>00</xsl:with-param>
	</xsl:apply-templates>
</xsl:template>

<xsl:template match="pgm:session">
	<xsl:param name="name"/>
	<xsl:param name="loc"/>
	<xsl:param name="day"/>
	<xsl:param name="hbeg"/>
	<xsl:param name="hend"/>
	<xsl:param name="mbeg"/>
	<xsl:param name="mend"/>
      <ical:VEVENT-PROP>
        <ical:VEVENT>
          <ical:DTSTART>
            <ical:DATE-TIME>
              <ical:TZID rdf:resource="#PDT" />
              <rdf:value><xsl:value-of select="$day"/>T<xsl:value-of select="$hbeg"/><xsl:value-of select="$mbeg"/></rdf:value>

              <util:hour><xsl:value-of select="$hbeg"/></util:hour>
              <util:minute><xsl:value-of select="$mbeg"/></util:minute>
            </ical:DATE-TIME>
          </ical:DTSTART>
          <ical:DTEND>
            <ical:DATE-TIME>
              <ical:TZID rdf:resource="#PDT" />
              <rdf:value><xsl:value-of select="$day"/>T<xsl:value-of select="$hend"/><xsl:value-of select="$mend"/></rdf:value>
              <util:hour><xsl:value-of select="$hend"/></util:hour>
              <util:minute><xsl:value-of select="$mend"/></util:minute>
            </ical:DATE-TIME>
          </ical:DTEND>
        <ical:DESCRIPTION><xsl:choose>
  <xsl:when test="@type='InvitedTalk'"><xsl:value-of select="pgm:talk/pgm:title/text()"/> (Invited talk)</xsl:when>
  <xsl:when test="@type='CoffeeBreak'">Coffee break</xsl:when>
  <xsl:when test="@type='Panel'"><xsl:value-of select="pgm:discussion/pgm:title/text()"/> (panel)</xsl:when>
  <xsl:when test="@type='Tutorial'">Tutorial: <xsl:value-of select="pgm:talk/pgm:title/text()"/></xsl:when>
  <xsl:when test="@type='PosterSession'">Poster session</xsl:when>
  <xsl:when test="@type='Lunch'">Lunch</xsl:when>
  <xsl:when test="(@type='Banquet') or (@type='SocialEvent') or (@type='Ceremony')"><xsl:value-of select="@title"/></xsl:when>
  <xsl:when test="@type='TechnicalSession'"><xsl:value-of select="@title"/></xsl:when>
  <xsl:otherwise>-</xsl:otherwise>
</xsl:choose></ical:DESCRIPTION>
        <ical:LOCATION>
          <ical:GEO>
            <ical:GEO-NAME>
              <xsl:element name="ical:TEXT">
                <xsl:attribute name="rdf:value"><xsl:value-of select="$loc"/></xsl:attribute>
              </xsl:element>
           </ical:GEO-NAME>
          </ical:GEO>
        </ical:LOCATION>
      </ical:VEVENT>
   </ical:VEVENT-PROP>
</xsl:template>

</xsl:stylesheet>

