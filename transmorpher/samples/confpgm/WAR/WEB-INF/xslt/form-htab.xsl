<?xml version="1.0" encoding="iso-8859-1" standalone="yes" ?>

<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->
<!-- $Id: form-htab.xsl,v 1.2 2002-11-08 09:46:50 triolet Exp $ 
This stylesheet convert a conference program document into a table formated
program
-->

<xsl:stylesheet version="1.0"
  xmlns:pgm="http://co4.inrialpes.fr/xml/pimlib/confpgm"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers">

<xsl:include href="form-iswc.xsl" />

<xsl:output
  method="html"
  encoding="iso-8859-1"
  omit-xml-declaration="no"
  standalone="no"
  doctype-public="-//IETF//DTD HTML//EN"
  indent="yes"/> 

<!-- toplevel -->

<xsl:template match="pgm:conference">
<h1><xsl:value-of select="@name"/> overview</h1>
<small>Detailled technical program:
<a href="/ProcessProgram/ProcessProgram?filename=program.html">HTML</a> !
<a href="/ProcessProgram/ProcessProgram?filename=program.vcs">vCal</a> !
<!--
<a href="program.daml">DAML+OIL</a> !
-->
<a href="/ProcessProgram/ProcessProgram?filename=overview.rdf">RDF/iCal</a> !
<a href="/ProcessProgram/ProcessProgram?filename=program.daml">DAML</a> !
<a href="/ProcessProgram/ProcessProgram?filename=program.xml">XML</a></small><br />

<table width="100%" border="1">
	<xsl:apply-templates/>
</table>

<small><a href="/ProcessProgram/ProcessProgram?filename=program.html">technical program in HTML</a><br/>
<a href="/ProcessProgram/ProcessProgram?filename=program.vcs">vCal format (for Palm, Calendar, PocketPC)</a><br />
<!--
<a href="program.daml">DAML+OIL format</a><br />
-->
<a href="/ProcessProgram/ProcessProgram?filename=overview.rdf">RDF/iCal format</a><br />
<a href="/ProcessProgram/ProcessProgram?filename=program.daml">DAML</a><br />
<a href="/ProcessProgram/ProcessProgram?filename=program.xml">raw XML/Confpgm format (source)</a></small><br />
</xsl:template>

<xsl:template match="pgm:day">
<tr bgcolor="#ddbf00" align="center">
<td colspan="2">
<h2 align="center"><xsl:value-of select="@day"/>/<xsl:value-of select="@month"/>/<xsl:value-of select="@year"/></h2>
</td>
</tr>
	<xsl:apply-templates/>
</xsl:template>


<xsl:template match="pgm:slot">
<tr>
<td width="16%" align="center" valign="center">
<h3><xsl:value-of select="@begin"/>-<xsl:value-of select="@end"/></h3>
</td>
<td>
<table width="100%" border="1" frame="void"><tr>
	<xsl:apply-templates/>
</tr></table>
</td>
</tr>
</xsl:template>

<xsl:template match="pgm:session">
<td colspan="100%">
<h3>
<xsl:choose>
  <xsl:when test="@type='InvitedTalk'"><xsl:value-of select="pgm:talk/pgm:title/text()"/> (Invited talk)</xsl:when>
  <xsl:when test="@type='CoffeeBreak'">Coffee break</xsl:when>
  <xsl:when test="@type='Panel'"><xsl:value-of select="pgm:discussion/pgm:title/text()"/> (panel)</xsl:when>
  <xsl:when test="@type='Lunch'">Lunch</xsl:when>
  <xsl:when test="@type='Tutorial'">Tutorial: <xsl:value-of select="pgm:talk/pgm:title/text()"/></xsl:when>
  <xsl:when test="@type='PosterSession'">Poster session</xsl:when>
  <xsl:when test="@type='TechnicalSession'"><xsl:value-of select="@title"/></xsl:when>
  <xsl:when test="(@type='Ceremony') or (@type='Banquet') or (@type='SocialEvent')"><xsl:value-of select="@title"/></xsl:when>
  <xsl:otherwise>-</xsl:otherwise>
</xsl:choose>
</h3>
<xsl:choose>
  <xsl:when test="@type='InvitedTalk'">
	<xsl:apply-templates select="pgm:talk/pgm:presenters"/>
  </xsl:when>
  <xsl:when test="@type='Panel'">
	<xsl:apply-templates select="pgm:discussion/pgm:presenters"/>
  </xsl:when>
</xsl:choose>
</td>
</xsl:template>

<xsl:template match="pgm:talk">
<dt><xsl:value-of select="pgm:title/text()"/></dt>
<dd>
	<xsl:apply-templates select="pgm:presenters"/>
</dd>
</xsl:template>

<xsl:template match="pgm:presenters">
	<xsl:apply-templates select="pgm:p[1]"/>
	<xsl:for-each select="pgm:p[position()>1]">
		<xsl:text>, </xsl:text>
		<xsl:apply-templates select="."/>
	</xsl:for-each>
</xsl:template>

<xsl:template match="pgm:p">
<xsl:value-of select="@first"/><xsl:text> </xsl:text><xsl:value-of select="@last"/>
<xsl:if test="@company!=''">
<xsl:text> (</xsl:text><xsl:value-of select="@company"/><xsl:text>)</xsl:text>
</xsl:if>
</xsl:template>

</xsl:stylesheet>

