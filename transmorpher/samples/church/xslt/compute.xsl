<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:output
  method="xml"
  version="1.0"
  encoding="iso-8859-1"
  omit-xml-declaration="no"
  standalone="no"
  doctype-system="church.dtd"
  indent="yes"/> 

  <xsl:template match="PLUS">
    <xsl:choose>
      <!-- x+0 = x -->
      <xsl:when test="*[position()=2 and self::Z]">
        <xsl:apply-templates select="*[1]"/>
      </xsl:when>
      <!-- x+(y+1) = (x+1)+y -->
      <xsl:when test="*[position()=2 and self::S]">
        <PLUS>
          <S><xsl:apply-templates select="*[1]"/></S>
          <xsl:apply-templates select="*[2]/*"/>
        </PLUS>
      </xsl:when>
      <xsl:otherwise>
        <PLUS>
          <xsl:apply-templates select="*[1]"/>
          <xsl:apply-templates select="*[2]"/>
        </PLUS>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="MULT">
    <xsl:choose>
      <!-- x*0 = 0 -->
      <xsl:when test="*[position()=2 and self::Z]">
        <Z/>
      </xsl:when>
      <!-- x*(y+1) = (x*y)+x -->
      <xsl:when test="*[position()=2 and self::S]">
        <PLUS>
          <MULT>
            <xsl:apply-templates select="*[1]"/>
            <xsl:apply-templates select="*[2]/*"/>
          </MULT>
          <xsl:apply-templates select="*[1]"/>
        </PLUS>
      </xsl:when>
      <xsl:otherwise>
        <MULT>
          <xsl:apply-templates select="*[1]"/>
          <xsl:apply-templates select="*[2]"/>
        </MULT>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

<xsl:template match="CHURCH|S|Z">
	<xsl:copy>
		<xsl:apply-templates/>
	</xsl:copy>
</xsl:template>


</xsl:stylesheet>
