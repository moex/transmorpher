@echo off
@set CLASSPATHOLD=%CLASSPATH%
@set PATHOLD=%PATH%

@set CLASSPATH=.;..\..\Classes;..\..\Resources
@set CLASSPATH=..\..\Lib\xalan.jar;%CLASSPATH%
@set CLASSPATH=..\..\Lib\xerces.jar;%CLASSPATH%
@set CLASSPATH=..\..\Lib\sax2.jar;%CLASSPATH%
@set CLASSPATH=..\..\Lib\bsf.jar;%CLASSPATH%
@set CLASSPATH=..\..\Lib\gnu-regexp.jar;%CLASSPATH%
echo %CLASSPATH%

rem java -mx196m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE xalan
java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan


set CLASSPATH=%CLASSPATHOLD%
set PATH=%PATHOLD%
