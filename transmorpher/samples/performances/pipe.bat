@echo off
@set CLASSPATHOLD=%CLASSPATH%
@set PATHOLD=%PATH%

@set CLASSPATH=.;..\..\Classes;..\..\Resources
@set CLASSPATH=..\..\Lib\xalan.jar;%CLASSPATH%
@set CLASSPATH=..\..\Lib\xerces.jar;%CLASSPATH%
@set CLASSPATH=..\..\Lib\sax2.jar;%CLASSPATH%
@set CLASSPATH=..\..\Lib\bsf.jar;%CLASSPATH%
@set CLASSPATH=..\..\Lib\gnu-regexp.jar;%CLASSPATH%

java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe


set CLASSPATH=%CLASSPATHOLD%
set PATH=%PATHOLD%
