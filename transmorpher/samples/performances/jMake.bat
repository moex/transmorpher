
@echo off 
@set CLASSPATHOLD=%CLASSPATH%
@set PATHOLD=%PATH%

@set CLASSPATH=..\Src;..\..\Classes;%CLASSPATH%

@set CLASSPATH=.;..\..\Classes;
@set CLASSPATH=..\..\Lib\xalan.jar;%CLASSPATH%
@set CLASSPATH=..\..\Lib\xerces.jar;%CLASSPATH%
@set CLASSPATH=..\..\Lib\bsf.jar;%CLASSPATH%
@set CLASSPATH=..\..\Lib\gnu-regexp.jar;%CLASSPATH%
echo %CLASSPATH%

javac -classpath %CLASSPATH% -O *.java
set CLASSPATH=%CLASSPATHOLD%
set PATH=%PATHOLD%
