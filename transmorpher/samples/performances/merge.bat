@echo off
@set CLASSPATHOLD=%CLASSPATH%
@set PATHOLD=%PATH%

@set CLASSPATH=.;..\..\Classes;..\..\Resources
@set CLASSPATH=..\..\Lib\xalan.jar;%CLASSPATH%
@set CLASSPATH=..\..\Lib\xerces.jar;%CLASSPATH%
@set CLASSPATH=..\..\Lib\sax2.jar;%CLASSPATH%
@set CLASSPATH=..\..\Lib\bsf.jar;%CLASSPATH%
@set CLASSPATH=..\..\Lib\gnu-regexp.jar;%CLASSPATH%
echo %CLASSPATH%

echo ************************************************
echo TEST PIPE 01
@copy .\input\bibexmo_01.xml .\input\current_01.xml
echo ------------------------------------------------
time
java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE org.apache.xalan.xslt.Process -IN toMerge.xml -XSL .\xslt\merge.xsl -OUT foo.xml
time
echo ************************************************
echo TEST PIPE 02
@copy .\input\bibexmo_02.xml .\input\current_01.xml
echo ------------------------------------------------
time
java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE org.apache.xalan.xslt.Process -IN toMerge.xml -XSL .\xslt\merge.xsl -OUT foo.xml
time
echo ************************************************
echo TEST PIPE 03
@copy .\input\bibexmo_03.xml .\input\current_01.xml
echo ------------------------------------------------
time
java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE org.apache.xalan.xslt.Process -IN toMerge.xml -XSL .\xslt\merge.xsl -OUT foo.xml
time
echo ************************************************
echo TEST PIPE 04
@copy .\input\bibexmo_04.xml .\input\current_01.xml
echo ------------------------------------------------
time
java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE org.apache.xalan.xslt.Process -IN toMerge.xml -XSL .\xslt\merge.xsl -OUT foo.xml
time
echo ************************************************
echo TEST PIPE 05
@copy .\input\bibexmo_05.xml .\input\current_01.xml
echo ------------------------------------------------
time
java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE org.apache.xalan.xslt.Process -IN toMerge.xml -XSL .\xslt\merge.xsl -OUT foo.xml
time
echo ************************************************
echo TEST PIPE 06
@copy .\input\bibexmo_06.xml .\input\current_01.xml
echo ------------------------------------------------
time
java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE org.apache.xalan.xslt.Process -IN toMerge.xml -XSL .\xslt\merge.xsl -OUT foo.xml
time
echo ************************************************
echo TEST PIPE 07
@copy .\input\bibexmo_07.xml .\input\current_01.xml
echo ------------------------------------------------
time
java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE org.apache.xalan.xslt.Process -IN toMerge.xml -XSL .\xslt\merge.xsl -OUT foo.xml
time
echo ************************************************
echo TEST PIPE 08
@copy .\input\bibexmo_08.xml .\input\current_01.xml
echo ------------------------------------------------
time
java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE org.apache.xalan.xslt.Process -IN toMerge.xml -XSL .\xslt\merge.xsl -OUT foo.xml
time
echo ************************************************
echo TEST PIPE 09
@copy .\input\bibexmo_09.xml .\input\current_01.xml
echo ------------------------------------------------
time
java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE org.apache.xalan.xslt.Process -IN toMerge.xml -XSL .\xslt\merge.xsl -OUT foo.xml
time
echo ************************************************
echo TEST PIPE 10
@copy .\input\bibexmo_10.xml .\input\current_01.xml
echo ------------------------------------------------
time
java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE org.apache.xalan.xslt.Process -IN toMerge.xml -XSL .\xslt\merge.xsl -OUT foo.xml
time

set CLASSPATH=%CLASSPATHOLD%
set PATH=%PATHOLD%
