@set CLASSPATH=..\..\Classes;..\..\Resources;.
@set CLASSPATH=..\..\Lib\xalan.jar;%CLASSPATH%
@set CLASSPATH=..\..\Lib\xerces.jar;%CLASSPATH%
@set CLASSPATH=..\..\Lib\sax2.jar;%CLASSPATH%
@set CLASSPATH=..\..\Lib\bsf.jar;%CLASSPATH%
@set CLASSPATH=..\..\Lib\gnu-regexp.jar;%CLASSPATH%


echo off
echo ************************************************
echo Performance TEST Pipeline
echo ************************************************

echo ************************************************
echo TEST PIPE 01
@copy .\input\bibexmo_01.xml .\input\current_01.xml
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
echo ************************************************
echo TEST PIPE 02
@copy .\input\bibexmo_02.xml .\input\current_01.xml
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
echo ************************************************
echo TEST PIPE 03
@copy .\input\bibexmo_03.xml .\input\current_01.xml
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
echo ************************************************
echo TEST PIPE 04
@copy .\input\bibexmo_04.xml .\input\current_01.xml
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
echo ************************************************
echo TEST PIPE 05
@copy .\input\bibexmo_05.xml .\input\current_01.xml
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
echo ************************************************
echo TEST PIPE 06
@copy .\input\bibexmo_06.xml .\input\current_01.xml
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
echo ************************************************
echo TEST PIPE 07
@copy .\input\bibexmo_07.xml .\input\current_01.xml
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
echo ************************************************
echo TEST PIPE 08
@copy .\input\bibexmo_08.xml .\input\current_01.xml
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
rem @java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
echo ************************************************
echo TEST PIPE 09
@copy .\input\bibexmo_09.xml .\input\current_01.xml
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
echo ************************************************
echo TEST PIPE 10
@copy .\input\bibexmo_10.xml .\input\current_01.xml
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE Pipe

echo ************************************************
echo TEST XALAN 01
@copy .\input\bibexmo_01.xml .\input\current_01.xml
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
echo ************************************************
echo TEST XALAN 02
@copy .\input\bibexmo_02.xml .\input\current_01.xml
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
echo ************************************************
echo TEST XALAN 03
@copy .\input\bibexmo_03.xml .\input\current_01.xml
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
echo ************************************************
echo TEST XALAN 04
@copy .\input\bibexmo_04.xml .\input\current_01.xml
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
echo ************************************************
echo TEST XALAN 05
@copy .\input\bibexmo_05.xml .\input\current_01.xml
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
echo ************************************************
echo TEST XALAN 06
@copy .\input\bibexmo_06.xml .\input\current_01.xml
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
echo ************************************************
echo TEST XALAN 07
@copy .\input\bibexmo_07.xml .\input\current_01.xml
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
echo ************************************************
echo TEST XALAN 08
@copy .\input\bibexmo_08.xml .\input\current_01.xml
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
echo ************************************************
echo TEST XALAN 09
@copy .\input\bibexmo_09.xml .\input\current_01.xml
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
echo ************************************************
echo TEST XALAN 10
@copy .\input\bibexmo_10.xml .\input\current_01.xml
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan
@java -mx196m  -classpath %CLASSPATH% -Djava.compiler=NONE xalan


echo ************************************************
echo TEST transmorph 01
@copy .\input\bibexmo_01.xml .\input\current_01.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------

echo ************************************************
echo TEST transmorph 02
@copy .\input\bibexmo_02.xml .\input\current_01.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------

echo ************************************************
echo TEST transmorph 03
@copy .\input\bibexmo_03.xml .\input\current_01.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------

echo ************************************************
echo TEST transmorph 04
@copy .\input\bibexmo_04.xml .\input\current_01.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------

echo ************************************************
echo TEST transmorph 05
@copy .\input\bibexmo_05.xml .\input\current_01.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------

echo ************************************************
echo TEST transmorph 06
@copy .\input\bibexmo_06.xml .\input\current_01.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------

echo ************************************************
echo TEST transmorph 07
@copy .\input\bibexmo_07.xml .\input\current_01.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------

echo ************************************************
echo TEST transmorph 08
@copy .\input\bibexmo_08.xml .\input\current_01.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------

echo ************************************************
echo TEST transmorph 09
@copy .\input\bibexmo_09.xml .\input\current_01.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------

echo ************************************************
echo TEST transmorph 10
@copy .\input\bibexmo_10.xml .\input\current_01.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -Xbootclasspath/p:..\..\Classes;%CLASSPATH% -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
@java -mx128m  -classpath %CLASSPATH% -Djava.compiler=NONE fr.fluxmedia.transmorpher.Application.transmorph ./process.xml
echo ---------------
