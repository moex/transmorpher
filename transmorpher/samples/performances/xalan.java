
// Imported TraX classes
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.dom.DOMResult;

// Imported SAX classes
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.Parser;
import org.xml.sax.helpers.ParserAdapter;
import org.xml.sax.helpers.XMLReaderFactory;
import org.xml.sax.XMLReader;
import org.xml.sax.ContentHandler;
import org.xml.sax.ext.LexicalHandler;

// Imported DOM classes
import org.w3c.dom.Node;

// Imported Serializer classes
import org.apache.xalan.serialize.Serializer;
import org.apache.xalan.serialize.SerializerFactory;
import org.apache.xalan.templates.OutputProperties;

// Imported JAVA API for XML Parsing 1.0 classes
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

// Imported java.io classes
import java.io.InputStream;
import java.io.Reader;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.File;


import org.apache.xalan.transformer.TransformerImpl;

  /**
   * This example shows how to chain a series of transformations by
   * piping SAX events from one Transformer to another. Each Transformer
   * operates as a SAX2 XMLFilter/XMLReader.
   */
public class xalan
{
	public static void main(String[] args)
	throws TransformerException, TransformerConfigurationException,
         SAXException, IOException
	{
    // Instantiate  a TransformerFactory.
  	TransformerFactory tFactory = TransformerFactory.newInstance();
    // Determine whether the TransformerFactory supports The use uf SAXSource
    // and SAXResult
    // Cast the TransformerFactory to SAXTransformerFactory.
    SAXTransformerFactory saxTFactory = ((SAXTransformerFactory) tFactory);

    // ******************************************************
    // Variables for every body
    // ******************************************************
    double begin = System.currentTimeMillis();
    double end = 0;
    TransformerHandler tHandler1 = null ;
    TransformerHandler tHandler2 = null ;
    TransformerHandler tHandler3 = null ;
    XMLReader reader = null ;
    reader = XMLReaderFactory.createXMLReader();
    Serializer serializer = null;

    // ******************************************************
    // first output file
    // ******************************************************

       
    // Create an XMLReader.
    //reader = XMLReaderFactory.createXMLReader();
    //reader.setProperty("http://xml.org/sax/properties/lexical-handler", tHandler1);


    serializer = SerializerFactory.getSerializer
                                   (OutputProperties.getDefaultMethodProperties("xml"));

    //start
    tHandler1 = saxTFactory.newTransformerHandler(new StreamSource("./xslt/stripAbstract.xsl"));
    reader.setProperty("http://xml.org/sax/properties/lexical-handler", tHandler1);
    reader.setContentHandler(tHandler1);
    serializer.setOutputStream(new FileOutputStream(new File("./output/tmp1.xml")));
    tHandler1.setResult(new SAXResult(serializer.asContentHandler()));
    
    reader.parse("./input/current_01.xml");
    tHandler1 = saxTFactory.newTransformerHandler(new StreamSource("../../samples/biblio/xslt/sort-ty.xsl"));
    reader.setProperty("http://xml.org/sax/properties/lexical-handler", tHandler1);
    reader.setContentHandler(tHandler1);
    serializer.setOutputStream(new FileOutputStream(new File("./output/tmp2.xml")));
    tHandler1.setResult(new SAXResult(serializer.asContentHandler()));
    reader.parse("./output/tmp1.xml");
    
    tHandler1 = saxTFactory.newTransformerHandler(new StreamSource("../../samples/biblio/xslt/form-harea.xsl"));
    reader.setProperty("http://xml.org/sax/properties/lexical-handler", tHandler1);
    reader.setContentHandler(tHandler1);
    serializer.setOutputStream(new FileOutputStream(new File("./output/biblio.html")));
    tHandler1.setResult(new SAXResult(serializer.asContentHandler()));
    reader.parse("./output/tmp2.xml");

    //end = System.currentTimeMillis();
    //System.out.println("1-time = " + (end - begin));


    // ******************************************************
    // second output file
    // ******************************************************

    // Create an XMLReader.
    //reader = XMLReaderFactory.createXMLReader();
    reader.setContentHandler(tHandler1);

    //start
    tHandler1 = saxTFactory.newTransformerHandler(new StreamSource("./xslt/stripAbstract.xsl"));
    serializer.setOutputStream(new FileOutputStream(new File("./output/tmp1.xml")));
    reader.setProperty("http://xml.org/sax/properties/lexical-handler", tHandler1);
    tHandler1.setResult(new SAXResult(serializer.asContentHandler()));
    reader.setContentHandler(tHandler1);
    reader.parse("./input/current_01.xml");
    
    tHandler1 = saxTFactory.newTransformerHandler(new StreamSource("../../samples/biblio/xslt/sort-cya.xsl"));
    reader.setProperty("http://xml.org/sax/properties/lexical-handler", tHandler1);
    reader.setContentHandler(tHandler1);
    serializer.setOutputStream(new FileOutputStream(new File("./output/tmp2.xml")));
    tHandler1.setResult(new SAXResult(serializer.asContentHandler()));
    reader.parse("./output/tmp1.xml");
    
    tHandler1 = saxTFactory.newTransformerHandler(new StreamSource("../../samples/biblio/xslt/form-bibtex.xsl"));
    reader.setProperty("http://xml.org/sax/properties/lexical-handler", tHandler1);
    reader.setContentHandler(tHandler1);
    serializer = SerializerFactory.getSerializer
                                   (OutputProperties.getDefaultMethodProperties("text"));
    serializer.setOutputStream(new FileOutputStream(new File("./output/biblio.bib")));
    tHandler1.setResult(new SAXResult(serializer.asContentHandler()));
    reader.parse("./output/tmp2.xml");

    //end = System.currentTimeMillis();
    //System.out.println("2-time = " + (end - begin));

    // ******************************************************
    // third output file
    // ******************************************************

    // Create a TransformerHandler for each stylesheet.
    tHandler1 = saxTFactory.newTransformerHandler(new StreamSource("../../samples/biblio/xslt/xmlverbatimwrapper.xsl"));

    // Create an XMLReader.
    //reader = XMLReaderFactory.createXMLReader();
    reader.setContentHandler(tHandler1);
    reader.setProperty("http://xml.org/sax/properties/lexical-handler", tHandler1);

    serializer = SerializerFactory.getSerializer
                                   (OutputProperties.getDefaultMethodProperties("xml"));
    serializer.setOutputStream(new FileOutputStream(new File("./output/biblio-xml.html")));
    tHandler1.setResult(new SAXResult(serializer.asContentHandler()));


    reader.parse("./input/current_01.xml");
    //end = System.currentTimeMillis();
    //System.out.println("3-time = " + (end - begin));

    // ******************************************************
    // fourth output file
    // ******************************************************

    // Create a TransformerHandler for each stylesheet.
    tHandler1 = saxTFactory.newTransformerHandler(new StreamSource("./xslt/troncybrunet.xsl"));

    // Create an XMLReader.
    //reader = XMLReaderFactory.createXMLReader();
    reader.setContentHandler(tHandler1);
    reader.setProperty("http://xml.org/sax/properties/lexical-handler", tHandler1);


    serializer = SerializerFactory.getSerializer
                                   (OutputProperties.getDefaultMethodProperties("xml"));
    serializer.setOutputStream(new FileOutputStream(new File("./output/biblio_tb.html")));
    tHandler1.setResult(new SAXResult(serializer.asContentHandler()));
    reader.parse("./input/current_01.xml");

    end = System.currentTimeMillis();
    System.out.println("time = " + (end - begin));
  }
}

