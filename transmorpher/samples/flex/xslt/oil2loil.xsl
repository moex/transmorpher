<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:dl="http://co4.inrialpes.fr/xml/dlml/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:rdfs="http://www.w3.org"
  xmlns:oil="http://www.ontoknowledge.org">

<xsl:output
  method="xml"
  version="1.0"
  encoding="UTF-8"
  omit-xml-declaration="no"
  standalone="no"
  doctype-system="http://co4.inrialpes.fr/xml/dlml/logic/dtd/aluni.dtd"
  indent="yes"/> 

<!-- toplevel -->
<xsl:template match="/">
<xsl:text disable-output-escaping="yes">

</xsl:text>
	<xsl:apply-templates/>
</xsl:template>

<!-- toplevel -->

<xsl:template match="oil:ontology">
	<dl:TERMINOLOGY logic="http://co4.inrialpes.fr/xml/dlml/logic/dtd/aluni.dtd">
		<xsl:apply-templates select="oil:DefinedSlot|oil:DefinedClass"/>
	</dl:TERMINOLOGY>
</xsl:template>

<!-- definers -->

<xsl:template match="oil:DefinedClass">
  <!-- for dealing with all OIL, I should add a test on the @type which could be defined -->
	<dl:CPRIM><dl:CATOM><xsl:value-of select="@rdf:ID"/></dl:CATOM>
        <dl:AND>
		<xsl:apply-templates select="rdfs:subClassOf|oil:hasPropertyRestriction"/>
              </dl:AND>
	</dl:CPRIM>
</xsl:template>

<xsl:template match="oil:DefinedSlot">
	<dl:RDEF><dl:RATOM><xsl:value-of select="@rdf:ID"/></dl:RATOM>
        <xsl:apply-templates select="oil:hasDomain" />
	</dl:RDEF>
</xsl:template>

<xsl:template match="oil:hasDomain">
  <dl:DOMAIN><xsl:apply-templates select="*"/></dl:DOMAIN>
</xsl:template>

<!-- intermediate tag -->

<xsl:template match="oil:And">
	<dl:AND>
          <xsl:apply-templates select="oil:hasOperand/*"/>
	</dl:AND>
</xsl:template>

<xsl:template match="oil:hasPropertyRestriction">
	<xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="oil:HasValue">
	<dl:ALL>
          <xsl:apply-templates select="*[1]"/>
          <xsl:apply-templates select="*[2]"/>
	</dl:ALL>
</xsl:template>

<xsl:template match="oil:onProperty">
  <dl:RATOM><xsl:value-of select="substring-after(@rdf:resource,'#')"/></dl:RATOM>
</xsl:template>

<!-- do not know how it is used -->
<xsl:template match="oil:toClass">
	<xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="rdfs:Class">
  <dl:CATOM><xsl:value-of select="substring-after(@rdf:about,'#')"/></dl:CATOM>
</xsl:template>

</xsl:stylesheet>
