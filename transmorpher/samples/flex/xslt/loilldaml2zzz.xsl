<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:dl="http://co4.inrialpes.fr/xml/dlml/">

<xsl:output
  method="xml"
  version="1.0"
  encoding="UTF-8"
  omit-xml-declaration="no"
  standalone="no"
  doctype-system="http://co4.inrialpes.fr/xml/dlml/logic/dtd/aluni.dtd"
  indent="yes"/> 

<!-- toplevel -->
<xsl:template match="/">
<xsl:text disable-output-escaping="yes">

</xsl:text>
<!-- The Ontology element does not enclose the classes and properties -->
<dl:TERMINOLOGY logic="http://co4.inrialpes.fr/xml/dlml/logic/dtd/u.dtd"
  name="techsup+printer">
  <xsl:for-each select="merge/include">
    <xsl:apply-templates select="document(@href)/dl:TERMINOLOGY/*" />
  </xsl:for-each>
</dl:TERMINOLOGY>
</xsl:template>

<xsl:template match="*|@*|text()">
<xsl:copy>
        <xsl:apply-templates select="*|@*|text()"/>
</xsl:copy>
</xsl:template>

</xsl:stylesheet>
