<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:dl="http://co4.inrialpes.fr/xml/dlml/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:daml="http://www.daml.org/2000/10/daml-ont#"
  xmlns:a="https://www.daml.org/actionitems/actionitems-20000905.rdfs#"
  xmlns:rdfs="http://www.w3.org">

<xsl:output
  method="xml"
  version="1.0"
  encoding="UTF-8"
  omit-xml-declaration="no"
  standalone="no"
  doctype-system="http://co4.inrialpes.fr/xml/dlml/logic/dtd/aluni.dtd"
  indent="yes"/> 

<!-- toplevel -->
<xsl:template match="/">
<xsl:text disable-output-escaping="yes">

</xsl:text>
<!-- The Ontology element does not enclose the classes and properties -->
<dl:TERMINOLOGY logic="http://co4.inrialpes.fr/xml/dlml/logic/dtd/aluni.dtd"
  name="{rdf:RDF/Ontology/@about}">
		<xsl:apply-templates select="Class|Property"/>
  <xsl:apply-templates select="rdf:RDF/Class|rdf:RDF/Property" />
</dl:TERMINOLOGY>
</xsl:template>

<!-- definers -->

<xsl:template match="Class">
	<dl:CPRIM><dl:CATOM><xsl:value-of select="@ID"/></dl:CATOM>
        <dl:AND>
		<xsl:apply-templates select="subClassOf|oneOf"/>
              </dl:AND>
	</dl:CPRIM>
</xsl:template>

<xsl:template match="Property">
	<dl:RPRIM><dl:RATOM><xsl:value-of select="@ID"/></dl:RATOM>
        <xsl:apply-templates select="domain" />
	</dl:RPRIM>
</xsl:template>

<!-- intermediate tag -->
<!-- this obviously forgets about the domain -->

<xsl:template match="oneOf">
	<dl:ONEOF>
          <xsl:for-each select="*">
          <dl:INDIVIDUAL name="{@ID}">
            <dl:CATOM><xsl:value-of select="local-name()"/></dl:CATOM>
          </dl:INDIVIDUAL>
        </xsl:for-each>
	</dl:ONEOF>
</xsl:template>

<xsl:template match="domain">
  <dl:DOMAIN>
      <dl:RATOM><xsl:value-of select="substring-after(@resource,'#')"/></dl:RATOM>
  </dl:DOMAIN>
</xsl:template>

<xsl:template match="subClassOf">
  <dl:CATOM><xsl:value-of select="substring-after(@resource,'#')"/></dl:CATOM>
</xsl:template>

<xsl:template match="text()|@*" />

</xsl:stylesheet>
