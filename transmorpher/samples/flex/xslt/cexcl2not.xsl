<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:dl="http://co4.inrialpes.fr/xml/dlml/">

<xsl:output
  method="xml"
  version="1.0"
  encoding="UTF-8"
  omit-xml-declaration="no"
  standalone="no"
  doctype-system="http://co4.inrialpes.fr/xml/dlml/logic/dtd/aluni.dtd"
  indent="yes"/> 

<!-- toplevel -->
<xsl:template match="/">
<xsl:text disable-output-escaping="yes">

</xsl:text>
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="dl:TERMINOLOGY">
	<dl:TERMINOLOGY logic="http://co4.inrialpes.fr/xml/dlml/logic/dtd/aluni.dtd">
          <xsl:comment>The terminology</xsl:comment>
          <xsl:apply-templates />
	</dl:TERMINOLOGY>
</xsl:template>

<!-- usual processing -->
<!-- see how it is done in the FACT2ALUNI2 -->

<xsl:template match="*|@*|text()">
<xsl:copy>
        <xsl:apply-templates select="*|@*|text()"/>
</xsl:copy>
</xsl:template>

<xsl:template match="dl:CEXCL">
  <xsl:if test="dl:CATOM[2]">
    <dl:CEXCL>
      <xsl:copy>
        <xsl:apply-templates select="dl:CATOM[position()>1]"/>
      </xsl:copy>
    </dl:CEXCL>
    <dl:CPRIM>
      <xsl:copy><xsl:apply-templates select="dl:CATOM[1]"/></xsl:copy>
      <dl:AND>
        <xsl:for-each select="dl:CATOM[position()>1]">
          <dl:NOT><xsl:copy><xsl:apply-templates/></xsl:copy></dl:NOT>
        </xsl:for-each>
        </dl:AND>
    </dl:CPRIM>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>
