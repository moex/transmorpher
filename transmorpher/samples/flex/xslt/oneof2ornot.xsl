<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:dl="http://co4.inrialpes.fr/xml/dlml/">

<xsl:output
  method="xml"
  version="1.0"
  encoding="UTF-8"
  omit-xml-declaration="no"
  standalone="no"
  doctype-system="http://co4.inrialpes.fr/xml/dlml/logic/dtd/aluni.dtd"
  indent="yes"/> 

<!-- toplevel -->
<xsl:template match="/">
<xsl:text disable-output-escaping="yes">

</xsl:text>
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="dl:TERMINOLOGY">
	<dl:TERMINOLOGY logic="http://co4.inrialpes.fr/xml/dlml/logic/dtd/aluni.dtd">
          <xsl:comment>The terminology</xsl:comment>
          <xsl:apply-templates />
          <xsl:comment>Introduction of the ONEOF (optional)</xsl:comment>
          <xsl:apply-templates mode="gatheroneof" />
	</dl:TERMINOLOGY>
</xsl:template>

<!-- gather one-of -->

<xsl:template match="@*|text()" mode="gatheroneof"/>

<xsl:template match="dl:ONEOF" mode="gatheroneof">
    <xsl:for-each select="dl:INDIVIDUAL">
      <dl:CPRIM><dl:CATOM><xsl:value-of select="@name"/></dl:CATOM>
        <dl:CATOM><xsl:value-of select="dl:CATOM/text()" /></dl:CATOM>
      </dl:CPRIM>
    </xsl:for-each>
</xsl:template>

<!-- usual processing -->

<xsl:template match="*|@*|text()">
<xsl:copy>
        <xsl:apply-templates select="*|@*|text()"/>
</xsl:copy>
</xsl:template>

<xsl:template match="dl:ONEOF">
  <dl:OR>
    <xsl:for-each select="dl:INDIVIDUAL">
      <dl:CATOM><xsl:value-of select="@name"/></dl:CATOM>
    </xsl:for-each>
  </dl:OR>
</xsl:template>


</xsl:stylesheet>
