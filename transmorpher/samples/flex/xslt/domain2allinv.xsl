<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:dl="http://co4.inrialpes.fr/xml/dlml/">

<xsl:output
  method="xml"
  version="1.0"
  encoding="UTF-8"
  omit-xml-declaration="no"
  standalone="no"
  doctype-system="http://co4.inrialpes.fr/xml/dlml/logic/dtd/aluni.dtd"
  indent="yes"/> 

<!-- toplevel -->
<xsl:template match="/">
<xsl:text disable-output-escaping="yes">

</xsl:text>
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="dl:TERMINOLOGY">
	<dl:TERMINOLOGY logic="http://co4.inrialpes.fr/xml/dlml/logic/dtd/aluni.dtd">
          <xsl:comment>Introduction of the DOMAIN</xsl:comment>
          <dl:CPRIM>
            <dl:ANYTHING />
              <dl:AND>
            <xsl:apply-templates select="dl:RPRIM|dl:RDEF" mode="gatherdomain" />
              </dl:AND>
          </dl:CPRIM>
          <xsl:comment>The terminology</xsl:comment>
          <xsl:apply-templates />
	</dl:TERMINOLOGY>
</xsl:template>

<!-- gather domains in role introduction and add this for root -->
<xsl:template match="dl:RPRIM|dl:RDEF" mode="gatherdomain">
  <dl:ALL><dl:INV><dl:RATOM><xsl:value-of select="dl:RATOM[1]/text()"/></dl:RATOM></dl:INV>
    <xsl:apply-templates select="dl:DOMAIN/*" />
  </dl:ALL>
</xsl:template>

<!-- usual processing -->

<xsl:template match="*|@*|text()">
<xsl:copy>
        <xsl:apply-templates select="*|@*|text()"/>
</xsl:copy>
</xsl:template>

<xsl:template match="dl:RPRIM">
  <dl:RPRIM><dl:RATOM><xsl:value-of select="dl:RATOM[1]/text()"/></dl:RATOM>
  <!-- there can only be domains -->
  <dl:ANYRELATION/>
  </dl:RPRIM>
</xsl:template>

<xsl:template match="dl:RDEF">
  <dl:RDEF><dl:RATOM><xsl:value-of select="dl:RATOM[1]/text()"/></dl:RATOM>
  <!-- there can only be domains -->
  <dl:ANYRELATION/>
  </dl:RDEF>
</xsl:template>

</xsl:stylesheet>
