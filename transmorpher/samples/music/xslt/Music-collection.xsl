<?xml version="1.0" encoding="UTF-8"?>
<!-- This XSL stylesheet demonstrates usage of template modes, template parameters, sorting, conditional tests, variables, and adding dynamic attributes to output elements. -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

	<xsl:param name="artist">All</xsl:param>
	<xsl:variable name="title"><xsl:value-of select="/music-collection/owner"/>'s CD Collection</xsl:variable>
	
	<xsl:template match="/">
    <html>
      <head>
        <title>
          <xsl:value-of select="$title"/>
        </title>
      </head>
      <body bgcolor="lightblue" text="black" >
        <h1>
        	<center><FONT COLOR="orange">
				 
				 	<a name="{title}">
				 	<xsl:attribute name="name">
						<xsl:value-of select="$title"/>
					</xsl:attribute>
					<xsl:value-of select="$title"/><xsl:text> : </xsl:text><xsl:value-of select="translate($artist,'_',' ')"/>
				</a>
				</FONT></center>
        </h1>
        <xsl:apply-templates select="music-collection"/>
      </body>
    </html>
  </xsl:template>
  <xsl:template match="music-collection">
    <!-- Output a table of contents for artists and CDs. -->
    <center><table border="2" cellpadding="5px">
      <!-- Output the table heading row. -->
      <tr>
        <th>Artists</th>
        <th>CDs</th>
      </tr>
      <!-- Output a single table data row. -->
      <tr>
        <!-- Output a table of contents for artists sorted by name. -->
        <td valign="top">
          <!-- There are two templates that output artists. They are distinguished by their "mode". -->
          <xsl:apply-templates select="artist" mode="summary">
            <xsl:sort select="name"/>
          </xsl:apply-templates>
        </td>
        <!-- Output a table of contents for CDs sorted by title. -->
        <td valign="top">
          <!-- There are two templates that output cds. They are distinguished by their "mode". -->
          <xsl:apply-templates select="artist/cd" mode="summary">
            <xsl:sort select="title"/>
          </xsl:apply-templates>
        </td>
      </tr>
    </table></center>
    <!-- Output detailed information about each CD sorted by name. -->
    <xsl:apply-templates select="artist">
      <xsl:sort select="name"/>
    </xsl:apply-templates>
  </xsl:template>
  <!-- Summarized information about artists -->
  <xsl:template match="artist" mode="summary">
    <div>
      <!-- Link to detailed artist information in the output document. -->
      <a href="#{name}">
        <xsl:attribute name="href">#<xsl:value-of select="name"/>
        </xsl:attribute>
        <xsl:value-of select="name"/>
      </a>
    </div>
  </xsl:template>
  <!-- Detailed information about artists (no mode) -->
  <xsl:template match="artist">
    <!-- Determine whether this is an even-numbered artist. -->
    <xsl:variable name="even" select="(position() mod 2) = 0"/>
    <!-- Create an anchor for the detailed artist information. -->
    <a name="{name}">
      <xsl:attribute name="name">
        <xsl:value-of select="name"/>
      </xsl:attribute>
      <h2 style="color:red">
        <p><xsl:value-of select="name"/>
				<xsl:text> </xsl:text><a><xsl:attribute name="href">#<xsl:value-of select="$title"/></xsl:attribute>Up</a>
				<xsl:text> </xsl:text><a><xsl:attribute name="href"><xsl:value-of select="translate(name,' ','_')"/>.html</xsl:attribute>Go</a></p>
			</h2>
			</a>
			
    <!-- Output detailed information about each of the CDs by this artist sorted by descending year. -->
    <xsl:apply-templates select="cd">
      <xsl:sort select="@year" order="descending"/>
      <xsl:with-param name="even" select="$even"/>
    </xsl:apply-templates>
  </xsl:template>
  <!-- Summarized information about CDs -->
  <xsl:template match="cd" mode="summary">
    <div>
      <!-- Link to detailed CD information in the output document. -->
      <a href="#{../name}-{title}">
        <xsl:attribute name="href">#<xsl:value-of select="../name"/>-<xsl:value-of select="title"/>
        </xsl:attribute>
        <xsl:value-of select="title"/>
      </a>
      <!-- Follow each CD title with the name of the artist. -->
	- <xsl:value-of select="../name"/>
    </div>
  </xsl:template>
  <!-- Detailed information about CDs (no mode) -->
  <xsl:template match="cd">
    <!-- This parameter indicates whether the CD is from an even-numbered artist -->
    <xsl:param name="even" select="false()"/>
    <!-- The table background color is based on whether the CD is from an even or odd-numbered artist. -->
    <xsl:variable name="bgcolor">
      <xsl:choose>
        <xsl:when test="$even">orange</xsl:when>
        <xsl:otherwise>white</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <!-- Determine whether the CD has times for any of its tracks. -->
    <xsl:variable name="has-times" select="count(track/time)>0"/>
    <div style="margin-left:2em">
      <!-- Create an anchor for the detailed CD information. -->
      <a name="{../name}-{title}">
        <xsl:attribute name="name">
          <xsl:value-of select="../name"/>-<xsl:value-of select="title"/>
        </xsl:attribute>
				<br/>
        <div style="margin-top:2ex; font- size:medium">
          <b><xsl:value-of select="title"/> - <xsl:value-of select="@year"/></b>
        </div>
				<br/>
      </a>
      <!-- Only output a table containing track information if this CD has data for at least one track. -->
      <xsl:if test="count(track)> 0">
        <table border="2" bgcolor="{$bgcolor}">
          <!-- Output the table head- ing row. -->
          <tr>
            <th>Track #</th>
            <th>Name</th>
            <!-- Only output a column for times if the current CD has at least one track with a time. -->
            <xsl:if test="$has-times">
              <th>Time</th>
            </xsl:if>
          </tr>
          <!-- Output the table data rows. -->
          <xsl:apply-templates select="track">
            <xsl:with-param name="has-times" select="$has-times"/>
          </xsl:apply-templates>
        </table>
      </xsl:if>
    </div>
  </xsl:template>
  <!-- Detailed information about tracks -->
  <xsl:template match="track">
    <!-- This parameter indicates whether any track of the CD has a time. -->
    <xsl:param name="has-times" select="false()"/>
    <!-- Output a table row for this track. -->
    <tr>
      <!-- Output the track number. -->
      <td align="center">
        <xsl:value-of select="@id"/>/<xsl:value-of select="../@tracks"/>
      </td>
      <!-- Output the track name. -->
      <td>
        <xsl:value-of select="name"/>
      </td>
      <!-- Only output this column if the current CD has at least one track with a time . -->
      <xsl:if test="$has-times">
        <td align="center">
          <!-- At least one track of the current CD has a time but this track may not. -->
          <xsl:choose>
            <xsl:when test="time"> 
              <xsl:value-of select="time"/>
           </xsl:when>
            <xsl:otherwise> 
              <!-- A &nbsp; forces table borders to be drawn. -->
						 </xsl:otherwise> 
           </xsl:choose> 
        </td>
      </xsl:if>
    </tr>
  </xsl:template>
</xsl:stylesheet>

