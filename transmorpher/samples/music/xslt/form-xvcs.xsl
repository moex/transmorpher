<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>

<!-- DOCTYPE vcalendar SYSTEM "vcalxml.dtd" -->
<!-- $Id: form-xvcs.xsl,v 1.2 2002-10-30 12:33:46 triolet Exp $ -->

<xsl:stylesheet version="1.0"
  xmlns:vcal="http://co4.inrialpes.fr/xml/pimlib/vcal/1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers">

<xsl:output
  method="text"
  encoding="iso-8859-1"
  omit-xml-declaration="yes"
  standalone="yes"
  indent="yes"/> 

<xsl:strip-space elements="*"/>

<!-- toplevel -->


<xsl:template match="VCALENDAR" priority="2">
<test><!-- Needed by Saxon : saxon have to output a well-formed document !-->
BEGIN:<xsl:value-of select="name()"/><xsl:text>
</xsl:text>
	<xsl:apply-templates/>
END:<xsl:value-of select="name()"/><xsl:text>
</xsl:text>
</test><!-- Needed by Saxon : saxon have to output a well-formed document !-->
</xsl:template>

<xsl:template match="VEVENT | VTODO" priority="1">
BEGIN:<xsl:value-of select="name()"/><xsl:text>
</xsl:text>
	<xsl:apply-templates/>
END:<xsl:value-of select="name()"/><xsl:text>
</xsl:text>
</xsl:template>

<!--xsl:template match="*">
<xsl:value-of select="name()"/>
	<xsl:apply-templates select="@*"/>:<xsl:value-of select="text()"/><xsl:text>
</xsl:text>
</xsl:template-->

<xsl:template match="*[text()]">
<xsl:choose>
<xsl:when test="name()='extension'">
<xsl:value-of select="@x-name"/>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="name()"/>
</xsl:otherwise>
</xsl:choose>
<xsl:apply-templates select="@*[name()!='x-name']"/>:<xsl:value-of select="text()"/><xsl:text>
</xsl:text>
</xsl:template>

<xsl:template match="@*">;<xsl:value-of select="name()"/>=<xsl:value-of select="."/>
</xsl:template>

</xsl:stylesheet>

