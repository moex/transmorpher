<?xml version="1.0" encoding="UTF-8"?>
<!-- This XSL stylesheet demonstrates usage of template modes, template parameters, sorting, conditional tests, variables, and adding dynamic attributes to output elements. -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

	<xsl:param name="artist">All</xsl:param>
	
	<xsl:template match="/">
    <html>
      
				<xsl:apply-templates select="VCALENDAR"/>
			
		</html>
	</xsl:template>
	
	<xsl:template match="VCALENDAR">
				<br></br>
				<center><h2><xsl:value-of select="$artist"/>'s Tour</h2></center>
				<center><a><xsl:attribute name="href"><xsl:value-of select="$artist"/>.vcs</xsl:attribute><xsl:value-of select="$artist"/>.vcs</a></center>
				<xsl:apply-templates select="VEVENT"/>
				
	</xsl:template>

	<xsl:template match="VEVENT">
		<ul>
		<li>
			<xsl:apply-templates select="DTSTART"/><br></br>
			<xsl:apply-templates select="SUMMARY"/>
				</li>
		</ul>
	</xsl:template>
	
	<xsl:template match="DTSTART">
		<xsl:value-of select="substring(text(),7,2)"/>/
		<xsl:value-of select="substring(text(),5,2)"/>/
		<xsl:value-of select="substring(text(),1,4)"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="substring(text(),10,2)"/>:
		<xsl:value-of select="substring(text(),12,2)"/>
		
	</xsl:template>
	
	<xsl:template match="SUMMARY">
				<xsl:value-of select="translate(text(),'\\','')"/>
	</xsl:template>
	</xsl:stylesheet>

