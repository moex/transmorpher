<?xml version="1.0" encoding="iso-8859-1"?>
<!--
encoding="iso-8859-1"
     =========================================================
     XSL document for CarnetAdresses DTD
     Date : 15 mar 01
     Author : FB
     =========================================================
-->

<xsl:stylesheet version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output
  method="html"
  encoding="iso-8859-1"
  version="4.0"
  indent="yes"
  doctype-public="-//w3c//dtd html 4.0 transitional//en"
  media-type="text/html"/>

<!--=======================================================-->
<!--  Document                                             -->
<!--=======================================================-->
<xsl:template match="CarnetAdresses">
  <!-- <xsl:message>D�but de la transformation</xsl:message> -->
  <html>
  <head>
  <xsl:choose>
    <xsl:when test="@name">
      <title>Annuaire de <xsl:value-of select="@name"/></title>
    </xsl:when>
    <xsl:otherwise>
      <title>Annuaire</title>
    </xsl:otherwise>
  </xsl:choose>

  </head>
  <body text="#FFDAC8" bgcolor="#114880" link="#FFB895" vlink="#FBAA02" alink="#FF8040">
  <center>
  <h1><font color="#ADAFD8"><font size="+4">

  <xsl:choose>
    <xsl:when test="@name">

      Annuaire de <xsl:value-of select="@name"/>

    </xsl:when>
    <xsl:otherwise>
      Annuaire
    </xsl:otherwise>
  </xsl:choose>
  </font></font></h1>
<br/>
<table border="0" width="70%"><tr><td align="center">
<xsl:text>&#xA;</xsl:text>
    <xsl:apply-templates select="Fiche/Personne" mode="table"><xsl:sort select="Prenom"/>
</xsl:apply-templates>
</td></tr></table>
<br/>
<br/> 

    <table border="0" width="80%">
    <xsl:for-each select="Fiche">
      <xsl:sort select="Personne/Prenom"/>
      <xsl:apply-templates  select="Personne" mode="normal"/>
      <xsl:apply-templates  select="Personne/Conjoint"/>
      <!--xsl:apply-templates  select="Maison[position()=1]/*"/-->
      <xsl:apply-templates  select="Maison/*"/>
      <xsl:apply-templates  select="Bureau/*"/>
      <xsl:apply-templates  select="Personne/Naissance"/>
      <tr><td>&#xa0;</td><td>&#xa0;</td><td>&#xa0;</td></tr>
    </xsl:for-each>
    </table>
  </center>
  </body>
  </html>
  <!-- <xsl:message>Fin de la transformation</xsl:message> -->
</xsl:template>

<!--=======================================================-->
<!--  Entete avec le nom                                   -->
<!--=======================================================-->
<xsl:template match="Personne" mode="normal">

<tr><td colspan="3" ><a><xsl:attribute name="name"><xsl:value-of select="Prenom"/>.<xsl:value-of select="Nom"/></xsl:attribute></a><font size="+2" color="#ADAFD8"><xsl:value-of select="Prenom"/>&#xa0;<xsl:value-of select="Nom"/></font></td></tr>
<tr><td>&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</td><td>&#xa0;</td><td>&#xa0;</td></tr>

<xsl:if test="Email">
<tr><td>&#xa0;</td><td>Mel</td><td><a><xsl:attribute name="href">MAILTO:<xsl:value-of select="Email"/>?Subject=Salut <xsl:value-of select="Prenom"/></xsl:attribute>
<xsl:value-of select="Email"/></a></td></tr>
</xsl:if>

</xsl:template>


<!--=======================================================-->
<!--  Suite avec l'adresse                                 -->
<!--=======================================================-->
<xsl:template match="Adresse">
<tr><td>&#xa0;</td><td>Adresse <xsl:choose><xsl:when test="parent::Bureau">bureau</xsl:when>
<xsl:otherwise>maison</xsl:otherwise></xsl:choose></td><td>
<xsl:if test="@pre"><xsl:value-of select="@pre"/>,&#xa0;</xsl:if>
<xsl:if test="@num"><xsl:value-of select="@num"/>,&#xa0;</xsl:if>
<xsl:if test="@type"><xsl:value-of select="@type"/>&#xa0;</xsl:if>
<xsl:if test="@nom"><xsl:value-of select="@nom"/>&#xa0;-&#xa0;</xsl:if>
<xsl:if test="@nom2"><xsl:value-of select="@nom2"/>&#xa0;-&#xa0;</xsl:if>
<xsl:if test="@postal"><xsl:value-of select="@postal"/>&#xa0;</xsl:if>
<xsl:if test="@ville"><xsl:value-of select="@ville"/>&#xa0;</xsl:if>
<xsl:if test="@ccedex">CEDEX</xsl:if>
</td></tr>
</xsl:template>

<!--=======================================================-->
<!--  Code                                                 -->
<!--=======================================================-->
<xsl:template match="Code">
<tr><td>&#xa0;</td><td>Code </td><td><xsl:value-of select="."/>
</td></tr>
</xsl:template>


<!--=======================================================-->
<!--  Conjoint                                             -->
<!--=======================================================-->
<xsl:template match="Personne/Conjoint">
<xsl:if test="Prenom">
<tr><td>&#xa0;</td><td>
Conjoint
</td><td><xsl:value-of select="Nom"/>&#xa0;<xsl:value-of select="Prenom"/></td></tr>
</xsl:if>
</xsl:template>

<!--=======================================================-->
<!--  Le t�l�phone                                         -->
<!--=======================================================-->
<xsl:template match="Email">
<tr><td>&#xa0;</td><td>
Mel </td><td><xsl:value-of select="."/></td></tr>
</xsl:template>

<!--=======================================================-->
<!--  Le t�l�phone                                         -->
<!--=======================================================-->
<xsl:template match="Tel">
<tr><td>&#xa0;</td><td>
Tel <xsl:choose>
    <xsl:when test="parent::Bureau">bureau</xsl:when>
    <xsl:otherwise>maison</xsl:otherwise>
  </xsl:choose>
</td><td><xsl:value-of select="."/></td></tr>
</xsl:template>

<!--=======================================================-->
<!--  Le fax                                               -->
<!--=======================================================-->
<xsl:template match="Fax">
<tr><td>&#xa0;</td><td>
Fax <xsl:choose>
    <xsl:when test="parent::Bureau">bureau</xsl:when>
    <xsl:otherwise>maison</xsl:otherwise>
  </xsl:choose>
</td><td><xsl:value-of select="."/></td></tr>
</xsl:template>

<!--=======================================================-->
<!--                                                 -->
<!--=======================================================-->

<!--=======================================================-->
<!--  Date de Naissance                                    -->
<!--=======================================================-->
<xsl:template match="Naissance">
<xsl:if test="enlevepourlinstant">
<tr><td>&#xa0;</td><td>Date de naissance</td><td>
<!--<xsl:if test="@jour"><xsl:value-of select="@jour"/>&#xa0;</xsl:if>
<xsl:if test="@mois"><xsl:value-of select="@mois"/>&#xa0;</xsl:if>-->
<xsl:if test="@annee"><xsl:value-of select="@annee"/></xsl:if>
</td></tr>
</xsl:if>
</xsl:template>

<!--=======================================================-->
<!--  La table des mati�res                                -->
<!--=======================================================-->
<xsl:template match="Personne" mode="table">
<a>
<xsl:attribute name="href">#<xsl:value-of select="Prenom"/>.<xsl:value-of select="Nom"/></xsl:attribute>
<xsl:value-of select="Prenom"/>&#xa0;<xsl:value-of select="Nom"/>
</a>&#xa0;&#xa0;&#xa0;
<xsl:text>&#xA;</xsl:text>
</xsl:template>

</xsl:stylesheet>
