<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <!-- copy la racine et ses attributs -->
  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template match="@*">
      <xsl:attribute name="{name()}"> <xsl:value-of select="."/>
      </xsl:attribute>
  </xsl:template>

 <!-- copy les element ou et leur attributs -->
   <xsl:template match="*">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>  