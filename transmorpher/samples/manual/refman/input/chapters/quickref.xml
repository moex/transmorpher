<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- $Id: quickref.xml,v 1.2 2003-06-06 09:32:00 triolet Exp $-->
<chapter id="quickref" type="appendice" previous="contrib">
	<title>Quick reference</title>
	<para></para>
	<sect1>
		<title>Generators</title>
		<sect2>
			<title>syntax</title>
			<code><![CDATA[<generate id="id" type="type" out="label file="[_stdin_|_null_]">
 {<with-param name="">value</with-param>}*
</generate>]]></code>
 		</sect2>
		<sect2>
			<title>description</title>
			<para>A generator is the starting point of an XML flow. It generates
			XML contents as SAX Events that can be handled by the following process.
			The <element>type</element> attribute allows the mapping to a specific generator.</para>
		</sect2>
		<sect2>
			<title>specific parameters</title>
			<list>
			<item>
			<para>type=readfile</para>
			</item>
			</list>
			<table>
				<row>
					<headercell width="35mm"><para>name</para></headercell>
					<headercell width="35mm"><para>value</para></headercell>
					<headercell width="35mm"><para>notes</para></headercell>
				</row>
				<row>
					<cell><para>file</para></cell>
					<cell><para>the file name to read</para></cell>
					<cell><para></para></cell>
				</row>
				<row>
					<cell><para>validation</para></cell>
					<cell><para>true or false (default)</para></cell>
					<cell><para>enables the validation (DTD)</para></cell>
				</row>
			</table>
		</sect2>
	</sect1>
	
	<sect1>
		<title>Serializers</title>
		<sect2>
			<title>syntax</title>
			<code><![CDATA[<serialize id="id" type="type" in="label">
 {<with-param name="">value</with-param>}*
</serialize>]]></code>
 		</sect2>
		<sect2>
			<title>description</title>
			<para>A serializer is the end point of an xml flow. It transforms SAX events into binary or
			char streams. The <element>type</element> attribute allows the mapping to a specific serializer.</para>
		</sect2>
		<sect2>
			<title>specific parameters</title>
			<list>
			<item><para>type=writefile</para></item>
			</list>
			<table>
				<row>
					<headercell width="35mm"><para>name</para></headercell>
					<headercell width="35mm"><para>value</para></headercell>
					<headercell width="35mm"><para>notes</para></headercell>
				</row>
				<row>
					<cell><para>file</para></cell>
					<cell><para>the file name to write</para></cell>
					<cell><para></para></cell>
				</row>
				<row>
					<cell><para>method</para></cell>
					<cell><para>xml, html or text</para></cell>
					<cell><para></para></cell>
				</row>
				<row>
					<cell><para>indent</para></cell>
					<cell><para>yes or no</para></cell>
					<cell><para></para></cell>
				</row>
				<row>
					<cell><para>version</para></cell>
					<cell><para></para></cell>
					<cell><para></para></cell>
				</row>
				<row>
					<cell><para>encoding</para></cell>
					<cell><para>the encoding to use for the output</para></cell>
					<cell><para>Default value is UTF-8</para></cell>
				</row>
				<row>
					<cell><para>omit-xml-declaration</para></cell>
					<cell><para>yes or no</para></cell>
					<cell><para></para></cell>
				</row>
				<row>
					<cell><para>standalone</para></cell>
					<cell><para>yes or no</para></cell>
					<cell><para></para></cell>
				</row>
				<row>
					<cell><para>doctype-public</para></cell>
					<cell><para></para></cell>
					<cell><para></para></cell>
				</row>
				<row>
					<cell><para>doctype-system</para></cell>
					<cell><para></para></cell>
					<cell><para></para></cell>
				</row>
				<row>
					<cell><para>media-type</para></cell>
					<cell><para></para></cell>
					<cell><para>default value is text/xml</para></cell>
				</row>
			</table>
			<list>
			<item><para>type=pdf (contribution)</para></item>
			</list>
			<table>
				<row>
					<headercell width="35mm"><para>name</para></headercell>
					<headercell width="35mm"><para>value</para></headercell>
					<headercell width="35mm"><para>notes</para></headercell>
				</row>
				<row>
					<cell><para>file</para></cell>
					<cell><para>the file name to write</para></cell>
					<cell><para></para></cell>
				</row>
				<row>
					<cell><para>format</para></cell>
					<cell><para>pdf or ps</para></cell>
					<cell><para>default is pdf</para></cell>
				</row>
			</table>
			<list>
			<item><para>type=svg (contribution)</para></item>
			</list>
			<table>
				<row>
					<headercell width="35mm"><para>name</para></headercell>
					<headercell width="35mm"><para>value</para></headercell>
					<headercell width="35mm"><para>notes</para></headercell>
				</row>
				<row>
					<cell><para>file</para></cell>
					<cell><para>the file name to write</para></cell>
					<cell><para></para></cell>
				</row>
				<row>
					<cell><para>transcoder</para></cell>
					<cell><para>JPEG, TIFF or PNG</para></cell>
					<cell><para>default is JPEG</para></cell>
				</row>
			</table>
		</sect2>
	</sect1>
	
	<sect1>
		<title>Apply externals</title>
		<sect2>
			<title>syntax</title>
			<code><![CDATA[<apply-external id="id" type="type" in="in label" out="out label">
 {<with-param name="">value</with-param>}*
</apply-external>]]></code>
 		</sect2>
		<sect2>
			<title>description</title>
			<para>An apply-external allows to apply some transformations on an XML flow. Actually, 
			Built-in implementation of this composent is base on XSLT.</para>
		</sect2>
		<sect2>
			<title>specific parameters</title>
			<list>
			<item><para>type=xslt</para></item>
			</list>
			<table>
				<row>
					<headercell width="35mm"><para>name</para></headercell>
					<headercell width="35mm"><para>value</para></headercell>
					<headercell width="35mm"><para>notes</para></headercell>
				</row>
				<row>
					<cell><para>file</para></cell>
					<cell><para>the xsl stylesheet</para></cell>
					<cell><para></para></cell>
				</row>
			</table>
			<list>
			<item><para>type=log (contribution)</para></item>
			</list>
			<table>
				<row>
					<headercell width="35mm"><para>name</para></headercell>
					<headercell width="35mm"><para>value</para></headercell>
					<headercell width="35mm"><para>notes</para></headercell>
				</row>
				<row>
					<cell><para>file</para></cell>
					<cell><para>the log file</para></cell>
					<cell><para></para></cell>
				</row>
			</table>
			<list>
			<item><para>type=sql (contribution)</para></item>
			</list>
			<table>
				<row>
					<headercell width="35mm"><para>name</para></headercell>
					<headercell width="35mm"><para>value</para></headercell>
					<headercell width="35mm"><para>notes</para></headercell>
				</row>
				<row>
					<cell><para>driver</para></cell>
					<cell><para>the driver needed to connect to the database</para></cell>
					<cell><para>For example, to connect to MySQL, the requested driver is org.gjt.mm.mysql.Driver</para></cell>
				</row>
				<row>
					<cell><para>base-URL</para></cell>
					<cell><para>url of the database server</para></cell>
					<cell><para></para></cell>
				</row>
				<row>
					<cell><para>base-name</para></cell>
					<cell><para>name of the base</para></cell>
					<cell><para></para></cell>
				</row>
				<row>
					<cell><para>user</para></cell>
					<cell><para>user name</para></cell>
					<cell><para></para></cell>
				</row>
				<row>
					<cell><para>password</para></cell>
					<cell><para>the user password</para></cell>
					<cell><para></para></cell>
				</row>
				<row>
					<cell><para>resultset-node</para></cell>
					<cell><para>the name of the root node</para></cell>
					<cell><para>for example, <element>ruleset</element></para></cell>
				</row>
				<row>
					<cell><para>row-node</para></cell>
					<cell><para>the name of the row node</para></cell>
					<cell><para>for example, <element>row</element></para></cell>
				</row>
			</table>
		</sect2>
	</sect1>
	
	<sect1>
		<title>Apply process</title>
		<sect2>
			<title>syntax</title>
			<code><![CDATA[<apply-process id="" ref="" in="in labels" out="out labels">
 {<with-param name="aName">value</with-param>}*
</apply-process>]]></code>
 		</sect2>
		<sect2>
			<title>description</title>
			<para>An apply-process component is used to call a set of processes defined in a process component.
			The requested process is defined by is name and can be called by several apply-process.</para>
		</sect2>
	</sect1>

</chapter>
