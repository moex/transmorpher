package fr.fluxmedia.transmorpher.utils;

import junit.framework.TestCase;
// JUnitDoclet begin import
import fr.fluxmedia.transmorpher.utils.LinearIndexedStruct;
// JUnitDoclet end import

/**
* Generated by JUnitDoclet, a tool provided by
* ObjectFab GmbH under LGPL.
* Please see www.junitdoclet.org, www.gnu.org
* and www.objectfab.de for informations about
* the tool, the licence and the authors.
*/


public class LinearIndexedStructTest
// JUnitDoclet begin extends_implements
extends TestCase
// JUnitDoclet end extends_implements
{
  // JUnitDoclet begin class
  fr.fluxmedia.transmorpher.utils.LinearIndexedStruct linearindexedstruct = null;
  // JUnitDoclet end class
  
  public LinearIndexedStructTest(String name) {
    // JUnitDoclet begin method LinearIndexedStructTest
    super(name);
    // JUnitDoclet end method LinearIndexedStructTest
  }
  
  public fr.fluxmedia.transmorpher.utils.LinearIndexedStruct createInstance() throws Exception {
    // JUnitDoclet begin method testcase.createInstance
    return new fr.fluxmedia.transmorpher.utils.LinearIndexedStruct();
    // JUnitDoclet end method testcase.createInstance
  }
  
  protected void setUp() throws Exception {
    // JUnitDoclet begin method testcase.setUp
    super.setUp();
    linearindexedstruct = createInstance();
    // JUnitDoclet end method testcase.setUp
  }
  
  protected void tearDown() throws Exception {
    // JUnitDoclet begin method testcase.tearDown
    linearindexedstruct = null;
    super.tearDown();
    // JUnitDoclet end method testcase.tearDown
  }
  
  public void testClear() throws Exception {
    // JUnitDoclet begin method clear
    // JUnitDoclet end method clear
  }
  
  public void testGet() throws Exception {
    // JUnitDoclet begin method get
    // JUnitDoclet end method get
  }
  
  public void testAdd() throws Exception {
    // JUnitDoclet begin method add
    // JUnitDoclet end method add
  }
  
  public void testListIterator() throws Exception {
    // JUnitDoclet begin method listIterator
    // JUnitDoclet end method listIterator
  }
  
  public void testGetKeys() throws Exception {
    // JUnitDoclet begin method getKeys
    // JUnitDoclet end method getKeys
  }
  
  public void testGetLength() throws Exception {
    // JUnitDoclet begin method getLength
    // JUnitDoclet end method getLength
  }
  
  public void testIndexOf() throws Exception {
    // JUnitDoclet begin method indexOf
    // JUnitDoclet end method indexOf
  }
  
  public void testRemove() throws Exception {
    // JUnitDoclet begin method remove
    // JUnitDoclet end method remove
  }
  
  
  
  /**
  * JUnitDoclet moves marker to this method, if there is not match
  * for them in the regenerated code and if the marker is not empty.
  * This way, no test gets lost when regenerating after renaming.
  * Method testVault is supposed to be empty.
  */
  public void testVault() throws Exception {
    // JUnitDoclet begin method testcase.testVault
    // JUnitDoclet end method testcase.testVault
  }
  
  public static void main(String[] args) {
    // JUnitDoclet begin method testcase.main
    junit.textui.TestRunner.run(LinearIndexedStructTest.class);
    // JUnitDoclet end method testcase.main
  }
}
