package fr.fluxmedia.transmorpher.utils;

import junit.framework.TestCase;
// JUnitDoclet begin import
import fr.fluxmedia.transmorpher.utils.Writer;
// JUnitDoclet end import

/**
* Generated by JUnitDoclet, a tool provided by
* ObjectFab GmbH under LGPL.
* Please see www.junitdoclet.org, www.gnu.org
* and www.objectfab.de for informations about
* the tool, the licence and the authors.
*/


public class WriterTest
// JUnitDoclet begin extends_implements
extends TestCase
// JUnitDoclet end extends_implements
{
  // JUnitDoclet begin class
  fr.fluxmedia.transmorpher.utils.Writer writer = null;
  // JUnitDoclet end class
  
  public WriterTest(String name) {
    // JUnitDoclet begin method WriterTest
    super(name);
    // JUnitDoclet end method WriterTest
  }
  
  public fr.fluxmedia.transmorpher.utils.Writer createInstance() throws Exception {
    // JUnitDoclet begin method testcase.createInstance
    return new fr.fluxmedia.transmorpher.utils.Writer();
    // JUnitDoclet end method testcase.createInstance
  }
  
  protected void setUp() throws Exception {
    // JUnitDoclet begin method testcase.setUp
    super.setUp();
    writer = createInstance();
    // JUnitDoclet end method testcase.setUp
  }
  
  protected void tearDown() throws Exception {
    // JUnitDoclet begin method testcase.tearDown
    writer = null;
    super.tearDown();
    // JUnitDoclet end method testcase.tearDown
  }
  
  public void testWrite() throws Exception {
    // JUnitDoclet begin method write
    // JUnitDoclet end method write
  }
  
  public void testWriteln() throws Exception {
    // JUnitDoclet begin method writeln
    // JUnitDoclet end method writeln
  }
  
  
  
  /**
  * JUnitDoclet moves marker to this method, if there is not match
  * for them in the regenerated code and if the marker is not empty.
  * This way, no test gets lost when regenerating after renaming.
  * Method testVault is supposed to be empty.
  */
  public void testVault() throws Exception {
    // JUnitDoclet begin method testcase.testVault
    // JUnitDoclet end method testcase.testVault
  }
  
  public static void main(String[] args) {
    // JUnitDoclet begin method testcase.main
    junit.textui.TestRunner.run(WriterTest.class);
    // JUnitDoclet end method testcase.main
  }
}
