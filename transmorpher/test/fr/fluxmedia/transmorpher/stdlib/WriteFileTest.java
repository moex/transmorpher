package fr.fluxmedia.transmorpher.stdlib;

import junit.framework.TestCase;
// JUnitDoclet begin import
import fr.fluxmedia.transmorpher.stdlib.WriteFile;
// JUnitDoclet end import

/**
* Generated by JUnitDoclet, a tool provided by
* ObjectFab GmbH under LGPL.
* Please see www.junitdoclet.org, www.gnu.org
* and www.objectfab.de for informations about
* the tool, the licence and the authors.
*/


public class WriteFileTest
// JUnitDoclet begin extends_implements
extends TestCase
// JUnitDoclet end extends_implements
{
  // JUnitDoclet begin class
  fr.fluxmedia.transmorpher.stdlib.WriteFile writefile = null;
  // JUnitDoclet end class
  
  public WriteFileTest(String name) {
    // JUnitDoclet begin method WriteFileTest
    super(name);
    // JUnitDoclet end method WriteFileTest
  }
  
  public fr.fluxmedia.transmorpher.stdlib.WriteFile createInstance() throws Exception {
    // JUnitDoclet begin method testcase.createInstance
    return new fr.fluxmedia.transmorpher.stdlib.WriteFile();
    // JUnitDoclet end method testcase.createInstance
  }
  
  protected void setUp() throws Exception {
    // JUnitDoclet begin method testcase.setUp
    super.setUp();
    writefile = createInstance();
    // JUnitDoclet end method testcase.setUp
  }
  
  protected void tearDown() throws Exception {
    // JUnitDoclet begin method testcase.tearDown
    writefile = null;
    super.tearDown();
    // JUnitDoclet end method testcase.tearDown
  }
  
  public void testSetOutputStream() throws Exception {
    // JUnitDoclet begin method setOutputStream
    // JUnitDoclet end method setOutputStream
  }
  
  public void testReset() throws Exception {
    // JUnitDoclet begin method reset
    // JUnitDoclet end method reset
  }
  
  public void testGetContentHandler() throws Exception {
    // JUnitDoclet begin method getContentHandler
    // JUnitDoclet end method getContentHandler
  }
  
  
  
  /**
  * JUnitDoclet moves marker to this method, if there is not match
  * for them in the regenerated code and if the marker is not empty.
  * This way, no test gets lost when regenerating after renaming.
  * Method testVault is supposed to be empty.
  */
  public void testVault() throws Exception {
    // JUnitDoclet begin method testcase.testVault
    // JUnitDoclet end method testcase.testVault
  }
  
  public static void main(String[] args) {
    // JUnitDoclet begin method testcase.main
    junit.textui.TestRunner.run(WriteFileTest.class);
    // JUnitDoclet end method testcase.main
  }
}
