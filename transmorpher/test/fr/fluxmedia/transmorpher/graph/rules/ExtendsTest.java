package fr.fluxmedia.transmorpher.graph.rules;

import junit.framework.TestCase;
// JUnitDoclet begin import
import fr.fluxmedia.transmorpher.graph.rules.Extends;
// JUnitDoclet end import

/**
* Generated by JUnitDoclet, a tool provided by
* ObjectFab GmbH under LGPL.
* Please see www.junitdoclet.org, www.gnu.org
* and www.objectfab.de for informations about
* the tool, the licence and the authors.
*/


public class ExtendsTest
// JUnitDoclet begin extends_implements
extends TestCase
// JUnitDoclet end extends_implements
{
  // JUnitDoclet begin class
  fr.fluxmedia.transmorpher.graph.rules.Extends extend = null;
  // JUnitDoclet end class
  
  public ExtendsTest(String name) {
    // JUnitDoclet begin method ExtendsTest
    super(name);
    // JUnitDoclet end method ExtendsTest
  }
  
  public fr.fluxmedia.transmorpher.graph.rules.Extends createInstance() throws Exception {
    // JUnitDoclet begin method testcase.createInstance
    return new fr.fluxmedia.transmorpher.graph.rules.Extends();
    // JUnitDoclet end method testcase.createInstance
  }
  
  protected void setUp() throws Exception {
    // JUnitDoclet begin method testcase.setUp
    super.setUp();
    extend = createInstance();
    // JUnitDoclet end method testcase.setUp
  }
  
  protected void tearDown() throws Exception {
    // JUnitDoclet begin method testcase.tearDown
    extend = null;
    super.tearDown();
    // JUnitDoclet end method testcase.tearDown
  }
  
  public void testSetGetRuleset() throws Exception {
    // JUnitDoclet begin method setRuleset getRuleset
    java.lang.String[] tests = {"", " ", "a", "A", "�", "�", "0123456789", "012345678901234567890", "\n", null};
    
    for (int i = 0; i < tests.length; i++) {
      extend.setRuleset(tests[i]);
      assertEquals(tests[i], extend.getRuleset());
    }
    // JUnitDoclet end method setRuleset getRuleset
  }
  
  public void testGenerateXML() throws Exception {
    // JUnitDoclet begin method generateXML
    // JUnitDoclet end method generateXML
  }
  
  public void testGenerateXSLTCode() throws Exception {
    // JUnitDoclet begin method generateXSLTCode
    // JUnitDoclet end method generateXSLTCode
  }
  
  public void testGenerateInsideXSLTCode() throws Exception {
    // JUnitDoclet begin method generateInsideXSLTCode
    // JUnitDoclet end method generateInsideXSLTCode
  }
  
  
  
  /**
  * JUnitDoclet moves marker to this method, if there is not match
  * for them in the regenerated code and if the marker is not empty.
  * This way, no test gets lost when regenerating after renaming.
  * Method testVault is supposed to be empty.
  */
  public void testVault() throws Exception {
    // JUnitDoclet begin method testcase.testVault
    // JUnitDoclet end method testcase.testVault
  }
  
  public static void main(String[] args) {
    // JUnitDoclet begin method testcase.main
    junit.textui.TestRunner.run(ExtendsTest.class);
    // JUnitDoclet end method testcase.main
  }
}
