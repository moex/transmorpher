package fr.fluxmedia.transmorpher.graph;

import junit.framework.TestCase;
// JUnitDoclet begin import
import fr.fluxmedia.transmorpher.graph.Transmorpher;
import java.io.File;
// JUnitDoclet end import

/**
* Generated by JUnitDoclet, a tool provided by
* ObjectFab GmbH under LGPL.
* Please see www.junitdoclet.org, www.gnu.org
* and www.objectfab.de for informations about
* the tool, the licence and the authors.
*/


public class TransmorpherTest
// JUnitDoclet begin extends_implements
extends TestCase
// JUnitDoclet end extends_implements
{
  // JUnitDoclet begin class
  fr.fluxmedia.transmorpher.graph.Transmorpher transmorpher = null;
	fr.fluxmedia.transmorpher.graph.Transmorpher t1 = null;
  fr.fluxmedia.transmorpher.graph.Transmorpher t2 = null;
	
  // JUnitDoclet end class
  
  public TransmorpherTest(String name) {
    // JUnitDoclet begin method TransmorpherTest
    super(name);
    // JUnitDoclet end method TransmorpherTest
  }
  
  public fr.fluxmedia.transmorpher.graph.Transmorpher createInstance() throws Exception {
    // JUnitDoclet begin method testcase.createInstance
    return new fr.fluxmedia.transmorpher.graph.Transmorpher(0);
    // JUnitDoclet end method testcase.createInstance
  }
  
  protected void setUp() throws Exception {
    // JUnitDoclet begin method testcase.setUp
    super.setUp();
    transmorpher = createInstance();
		t1 = new Transmorpher( "testTrans", "1.0", 1, false);
    t2 = new Transmorpher( 2 );
    // JUnitDoclet end method testcase.setUp
  }
  
  protected void tearDown() throws Exception {
    // JUnitDoclet begin method testcase.tearDown
    transmorpher = null;
		t1 = null;
    t2 = null;
    super.tearDown();
    // JUnitDoclet end method testcase.tearDown
  }
  
  public void testInitListOfType() throws Exception {
    // JUnitDoclet begin method initListOfType
		t1 =  new Transmorpher( 2 );
		assertEquals("fr.fluxmedia.transmorpher.stdlib.Broadcast",t1.getClassForType("broadcast"));
    // JUnitDoclet end method initListOfType
  }
  
  public void testSetGetName() throws Exception {
    // JUnitDoclet begin method setName getName
    java.lang.String[] tests = {"", " ", "a", "A", "�", "�", "0123456789", "012345678901234567890", "\n", null};
    
    for (int i = 0; i < tests.length; i++) {
      transmorpher.setName(tests[i]);
      assertEquals(tests[i], transmorpher.getName());
    }
		assertTrue(t1.getName().equals("testTrans"));
    t2.setName( "testTrans" );
    assertTrue( t2.getName().equals("testTrans") );
    // JUnitDoclet end method setName getName
  }
  
  public void testSetGetDebug() throws Exception {
    // JUnitDoclet begin method setDebug getDebug
    int[] tests = {Integer.MIN_VALUE, -1, 0, 1, Integer.MAX_VALUE};
    
    for (int i = 0; i < tests.length; i++) {
      transmorpher.setDebug(tests[i]);
      assertEquals(tests[i], transmorpher.getDebug());
    }
		assertEquals(t1.getDebug(),1);
    assertEquals(t2.getDebug(),2);
    t2.setDebug( 1 );
    assertEquals( t2.getDebug(), 1 );
    // JUnitDoclet end method setDebug getDebug
  }
  
  public void testIsThread() throws Exception {
    // JUnitDoclet begin method isThread
		assertTrue( !t1.isThread() );
    assertTrue( !t2.isThread() );
    // JUnitDoclet end method isThread
  }
  
  public void testUseThread() throws Exception {
    // JUnitDoclet begin method useThread
		t1.useThread(true);
		assertTrue( t1.isThread() );
    // JUnitDoclet end method useThread
  }
  
  public void testSetGetFile() throws Exception {
    // JUnitDoclet begin method setFile getFile
    java.io.File[] tests = {new java.io.File("aa"), null};
    
    for (int i = 0; i < tests.length; i++) {
      transmorpher.setFile(tests[i]);
      assertEquals(tests[i], transmorpher.getFile());
    }
		assertTrue( t1.getFile() != null );
    assertTrue( t1.getFile().getName().equals("testTrans.xml") );
    t2.setFile( new File( "testTrans.xml" ) );
    assertTrue( t2.getFile() != null );
    assertTrue( t2.getFile().getName().equals("testTrans.xml") );
    // JUnitDoclet end method setFile getFile
  }
  
  public void testSetGetVersion() throws Exception {
    // JUnitDoclet begin method setVersion getVersion
    java.lang.String[] tests = {"", " ", "a", "A", "�", "�", "0123456789", "012345678901234567890", "\n", null};
    
    for (int i = 0; i < tests.length; i++) {
      transmorpher.setVersion(tests[i]);
      assertEquals(tests[i], transmorpher.getVersion());
    }
		assertTrue( t1.getVersion().equals("1.0") );
    // Might not be ideal
    assertTrue( t2.getVersion() == null );
    t2.setVersion( "0.1" );
    // This one is an error case: it should not be possible to set
    // a version other than the current one.
    assertTrue( t2.getVersion().equals("0.1") );
    // JUnitDoclet end method setVersion getVersion
  }
  
  public void testOptimizedP() throws Exception {
    // JUnitDoclet begin method optimizedP
		assertTrue( !t1.optimizedP() );
    assertTrue( !t2.optimizedP() );
    // JUnitDoclet end method optimizedP
  }
  
  public void testSetOptimized() throws Exception {
    // JUnitDoclet begin method setOptimized
		t2.setOptimized( true );
    assertTrue( t2.optimizedP() );
    // JUnitDoclet end method setOptimized
  }
  
  public void testGetTransformations() throws Exception {
    // JUnitDoclet begin method getTransformations
    // JUnitDoclet end method getTransformations
  }
  
  public void testFindTransformation() throws Exception {
    // JUnitDoclet begin method findTransformation
    // JUnitDoclet end method findTransformation
  }
  
  public void testGetTransformation() throws Exception {
    // JUnitDoclet begin method getTransformation
    // JUnitDoclet end method getTransformation
  }
  
  public void testAddTransformation() throws Exception {
    // JUnitDoclet begin method addTransformation
    // JUnitDoclet end method addTransformation
  }
  
  public void testRemoveTransformation() throws Exception {
    // JUnitDoclet begin method removeTransformation
    // JUnitDoclet end method removeTransformation
  }
  
  public void testGetClassForType() throws Exception {
    // JUnitDoclet begin method getClassForType
		assertEquals("fr.fluxmedia.transmorpher.stdlib.Broadcast",t2.getClassForType("broadcast"));
		assertEquals("fr.fluxmedia.transmorpher.stdlib.ReadFile",t2.getClassForType("readfile"));

    // JUnitDoclet end method getClassForType
  }
  
  public void testGetExtern() throws Exception {
    // JUnitDoclet begin method getExtern
		assertTrue( t2.getExtern("bla") == null );
		t2.addExtern( "bla", "dummy.class.bla", "generator", false );
		// This interface is too light
		assertTrue( t2.getExtern("bla") != null );
		assertTrue( t2.getExtern("bla").equals("dummy.class.bla") );
		t2.removeExtern("bla");
		assertTrue( t2.getExtern("bla") == null );
    // JUnitDoclet end method getExtern
  }
  
  public void testAddExtern() throws Exception {
    // JUnitDoclet begin method addExtern
    // JUnitDoclet end method addExtern
  }
  
  public void testRemoveExtern() throws Exception {
    // JUnitDoclet begin method removeExtern
    // JUnitDoclet end method removeExtern
  }
  
  public void testGetDefexterns() throws Exception {
    // JUnitDoclet begin method getDefexterns
    // JUnitDoclet end method getDefexterns
  }
  
  public void testReserveImport() throws Exception {
    // JUnitDoclet begin method reserveImport
    // JUnitDoclet end method reserveImport
  }
  
  public void testRecordImport() throws Exception {
    // JUnitDoclet begin method recordImport
    // JUnitDoclet end method recordImport
  }
  
  public void testAddImport() throws Exception {
    // JUnitDoclet begin method addImport
		assertTrue( t2.getImports() != null );
		assertTrue( t2.getIncludes() != null );
		assertEquals( t1.getImports().size(), 0 );
		// { String name = "";
		// t1.addImport( name );
		// assertTrue( t1.getImports().contains((Object)name) );
		// assertEquals( t1.getIncludes().size(), 0 );
		// t1.addInclude( name );
		// assertTrue( t1.getIncludes().contains((Object)name) );}
    // JUnitDoclet end method addImport
  }
  
  public void testGetImports() throws Exception {
    // JUnitDoclet begin method getImports
    // JUnitDoclet end method getImports
  }
  
  public void testAddInclude() throws Exception {
    // JUnitDoclet begin method addInclude
    // JUnitDoclet end method addInclude
  }
  
  public void testGetIncludes() throws Exception {
    // JUnitDoclet begin method getIncludes
    // JUnitDoclet end method getIncludes
  }
  
  public void testSetGetMain() throws Exception {
    // JUnitDoclet begin method setMain getMain
   fr.fluxmedia.transmorpher.graph.Main[] tests = {new fr.fluxmedia.transmorpher.graph.Main(null,null)};
    
    for (int i = 0; i < tests.length; i++) {
      transmorpher.setMain(tests[i]);
      assertEquals(tests[i], transmorpher.getMain());
    } 
    // JUnitDoclet end method setMain getMain
  }
  
  public void testRetroNull() throws Exception {
    // JUnitDoclet begin method retroNull
    // JUnitDoclet end method retroNull
  }
  
  public void testSave() throws Exception {
    // JUnitDoclet begin method save
    // JUnitDoclet end method save
  }
  
  public void testSaveAll() throws Exception {
    // JUnitDoclet begin method saveAll
    // JUnitDoclet end method saveAll
  }
  
  public void testGenerateXML() throws Exception {
    // JUnitDoclet begin method generateXML
    // JUnitDoclet end method generateXML
  }
  
  public void testGenerateStylesheet() throws Exception {
    // JUnitDoclet begin method generateStylesheet
    // JUnitDoclet end method generateStylesheet
  }
  
  public void testGenerateExec() throws Exception {
    // JUnitDoclet begin method generateExec
    // JUnitDoclet end method generateExec
  }
  
  public void testGenerateJavaCode() throws Exception {
    // JUnitDoclet begin method generateJavaCode
    // JUnitDoclet end method generateJavaCode
  }
  
  public void testExec() throws Exception {
    // JUnitDoclet begin method exec
    // JUnitDoclet end method exec
  }
  
  
  
  /**
  * JUnitDoclet moves marker to this method, if there is not match
  * for them in the regenerated code and if the marker is not empty.
  * This way, no test gets lost when regenerating after renaming.
  * Method testVault is supposed to be empty.
  */
  public void testVault() throws Exception {
    // JUnitDoclet begin method testcase.testVault
    // JUnitDoclet end method testcase.testVault
  }
  
  public static void main(String[] args) {
    // JUnitDoclet begin method testcase.main
    junit.textui.TestRunner.run(TransmorpherTest.class);
    // JUnitDoclet end method testcase.main
  }
}
