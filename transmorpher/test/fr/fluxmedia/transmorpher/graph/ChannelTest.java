package fr.fluxmedia.transmorpher.graph;

import junit.framework.TestCase;
// JUnitDoclet begin import
import fr.fluxmedia.transmorpher.graph.Channel;
import fr.fluxmedia.transmorpher.graph.Main;

// JUnitDoclet end import

/**
* Generated by JUnitDoclet, a tool provided by
* ObjectFab GmbH under LGPL.
* Please see www.junitdoclet.org, www.gnu.org
* and www.objectfab.de for informations about
* the tool, the licence and the authors.
*/


public class ChannelTest
// JUnitDoclet begin extends_implements
extends TestCase
// JUnitDoclet end extends_implements
{
  // JUnitDoclet begin class
  fr.fluxmedia.transmorpher.graph.Channel channel = null;
  // JUnitDoclet end class
  
  public ChannelTest(String name) {
    // JUnitDoclet begin method ChannelTest
    super(name);
    // JUnitDoclet end method ChannelTest
  }
  
  public fr.fluxmedia.transmorpher.graph.Channel createInstance() throws Exception {
    // JUnitDoclet begin method testcase.createInstance
    return new fr.fluxmedia.transmorpher.graph.Channel();
    // JUnitDoclet end method testcase.createInstance
  }
  
  protected void setUp() throws Exception {
    // JUnitDoclet begin method testcase.setUp
    super.setUp();
    channel = createInstance();
    // JUnitDoclet end method testcase.setUp
  }
  
  protected void tearDown() throws Exception {
    // JUnitDoclet begin method testcase.tearDown
    channel = null;
    super.tearDown();
    // JUnitDoclet end method testcase.tearDown
  }
  
  public void testToString() throws Exception {
    // JUnitDoclet begin method toString
		Channel channel1 = new Channel("test",null);
		assertEquals("test",channel1.getName());
		channel1 = null;
    // JUnitDoclet end method toString
  }
  
  public void testIn() throws Exception {
    // JUnitDoclet begin method in
		Port port = new Port();
		Channel channel1 = new Channel("test",null,port,null);
		assertEquals(port,channel1.in());
		channel1 = null;
		port=null;
    // JUnitDoclet end method in
  }
  
  public void testOut() throws Exception {
    // JUnitDoclet begin method out
		Port port = new Port();
		Channel channel1 = new Channel("test",null,null,port);
		assertEquals(port,channel1.out());
		channel1 = null;
		port=null;
    // JUnitDoclet end method out
  }
  
  public void testSetIn() throws Exception {
    // JUnitDoclet begin method setIn
		Port port = new Port();
		Channel channel1 = new Channel("test",null);
		channel1.setIn(port);
		assertEquals(port,channel1.in());
		channel1 = null;
		port=null;
    // JUnitDoclet end method setIn
  }
  
  public void testSetOut() throws Exception {
    // JUnitDoclet begin method setOut
		Port port = new Port();
		Channel channel1 = new Channel("test",null);
		channel1.setOut(port);
		assertEquals(port,channel1.out());
		channel1 = null;
		port=null;
    // JUnitDoclet end method setOut
  }
  
  public void testSetGetName() throws Exception {
    // JUnitDoclet begin method setName getName
    java.lang.String[] tests = {"", " ", "a", "A", "�", "�", "0123456789", "012345678901234567890", "\n", null};
    
    for (int i = 0; i < tests.length; i++) {
      channel.setName(tests[i]);
      assertEquals(tests[i], channel.getName());
    }
    // JUnitDoclet end method setName getName
  }
  
  public void testGetProcess() throws Exception {
    // JUnitDoclet begin method getProcess
		MainProcess main = new Main(null,null);
		Channel channel1 = new Channel("test",main);
		
		assertEquals(main,channel1.getProcess());
    // JUnitDoclet end method getProcess
  }
  
  public void testClearNull() throws Exception {
    // JUnitDoclet begin method clearNull
		channel.clearNull();
		assertFalse(channel.nullifiedP());
    // JUnitDoclet end method clearNull
  }
  
  public void testNullify() throws Exception {
    // JUnitDoclet begin method nullify
		channel.nullify();
		assertTrue(channel.nullifiedP());
    // JUnitDoclet end method nullify
  }
  
  public void testClearVisited() throws Exception {
    // JUnitDoclet begin method clearVisited
		channel.clearVisited();
		assertFalse(channel.visitedP());
    // JUnitDoclet end method clearVisited
  }
  
  public void testSetVisited() throws Exception {
    // JUnitDoclet begin method setVisited
		channel.setVisited();
		assertTrue(channel.visitedP());
    // JUnitDoclet end method setVisited
  }
  
  public void testVisitedP() throws Exception {
    // JUnitDoclet begin method visitedP
		channel.setVisited();
		assertTrue(channel.visitedP());
    // JUnitDoclet end method visitedP
  }
  
  public void testNullifiedP() throws Exception {
    // JUnitDoclet begin method nullifiedP
		channel.nullify();
		assertTrue(channel.nullifiedP());
    // JUnitDoclet end method nullifiedP
  }
  
  
  
  /**
  * JUnitDoclet moves marker to this method, if there is not match
  * for them in the regenerated code and if the marker is not empty.
  * This way, no test gets lost when regenerating after renaming.
  * Method testVault is supposed to be empty.
  */
  public void testVault() throws Exception {
    // JUnitDoclet begin method testcase.testVault
    // JUnitDoclet end method testcase.testVault
  }
  
  public static void main(String[] args) {
    // JUnitDoclet begin method testcase.main
    junit.textui.TestRunner.run(ChannelTest.class);
    // JUnitDoclet end method testcase.main
  }
}
