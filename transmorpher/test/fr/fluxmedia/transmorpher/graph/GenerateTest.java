package fr.fluxmedia.transmorpher.graph;

import junit.framework.TestCase;
// JUnitDoclet begin import
import fr.fluxmedia.transmorpher.graph.Generate;
import fr.fluxmedia.transmorpher.graph.Port;
import fr.fluxmedia.transmorpher.graph.Main;
import fr.fluxmedia.transmorpher.graph.PortList;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.stdlib.ReadFile;

// JUnitDoclet end import

/**
* Generated by JUnitDoclet, a tool provided by
* ObjectFab GmbH under LGPL.
* Please see www.junitdoclet.org, www.gnu.org
* and www.objectfab.de for informations about
* the tool, the licence and the authors.
*/


public class GenerateTest
// JUnitDoclet begin extends_implements
extends TestCase
// JUnitDoclet end extends_implements
{
  // JUnitDoclet begin class
  fr.fluxmedia.transmorpher.graph.Generate generate = null;
  // JUnitDoclet end class
  
  public GenerateTest(String name) {
    // JUnitDoclet begin method GenerateTest
    super(name);
    // JUnitDoclet end method GenerateTest
  }
  
  public fr.fluxmedia.transmorpher.graph.Generate createInstance() throws Exception {
    // JUnitDoclet begin method testcase.createInstance
    return new fr.fluxmedia.transmorpher.graph.Generate();
    // JUnitDoclet end method testcase.createInstance
  }
  
  protected void setUp() throws Exception {
    // JUnitDoclet begin method testcase.setUp
    super.setUp();
    generate = createInstance();
    // JUnitDoclet end method testcase.setUp
  }
  
  protected void tearDown() throws Exception {
    // JUnitDoclet begin method testcase.tearDown
    generate = null;
    super.tearDown();
    // JUnitDoclet end method testcase.tearDown
  }
  
  public void testRetroNull() throws Exception {
    // JUnitDoclet begin method retroNull
    // JUnitDoclet end method retroNull
  }
  
  public void testSetGetFile() throws Exception {
    // JUnitDoclet begin method setFile getFile
    java.lang.String[] tests = {"", " ", "a", "A", "�", "�", "0123456789", "012345678901234567890", "\n"};
    
    for (int i = 0; i < tests.length; i++) {
      generate.setFile(tests[i]);
      assertEquals(tests[i], generate.getFile());
    }
    // JUnitDoclet end method setFile getFile
  }
  
  public void testGenerateXML() throws Exception {
    // JUnitDoclet begin method generateXML
    // JUnitDoclet end method generateXML
  }
  
  public void testCreateProcess() throws Exception {
    // JUnitDoclet begin method createProcess
    // JUnitDoclet end method createProcess
  }
  
  public void testGenerateJavaCode() throws Exception {
    // JUnitDoclet begin method generateJavaCode
    // JUnitDoclet end method generateJavaCode
  }
  
  /////////////// test of CALLIMPL methods //////////////
	
	public void testAddOut() throws Exception {
    // JUnitDoclet begin method testAddOut
		Port port = new Port ();
		generate.addOut(port);
		
		assertTrue(((PortList)generate.outPorts()).getPort(0)!=null);
    // JUnitDoclet end method testAddOut
  }
	
	public void testAddIn() throws Exception {
    // JUnitDoclet begin method testAddIn
		Port port = new Port ();
		generate.addIn(port);
		
		assertTrue(((PortList)generate.inPorts()).getPort(0)!=null);
    // JUnitDoclet end method testAddIn
  }
	
	public void testSetGetType() throws Exception {
    // JUnitDoclet begin method setType getType
		 java.lang.String[] tests = {"", " ", "a", "A", "�", "�", "0123456789", "012345678901234567890", "\n", };
    
    for (int i = 0; i < tests.length; i++) {
      generate.setType(tests[i]);
      assertEquals(tests[i], generate.getType());
    }
		
		Generate gen = new Generate ("test_id","test_type");
		assertEquals("test_type",gen.getType());
		// JUnitDoclet end method setType getType
  }
	
	public void testSetGetParameters() throws Exception {
    // JUnitDoclet begin method setParameters getParameters
    Parameters parameters = new Parameters();
	
		generate.setParameters(parameters);
		assertEquals(parameters, generate.getParameters());
		// JUnitDoclet end method testSetGetParameters
  }
	
  public void testSetGetId() throws Exception {
    // JUnitDoclet begin method setId GetId
		 java.lang.String[] tests = {"", " ", "a", "A", "�", "�", "0123456789", "012345678901234567890", "\n", };
    
    for (int i = 0; i < tests.length; i++) {
      generate.setId(tests[i]);
      assertEquals(tests[i], generate.getId());
    }
		Generate gen = new Generate ("test_id","test_type");
		assertEquals("test_id",gen.getId());
		// JUnitDoclet end method setId GetId
  }
	
	public void testGetProcess() throws Exception {
    // JUnitDoclet begin method testGetProcess
		 Process main = new Main(null,null);
		 
		 generate = new Generate("test","read",main);
		 
		 assertEquals(main,generate.getProcess());
		// JUnitDoclet end method testGetProcess
  }
	
	public void testNewProcess() throws Exception {
    // JUnitDoclet begin method testCreateProcess
		 String[] vPortOut = generate.outPorts().toStringList();
		 Object[] params = {(Object) vPortOut,generate.getParameters(), (Object)generate.getAttributes()};
		 assertTrue(generate.newProcess("fr.fluxmedia.transmorpher.stdlib.ReadFile",params) instanceof ReadFile);
		// JUnitDoclet end method testNewProcess
  }
  /**
  * JUnitDoclet moves marker to this method, if there is not match
  * for them in the regenerated code and if the marker is not empty.
  * This way, no test gets lost when regenerating after renaming.
  * Method testVault is supposed to be empty.
  */
  public void testVault() throws Exception {
    // JUnitDoclet begin method testcase.testVault
    // JUnitDoclet end method testcase.testVault
  }
  
  public static void main(String[] args) {
    // JUnitDoclet begin method testcase.main
    junit.textui.TestRunner.run(GenerateTest.class);
    // JUnitDoclet end method testcase.main
  }
}
