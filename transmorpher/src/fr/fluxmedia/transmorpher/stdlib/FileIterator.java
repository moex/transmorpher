/**
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.stdlib;
// Imported Transmorpher classes
import fr.fluxmedia.transmorpher.engine.TIterator;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;

// Imported Java classes
import java.util.Enumeration;
import java.util.Vector;
import java.util.NoSuchElementException;
import java.io.*;

/**
 * This iterator returns all the files contained by a directory..
 *
 *@author    triolet
 */
public final class FileIterator implements TIterator {

	/** iterator parameters */
	private Parameters param;

	/**iterator name */
	private String name;

	/** index of filenames array */
	private int index = 0;

	/** directory name */
	private String dir;

	/** suffix for filter */
	private String suffix;

	/** recursive search of files*/
	private String recursive="false";
	
	/** flag for sort files*/
	private String sort="false";
	
	/** filter for file names */
	private FilenameFilter filter;

	/** an array of file names */
	private Vector<String> files;
	
	private String type="file";
	/**
	 * The constructor
	 *
	 *@param  n  the name of this iterator
	 *@param  p  the parameters of this itterator
	 */
	public FileIterator(String n, Parameters p) {
		param = p;
		name = n;
		files = new Vector<String>();
		init(p);
	}

	/**
	 * Static initialization is currently not used
	 *
	 *@param  p  the parameters of this iterator
	 */
	public void staticInit(Parameters p) {
	}

	/**
	 * The initializer
	 *
	 *@param  p  The parameters of this iterator
	 */
	public void init(Parameters p) {
		// Here we must evaluate param in the context of p
		// JE: Check if this is correct
		param = p.bindParameters(param);
		if (param.getParameter("dir") != null) {
			dir = (String) param.getParameter("dir");
		}
		if (param.getParameter("recursive") != null) {
			recursive = (String) param.getParameter("recursive");
		}
		if (param.getParameter("sort") != null) {
			sort = (String) param.getParameter("sort");
		}
		if (param.getParameter("filter") != null) {
			suffix = (String) param.getParameter("filter");
			//System.out.println(suffix);
			filter =
				new FilenameFilter() {
					public boolean accept(File dir, String name) {
						if (name.endsWith(suffix)) {
							return true;
						} else {
							return (new File(dir, name)).isDirectory();
						}
					}
				};
		}
		listDirectory(dir);
	}

	/**
	 * Stores all the file names of a directory in an array.
	 * This method is not recursive yet.
	 *
	 *@param  directory  The directory to explore.
	 */
	private void listDirectory(String directory) {
		String[] tmp ;
		File d = new File(directory);
		
		if (!d.isDirectory()) {
			throw new IllegalArgumentException("Unknown directory");
		}
		tmp = d.list(filter);
		if (sort.equals("true")){
			java.util.Arrays.sort(tmp);
		}
		for (int i=0;i<tmp.length;i++){
			//System.out.println(tmp[i]);
			d= new File(directory+"/"+tmp[i]);
			if (!d.isDirectory()){
				files.add(directory+"/"+tmp[i]);
			}
			else{
				if (recursive.equals("true")){
					//System.out.println("listDirectory "+tmp[i]);
					listDirectory(directory+"/"+tmp[i]);
				}
			}
		}
	}
	public String getName(){
	    return name;
	}
	/**
	 * Tests if this iterator has more elements. If not, returns false.
	 *
	 *@return    true if has more elements, else false.
	 */
	public final boolean hasMoreElements() {
		return (index < files.size());
	}

	/**
	 * Returns the next file name..
	 *
	 *@return    The next file name.
	 */
	public final Object nextElement() {
		return files.get(index++);
	}
	
	public String getType(){
			return type;
	}
}//end class

