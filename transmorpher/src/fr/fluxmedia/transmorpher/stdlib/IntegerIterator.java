/**
 * $Id: IntegerIterator.java,v 1.1 2002-11-06 14:08:21 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
  * Iterate over an integer
*/

package fr.fluxmedia.transmorpher.stdlib ;

// Imported Java classes

import java.util.NoSuchElementException;
import java.util.Enumeration;

// Imported Transmorpher classes

import fr.fluxmedia.transmorpher.engine.TIterator;
import fr.fluxmedia.transmorpher.utils.Parameters ;
import fr.fluxmedia.transmorpher.utils.StringParameters;

public final class IntegerIterator implements TIterator {

    private Parameters param;
    private int result;
    private int value;
    private int incr = 1;
    private int from = 0;
    private int to = 0;
    private boolean down = false;
    private String name;
		private String type="string";
    /** 
     *The constructor 
     */
    public IntegerIterator(String n, Parameters p) {
	param = p;
	name = n;
	init(p);
    }

    /** 
     *Static initialization is currently not used 
     */
    public void staticInit(Parameters p) {
    }

    /**
     * The initializer
     */
    public void init(Parameters p) {
	// Here we must evaluate param in the context of p
	// JE: Check if this is correct
	param = p.bindParameters( param );
	// We must raise an error whenever, this is not castable...
	if ( param.getParameter("from") != null )
	    from = Integer.parseInt((String)param.getParameter("from"));
	if ( param.getParameter("to") != null )
	    to = Integer.parseInt((String)param.getParameter("to"));
	if ( param.getParameter("incr") != null )
	    incr = Integer.parseInt((String)param.getParameter("incr"));
	
	// Check that this is well formed
	down = (to < from);
	if ( ( !down && ( incr <= 0 ) )
	     ||( down && ( incr >= 0 ) ) )
	    throw new NoSuchElementException(); // bad exception...
	// Final initialization
	value = from;
    }
    
    public String getName(){
	    return name;
    }
    /** 
     *Is this the last handler 
     */
    public final boolean hasMoreElements() {
	if ( ( down && (value>= to) ) || ( !down && (value <= to) ) )
	    return true;
	else return false;
    }
  
    /** The next integer
     * having an integer object, created at each iteration, is a very
     * poor policy. We should find something better
     * (but it is not possible to set the value).
     */
    public final Object nextElement() {
	result = value;
	value = value + incr;
	return (Object)String.valueOf(result);
    }
		
		public String getType(){
			return type;
	}
} //end class

