/**
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.stdlib;
// Imported Transmorpher classes
import fr.fluxmedia.transmorpher.engine.TIterator;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
// Imported Java classes
import java.util.Enumeration;
import java.util.NoSuchElementException;

/**
 * This iterator is only used to set up LoopManagerHandler with TestHandler instead of LoopHandler.
 *
 *@author    triolet
 */
public final class FixpointIterator implements TIterator {

	private Parameters param;
	private String name;
	private String condition;
	private String type="fixpoint";
	/**
	 * The constructor
	 *
	 *@param  n  the name of this iterator
	 *@param  p  the parameters of this itterator
	 */
	public FixpointIterator(String n, Parameters p) {
		param = p;
		name = n;
		init(p);
	}

	public String getName(){
		return name;
	}
	
	/**
	 * Static initialization is currently not used
	 *
	 *@param  p  the parameters of this iterator
	 */
	public void staticInit(Parameters p) {
	}

	/**
	 * The initializer
	 *
	 *@param  p  The parameters of this iterator
	 */
	public void init(Parameters p) {
		// Here we must evaluate param in the context of p
		// JE: Check if this is correct
		param = p.bindParameters(param);
		if (param.getParameter("condition") != null) {
			condition = (String) param.getParameter("condition");
		}
	}

	/**
	 * Always returns true.
	 *
	 *@return    always true.
	 */
	public final boolean hasMoreElements() {
		return true;
	}

	/**
	 * Always returns the same value.
	 *
	 *@return    something.
	 */
	public final Object nextElement() {
		String s = "";
		return (Object) s;
	}
	
	public String getType(){
			return type;
	}
}//end class

