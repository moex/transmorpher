/*
 * $Id: StdOut.java,v 1.3 2003-02-11 15:41:01 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
/**
 * This class offer a serializer mechanisme. A way to finish the transphormation process
 * bye writing XML output file
 *
 *@since     jdk 1.3 && SAX 2.0
 *@author    laurent tardif@inrialpes.fr
 *@date      10 02 2001
 */

package fr.fluxmedia.transmorpher.stdlib;

import fr.fluxmedia.transmorpher.engine.TSerializer;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;

import fr.fluxmedia.transmorpher.utils.TMException;
import fr.fluxmedia.transmorpher.utils.TMException;
import fr.fluxmedia.transmorpher.utils.TMRuntimeException;
import fr.fluxmedia.transmorpher.utils.TMRuntimeException;

import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import org.xml.sax.ContentHandler;

// Imported SAX classes

import org.xml.sax.SAXException;

/**
 *  Description of the Class
 *
 *@author    triolet
 */
public final class StdOut extends TSerializer {

	/**
	 *  the handler for the serialization
	 */
	protected TransformerHandler handler;

	/**
	 *  a factory to create a transformer handler
	 */
	protected SAXTransformerFactory tfactory = null;

	/**
	 *  Description of the Field
	 */
	public java.util.Properties Op = null;

	/**
	 *Constructor for the StdOut object
	 *
	 *@param  pIn                     in port of this object
	 *@param  pParam             the parameters of this component
	 *@param  pStaticAttributes  static attributes of tjis component
	 *@exception  TMRuntimeException  Description of the Exception
	 */
	public StdOut(String[] pIn, Parameters pParam, StringParameters pStaticAttributes) throws TMRuntimeException {
		super(pIn, pParam, pStaticAttributes);
		try {
			handler = getTransformerFactory().newTransformerHandler();
			String method = (String)pParam.getParameter("method");

		if (method == null) {
			method = "xml";
		}
		if (method == "html") {
			method = "xhtml";
		}
		if (Op == null) {
			Op = new java.util.Properties();
		}
		Op.setProperty("method", method);
		if (pParam.getParameter("version") != null) {
			Op.setProperty("version", (String)pParam.getParameter("version"));
		} else {
			Op.setProperty("version", "1.0");
		}
		if (pParam.getParameter("encoding") != null) {
			Op.setProperty("encoding", (String)pParam.getParameter("encoding"));
		} else {
			Op.setProperty("encoding", "UTF-8");
		}
		if (pParam.getParameter("omit-xml-declaration") != null) {
			Op.setProperty("omit-xml-declaration", (String)pParam.getParameter("omit-xml-declaration"));
		} else {
			Op.setProperty("omit-xml-declaration", "no");
		}
		if (pParam.getParameter("standalone") != null) {
			Op.setProperty("standalone", (String)pParam.getParameter("standalone"));
		} else {
			Op.setProperty("standalone", "yes");
		}
		if (pParam.getParameter("doctype-public") != null) {
			Op.setProperty("doctype-public", (String)pParam.getParameter("doctype-public"));
		}
		if (pParam.getParameter("doctype-system") != null) {
			Op.setProperty("doctype-system", (String)pParam.getParameter("doctype-system"));
		}
		if (pParam.getParameter("cdata-section-elements") != null) {
			Op.setProperty("cdata-section-elements", (String)pParam.getParameter("cdata-section-elements"));
		}
		if (pParam.getParameter("indent") != null) {
			Op.setProperty("indent", (String)pParam.getParameter("indent"));
		} else {
			Op.setProperty("indent", "no");
		}
		if (pParam.getParameter("media-type") != null) {
			Op.setProperty("media-type", (String)pParam.getParameter("media-type"));
		} else {
			Op.setProperty("media-type", "text/xml");
		}
		} catch (Exception e) {
			throw new TMRuntimeException(e, "[StdOut] Cannot create object");
		}
	}

	/**
	 *  Gets the transformerFactory attribute of the StdOut object
	 *
	 *@return    The transformerFactory value
	 */
	protected SAXTransformerFactory getTransformerFactory() {
		if (tfactory == null) {
			tfactory = (SAXTransformerFactory)TransformerFactory.newInstance();
		}
		return tfactory;
	}

	/**
	 *  Sets the outputStream attribute of the StdOut object
	 */
	public void setOutputStream() {
		handler.getTransformer().setOutputProperties(Op);
		handler.setResult(new StreamResult(System.out));
	}

	/**
	 *  Gets the contentHandler attribute of the StdOut object
	 *
	 *@return    The contentHandler value
	 */
	public ContentHandler getContentHandler() {
		return (ContentHandler)handler;
	}

}

