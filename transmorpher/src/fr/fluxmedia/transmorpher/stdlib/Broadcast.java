/**
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
  * this class define the dispatcher process.
  * dispatch one data flow to different process.
  *
  * @ author laurent tardif
  * @date 10 02 2001
  * @since jdk 1.2 SAX 2.0
  */

package fr.fluxmedia.transmorpher.stdlib ;

// Imported transmorpher classes

import fr.fluxmedia.transmorpher.engine.TDispatcher;
import fr.fluxmedia.transmorpher.utils.Parameters;

// Imported SAX classes

import org.xml.sax.ContentHandler;
import org.xml.sax.Attributes;
import org.xml.sax.Locator ;
import org.xml.sax.SAXException;

import java.util.Hashtable;

public final class Broadcast extends TDispatcher {

    /** the constructor */
    public Broadcast(String[] pIn, String[] pOut,Parameters pParam){
	super(pIn,pOut,pParam);
	finished=false;
    }
        
    /** the start element method of content handler interface */
    public final void startElement(String ns,String localName,String name, Attributes atts) throws SAXException {
	for ( ContentHandler ch: contentHandlers ) {
	    ch.startElement( ns, localName, name,  atts);
	}
    }
    
    /** the endElement method of content handler interface */
    public final void endElement(String ns,String localName,String name) throws SAXException {
	for ( ContentHandler ch: contentHandlers ) {
	    ch.endElement( ns, localName, name);
	}
    }

    /** the setDocumentLocator method of content handler interface */
    public final void setDocumentLocator(Locator locator) {
	for ( ContentHandler ch: contentHandlers ) {
	    ch.setDocumentLocator( locator);
	}
    }

    /** the startDocument method of content handler interface */
    public final void startDocument() throws SAXException {
	for ( ContentHandler ch: contentHandlers ) {
	    ch.startDocument();
	}
    }

    /** the endDocument method of content handler interface */
    public final void endDocument() throws SAXException {
				setFinished(true);

	for ( ContentHandler ch: contentHandlers ) {
	    ch.endDocument();
	}
    }

    /** the skippedEntity method of content handler interface */
    public final void skippedEntity(java.lang.String name) throws SAXException {
	for ( ContentHandler ch: contentHandlers ) {
	    ch.skippedEntity( name);
	}
    }

    /** the processingInstruction method of content handler interface */
    public final void processingInstruction(java.lang.String target, java.lang.String data) throws SAXException {
	for ( ContentHandler ch: contentHandlers ) {
	    ch.processingInstruction(target, data);
	}
    }

    /** the ignorableWhitespace method of content handler interface */
    public final void ignorableWhitespace(char[] c, int start, int length) throws SAXException{
	for ( ContentHandler ch: contentHandlers ) {
	    ch.ignorableWhitespace( c,  start,  length);
	}
    }

    /** the characters method of content handler interface */
    public final void characters(char[] c, int start, int length) throws SAXException{
	for ( ContentHandler ch: contentHandlers ) {
		char[] tmp = null;
		tmp=new char[length + start];
		for (int j = start; j < length + start; j++) {
			tmp[j] = c[j];
		}
	    ch.characters( c,  start,  length);
	}
    }

    /** the startPrefixMapping method of content handler interface */
    public final void startPrefixMapping(java.lang.String prefix, java.lang.String uri) throws SAXException{
	for ( ContentHandler ch: contentHandlers ) {
	    ch.startPrefixMapping( prefix,uri);
	}
    }

    /** the endPrefixMapping method of content handler interface */
    public final void endPrefixMapping(java.lang.String prefix) throws SAXException{
	for ( ContentHandler ch: contentHandlers ) {
	    ch.endPrefixMapping( prefix);
	}
    }
    
} //end class
