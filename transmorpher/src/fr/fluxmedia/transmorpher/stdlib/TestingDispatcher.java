/*
 * $Id: TestingDispatcher.java,v 1.4 2002-11-06 14:08:21 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.transmorpher.stdlib;

// Imported transmorpher classes

import fr.fluxmedia.transmorpher.engine.TDispatcher;
import fr.fluxmedia.transmorpher.utils.Parameters;

// Imported java classes

import java.util.Hashtable;

// Imported SAX classes

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/**
 * This is a simple dispatcher with test. It compares two values, if these values are equals,
 * input is sent to the first output, if these value are not equals, input is sent to the second
 * output.
 * Input is sent to only one output. An empty document is sent to the other.
 *
 *@author    triolet
 */
public final class TestingDispatcher extends TDispatcher {

	/** the test value*/
	String test = "";
	/** the value to compare with the test value*/
	String value = "";
	/** a boolean to store the test result*/
	boolean then = false;

	/**
	 * the constructor
	 *
	 *@param  pIn     Description of the Parameter
	 *@param  pOut    Description of the Parameter
	 *@param  pParam  Description of the Parameter
	 */
	public TestingDispatcher(String[] pIn, String[] pOut, Parameters pParam) {
		super(pIn, pOut, pParam);
	}

	/**
	 * the start element method of content handler interface
	 *
	 *@param  ns                Description of the Parameter
	 *@param  localName         Description of the Parameter
	 *@param  name              Description of the Parameter
	 *@param  atts              Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public final void startElement(String ns, String localName, String name, Attributes atts) throws SAXException {
		if (then) {
			contentHandlers[0].startElement(ns, localName, name, atts);
		} else {
			contentHandlers[1].startElement(ns, localName, name, atts);
		}
	}


	/**
	 * the endElement method of content handler interface
	 *
	 *@param  ns                Description of the Parameter
	 *@param  localName         Description of the Parameter
	 *@param  name              Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public final void endElement(String ns, String localName, String name) throws SAXException {
		if (then) {
			contentHandlers[0].endElement(ns, localName, name);
		} else {
			contentHandlers[1].endElement(ns, localName, name);
		}
	}

	/**
	 * the setDocumentLocator method of content handler interface
	 *
	 *@param  locator  The new documentLocator value
	 */
	public final void setDocumentLocator(Locator locator) {
		if (then) {
			contentHandlers[0].setDocumentLocator(locator);
		} else {
			contentHandlers[1].setDocumentLocator(locator);
		}
	}

	/**
	 * the startDocument method of content handler interface
	 *
	 *@exception  SAXException  Description of the Exception
	 */
	public final void startDocument() throws SAXException {
		test = (String)getParameters().getParameter("test");
		value = (String)getParameters().getParameter("value");

		if (test.equals(value)) {
			then = true;
		}

		contentHandlers[0].startDocument();
		contentHandlers[1].startDocument();
	}

	/**
	 * the endDocument method of content handler interface
	 *
	 *@exception  SAXException  Description of the Exception
	 */
	public final void endDocument() throws SAXException {
		contentHandlers[0].endDocument();
		contentHandlers[1].endDocument();
	}


	/**
	 * the skippedEntity method of content handler interface
	 *
	 *@param  name              Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public final void skippedEntity(java.lang.String name) throws SAXException {
		if (then) {
			contentHandlers[0].skippedEntity(name);
		} else {
			contentHandlers[1].skippedEntity(name);
		}
	}

	/**
	 * the processingInstruction method of content handler interface
	 *
	 *@param  target            Description of the Parameter
	 *@param  data              Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public final void processingInstruction(java.lang.String target, java.lang.String data) throws SAXException {
		if (then) {
			contentHandlers[0].processingInstruction(target, data);
		} else {
			contentHandlers[1].processingInstruction(target, data);
		}
	}

	/**
	 * the ignorableWhitespace method of content handler interface
	 *
	 *@param  ch                Description of the Parameter
	 *@param  start             Description of the Parameter
	 *@param  length            Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public final void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
		if (then) {
			contentHandlers[0].ignorableWhitespace(ch, start, length);
		} else {
			contentHandlers[1].ignorableWhitespace(ch, start, length);
		}
	}


	/**
	 * the characters method of content handler interface
	 *
	 *@param  ch                Description of the Parameter
	 *@param  start             Description of the Parameter
	 *@param  length            Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public final void characters(char[] ch, int start, int length) throws SAXException {
		if (then) {
			contentHandlers[0].characters(ch, start, length);
		} else {
			contentHandlers[1].characters(ch, start, length);
		}
	}


	/**
	 * the startPrefixMapping method of content handler interface
	 *
	 *@param  prefix            Description of the Parameter
	 *@param  uri               Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public final void startPrefixMapping(java.lang.String prefix, java.lang.String uri) throws SAXException {
		if (then) {
			contentHandlers[0].startPrefixMapping(prefix, uri);
		} else {
			contentHandlers[1].startPrefixMapping(prefix, uri);
		}
	}

	/**
	 * the endPrefixMapping method of content handler interface
	 *
	 *@param  prefix            Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public final void endPrefixMapping(java.lang.String prefix) throws SAXException {
		if (then) {
			contentHandlers[0].endPrefixMapping(prefix);
		} else {
			contentHandlers[1].endPrefixMapping(prefix);
		}
	}
}

