/**
 * $Id: RegularExpression.java,v 1.2 2002-11-07 08:13:26 triolet Exp $
 *
 * Transmorpher
 * 
 * Copyright (C) 2001 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
* @authors : Chomat Guillaume
* @date : 30 05 2001
* @content : attempt about regular expression in java
*/

package fr.fluxmedia.transmorpher.stdlib ;

import java.util.*;
import gnu.regexp.RE;
import gnu.regexp.REException;

public class RegularExpression {

    public static String substitute(String text,String source , String target) throws REException{
	RE regularExpression = new RE(source);
	return regularExpression.substitute(text,target);
    }

    public static String substituteAll(String text,String source , String target) throws REException{
	RE regularExpression = new RE(source);
	return regularExpression.substituteAll(text,target);
    }

}
