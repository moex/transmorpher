/**
 * $Id: DirectoryIterator.java,v 1.1 2002-11-06 14:08:21 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
  * Iterate over the filenames of a directory
*/

package fr.fluxmedia.transmorpher.stdlib ;

// Imported Java classes

import java.io.File;
import java.io.FilenameFilter;

// Imported Transmorpher classes

import fr.fluxmedia.transmorpher.engine.TIterator;
import fr.fluxmedia.transmorpher.utils.Parameters ;
import fr.fluxmedia.transmorpher.utils.StringParameters;

public final class DirectoryIterator implements TIterator {

    private Parameters param;
    private FilenameFilter filter;
    private File dir;
    private File list[];
    private int curr = 0;
    private int left;
    private String name;
    private String type="file";

    /** The constructor */
    public DirectoryIterator(String n, Parameters p) {
	param = p;
	name = n;
    }

		public String getName(){
			return name;
		}
		
    /** Static initialization is currently not used */
    public void staticInit(Parameters p) {
    }

    /** The initializer */
    public void init(Parameters p) {
	// Here we must evaluate param in the context of p
	// JE: Check if this is correct
	param = p.bindParameters( param );
	// We must raise an error whenever, this is not castable...
	if ( param.getParameter("path") != null )
	    dir = new File((String)param.getParameter("path"));
	// Currently not used (Filename filter is an interface)
	//if ( param.getParameter("filter") != null )
	//    filter = param.getParameter("filter");
	// We could also allow other parameters:
	// writable, directory, file...
	// JE: Checks... and raise errors
	if( !dir.isDirectory() ){}
	// Final initialization
	list = dir.listFiles();
	curr = 0;
	left = 0;
	for ( int i=0; i<list.length; i++){
	    if( (list[i]).isDirectory() ){
		list[i] = (File)null;
	    } else left++;
	}
    }

    /** Is this the last file */
    public final boolean hasMoreElements() {
	return ( left != 0 );
    }
  
    /** The next file
     */
    public final Object nextElement() {
	while( list[curr++] == null );
	left--;
	return (Object)list[curr-1].getPath();
    }
		
		public String getType(){
			return type;
	}
} //end class



