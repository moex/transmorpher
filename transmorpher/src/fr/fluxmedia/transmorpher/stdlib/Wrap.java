/**
 * $Id: Wrap.java,v 1.1 2002-11-06 14:08:21 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
  * Wrap XML files in a common root
  *
  * @author jerome.euzenat@inrialpes.fr
  * @date 02/07/ 2002
  * @see SAX 2.0 && JDK 1.3
*/

package fr.fluxmedia.transmorpher.stdlib ;

import fr.fluxmedia.transmorpher.utils.Parameters;

import fr.fluxmedia.transmorpher.engine.*;

// Imported SAX CLASSES

import org.xml.sax.SAXException;
import org.xml.sax.ContentHandler;
import org.xml.sax.Attributes;
import org.xml.sax.Locator ;

public final class Wrap extends TMerger {

    /** The constructor */
    public Wrap(String[] pIn, String[] pOut,Parameters pParam) {
	super(pIn,pOut,pParam,(ContentHandler)new WrapHandler());
	((WrapHandler)getManagingHandler()).setOwner(this);
	// Maybe we should add more on determining the ns (implicit+explicit)
	String tag = (String)pParam.getParameter("tag");
	((WrapHandler)getManagingHandler()).setTag(tag);
    }

    /** Is this the last handler */
    public final boolean isLastHandler() {
	return ( iCurrentHandler == iNbIn-1 );
    }
  
    /** Is this the first Handler? */
    public final boolean isFirstHandler() {
	return ( iCurrentHandler == 0 );
    }

} //end class

/** WrapHandler does the real handling of SAX events.
 */
    
final class WrapHandler implements ContentHandler {

    String tag;
    String ns;
    String localName;
    
    /** the manager */
    Wrap iOwner = null;
    
    /** parenthesis rank */
    int rank = 0;
    
    /** the constructor */
    public WrapHandler () {
	rank = 0;
    }

    void setOwner ( Wrap pOwner ) {
	iOwner = pOwner ;
    }

    void setTag ( String tag ) {
	int index = 0;
	int curr = 0;
	this.tag = tag ;
	// fin the last ":"
	while ( index != -1 ) {
	    curr = index;
	    index = tag.indexOf(':',curr);
	}
	if ( curr != 0 ){
	    ns = tag.substring(0,curr);
	    localName = tag.substring(curr+1);
	 } else {
	     ns = "";
	     localName = tag;
	 }   
    }

    /** The startElement command */
    public final void startElement(String ns,String localName,String name, Attributes atts) throws SAXException {
	rank++;
	// Add the wrapping tag before first input
	if( rank == 1 && iOwner.isFirstHandler() ){
	    iOwner.getContentHandler().startElement(this.ns,this.localName,this.tag,(Attributes)null);
	}
	iOwner.getContentHandler().startElement(ns,localName,name,atts);
    }

    /** The endElement command */
    public final void endElement(String ns,String localName,String name) throws SAXException {
	// Add the wrapping endtag after the last output
	iOwner.getContentHandler().endElement(ns,localName,name);
	if( rank == 1 && iOwner.isLastHandler() ){
	      iOwner.getContentHandler().endElement(this.ns,this.localName,this.tag);
	}
	rank--;
    }
    
    /** The startDocument command */
    public final void startDocument() throws SAXException {
	if( iOwner.isFirstHandler() ){
	    iOwner.getContentHandler().startDocument();
	}
    }
    
    /** The setDocumentLocator command */
    public final void setDocumentLocator(Locator locator) {
      iOwner.getContentHandler().setDocumentLocator(locator);
    }
    
    /** The endDocument command */
    public final void endDocument() throws SAXException {
	if (iOwner.isLastHandler()) {
	    iOwner.getContentHandler().endDocument();
        }
	iOwner.isFinished();
    }
    
    /** The skippedEntity command */
    public final void skippedEntity(java.lang.String name) throws SAXException{
	iOwner.getContentHandler().skippedEntity(name);
    }
    
    /** The processingInstruction command */
    public final void processingInstruction(java.lang.String target, java.lang.String data) throws SAXException{
	if( iOwner.isFirstHandler() ){
	    iOwner.getContentHandler().processingInstruction(target,data);
	}
    }
    
    /** The ignorableWhitespace command */
    public final void ignorableWhitespace(char[] ch, int start, int length) throws SAXException{
	iOwner.getContentHandler().ignorableWhitespace(ch,start,length);
    }
    
    /** The characters command */
    public final void characters(char[] ch, int start, int length) throws SAXException{
	iOwner.getContentHandler().characters(ch,start,length);
	/*
	  String toto = "";
	  for (int i=start; i<length+start; i++) toto+=ch[i];
	  System.out.println("Handler . characters ; " + toto );
	*/
    }
    
    /** The startPrefixMapping command */
    public final void startPrefixMapping(java.lang.String prefix, java.lang.String uri) throws SAXException{
	iOwner.getContentHandler().startPrefixMapping(prefix,uri);
    }
    
    /** The endPrefixMapping command */
    public final void endPrefixMapping(java.lang.String prefix) throws SAXException{
	iOwner.getContentHandler().endPrefixMapping(prefix);
    }
    
} //end class



