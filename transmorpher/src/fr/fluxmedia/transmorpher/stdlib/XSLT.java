/**
 * $Id: XSLT.java,v 1.2 2003-04-22 14:53:57 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

 /** The instance  constructor
  
  @throws TransformerException
  @throws TransformerConfigurationException
  @throws SAXException
  @throws IOException
  
  */

package fr.fluxmedia.transmorpher.stdlib;

import fr.fluxmedia.transmorpher.engine.TApplyExternal;
import fr.fluxmedia.transmorpher.engine.TTransformation;
import fr.fluxmedia.transmorpher.utils.StringParameters;
import fr.fluxmedia.transmorpher.utils.Parameters;

// Imported TraX classes

import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;

// Imported SAX classes

import org.xml.sax.SAXException;
import org.xml.sax.ContentHandler;

// Imported Java classes

import java.io.IOException;

/**
  The function just have to behave like the built-inTTransformation
*/
public final class XSLT extends TTransformation implements TApplyExternal {

    public XSLT (String[] pIn, String[] pOut, Parameters pParam, StringParameters staticParams)
	throws TransformerException, TransformerConfigurationException,
	       SAXException, IOException{
	super(pIn,pOut, pParam);
    }
		
	public void setContentHandler(ContentHandler handler){
	}
} //end class
