/*
 * $Id: WriteFile.java,v 1.5 2007-03-05 22:40:39 euzenat Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.stdlib;

import fr.fluxmedia.transmorpher.engine.TSerializer;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
import fr.fluxmedia.transmorpher.utils.TMException;
import fr.fluxmedia.transmorpher.utils.TMRuntimeException;

// Imported java.io classes
import java.io.BufferedOutputStream;

// Imported JAXP classes

import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import org.xml.sax.ContentHandler;

// Imported SAX classes

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * This class offer a serializer mechanisme. A way to finish the transphormation process
 * by writing XML output file
 *
 *@author    laurent tardif@inrialpes.fr
 *@since     jdk 1.3 && SAX 2.0
 *@date      10 02 2001
 */
public final class WriteFile extends TSerializer {

	final static int DEFAULT_BUFFER_SIZE = 8192;

	/**
	 *  Description of the Field
	 */
	public java.util.Properties Op = null;

	/**
	 *  The handler for the transformation
	 */
	protected TransformerHandler handler;

	/**
	 *  Buffer size
	 */
	protected int outputBufferSize = DEFAULT_BUFFER_SIZE;

	/**
	 *  a factory to create the transformer handler
	 */
	protected SAXTransformerFactory tfactory = null;

	/**
	 *Constructor for the WriteFile object
	 *
	 *@param  pIn                     in ports of this object
	 *@param  pParam                  parameters of this object
	 *@param  pStaticAttributes       static parameters of this object
	 *@exception  TMRuntimeException  Description of the Exception
	 */
	public WriteFile(String[] pIn, Parameters pParam, StringParameters pStaticAttributes)
		 throws TMRuntimeException {
		super(pIn, pParam, pStaticAttributes);
		try {
			handler = getTransformerFactory().newTransformerHandler();
			String method = (String)pParam.getParameter("method");
			format = (String)pParam.getParameter("format");
			if (method == null) {
				method = "xml";
			}
			if (method.equals("html")) {
				method = "xhtml";
			}
			if (Op == null) {
				Op = new java.util.Properties();
			}
			Op.setProperty("method", method);
			if (pParam.getParameter("version") != null) {
				Op.setProperty("version", (String)pParam.getParameter("version"));
			} else {
				Op.setProperty("version", "1.0");
			}
			if (pParam.getParameter("encoding") != null) {
				Op.setProperty("encoding", (String)pParam.getParameter("encoding"));
			} else {
				Op.setProperty("encoding", "UTF-8");
			}
			if (pParam.getParameter("omit-xml-declaration") != null) {
				Op.setProperty("omit-xml-declaration", (String)pParam.getParameter("omit-xml-declaration"));
			} else {
				Op.setProperty("omit-xml-declaration", "no");
			}
			if (pParam.getParameter("standalone") != null) {
				Op.setProperty("standalone", (String)pParam.getParameter("standalone"));
			} else {
				Op.setProperty("standalone", "yes");
			}
			if (pParam.getParameter("doctype-public") != null) {
				Op.setProperty("doctype-public", (String)pParam.getParameter("doctype-public"));
			}
			if (pParam.getParameter("doctype-system") != null) {
				Op.setProperty("doctype-system", (String)pParam.getParameter("doctype-system"));
			}
			if (pParam.getParameter("cdata-section-elements") != null) {
				Op.setProperty("cdata-section-elements", (String)pParam.getParameter("cdata-section-elements"));
			}
			if (pParam.getParameter("indent") != null) {
				Op.setProperty("indent", (String)pParam.getParameter("indent"));
			} else {
				Op.setProperty("indent", "no");
			}
			if (pParam.getParameter("media-type") != null) {
				Op.setProperty("media-type", (String)pParam.getParameter("media-type"));
			} else {
				Op.setProperty("media-type", "text/xml");
			}
		} catch (Exception e) {
			throw new TMRuntimeException(e, "[WriteFile] Cannot create object");
		}
	}

	/**
	 *  Gets the transformerFactory attribute of the WriteFile object
	 *
	 *@return    The transformerFactory value
	 */
	protected SAXTransformerFactory getTransformerFactory() {
		if (tfactory == null) {
			tfactory = (SAXTransformerFactory)TransformerFactory.newInstance();
		}
		return tfactory;
	}

	/**
	 *  Sets the ouput file of this serializer
	 *
	 *@exception  TMRuntimeException  Description of the Exception
	 *@exception  TMException         Description of the Exception
	 *@exception  SAXException        Description of the Exception
	 */
	public void setOutputStream() throws TMRuntimeException, TMException, SAXException {
		super.setOutputStream();
		BufferedOutputStream streamBuffer = new BufferedOutputStream(getOutputStream(), outputBufferSize);
		handler.getTransformer().setOutputProperties(Op);
		handler.setResult(new StreamResult(streamBuffer));
	}

	/**
	 * Creates a new transformer handler at each loop in a repeat
	 *
	 *@exception  TMRuntimeException  Description of the Exception
	 */
	public void reset() throws TMRuntimeException {
		try {
			handler = getTransformerFactory().newTransformerHandler();
		} catch (Exception e) {
			throw new TMRuntimeException(e, "[WriteFile] Reset error");
		}
	}

	/**
	 *  Gets the contentHandler attribute of the WriteFile object
	 *
	 *@return    The contentHandler value
	 */
	public ContentHandler getContentHandler() {
		return (ContentHandler)handler;
	}

}

