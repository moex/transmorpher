/*
 * $Id: ReadFile.java,v 1.7 2006-11-28 20:42:49 euzenat Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2006 INRIA Rh�ne-Alpes.
 *
 * http://transmorpher.gforge.inria.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.transmorpher.stdlib;

// Imported Transmorpher classes

import fr.fluxmedia.transmorpher.engine.TReader;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
import fr.fluxmedia.transmorpher.utils.TMException;

// Imported Java classes

import java.io.IOException;

// Imported SAX and JAXP classes

import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.SAXParseException;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;

/**
 * This class provide a basic process which is
 * able to read an XML file
 * reads a file which coordinates (local path or URL) is give in the
 * file parameter of the generate call, parse it and generate a SAX event
 * flow to the next handler
 *
 *@author    Laurent.Tardif@inrialpes.fr
 *@since     jdk 1.3 && SAX 2.0
 */

public final class ReadFile extends TReader {

    /**
     *Constructor for the ReadFile object
     *
     *@param  pOut               Description of the Parameter
     *@param  pParam             Description of the Parameter
     *@param  pStaticAttributes  Description of the Parameter
     *@exception  SAXException   Description of the Exception
     *@exception  IOException    Description of the Exception
     */
    public ReadFile(String[] pOut, Parameters pParam, StringParameters pStaticAttributes) throws SAXException, IOException {
	super(pOut, pParam, pStaticAttributes);
	SAXParserFactory parserFactory = SAXParserFactory.newInstance();
	try {
	    try {
		parserFactory.setFeature(NAMESPACES_FEATURE_ID, DEFAULT_NAMESPACES);
		parserFactory.setFeature(NAMESPACES_PREFIXES_FEATURE_ID, DEFAULT_NAMESPACES_PREFIXES);
		/*if (((String)getParameter("validation"))!=null){
		    if (((String)getParameter("validation")).equals("true")){
			parserFactory.setFeature(VALIDATION_FEATURE_ID, true);
		    }
		    else
			parserFactory.setFeature(VALIDATION_FEATURE_ID, DEFAULT_VALIDATION);
			}*/
		parserFactory.setFeature(VALIDATION_FEATURE_ID, false);
		/* JE:
		 * Apparently SCHEMA_VALIDATION_FEATURE_ID was there in a 
		 * early version of xerces but did not make it to the official
		 * Java XML stuff.
		 * I have to comment it out.
		 */
		//parserFactory.setFeature(SCHEMA_VALIDATION_FEATURE_ID, DEFAULT_SCHEMA_VALIDATION);
		// In the cases, the parser does not fully implement SAX
	    } catch (SAXNotRecognizedException e) {
		e.printStackTrace();
	    } catch (SAXNotSupportedException e) {
		e.printStackTrace();
	    };
	    iReader = (parserFactory.newSAXParser()).getXMLReader();
	} catch (ParserConfigurationException e) {
	    System.err.println("[ReadFile] Exception :" + e);
	}//end try
    }
    
    /**
     *  Description of the Method
     *
     *@exception  IOException   if an IO operations failed
     *@exception  SAXException  can wrap others exceptions
     */
    public void read() throws IOException, SAXException {
	try { iReader.parse(getInputSource()); }
	catch (Exception e) { e.printStackTrace(); }
    }
}

