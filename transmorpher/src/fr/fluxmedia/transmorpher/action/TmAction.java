
/**
 * $Id: TmAction.java,v 1.2 2002-06-28 14:17:58 gchomat Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
  * define an action in the transmorpher
  *
  * @author Jerome.Euzenat@inrialpes.fr
  * @since jdk 1.3 / SAX 2.0
  */

package fr.fluxmedia.transmorpher.action;

public interface TmAction{
    
    public static int ADD = 0;
    public static int REMOVE = 1;
    public static int MODIFY = 2;   

    /**
     * Get the value of type.
     * @return value of type.
     */
    public int getType();
    
    /**
     * Get the value of source.
     * @return value of source.
     */
    public Object getSource();
       
    /**
       * Get the value of target.
       * @return value of target.
       */
    public Object getTarget();
 
    /**
     * Get the value of oldValue.
     * @return value of oldValue.
     */
    public Object getOldValue();
       
    /**
     * Get the value of newValue.
     * @return value of newValue.
     */
    public Object getNewValue();
	    
}
