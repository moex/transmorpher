/**
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2003-2004, 2022 INRIA Rh�ne-Alpes.
 *
 * https://gitlab.inria.fr/moex/transmorpher
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
  * Transmorpher graph processes definitions
  *
  * @author Jerome.Euzenat@inrialpes.fr
  * @since jdk 1.3
  */

package fr.fluxmedia.transmorpher.utils ;

public interface Version {

    public static final String VERSION = "1.0";
    public static final String RELEASE = "1.0.7 (git)";
    public static final String NAME = "Transmorpher";
    public static final String DATE = "@DATE@";
    public static final String URL = "https://transmorpher.inrialpes.fr";
    public static final String RIGHTS = "(C) FluxMedia & INRIA Rh�ne-Alpes, 2001-2002\n(C) INRIA Rh�ne-Alpes, 2003-2004, 2022";
}
