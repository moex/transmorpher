/**
 * Transmorpher
 *
 * Copyright (C) 2001 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2022 INRIA Rh�ne-Alpes.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** define the Process parameter structures
*/

package fr.fluxmedia.transmorpher.utils ;

// import java classes
import java.util.Hashtable;
import java.util.Enumeration;

public class StringParameters extends Parameters{
 
    
  public StringParameters() {
    super();
  }
    
  public void setParameter( String name, String value ){
    parameters.put( name, value );
  }

  public String getStringParameter( String name ){
    return (String)parameters.get( name );
  }


} //end class

