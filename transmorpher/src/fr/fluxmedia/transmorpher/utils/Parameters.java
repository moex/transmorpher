/**
 * Transmorpher
 *
 * Copyright (C) 2001 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2022 INRIA Rh�ne-Alpes.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** define the Process parameter structures
*/

package fr.fluxmedia.transmorpher.utils ;

// import java classes
import java.util.Hashtable;
import java.util.Enumeration;

public class Parameters {
 
  /** The list of unlinked out  XML_Port */
    Hashtable<String,Object> parameters = null;
    
  public Parameters() {
    parameters = new Hashtable<String,Object>();
  }
  
  public Parameters(Parameters toCopy) {
    parameters = new Hashtable<String,Object>();
    setParameters(toCopy);
  }
 
  public boolean isEmpty() {
    return parameters.isEmpty();
  }

  public void setParameter(String name, Object value){
    parameters.put(name,value);
  }

  public void setParameters(Parameters toCopy){
    for( Enumeration<String> e = toCopy.getNames() ; e.hasMoreElements(); ){
      String key = e.nextElement();
      Object val = toCopy.getParameter(key);
      if ( val != null )setParameter( key, val );
    } //end for
  }
 
  public void unsetParameter(String name){
    parameters.remove(name);
  }

  public Object getParameter(String name){
    return parameters.get(name);
  }

  public Enumeration<String> getNames(){
    return parameters.keys();
  }

  public String evalString( String s ){
      String result = "";
      int first = 0;
      int index = 0;
      String name = null;
      String value = null;

      for( ; index != -1 ; ){
	  index = s.indexOf('$',first);
	  if ( index == -1 ) result = result+s.substring(first); // EOS
	  else {
	      int bck = index;
	      // Get the number of preceding backslashes (=index-bck)
	      while ( ( bck != 0 && s.charAt(bck-1) == '\\') ) bck--;
	      if ( (index-bck)%2 != 0 ) {
		  // odd number of backslashes: ignore the $
		  result = result+s.substring(first,index+1);
		  first = index+1;
	      } else { // Regular case
		  result = result+s.substring(first,index);
		  index++;
		  for( ; (index < s.length()) && (s.charAt(index) == ' ' || s.charAt(index) == '\t') ; index++ );
		  // Get the name
		  if (s.charAt(index) == '{'){
		      first = index+1;
		      index = s.indexOf('}',first);
		      name = s.substring(first,index);
		      if ( index == s.length() ) first = -1;
		      else first = index+1;
		  } else {
		      name = s.substring(index);
		      index = -1;
		  }
		  // Get the value and set it if not null
		  value = (String)getParameter(name);

		  if ( value != null ) result = result+value;
		  else {
		      // JE: strange, this is just a warning
		      System.err.println("Variable not bound: "+name);
		      return s;
		  }
	      }
	  }
      }
      return result;
  }

      /** this does not create a new parameter structure but modifies the arguments */

    // Eval the parameters in a parameter list
  public Parameters bindParameters( Parameters p ){
    for( Enumeration<String> e = p.getNames() ; e.hasMoreElements(); ){
      String key = e.nextElement();
      String val = (String)p.getParameter(key);
      //System.err.println("Set "+key+" to "+evalString(val));
      if ( val != null ) p.setParameter( key, evalString(val) );
    } //end for
    return p;
  }

    public String evalParameter(String name){
	return evalString((String)this.getParameter(name));
    }
	
    // Replace the parameters in a parameter list, if they are not defined, eval their default
  public Parameters bindCallerParameters( Parameters p ){
      for( Enumeration<String> e = p.getNames() ; e.hasMoreElements(); ){
	  String key = e.nextElement();
	  String val = (String)getParameter(key); // resolve the variable
	  if ( val == null ) val = evalString((String)p.getParameter(key)); // get its default value
	  //System.err.println("Set "+key+" to "+val);
	  if ( val != null ) p.setParameter( key, val );
      } //end for
      return p;
  }
    
  public void generateXMLDeclarations (){
    for( Enumeration<String> e = parameters.keys() ; e.hasMoreElements(); ){
      String key = e.nextElement();
      String val = (String)parameters.get(key);
      if ( val == null ) val = "";
      System.out.print("    <param name=\""+key+"\">"+val+"</param>");
    } //end for
  }
  public void generateXMLCalls (){
    for( Enumeration<String> e = parameters.keys() ; e.hasMoreElements(); ){
      String key = e.nextElement();
      System.out.println("        <with-param name=\""+key+"\">"+(String)parameters.get(key)+"</with-param>");
    } //end for
  } //end proc
 
  public void printParameters (){
    System.out.println("[");
    for( Enumeration<String> e = parameters.keys() ; e.hasMoreElements(); ){
      String key = e.nextElement();
      System.out.println( key+"="+(String)parameters.get(key)+";" );
    } //end for
    System.out.println("]");
  } //end proc
 
} //end class

