/**
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** define a general-purpose structure whose elements are accessible
    both through a hashtable and a linear order
*/

package fr.fluxmedia.transmorpher.utils ;

// import java classes
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Iterator;

public class LinearIndexedStruct<O> implements Iterable<O> {
 
  /** The list of unlinked out  XML_Port */
    Hashtable<String,O> table = null;
    LinkedList<O> list = null;
    int last = 0;
    
  public LinearIndexedStruct() {
      table = new Hashtable<String,O>();
    list = new LinkedList<O>();
  }

  public void clear(){
    table.clear();
    list.clear();
    last = 0;
  }
    
  public O get(String name){
    return table.get(name);
  }

  public O get(int i){
    return list.get(i);
  }

  public void add(String n, O o){
    table.put(n, o);
    list.addLast(o);
    last++;
  }

  public void add(String n, O o, int i){
    table.put(n, o);
    last++;
    for( int j=last ; j>i; j++) list.set(j,list.get(j-1));
    list.set(i,o);
  }

    public ListIterator<O> listIterator(){
	return list.listIterator();
    }
    
    public Iterator<O> iterator(){
	return list.iterator();
    }
    
    public Enumeration<String> getKeys(){
	return table.keys();
    }

    public int getLength(){
	return last;
    }

    public int indexOf(O o){
	return list.indexOf(o);
    }
    
    public void remove(String name){
	O o = table.get(name);
	table.remove( name );	
		
	for( int i=list.indexOf(o) ; i<last-1; i++) list.set(i,list.get(i+1));
	list.removeLast();
	last--;
  }

}

