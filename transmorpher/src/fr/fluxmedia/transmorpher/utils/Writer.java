/**
 * $Id: Writer.java,v 1.1 2002-11-06 14:08:21 serge Exp $
 *
 * Transmorpher
 * 
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** 
 * Provides convenient write methods (indent + ln)
*/

package fr.fluxmedia.transmorpher.utils ;

import java.io.IOException;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;

public class Writer extends BufferedWriter {
  
    public Writer(String pName) throws java.io.IOException {
	super(new OutputStreamWriter(new FileOutputStream(pName),"UTF-8"));
    }
    
    public void write(int Decalage,String pString) throws java.io.IOException {
	for (int i=0;i<Decalage;i++) write(" ");
	write(pString);    
    }

    public void writeln(int Decalage,String pString) throws java.io.IOException {
	write(Decalage,pString);       
	newLine();
    }
    
    public void writeln(String pString) throws java.io.IOException {
	write(pString);
	newLine();
    }

    public void writeln() throws java.io.IOException {
	newLine();
    }
}

/*
 This is a test class for deciding which solution is the best between
 concatenating or printing the indentation.

 Printing is constantly better
 (even if we do printing first, but a bit less)

 The value of ii from 1 to 10 is a realistic intentation.
 With higher values (from 10^1 to 10^5) printing is even better.

import java.io.File;
import java.io.IOException;
import java.io.FileWriter;


public final class mytest {

    public final static void main(String argc[]) {
	String pString = "toot";
	double begin = 0;
	double end = 0;
	FileWriter f = null;

	for ( int ii=1; ii<10; ii++ ){
	    try {

		f = new FileWriter("/tmp/toto1");
		java.lang.System.gc();
		begin = System.currentTimeMillis();
		for ( int j=0; j<1000; j++ ){
		    String vParam ="";
		    for (int i=0;i<ii;i++) vParam+=" ";
		    f.write(vParam+pString);
		}
		end = System.currentTimeMillis();
		f.close();
		System.err.print("Concatenation (" + (end - begin) + ") ");
		
		f = new FileWriter("/tmp/toto2");
		java.lang.System.gc();
		begin = System.currentTimeMillis();
		for ( int j=0; j<1000; j++ ){
		    for (int i=0;i<ii;i++) f.write(" ");
		    f.write(pString);
		}
		end = System.currentTimeMillis();
		f.close();
		System.err.print("Impression  (" + (end - begin) + ") ");
	    } catch ( Exception e ) {e.printStackTrace();}
	    System.err.println();
	}
    }
}
*/
