/**
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** A First-in first-out on the model of the java.util.Stack
 * @since Transmorpher 1.0
 * @see java.util.Stack#Stack()
 * @see java.util.Vector#Vector()
*/

package fr.fluxmedia.transmorpher.utils;

// import java classes
import java.util.Vector;
import java.util.EmptyStackException;

public class Fifo extends Vector<Object> {

    /** The first position at which there is unpoped value */
    private int first = 0;

    /** The last position at which there is no value  */
    private int last = 0; 

    /** The constructor allocates a vector */
    
    /** Creates an empty Fifo
     * @return a new empty Fifo object
     */
    public Fifo() {
	super(1024); // That is arbitrary
    }

    /** Pushes an item onto the top of the Fifo
     * @param item the item to be pushed on the Fifo
     * @return the item argument
     * @see java.util.Vector#addElement(Object)
     */
    public void push(Object item){
	last++;
	addElement(item);
    }

    /** Returns the first element of the Fifo which is withdrawn
     * @return the first element on top of the Fifo
     * @throws java.util.EmptyStackException
     */
    public Object pop() throws EmptyStackException {
	if ( first == last ) throw new EmptyStackException();
	// For garbage collection
	return set(first++,(Object)null);
    }

    /** Returns the first element of the Fifo (which is left unchanged)
     * @return the first element on top of the Fifo
     * @throws java.util.EmptyStackException
     */
    public Object peek() throws EmptyStackException {
	if ( first == last ) throw new EmptyStackException();
	return elementAt(first);
    }

  
    /** Test if the Fifo is empty
     * @return true if and only if the Fifo contains no item; false otherwise
     */
    public boolean isEmpty() {
	return ( first == last );
    }

    /** Returns the number of elements in the Fifo
     * @return the number of elements in the Fifo
     */
    public int size() {
	return ( last-first );
	}

    /** Empties the Fifo
     */
    public void clear() {
	super.clear(); // For garbage collection
	first = 0;
	last = 0;
    }

    /** Empties the Fifo
     */
    public void removeAllElements() {
	super.removeAllElements(); // For garbage collection
	first = 0;
	last = 0;
    }

    /** Returns the 1-based position where the object item is in the Fifo. If the object item accours at an element of the Fifo, this method returns the distance from the bottom of the Fifo of the occurence nearest to the bottom. The bottom item is considered at distance 1. The equals method is used to compare item to the items in this Fifo.
     * @param item the item to be found
     * @return the 1-based position for the begining of the Fifo where the object is located; the return value -1 indicates that the object is not in the Fifo.
     * @see java.util.Vector#addElement(Object)
     */
    public int search(Object item){
	boolean found = false;
	int i;
	for( i = first; !found && i != last; i++ )
	    if( item.equals(elementAt(i)) ) found = true;
	return found?(first-i):-1;
    }

  
} //end class
