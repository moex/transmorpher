/**
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.utils ;

/* Java imports */
import java.io.* ;
import java.util.* ;

public class SystemResources {

  static ResourceBundle vBundle = null;
  static Locale locale = null;
  static Properties vSystemProperties = null;
    static Hashtable<String,String> resource = null;
  
  public SystemResources() {
    resource = new Hashtable<String,String>();
    vBundle = ResourceBundle.getBundle( "user");
    
    //System.out.println("version " +vBundle.getString("LANGUAGE"));
    //System.out.println("version" +vBundle.getString("COUNTRY"));
    locale = new Locale(vBundle.getString("LANGUAGE"),vBundle.getString("COUNTRY"));
    vBundle = ResourceBundle.getBundle( "system",locale);
    for (Enumeration<String> e = vBundle.getKeys() ; e.hasMoreElements() ;) {
      String s = e.nextElement();
      resource.put(s,  vBundle.getString(s));        
    }
    //System.out.println("reader="+vBundle.getString("READER"));
    vSystemProperties = System.getProperties();
    for (Enumeration e = vSystemProperties.keys() ; e.hasMoreElements() ;) {
      String s = (String)e.nextElement();
      resource.put(s,  vSystemProperties.getProperty(s));        
    }
  }
    
  public static void setProperty(String name, String Value){
    resource.put(name,Value);
  }
 
  public static String getProperty(String name){
    return resource.get(name);
  }
  
  public static Locale getLocale() {
    return locale;
  }
    
}
