/**
 * $Id: TMException.java,v 1.1 2002-11-06 14:08:21 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
  * Transmorpher root exception class
  *
  * @author Jerome.Euzenat@inrialpes.fr
  * @since jdk 1.3
  */

package fr.fluxmedia.transmorpher.utils ;

import java.lang.Exception;

public class TMException extends Exception {

    public TMException (String msg){
	super("Transmorpher: "+msg);
    }
}
