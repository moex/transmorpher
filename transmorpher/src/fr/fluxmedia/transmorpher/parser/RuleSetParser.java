/**
 * $Id: RuleSetParser.java,v 1.5 2004-02-24 16:27:06 jerome Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2004 INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.parser ;

// Imported XML classes

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;

//Imported SAX classes

import org.xml.sax.SAXException;
import org.xml.sax.Attributes;

//Imported JAVA classes

import java.io.*;
import java.io.IOException;

import java.util.Stack;
import java.util.Vector;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.lang.Integer;

//Imported Transmorpher classes

import fr.fluxmedia.transmorpher.utils.TMException;
import fr.fluxmedia.transmorpher.utils.Parameters ;
import fr.fluxmedia.transmorpher.graph.Ruleset;
import fr.fluxmedia.transmorpher.graph.RepeatProcess;
import fr.fluxmedia.transmorpher.graph.Transmorpher;
import fr.fluxmedia.transmorpher.graph.Transformation;
import fr.fluxmedia.transmorpher.graph.Namespaced;
import fr.fluxmedia.transmorpher.graph.Call;
import fr.fluxmedia.transmorpher.graph.rules.* ;

public class RuleSetParser extends FMParser{
  private String uri = null;
  private Transmorpher currentTransmorpher = null;
  private Transformation currentTransformation = null;
  private Call currentCall = null;
  private ModTag currentRule = null;
  private Rule rule = null;
  private String currentParam = null;
  private String currentWParam = null;
  private Parameters currentParams = null;

  /** Are we copying some XSLT */
  boolean  inLitteralXSLT = false;
  ProcessParser iMainParser = null;
    
  /** The constructor, build the XML PArser but not parse the document */
  public RuleSetParser(int debug_mode,ProcessParser pMainParser) throws ParserConfigurationException, SAXException {
    super(debug_mode);
    iMainParser = pMainParser ;
  }
   
  public void setTransmorpher (Transmorpher pCurrentTransmorpher) {
    currentTransmorpher= pCurrentTransmorpher ;
  }
  
  
    /* ************* */
    /* START ELEMENT */
    /* ************* */
    
  /** Call by the XML parser at the begining of an element */
  public final void startElement(String namespaceURI, String localName, String qname, Attributes atts) {
    if(debugMode > 2) System.err.println("startElement ProcessParser : " + localName);
    if ( inLitteralXSLT == true ){
    //JE: here we should not do that but put the XSLT in a XSLTTemplate element
    //if(atts!=null) System.out.println(atts.getValue("match"));
    //iXslFile.Writeln(2,"<"+qname+" "+listAttribute(atts)+">");
      } else if (namespaceURI.equals("http://transmorpher.fluxmedia.fr/1.0")) {
	    String[] inChannels = AnalysePortNames(atts.getValue("in"));
	    String[] outChannels = AnalysePortNames(atts.getValue("out"));
      if (localName.equals("with-param")) {
          currentWParam = atts.getValue("name");
          if ( atts.getValue("select") != null ) {
	      if ( currentParams != null )
		  currentParams.setParameter(currentWParam,atts.getValue("select"));
          }
      }  else if (localName.equals("param")) {
          currentParam = atts.getValue("name");
          if ( atts.getValue("select") != null ) {
	      currentParams.setParameter(currentParam,atts.getValue("select"));
          }
      } else if (localName.equals("namespace")) {
          rule = (Rule)new Namespace(atts.getValue("name"),atts.getValue("uri"));
          ((Namespaced)currentTransformation).addNamespace( (Namespace)rule );
      } else if (localName.equals("extends")) {
          rule = (Rule)new Extends(atts.getValue("href"));
          ((Ruleset)currentTransformation).addRule( (Rule)rule );
      } else if (localName.equals("maptag")) {
          rule = (Rule)new MapTag(atts.getValue("match"),atts.getValue("target"),atts.getValue("context"));
          ((Ruleset)currentTransformation).addRule( (Rule)rule );
      } else if (localName.equals("addtag")) {
          rule = (Rule)new AddTag(atts.getValue("match"),atts.getValue("context"));
	  if ( currentRule == null ) { //Not valid per DTD
              ((Ruleset)currentTransformation).addRule( (Rule)rule );
          } else {
	      currentRule.addRule( (Rule)rule );
          }
      } else if (localName.equals("remtag")) {
          rule = (Rule)new RemTag(atts.getValue("match"),atts.getValue("context"));
          ((Ruleset)currentTransformation).addRule( (Rule)rule );
      } else if (localName.equals("flatten")) {
          rule = (Rule)new Flatten(atts.getValue("match"));
          ((Ruleset)currentTransformation).addRule( (Rule)rule );
      } else if (localName.equals("modtag")) {
          rule = new ModTag(atts.getValue("match"),atts.getValue("target"),atts.getValue("context"));
          ((Ruleset)currentTransformation).addRule( rule );
          currentRule = (ModTag)rule;
      } else if (localName.equals("mapatt")) {
          rule = (Rule)new MapAtt(atts.getValue("match"),atts.getValue("target"),atts.getValue("context"));
          if ( currentRule == null ) {
              ((Ruleset)currentTransformation).addRule( (Rule)rule );
          } else {
        currentRule.addRule( (Rule)rule );
          }
      } else if (localName.equals("defatt")) {
          rule = (Rule)new DefAtt(atts.getValue("match"),atts.getValue("value"),atts.getValue("context"));
          if ( currentRule == null ) {
	      // This should not be possible
              ((Ruleset)currentTransformation).addRule( (Rule)rule );
          } else {
        currentRule.addRule( (Rule)rule );
          }
      } else if (localName.equals("rematt")) {
          rule = (Rule)new RemAtt(atts.getValue("match"),atts.getValue("context"));
          if ( currentRule == null ) {
              ((Ruleset)currentTransformation).addRule( (Rule)rule );
          } else {
        currentRule.addRule( (Rule)rule );
          }
      } else if (localName.equals("addatt")) {
          rule = (Rule)new AddAtt(atts.getValue("match"),atts.getValue("value"));
          if ( currentRule == null ) {
              ((Ruleset)currentTransformation).addRule( (Rule)rule );
          } else {
	      currentRule.addRule( (Rule)rule );
          }
      } else if (localName.equals("resubst")) {
          rule = (Rule)new RESubst(atts.getValue("match"),atts.getValue("context"),
                                   atts.getValue("source"),atts.getValue("target"),atts.getValue("apply"));
          ((Ruleset)currentTransformation).addRule( (Rule)rule );
      } else if (localName.equals("ruleset")) {
          currentTransformation = new Ruleset(atts.getValue("name"),currentTransmorpher);
          currentTransmorpher.addTransformation(atts.getValue("name"),currentTransformation);
	        currentParams = currentTransformation.getParameters();
      } else {
	  System.err.println("[RuleSetParser] Unknown element : " + localName);
	  // JE: this is impossible because of SAX
	  // throw new TMException("[RuleSetParser] Unknown element : " + localName);
      } //end if
     
    } else if (namespaceURI.equals("http://www.w3.org/1999/XSL/Transform") && localName.equals("template")) {
      rule = (Rule)new XSLTTemplate(atts.getValue("match"),atts.getValue("mode"));
      ((Ruleset)currentTransformation).addRule( (Rule)rule );
      inLitteralXSLT = true;
      // JE: Copy the structure or the sax events?
      // JE: Possibility to write in a string?
      //iXslFile.Writeln(2,"<"+qname+" "+listAttribute(atts)+" >");
    } else {
	System.err.println("[RuleSetParser] Unknown namespace ("+namespaceURI+") for "+qname);
	// JE: this is impossible because of SAX
	//throw new TMException("[RuleSetParser] Unknown namespace ("+namespaceURI+") for "+qname);
    }
  } // startElement(String namespaceURI, String localName, String qname, Attributes atts)
    

  /* The content of the tags */
  /* No content in Transmorpher so far but in XSLT code */
  
  public void characters(char ch[], int start, int length) {
  
    String content = "";
    int end = start + length;
  
    for( int i = start; i < end ; i++ ) content = content + ch[i];
    if(debugMode > 2) System.err.println("Content : " + content);
  
    if( inLitteralXSLT == true ) {
      // JE: to be put in the corresponding string
      //iXslFile.Writeln(2,content);
      } else if ( currentParam != null ) {
        currentParams.setParameter(currentParam,content);
      } else if ( currentWParam != null ) {
        currentParams.setParameter(currentWParam,content);
      }
    }

  /* *********** */
  /* END ELEMENT */
  /* *********** */

  /** Call by the XML parser at the end of an element */
  public final void endElement (String namespaceURI,String pName, String qName ) {
      /* try { */
  
  if( inLitteralXSLT == true ){
      //iXslFile.Writeln(2,"</"+qName+">");
      if( namespaceURI.equals("http://www.w3.org/1999/XSL/Transform") && pName.equals("template") )
        inLitteralXSLT = false;
  } else
    if(namespaceURI.equals("http://transmorpher.fluxmedia.fr/1.0")) {
      if(debugMode > 2) System.err.println("endElement RulesetParser : " + pName);
      if (pName.equals("param")) {
	        currentParam = null;
      } else if (pName.equals("with-param")) {
	        currentWParam = null;
      } else if (pName.equals("modtag")) {
        currentRule = null;
      } else if (pName.equals("maptag")) {
      } else if (pName.equals("remtag")) {
      } else if (pName.equals("flatten")) {
      } else if (pName.equals("mapatt")) {
      } else if (pName.equals("rematt")) {
      } else if (pName.equals("addatt")) {
      } else if (pName.equals("addtag")) {
      } else if (pName.equals("resubst")) {
      } else if (pName.equals("extends")) {
      } else if (pName.equals("query")) {
         currentTransformation = null;
      } else if (pName.equals("ruleset")) {
        currentTransformation = null;
        iMainParser.setEndRuleSet();
      } //end if

    } else {
	System.err.println("[RulesetParser] Unknown namespace : "+namespaceURI);
	// JE: this is impossible because of SAX
	//throw new TMException("[RulesetParser] Unknown namespace : "+namespaceURI);
    }
   } //end endElement
    

    /*---------------------------- ANALYSE PORT NAME -----------------------------------------*/
    
  /** Analyze the port Name attributes */
  public static final String[] AnalysePortNames(String pListe) {
      if ( pListe != null ) {
  // JE: I would like to add "," in the tokenizer (possible?).
    StringTokenizer vTokens = new StringTokenizer( pListe, " ", true );
    String[] vAns = new String[vTokens.countTokens()/2+1];
    String vToken = null;
    int i = 0;
    while ( vTokens.hasMoreTokens() ) {
      vToken = vTokens.nextToken();
      if (vToken.equals(" ")) {
      } else {
        vAns[i++]=vToken;
      } //end if
    }//end while
    return vAns;
      } else {
	  return null;
      }
  } //end proc
    
    /** Reset the parameters as they were after taking care of a Call */
    private void cleanStack(){
	currentParams = currentCall.getProcess().getParameters();
	if ( (currentTransformation != null) && (currentTransformation instanceof RepeatProcess) ){
	    currentCall = ((RepeatProcess)currentTransformation).getCaller();
	} else {
	    currentCall = null;
	}
    }
    
    /*------------------------------------ LIST ATTRIBUTE -----------------------------------*/

    /** Returns a XML-attribute string from the result of such a string */
    // Used only for transmitting litteral XSLT Templates
    public final String listAttribute(Attributes atts){
  String listAttribute="";
  if (atts.getLength()!=0)
      {
    for(int i=0 ; i<atts.getLength() ; i++){
        listAttribute=listAttribute+atts.getLocalName(i)+"=\""+atts.getValue(i)+"\" ";
    }
    
      }
  
      return listAttribute;
    }
    
}//end class
    

