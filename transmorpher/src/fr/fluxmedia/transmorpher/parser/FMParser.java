/*
 * $Id: FMParser.java,v 1.3 2003-01-31 15:23:47 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.parser;
//Imported Transmorpher Classes
import fr.fluxmedia.transmorpher.graph.Transmorpher;
//Imported JAVA Classes
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Stack;
//Imported JAXP Classes
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
// Imported SAX Classes
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLFilterImpl;

/**
 * Define a parser for XML Document
 *
 *@Author  : Laurent Tardif
 *Date    : 05 01 2001
 *Content : Define a parser for XML Document
 */
public class FMParser extends DefaultHandler {

	/**
	 * XML Parser
	 */
	protected SAXParser parser = null;

	/**
	 * The current context of the parsing
	 */
	protected Stack iContext = null;

	/**
	 * The output for a servlet
	 */
	protected PrintWriter output = null;

	/**
	 *	the mode validating or not
	 */
	protected int debugMode = 0;

	/*------------------------------------------ CONSTRUCTOR ----------------------------- */
	/**
	 * The constructor, build the XML Parser but not parse the document
	 *
	 *@param  debugMode                        	value of the debug mode
	 *@exception  ParserConfigurationException  Description of the Exception
	 *@exception  SAXException                  Description of the Exception
	 */
	public FMParser(int debugMode) throws ParserConfigurationException, SAXException {
		this.debugMode = debugMode;
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();
		if (debugMode > 0) {
			parserFactory.setValidating(true);
		} else {
			parserFactory.setValidating(false);
		}
		parserFactory.setNamespaceAware(true);
		parser = parserFactory.newSAXParser();
	}

	/*------------------------------------------- END CONSTRUCTOR ------------------------ */
	/**
	 * Parse the document corresponding to the URI given in parameter
	 *
	 *@param  uri               the URI of the document to parse
	 *@return                   a graph structure
	 *@exception  SAXException  
	 *@exception  IOException   sends when an IO errors occurs
	 */
	public Transmorpher newParse(String uri) throws SAXException, IOException {
		parser.parse(uri, this);
		return new Transmorpher(debugMode);
	}

	/**
	 * This method is called when a warnig occurs during the parsing.
	 * A message with informations on the warning is printed on sdterr
	 *
	 *@param  exception  Description of the Parameter
	 */
	public void warning(SAXParseException exception) {
		System.err.println("**Warning**\n"+
												" URI : "+exception.getSystemId()+"\n"+
												" Line : "+exception.getLineNumber()+"\n" +
												" Column : "+exception.getColumnNumber()+"\n" +
												" Message :"+exception.getMessage()+"\n" +
												"*************");
	}

	/**
	 *  This method is called when an error occurs during the parsing.
	 *  A message with informations on the error is printed on sdterr
	 *
	 *@param  exception  Description of the Parameter
	 */
	public void error(SAXParseException exception) {
		System.err.println("**Error**\n"+
												" URI : "+exception.getSystemId()+"\n"+
												" Line : "+exception.getLineNumber()+"\n" +
												" Column : "+exception.getColumnNumber()+"\n" +
												" Message : "+exception.getMessage()+"\n" +
												"*************");
	}

	/**
		*  This method is called when an fatal error occurs during the parsing.
		*  A message with informations on the fatal error is printed on sdterr
		*  Transmorpher is stopped when this kind of errors occurs.
		*
		*@param  exception         Description of the Parameter
		*@exception  SAXException  Description of the Exception
		*/
	public void fatalError(SAXParseException exception) throws SAXException {
			System.err.println("**Fatal Error**\n"+
												" URI : "+exception.getSystemId()+"\n"+
												" Line : "+exception.getLineNumber()+"\n" +
													" Column : "+exception.getColumnNumber()+"\n" +
												" Message : "+exception.getMessage()+"\n"+
												"*************");
		 throw new SAXException("Fatal Error occured during the process parsing");
	}
}

