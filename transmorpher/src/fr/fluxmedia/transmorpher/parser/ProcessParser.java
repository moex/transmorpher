/*
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.parser ;

// Imported XML classes
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;

//Imported SAX classes
import org.xml.sax.SAXException;
import org.xml.sax.Attributes;

//Imported JAVA classes
import java.io.*;
import java.io.IOException;
import java.lang.Integer;
import java.util.Observer;
import java.util.Stack;
import java.util.Vector;
import java.util.Enumeration;
import java.util.ListIterator;
import java.util.StringTokenizer;
import java.util.Observer;
import java.util.Hashtable;

//Imported Transmorpher classes
import fr.fluxmedia.transmorpher.utils.Parameters ;
import fr.fluxmedia.transmorpher.utils.* ;
import fr.fluxmedia.transmorpher.graph.* ;
import fr.fluxmedia.transmorpher.graph.rules.* ;

/**
 * This class allows the creation of a parser for a Transmorpher process.
 * Methods are provided to create a graph corresponding to the process
 * defined in the transmorpher language.
 */
public class ProcessParser extends FMParser {
	
		/**
		 * an uri to a process
		 */
    protected String uri = null;
		/**
		 * a buffer
		 */
    protected String tmp = "";
		/**
		 * the current transmorpher in creation
		 */
    protected Transmorpher currentTransmorpher = null;
		/**
		 * the current transformation in creation
		 */
    protected Transformation currentTransformation = null;
		/**
		 * the current call in creation
		 */
    protected Call currentCall = null;
		/**
		 * the current rule in creation
		 */
    protected ModTag currentRule = null;
		/**
		 * a rule
		 */
    protected Rule rule = null;
		/**
		 * the name of the current parameter to parse
		 */
    protected String currentParam = null;
		/**
		 * the name of the current with-param to parse
		 */
    protected String currentWParam = null;
		/**
		 * the current iterator to parse
		 */
    protected Iterator currentIterator = null;
		/**
		 * the current parameters
		 */
    protected Parameters currentParams = null;
		/**
		 * to store imported processes
		 */
    protected Hashtable<String,Transmorpher> imports = null;
		/**
		 * 
		 */
    protected Stack<Transmorpher> tStack = null;
    
		/**
		 * for the GUI (flowcomposer)
		 */
    protected Object[] observers;
    /**
		 * a parser for the ruleset
		 */
    protected RuleSetParser ruleSetParser = null ;
		/**
		 * a parser for the query
		 */
    protected QueryParser queryParser = null ;
    
    /** 
			*Are we copying some XSLT 
			*/
    protected boolean  inLitteralXSLT = false;
		/**
		 * If true , parser is in a ruleset part of the process
		 */
    protected boolean inRuleSet = false ;
		/**
		 * If true , parser is in a query part of the process
		 */
    protected boolean inQuery = false ;
    
  /*-------------------------------------------- CONSTRUCTOR  -----------------------------*/
    
		/** 
			* Creates an XML Parser.
			* @param debugMode The value of the debug mode
			*/
		public ProcessParser( int debugMode) throws ParserConfigurationException, SAXException {
			this(debugMode,null);
			imports = new Hashtable<String,Transmorpher>(15,5);
			tStack = new Stack<Transmorpher>();
		}
    
		/** 
			* Creates an XML Parser. This constructor is used by Flowcomposer only.
			* @param debugMode The value of the debug mode
			* @param observers the observers for flowcomposer
			*/
		public ProcessParser(int debugMode, Object[] observers) throws ParserConfigurationException, SAXException {
			super(debugMode);
			ruleSetParser = new RuleSetParser(debugMode,this);
			queryParser = new QueryParser(debugMode,this);
			this.observers = observers;
			imports = new Hashtable<String,Transmorpher>(15,5);
			tStack = new Stack<Transmorpher>();
		}

    
  /*--------------------------------------------- END CONSTRUCTOR -------------------------*/

		/** 
		 * Parses the document corresponding to the URI given in parameter
		 * If the current process has links (import or include) to others documents then they are 
		 * parsed.
		 * @param uri URI of the document to parse
		 */
		public Transmorpher newParse(String uri) throws SAXException, IOException {
			this.uri = uri;
			parser.parse(uri,this);
			// Deal with imported Transmorphers:
			// 1) stack currentTransmorpher
			tStack.push(currentTransmorpher);
			// 2) for all imports: 
			for ( Object o: currentTransmorpher.getImports() ) {
				String currentURI = (String)o;
				if ( imports.get( currentURI ) != null ){
					tStack.peek().recordImport(currentURI, imports.get( currentURI ));
				} else {
					//    3) call Newparse on them
					currentTransmorpher = newParse( currentURI );
					//    4) put them in the import of the Transmorpher
					tStack.peek().recordImport(currentURI, currentTransmorpher );
					imports.put( currentURI, currentTransmorpher );
				}
			}
			currentTransmorpher = tStack.pop();
			return currentTransmorpher;
		}
    
		
		////////////////// CALLBACKS /////////////////
		
    /* ************* */
    /* START ELEMENT */
    /* ************* */
    
  /** 
		* Called by the XML parser at the begining of an element.
		* The corresponing graph component is create for each element.
		*
		* @param namespaceURI 	The namespace of the current element
		* @param localName 			The local name of the current element
		* @param qname					The name of the current element 
		* @param atts 					The attributes name of the current element 
		*/
		public  void startElement(String namespaceURI, String localName, String qname, Attributes atts) {
			if(debugMode > 2) 
				System.err.println("startElement ProcessParser : " + localName);
			if (inRuleSet) {
				ruleSetParser.startElement(namespaceURI,localName,qname,atts);
			} 
			else 
				if (inQuery) {
					queryParser.startElement(namespaceURI,localName,qname,atts);
				} 
				else 
					if ( inLitteralXSLT ){
					//JE: here we should not do that but put the XSLT in a XSLTTemplate element
					//if(atts!=null) System.out.println(atts.getValue("match"));
					//iXslFile.Writeln(2,"<"+qname+" "+listAttribute(atts)+">");
					}
					else 
						if (namespaceURI.equals("http://transmorpher.fluxmedia.fr/1.0")) {
							String[] inChannels = AnalysePortNames(atts.getValue("in"));
							String[] outChannels = AnalysePortNames(atts.getValue("out"));
							if(debugMode > 2) 
								System.err.println("startElement ProcessParser : "+ localName);
							if (localName.equals("with-param")) {
								currentWParam = atts.getValue("name");
								if ( atts.getValue("select") != null ) {
									if ( currentParams != null )
										currentParams.setParameter(currentWParam,atts.getValue("select"));
								}
							}  else 
								if (localName.equals("param")) {
									currentParam = atts.getValue("name");
									if ( atts.getValue("select") != null ) {
										currentParams.setParameter(currentParam,atts.getValue("select"));
									}
								} else 
								/*/*************** GENERATE **************/
									if (localName.equals("generate")) {
										currentCall = (Call)new Generate(atts.getValue("id"),atts.getValue("type"),
																						(fr.fluxmedia.transmorpher.graph.Process)currentTransformation,
																						outChannels.length,
																						atts.getValue("file"));	// Compatibility
										fixOutPorts( currentCall, currentCall.outPorts(), outChannels, 1, "generate");
										currentParams = currentCall.getParameters();
										((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).addCall((Call)currentCall);
								} else 
								/*/*************** SERIALIZE **************/
									if (localName.equals("serialize")) {
										currentCall = (Call)new Serialize(atts.getValue("id"),atts.getValue("type"),
																						(fr.fluxmedia.transmorpher.graph.Process)currentTransformation,
																						inChannels.length,
																						atts.getValue("file"));// Compatibility
										fixInPorts( currentCall, currentCall.inPorts(), inChannels, 1, "serialize");
										currentParams = currentCall.getParameters();
										((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).addCall((Call)currentCall);
								} else
								/*/*************** DISPATCH **************/
									if (localName.equals("dispatch")) {
										currentCall = (Call)new Dispatch(atts.getValue("id"),atts.getValue("type"),
																						(fr.fluxmedia.transmorpher.graph.Process)currentTransformation,
																						outChannels.length);								
										fixInPorts( currentCall, currentCall.inPorts(), inChannels, 1, "dispatch");
										fixOutPorts( currentCall, currentCall.outPorts(), outChannels, -1, "dispatch");
										currentParams = currentCall.getParameters();
										((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).addCall((Call)currentCall);
								} else
								/*/*************** MERGE **************/
									if (localName.equals("merge")) {
										currentCall = (Call)new Merge(atts.getValue("id"),atts.getValue("type"),
																						(fr.fluxmedia.transmorpher.graph.Process)currentTransformation,
																						inChannels.length);
										fixInPorts( currentCall, currentCall.inPorts(), inChannels, -1, "merge");
										fixOutPorts( currentCall, currentCall.outPorts(), outChannels, 1, "merge");
										currentParams = currentCall.getParameters();
										((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).addCall((Call)currentCall);
								} else
								/*/*************** APPLY-EXTERNAL **************/
									if (localName.equals("apply-external")) {
										currentCall = (Call)new ApplyExternal(atts.getValue("id"),atts.getValue("type"),
																							atts.getValue("file"),
																							(fr.fluxmedia.transmorpher.graph.Process)currentTransformation,
																							inChannels.length,
																							outChannels.length);
										fixInPorts( currentCall, currentCall.inPorts(), inChannels, -1, "apply-external");
										fixOutPorts( currentCall, currentCall.outPorts(), outChannels, -1, "apply-external");
										currentParams = currentCall.getParameters();
										((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).addCall((Call)currentCall);
								} else
								/*/*************** APPLY-PROCESS **************/
									if (localName.equals("apply-process")) {
										currentCall = (Call)new ApplyProcess(atts.getValue("id"),atts.getValue("type"),
																							(fr.fluxmedia.transmorpher.graph.Process)currentTransformation,
																							atts.getValue("ref"),
																							inChannels.length,
																							outChannels.length);
										fixInPorts( currentCall, currentCall.inPorts(), inChannels, -1, "apply-process");
										fixOutPorts( currentCall, currentCall.outPorts(), outChannels, -1, "apply-process");
										currentParams = currentCall.getParameters();
										((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).addCall((Call)currentCall);
								} else
								/*/*************** APPLY-QUERY **************/
									if (localName.equals("apply-query")) {
										currentCall = (Call)new ApplyQuery(atts.getValue("id"),atts.getValue("type"),
																							(fr.fluxmedia.transmorpher.graph.Process)currentTransformation,
																							atts.getValue("ref"));
										fixInPorts( currentCall, currentCall.inPorts(), inChannels, 1, "apply-query");
										fixOutPorts( currentCall, currentCall.outPorts(), outChannels, 1, "apply-query");
										currentParams = currentCall.getParameters();
										((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).addCall((Call)currentCall);
								} else
										/*/*************** APPLY-RULESET **************/
									if (localName.equals("apply-ruleset")) {
										currentCall = (Call)new ApplyRuleset(atts.getValue("id"),"Ruleset",
																							(fr.fluxmedia.transmorpher.graph.Process)currentTransformation,
																							atts.getValue("ref"));
										fixInPorts( currentCall, currentCall.inPorts(), inChannels, 1, "apply-ruleset");
										fixOutPorts( currentCall, currentCall.outPorts(), outChannels, 1, "apply-ruleset");
										((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).addCall((Call)currentCall);
										currentParams = currentCall.getParameters();
								} else
									/*/*************** REPEAT *********************/
									if (localName.equals("repeat")) {
										String[] bufChannels = AnalysePortNames(atts.getValue("bin"));
										// Repeat is both Call and Process
										// First, Call
										currentCall = (Call)new Repeat(atts.getValue("id"),
																							(fr.fluxmedia.transmorpher.graph.Process)currentTransformation,
																							inChannels.length,
																							outChannels.length,
																							bufChannels.length);
										fixInPorts( currentCall, currentCall.inPorts(), inChannels, -1, "repeat");
										fixOutPorts( currentCall, currentCall.outPorts(), outChannels, -1, "repeat");
										((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).addCall((Call)currentCall);
										// then Process
										currentTransformation = ((Repeat)currentCall).getProcessBody();
										fixInPorts( currentTransformation, currentTransformation.inPorts(), inChannels);
										fixBuf( currentTransformation, ((Repeat)currentCall).inBuf(),currentTransformation.inPorts(), bufChannels, inChannels.length);
										fixOutPorts( currentTransformation, currentTransformation.outPorts(), outChannels);
									
										currentParams = currentCall.getParameters();
									} else
									/*/*************** ITERATE *******************/
										if (localName.equals("iterate")) {
											currentIterator = (Iterator)new Iterator(atts.getValue("name"),atts.getValue("type"),
											(Repeat)currentCall);
											((Repeat)currentCall).addIterator(currentIterator);
											currentParams = currentIterator.getParameters();
									} else
									/*/*************** PROCESS *******************/
									 	if (localName.equals("process")) {
											currentTransformation = new fr.fluxmedia.transmorpher.graph.Process(atts.getValue("name"),
																										currentTransmorpher,
																										inChannels.length,outChannels.length);
											fixInPorts( currentTransformation, currentTransformation.inPorts(), inChannels);
											fixOutPorts( currentTransformation, currentTransformation.outPorts(), outChannels);
											currentTransmorpher.addTransformation(atts.getValue("name"),currentTransformation);
											currentParams = currentTransformation.getParameters();
									} else
									/*/*************** QUERY *********************/
										if (localName.equals("query")) {
											inQuery = true ;
											queryParser.setTransmorpher (currentTransmorpher);
											queryParser.startElement(namespaceURI,localName,qname,atts);
									} else
									/*/*************** RULESET *******************/
										if (localName.equals("ruleset")) {
											inRuleSet = true ;
											ruleSetParser.setTransmorpher (currentTransmorpher);
											ruleSetParser.startElement(namespaceURI,localName,qname,atts);
								 } else
								 /*/*************** TRANSMORPHER **************/
										if (localName.equals("transmorpher")) {
											currentTransmorpher = new Transmorpher(atts.getValue("name"),atts.getValue("version"),
																									debugMode,
																									atts.getValue("reloc"),
																									( (atts.getValue("optimized") != null) && !(atts.getValue("optimized").equals("true"))) );
											currentTransmorpher.setFile( new File(uri) );
											// add observer to transmorpher
											if(observers != null){
											    for( Object o: observers ){
													if( o instanceof Observer )
														currentTransmorpher.addObserver( (Observer)o );
												}
											}
									} else
									/*/*************** DEFEXTERN *****************/
										if (localName.equals("defextern")) {
											currentTransmorpher.addExtern(atts.getValue("name"),atts.getValue("class"),
																								atts.getValue("implements"),
																								((atts.getValue("default") != null) && !(atts.getValue("default").equals("true"))));
									} else
									/*/*************** INCLUDE *****************/
										if (localName.equals("include")) {
											currentTransmorpher.addInclude(atts.getValue("href"));
									} else 
									/*/*************** IMPORT **************/
										if (localName.equals("import")) {
											currentTransmorpher.reserveImport(atts.getValue("href"));
									} else
									/*/*************** MAIN **************/
										if (localName.equals("main")) {
											currentTransformation = (fr.fluxmedia.transmorpher.graph.Process)new Main(atts.getValue("name"),currentTransmorpher);
											if ( currentTransmorpher.getMain() == null) {
												currentTransmorpher.setMain((MainProcess)currentTransformation);
											} else {
												System.err.println("Error: no two main in transmorpher stylesheet");
												// JE: Impossible because of SAX
												//throw new TMException("[ProcessParser] no two main in transmorpher stylesheet");
											}
										currentTransmorpher.addTransformation(atts.getValue("name"),(Transformation)currentTransformation);
										currentParams = currentTransformation.getParameters();
									} else
									/*/*************** SERVLET **************/
										if (localName.equals("servlet")) {
											currentTransformation = (fr.fluxmedia.transmorpher.graph.Process)new Servlet(atts.getValue("name"),currentTransmorpher);
											if ( currentTransmorpher.getMain() == null) {
												currentTransmorpher.setMain((MainProcess)currentTransformation);
											} else {
												System.err.println("Error: no two main in transmorpher stylesheet");
											// JE: Impossible because of SAX
											//throw new TMException("[ProcessParser] no two main in transmorpher stylesheet");
											}
										currentTransmorpher.addTransformation(atts.getValue("name"),(Transformation)currentTransformation);
										currentParams = currentTransformation.getParameters();
									} else
									/*/*************** TRANSFORMER **************/
										if (localName.equals("transformer")) {
											currentTransformation = (fr.fluxmedia.transmorpher.graph.Process)new Transformer(atts.getValue("name"),currentTransmorpher);
											if ( currentTransmorpher.getMain() == null) {
												currentTransmorpher.setMain((MainProcess)currentTransformation);
											} else {
												System.err.println("Error: no two main in transmorpher stylesheet");
												// JE: Impossible because of SAX
												//throw new TMException("[ProcessParser] no two main in transmorpher stylesheet");
											}
										currentTransmorpher.addTransformation(atts.getValue("name"),(Transformation)currentTransformation);
										currentParams = currentTransformation.getParameters();
									} else {
										System.err.println("[ProcessParser] Unknown element : " + localName);
										// JE: Impossible because of SAX
										//throw new TMException("[ProcessParser] Unknown element : " + localName);
										} //end if
							} else 
								if (namespaceURI.equals("http://www.w3.org/1999/XSL/Transform") && localName.equals("template")) {
									rule = (Rule)new XSLTTemplate(atts.getValue("match"),atts.getValue("mode"));
									((Ruleset)currentTransformation).addRule( (Rule)rule );
									inLitteralXSLT = true;
									// JE: Copy the structure or the sax events?
									// JE: Possibility to write in a string?
									//iXslFile.Writeln(2,"<"+qname+" "+listAttribute(atts)+" >");
								} else {
									System.err.println("[ProcessParser] Unknown namespace (" + namespaceURI+") for "+qname);
									// JE: Impossible because of SAX
									//throw new TMException("[ProcessParser] Unknown namespace (" + namespaceURI+") for "+qname);
								}
		} // startElement(String namespaceURI, String localName, String qname, Attributes atts)

		/**
		 * This method is used to get data of an element
		 * Data are stored in a buffer (tmp)
		 */
		public void characters(char ch[], int start, int length) {
		
			String content = tmp+"";
			int end = start + length;
			for( int i = start; i < end ; i++ ) 
				content = content + ch[i];
			if(debugMode > 2) System.err.println("Content : " + content);
			if( inLitteralXSLT == true ) {
				// JE: to be put in the corresponding string
				//iXslFile.Writeln(2,content);
			} 
			tmp=content;
		}


  /* *********** */
  /* END ELEMENT */
  /* *********** */

		/**
		 * Sets the inRuleset flag to false.
		 */
		public void setEndRuleSet() {
			 inRuleSet =false ; 
		}
	
		/**
		 * Sets the inQuery flag to false.
		 */
		public void setEndQuery() {
			 inQuery =false ; 
		}

		/** 
			* Called by the XML parser at the end of an element.
			*
			* @param namespaceURI 	The namespace of the current element
			* @param localName 			The local name of the current element
			* @param qname					The name of the current element 
			*/
		public  void endElement(String namespaceURI,String pName, String qName ) {
			if(debugMode > 2) 
				System.err.println("endElement ProcessParser : " + pName);
			if (inRuleSet) {
				ruleSetParser.endElement(namespaceURI,pName,qName);
			} else if (inQuery) {
				queryParser.endElement(namespaceURI,pName,qName);
			} else if( inLitteralXSLT ){
				//iXslFile.Writeln(2,"</"+qName+">");
			
			if( namespaceURI.equals("http://www.w3.org/1999/XSL/Transform") && pName.equals("template") )
				inLitteralXSLT = false;
			} else if(namespaceURI.equals("http://transmorpher.fluxmedia.fr/1.0"))  {
				if (pName.equals("param")) {
					if( currentParam != null ) {
						currentParams.setParameter(currentParam,tmp);
						tmp="";
					} else if ( currentWParam != null ) {
						currentParams.setParameter(currentWParam,tmp);
						tmp="";
					}
					currentParam = null;
				} else if (pName.equals("with-param")) {
					if( currentParam != null ) {
						currentParams.setParameter(currentParam,tmp);
						tmp="";
					} else if ( currentWParam != null ) {
						currentParams.setParameter(currentWParam,tmp);
						tmp="";
					}
					currentWParam = null;
				} else if (pName.equals("generate")) {
					cleanStack();
				} else if (pName.equals("serialize")) {
					cleanStack();
				} else if (pName.equals("dispatch")) {
					cleanStack();
				} else if (pName.equals("merge")) {
					cleanStack();
				}	else if (pName.equals("defextern")) {
				}else if (pName.equals("import")) {
				}else if (pName.equals("apply-external")) {
					cleanStack();
				} else if (pName.equals("apply-process")) {
					cleanStack();
				} else if (pName.equals("apply-query")) {
					cleanStack();
				} else if (pName.equals("apply-ruleset")) {
					cleanStack();
				} else if (pName.equals("repeat")) {
					checkChannels( (fr.fluxmedia.transmorpher.graph.Process)currentTransformation );
					currentTransformation = currentCall.getProcess();
					currentParams = currentTransformation.getParameters();
					cleanStack();
				} else if (pName.equals("iterate")) {
					currentCall = currentIterator.getRepeat();
					currentParams = currentCall.getParameters();
				} else if (pName.equals("extends")) {
				} else if (pName.equals("process")) {
					checkChannels( (fr.fluxmedia.transmorpher.graph.Process)currentTransformation );
					((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).setUp();
					currentTransformation = null;
				} else if (pName.equals("main")) {
					checkChannels( (fr.fluxmedia.transmorpher.graph.Process)currentTransformation );
					((Main)currentTransformation).setUp();
					currentTransformation = null;
				} else if (pName.equals("transformer")) {
					checkChannels( (fr.fluxmedia.transmorpher.graph.Process)currentTransformation );
					((Transformer)currentTransformation).setUp();
					currentTransformation = null;
				} else if (pName.equals("servlet")) {
					checkChannels( (fr.fluxmedia.transmorpher.graph.Process)currentTransformation );
					((Servlet)currentTransformation).setUp();
					currentTransformation = null;
				} else if (pName.equals("transmorpher")) {
				} else {
					System.err.println("[ProcessParser] Unknown element : " + pName);
					// JE: Impossible because of SAX
					//throw new TMException("[ProcessParser] Unknown element : " + pName);
				} //end if
			}
			else {
				System.err.println("[ProcessParser] Unknown namespace : "+namespaceURI);
				// JE: Impossible because of SAX
				//throw new TMException("[ProcessParser] Unknown namespace : "+namespaceURI);
			}
  
     } //end endElement
    
		
    /*--------------------- CREATE THE PORTS FROM THE ARGUMENT LIST ----------------------------------*/

    // JE: Concerning transformations, everything should be simpler because nothing
    // is created yet so we just have to register. But, OK.
    // This is only used for processes!!i
    // FT: if we are in repeat transformation, all INPUTs are FeedBacks not Channels
		/**
		*	Creates the in ports of a transformation.
		* 
		*@param t 	The transformation
		*@param list The port list which store the in ports
		*@param portNames The names of the ports to create
		*/
		protected void fixInPorts( Transformation t, PortList list, String[] portNames) {
			for( int i = 0; i != portNames.length; i++) {
			list.setPort(i, new Port( portNames[i], (Transformation)t, i,Port.INPUT ));
			Port p=list.getPort(i);
									 if (t instanceof RepeatProcess){
														String name = p.getName();
														if ( (name.equals("_null_") || name.equals("_stdout_") || name.equals("_stdin_")) ) {
							System.err.println("[ProcessParser] cannot use "+name+" in repeat buffers");
							// JE: Impossible because of SAX
							//throw new TMException("[ProcessParser] cannot use "+name+" in repeat buffers");
														} else {
																		FeedBack f=null;
																		if (((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).getChannel( name )instanceof FeedBack)
																						f = (FeedBack)((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).getChannel( name );
																		if ( f == null ) {
																						f = new FeedBack( name, (fr.fluxmedia.transmorpher.graph.Process)currentTransformation );
																						((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).addChannel( (Channel)f );
																		}
																		f.setIn( p );
																		p.setChannel( (Channel)f );
		
														}
										}
			}
			registerChannelIn( list, false );
		}

		/**
		*	Creates the out ports of a transformation.
		* 
		*@param t 	The transformation
		*@param list The port list which store the out ports
		*@param portNames The names of the ports to create
		*/
		protected void fixOutPorts( Transformation t, PortList list, String[] portNames) {
			for( int i = 0; i != portNames.length; i++) {
					list.setPort(i, new Port( portNames[i], (Transformation)t, i,Port.OUTPUT ));
			}
			registerChannelOut( list, false );
		}
		/**
		*	Creates the buffered ports of a transformation.
		* 
		*@param t 	The transformation
		*@param bufList The port list which store the buffered ports
		*@param inList The port list which store the in ports
		*@param portNames The names of the ports to create
		*@param expected The required number of ports  
		*/
		protected void fixBuf(Transformation t , PortList bufList,PortList inList, String[] portNames, int expected) {
			if ( portNames.length != expected ) {
				System.err.println("[ProcessParser] Repeat must have as many ("+expected+") buffers as input (given "+portNames.length+")");
				// JE: impossible because of SAX
				//throw new TMException("[ProcessParser] Repeat must have as many ("+expected+") buffers as input (given "+portNames.length+")");
			} else {	
				for( int i = 0; i != portNames.length; i++) {
					bufList.setPort(i, new Port( portNames[i],t , i));
					Port p = bufList.getPort(i);
					Port inP = inList.getPort(i);
		
					String name = p.getName();
					String inName= inP.getName();
					if ( (name.equals("_null_") || name.equals("_stdout_") || name.equals("_stdin_")) ) {
						System.err.println("[ProcessParser] cannot use "+name+" in repeat buffers");
						// JE: impossible because of SAX
						//throw new TMException("[ProcessParser] cannot use "+name+" in repeat buffers");
					} else {
							FeedBack buf=null;
							FeedBack in=null;
							if (((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).getChannel( name )instanceof FeedBack){
								buf = (FeedBack)((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).getChannel( name );
								buf.setOut(p); //innerOUT
							}
							else{
								buf = new FeedBack( name, (fr.fluxmedia.transmorpher.graph.Process)currentTransformation );
								((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).addChannel( (Channel)buf );
								buf.setOut(p); //set innerOutput
							}
							buf.setIn( p );
							p.setChannel( (Channel)buf );
							in  = (FeedBack)((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).getChannel( inName );
							in.setIn(p);
						}
					}
				}
			}
    
		/**
		*	Creates the out buffered ports of a repeat.
		* 
		*@param r 	The repeat 
		*@param list The port list which store the buffered ports
		*@param inList The port list which store the in ports
		*@param portNames The names of the ports to create
		*@param expected The required number of ports  
		*/
		protected void fixOutBuf( Repeat r, PortList list, String[] portNames, int expected) {
			if ( portNames.length != expected ) {
					System.err.println("[ProcessParser] Repeat must have as many ("+expected+") buffers as output (given "+portNames.length+")");
				// JE: impossible because of SAX
				//throw new TMException("[ProcessParser] Repeat must have as many ("+expected+") buffers as output (given "+portNames.length+")");
			} else {
				for( int i = 0; i != portNames.length; i++) {
					list.setPort(i, new Port( portNames[i], r, i ));
				}
				registerChannelOut( list, false );
			}  
		}
		
		/**
		*	Creates the in ports of a call.
		* 
		*@param t 	The call
		*@param list The port list which store the in ports
		*@param portNames The names of the ports to create
		*@param expected The required number of ports  
		*@param type The type of the call
		*/
		protected void fixInPorts( Call c, PortList list, String[] portNames, int expected, String type) {
			if ( (expected != -1) && (portNames.length != expected) ) {
					System.err.println("[ProcessParser]"+type+" must have "+expected+" input (given "+portNames.length+")");
					// JE: impossible because of SAX
					//throw new TMException("[ProcessParser]"+type+" must have "+expected+" input (given "+portNames.length+")");
			} else {  	
				for( int i = 0; i != portNames.length; i++) {
					list.setPort(i, new Port( portNames[i], c, i ,Port.INPUT));
					Port p=list.getPort(i);
				}
				registerChannelOut( list, true );
			}
		}

		/**
		*	Creates the out ports of a call.
		* 
		*@param t 	The call
		*@param list The port list which store the in ports
		*@param portNames The names of the ports to create
		*@param expected The required number of ports  
		*@param type The type of the call
		*/
		protected void fixOutPorts( Call c, PortList list, String[] portNames, int expected, String type) {
			if ( (expected != -1) && (portNames.length != expected) ) {
					System.err.println("[ProcessParser]"+type+" must have "+expected+" input (given "+portNames.length+")");
					// JE: impossible because of SAX
					//throw new TMException("[ProcessParser]"+type+" must have "+expected+" input (given "+portNames.length+")");
			} else {
					for( int i = 0; i != portNames.length; i++) {
						list.setPort(i, new Port( portNames[i], c, i , Port.OUTPUT));
					}
			}
			registerChannelIn( list, true );
		}

		/**
		*	Checks the channel of the process. Channel must be connected to in and out port 
		* 
		*@param process the process to check
		*/
		public void checkChannels( fr.fluxmedia.transmorpher.graph.Process t ) {
			if(debugMode>1){  
			 System.err.println("************** CHECK "+t.getName()+" ***************");
			}
			for( Enumeration<Channel> e = t.getChannels(); e.hasMoreElements();) {
			    Channel c = e.nextElement();
				if(debugMode>1){
				 if (c instanceof FeedBack){
					 System.err.println("FEEDBACK "+c+" INPUT "+(c.in()).getName()+" OUTPUT "+(c.out()).getName());
					 if (( ((FeedBack) c).innerInput())!=null)	
						 System.err.println("INNER INPUT "+( ((FeedBack)c).innerInput()).getName());	
					 if (( ((FeedBack) c).innerOutput())!=null)	
						 System.err.println("INNER OUTPUT "+( ((FeedBack)c).innerOutput()).getName());	
				 }  else if (!((c instanceof Null)|( c instanceof StdIn )|( c instanceof StdOut )))
								 System.err.println("CHANNEL "+c+" INPUT "+(c.in()).getName()+" OUTPUT "+(c.out()).getName());
				}
				// System.err.println(c.getName()+" : "+c.in()+" --> "+c.out());
				if ( c instanceof Null ) {
						// No cause for problem... so far
				} else if ( c instanceof StdIn ) {
						// No cause for problem... so far
				} else if ( c instanceof StdOut ) {
						// No cause for problem... so far
				} else if ( (c.in() == null) || (c.out() == null) ) {
						System.err.println("[ProcessParser] dangling channel ("+c.getName()+") in "+t.getName());
						// JE: impossible because of SAX
						//throw new TMException("[ProcessParser] dangling channel ("+c.getName()+") in "+t.getName());
				}
			}
		}

	/* The algorithm depends on the context (transformation or call)
	 * If we are in a Transformation:
	 * the in are input for the channel, they cannot be anything but Channel
	 * the out are output for the channel, they cannot be anything but Channel
	 * If we are in a Call:
	 * the in are output to the channel, they cannot be anything but Channel and _stdin_
	 * the out are input to the channel, they cannot be anything but Channel and _stdout_ or _null_
	 */

    /** 
		 * Register a channel with its input 
		 * The algorithm depends on the context (transformation or call)
		 * If we are in a Transformation:
		 * the in are input for the channel, they cannot be anything but Channel
		 * the out are output for the channel, they cannot be anything but Channel
		 * If we are in a Call:
		 * the in are output to the channel, they cannot be anything but Channel and _stdin_
		 * the out are input to the channel, they cannot be anything but Channel and _stdout_ or _null_
		 *
		 *@param list 	the list of the port to register
		 *@param Callp 	if true, this is a call, else this is a transformation  
		 */
		public void registerChannelIn( PortList list, boolean Callp ) {
		    for( int i = list.length()-1; i != -1; i-- ) { // JE: reverse order
				Port p = list.getPort(i);
				String name = p.getName();
				Channel c;
				if ( !Callp && (name.equals("_null_") || name.equals("_stdout_")) ) {
					System.err.println("[ProcessParser] cannot use "+name+" in transformation signature");
					// JE: impossible because of SAX
					//throw new TMException("[ProcessParser] cannot use "+name+" in transformation signature");
				} else if ( name.equals("_stdin_") ) {
					System.err.println("[ProcessParser] cannot use "+name+" as output channel");
					// JE: impossible because of SAX
					//throw new TMException("[ProcessParser] cannot use "+name+" as output channel");
				} else if ( name.equals("_stdout_") ) {
						// JE: also check if current transformation is main
						if ( ((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).getChannel( name ) != null ){
							System.err.println("[ProcessParser] Two process writing to _stdout_");
							// JE: impossible because of SAX
							//throw new TMException("[ProcessParser] Two process writing to _stdout_");
						} else {
							c = new StdOut( name, (fr.fluxmedia.transmorpher.graph.Process)currentTransformation );
							((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).addChannel( c );
							c.setIn( p );
							p.setChannel( c );
						}
				} else if ( name.equals("_null_") ) {
					c = new Null( name, (fr.fluxmedia.transmorpher.graph.Process)currentTransformation );
					((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).addChannel( c );
					c.setIn( p );
					// TAKE CARE there could be several (but useless)
					p.setChannel( c );
				} else {
					c = ((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).getChannel( name );
					if ( c != null ) {
						if ( (c.in() == null) || (c instanceof FeedBack) ) {// && take care the the innerInput is not already set...
							c.setIn(p);
							p.setChannel( c );
						} else {
							System.err.println("[ProcessParser] two in ports on the same channel"+c.getName()+" "+p.getName());
							// JE: impossible because of SAX
							//throw new TMException("[ProcessParser] two in ports on the same channel"+c.getName()+" "+p.getName());
						}
					} else {
						c = new Channel( name, (fr.fluxmedia.transmorpher.graph.Process)currentTransformation );
						((fr.fluxmedia. transmorpher.graph.Process)currentTransformation).addChannel( c );
						c.setIn( p );
						p.setChannel( c );
					}
				}
			}
		}

    /** 
		 * Register a channel with its output 
		 * The algorithm depends on the context (transformation or call)
		 * If we are in a Transformation:
		 * the in are input for the channel, they cannot be anything but Channel
		 * the out are output for the channel, they cannot be anything but Channel
		 * If we are in a Call:
		 * the in are output to the channel, they cannot be anything but Channel and _stdin_
		 * the out are input to the channel, they cannot be anything but Channel and _stdout_ or _null_
		 *
		 *@param list 	the list of the port to register
		 *@param Callp 	if true, this is a call, else this is a transformation  
		 */
		public void registerChannelOut( PortList list, boolean Callp ) {
		    for( int i = list.length()-1; i != -1; i-- ) { // JE: reverse order
				Port p = list.getPort(i);
				String name = p.getName();
				Channel c;
				if ( name.equals("_null_") || name.equals("_stdout_") ) {
					System.err.println("[ProcessParser] cannot use "+name+" as output channel");
					// JE: impossible because of SAX
					//throw new TMException("[ProcessParser] cannot use "+name+" as output channel");
				} else if ( !Callp && name.equals("_stdin_") ){
					System.err.println("[ProcessParser] cannot use "+name+" in transformation signature");
					// JE: impossible because of SAX
					//throw new TMException("[ProcessParser] cannot use "+name+" in transformation signature");
				} else if ( name.equals("_stdin_") ){
					// JE: also check if current transformation is main
					if ( ((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).getChannel( name ) != null ){
						System.err.println("[ProcessParser] Two processes listening to _stdin_");
						// JE: impossible because of SAX
						//throw new TMException("[ProcessParser] Two processes listening to _stdin_");
					} else {
						c = new StdIn( name, (fr.fluxmedia.transmorpher.graph.Process)currentTransformation );
						((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).addChannel( c );
						c.setOut( p );
						// TAKE CARE there could be several (but useless)
						p.setChannel( c );
					}
				} else {
					c = ((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).getChannel( name );
					if ( c != null ) {
						if ( (c.out() == null) || (c instanceof FeedBack) ) {// && take care the the innerOutput is not already set...
							if ((c instanceof FeedBack)&&(((FeedBack) c).innerOutput()==null)){
								c.setOut( p ); //set innerOutput
								if (c.out()==null)
									c.setOut( p) ; //set output
								}
								else
									c.setOut(p);
								p.setChannel( c );
								((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).addChannel( c );
						} else {
							System.err.println("[ProcessParser] two out ports on the same channel "+c.getName()+" "+p.getName());
							// JE: impossible because of SAX
							//throw new TMException("[ProcessParser] two out ports on the same channel "+c.getName()+" "+p.getName());
						}
					} else {
						c = new Channel( name, (fr.fluxmedia.transmorpher.graph.Process)currentTransformation );
						((fr.fluxmedia.transmorpher.graph.Process)currentTransformation).addChannel( c );
						c.setOut( p );
					}
				}
			}
		}


    /*---------------------------- ANALYSE PORT NAME -----------------------------------------*/
    
  /** 
		* Analyzes the port Name attributes
		* "a b c" will be translate to {"a","b","c"}
		* @param pList the list of ports names
		* @return String[] contains the names of the ports or null if pList is null
		*/
		public static  String[] AnalysePortNames(String pListe) {
			if ( pListe != null ) {
				// JE: I would like to add "," in the tokenizer (possible?).
				StringTokenizer vTokens = new StringTokenizer( pListe, " ", true );
				String[] vAns = new String[vTokens.countTokens()/2+1];
				String vToken = null;
				int i = 0;
				while ( vTokens.hasMoreTokens() ) {
					vToken = vTokens.nextToken();
					if (vToken.equals(" ")) {
					} else {
						vAns[i++]=vToken;
						} //end if
				}//end while
				return vAns;
			} else {
			return null;
			}
		} //end proc
    
    /** 
		 * Resets the parameters as they were after taking care of a Call 
		 */
    protected void cleanStack(){
			currentParams = currentCall.getProcess().getParameters();
			if ( (currentTransformation != null) && (currentTransformation instanceof RepeatProcess) ){
				currentCall = ((RepeatProcess)currentTransformation).getCaller();
			} else {
				currentCall = null;
			}
    }
    
    /*------------------------------------ LIST ATTRIBUTE -----------------------------------*/

    /** 
		 * Returns a XML-attribute string from the result of such a string 
		 * @param atts attributes to transform
		 * @return String a string (in XML format) which contains the attributes
		 */
    // Used only for transmitting litteral XSLT Templates
    public String listAttribute(Attributes atts){
			String listAttribute="";
			if (atts.getLength()!=0) {
				for(int i=0 ; i<atts.getLength() ; i++){
					listAttribute=listAttribute+atts.getLocalName(i)+"=\""+atts.getValue(i)+"\" ";
				}
      }
      return listAttribute;
    }
    
}
    
