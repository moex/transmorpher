/**
 * $Id: QueryParser.java,v 1.2 2003-01-16 13:37:41 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.parser ;

// Imported XML classes

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;

//Imported SAX classes

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.Attributes;

//Imported JAVA classes

import java.io.*;
import java.io.IOException;

import java.util.Stack;
import java.util.Vector;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.lang.Integer;

//Imported Transmorpher classes

import fr.fluxmedia.transmorpher.utils.Parameters ;
import fr.fluxmedia.transmorpher.utils.TMException ;
import fr.fluxmedia.transmorpher.graph.Transmorpher ;
import fr.fluxmedia.transmorpher.graph.Transformation ;
import fr.fluxmedia.transmorpher.graph.Call ;
import fr.fluxmedia.transmorpher.graph.RepeatProcess ;
import fr.fluxmedia.transmorpher.graph.Namespaced ;
import fr.fluxmedia.transmorpher.graph.Query ;
import fr.fluxmedia.transmorpher.graph.rules.Rule;
import fr.fluxmedia.transmorpher.graph.rules.Select ;
import fr.fluxmedia.transmorpher.graph.rules.Namespace ;

public class QueryParser extends FMParser{
  private String uri = null;
  private Transmorpher currentTransmorpher = null;
  private Transformation currentTransformation = null;
  private Call currentCall = null;
  private Rule rule = null;
  private String currentParam = null;
  private String currentWParam = null;
  private Parameters currentParams = null;

  /** Are we copying some XSLT */
  boolean  inLitteralXSLT = false;
    
  ProcessParser iMainParser = null;
    
  /** The constructor, build the XML PArser but not parse the document */
  public QueryParser(int debug_mode,ProcessParser pMainParser) throws ParserConfigurationException, SAXException {
    super(debug_mode);
    iMainParser = pMainParser ;
  }
  
  public void setTransmorpher (Transmorpher pCurrentTransmorpher) {
    currentTransmorpher= pCurrentTransmorpher ;
  }
    
    /* ************* */
    /* START ELEMENT */
    /* ************* */
    
    /** Call by the XML parser at the begining of an element */
    public final void startElement(String namespaceURI, String localName, String qname, Attributes atts) {
  
      if (namespaceURI.equals("http://transmorpher.fluxmedia.fr/1.0")) {
	    String[] inChannels = AnalysePortNames(atts.getValue("in"));
	    String[] outChannels = AnalysePortNames(atts.getValue("out"));
	    if (debugMode > 2) System.err.println("startElement ProcessParser : " + localName);
      if (localName.equals("with-param")) {
          currentWParam = atts.getValue("name");
          if ( atts.getValue("select") != null ) {
	      if ( currentParams != null )
		  currentParams.setParameter(currentWParam,atts.getValue("select"));
          }
      }  else if (localName.equals("param")) {
          currentParam = atts.getValue("name");
          if ( atts.getValue("select") != null ) {
	      currentParams.setParameter(currentParam,atts.getValue("select"));
          }
      } else if (localName.equals("namespace")) {
          rule = (Rule)new Namespace(atts.getValue("name"),atts.getValue("uri"));
          ((Namespaced)currentTransformation).addNamespace( (Namespace)rule );
      } else if (localName.equals("select")) {
          rule = (Rule)new Select(atts.getValue("match"),atts.getValue("test"));
          ((Query)currentTransformation).addSelection( (Select)rule );
      } else if (localName.equals("query")) {
          currentTransformation = new Query(atts.getValue("name"),currentTransmorpher,atts.getValue("root"));
          currentTransmorpher.addTransformation(atts.getValue("name"),currentTransformation);
					currentParams = currentTransformation.getParameters();
      } else {
          System.err.println("[QueryParser] Unknown element : " + localName);
	  // JE: Impossible because of SAX
	  //throw new TMException("[QueryParser] Unknown element : " + localName);
      } //end if
     
    } else {
	  System.err.println("[QueryParser] Unknown namespace ("+namespaceURI+") for "+qname);
      // JE: Impossible because of SAX
      //throw new TMException("[QueryParser] Unknown namespace ("+namespaceURI+") for "+qname);
    }
  } // startElement(String namespaceURI, String localName, String qname, Attributes atts)
    

  /* The content of the tags */
  /* No content in Transmorpher so far but in XSLT code */
  
  public void characters(char ch[], int start, int length) {
  
    String content = "";
    int end = start + length;
  
    for( int i = start; i < end ; i++ ) content = content + ch[i];
    if (debugMode > 2) System.err.println("Content : " + content);
  
    if( inLitteralXSLT == true ) {
      // JE: to be put in the corresponding string
      //iXslFile.Writeln(2,content);
      } else if ( currentParam != null ) {
        currentParams.setParameter(currentParam,content);
      } else if ( currentWParam != null ) {
        currentParams.setParameter(currentWParam,content);
      }
    }

  /* *********** */
  /* END ELEMENT */
  /* *********** */

  /** Call by the XML parser at the end of an element */
  public final void endElement(String namespaceURI,String pName, String qName ) {
    if(namespaceURI.equals("http://transmorpher.fluxmedia.fr/1.0")) {
      if(debugMode > 2) System.err.println("endElement QueryParser : " + pName);
      if (pName.equals("param")) {
      } else if (pName.equals("select")) {
      } else if (pName.equals("extends")) {
      } else if (pName.equals("namespace")) {
      } else if (pName.equals("query")) {
					currentTransformation.setParameters(currentParams);
         currentTransformation = null;
         iMainParser.setEndQuery();
      }} else {
	System.err.println("[QueryParser]Error : Unknown namespace : "+namespaceURI);
	// JE: impossible because of SAX
	//throw new SAXParseException("[QueryParser] Unknown namespace : "+namespaceURI);
    }
   } //end endeLement
    

    /*---------------------------- ANALYSE PORT NAME -----------------------------------------*/
    
  /** Analyze the port Name attributes */
  public static final String[] AnalysePortNames(String pListe) {
      if ( pListe != null ) {
  // JE: I would like to add "," in the tokenizer (possible?).
    StringTokenizer vTokens = new StringTokenizer( pListe, " ", true );
    String[] vAns = new String[vTokens.countTokens()/2+1];
    String vToken = null;
    int i = 0;
    while ( vTokens.hasMoreTokens() ) {
      vToken = vTokens.nextToken();
      if (vToken.equals(" ")) {
      } else {
        vAns[i++]=vToken;
      } //end if
    }//end while
    return vAns;
      } else {
	  return null;
      }
  } //end proc
    
    /** Reset the parameters as they were after taking care of a Call */
    private void cleanStack(){
	currentParams = currentCall.getProcess().getParameters();
	if ( (currentTransformation != null) && (currentTransformation instanceof RepeatProcess) ){
	    currentCall = ((RepeatProcess)currentTransformation).getCaller();
	} else {
	    currentCall = null;
	}
    }
    
    /*------------------------------------ LIST ATTRIBUTE -----------------------------------*/

    /** Returns a XML-attribute string from the result of such a string */
    // Used only for transmitting litteral XSLT Templates
    public final String listAttribute(Attributes atts){
  String listAttribute="";
  if (atts.getLength()!=0)
      {
    for(int i=0 ; i<atts.getLength() ; i++){
        listAttribute=listAttribute+atts.getLocalName(i)+"=\""+atts.getValue(i)+"\" ";
    }
    
      }
  
      return listAttribute;
    }
    
}//end class
    


