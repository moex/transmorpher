/*
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003, 2022.
 *
 * https://gitlab.inria.fr/moex.transmorpher - https://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.transmorpher.graph;

import fr.fluxmedia.transmorpher.utils.LinearIndexedStruct;
import fr.fluxmedia.transmorpher.utils.Writer;
import java.util.Enumeration;

import java.util.Hashtable;
import java.util.ListIterator;
import java.util.Stack;

/**
 * Transmorpher graph repeat process definitions
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */

public class RepeatProcess extends Process {

	/**
	 *  The repeat call
	 */
	protected Repeat caller = null;
	/**
	 *  The list of buffered ports
	 */
	protected PortList buf = null;

	/**
	 *Constructor for the RepeatProcess object
	 *
	 *@param  name     	The name of the Repeat Process to create
	 *@param  repeat    The Repeat Call insatnce corresponding to this RepeatProcess
	 *@param  nbIn   		The number of In ports
	 *@param  nbOut  		The number of Out ports
	 *@param  nbBuf  		The number of buffered ports
	 */
	public RepeatProcess(String name, Repeat repeat, int nbIn, int nbOut, int nbBuf) {
		super(name, (Transmorpher)null, nbIn, nbOut);
		caller = repeat;
		buf = new PortList(nbBuf);
	}

	/**
	 * Returns buf list
	 *
	 *@return    Description of the Return Value
	 */
	public PortList bufPorts() {
		return buf;
	}

	/**
	 * Prints the XML expression of the process
	 */
	public void generateXML() {
	    for ( Call c: calls ) {
		c.generateXML();
	    }
	}

	/**
	 * Prints the Java code for the execution of the call
	 *
	 *@param  file  Description of the Parameter
	 */
	public void generateJavaCode(Writer file) {
		System.out.println("[RepeatProcess]generateJavaCode : not yet implemented");
	}

	/**
	 *  
	 */
	public void setUp() {
	    for ( Call c: calls ) {
		c.setUp();
	    }
	}

	/**
	 *  Gets the caller attribute of the RepeatProcess object
	 *
	 *@return    The caller value
	 */
	public Repeat getCaller() {
		return caller;
	}

	/**
	 * This implementation takes into account the fact that
	 *this particular process body is imbedded into a Call.
	 *This works recursively since that call can be embedded
	 *in the same way and then will use the same kind of getTransmorpher.
	 *
	 *@return    The transmorpher value
	 */
	public Transmorpher getTransmorpher() {
		return caller.getProcess().getTransmorpher();
	}

}

