/**
 * Transmorpher
 *
 * Copyright (C) 2001 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


package fr.fluxmedia.transmorpher.graph;
import java.io.Serializable;

/**
 * Transmorpher graph list of ports/channels
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */
public class Port implements Serializable {

	/**
	 *  Description of the Field
	 */
	public static int OUTPUT = 0;
	/**
	 *  Description of the Field
	 */
	public static int INPUT = 1;

	/**
	 *  Description of the Field
	 */
	protected Channel channel = null;
	/**
	 *  Description of the Field
	 */
	protected Transformation tOwner = null;
	/**
	 *  Description of the Field
	 */
	protected Call cOwner = null;
	/**
	 *  Description of the Field
	 */
	protected int number = 0;
	/**
	 *  Description of the Field
	 */
	protected String name = null;
	/**
	 *  Description of the Field
	 */
	protected int type;

	/**
	 *Constructor for the Port object
	 */
	public Port() { }

	/**
	 * Port creator for transformations
	 *
	 *@param  n  Description of the Parameter
	 *@param  t  Description of the Parameter
	 *@param  i  Description of the Parameter
	 */
	public Port(String n, Transformation t, int i) {
		name = n;
		number = i;
		tOwner = t;
	}


	/**
	 * Port creator for transformations
	 *
	 *@param  n     Description of the Parameter
	 *@param  t     Description of the Parameter
	 *@param  i     Description of the Parameter
	 *@param  type  Description of the Parameter
	 */
	public Port(String n, Transformation t, int i, int type) {
		name = n;
		number = i;
		tOwner = t;
		this.type = type;
	}


	/**
	 * Port creator for calls
	 *
	 *@param  n  Description of the Parameter
	 *@param  c  Description of the Parameter
	 *@param  i  Description of the Parameter
	 */
	public Port(String n, Call c, int i) {
		name = n;
		number = i;
		cOwner = c;
	}

	/**
	 *Constructor for the Port object
	 *
	 *@param  n     Description of the Parameter
	 *@param  c     Description of the Parameter
	 *@param  i     Description of the Parameter
	 *@param  type  Description of the Parameter
	 */
	public Port(String n, Call c, int i, int type) {
		name = n;
		number = i;
		cOwner = c;
		this.type = type;
	}


	/**
	 *  Sets the channel attribute of the Port object
	 *
	 *@param  c  The new channel value
	 */
	public void setChannel(Channel c) {
		channel = c;
		if (c != null) {
			name = c.getName();
		} else {
			name = "";
		}
	}

	/**
	 *  Gets the channel attribute of the Port object
	 *
	 *@return    The channel value
	 */
	public Channel getChannel() {
		return channel;
	}

	/**
	 *  Sets the name attribute of the Port object
	 *
	 *@param  newName  The new name value
	 */
	public void setName(String newName) {
		name = newName;
	}

	/**
	 *  Gets the name attribute of the Port object
	 *
	 *@return    The name value
	 */
	public String getName() {
		return name;
	}

	/**
	 *  Gets the type attribute of the Port object
	 *
	 *@return    The type value
	 */
	public int getType() {
		return type;
	}

	/**
	 *  Sets the type attribute of the Port object
	 *
	 *@param  type  The new type value
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 *  Description of the Method
	 *
	 *@return    Description of the Return Value
	 */
	public boolean TransformationPortP() {
		return (tOwner != null);
	}

	/**
	 *  Description of the Method
	 *
	 *@return    Description of the Return Value
	 */
	public boolean CallPortP() {
		return (cOwner != null);
	}

	/**
	 *  Gets the transformation attribute of the Port object
	 *
	 *@return    The transformation value
	 */
	public Transformation getTransformation() {
		return tOwner;
	}

	/**
	 *  Sets the transformation attribute of the Port object
	 *
	 *@param  tOwner  The new transformation value
	 *@return         Description of the Return Value
	 */
	public void setTransformation(Transformation tOwner) {
		this.tOwner = tOwner;
	}

	/**
	 *  Gets the call attribute of the Port object
	 *
	 *@return    The call value
	 */
	public Call getCall() {
		return cOwner;
	}

	/**
	 *  Sets the call attribute of the Port object
	 *
	 *@param  cOwner  The new call value
	 */
	public void setCall(Call cOwner) {
		this.cOwner = cOwner;
	}

}//end class

