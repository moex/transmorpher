/*
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003, 2022.
 *
 * https://gitlab.inria.fr/moex.transmorpher - https://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.graph;

import fr.fluxmedia.transmorpher.engine.TApplyProcess;
import fr.fluxmedia.transmorpher.engine.TProcess;
import fr.fluxmedia.transmorpher.engine.TProcessComposite;
import fr.fluxmedia.transmorpher.utils.LinearIndexedStruct;
import fr.fluxmedia.transmorpher.utils.Writer;

import java.io.IOException;

import java.util.ListIterator;

/**
 * This class allows to create an object able to  call a transformation defined in a Process
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */

public class ApplyProcess extends ApplyImpl {

	/**
	 *Creates an instance of ApplyProcess with an id and a type
	 *
	 *@param  id  	The id of the instance to create
	 *@param  type  The type of the instance to create
	 */
	public ApplyProcess(String id, String type) {
		this(id, type, null, null);
	}

	/**
	 *Creates an instance of ApplyProcess with an id, a type and a process
	 *
	 *@param  id  			The id of the instance to create
	 *@param  type  		The type of the instance to create
	 *@param  reference	The reference of the process to apply
	 */
	public ApplyProcess(String id, String type, String reference) {
		this(id, type, null, reference);
	}

	/**
	 *Constructor for the ApplyProcess object
	 *
	 *@param  id  			The id of the instance to create
	 *@param  type  		The type of the instance to create
	 *@param  process		The process that owns this instance
	 */
	public ApplyProcess(String id, String type, Process process) {
		this(id, type, process, null, 0, 0);
	}

	/**
	 *Constructor for the ApplyProcess object
	 *
	 *@param  id  			The id of the instance to create
	 *@param  type  		The type of the instance to create
	 *@param  process		The process that owns this instance
	 *@param  reference	The reference of the process to apply
	 */
	public ApplyProcess(String id, String type, Process process, String reference) {
		this(id, type, process, reference, 0, 0);
	}


	/**
	 *Constructor for the ApplyProcess object
	 *
	 *@param  id  			The id of the instance to create
	 *@param  type  		The type of the instance to create
	 *@param  process		The process that owns this instance
	 *@param  reference	The reference of the process to apply
	 *@param  in   			The number of in ports
	 *@param  out  			The number of out ports
	 */
	public ApplyProcess(String id, String type, Process process, String reference, int in, int out) {
		super(id, type, process, reference ,in, out);
	}

	/**
	 * ask a nullify call to the called process
	 *
	 *@param  out  Description of the Parameter
	 *@param  in   Description of the Parameter
	 */
	public void retroNull(PortList out, PortList in) {
		// JE: check is it is a Process
		Process called = (Process)(getProcess().getTransmorpher().findTransformation(getRef()));
		nullified = called.retroNull(out, in, true);
		visited = true;

		update(null);
	}

	/**
	 *  Prints an XML description of this instance
	 */
	public void generateXML() {
		System.out.print("      <apply-process id=\"" + id + "\" ref=\"" + getRef() + "\" ");
		System.out.print("in=\"");
		inPorts.generateXML();
		System.out.print("\" out=\"");
		outPorts.generateXML();
		if (parameters.isEmpty()) {
			System.out.println("\"/>");
		} else {
			System.out.println("\">");
			parameters.generateXMLCalls();
			System.out.println("      </apply-process>");
		}
	}//end generate XML

	/**
	 *  Creates a TApplyProcess corresponding to this ApplyProcess object and adds it to the composite
	 * process in the execution structure.
	 *
	 *@param  currentProcess  the container for this serializer
	 */
	public final void createProcess(TProcessComposite currentProcess) {
		try {
			String[] vPortIn = inPorts().toStringList();
			String[] vPortOut = outPorts().toStringList();

			// get the name corresponding to the generated stylesheet and pass it in parameter
			String ref = getAttributes().getStringParameter("ref");
			Transmorpher t = getProcess().getTransmorpher();
			Process p = (Process)(t.findTransformation(ref));
			LinearIndexedStruct<Call> vCalls = p.getCalls();
			//System.out.println("[ExecutionStructure]ApplyProcess : findTransformation"
			//                    +((fr.fluxmedia.transmorpher.graph.Process)iTransmorpher.getTransformation(ref)));
			TProcessComposite vCurrent =
				(TProcessComposite)new TApplyProcess(vPortIn, vPortOut,
				getParameters(),
				getAttributes());
			PortList LocIn = p.inPorts();
			PortList LocOut = p.outPorts();
			((TApplyProcess)vCurrent).setLocalName(LocIn.getNames(), LocOut.getNames());

			for ( Call c: vCalls ) {
			    c.createProcess(vCurrent);
			}
			currentProcess.addProcess(vCurrent);
			update(vCurrent);
		} catch (Exception e) {
			System.out.println("[ApplyProcess] Error : cannot create Process");
			System.out.println("[ApplyProcess] " + e);
		}
	}//end proc

	/**
	 *  
	 */
	public void setUp() {
		if (getProcess().getTransmorpher().findTransformation(getRef()) == null) {
			System.err.println("warning : unknown process : " + getRef());
		}
		update(null);
	}//end setUp

	/**
	 *  Prints a java code description of this instance
	 *
	 * WARNING :This is the main problematic aspect:
	 * There is a possible recursion in Process calls: they should thus be stacked for working correctly
	 *
	 *@param  file             The writer used for printing the code in a file
	 *@exception  IOException  when an IO errors occur
	 */
	public void generateJavaCode(Writer file) throws IOException {
		String r = getAttributes().getStringParameter("ref");
		Transmorpher tr = getProcess().getTransmorpher();
		fr.fluxmedia.transmorpher.graph.Process processDef = ((fr.fluxmedia.transmorpher.graph.Process)tr.findTransformation(r));
		file.writeln();
		file.writeln(4, "//------------------ Generation of ApplyProcess " + getId() + " -------------------");
		generatePorts(file);
		generateParameters(file);
		file.writeln(4, "tmCurrentProcess = new TApplyProcess(portIn,portOut,tmParameters,tmAttributes);");
		file.writeln(4, "tmCurrentProcess.setName(\"" + getId() + "\");");
		file.writeln();
		file.writeln(4, "tmCurrentCompositeProcess.addProcess(tmCurrentProcess);");
		file.writeln();
		// JE: Here we stack the tmCurrentCompositeProcess
		file.writeln(4, "tmProcessStack.push( tmCurrentCompositeProcess );");
		file.writeln(4, "tmCurrentCompositeProcess = (TProcessComposite)tmCurrentProcess;");
		file.writeln();
		// JE: Change of port names ?
		file.writeln(4, "LocIn = new String[" + processDef.inPorts().getNames().length + "];");
		for (int i = 0; i < processDef.inPorts().length(); i++) {
			file.writeln(4, "LocIn[" + i + "]=\"" + processDef.inPorts().getNames()[i] + "\";");
		}
		file.writeln(4, "LocOut = new String[" + processDef.outPorts().getNames().length + "];");
		for (int i = 0; i < processDef.outPorts().length(); i++) {
			file.writeln(4, "LocOut[" + i + "]=\"" + processDef.outPorts().getNames()[i] + "\";");
		}
		file.writeln(4, "((TApplyProcess)tmCurrentProcess).setLocalName(LocIn,LocOut);");
		file.writeln(4, "//------------------ Generation of process body for " + getId() + " -------------------");
		for ( Call call: processDef.getCalls() ) {
		    call.generateJavaCode(file);
		}
		file.writeln(4, "//----------------------- End of ----process body for " + getId() + " ---------------------");
		// JE: Here we unstack the tmCurrentCompositeProcess
		file.writeln(4, "tmCurrentCompositeProcess = tmProcessStack.pop();");
	}

	/**
	 *  Do nothing ( to remove ?)
	 *
	 *@param  file  Description of the Parameter
	 */
	public void generateLocalName(Writer file) { }

}

