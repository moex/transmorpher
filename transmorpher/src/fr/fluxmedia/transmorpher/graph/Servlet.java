/*
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003, 2022.
 *
 * https://gitlab.inria.fr/moex.transmorpher - https://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.transmorpher.graph;

import fr.fluxmedia.transmorpher.engine.*;
import fr.fluxmedia.transmorpher.utils.LinearIndexedStruct;
import fr.fluxmedia.transmorpher.utils.Parameters;

import fr.fluxmedia.transmorpher.utils.Writer;
import java.io.IOException;
import java.util.Enumeration;

import java.util.Hashtable;
import java.util.ListIterator;

/**
 * Transmorpher graph servlet definitions
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */

public class Servlet extends MainProcess {

	/**
	 *Constructor for the Servlet object
	 *
	 *@param  name  	The name of the Servlet instance to create
	 *@param  owner   The transmorpher which owns this instance
	 */
	public Servlet(String name, Transmorpher owner) {
		super(name, owner, 0, 0);
	}

	/**
	 *  Description of the Method
	 */
	public void retroNull() {
	}

	/**
	 * Prints the XML expression of the servlet
	 */
	public void generateXML() {
		System.out.println("  <servlet name=\"" + name + "\">");
		parameters.generateXMLDeclarations();
		for ( Call call: calls ) {
		    call.generateXML();
		}
		System.out.println("  </servlet>");
		System.out.println();
	}

	/**
	 *  Creates an execution component corresponding to this Servlet and starts the creation
	 *  of execution components of each calls of this Servlet
	 *
	 *@return    an execution component
	 */
	public TProcessComposite createMainProcess() {
		//FT : I think a servlet can only be compiled 
		try {
			TProcessComposite vCurrent = new TServlet(getParameters());
			for ( Call call: calls ) {
			    call.createProcess(vCurrent);
			}
			return vCurrent;
		} catch (Exception e) {
			System.out.println("[Servlet] Error : cannot create Process");
			System.out.println("[Servlet] " + e);
			return null;
		}
	}

	/**
	  *  Generates java code for import
	 *
	 *@param  file            The writer used for printing in a file
	 *@param  externs         Description of the Parameter
	 *@exception  IOException When IO errors occur
	 */
	 
	public void generateImport(Writer file, LinearIndexedStruct<String> externs) throws IOException {
		file.writeln("//import Servlet classes");
		file.writeln("import java.io.*;");
		file.writeln("import javax.servlet.*;");
		file.writeln("import javax.servlet.http.*;");
		file.writeln("import java.util.*;");

		file.writeln();
	}

	/**
	 *  Generates java code for class header
	 *
	 *@param  file            The writer used for printing in a file
	 *@exception  IOException When IO errors occur
	 */
	public void generateClassHeader(Writer file) throws IOException {
	    // Avoid warning
	    file.writeln("@SuppressWarnings(\"serial\")");
	    file.writeln("public class " + getName() + " extends HttpServlet {");
	}

	/**
	 *  Generates java code for main header
	 *
	 *@param  file            The writer used for printing in a file
	 *@exception  IOException When IO errors occur
	 */
	public void generateMainHeader(Writer file) throws IOException {
		file.writeln(2, "ServletContext context;");
		file.writeln(2, "Parameters servletParameters = null;");
		file.writeln(2, "String root;");
		file.writeln(2, "public void init(ServletConfig config) throws ServletException{");
		file.writeln(4, "try{");
		file.writeln(6, "context = config.getServletContext();");
		file.writeln(6, "root = context.getRealPath(\"/\");");
		//iFile.writeln(6,"vSR= new SystemResources();");
		file.writeln(6, "tmProcessStack = new Stack<TProcessComposite>();");
		file.writeln(4, "} catch (Exception TransformException) {");
		file.writeln(4, "}");
		file.writeln(2, "}");
		file.writeln();
		file.writeln(2, "protected void doGet(HttpServletRequest req, HttpServletResponse res)");
		file.writeln(4, "          throws ServletException, IOException {");
		file.writeln(4, "   doPost(req,res);");
		file.writeln(2, "}");
		file.writeln();
		file.writeln(2, "protected void doPost(HttpServletRequest req, HttpServletResponse res)");
		file.writeln(4, "          throws ServletException, IOException {");
		file.writeln();
		//iFile.writeln(4,"tmProcessFactory.initFactory(newDefexterns());");
	}

	/**
	 *  Generates java code for parameters
	 *
	 *@param  file            The writer used for printing in a file
	 *@exception  IOException When IO errors occur
	 */
	public void generateReadParameters(Writer file) throws IOException {
		// JE: Here are only considered the parameters that have been declared
		file.writeln(6, "servletParameters = new Parameters();");

		file.writeln();
		file.writeln(6, "try{");
		// See if this must not be modified
		file.writeln(8, "res.setContentType(\"text/html\");");
		//iFile.writeln(8,"PrintWriter out = res.getWriter();");
		file.writeln(8, "System.setOut(new PrintStream(res.getOutputStream()));");
	}

	/**
	 *  Generates java code for body
	 *
	 *@param  file            The writer used for printing in a file
	 *@param  externs          Description of the Parameter
	 *@exception  IOException When IO errors occur
	 */
	public void generateBody(Writer file, LinearIndexedStruct<String> externs) throws IOException {
		// JE: I strongly suspect that this should take place in the ReadParameters stuff
		Parameters param = getParameters();
		for (Enumeration e = param.getNames(); e.hasMoreElements(); ) {
			String key = (String)e.nextElement();
			file.writeln(8, "servletParameters.setParameter(\"" + key + "\", \"" + (String)param.getParameter(key) + "\");");
		}
		for (Enumeration e = getParameters().getNames(); e.hasMoreElements(); ) {
			Object key = e.nextElement();
			file.writeln(6, "if ( req.getParameter(\"" + key + "\") != null ){");
			file.writeln(8, "servletParameters.setParameter(\"" + key + "\",req.getParameter(\"" + key + "\"));");
			file.writeln(6, "};");
		}//end for
		file.writeln(8, "tmCurrentCompositeProcess = new TServlet(servletParameters);");
		for ( Call call: calls ) {
		    call.generateJavaCode(file);
		}
		// JE: warning, reuse of parameters
		file.writeln(8, "setOutput(tmCurrentCompositeProcess,res);");

		file.writeln(8, "tmCurrentCompositeProcess.bindParameters(servletParameters);");
		file.writeln(8, "tmCurrentCompositeProcess.execProcess(false);");
		file.writeln(8, "tmProcessStack=null;");

	}

	/**
	  *  Generates java code for end
	 *
	 *@param  file            The writer used for printing in a file
	 *@exception  IOException When IO errors occur
	 */
	public void generateEnd(Writer file) throws IOException {
		file.writeln(4, "if (debug_mode>0) {");
		file.writeln(6, "end = System.currentTimeMillis();");
		file.writeln(6, "System.err.println(\">> End of execution (\"+(end-begin)+\")\");}");
		file.writeln(4, "} catch (Exception e) {");
		file.writeln(6, "System.out.println(e);");
		file.writeln(6, "e.printStackTrace(System.out);");
		file.writeln(4, "}");
		file.writeln(2, "} //end main");
		generateSetOutput(file);
		file.writeln("} //end class");
	}

	/**
	 *  This method allows to connect the TSerializer ,corresponding to the user request ,
	 *	to the standart output. Others are connected to null.
	 *
	 *@param  file            The writer used for printing in a file
	 *@exception  IOException When IO errors occur
	 */
	public void generateSetOutput(Writer file) throws IOException {
		file.writeln(2, "protected void setOutput(TProcessComposite tmCurrentCompositeProcess,HttpServletResponse res){");
		file.writeln(4, "for (Enumeration e = tmCurrentCompositeProcess.getProcessBasic().elements() ; e.hasMoreElements() ;) {");
		file.writeln(6, "boolean test=true;");
		file.writeln(6, "TProcess_Impl process=(TProcess_Impl)e.nextElement();");
		file.writeln(6, "if (process instanceof TSerializer){");
		file.writeln(8, "for (Enumeration a = servletParameters.getNames() ; a.hasMoreElements() ;){");
		file.writeln(10, "String name = (String)a.nextElement();");
		file.writeln(10, "if (process.getParameter(name)!=null){");
		file.writeln(12, "if ((process.getParameter(name).equals(servletParameters.getParameter(name)))||(process.getParameter(name).equals(\"*\"))){");
		file.writeln(14, "test=true;");
		file.writeln(12, "}");
		file.writeln(12, "else{");
		file.writeln(14, "test=false;");
		file.writeln(14, "break;");
		file.writeln(12, "}");
		file.writeln(10, "}");
		file.writeln(10, "else{");
		file.writeln(12, "test=true;");
		file.writeln(10, "}");
		file.writeln(8, "}");
		file.writeln(8, "if (test){");
		file.writeln(10, "((TSerializer)process).printStdout(true);");
		file.writeln(10, "res.setContentType((String)process.getParameter(\"content-type\"));");
		file.writeln(8, "}");
		file.writeln(6, "}");
		file.writeln(4, "}");
		file.writeln(2, "}");
	}
}

