/*
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003, 2022.
 *
 * https://gitlab.inria.fr/moex.transmorpher - https://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.transmorpher.graph;

import fr.fluxmedia.transmorpher.action.TmActionImpl;
import fr.fluxmedia.transmorpher.utils.LinearIndexedStruct;

import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
import fr.fluxmedia.transmorpher.utils.Writer;
import java.io.IOException;

import java.util.Observable;

/**
 * This class is an helper class that implements most of the Transformation interface methods 
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */

public abstract class TransformationImpl extends Observable implements Transformation {

	/**
	 *  The name of the transformation instance
	 */
	protected String name;
	/**
	 *  The list of in ports
	 */
	protected PortList inPorts = null;
	/**
	 *  The list of out ports
	 */
	protected PortList outPorts = null;
	/**
	 *  The storage object for the parameters of this Transformation instance
	 */
	protected Parameters parameters = null;
	/**
	 *  The storage object for the attributes of this Transformation instance
	 */
	protected StringParameters attributes = null;
	/**
	 *  The Transmorpher instance that owns this Transformation
	 */
	protected Transmorpher transmorpher = null;
	/**
	 *  The type of this Transformation instance
	 */
	protected String type;

	/**
	 *Creates an instance of Transformation with a name, a type and the owner of this instance
	 *
	 *@param  name   				The name of the instance to create
	 *@param  type   				The type of the instance to create
	 *@param  transmorpher  The owner of the instance to create
	 */
	public TransformationImpl(String name, String type, Transmorpher transmorpher) {
		this(name, type, transmorpher, 0, 0);
	}

	/**
	 *Creates an instance of Transformation with a name, a type and the owner of this instance
	 *
	 *@param  name   				The name of the instance to create
	 *@param  type   				The type of the instance to create
	 *@param  transmorpher  The owner of the instance to create
	 *@param  nbIn   				The number of in ports
	 *@param  nbOut  				The number of out ports
	 */
	public TransformationImpl(String name, String type, Transmorpher transmorpher, int nbIn, int nbOut) {
		this.name = name;
		this.type = type;
		this.transmorpher = transmorpher;
		inPorts = new PortList(nbIn);
		outPorts = new PortList(nbOut);
		parameters = new Parameters();
		attributes = new StringParameters();
		attributes.setParameter("type", type);
	}

	/**
	 *  Gets the name attribute of the TransformationImpl object
	 *
	 *@return    The name value
	 */
	public String getName() {
		return name;
	}

	/**
	 *When the process name change, we must change the key in transmorpher storage structure.
	 *
	 *@param  newName  The new name value
	 */
	public void setName(String newName) {

		if (transmorpher != null) {
			//remove in the storage struct the transformation with the old id key
			transmorpher.getTransformations().remove(this.name);
			//add in the storage struct the transformation with the new id key
			transmorpher.getTransformations().add(newName, this);
		}
		update(new TmActionImpl(TmActionImpl.MODIFY, this, this.name, this.name, newName));
		this.name = newName;
	}

	/**
	 *  Gets the transmorpher attribute of the TransformationImpl object
	 *
	 *@return    The transmorpher value
	 */
	public Transmorpher getTransmorpher() {
		return transmorpher;
	}

	/**
	 *  Gets the type attribute of the TransformationImpl object
	 *
	 *@return    The type value
	 */
	public String getType() {
		return type;
	}

	/**
	 *  Sets the type attribute of the TransformationImpl object
	 *
	 *@param  t  The new type value
	 */
	public void setType(String t) {
		type = t;
		update(null);
	}

	/**
	 *  Sets the parameters attribute of the TransformationImpl object
	 *
	 *@param  p  The new parameters value
	 */
	public void setParameters(Parameters p) {
		parameters = p;
		update(p);
	}

	/**
	 *  Gets the parameters attribute of the TransformationImpl object
	 *
	 *@return    The parameters value
	 */
	public Parameters getParameters() {
		return parameters;
	}

	/**
	 *  Gets the attributes attribute of the TransformationImpl object
	 *
	 *@return    The attributes value
	 */
	public StringParameters getAttributes() {
		return attributes;
	}

	/**
	 *  Adds a new in port at the end of the storage list
	 *
	 *@param  port  The in port to add 
	 */
	public void addIn(Port port) {
		addIn(inPorts.length(), port);
	}

	/**
	 *  Adds a new in port at the given index in the storage list
	 *
	 *@param  index  	The index
	 *@param  port  	The port to add 
	 */
	public void addIn(int index, Port port) {
		inPorts.setPort(index, port);
		update(new TmActionImpl(TmActionImpl.ADD, this, port, null, null));
	}

	/**
	 *  Gets the list of in ports
	 *
	 *@return    The in port list
	 */
	public PortList inPorts() {
		return inPorts;
	}

	/**
	 *  Adds a new out port at the end of the storage list
	 *
	 *@param  port  The port to add
	 */
	public void addOut(Port port) {
		addOut(outPorts.length(), port);
	}

	/**
	 *  Adds a new out port at the given index in the storage list
	 *
	 *@param  index  	The index
	 *@param  port  	The port to add
	 */
	public void addOut(int i, Port p) {
		outPorts.setPort(i, p);
		update(new TmActionImpl(TmActionImpl.ADD, this, p, null, null));
	}

	/**
	 *  Gets the list of out ports
	 *
	 *@return    The out port list
	 */
	public PortList outPorts() {
		return outPorts;
	}

	/**
	 * Prints the XML expression of the Transformation instance
	 */
	public abstract void generateXML();

	/**
	 * Prints the Java code for the execution of Transformation instance
	 *
	 *@param  file             The writer used for printing java code in a file
	 *@param  externs          The list of external components 
	 *@exception  IOException  When IO errors occur
	 */
	public abstract void generateJavaCode(Writer file, LinearIndexedStruct<String> externs) throws IOException;

	/**
	 * This is the trivial case if all the output is null then all the input are null *
	 *
	 *@param  out  Description of the Parameter
	 *@param  in   Description of the Parameter
	 */
	public void retroNull(PortList out, PortList in) {
		boolean nullified = true;
		for (int i = 0; nullified && (i != out.length()); i++) {
			if (!out.getPort(i).getChannel().nullifiedP()) {
				nullified = false;
			}
		}
		for (int i = 0; i != in.length(); i++) {
			in.getPort(i).getChannel().setVisited();
			if (nullified) {
				in.getPort(i).getChannel().nullify();
			}
		}
	}


	/**
	 *  This method is used by FlowComposer ( a GUI tool for Transmorpher)
	 *
	 *@param  object  Description of the Parameter
	 */
	public void update(Object object) {
		setChanged();
		notifyObservers(object);
	}

}

