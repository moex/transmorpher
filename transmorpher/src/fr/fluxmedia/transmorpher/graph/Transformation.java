/*
 * $Id: Transformation.java,v 1.2 2003-01-30 15:47:46 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.transmorpher.graph;

import fr.fluxmedia.transmorpher.utils.LinearIndexedStruct;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
import fr.fluxmedia.transmorpher.utils.Writer;

import java.io.IOException;

/**
 * An object that implements the Transformation interface is a component
 * of a transmorpher graph. It can  contain other compononents .
 * Methods are provided to generate an XML or a Java code representation
 * of the Transformation. *
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */

public interface Transformation {

	/**
	 * Prints the XML expression of the Transformation
	 */
	public void generateXML();

	/**
	 * Prints the Java code for the execution of the Transformation
	 *
	 *@param  file             The Writer used for writing java code in a file
	 *@param  externs          List of external definition of component
	 *@exception  IOException  when an IO errors occur
	 */
	public void generateJavaCode(Writer file, LinearIndexedStruct<String> externs) throws IOException;

	/**
	 *  Gets the name attribute of the Transformation object
	 *
	 *@return    The name value
	 */
	public String getName();

	/**
	 *  Sets the name attribute of the Transformation object
	 *
	 *@param  name  The new name value
	 */
	public void setName(String name);

	/**
	 *  Gets the type attribute of the Transformation object
	 *
	 *@return    The type value
	 */
	public String getType();

	/**
	 *  Sets the type attribute of the Transformation object
	 *
	 *@param  type  The new type value
	 */
	public void setType(String type);

	/**
	 *  Sets the parameters attribute of the Transformation object
	 *
	 *@param  param  The new parameters value
	 */
	public void setParameters(Parameters param);

	/**
	 *  Gets the parameters attribute of the Transformation object
	 *
	 *@return    The parameters value
	 */
	public Parameters getParameters();

	/**
	 *  Gets the attributes attribute of the Transformation object
	 *
	 *@return    The attributes value
	 */
	public StringParameters getAttributes();

	/**
	 *  Gets the transmorpher attribute of the Transformation object
	 *
	 *@return    The transmorpher value
	 */
	public Transmorpher getTransmorpher();

	/**
	 *  Gets the in ports list of this Trasnformation instance
	 *
	 *@return    The list of ports
	 */
	public PortList inPorts();

	/**
	 *  Gets the in ports list of this Trasnformation instance
	 *
	 *@return    The list of ports
	 */
	public PortList outPorts();

	/**
	 *  Description of the Method
	 *
	 *@param  out  Description of the Parameter
	 *@param  in   Description of the Parameter
	 */
	public void retroNull(PortList out, PortList in);

	/**
	 *  This method is specific to FlowComposer (a GUI tool for Transmorpher)
	 *
	 *@param  object  Description of the Parameter
	 */
	public void update(Object object);
}

