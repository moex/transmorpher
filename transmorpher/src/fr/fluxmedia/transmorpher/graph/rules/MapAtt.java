/**
 * $Id: MapAtt.java,v 1.4 2004-02-24 15:08:00 jerome Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2004 INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * Transmorpher graph rule node interface
 *
 *@since     jdk 1.3 / SAX 2.0
 *@author    Jerome.Euzenat@inrialpes.fr
 */

package fr.fluxmedia.transmorpher.graph.rules;

import fr.fluxmedia.transmorpher.utils.Writer;

import java.io.IOException;

/**
 * Transmorpher graph rule node interface
 *
 *@since     jdk 1.3 / SAX 2.0
 *@author    Jerome.Euzenat@inrialpes.fr
 */
public class MapAtt implements Rule {

	/**
	 *  Description of the Field
	 */
	public String match = null;
	/**
	 *  Description of the Field
	 */
	public String target = null;
	/**
	 *  Description of the Field
	 */
	public String context = null;

	/**
	 *Constructor for the MapAtt object
	 */
	public MapAtt() {
		super();

	}

	/**
	 *Constructor for the MapAtt object
	 *
	 *@param  m  Description of the Parameter
	 *@param  t  Description of the Parameter
	 */
	public MapAtt(String m, String t) {
		super();
		match = m;
		target = t;
	}

	/**
	 *Constructor for the MapAtt object
	 *
	 *@param  m  Description of the Parameter
	 *@param  t  Description of the Parameter
	 *@param  c  Description of the Parameter
	 */
	public MapAtt(String m, String t, String c) {
		this(m, t);
		context = c;
	}


	/**
	 *  Sets the match attribute of the MapAtt object
	 *
	 *@param  match  The new match value
	 */
	public void setMatch(String match) {
		this.match = match;
	}


	/**
	 *  Gets the match attribute of the MapAtt object
	 *
	 *@return    The match value
	 */
	public String getMatch() {
		return match;
	}

	/**
	 *  Sets the target attribute of the MapAtt object
	 *
	 *@param  target  The new target value
	 */
	public void setTarget(String target) {
		this.target = target;
	}


	/**
	 *  Description of the Method
	 *
	 *@return    Description of the Return Value
	 */
	public String getTarget() {
		return target;
	}

	/**
	 *  Sets the context attribute of the MapAtt object
	 *
	 *@param  context  The new context value
	 */
	public void setContext(String context) {
		this.context = context;
	}


	/**
	 *  Gets the context attribute of the MapAtt object
	 *
	 *@return    The context value
	 */
	public String getContext() {
		return context;
	}

	/**
	 * Prints the XML expression of the rule
	 */
	public void generateXML() {
		System.out.print("    <mapatt match=\"" + match + "\"");
		if (context != null) {
			System.out.print(" context=\"" + context + "\"");
		}
		System.out.println("target=\"" + target + "/>");
	}

	/**
	 *  Description of the Method
	 *
	 *@param  file             Description of the Parameter
	 *@exception  IOException  Description of the Exception
	 */
	public void generateXSLTCode(Writer file) throws IOException {
		file.writeln(2, "<!-- Mapping attributes " + match + " to " + target + " -->");
		file.write(2, "<xsl:template match=\"");
		if (context != null) {
			file.write(context + "/");
		}
		file.writeln("@" + match + "\">");
		file.writeln(4, "<xsl:attribute name=\"" + target + "\">");
		file.write("<xsl:value-of select=\".\"/>");
		file.writeln(4, "</xsl:attribute>");
		file.writeln(2, "</xsl:template>");
		file.writeln("");
	}

	/**
	 * Prints the XSLT code inside a ModTag template
	 *
	 *@param  file             Description of the Parameter
	 *@exception  IOException  Description of the Exception
	 */
	public void generateInsideXSLTCode(Writer file) throws IOException {
		file.writeln(6, "<!-- Mapping attributes " + match + " to " + target + " -->");
		file.writeln(6, "<xsl:attribute name=\"" + target + "\">");
		file.write("<xsl:value-of select=\"@" + match + "\"/>");
		file.writeln(6, "</xsl:attribute>");
		file.writeln("");
	}

}

