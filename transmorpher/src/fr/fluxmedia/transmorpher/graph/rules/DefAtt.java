/**
 * $Id: DefAtt.java,v 1.2 2003-01-16 13:37:41 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */



package fr.fluxmedia.transmorpher.graph.rules;

import fr.fluxmedia.transmorpher.utils.Writer;

import java.io.IOException;

/**
 * Transmorpher default attribute rule
 *
 *@since     jdk 1.3 / SAX 2.0
 *@author    Jerome.Euzenat@inrialpes.fr
 */
public class DefAtt implements Rule {

	/**
	 *  Description of the Field
	 */
	public String match = null;
	/**
	 *  Description of the Field
	 */
	public String value = null;
	/**
	 *  Description of the Field
	 */
	public String context = null;


	/**
	 *Constructor for the DefAtt object
	 */
	public DefAtt() {
		super();
	}

	/**
	 *Constructor for the DefAtt object
	 *
	 *@param  m  Description of the Parameter
	 *@param  v  Description of the Parameter
	 */
	public DefAtt(String m, String v) {
		super();
		match = m;
		value = v;
	}

	/**
	 *Constructor for the DefAtt object
	 *
	 *@param  m  Description of the Parameter
	 *@param  v  Description of the Parameter
	 *@param  c  Description of the Parameter
	 */
	public DefAtt(String m, String v, String c) {
		this(m, v);
		context = c;
	}

	/**
	 *  Sets the match attribute of the DefAtt object
	 *
	 *@param  match  The new match value
	 */
	public void setMatch(String match) {
		this.match = match;
	}

	/**
	 *  Gets the match attribute of the DefAtt object
	 *
	 *@return    The match value
	 */
	public String getMatch() {
		return match;
	}

	/**
	 *  Gets the context attribute of the DefAtt object
	 *
	 *@return    The context value
	 */
	public String getContext() {
		return context;
	}

	/**
	 *  Sets the context attribute of the DefAtt object
	 *
	 *@param  context  The new context value
	 */
	public void setContext(String context) {
		this.context = context;
	}

	/**
	 *  Gets the value attribute of the DefAtt object
	 *
	 *@return    The value value
	 */
	public String getValue() {
		return value;
	}

	/**
	 *  Sets the value attribute of the DefAtt object
	 *
	 *@param  value  The new value value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Prints the XML expression of the rule
	 */
	public void generateXML() {
		System.out.print("    <defatt match=\"" + match + "\"");
		if (context != null) {
			System.out.print(" context=\"" + context + "\"");
		}
		System.out.println("value=\"" + value + "/>");
	}

	/**
	 *  Description of the Method
	 *
	 *@param  file             Description of the Parameter
	 *@exception  IOException  Description of the Exception
	 */
	public void generateXSLTCode(Writer file) throws IOException {
		file.writeln(2, "<!-- Default attributes for " + match + ": " + value + " -->");
		file.write(2, "<xsl:template match=\"");
		if (context != null) {
			file.write(context + "/");
		}
		file.writeln("@" + match + "\">");
		file.writeln(4, "<xsl:attribute name=\"" + value + "\">");
		file.write("<xsl:value-of select=\"@" + match + "\"/>");
		file.writeln(4, "</xsl:attribute>");
		file.writeln(2, "</xsl:template>");
		file.writeln("");
	}

	/**
	 * Prints the XSLT code inside a ModTag template
	 *This is the only way for a default attribute rule to work
	 *
	 *@param  file             Description of the Parameter
	 *@exception  IOException  Description of the Exception
	 */
	public void generateInsideXSLTCode(Writer file) throws IOException {
		file.writeln(6, "<!-- Default attributes for " + match + ": " + value + " -->");
		file.writeln(6, "<xsl:if test=\"not(@\"" + match + ")\">");
		file.writeln(8, "<xsl:attribute name=\"" + match + "\">");
		file.write(value);
		file.writeln(8, "</xsl:attribute>");
		file.writeln(6, "</xsl:if>");
		file.writeln("");
	}

}

