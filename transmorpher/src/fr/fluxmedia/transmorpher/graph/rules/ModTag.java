/**
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2004, 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


package fr.fluxmedia.transmorpher.graph.rules;

import fr.fluxmedia.transmorpher.utils.TMException;

import fr.fluxmedia.transmorpher.utils.Writer;
import java.io.IOException;

import java.util.ArrayList;

/**
 * Transmorpher graph rule node interface
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */
public class ModTag implements Rule {

	/**
	 *  Description of the Field
	 */
	public ArrayList<Rule> rules = null;
	/**
	 *  Description of the Field
	 */
	public String match = null;
	/**
	 *  Description of the Field
	 */
	public String context = null;
	/**
	 *  Description of the Field
	 */
	public String target = null;


	/**
	 *Constructor for the ModTag object
	 */
	public ModTag() {
		super();
		rules = new ArrayList<Rule>(10);
	}

	/**
	 *Constructor for the ModTag object
	 *
	 *@param  m  Description of the Parameter
	 */
	public ModTag(String m) {
		super();
		match = m;
		rules = new ArrayList<Rule>(10);
	}

	/**
	 *Constructor for the ModTag object
	 *
	 *@param  m  Description of the Parameter
	 *@param  c  Description of the Parameter
	 */
	public ModTag(String m, String c) {
		this(m);
		context = c;
	}

	/**
	 *Constructor for the ModTag object
	 *
	 *@param  m  Description of the Parameter
	 *@param  t  Description of the Parameter
	 *@param  c  Description of the Parameter
	 */
	public ModTag(String m, String t, String c) {
		this(m, c);
		target = t;
	}


	/**
	 *  Sets the match attribute of the ModTag object
	 *
	 *@param  match  The new match value
	 */
	public void setMatch(String match) {
		this.match = match;
	}


	/**
	 *  Gets the match attribute of the ModTag object
	 *
	 *@return    The match value
	 */
	public String getMatch() {
		return match;
	}


	/**
	 *  Sets the target attribute of the ModTag object
	 *
	 *@param  target  The new target value
	 */
	public void setTarget(String target) {
		this.target = target;
	}


	/**
	 *  Gets the target attribute of the ModTag object
	 *
	 *@return    The target value
	 */
	public String getTarget() {
		return target;
	}


	/**
	 *  Sets the context attribute of the ModTag object
	 *
	 *@param  context  The new context value
	 */
	public void setContext(String context) {
		this.context = context;
	}


	/**
	 *  Gets the context attribute of the ModTag object
	 *
	 *@return    The context value
	 */
	public String getContext() {
		return context;
	}

	/**
	 *  Gets the rules attribute of the ModTag object
	 *
	 *@return    The rules value
	 */
	public ArrayList<Rule> getRules() {
		return rules;
	}

	/**
	 *  Adds a feature to the Rule attribute of the ModTag object
	 *
	 *@param  r  The feature to be added to the Rule attribute
	 */
	public void addRule(Rule r) {
		rules.add( r );
	}

	/**
	 * Prints the XML expression of the rule
	 */
	public void generateXML() {
		System.out.print("    <modtag match=\"" + match + "\"");
		if (target != null) {
			System.out.print(" target=\"" + target + "\"");
		}
		if (context != null) {
			System.out.print(" context=\"" + context + "\"");
		}
		System.out.println(">");
		for ( Rule rl: rules ) {
		    rl.generateXML();
		}
		System.out.println("</modtag>");
	}

	/**
	 *  Description of the Method
	 *
	 *@param  file             Description of the Parameter
	 *@exception  TMException  Description of the Exception
	 *@exception  IOException  Description of the Exception
	 */
	public void generateXSLTCode(Writer file) throws TMException, IOException {
	    file.writeln(2, "<!-- Modifying element " + match + " -->");
	    file.write(2, "<xsl:template match=\"");
	    if (context != null) {
		file.write(context + "/");
	    }
	    file.writeln(match + "\">");
	    if (target != null) {
		file.write(4, "<" + target + " xsl:exclude-result-prefixes=\"regexp #default\">");
	    } else {
		file.writeln(4, "<xsl:copy>");
	    }
	    for ( Rule rl: rules ) {
		rl.generateInsideXSLTCode(file);
	    }
	    file.writeln(6, "<xsl:apply-templates select=\"*|@*|text()\"/>");
	    if (target != null) {
		file.write(4, "</" + target + ">");
	    } else {
		file.writeln(4, "</xsl:copy>");
	    }
	    file.writeln(2, "</xsl:template>");
	    file.writeln("");
	    for ( Rule r: rules ) {
		if ((r instanceof RemAtt) || (r instanceof MapAtt)) {
		    RemAtt rr = (RemAtt)r;
		    file.writeln(2, "<!-- Removing attributes " + rr.match
				 + " on elements " + rr.context + " -->");
		    file.writeln(4, "  <xsl:template match=\"" + rr.context + "/@" + rr.match + "\"/>");
		    file.writeln(2, "");
		}
	    }
	    file.writeln("");
	}

	/**
	 * Prints the XSLT code inside a ModTag template
	 *
	 *@param  file             Description of the Parameter
	 *@exception  TMException  Description of the Exception
	 */
	public void generateInsideXSLTCode(Writer file) throws TMException {
		throw new TMException("[ModTag]generateInsideXSLTCode : not yet implemented");
	}

}

