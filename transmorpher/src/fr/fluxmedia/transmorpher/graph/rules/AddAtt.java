/**
 * $Id: AddAtt.java,v 1.3 2003-01-16 13:37:41 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */



package fr.fluxmedia.transmorpher.graph.rules;

import fr.fluxmedia.transmorpher.utils.Writer;
import java.io.IOException;

/**
 * Transmorpher graph rule node interface
 *
 *@since     jdk 1.3 / SAX 2.0
 *@author    Jerome.Euzenat@inrialpes.fr
 */
public class AddAtt implements Rule {

	/**
	 *  Description of the Field
	 */
	public String match = null;
	/**
	 *  Description of the Field
	 */
	public String value = null;

	/**
	 *Constructor for the AddAtt object
	 */
	public AddAtt() {
		super();
	}

	/**
	 *Constructor for the AddAtt object
	 *
	 *@param  n  Description of the Parameter
	 *@param  v  Description of the Parameter
	 */
	public AddAtt(String n, String v) {
		super();
		match = n;
		value = v;
	}

	/**
	 *  Sets the match attribute of the AddAtt object
	 *
	 *@param  match  The new match value
	 */
	public void setMatch(String match) {
		this.match = match;
	}

	/**
	 *  Sets the value attribute of the AddAtt object
	 *
	 *@param  value  The new value value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 *  Gets the match attribute of the AddAtt object
	 *
	 *@return    The match value
	 */
	public String getMatch() {
		return match;
	}

	/**
	 *  Gets the value attribute of the AddAtt object
	 *
	 *@return    The value value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Prints the XML expression of the rule
	 */
	public void generateXML() {
		System.out.println("    <addatt match=\"" + match + "\" value=\"" + value + "\"/>");
	}

	/**
	 *  Description of the Method
	 *
	 *@param  file             Description of the Parameter
	 *@exception  IOException  Description of the Exception
	 */
	public void generateXSLTCode(Writer file) throws IOException {
		file.writeln(6, "<!-- Adding attributes " + match + " -->");
		file.write(6, "<xsl:attribute name=\"" + match + "\">");
		file.write(value);
		file.write("</xsl:attribute>");
		file.writeln("");
	}

	/**
	 * Prints the XSLT code inside a ModTag template
	 *
	 *@param  file             Description of the Parameter
	 *@exception  IOException  Description of the Exception
	 */
	public void generateInsideXSLTCode(Writer file) throws IOException {
		generateXSLTCode(file);
	}

}

