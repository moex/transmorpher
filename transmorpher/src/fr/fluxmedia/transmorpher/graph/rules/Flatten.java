/**
 * $Id: Flatten.java,v 1.5 2004-02-24 16:27:06 jerome Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2004 INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


package fr.fluxmedia.transmorpher.graph.rules;
import fr.fluxmedia.transmorpher.utils.TMException;

import fr.fluxmedia.transmorpher.utils.Writer;

import java.io.IOException;

/**
 * Transmorpher graph rule node interface
 *
 *@since     jdk 1.3 / SAX 2.0
 *@author    Jerome.Euzenat@inrialpes.fr
 */
public class Flatten implements Rule {

	/**
	 *  Description of the Field
	 */
	public String match = null;

	/**
	 *Constructor for the Flatten object
	 */
	public Flatten() {
		super();
	}

	/**
	 *Constructor for the Flatten object
	 *
	 *@param  m  Description of the Parameter
	 */
	public Flatten(String m) {
		super();
		match = m;
	}

	/**
	 *  Sets the match attribute of the Flatten object
	 *
	 *@param  match  The new match value
	 */
	public void setMatch(String match) {
		this.match = match;
	}

	/**
	 *  Gets the match attribute of the Flatten object
	 *
	 *@return    The match value
	 */
	public String getMatch() {
		return match;
	}

	/**
	 * Prints the XML expression of the rule
	 */
	public void generateXML() {
		System.out.print("    <flatten match=\"" + match + "\"/>");
	}

	/**
	 *  Description of the Method
	 *
	 *@param  file             Description of the Parameter
	 *@exception  IOException  Description of the Exception
	 */
	public void generateXSLTCode(Writer file) throws IOException {
		/**
		 *  Flatten specification:
		 *(A (x) (A y) (z)) is reduced into (A x y z)
		 *This is applied recursivly, i.e. let T being the Flatten transformations it is defiend by:
		 *T((A x...y)) -> (A S(x)... S(y))
		 *T((B x...y)) -> (B T(x)...T(y)): this is default template
		 *S((A x...y)) -> S(x)...S(y)
		 *S((B x...y)) -> (B T(x)...T(y))
		 *T= default mode, S=insideA mode
		 */
		file.writeln(2, "<!-- Flatten for " + match + " (default mode) -->");
		file.writeln(2, "<xsl:template match=\"" + match + "\">");
		file.writeln(4, "<xsl:copy>");
		file.writeln(6, "<xsl:apply-templates select=\"./*|./text()\" mode=\"inside" + match + "\"/>");
		file.writeln(4, "</xsl:copy>");
		file.writeln(2, "</xsl:template>");
		file.writeln("");

		file.writeln(2, "<!-- Flatten for " + match + " (inside mode) -->");
		file.writeln(2, "<xsl:template match=\"" + match + "\" mode=\"inside" + match + "\">");
		file.writeln(4, "<xsl:for-each select=\"*|text()\">");
		file.writeln(6, "<xsl:copy>");
		file.writeln(8, "<xsl:apply-templates mode=\"inside" + match + "\"/>");
		file.writeln(6, "</xsl:copy>");
		file.writeln(4, "</xsl:for-each>");
		file.writeln(2, "</xsl:template>");
		file.writeln("");
		file.writeln(2, "<xsl:template match=\"*|@*|text()\" mode=\"inside" + match + "\">");
		file.writeln(4, "<xsl:apply-templates select=\".\"/>");
		file.writeln(2, "</xsl:template>");
		file.writeln("");
	}

	/**
	 * Prints the XSLT code inside a ModTag template
	 *
	 *@param  file             Description of the Parameter
	 *@exception  TMException  Description of the Exception
	 */
	public void generateInsideXSLTCode(Writer file) throws TMException {
		throw new TMException("[Flatten]generateInsideXSLTCode : not yet implemented");
	}

}

