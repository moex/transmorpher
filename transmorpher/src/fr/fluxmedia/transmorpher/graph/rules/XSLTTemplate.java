/**
 * $Id: XSLTTemplate.java,v 1.2 2003-01-16 13:37:41 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.graph.rules;

import fr.fluxmedia.transmorpher.utils.Writer;

/**
 * Transmorpher graph XSLT Template node
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */
public class XSLTTemplate implements Rule {

	/**
	 *  Description of the Field
	 */
	public String match = null;
	/**
	 *  Description of the Field
	 */
	public String mode = null;
	/**
	 *  Description of the Field
	 */
	public String content = null;

	/**
	 *Constructor for the XSLTTemplate object
	 */
	public XSLTTemplate() {
		super();
	}

	/**
	 *Constructor for the XSLTTemplate object
	 *
	 *@param  m   Description of the Parameter
	 *@param  md  Description of the Parameter
	 */
	public XSLTTemplate(String m, String md) {
		super();
		match = m;
		mode = md;
	}

	/**
	 *  Sets the mode attribute of the XSLTTemplate object
	 *
	 *@param  mode  The new mode value
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}

	/**
	 *  Gets the mode attribute of the XSLTTemplate object
	 *
	 *@return    The mode value
	 */
	public String getMode() {
		return mode;
	}

	/**
	 *  Sets the match attribute of the XSLTTemplate object
	 *
	 *@param  match  The new match value
	 */
	public void setMatch(String match) {
		this.match = match;
	}

	/**
	 *  Gets the match attribute of the XSLTTemplate object
	 *
	 *@return    The match value
	 */
	public String getMatch() {
		return match;
	}

	/**
	 *  Gets the content attribute of the XSLTTemplate object
	 *
	 *@return    The content value
	 */
	public String getContent() {
		return content;
	}

	/**
	 *  Sets the content attribute of the XSLTTemplate object
	 *
	 *@param  c  The new content value
	 */
	public void setContent(String c) {
		content = c;
	}

	/**
	 * Prints the XML expression of the rule
	 */
	public void generateXML() {
		System.out.print("    <xsl:template match=\"" + match + "\"");
		if (mode != null) {
			System.out.print(" mode=\"" + mode + "\">");
		} else {
			System.out.print(">");
		}
		// JE: Certainly not the corect way to do it
		System.out.println(content);
		System.out.print("    </xsl:template>");
	}

	/**
	 *  Description of the Method
	 *
	 *@param  file  Description of the Parameter
	 */
	public void generateXSLTCode(Writer file) {
		// JE: set the current output to RW
		generateXML();
	}

	/**
	 * Prints the XSLT code inside a ModTag template
	 *
	 *@param  file  Description of the Parameter
	 */
	public void generateInsideXSLTCode(Writer file) {
		// JE: Certainly not the corect way to do it
		System.out.println(content);
	}

}

