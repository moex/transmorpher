/**
 * $Id: RESubst.java,v 1.2 2003-01-16 13:37:41 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.graph.rules;
import fr.fluxmedia.transmorpher.utils.TMException;

import fr.fluxmedia.transmorpher.utils.Writer;

import java.io.IOException;

/**
 * Transmorpher graph rule node interface
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */
public class RESubst implements Rule {

	/**
	 *  Description of the Field
	 */
	public String match = null;
	/**
	 *  Description of the Field
	 */
	public String context = null;
	/**
	 *  Description of the Field
	 */
	public String source = null;
	/**
	 *  Description of the Field
	 */
	public String target = null;
	/**
	 *  Description of the Field
	 */
	public String apply = "all";

	/**
	 *Constructor for the RESubst object
	 */
	public RESubst() {
		super();
	}

	/**
	 *Constructor for the RESubst object
	 *
	 *@param  m  Description of the Parameter
	 *@param  s  Description of the Parameter
	 *@param  t  Description of the Parameter
	 */
	public RESubst(String m, String s, String t) {
		super();
		match = m;
		source = s;
		target = t;
	}

	/**
	 *Constructor for the RESubst object
	 *
	 *@param  m  Description of the Parameter
	 *@param  c  Description of the Parameter
	 *@param  s  Description of the Parameter
	 *@param  t  Description of the Parameter
	 *@param  a  Description of the Parameter
	 */
	public RESubst(String m, String c, String s, String t, String a) {
		this(m, s, t);
		context = c;
		apply = a;
	}

	/**
	 *  Sets the match attribute of the RESubst object
	 *
	 *@param  match  The new match value
	 */
	public void setMatch(String match) {
		this.match = match;
	}

	/**
	 *  Gets the match attribute of the RESubst object
	 *
	 *@return    The match value
	 */
	public String getMatch() {
		return match;
	}

	/**
	 *  Sets the context attribute of the RESubst object
	 *
	 *@param  context  The new context value
	 */
	public void setContext(String context) {
		this.context = context;
	}

	/**
	 *  Gets the context attribute of the RESubst object
	 *
	 *@return    The context value
	 */
	public String getContext() {
		return context;
	}

	/**
	 *  Sets the target attribute of the RESubst object
	 *
	 *@param  target  The new target value
	 */
	public void setTarget(String target) {
		this.target = target;
	}

	/**
	 *  Gets the target attribute of the RESubst object
	 *
	 *@return    The target value
	 */
	public String getTarget() {
		return target;
	}

	/**
	 *  Sets the source attribute of the RESubst object
	 *
	 *@param  source  The new source value
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 *  Gets the source attribute of the RESubst object
	 *
	 *@return    The source value
	 */
	public String getSource() {
		return source;
	}

	/**
	 *  Sets the apply attribute of the RESubst object
	 *
	 *@param  apply  The new apply value
	 */
	public void setApply(String apply) {
		this.apply = apply;
	}

	/**
	 *  Gets the apply attribute of the RESubst object
	 *
	 *@return    The apply value
	 */
	public String getApply() {
		return apply;
	}

	/**
	 * Prints the XML expression of the rule
	 */
	public void generateXML() {
		System.out.print("    <resubst match=\"" + match + "\"");
		if (context != null) {
			System.out.print(" context=\"" + context + "\"");
		}
		System.out.println(" source=\"" + source + "\" target=\"" + target + "\"/>");
	}

	/**
	 *  Description of the Method
	 *
	 *@param  file             Description of the Parameter
	 *@exception  IOException  Description of the Exception
	 */
	public void generateXSLTCode(Writer file) throws IOException {
		int arob = match.indexOf("@");
		file.writeln(2, "<!-- Substituting " + apply + " " + source + " by " + target + " in " + match + " -->");
		file.write(2, "<xsl:template match =\"");
		if (context != null) {
			file.write(context + "/");
		}
		file.writeln(match + "\">");
		file.writeln(4, "<xsl:if test=\"function-available('regexp:substitute') and function-available('regexp:substituteAll') \">");

		if (arob > 0) {
			file.writeln(6, "<xsl:attribute name=\"" + match.substring(arob + 1) + "\">");
		}
		if (apply.equals("once")) {
			file.writeln(8, "<xsl:value-of select=\"regexp:substitute(.,'" + source + "','" + target + "')\"/>");
		} else {
			file.writeln(8, "<xsl:value-of select=\"regexp:substituteAll(.,'" + source + "','" + target + "')\"/>");
		}//end if
		if (arob > 0) {
			file.writeln(6, "</xsl:attribute>");
		}
		file.writeln(4, "</xsl:if>");
		file.writeln(2, "</xsl:template>");
		file.writeln("");
	}

	/**
	 * Prints the XSLT code inside a ModTag template
	 *
	 *@param  file             Description of the Parameter
	 *@exception  TMException  Description of the Exception
	 */
	public void generateInsideXSLTCode(Writer file) throws TMException {
		throw new TMException("[RESubst]generateInsideXSLTCode : not yet implemented");
	}

}

