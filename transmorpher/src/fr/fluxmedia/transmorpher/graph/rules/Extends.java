/**
 * $Id: Extends.java,v 1.2 2003-01-16 13:37:41 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */



package fr.fluxmedia.transmorpher.graph.rules;

import fr.fluxmedia.transmorpher.utils.Writer;

import java.io.IOException;

/**
 * Transmorpher graph rule node interface
 *
 *@since     jdk 1.3 / SAX 2.0
 *@author    Jerome.Euzenat@inrialpes.fr
 */
public class Extends implements Rule {

	/**
	 *  Description of the Field
	 */
	public String ruleset = null;

	/**
	 *Constructor for the Extends object
	 */
	public Extends() {
		super();
	}

	/**
	 *Constructor for the Extends object
	 *
	 *@param  r  Description of the Parameter
	 */
	public Extends(String r) {
		super();
		ruleset = r;
	}

	/**
	 *  Gets the ruleset attribute of the Extends object
	 *
	 *@return    The ruleset value
	 */
	public String getRuleset() {
		return ruleset;
	}

	/**
	 *  Sets the ruleset attribute of the Extends object
	 *
	 *@param  ruleset  The new ruleset value
	 */
	public void setRuleset(String ruleset) {
		this.ruleset = ruleset;
	}

	/**
	 * Prints the XML expression of the rule
	 */
	public void generateXML() {
		System.out.print("    <extends href=\"" + ruleset + "\"/>");
	}

	/**
	 *  Description of the Method
	 *
	 *@param  file             Description of the Parameter
	 *@exception  IOException  Description of the Exception
	 */
	public void generateXSLTCode(Writer file) throws IOException {
		file.writeln(2, "<!-- Importing ruleset " + ruleset + "-->");
		file.writeln(2, "<xsl:import href=\"" + ruleset + ".xsl\"/>");
		file.writeln("");
	}

	/**
	 * Prints the XSLT code inside a ModTag template
	 *
	 *@param  file  Description of the Parameter
	 */
	public void generateInsideXSLTCode(Writer file) { }

}

