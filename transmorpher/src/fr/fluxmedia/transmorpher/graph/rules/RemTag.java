/**
 * $Id: RemTag.java,v 1.2 2003-01-16 13:37:41 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


package fr.fluxmedia.transmorpher.graph.rules;
import fr.fluxmedia.transmorpher.utils.TMException;
import fr.fluxmedia.transmorpher.utils.Writer;

import java.io.IOException;

/**
 * Transmorpher graph rule node interface
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */
public class RemTag implements Rule {

	/**
	 *  Description of the Field
	 */
	public String match = null;
	/**
	 *  Description of the Field
	 */
	public String context = null;

	/**
	 *Constructor for the RemTag object
	 */
	public RemTag() {
		super();
	}

	/**
	 *Constructor for the RemTag object
	 *
	 *@param  m  Description of the Parameter
	 */
	public RemTag(String m) {
		super();
		match = m;
	}

	/**
	 *Constructor for the RemTag object
	 *
	 *@param  m  Description of the Parameter
	 *@param  c  Description of the Parameter
	 */
	public RemTag(String m, String c) {
		this(m);
		context = c;
	}

	/**
	 *  Sets the match attribute of the RemTag object
	 *
	 *@param  match  The new match value
	 */
	public void setMatch(String match) {
		this.match = match;
	}


	/**
	 *  Gets the match attribute of the RemTag object
	 *
	 *@return    The match value
	 */
	public String getMatch() {
		return match;
	}


	/**
	 *  Sets the context attribute of the RemTag object
	 *
	 *@param  context  The new context value
	 */
	public void setContext(String context) {
		this.context = context;
	}

	/**
	 *  Gets the context attribute of the RemTag object
	 *
	 *@return    The context value
	 */
	public String getContext() {
		return context;
	}

	/**
	 * Prints the XML expression of the rule
	 */
	public void generateXML() {
		System.out.print("    <remtag match=\"" + match + "\"");
		if (context != null) {
			System.out.print(" context=\"" + context + "\"");
		}
		System.out.println("/>");
	}

	/**
	 *  Description of the Method
	 *
	 *@param  file             Description of the Parameter
	 *@exception  IOException  Description of the Exception
	 */
	public void generateXSLTCode(Writer file) throws IOException {
		file.writeln(2, "<!-- Removing elements " + match + " -->");
		file.write(2, "<xsl:template match=\"");
		if (context != null) {
			file.write(context + "/");
		}
		file.writeln(match + "\"/>");
		file.writeln("");
	}

	/**
	 * Prints the XSLT code inside a ModTag template
	 *
	 *@param  file             Description of the Parameter
	 *@exception  TMException  Description of the Exception
	 */
	public void generateInsideXSLTCode(Writer file) throws TMException {
		throw new TMException("[RemTag]generateInsideXSLTCode : not yet implemented");
	}

}

