/**
 * $Id: Namespace.java,v 1.2 2003-01-16 13:37:41 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


package fr.fluxmedia.transmorpher.graph.rules;

import fr.fluxmedia.transmorpher.utils.Writer;

import java.io.IOException;

/**
 * Transmorpher graph namespace interface
 *
 *@since     jdk 1.3 / SAX 2.0
 *@author    Jerome.Euzenat@inrialpes.fr
 */
public class Namespace implements Rule {

	/**
	 *  Description of the Field
	 */
	public String name = null;
	/**
	 *  Description of the Field
	 */
	public String uri = null;

	/**
	 *Constructor for the Namespace object
	 */
	public Namespace() {
		super();

	}

	/**
	 *Constructor for the Namespace object
	 *
	 *@param  n  Description of the Parameter
	 *@param  v  Description of the Parameter
	 */
	public Namespace(String n, String v) {
		super();
		name = n;
		uri = v;
	}

	/**
	 *  Sets the name attribute of the Namespace object
	 *
	 *@param  name  The new name value
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 *  Gets the name attribute of the Namespace object
	 *
	 *@return    The name value
	 */
	public String getName() {
		return name;
	}


	/**
	 *  Sets the uRI attribute of the Namespace object
	 *
	 *@param  uri  The new uRI value
	 */
	public void setURI(String uri) {
		this.uri = uri;
	}


	/**
	 *  Gets the uRI attribute of the Namespace object
	 *
	 *@return    The uRI value
	 */
	public String getURI() {
		return uri;
	}

	/**
	 * Prints the XML expression of the rule
	 */
	public void generateXML() {
		System.out.println("    <namespace name=\"" + name + "\" uri=\"" + uri + "\"/>");
	}

	/**
	 *  Description of the Method
	 *
	 *@param  file             Description of the Parameter
	 *@exception  IOException  Description of the Exception
	 */
	public void generateXSLTCode(Writer file) throws IOException {
		file.writeln(2, "xmlns:" + name + "=\"" + uri + "\"");
	}

	/**
	 * Prints the XSLT code inside a ModTag template
	 *
	 *@param  file  Description of the Parameter
	 */
	public void generateInsideXSLTCode(Writer file) { }

}

