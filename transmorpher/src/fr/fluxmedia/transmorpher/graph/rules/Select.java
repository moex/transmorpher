/**
 * $Id: Select.java,v 1.2 2003-01-16 13:37:41 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */



package fr.fluxmedia.transmorpher.graph.rules ;

import java.io.IOException;

import fr.fluxmedia.transmorpher.utils.Writer;
/**
  * Transmorpher graph rule node interface
  *
  * @author Jerome.Euzenat@inrialpes.fr
  * @since jdk 1.3 / SAX 2.0
  */
public class Select implements Rule {

  public String match = null;
	public String test = null;

	 public Select(){
    super();
  }
	
  public Select(String m){
    super();
    match = m;
  }

	public Select(String m,String test){
    this(m);
		this.test=test;
  }
	
	public void setMatch(String match) {
		this.match = match;
	}

	
	public String getMatch() {
		return match;
	}

	
	public void setTest(String test) {
		this.test = test;
	}
	
	public String getTest() {
		return test;
	}
  /** Prints the XML expression of the rule */
  public void generateXML(){
		if(test==null)
			System.out.println("    <select match=\""+match+"\"/>");
		else
			System.out.println("    <select match=\""+match+"\" test=\""+test+"\"/>");
  }

  /** Prints the XSLT template for the rule */
  public void generateXSLTCode(Writer file) throws IOException {
    file.writeln(2,"<!-- Selection -->");
    file.writeln(2,"<xsl:template match=\""+match+"\">");
    if(test!=null) file.writeln(2,"<xsl:if test=\""+test+"\">");
    file.writeln(4,"<xsl:copy-of select=\".\"/>");
    if(test!=null) file.writeln(2,"</xsl:if>");
    file.writeln(2,"</xsl:template>");
    file.writeln("");
  }

    /** Prints the XSLT template for the rule */
    public void generateInsideXSLTCode(Writer file){}

}
