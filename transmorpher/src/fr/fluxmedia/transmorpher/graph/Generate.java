/*
 * $Id: Generate.java,v 1.5 2003-01-28 15:40:53 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
/**
 * CAUTION, This will have to change the parameter cannot always be a file
 */

package fr.fluxmedia.transmorpher.graph;
import fr.fluxmedia.transmorpher.engine.TProcess;
import fr.fluxmedia.transmorpher.engine.TProcessComposite;

import fr.fluxmedia.transmorpher.utils.Writer;

import java.io.IOException;

/**
 * This class allows to instanciate a call for an object able to produce SAX events.
 *
 * There is two ways to create a Generate object : 
 * <ul>
 * <li>Give an XML representation of the serialize to instanciate :
 *	<code><i><br/><br/>
 *  &lt;generate type="readfile" id="bibexmo" out="R1"&gt;<br/>
 *     &lt;with-param name="file"&gt;../samples/biblio/input/bibexmo.xml&lt;/with-param&gt;<br/>
 *  &lt;/generate&gt;<br/></i>
 *	</code><br/>
 *  For a Generate object, one out port is required. It can accept many parameters ( with-param element)
 *  Type Attribute describe the type of the TReader to create : a built-in TReader (stdlib) or 
 *  a specific TReader (tmcontrib).<br>
 * </li>
 * <li>Use Flowcomposer ( a GUI tool for transmorpher )</li>
 * </ul>
 * 
 *
 * CAUTION, This will have to change the parameter cannot always be a file
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */
public class Generate extends CallImpl {

	/**
	 * Creates a simple instance of Generate
	 */
	public Generate() {
		this(null, null);
	}

	/**
	 * Creates an instance of Generate with an id and a type
	 *
	 *@param  id    The id
	 *@param  type  The type
	 */
	public Generate(String id, String type) {
		this(id, type, (String)null);
	}

	/**
	 * Creates an instance of Generate with an id , a type and a file name (compatibility with old versions)
	 *
	 *@param  id    The id
	 *@param  type  The type
	 *@param  file  the file name
	 */
	public Generate(String id, String type, String file) {
		this(id, type, null, 0, file);
	}

	/**
	 * Creates an instance of Generate with an id , a type and a process that owns this serialize
	 *
	 *@param  id       The id
	 *@param  type     The type
	 *@param  process  the process
	 */
	public Generate(String id, String type, Process process) {
		this(id, type, process, 0);
	}

	/**
	 * Creates an instance of Generate with an id , a type , a process that owns this serialize and a file name
	 *
	 *@param  id       The id
	 *@param  type     The type
	 *@param  file     The file name
	 *@param  process  the process
	 */
	public Generate(String id, String type, String file, Process process) {
		this(id, type, process, 0, file);
	}

	/**
	 * Creates an instance of Generate with an id , a type , a process that owns this serialize
	 * and the number of out ports.
	 *
	 *@param  id       The id
	 *@param  type     The type
	 *@param  process  The process
	 *@param  out      The number of out
	 */
	public Generate(String id, String type, Process process, int out) {
		this(id, type, process, out, null);
	}


	/**
	 * Creates an instance of Generate with an id , a type , a process that owns this serialize,
	 * the number of out ports and a file name
	 *
	 *@param  id       The id
	 *@param  type     The type
	 *@param  process  The process
	 *@param  out      The number of out
	 *@param  file     The file name
	 */
	public Generate(String id, String type, Process process, int out, String file) {
		super(id, type, process, 0, out);
		/**
		 * This is for legacy code
		 */
		if (file != null) {
			parameters.setParameter("file", file);
		}
	}

	/**
	 * A generate can be nullified but it will propagate to nothing
	 *
	 *@param  out  Description of the Parameter
	 *@param  in   Description of the Parameter
	 */
	public void retroNull(PortList out, PortList in) {
		nullified = out.getPort(0).getChannel().nullifiedP();
		visited = true;
		update(null);
	}

	/**
	 *  Sets the file attribute of the Generate object
	 *
	 *@param  uri  The new file value
	 */
	public void setFile(String uri) {
		if (uri != null) {
			parameters.setParameter("file", uri);
		}
	}

	/**
	 *  Gets the file attribute of the Generate object
	 *
	 *@return    The file value
	 */
	public String getFile() {

		return (String)parameters.getParameter("file");
	}


	/**
	 *  Description of the Method
	 */
	public void generateXML() {
		System.out.print("      <generate id=\"" + id + "\" type=\"" + getType() + "\"");
		System.out.print(" out=\"");
		outPorts.generateXML();
		if (parameters.isEmpty()) {
			System.out.println("\"/>");
		} else {
			System.out.println("\">");
			parameters.generateXMLCalls();
			System.out.println("      </generate>");
		}
	}//end generate xml

	/**
	 *  Creates a TReader corresponding to this generate object and adds it to the composite
	 * process in the execution structure.
	 *
	 *@param  currentProcess  the container for this serializer
	 */
	public final void createProcess(TProcessComposite currentProcess) {

		String[] vPortOut = outPorts().toStringList();
		String className = this.getProcess().getTransmorpher().getClassForType(getType());
		Object[] params = {(Object)vPortOut, (Object)getParameters(), (Object)getAttributes()};
		TProcess vCurrent = this.newProcess(className, params);

		currentProcess.addProcess(vCurrent);

		update(vCurrent);
	}//end proc

	/**
	 *  Description of the Method
	 *
	 *@param  file             Description of the Parameter
	 *@exception  IOException  Description of the Exception
	 */
	public void generateJavaCode(Writer file) throws IOException {
		file.writeln();
		file.writeln(4, "//------------------ Generation of Generator " + getId() + " -------------------");
		generatePorts(file);
		generateParameters(file);
		String className = this.getProcess().getTransmorpher().getClassForType(getType());
		file.writeln(4, "tmCurrentProcess = new " + className + "(portOut,tmParameters,tmAttributes);");
		file.writeln(4, "tmCurrentProcess.setName(\"" + getId() + "\");");
		file.writeln("");
		file.writeln(4, "tmCurrentCompositeProcess.addProcess(tmCurrentProcess);");
	}//end proc
}

