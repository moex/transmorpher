/**
 * $Id: Property.java,v 1.1 2002-11-28 13:47:53 jerome Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
  * Transmorpher graph call node interface
  *
  * @author Jerome.Euzenat@inrialpes.fr
  * @since jdk 1.3 / SAX 2.0
  */

package fr.fluxmedia.transmorpher.graph.prop ;

import fr.fluxmedia.transmorpher.utils.Writer;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
import fr.fluxmedia.transmorpher.engine.TProcessComposite;

import java.io.Serializable;
import java.io.IOException;

public interface Property extends Serializable {

    /** Prints the XML expression of the call */
    public void generateXML();

    public static final int COMPOSITION = 0;
    public static final int GENERATE = 1;
    public static final int SERIALIZE = 2;
    public static final int MERGE = 3;
    public static final int DISPATCH = 4;
    public static final int REPEAT = 5;
    public static final int ADDATT = 101;
    public static final int ADDTAG = 102;
    public static final int REMATT = 103;
    public static final int REMTAG = 104;

    public boolean satisfied(int Operation);
    
}
