/*
 * $Id: Channel.java,v 1.2 2003-01-29 15:59:16 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.transmorpher.graph;

import fr.fluxmedia.transmorpher.utils.Writer;

import java.io.Serializable;
import java.util.Observable;

/**
 * Transmorpher graph channel representation
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */
public class Channel extends Observable implements Serializable {

	/**
	 *  in port of this channel
	 */
	protected Port in = null;
	/**
	 *  out port of this channel
	 */
	protected Port out = null;
	/**
	 *  name of the channel
	 */
	protected String name = null;
	/**
	 *  Description of the Field
	 */
	protected boolean visited = false;
	/**
	 *  Description of the Field
	 */
	protected boolean nullified = false;
	/**
	 *  the process that contains this channel
	 */
	protected Process process = null;

	/**
	 *Creates a simple instance of Channel
	 */
	public Channel() { }

	/**
	 *Creates an instance of Channel with a name and a process
	 *
	 *@param  name     The name of the channel to create
	 *@param  process  The process that contains the channel
	 */
	public Channel(String name, Process process) {
		this(name, process, null, null);
	}

	/**
	 *Creates an instance of Channel with a name and a process
	 *
	 *@param  name     The name of the channel to create
	 *@param  process  The process that contains the channel
	 *@param  in       The in port of the channel
	 *@param  out      The out port of the channel
	 */
	public Channel(String name, Process process, Port in, Port out) {
		this.name = name;
		this.process = process;
		setIn(in);
		setOut(out);
	}

	/**
	 *  Returns a description of this channel in a string
	 *  
	 *@return    a string : [Channel "name" IN : "in port name" OUT="out port name"]
	 */
	public String toString() {
		//String description = "[Channel " + name + " IN : " + in.getName() + " OUT : " + out.getName() + "]";
		return name;
	}

	/**
	 *  Returns the in port of this channel
	 *
	 *@return    The in port
	 */
	public Port in() {
		return in;
	}

	/**
	 *  Returns the out port of this channel
	 *
	 *@return    The out port
	 */
	public Port out() {
		return out;
	}

	/**
	 *  Sets the in port of the Channel object
	 *
	 *@param  port  The new in port
	 */
	public void setIn(Port port) {
		if (in != null) {
			in.setChannel(null);
		}

		in = port;

		if (in != null) {
			in.setChannel(this);
		}
	}

	/**
	 *  Sets the out port of the Channel object
	 *
	 *@param  p  The new out port
	 */
	public void setOut(Port port) {
		if (out != null) {
			out.setChannel(null);
		}

		out = port;

		if (out != null) {
			out.setChannel(this);
		}
	}

	/**
	 *  Sets the name attribute of the Channel object
	 *
	 *@param  newName  The new name value
	 */
	public void setName(String newName) {

		//A channel's change name must also change associated port name
		if (process != null) {
			process.getAllChannels().remove(this.name);
			process.getAllChannels().put(newName, this);
		}
		if (in != null) {
			in.setName(newName);
		}
		if (out != null) {
			out.setName(newName);
		}

		name = newName;
	}

	/**
	 *  Gets the name attribute of the Channel object
	 *
	 *@return    The name value
	 */
	public String getName() {
		return name;
	}

	/**
	 *  Gets the process that owns this Channel object
	 *
	 *@return    The process
	 */
	public Process getProcess() {
		return process;
	}

	/**
	 *  Description of the Method
	 */
	public void clearNull() {
		nullified = false;
	}

	/**
	 *  Description of the Method
	 */
	public void nullify() {
		nullified = true;
	}

	/**
	 *  Description of the Method
	 */
	public void clearVisited() {
		visited = false;
	}

	/**
	 *  Sets the visited attribute of the Channel object
	 */
	public void setVisited() {
		visited = true;
	}

	/**
	 *  Description of the Method
	 *
	 *@return    Description of the Return Value
	 */
	public boolean visitedP() {
		return visited;
	}

	/**
	 *  Description of the Method
	 *
	 *@return    Description of the Return Value
	 */
	public boolean nullifiedP() {
		return nullified;
	}

}

