/*
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2003, 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.transmorpher.graph;

import fr.fluxmedia.transmorpher.action.*;
import fr.fluxmedia.transmorpher.engine.*;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
import fr.fluxmedia.transmorpher.utils.Writer;
import java.io.IOException;

import java.util.Enumeration;
import java.util.Observable;

/**
 * This class is an helper class which implements most of methods specified
 * in the Call interface.
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */
public abstract class CallImpl extends Observable implements Call {

	/**
	 *  The id of the call
	 */
	protected String id = null;
	/**
	 *  The storage object for the parameters of this call.
	 * 	Parameters are parameterizable.
	 */
	protected Parameters parameters = null;
	/**
	 *  The storage object for the attributes of this call.
	 *	Attributes are not parameterizable.
	 */
	protected StringParameters attributes = null;
	/**
	 *  The storage structure for the in ports of this call
	 */
	protected PortList inPorts = null;
	/**
	 *  The storage structure for the out ports of this call
	 */
	protected PortList outPorts = null;
	/**
	 *  Description of the Field
	 */
	protected boolean visited = false;
	/**
	 *  Description of the Field
	 */
	protected boolean nullified = false;
	/**
	 *  The process owner of this call
	 */
	protected Process process = null;

	public CallImpl(){
	}
	/**
	 * Constructor for the CallImpl object
	 *
	 *@param  id           The name of this call
	 *@param  type         The type of this call
	 *@param  process      The process owner of this call
	 *@param  numberOfIn   The number of in ports
	 *@param  numberOfOut  The number of out port
	 */
	public CallImpl(String id, String type, Process process, int numberOfIn, int numberOfOut) {
		this.id = id;
		this.process = process;
		inPorts = new PortList(numberOfIn);
		outPorts = new PortList(numberOfOut);
		parameters = new Parameters();
		attributes = new StringParameters();
		if (type != null) {
			attributes.setParameter("type", type);
		}
		if (id != null) {
			attributes.setParameter("id", id);
		}
	}

	/**
	 * Prints the XML expression of the call
	 */
	public abstract void generateXML();

	/**
	 * Prints the Java code for the execution of the call
	 *
	 *@param  file             The writer used for printing the java code
	 *@exception  IOException  If an I/O error occurs
	 */
	public abstract void generateJavaCode(Writer file) throws IOException;

	/**
	 *  Prints a description of this object in a string form
	 *
	 *@return    The string which contains the description
	 */
	public String toString() {
		return id;
	}

	/**
	 *  Gets the id attribute of the CallImpl object
	 *
	 *@return    The id value
	 */
	public String getId() {
		return id;
	}

	/**
	 *A modification of the id involved a modification in the process Struct Storage.
	 *
	 *@param  id   The new id value
	 */
	public void setId(String id) {

		if (process != null) {

			if ((!this.id.equals(id))) {

				//remove in the storage struct the call with the old id key
				process.getCalls().remove(id);

				//add in the storage struct the call with the new id key
				process.getCalls().add(id, this);

			}
			this.id = id;
		} else {
			//to be thought
			this.id = id;
		}
		update(null);
	}

	/**
	 *  Gets the type attribute of the CallImpl object
	 *
	 *@return    The type value
	 */
	public String getType() {
		return attributes.getStringParameter("type");
	}

	/**
	 *  Sets the type attribute of the CallImpl object
	 *
	 *@param  t  The new type value
	 */
	public void setType(String t) {
		attributes.setParameter("type", t);
		update(null);
	}

	/**
	 *  Sets the parameters attribute of the CallImpl object
	 *
	 *@param  param  The new parameters value
	 */
	public void setParameters(Parameters param) {
		parameters = param;
		update(null);
	}

	/**
	 *  Gets the parameters attribute of the CallImpl object
	 *
	 *@return    The parameters value
	 */
	public Parameters getParameters() {
		return parameters;
	}

	/**
	 *  Gets the process attribute of the CallImpl object
	 *
	 *@return    The process value
	 */
	public StringParameters getAttributes() {
		return attributes;
	}

	/**
	 *  Gets the process attribute of the CallImpl object
	 *
	 *@return    The process value
	 */
	public Process getProcess() {
		return process;
	}

	/**
	 *  Adds a feature to the Out attribute of the CallImpl object
	 *
	 *@param  port  The feature to be added to the Out attribute
	 */
	public void addOut(Port port) {
		addOut(outPorts.length(), port);
	}

	/**
	 *  Adds a feature to the In attribute of the CallImpl object
	 *
	 *@param  port  The feature to be added to the In attribute
	 */
	public void addIn(Port port) {
		addIn(inPorts.length(), port);
	}

	/**
	 *  Adds a feature to the In attribute of the CallImpl object
	 *
	 *@param  index  The index in the storage object at which port has to be added
	 *@param  port   The feature to be added to the In attribute
	 */
	public void addIn(int index, Port port) {
		inPorts.setPort(index, port);
		update(new TmActionImpl(TmActionImpl.ADD, this, port, null, null));
	}

	/**
	 *  Adds a feature to the Out attribute of the CallImpl object
	 *
	 *@param  index  The index in the storage object at which port has to be added
	 *@param  port   The feature to be added to the Out attribute
	 */
	public void addOut(int index, Port port) {
		outPorts.setPort(index, port);
		update(new TmActionImpl(TmActionImpl.ADD, this, port, null, null));
	}

	/**
	 *  Returns the storage object of the in ports
	 *
	 *@return    the PortList object
	 */
	public PortList inPorts() {
		return inPorts;
	}

	/**
	 *  Returns the storage object of the out ports
	 *
	 *@return    the PortList object
	 */
	public PortList outPorts() {
		return outPorts;
	}

	/**
	 * clear the "visited" mark
	 */
	public void clearVisited() {
		visited = false;
		update(null);
	}

	/**
	 * clear the "null" mark
	 */
	public void clearNull() {
		nullified = false;
		update(null);
	}

	/**
	 * propagate the null values of the input to the output.
	 * by default, mark the channel as visited
	 */
	public void retroNull() {
		visited = true;
		update(null);
	}

	/**
	 */
	public void setUp() {
		// By default do nothing

	}

	/**
	 *  This method is used for printing a java description of this Call ports
	 *
	 *@param  file             The writer used for printing in a file
	 *@exception  IOException  If an I/O error occurs
	 */
	public void generatePorts(Writer file) throws IOException {
		file.writeln(4, "portIn = new String[" + this.inPorts().getNames().length + "];");
		for (int i = 0; i < inPorts.length(); i++) {
			file.writeln(4, "portIn[" + i + "]=\"" + this.inPorts().getNames()[i] + "\";");
		}
		file.writeln(4, "portOut = new String[" + this.outPorts().getNames().length + "];");
		for (int i = 0; i < outPorts.length(); i++) {
			file.writeln(4, "portOut[" + i + "]=\"" + this.outPorts().getNames()[i] + "\";");
		}
	}

	/**
	 *  This method is used for printing a java description of this Call parameters and attributes
	 *
	 *@param  file             Description of the Parameter
	 *@exception  IOException  Description of the Exception
	 */
	public void generateParameters(Writer file) throws IOException {
		file.writeln();
		file.writeln(4, "tmAttributes = new StringParameters(); ");
		for (Enumeration e = getAttributes().getNames(); e.hasMoreElements(); ) {
			String key = (String)e.nextElement();
			String val = (String)getAttributes().getParameter(key);
			file.writeln(4, "tmAttributes.setParameter(\"" + key + "\",\"" + val + "\" );");
		}
		file.writeln(4, "tmParameters = new Parameters(); ");
		for (Enumeration e = getParameters().getNames(); e.hasMoreElements(); ) {
			String key = (String)e.nextElement();
			String val = (String)getParameters().getParameter(key);
			if (((getProcess().getTransmorpher().getMain()) instanceof Servlet) && (key.equals("file"))) {
				if (val.startsWith("http")) {
					file.writeln(4, "tmParameters.setParameter(\"" + key + "\",\"" + val + "\" );");
				} else {
					file.writeln(4, "tmParameters.setParameter(\"" + key + "\",root+\"" + val + "\" );");
				}
			} else {
				file.writeln(4, "tmParameters.setParameter(\"" + key + "\",\"" + val + "\" );");
			}
		}
		file.writeln();
	}//end proc

	/**
	 *  Creates the execution object ( from engine package ) corresponding to this
	 * call
	 *
	 *@param  composite  the container for the execution object
	 */
	public void createProcess(TProcessComposite composite) { }

	/**
	 * A specific method for flowcomposer
	 *
	 *@param  object  Description of the Parameter
	 */
	public void update(Object object) {
		setChanged();
		notifyObservers(object);
	}

	/**
	 *  This method is used for creating the execution object corresponding to this call.
	 *
	 *@param  type    The type of the execution object to create
	 *@param  params  the parameters to pass to the selected constructor
	 *@return         a Tprocess
	 */
	public final static TProcess newProcess(String type, Object[] params) {
		TProcess process = null;
		//System.err.println("Creating "+type+" process with "+type+" class");
		try {
			Class processClass = Class.forName(type);
			java.lang.reflect.Constructor[] processConstructors = processClass.getConstructors();
			//System.err.println("Result : "+processConstructors[0].getName());
			process = (TProcess)processConstructors[0].newInstance(params);
		} catch (Exception e) {
			System.out.println("[CallImpl] Error : cannot create Process");
			System.out.println("[CallImpl] " + e);
		}
		return process;
	}
}

