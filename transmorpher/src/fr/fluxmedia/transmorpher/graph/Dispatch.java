/*
 * $Id: Dispatch.java,v 1.5 2003-01-28 15:40:53 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.graph;
import fr.fluxmedia.transmorpher.engine.TProcess;
import fr.fluxmedia.transmorpher.engine.TProcessComposite;

import fr.fluxmedia.transmorpher.utils.Writer;

import java.io.IOException;


/**
 * This class allows to instanciate a call for an object able to produce multiple SAX events flows.
 *
 * There is two ways to create a Dispatch object : 
 * <ul>
 * <li>Give an XML representation of the dispatch to instanciate :
 *	<code><i><br/><br/>
 *  &lt;dispatch type="broadcast" id="disp" in="i1" out="o1 o2 o3"/&gt;<br/>
 *	</i></code><br/>
 *  For a Dispatch object, one in port is required and at least two out ports.
 *  Type Attribute describe the type of the TDispatcher to create : a built-in TDispatcher (stdlib) or 
 *  a specific TDispatcher (tmcontrib).<br>
 * </li>
 * <li>Use Flowcomposer ( a GUI tool for transmorpher )</li>
 * </ul>
 * 
 *	
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */

public class Dispatch extends CallImpl {

	/**
	 *Creates a simple instance of Dispatch
	 */
	public Dispatch() {
		this(null, null);
	}

	/**
	 *Creates an instance of Dispatch with an id and a type
	 *
	 *@param  id  The id of the Dispatch instance to create
	 *@param  type  The type of the Dispatch instance to create
	 */
	public Dispatch(String id, String type) {
		this(id, type, null);
	}

	/**
	 *Creates an instance of Dispatch with an id , a type and a process
	 *
	 *@param  id  			The id of the Dispatch instance to create
	 *@param  type  		The type of the Dispatch instance to create
	 *@param  process  	The process that owns this Dispatch instance
	 */
	public Dispatch(String id, String type, Process process) {
		this(id, type, process, 0);
	}

	/**
	 *Constructor for the Dispatch object
	 *
	 *@param  id  			The id of the Dispatch instance to create
	 *@param  type  		The type of the Dispatch instance to create
	 *@param  process  	The process that owns this Dispatch instance
	 *@param  out  			The number of out ports
	 */
	public Dispatch(String id, String type, Process process, int out) {
		super(id, type, process, 1 /*Nb In */, out);
	}


	/**
	 * Propagates the nullity of all its output to its sole input
	 *
	 *@param  out  Description of the Parameter
	 *@param  in   Description of the Parameter
	 */
	public void retroNull(PortList out, PortList in) {
		nullified = true;
		for (int i = 1; nullified && i != out.length(); i++) {
			if (!(out.getPort(i).getChannel().nullifiedP())) {
				nullified = false;
			}
		}// end for
		in.getPort(0).getChannel().nullify();
		if (nullified) {
			in.getPort(0).getChannel().nullify();
		}
		visited = true;

		//notify observer
		update(null);
	}

	/**
	 *  Prints an XML description of this Dispatch instance
	 */
	public void generateXML() {
		System.out.print("      <dispatch id=\"" + id + "\" type=\"" + getType() + "\" ");
		System.out.print("in=\"");
		inPorts.generateXML();
		System.out.print("\" out=\"");
		outPorts.generateXML();
		System.out.println("\"/>");
	}

	/**
	 *  Creates a TDispatcher corresponding to this Dispatch object and adds it to the composite
	 * process in the execution structure.
	 *
	 *@param  currentProcess  the container for this serializer
	 */
	public final void createProcess(TProcessComposite currentProcess) {
		String[] vPortIn = inPorts().toStringList();
		String[] vPortOut = outPorts().toStringList();
		String className = this.getProcess().getTransmorpher().getClassForType(getType());
		Object[] params = {(Object)vPortIn, (Object)vPortOut, (Object)getParameters()};
		TProcess vCurrent = this.newProcess(className, params);
		currentProcess.addProcess(vCurrent);
		update(vCurrent);
	}//end proc

	/**
	 *  Prints a java code description of this Dispatch instance
	 *
	 *@param  file             a writer used for printing in a file
	 *@exception  IOException  when an IO errors occur
	 */
	public void generateJavaCode(Writer file) throws IOException {
		file.writeln();
		file.writeln(4, "//------------------ Generation of Dispatcher " + getId() + " -------------------");
		generatePorts(file);
		generateParameters(file);
		String className = this.getProcess().getTransmorpher().getClassForType(getType());

		file.writeln(4, "tmCurrentProcess = new " + className + "(portIn,portOut,tmParameters);");
		file.writeln(4, "tmCurrentProcess.setName(\"" + getId() + "\");");
		file.writeln();
		file.writeln(4, "tmCurrentCompositeProcess.addProcess(tmCurrentProcess);");
	}//end proc
}

