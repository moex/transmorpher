/*
 * $Id: Serialize.java,v 1.5 2003-01-28 15:40:53 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.transmorpher.graph;

import fr.fluxmedia.transmorpher.engine.TProcess;
import fr.fluxmedia.transmorpher.engine.TProcessComposite;
import fr.fluxmedia.transmorpher.utils.Writer;

import java.io.IOException;

/**
 * This class allows to instanciate a call that is used for ending a transformation.
 * There is two ways to create a Serialize object : 
 * <ul>
 * <li>Give an XML representation of the Serialize to instanciate :
 *	<code><i><br/><br/>
 *  &lt;serialize type="writefile" id="write" in="R1"&gt;<br/>
 *    &lt;with-param name="file"&gt;../samples/biblio/output/bibexmo.xml&lt;/with-param&gt;<br/>
 *    &lt;with-param name="method"&gt;xml&gt;<br/>
 *  &lt;/generate&gt;<br/></i>
 *	</code><br/>
 *  For a Serialize object, one in port is required. It can accept many parameters ( with-param element)
 *  Type attribute describe the type of the TSerializer to create : a built-in TSerializer (stdlib) or 
 *  a specific TSerializer (tmcontrib).<br>
 * </li>
 * <li>Use Flowcomposer ( a GUI tool for transmorpher )</li>
 * </ul>
 * CAUTION, This will have to change the parameter cannot always be a file
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */

public class Serialize extends CallImpl {

	/**
	 * Creates a simple instance of serializer
	 */
	public Serialize() {
		this(null, null);
	}

	/**
	 * Creates an instance of serializer with an id and a type
	 *
	 *@param  id    The id of this serializer
	 *@param  type  The type of this serializer
	 */
	public Serialize(String id, String type) {
		this(id, type, (String)null);
	}

	/**
	 * Creates an instance of serializer with an id, a type and a file name (for compatibility with old versions of Transmorpher)
	 *
	 *@param  id        The id of this serializer
	 *@param  type      The type of this serializer
	 *@param  fileName  a file name
	 */
	public Serialize(String id, String type, String fileName) {
		this(id, type, null, 0, fileName);
	}

	/**
	 * Creates an instance of serializer with an id, a type and a process that owns this call
	 *
	 *@param  id       The id of this serializer
	 *@param  type     The type of this serializer
	 *@param  process  a Process
	 */
	public Serialize(String id, String type, Process process) {
		this(id, type, process, 0);
	}

	/**
	 * Creates an instance of serializer with an id, a type , a process that owns this call and a file name (for compatibility with old versions of Transmorpher)
	 *
	 *@param  id        The id of this serializer
	 *@param  type      The type of this serializer
	 *@param  fileName  a file name
	 *@param  process   a process
	 */
	public Serialize(String id, String type, String fileName, Process process) {
		this(id, type, process, 0, fileName);
	}

	/**
	 * Creates an instance of serializer with an id, a type , a process that owns this call and a number of in
	 *
	 *@param  in       The number of in port of this serializer
	 *@param  id       The id of this serializer
	 *@param  type     The type of this serializer
	 *@param  process  a Process
	 */
	public Serialize(String id, String type, Process process, int in) {
		this(id, type, process, in, null);
	}

	/**
	 * Creates an instance of serializer with an id, a type , a process that owns this call, a number of in ports, and a file name (for compatibility with old versions of Transmorpher)
	 *
	 *@param  id     Description of the Parameter
	 *@param  type     Description of the Parameter
	 *@param  process     Description of the Parameter
	 *@param  in    Description of the Parameter
	 *@param  file  Description of the Parameter
	 */
	public Serialize(String id, String type, Process process, int in, String file) {
		super(id, type, process, in, 0);
		/**
		 * This is for legacy code
		 */
		if (file != null) {
			attributes.setParameter("file", file);
		}
	}

	/**
	 * A serialize cannot be nullified
	 *
	 *@param  out  Description of the Parameter
	 *@param  in   Description of the Parameter
	 */
	public void retroNull(PortList out, PortList in) {
		visited = true;
		in.getPort(0).getChannel().setVisited();

		update(null);
	}

	/**
	 *  Sets the file attribute of the Serialize object
	 *
	 *@param  uri  The new file value
	 */
	public void setFile(String uri) {
		if (uri != null) {
			parameters.setParameter("file", uri);
		}
	}


	/**
	 *  Prints an XML representation of a serializer
	 */
	public void generateXML() {
		System.out.print("      <serialize id=\"" + id + "\" type=\"" + getType() + "\"");
		System.out.print(" in=\"");
		inPorts.generateXML();
		if (parameters.isEmpty()) {
			System.out.println("\"/>");
		} else {
			System.out.println("\">");
			parameters.generateXMLCalls();
			System.out.println("      </serialize>");
		}
	}

	/**
	 *  Creates a TSerializer corresponding to this serializer object and adds it to the composite
	 * process in the execution structure.
	 *
	 *@param  currentProcess  the container for this serializer
	 */
	public final void createProcess(TProcessComposite currentProcess) {
		String[] vPortIn = inPorts().toStringList();
		String className = this.getProcess().getTransmorpher().getClassForType(getType());
		Object[] params = {(Object)vPortIn, (Object)getParameters(), (Object)getAttributes()};
		TProcess vCurrent = this.newProcess(className, params);
		currentProcess.addProcess(vCurrent);
		
		// for flowcomposer
		update(vCurrent);

	}//end proc

	/**
	 *  Prints a java code description of this serializer.
	 *
	 *@param  file             a write for printing the java cade in a file
	 *@exception  IOException  when an IO errors occur.
	 */
	public void generateJavaCode(Writer file) throws IOException {
		file.writeln();
		file.writeln(4, "//------------------ Generation of Serializer " + getId() + " -------------------");
		generatePorts(file);
		generateParameters(file);
		String className = this.getProcess().getTransmorpher().getClassForType(getType());

		file.writeln(4, "tmCurrentProcess = new " + className + "(portIn,tmParameters,tmAttributes);");
		file.writeln(4, "tmCurrentProcess.setName(\"" + getId() + "\");");
		file.writeln();
		file.writeln(4, "tmCurrentCompositeProcess.addProcess(tmCurrentProcess);");
	}//end proc

}

