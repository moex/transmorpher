/*
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2003, 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.transmorpher.graph;
import fr.fluxmedia.transmorpher.graph.rules.Namespace;
import fr.fluxmedia.transmorpher.graph.rules.Select;

import fr.fluxmedia.transmorpher.utils.LinearIndexedStruct;
import fr.fluxmedia.transmorpher.utils.Version;
import fr.fluxmedia.transmorpher.utils.Writer;
import java.io.File;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;

/**
 * Transmorpher graph query definitions
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */

public class Query extends TransformationImpl implements Namespaced {

	/**
	 *  List of selection
	 */
	protected ArrayList<Select> selects = null;
	/**
	 *  List of namespaces
	 */
	protected ArrayList<Namespace> namespaces = null;
	/**
	 *  the root value
	 */
	protected String root = null;

	/**
	 *Constructor for the Query object
	 *
	 *@param  name  The name of the Query instance
	 *@param  owner  The Transmorpher instance that owns this Query instance
	 *@param  root  Description of the Parameter
	 */
	public Query(String name, Transmorpher owner, String root) {
		super(name, "Query", owner, 1, 1);
		selects = new ArrayList<Select>(10);
		namespaces = new ArrayList<Namespace>(10);
		this.root = root;
	}

	/**
	 *  Adds a namespace to this Query instance
	 *
	 *@param  name  The namespace to add
	 */
	public void addNamespace(Namespace name) {
		namespaces.add( name );
	}

	/**
	 *  Adds a selection to this Query instance
	 *
	 *@param  selection  The selection to Add
	 */
	public void addSelection(Select selection) {
		selects.add( selection );
	}

	/**
	 *  Adds a selection at the specified index to this Query instance
	 *
	 *@param  selection  The selection to Add
	 *@param  index  index in the list
	 */
	public void insertSelection(Select selection, int index) {
		selects.add(index, selection);
	}

	/**
	 * Prints the XML expression of the process
	 */
	public void generateXML() {
		System.out.println("  <query name=\"" + name + "\" type=\"" + type + "\" root=\"" + root + "\">");
		for ( Namespace ns: namespaces ) ns.generateXML();
		parameters.generateXMLDeclarations();
		for ( Select sl: selects ) sl.generateXML();
		System.out.println("  </query>");
		System.out.println();
	}

	/**
	 * Prints the XML expression of the process
	 *
	 *@param  file  Description of the Parameter
	 */
	public void generateXMLinFile(Writer file) {
		System.out.println("[Query]generateXMLinFile : not implemented yet");
	}

	/**
	 * Prints the Java code for the execution of this Query instance
	 *
	 *@param  file            The writer used for printing in a file
	 *@param  externs  Description of the Parameter
	 */
	public void generateJavaCode(Writer file, LinearIndexedStruct<String> externs) {
		System.out.println("[Query]generateJavaCode : not implemented yet");
	}

	/**
	 * generate the stylesheet for the Query (in the correct file)
	 *
	 *@param  		reloc         The path where the stylesheet have to be put
	 *@exception  IOException  	When IO errors occur
	 */
	public void generateXSLTStylesheet(String reloc) throws IOException {
		String vPath = getTransmorpher().getFile().getAbsoluteFile().getParent();
		//System.out.println("[Query] +file="+vPath+"/"+name);
		if (vPath == null) {
			vPath = ".";
		} else {
			vPath = vPath.replace('\\', '/');
		}
		if (reloc != null) {
			vPath = reloc;
		}
		attributes.setParameter("file", vPath + "/" + name + ".xsl");
		generateXSLTCode(new Writer(vPath + "/" + name + ".xsl"));
	}

	/**
	* Prints the xsl for the execution of this Query instance
	 *
	 *@param  file            The writer used for printing in a file
	 */
	public void generateXSLTCode(Writer file) throws IOException {
		file.writeln("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		file.writeln("");
		file.writeln("<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" version=\"1.0\"");
		for ( Namespace ns: namespaces ) ns.generateXSLTCode(file);
		file.writeln(">");
		file.writeln("");
		// JE: why is this not in first?
		file.writeln("<!-- ************************************************************");
		file.writeln("     Generated by " + Version.NAME + " " + Version.RELEASE);
		Date date = new Date();
		file.writeln("     " + date);
		file.writeln("     See " + Version.URL);
		file.writeln("     ************************************************************ -->");
		for (Enumeration<String> e = parameters.getNames(); e.hasMoreElements(); ) {
			String key = e.nextElement();
			file.writeln(2, "<xsl:param name=\"" + key + "\">" + parameters.getParameter(key) + "</xsl:param>");
		}
		file.writeln("");
		file.writeln(2, "<xsl:template match=\"/\">");
		file.writeln(4, "<" + root + ">");
		file.writeln(6, "<xsl:apply-templates select=\"*\"/>");
		file.writeln(4, "</" + root + ">");
		file.writeln(2, "</xsl:template>");
		file.writeln("");
		file.writeln(2, "<xsl:template match=\"text()\"/>");
		file.writeln("");
		for ( Select sl: selects ) sl.generateXSLTCode(file);
		file.writeln("</xsl:stylesheet>");
		file.close();
	}//end proc

}

