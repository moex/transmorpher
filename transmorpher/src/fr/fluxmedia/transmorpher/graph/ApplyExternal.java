/*
 *  $Id: ApplyExternal.java,v 1.4 2003-01-28 15:40:53 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.transmorpher.graph;

import fr.fluxmedia.transmorpher.engine.TProcess;
import fr.fluxmedia.transmorpher.engine.TProcessComposite;
import fr.fluxmedia.transmorpher.utils.Writer;

import java.io.IOException;

/**
 * This class allows to instanciate a call for an object able to apply a transformation on the SAX events flow.
 *
 * <br/><code><i><br/>
 * &lt;apply-external id="FormatHTML" type="xslt" in="X2" out="Z34"&gt;<br/>
 *       &lt;with-param name="file"&gt;../samples/biblio/xslt/form-hauth.xsl&lt;/with-param&gt;<br/>
 * &lt;/apply-external&gt;<br/>
 * </i></code></br>
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */
public class ApplyExternal extends ApplyImpl {

	/**
	 * Creates a simple instance of ApplyExternal 
	 */
	public ApplyExternal() {
		this((String)null, (String)null);
	}

	/**
	 *  Creates an instance of ApplyExternal with an id and a type 
	 *
	 *@param  id     			The id of this instance
	 *@param  type    		The type of this instance
	 */
	public ApplyExternal(String id, String type) {
		this( id,  type, (String)null);
	}

	/**
	 *  Creates an instance of ApplyExternal with an id,a type and a file name 
	 *
	 *@param  id     			The id of this instance
	 *@param  type    		The type of this instance
	 *@param  file				The file name
	 */
	public ApplyExternal(String id, String type, String file) {
		this( id,  type, file, null);
	}

	/**
	 *  Creates an instance of ApplyExternal with an id,a type and a process
	 *
	 *@param  id     			The id of this instance
	 *@param  type    		The type of this instance
	 *@param  process     The process that owns this instance
	 */
	public ApplyExternal(String id, String type, Process process) {
		this(id, type, null, process);
	}

	/**
	 *  Creates an instance of ApplyExternal with an id,a type, a file name and a process
	 *
	 *@param  id     			The id of this instance
	 *@param  type    		The type of this instance
	 *@param  file				The file name
	 *@param  process     The process that owns this instance
	 */
	public ApplyExternal(String id, String type, String file, Process process) {
		this(id, type, file, process, 0, 0);
	}

	/**
	 *  Creates an instance of ApplyExternal with an id,a type, a file name, a process, a number of in and a number of out
	 *
	 *@param  id     			The id of this instance
	 *@param  type    		The type of this instance
	 *@param  file				The file name
	 *@param  process     The process that owns this instance
	 *@param  in    			The umber of in ports
	 *@param  out   			The number of out ports
	 */
	public ApplyExternal(String id, String type, String file, Process process, int in, int out) {
		super(id, type, process, null, in, out);
		if (file != null) {
			parameters.setParameter("file", file);
		}
	}

	/**
	 *  Sets the file attribute of the ApplyExternal object
	 *
	 *@param  uri  The new file value
	 */
	public void setFile(String uri) {
		if (uri != null) {
			parameters.setParameter("file", uri);
		}
	}

	/**
	 *  Prints an XML description of this instance
	 */
	public void generateXML() {
		System.out.print("      <apply-external id=\"" + id + "\" type=\"" + getType() + "\" ");
		System.out.print("in=\"");
		inPorts.generateXML();
		System.out.print("\" out=\"");
		outPorts.generateXML();
		if (parameters.isEmpty()) {
			System.out.println("\"/>");
		} else {
			System.out.println("\">");
			parameters.generateXMLCalls();
			System.out.println("      </apply-external>");
		}//end if
	}//end generate XML

	/**
	 *  Creates a new TApplyProcess corresponding to this ApplyProcess instance
	 *
	 *@param  currentProcess   The container of this component.
	 */
	public final void createProcess(TProcessComposite currentProcess) {
		String[] vPortIn = inPorts().toStringList();
		String[] vPortOut = outPorts().toStringList();
		String className = this.getProcess().getTransmorpher().getClassForType(getType());
		Object[] params = {(Object)vPortIn, (Object)vPortOut, (Object)getParameters(), (Object)getAttributes()};
		TProcess vCurrent = this.newProcess(className, params);
		currentProcess.addProcess(vCurrent);
		update(vCurrent);
	}//end proc

	/**
	 *  Generates a java code description of this instance
	 *
	 *@param  file             a writer used for printing the code in a file
	 *@exception  IOException  when IO errors occur
	 */
	public void generateJavaCode(Writer file) throws IOException {
		file.writeln();
		file.writeln(4, "//------------------ Generation of ApplyExternal " + getId() + " -------------------");
		generatePorts(file);
		generateParameters(file);
		String className = this.getProcess().getTransmorpher().getClassForType(getType());

		file.writeln(4, "tmCurrentProcess = new " + className + "(portIn,portOut,tmParameters,tmAttributes);");
		//+",tmAttributes);");
		file.writeln(4, "tmCurrentProcess.setName(\"" + getId() + "\");");
		file.writeln();
		file.writeln(4, "tmCurrentCompositeProcess.addProcess(tmCurrentProcess);");
	}//end proc
}

