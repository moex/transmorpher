/*
 * $Id: Call.java,v 1.5 2003-01-30 15:47:46 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.graph;

import fr.fluxmedia.transmorpher.engine.TProcessComposite;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
import fr.fluxmedia.transmorpher.utils.Writer;

import java.io.IOException;
import java.io.Serializable;

/**
 * An object that implements the Call interface is a basic component
 * of a transmorpher graph. It can not contain other compononents .
 * Methods are provided to generate an XML or a Java code representation
 * of the Call.
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */

public interface Call extends Serializable {

	/**
	 * Prints the XML expression of the call
	 */
	public void generateXML();

	/**
	 * Prints the Java code for the execution of the call
	 *
	 *@param  file             the file to write in
	 *@exception  IOException  sends if an IO error occurs.
	 */
	public void generateJavaCode(Writer file) throws IOException;


	/**
	 *  Gets the id attribute of the Call object
	 *
	 *@return    The id value
	 */
	public String getId();

	/**
	 *  Sets the id attribute of the Call object
	 *
	 *@param  name  The new id value
	 */
	public void setId(String name);

	/**
	 *  Gets the type attribute of the Call object
	 *
	 *@return    The type value
	 */
	public String getType();

	/**
	 *  Sets the type attribute of the Call object
	 *
	 *@param  type  The new type value
	 */
	public void setType(String type);

	/**
	 *  Sets the parameters attribute of the Call object
	 *
	 *@param  param  The new parameters value
	 */
	public void setParameters(Parameters param);

	/**
	 *  Gets the attributes attribute of the Call object
	 *
	 *@return    The attributes value
	 */
	public StringParameters getAttributes();

	/**
	 *  Gets the parameters attribute of the Call object
	 *
	 *@return    The parameters value
	 */
	public Parameters getParameters();

	/**
	 *  Gets the process attribute of the Call object
	 *
	 *@return    The process value
	 */
	public Process getProcess();

	/**
	 * Returns the PortList which contains the in ports of this Call
	 *
	 *@return    the in portlist
	 */
	public PortList inPorts();

	/**
	 *  Returns the PortList which contains the out ports of this Call
	 *
	 *@return    the out portlist. 
	 */
	public PortList outPorts();

	/**
	 *  Description of the Method
	 */
	public void setUp();

	/**
	 *  Description of the Method
	 */
	public void clearVisited();

	/**
	 *  Description of the Method
	 */
	public void clearNull();

	/**
	 *  Creates an execution component corresponding to this Call object and adds it to the composite
	 * process in the execution structure.
	 *
	 *@param  composite  The composite process. 
	 */
	public void createProcess(TProcessComposite composite);

}

