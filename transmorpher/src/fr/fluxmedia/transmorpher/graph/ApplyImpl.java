/*
 * $Id: ApplyImpl.java,v 1.2 2003-01-28 15:40:53 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.transmorpher.graph;

/**
 * This class is an helper class for the Apply Call .
 * Methods to manage the reference (name of the transformation to call) are provided
 *
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */
public abstract class ApplyImpl extends CallImpl {

	// JE: LT has discarded the ref as class attribute, but I would prefer it...
	// String ref = null;

	/**
	 * Creates an instance of ApplyImpl with an id, a type, a process, a reference ,a nuber of in and a number of out 
	 *
	 *@param  id    			The id of this instance
	 *@param  type   			The type of this instance
	 *@param  process    	The process that owns this instance
	 *@param  reference   The name of the transformation called by this instance
	 *@param  in   				The number of in ports
	 *@param  out  				The number of out ports
	 */
	public ApplyImpl(String id, String type, Process process, String reference, int in, int out) {
		super(id, type, process, in, out);
		// ref = r;
		if (reference != null) {
			attributes.setParameter("ref", reference);
		}
	}

	/**
	 * Propagates the nullity of all its output to all its input.
	 * This is a basic implementation that does not take into account
	 * the structure of the applied stuff and can be overloaded
	 *
	 *@param  out  Description of the Parameter
	 *@param  in   Description of the Parameter
	 */
	public void retroNull(PortList out, PortList in) {
		nullified = true;
		for (int i = 1; nullified && i != out.length(); i++) {
			if (!(out.getPort(i).getChannel().nullifiedP())) {
				nullified = false;
			}
		}// end for
		for (int i = 1; i != in.length(); i++) {
			in.getPort(i).getChannel().setVisited();
			if (nullified) {
				in.getPort(i).getChannel().nullify();
			}
		}//end for
		visited = true;
	}

	/**
	 *  Gets the name of the transformation called by this ApplyImpl object
	 *
	 *@return    The reference value
	 */
	public String getRef() {
		// return ref;
		return attributes.getStringParameter("ref");
	}

	/**
	 *  Sets the name of the transformation this ApplyImpl object has to call
	 *
	 *@param  reference  The new reference value
	 */
	public void setRef(String reference) {
		// ref = r;
		if (reference != null) {
			attributes.setParameter("ref", reference);
		}
	}

}

