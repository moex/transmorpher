/*
 * $Id: ApplyRuleset.java,v 1.5 2003-01-29 15:59:16 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.graph;

import fr.fluxmedia.transmorpher.engine.*;
import fr.fluxmedia.transmorpher.utils.Writer;

import java.io.IOException;

/**
	*The ApplyRuleset Class introduces the use of a ruleset in a process body. Its structure is the following:
	* <br/><code><i>
	*	&lt;apply-ruleset type="type" ref="name" id="id" in="channel"&gt;<br/>
	* {&lt;with-param&gt;}<br/>
	*	&lt;/apply-ruleset&gt;<br/>
	* </i></code><br/>
 *@since     jdk 1.3 / SAX 2.0
 *@author    Jerome.Euzenat@inrialpes.fr
 */
public class ApplyRuleset extends ApplyImpl {

	/**
	 *Creates a simple instance of ApplyRuleset
	 */
	public ApplyRuleset() {
		this((String)null, (String)null);
	}

	/**
	 *Creates an instance of ApplyRuleset with an id and a type
	 *
	 *@param  id  	The id of the instance to create
	 *@param  type  The type of the instance to create
	 */
	public ApplyRuleset(String id, String type) {
		this(id, type, null, null, null);
	}


	/**
	 *Creates an instance of ApplyRuleset with an id, a type, a process
	 *
	 *@param  id  		The id of the instance to create
	 *@param  type  	The type of the instance to create
	 *@param  process	The process that owns this instance
	 */
	public ApplyRuleset(String id, String type, Process process) {
		this(id, type, process, null, null);
	}

	/**
	 *Creates an instance of ApplyRuleset with an id , a type, a process and a reference
	 *
	 *@param  id  			The id of the instance to create
	 *@param  type  		The type of the instance to create
	 *@param  process		The process that owns this instance
	 *@param  reference The name of the Ruleset called by this instance
	 */
	public ApplyRuleset(String id, String type, Process process, String reference) {
		this(id, type, process, reference, "one-pass/top-down/apply-once");
	}

	/**
	 *Creates an instance of ApplyRuleset with an id , a type, a process, a reference and a strategy
	 *
	 *@param  id  			The id of the instance to create
	 *@param  type  		The type of the instance to create
	 *@param  process		The process that owns this instance
	 *@param  reference The name of the Ruleset called by this instance
	 *@param  strategy  Description of the Parameter
	 */
	public ApplyRuleset(String id, String type, Process process, String reference, String strategy) {
		super(id, type, process, reference, 1, 1);
		// JE I still do not like to have these attributes in parameters
		if (strategy != null) {
			attributes.setParameter("strategy", strategy);
		}
	}

	/**
	 * Propagate the nullify of its sole output to all the input.
	 *
	 *@param  out  Description of the Parameter
	 *@param  in   Description of the Parameter
	 */
	public void retroNull(PortList out, PortList in) {
		nullified = out.getPort(0).getChannel().nullifiedP();
		in.getPort(0).getChannel().setVisited();
		if (nullified) {
			in.getPort(0).getChannel().nullify();
		}
		visited = true;

		update(null);
	}

	/**
	 *  Sets the strategy attribute of the ApplyRuleset object
	 *
	 *@param  s  The new strategy value
	 */
	public final void setStrategy(String strategy) {
		attributes.setParameter("strategy", strategy);
		update(null);

	}

	/**
	 *  Gets the strategy attribute of the ApplyRuleset object
	 *
	 *@return    The strategy value
	 */
	public final String getStrategy() {
		return attributes.getStringParameter("strategy");
	}

	/**
	 *  Prints an XML description of this ApplyRuleset instance
	 */
	public final void generateXML() {
		System.out.print("      <apply-ruleset id=\"" + id + "\" ref=\"" + getRef() + "\" strategy=\"" + getStrategy() + "\" ");
		System.out.print("in=\"");
		inPorts.generateXML();
		System.out.print("\" out=\"");
		outPorts.generateXML();
		if (parameters.isEmpty()) {
			System.out.println("\"/>");
		} else {
			System.out.println("\">");
			parameters.generateXMLCalls();
			System.out.println("      </apply-ruleset>");
		}
	}

	/**
	 *  Creates a TApplyRuleset corresponding to this ApplyRulset object and adds it to the composite
	 * 	process in the execution structure.
	 *
	 *@param  currentProcess  the container for this serializer
	 *
		*/
	public final void createProcess(TProcessComposite currentProcess) {
		try {
			String[] vPortIn = inPorts().toStringList();
			String[] vPortOut = outPorts().toStringList();
			// get the name corresponding to the generated stylesheet and pas it in parameter
			String ref = getAttributes().getStringParameter("ref");
			Transmorpher t = getProcess().getTransmorpher();
			String f = t.findTransformation(ref).getAttributes().getStringParameter("file");
			// static because generate by Transmorpher from the ruleset name
			getParameters().setParameter("file", f);
			TProcess vCurrent = new TApplyRuleset(vPortIn, vPortOut,
				getParameters(),
				getAttributes());
			currentProcess.addProcess(vCurrent);

			update(vCurrent);
		} catch (Exception e) {
			System.out.println("[ApplyRuleset] Error : cannot create Process");
			System.out.println("[ApplyRuleset] " + e);
		}
	}//end proc

	/**
	 *  
	 */
	public final void setUp() {
		if (getProcess().getTransmorpher().findTransformation(getRef()) == null) {
			System.err.println("warning : unknown ruleset : " + getRef());
		}//end if

		update(null);

	}//end proc


	/**
	 *  Prints a java code description of this ApplyQuery instance
	 *
	 *@param  file             A writer used for printing in afile
	 *@exception  IOException  when an IO errors occur
	 */
	public void generateJavaCode(Writer file) throws IOException {
		file.writeln();
		file.writeln(4, "//------------------ Generation of applyRuleSet " + getId() + " -------------------");
		generatePorts(file);
		// Fetch the file to be processed by XSLT (this is very xsltish)
		String ref = getAttributes().getStringParameter("ref");
		Transmorpher tr = getProcess().getTransmorpher();
		String f = tr.findTransformation(ref).getAttributes().getStringParameter("file");
		// Added just for being generated afterwards
		getParameters().setParameter("file", f);
		generateParameters(file);
		file.writeln(4, "tmCurrentProcess = new fr.fluxmedia.transmorpher.engine.TApplyRuleset(portIn,portOut,tmParameters,tmAttributes);");
		file.writeln(4, "tmCurrentProcess.setName(\"" + getId() + "\");");
		file.writeln("");
		file.writeln(4, "tmCurrentCompositeProcess.addProcess(tmCurrentProcess);");
	}//end proc

}

