/*
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.transmorpher.graph;

import fr.fluxmedia.transmorpher.action.TmActionImpl;
import fr.fluxmedia.transmorpher.engine.TApplyProcess;
import fr.fluxmedia.transmorpher.engine.TProcess;
import fr.fluxmedia.transmorpher.engine.TProcessComposite;
import fr.fluxmedia.transmorpher.utils.LinearIndexedStruct;
import fr.fluxmedia.transmorpher.utils.Writer;

import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.ListIterator;
import java.util.Stack;

/**
 * Transmorpher graph processes definitions.
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */

public class Process extends TransformationImpl {

	/**
	 *  The storage object for the calls of this Process instance 
	 */
	protected LinearIndexedStruct<Call> calls = null;
	/**
	 *  The storage object for the channels of this Process instance 
	 */
    protected Hashtable<String,Channel> channels = null;

	/**
	 *Creates an instance of Process 
	 *
	 *@param  name  	The name of this instance
	 *@param  owner  The Transmorpher instance that owns this instance
	 */
	public Process(String name, Transmorpher owner) {
		this(name, owner, 0, 0);
	}

	/**
	 *Constructor for the Process object
	 *
	 *@param  name  	The name of this instance
	 *@param  owner  	The Transmorpher instance that owns this instance
	 *@param  nbIn   	The number of in ports of this instance
	 *@param  nbOut  	The number of out ports of this instance
	 */
	public Process(String name, Transmorpher owner, int nbIn, int nbOut) {
		super(name, "Process", owner, nbIn, nbOut);
		calls = new LinearIndexedStruct<Call>();
		channels = new Hashtable<String,Channel>(15, 5);
	}

	/**
	 *  Adds a Call instance in this Process 
	 *
	 *@param  call  The call to add
	 */
	public void addCall(Call call) {
		calls.add( call.getId(), call );
		update(new TmActionImpl(TmActionImpl.ADD, this, call, null, null));
	}

	/**
	 *  Removes a call from this Process
	 *
	 *@param  call  The call instance to remove
	 */
	public void removeCall(Call call) {
		for( Port p : call.inPorts() ) removeChannel( p.getChannel() );
		for( Port p : call.outPorts() ) removeChannel( p.getChannel() );
		calls.remove( call.getId() );
		update(new TmActionImpl(TmActionImpl.REMOVE, this, call, null, null));
	}


	/**
	 * Delete all the calls to a particular transformation
	 * name is the name of a transformation (local or imported) to be suppressed.
	 *
	 *@param  name  The name of the transformation instance to remove
	 */
	public void deleteCallsTo(String name) {
		for (ListIterator<Call> l = calls.listIterator(); l.hasNext(); ) {
			Call currentCall = l.next();
			if ((currentCall instanceof ApplyImpl) &&
				((ApplyImpl)currentCall).getRef().equals(name)) {
				removeCallAndPorts(currentCall);
			}
		}
	}

	/**
	 * Delete a call (and the channels connected to this call)
	 *
	 *@param  c  Description of the Parameter
	 */
	public void removeCallAndPorts(Call call) {
		for( Port p : call.inPorts() ) removeChannelAndPorts( p.getChannel() );
		for( Port p : call.outPorts() ) removeChannelAndPorts( p.getChannel() );
		calls.remove(call.getId());
		// the call should not be reachable anymore
	}

	/**
	 * Delete a channel and Port associated.
	 *
	 *@param  c  Description of the Parameter
	 */
	public void removeChannelAndPorts(Channel c) {
		if (c.in() != null) {
			if (c.in().TransformationPortP()) {
				c.in().getTransformation().inPorts().removePort(c.in());
			} else {
				c.in().getCall().outPorts().removePort(c.in());
			}
		}
		if (c.out() != null) {
			if (c.out().TransformationPortP()) {
				c.out().getTransformation().outPorts().removePort(c.out());
			} else {
				c.out().getCall().inPorts().removePort(c.out());
			}
		}
		// remove the channel
		channels.remove( c.getName() );
		// the channel should not be reachable anymore
	}

	/**
	 *  Gets the calls attribute of the Process object
	 *
	 *@return    The calls value
	 */
	public LinearIndexedStruct<Call> getCalls() {
		return calls;
	}

	/**
	 *  Gets the call attribute of the Process object
	 *
	 *@param  name  The name of the call required
	 *@return    The call value
	 */
	public Call getCall(String name) {
		return calls.get(name);
	}

	/**
	 *  Adds a channel to this process
	 *
	 *@param  channel  The channel to add
	 */
	public void addChannel(Channel channel) {
		channels.put( channel.getName(), channel );

		if (channel.in() != null && channel.out() != null) {
			update(new TmActionImpl(TmActionImpl.ADD, this, channel, null, null));
		}
	}

	/**
	 *  Gets an enumeration of the channels of this Process object
	 *
	 *@return    The channels enumeration
	 */
	public Enumeration<Channel> getChannels() {
		return channels.elements();
	}

	/**
	 *  Gets the all channels  of the Process object
	 *
	 *@return    The allChannels 
	 */
	public Hashtable<String,Channel> getAllChannels() {
		return channels;
	}

	/**
	 *  Gets the channel  of this Process object cooresponding to the given name
	 *
	 *@param  name  The name of the channel
	 *@return    The channel if exist else null
	 */
	public Channel getChannel(String name) {
		return channels.get(name);
	}

	/**
	 *Remove a channel 
	 *
	 *@param  channel  The channel to remove
	 */

	public void removeChannel(Channel channel) {
		if (channel != null) {
			if (channel.in() != null) {
				channel.in().setChannel(null);
				channel.in().setName(null);
			}
			if (channel.out() != null) {
				channel.out().setChannel(null);
				channel.out().setName(null);
			}

			channels.remove( channel.getName() );
			update(new TmActionImpl(TmActionImpl.REMOVE, this, channel, null, null));
		}
	}


	/**
	 *  Description of the Method
	 *
	 *@param  out  Description of the Parameter
	 *@param  in   Description of the Parameter
	 */
	public void retroNull(PortList out, PortList in) {
		retroNull(out, in, true);
	}

	// Problem with the STACK
	// The boolean is only used for selecting the method
	/**
	 *  Description of the Method
	 *
	 *@param  out  Description of the Parameter
	 *@param  in   Description of the Parameter
	 *@param  b    Description of the Parameter
	 *@return      Description of the Return Value
	 */
	public boolean retroNull(PortList out, PortList in, boolean b) {
		Stack<Call> stack = new Stack<Call>();
		for (int i = 0; i != out.length(); i++) {
			outPorts.getPort(i).getChannel().setVisited();
			if (out.getPort(i).getChannel().nullifiedP()) {
				outPorts.getPort(i).getChannel().nullify();
			}
			// stack.push(outPorts.getPort(i).getChannel().);
		}
		retroNull();
		// report on in
		boolean nullified = true;
		for (int i = 0; i != in.length(); i++) {
			in.getPort(i).getChannel().setVisited();
			if (inPorts.getPort(i).getChannel().nullifiedP()) {
				in.getPort(i).getChannel().nullify();
			} else {
				nullified = false;
			}
		}
		return nullified;
	}

	/**
	 *  Description of the Method
	 */
	public void retroNull() {
		Stack<Call> stack = new Stack<Call>();
		for (Enumeration<Channel> e = getChannels(); e.hasMoreElements(); ) {
			Channel c = e.nextElement();
			if (c instanceof Null) {
				c.setVisited();
				if (c.in().CallPortP()) {
					stack.push(c.in().getCall());
				}
			}
		}
		// enumeration of the Stack
		/*
		 *for (;;) {
		 *Call c = (Call)stack.pop();
		 *if () // all in are visited
		 *c.retroNull();
		 *}
		 */
		clearVisited();
	}

	/**
	 *  Description of the Method
	 */
	public void clearNull() {
		for (ListIterator<Call> l = calls.listIterator(); l.hasNext(); ) {
			l.next().clearNull();
		}
		for (Enumeration<Channel> e = getChannels(); e.hasMoreElements(); ) {
			e.nextElement().clearNull();
		}
	}

	/**
	 *  Description of the Method
	 */
	public void clearVisited() {
		for (ListIterator<Call> l = calls.listIterator(); l.hasNext(); ) {
			l.next().clearVisited();
		}
		for (Enumeration<Channel> e = getChannels(); e.hasMoreElements(); ) {
			e.nextElement().clearVisited();
		}
	}

	/**
	 *  Description of the Method
	 *
	 *@return    Description of the Return Value
	 */
	private String XMLName() {
		return "process";
	}

	/**
	 * Prints the XML expression of the process
	 */
	public void generateXML() {
		System.out.print("<" + XMLName() + " name=\"" + name + "\" in=\"");
		inPorts.generateXML();
		System.out.print("\" out=\"");
		outPorts.generateXML();
		System.out.println("\">");
		parameters.generateXMLDeclarations();
		for (ListIterator<Call> l = calls.listIterator(); l.hasNext(); ) {
			l.next().generateXML();
		}
		System.out.println("  </" + XMLName() + ">");
		System.out.println();
	}

	/**
	 *  
	 */
	public void setUp() {
		for (ListIterator<Call> l = calls.listIterator(); l.hasNext(); ) {
			l.next().setUp();
		}
	}

	/**
	 *  Prints a java code description of this process instance
	 *
	 *@param  file            	The write used for printing in a file
	 *@param  externs          	Description of the Parameter
	 *@exception  IOException  	when IO errors occur.
	 */
	public void generateJavaCode(Writer file, LinearIndexedStruct<String> externs) throws IOException { }

}

