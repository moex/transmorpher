/*
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2003, 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.graph;

// Transmorpher imports

import fr.fluxmedia.transmorpher.action.*;
import fr.fluxmedia.transmorpher.engine.TProcessComposite;
import fr.fluxmedia.transmorpher.parser.FMParser;
import fr.fluxmedia.transmorpher.parser.ProcessParser;
import fr.fluxmedia.transmorpher.utils.LinearIndexedStruct;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.TMException;
import fr.fluxmedia.transmorpher.utils.Version;
import fr.fluxmedia.transmorpher.utils.Writer;

// import Java classes

import java.util.Hashtable;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Observable;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Enumeration;

// JAXP
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

// SAX class
import org.xml.sax.SAXException;

/**
 * This class provides methods to create a graph structure corresponding to a transformation process
 * defined in an XML file or with Flowcomposer ( a GUI tool for Transmorpher) .
 * There is many ways to use this structure :
 * <ul>
 * <li>It can be printed in an XML form on stdout.</li>
 * <li>It can be transformed in a java class that can be compiled</li>
 * <li>It can be executed (using threads or not).</li>
 * </ul>
 *
 */
public class Transmorpher extends Observable {

	/**
	 * The name of the Transmorpher sheet
	 */
	protected String name = null;

	/**
	 * The filename of the Transmorpher sheet
	 */
	protected File href = null;

	/**
	 * The main process of the Transmorpher sheet
	 */
	protected MainProcess main = null;

	/**
	 * the list of Processes
	 */
	protected LinearIndexedStruct<Transformation> transformations = null;

	/**
	 * the version to be used
	 */
	protected String version = null;

	/** 
	*	the execution structure  
	*/
	protected TProcessComposite iExecutionStructure = null;

	/**
	 * the list of imports must be a list of Transmorphers
	 *once the imports have been parsed. In such a case the URL can be found
	 *in its href attribute
	 * But it does not seems that way
	 */
	protected LinkedList<Object> imports = null;

	/**
	 * the list of includes
	 * same as above
	 */
	protected LinkedList<Object> includes = null;

	/**
	 * the list of declared names of components
	 */
	protected LinearIndexedStruct<String> defexterns = null;
	/**
	 *  store the types of the (built-in) components  and their corresponding classes to use for creating them
	 */
	protected LinearIndexedStruct<String> listOfType = null;
	/**
	 *  value of the debug mode
	 */
	protected int debug = 0;

	/**
	 *  a path for the output of transmorpher (java or xslt)
	 */
	protected String reloc = "";

	/**
	 *  if true the production of XML description of this instance is required
	 */
	protected boolean opt = false;

	/**
	 *  a flag to show if thread are used or not
	 */
	protected boolean useThread = false;

	/**
	 *Constructor for the Transmorpher object
	 *
	 *@param  debug  the debug mode value
	 */
	public Transmorpher(int debug) {
		// JE: name might be compulsory...
		this.debug = debug;
		transformations = new LinearIndexedStruct<Transformation>();
		defexterns = new LinearIndexedStruct<String>();
		imports = new LinkedList<Object>();
		includes = new LinkedList<Object>();
		if (listOfType == null) {
			initListOfType();
		}
	}

	/**
	 *Constructor for the Transmorpher object
	 *
	 *@param  name  		The name of this Transmorpher instance
	 *@param  version  	The version of this Transmorpher instance
	 *@param  debug  		The debug mode value
	 *@param  optimize  The optimize flag (if true , this instance will produce an XML description of itself)
	 */
	public Transmorpher(String name, String version, int debug, boolean optimize) {
		this(debug);
		opt = optimize;
		this.name = name;
		href = new File(name + ".xml");
		this.version = version;
		if (!version.equals(Version.VERSION)) {
			System.err.println("Warning : bad version " + version + " instead of " + Version.VERSION);
		}
	}

	/**
	 *Constructor for the Transmorpher object
	 *
	 *@param  name  		The name of this Transmorpher instance
	 *@param  version  	The version of this Transmorpher instance
	 *@param  debug  		The debug mode value
	 *@param  reloc  		a path for outputs
	 *@param  optimize  The optimize flag (if true , this instance will produce an XML description of itself)
	 */
	public Transmorpher(String name, String version, int debug, String reloc, boolean optimize) {
		this(name, version, debug, optimize);

		if (reloc != null) {
			this.reloc = reloc;
		}
	}

	// This is a Transmorpher inheriting some externs...
	/**
	 *Constructor for the Transmorpher object
	 *
	 *@param  name  				The name of this Transmorpher instance
	 *@param  version  			The version of this Transmorpher instance
	 *@param  debug  				The debug mode value
	 *@param  reloc  				a path for outputs
	 *@param  optimize  		The optimize flag (if true , this instance will produce an XML description of itself)
	 *@param  transmorpher  Description of the Parameter
	 */
	public Transmorpher(String name, String version, int debug, String reloc, boolean optimize, Transmorpher transmorpher) {
		this(name, version, debug, reloc, optimize);
		defexterns = transmorpher.getDefexterns();
	}

	/**
	 *  Initializes the list of type supported by Transmorpher. 
	 * This list contains all the types of the execution components that are provided
	 * with Transmorpher and the corresponding classes to use for instanciate these components. 
	 */
	public void initListOfType() {
		listOfType = new LinearIndexedStruct<String>();
		// Default merger/dispatchers
		listOfType.add("broadcast", "fr.fluxmedia.transmorpher.stdlib.Broadcast");
		listOfType.add("concat", "fr.fluxmedia.transmorpher.stdlib.Concat");
		listOfType.add("wrap", "fr.fluxmedia.transmorpher.stdlib.Wrap");
		// Default reader/writers
		listOfType.add("readfile", "fr.fluxmedia.transmorpher.stdlib.ReadFile");
		listOfType.add("writefile", "fr.fluxmedia.transmorpher.stdlib.WriteFile");
		// These two should be replaced by two different classes...
		listOfType.add("standardInput", "fr.fluxmedia.transmorpher.engine.TReader");
		listOfType.add("standardOutput", "fr.fluxmedia.transmorpher.stdlib.StdOut");
		// Default transformations
		listOfType.add("xslt", "fr.fluxmedia.transmorpher.stdlib.XSLT");
		listOfType.add("tmq", "fr.fluxmedia.transmorpher.stdlib.TMQuery");
		listOfType.add("if", "fr.fluxmedia.transmorpher.stdlib.TestingDispatcher");
	}

	/**
	 *  Gets the name attribute of the Transmorpher object
	 *
	 *@return    The name value
	 */
	public final String getName() {
		return name;
	}

	/**
	 *  Sets the name attribute of the Transmorpher object
	 *
	 *@param  name  The new name value
	 */
	public final void setName(String name) {
		this.name = name;
		setChanged();
		//notifyObservers(new TmAddTransformationAction(t));
		notifyObservers();
	}

	/**
	 * Get the value of debug.
	 *
	 *@return    value of debug.
	 */
	public int getDebug() {
		return debug;
	}

	/**
	 * Set the value of debug.
	 *
	 *@param  debug  Value to assign to debug.
	 */
	public void setDebug(int debug) {
		this.debug = debug;
	}

	/**
	 * Tests if this Transmorpher instance uses thread
	 *@return    true if using thread.
	 */
	public boolean isThread() {
		return useThread;
	}

	/**
	 * Initializes this Transmorpher instance for the use of thread or not
	 *@param  thread  if true, thread are used.
	 */
	public void useThread(boolean thread) {
		this.useThread = thread;
	}


	/**
	 *  Gets the file attribute of the Transmorpher object
	 *
	 *@return    The file value
	 */
	public final File getFile() {
		return href;
	}

	/**
	 *  Sets the file attribute of the Transmorpher object
	 *
	 *@param  uri  The new file value
	 */
	public final void setFile(File uri) {
		href = uri;
	}

	/**
	 *  Gets the version attribute of the Transmorpher object
	 *
	 *@return    The version value
	 */
	public final String getVersion() {
		return version;
	}

	/**
	 *  Sets the version attribute of the Transmorpher object
	 *
	 *@param  version  The new version value
	 */
	public final void setVersion(String version) {
		this.version = version;
	}

	/**
	 *  Gets the value of the optimized flag
	 *
	 *@return    true if an XML description of this instance is required else false
	 */
	public final boolean optimizedP() {
		return opt;
	}

	/**
	 *  Sets the optimized attribute of the Transmorpher object
	 *
	 *@param  b  The new optimized value
	 */
	public final void setOptimized(boolean optimize) {
		opt = optimize;
	}

	/**
	 *  Gets the main attribute of the Transmorpher object
	 *
	 *@return    The main value
	 */
	public final MainProcess getMain() {
		return main;
	}

	/**
	 *  Gets the transformations attribute of the Transmorpher object
	 *
	 *@return    The transformations value
	 */
	public LinearIndexedStruct<Transformation> getTransformations() {
		return transformations;
	}

	/**
	 *  Returns a transformtion corresponding to the name. 
	 *
	 *@param  name  The name of the transformation to find
	 *@return       The transformation corresponding to the name, null if no transformation found
	 */
	public final Transformation findTransformation(String name) {
		Transformation result = transformations.get(name);
		if (result != null) {
			return result;
		}
		for (ListIterator<Object> l = imports.listIterator(0); (l.hasNext() && (result == null)); ) {
			Object o = l.next();
			if (o instanceof Transmorpher) {
				result = ((Transmorpher)o).findTransformation(name);
			}
		}
		return result;
	}

	/**
	 *  Gets the transformation attribute of the Transmorpher object
	 *
	 *@param  name  Description of the Parameter
	 *@return       The transformation value
	 */
	public final Transformation getTransformation(String name) {
		return transformations.get(name);
	}

	/**
	 *  Adds a feature to the Transformation attribute of the Transmorpher object
	 *
	 *@param  name  The feature to be added to the Transformation attribute
	 *@param  t     The feature to be added to the Transformation attribute
	 */
	public final void addTransformation(String name, Transformation t) {
		// JE: If it already exists one == error (this will cause filename problems)
		transformations.add( name, t );
		setChanged();
		notifyObservers(new TmActionImpl(TmActionImpl.ADD, this, t, null, null));
		//notifyObservers(t);
	}

	/**
	 *  Removes a transformtion from this Transmorpher instance
	 *
	 *@param  trans The transformation to remove
	 */
	public final void removeTransformation(Transformation trans) {
		transformations.remove( trans.getName() );
		// Check that all the processes defined here do not use the transformation.
		// If so, suppress it from the transformation p.deleteCall( c )
		if (!(trans instanceof MainProcess)) {
		    for ( Transformation t: transformations ) {
			if (t instanceof Process) {
			    ((Process)t).deleteCallsTo(trans.getName());
			}
		    }
		}// end if
		// Take care of the main
		if (main == (MainProcess)trans) {
			main = null;
		}
		// the transformation should not be reachable anymore

		setChanged();
		notifyObservers(new TmActionImpl(TmActionImpl.REMOVE, this, trans, null, null));

	}

	/**
	 *  Looks for a name in the list of type and in the defextern list.
	 * Returns the name of the class which corresponds to this name.
	 *
	 *@param  name  The name
	 *@return       The class corresponding to the searched type
	 */
	public final String getClassForType(String name) {
		String className = null;
		className = listOfType.get(name);
		if (className == null) {
			className = defexterns.get(name);
		}
		return className;
	}

	/**
	 *  Looks for a name in the defextern list.
	 * Returns the name of the class which corresponds to this name.
	 *
	 *@param  name  Description of the Parameter
	 *@return       The class corresponding to the searched name
	 */
	public final String getExtern(String name) {
		return defexterns.get(name);
	}

	/**
	 *  Adds an externs definition to the defextern list
	 *
	 *@param  name  The type of the component to add
	 *@param  className    The class of the component to add
	 *@param  impl  The feature to be added to the Extern attribute
	 *@param  def   The feature to be added to the Extern attribute
	 */
	public final void addExtern(String name, String className, String impl, boolean def) {
		if (getExtern(name) != null) {
			System.err.println("Error: " + name + " external already declared");
		} else {
			defexterns.add(name, className);
		}
	}

	/**
	 *  Removes the value corresponding to the name from the extern list
	 *
	 *@param  name  the name of the extern defniition to remove
	 */
	public final void removeExtern(String name) {
		defexterns.remove(name);
		// Check that all the processes defined here do not use the external call.
		// If so, suppress it from the transformation p.deleteCall( c )
		for ( Transformation t: transformations ) {
		    if (t instanceof Process) {
			((Process)t).deleteCallsTo(name);
		    }
		}
	}

	/**
	 *  Gets the defexterns attribute of the Transmorpher object
	 *
	 *@return    The defexterns value
	 */
	public final LinearIndexedStruct<String> getDefexterns() {
		return defexterns;
	}

	/**
	 * just put the import URI in the linked list *
	 *
	 *@param  i  Description of the Parameter
	 */
	public final void reserveImport(String i) {
		imports.add( i );
	}

	/**
	 * replace the import URI by the actual import *
	 *
	 *@param  i  Description of the Parameter
	 *@param  t  Description of the Parameter
	 */
	public final void recordImport(String i, Transmorpher t) {
		imports.set( imports.indexOf((Object)i), t );
	}

	/**
	 * This is an internal method for dynamically adding imports *
	 *
	 *@param  i                                 The feature to be added to the Import attribute
	 *@exception  ParserConfigurationException  Description of the Exception
	 *@exception  SAXException                  Description of the Exception
	 *@exception  IOException                   Description of the Exception
	 */
	public final void addImport(String i) throws ParserConfigurationException, SAXException, IOException {
		// JE: The String i must be transformed in a URL that could be loaded!
		FMParser iProcessParser = new ProcessParser(0);
		Transmorpher timp = iProcessParser.newParse(i);
		// JE: Check if there will not be a name conflict
		imports.add( timp );
	}

	/**
	 *  Gets the imports attribute of the Transmorpher object
	 *
	 *@return    The imports value
	 */
	public final LinkedList<Object> getImports() {
		return imports;
	}

	/**
	 *  Adds a feature to the Include attribute of the Transmorpher object
	 *
	 *@param  i  The feature to be added to the Include attribute
	 */
	public final void addInclude(String i) {
		includes.add( i );
	}

	/**
	 *  Gets the includes attribute of the Transmorpher object
	 *
	 *@return    The includes value
	 */
	public final LinkedList<Object> getIncludes() {
		return includes;
	}

	/**
	 *  Sets the main attribute of the Transmorpher object
	 *
	 *@param  p  The new main value
	 */
	public final void setMain(MainProcess p) {
		main = p;
	}

	/**
	 *  Description of the Method
	 */
	public final void retroNull() {
		main.retroNull();
	}

	/**
	 *  Description of the Method
	 *
	 *@exception  TMException  Description of the Exception
	 */
	public final void save() throws TMException {
		if (href != null && href.canWrite()) {
			if (href.exists()) {
				href.renameTo(new File(href + "~"));
			}
			PrintStream stdout = System.out;
			try {
				System.setOut(new PrintStream(
					new BufferedOutputStream(
					new FileOutputStream(href))));
				generateXML();
				System.out.close();
			} catch (FileNotFoundException e) {
				throw new TMException("Cannot save in file " + href);
			} finally {
				System.setOut(stdout);
			}
		} else {
			throw new TMException("Cannot save in file " + href);
		}
	}

	/**
	 *  Description of the Method
	 *
	 *@exception  TMException  Description of the Exception
	 */
	public final void saveAll() throws TMException {
		// JE: How to avoid saving twice ?
	    for ( Object o: imports ) {
		((Transmorpher)o).saveAll();
	    }
	    for ( Object o: includes ) {
		((Transmorpher)o).saveAll();
	    }
	    save();
	}

	/**
	 *  Prints an XML description of this Transmorpher instance
	 */
	public final void generateXML() {
		// This does not generate the imported files
		System.out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		System.out.println("<!DOCTYPE transmorpher SYSTEM \"http://transmorpher.inrialpes.fr/dtd/transmorpher.dtd\">");
		System.out.println("<transmorpher name=\"" + name + "\"");
		System.out.println("              version=\"" + version + "\"");
		System.out.println("              reloc=\"" + reloc + "\"");
		System.out.println("              optimized=\"" + opt + "\"");
		System.out.println("              xmlns=\"http://transmorpher.fluxmedia.fr/1.0\"");
		System.out.println("              xmlns:regexp=\"http://fr.fluxmedia.transmorpher.stdlib.RegularExpression\">");
		System.out.println();

		Enumeration<String> keys = defexterns.getKeys();
		while (keys.hasMoreElements()) {
			String name = keys.nextElement();
			System.out.println("<defextern name=\"" + name + "\" class=\"" + defexterns.get(name) + "\"/>");
		}
		for ( Object o: imports ) {
		    System.out.println("<import href=\"" + ((Transmorpher)o).getFile().getPath() + "\"/>");
		}
		for ( Object o: includes ) {
			System.out.println("<include href=\"" + ((Transmorpher)o).getFile().getPath() + "\"/>");
		}
		for ( Transformation t: transformations ) {
		    if (t != main) t.generateXML();
		}
		if (main != null) {
			main.generateXML();
		}
		System.out.println("</transmorpher>");
	}//end generate XML

	/**
	 *  Launches the conversion of Ruleset and Query to XSLT files
	 *
	 *@exception  TMException  Description of the Exception
	 *@exception  IOException  Description of the Exception
	 */
	public void generateStylesheet() throws TMException, IOException {
	    for ( Transformation t: transformations ) {
		if (t instanceof Ruleset) {
		    ((Ruleset)t).generateXSLTStylesheet(reloc);
		} else if (t instanceof Query) {
		    ((Query)t).generateXSLTStylesheet(reloc);
		}
	    }
	}

	/**
	 * Generates the execution structure corresponding to this Transmorpher instance
	 *
	 *@param  		reloc                              Description of the Parameter
	 *@exception  TransformerException               Description of the Exception
	 *@exception  TransformerConfigurationException  Description of the Exception
	 *@exception  TMException                        Description of the Exception
	 *@exception  SAXException                       Description of the Exception
	 *@exception  IOException                        Description of the Exception
	 */
	public final void generateExec(String reloc)
		 throws TransformerException,
		TransformerConfigurationException,
		TMException,
		SAXException,
		IOException {
		if (reloc != null) {
			this.reloc = reloc;
		}
		generateStylesheet();
		for (ListIterator<Object> l = imports.listIterator(0); l.hasNext(); ) {
			((Transmorpher)l.next()).generateStylesheet();
		}

		if (getMain() != null) {
			iExecutionStructure = getMain().createMainProcess();
			// JE: This should include the generation of the import/include!
			// So far, this is not absolutely easy
		} else {
			throw new TMException("Cannot generate execution structure for mainless Transformation");
		}
	}//end generateExec

	/**
	 *  Generates a java code description for this Transmorpher instance
	 *
	 *@param  		reloc        The path for the outputs
	 *@exception  IOException  Description of the Exception
	 *@exception  TMException  Description of the Exception
	 */
	public final void generateJavaCode(String reloc) throws IOException, TMException {
		String iCompilDirectory = null;
		Writer iFile = null;
		if (reloc != null) {
			this.reloc = reloc;
		}
		for (ListIterator<Transformation> l = transformations.listIterator(); l.hasNext(); ) {
			Transformation t = l.next();
			if (t instanceof Ruleset) {
				((Ruleset)t).generateXSLTStylesheet(this.reloc);
			} else if (t instanceof Query) {
				((Query)t).generateXSLTStylesheet(this.reloc);
			}
		}
		try {
			iCompilDirectory = getFile().getAbsoluteFile().getParent();
			if (this.reloc != null) {
				iCompilDirectory = this.reloc;
			} else {
				iCompilDirectory = getFile().getAbsoluteFile().getParent();
			}
			File dir = new File(iCompilDirectory);
			dir.mkdir();// directly tries to create the directory that might exist
			iCompilDirectory += File.separator;
			iFile = new Writer(iCompilDirectory + getMain().getName() + ".java");
			getMain().generateJavaCode(iFile, defexterns);
		} finally {
			iFile.close();
		}
		
	}//end generate JavaCode

	/**
	 *  Starts the execution of the process described by this Transmorpher instance
	 *
	 *@param  p              Description of the Parameter
	 *@exception  Exception  Description of the Exception
	 */
	public final void exec(Parameters p) throws Exception {
		if (iExecutionStructure != null) {
			iExecutionStructure.bindParameters(p);
			iExecutionStructure.execProcess(useThread);
		}
	}//end run

}

