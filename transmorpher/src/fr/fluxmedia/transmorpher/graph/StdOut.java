/*
 * $Id: StdOut.java,v 1.2 2003-01-30 15:47:46 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.graph ;

import fr.fluxmedia.transmorpher.utils.Writer;
import fr.fluxmedia.transmorpher.utils.TMException;

/**
  * Transmorpher graph _stdout_ channel representation
  *
  * @author Jerome.Euzenat@inrialpes.fr
  * @since jdk 1.3 / SAX 2.0
  */
public class StdOut extends Channel {

	/**
	 * Creates an instance of SdtOut with a name and a process
	 *
	 *@param  name  		The name of the instance to create
	 *@param  process   The process that owns this instance
	 */
  public StdOut(String name, Process process){
    super(name,process);
  }

	/**
	 * Creates an instance of SdtIn with a name ,a process, an in port and an out port (null).
	 *
	 *@param  name  					The name of the instance to create 
	 *@param  process   			The process that owns this instance
	 *@param  in     					The in port of the instance to create 
	 *@param  out     				The out port of the instance to create (must be null)
	 *@exception  TMException  if out port is not null
	 */
  public StdOut(String name, Process process, Port in, Port out) throws TMException {
    super(name, process, in, (Port)null);
    if ( out != null )
      throw new TMException("Cannot give output to stdout channel");
  }

}
