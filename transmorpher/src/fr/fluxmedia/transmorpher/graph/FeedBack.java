/**
 * $Id: FeedBack.java,v 1.1 2002-11-06 14:08:21 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
  * Transmorpher graph feedback channel representation
  *
  * @author Jerome.Euzenat@inrialpes.fr
  * @since jdk 1.3 / SAX 2.0
  */

package fr.fluxmedia.transmorpher.graph ;

import fr.fluxmedia.transmorpher.utils.Writer;

      /** A feed back channel is made of two subchannels, It thus takes two
       * additionnal ports.
       * in is the begining of the first sub-channel
       * innerIn that of the second one (that will be recopied on the first one)
       * out is be the output of the first subchannel
       * innerOut is the input of the second subchannel.
       * It can be either (1) empty, (2) filled with
       * an output port of the repeat, (3) filled with an input port of a
       * successive call */

public class FeedBack extends Channel {
      
      private Port innerInput = null;
      private Port innerOutput = null;
      
  public FeedBack(String name, Process p){
    super(name,p);
  }
      
  /** This might really be useless */
  public FeedBack(String name, Process p, Port i, Port ii, Port io, Port o){
    super(name,p);
    in = i;
    innerInput = ii;
    innerOutput = io;
    out = o;
  }
  
  public Port innerInput(){
	return innerInput;
  }

  public Port innerOutput(){
	return innerOutput;
  }

  public void setIn(Port p) {
      if ( in == null ) {
          in = p;
	//System.out.println("FEEDBACK "+this.getName()+" INPUT :"+p.getName()+" ("+p+")");
      } else {
	  innerInput = p;
	//System.out.println("FEEDBACK "+this.getName()+" INNER INPUT :"+p.getName()+" ("+p+")");
      }
  }
  
  /** This is inversed because in the case of an output of the repeat,
  then it must be stored second */
  public void setOut( Port p ) {
      if ( innerOutput == null ) {
	//System.out.println("FEEDBACK "+this.getName()+" INNER OUTPUT :"+p.getName()+" ("+p+")");
          innerOutput = p;
      } else {
	  out = p;
	//System.out.println("FEEDBACK "+this.getName()+" OUTPUT :"+p.getName()+" ("+p+")");
      }
  }

}//end class
