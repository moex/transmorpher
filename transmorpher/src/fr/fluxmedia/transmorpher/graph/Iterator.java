/*
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003, 2022.
 *
 * https://gitlab.inria.fr/moex.transmorpher - https://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.transmorpher.graph;
import fr.fluxmedia.transmorpher.engine.*;
import fr.fluxmedia.transmorpher.utils.Parameters;

// imported fr.fluxmedia.transmorpher classes

import fr.fluxmedia.transmorpher.utils.Writer;
import java.io.IOException;

//imported java classes

import java.util.Enumeration;

/**
 *  Transmorpher graph iterator implementation The iterator class introduces the
 *  use of a variable which will be iterated over a particular structure. It
 *  must contain a name, a type and a number of other parameters that will
 *  specify its behavior.
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */
public class Iterator {

	/**
	 *  Iterator name.
	 */
	protected String name = null;

	/**
	 *  The iterator parameters.
	 */
	protected Parameters parameters = null;

	/**
	 * Iterator type
	 */
	protected String type = null;

	/**
	 *  The owner . It must be a Repeat
	 */
	protected Repeat repeat = null;

	/**
	 *  Creates a simple instance of iterator.
	 *
	 */
	public Iterator(){
	}
	
	/**
	 *  Creates an instance of iterator.
	 *
	 *@param  name    the name of the iterator.
	 *@param  type    the type of the iterator.
	 *@param  repeat  the owner of the iterator.
	 */
	public Iterator(String name, String type, Repeat repeat) {
		parameters = new Parameters();
		this.name = name;
		this.type = type;
		this.repeat = repeat;
	}

	/**
	 *  Prints the XML expression of the call. This is a default printer that can
	 *  be overloaded
	 */
	public void generateXML() {
		System.out.print("        <iterate name=\"" + name + "\" type=\"" + type + "\"");
		if (parameters.isEmpty()) {
			System.out.println("/>");
		} else {
			System.out.println(">");
			parameters.generateXMLCalls();
			System.out.println("        </iterate>");
		}
	}

	/**
	 *  Returns the name.
	 *
	 *@return    the name of the iterator.
	 */
	public String getName() {
		return name;
	}

	/**
	 *  Returns the owner (repeat) of the iterator.
	 *
	 *@return    the owner.
	 */
	public Repeat getRepeat() {
		return repeat;
	}

	/**
	 *  Sets the type of iterator.
	 *
	 *@param  type  the type.
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 *  Returns iterator type.
	 *
	 *@return    the type.
	 */
	public String getType() {
		return type;
	}

	/**
	 *  Sets parameters.
	 *
	 *@param  parameters  The new parameters value
	 */
	public void setParameters(Parameters parameters) {
		this.parameters = parameters;
	}

	/**
	 *  Returns parameters.
	 *
	 *@return    parameters.
	 */
	public Parameters getParameters() {
		return parameters;
	}

	/**
	 *  Description of the Method
	 *
	 *@param  type    Description of the Parameter
	 *@param  params  Description of the Parameter
	 *@return         Description of the Return Value
	 */
	private TIterator newIterator(String type, Object[] params) {
		try {
			Class processClass = Class.forName(type);
			java.lang.reflect.Constructor[] processConstructors = processClass.getConstructors();
			return (TIterator)processConstructors[0].newInstance(params);
		} catch (Exception e) {
			System.out.println("[Iterator] Error : cannot create Iterator");
			System.out.println("[Iterator] " + e);
			return (TIterator)null;
		}
	}

	/**
	 *  Creates a new TIterator corresponding to the iterator and adds it to the
	 *  current composite process (this must be a TLoop because iterators cannot be
	 *  used inside others components)
	 *
	 *@param  process  the current TLoop.
	 */
	public void createIterator(TProcessComposite process) {
		String className = this.getRepeat().getProcess().getTransmorpher().getClassForType(getType());
		Object[] params = {(Object)name, (Object)parameters};

		TIterator iterator = newIterator(className, params);
		if (process instanceof TLoop) {
			((TLoop)process).addIterator(iterator);
		} else {
			System.out.println("Can not add an Iterator to this process");
		}
	}

	/**
	 *  Generates java code for this component.
	 *
	 *@param  file             	a writer for printing in a file
	 *@exception  IOException 	when an IO errors occur
	 */
	public void generateJavaCode(Writer file) throws IOException {
		file.writeln();
		file.writeln(4, "//------------------ Generation of Iterator " + name + " -------------------");
		file.writeln(4, "name=\"" + name + "\";");
		file.writeln(4, "type=\"" + type + "\";");
		generateParameters(file);
		String className = this.getRepeat().getProcess().getTransmorpher().getClassForType(getType());

		file.writeln(4, "iterator = new " + className + "(name,tmParameters);");
		file.writeln(4, "if (tmCurrentCompositeProcess instanceof TLoop) {");
		file.writeln(8, "((TLoop) tmCurrentCompositeProcess).addIterator(iterator);");
		file.writeln(4, "}");
	}

	/**
	 *  Generates java code for the parameters of this iterator.
	 *
	 *@param  file             	a writer for printing in a file
	 *@exception  IOException 	when an IO errors occur
	 */
	public void generateParameters(Writer file) throws IOException {
		file.writeln();
		file.writeln(4, "tmParameters = new Parameters(); ");
		for (Enumeration e = getParameters().getNames(); e.hasMoreElements(); ) {
			String key = (String)e.nextElement();
			String val = (String)getParameters().getParameter(key);
			file.writeln(4, "tmParameters.setParameter(\"" + key + "\",\"" + val + "\" );");
		}
		file.writeln();
	}
}

