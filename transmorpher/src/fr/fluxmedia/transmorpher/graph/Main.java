/*
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003, 2022.
 *
 * https://gitlab.inria.fr/moex.transmorpher - https://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.transmorpher.graph;
import fr.fluxmedia.transmorpher.engine.*;
import fr.fluxmedia.transmorpher.utils.LinearIndexedStruct;
import fr.fluxmedia.transmorpher.utils.Parameters;

import fr.fluxmedia.transmorpher.utils.Writer;
import java.io.IOException;
import java.util.Enumeration;

import java.util.Hashtable;
import java.util.ListIterator;

/**
 * A Main instance is a graph component that can contains calls
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */
public class Main extends MainProcess {

	/**
	 *Constructor for the Main object
	 *
	 *@param  name  	The name of the Main instance to create
	 *@param  owner   The transmorpher which owns this instance
	 */
	public Main(String name, Transmorpher owner) {
		super(name, owner, 0, 0);
	}

	/**
	 * Prints the XML expression of the main
	 */
	public void generateXML() {
		System.out.println("  <main name=\"" + getName() + "\">");
		parameters.generateXMLDeclarations();
		for ( Call call: calls ) {
		    call.generateXML();
		}
		System.out.println("  </main>");
		System.out.println();
	}

	/**
	 *  Creates an execution component corresponding to this Main and starts the creation
	 *  of execution components of each calls of this Main
	 *
	 *@return    an execution component
	 */
	public TProcessComposite createMainProcess() {
		try {
			TProcessComposite vCurrent = new TMain(getParameters());
			for ( Call call: calls ) {
			    call.createProcess(vCurrent);
			}
			return vCurrent;
		} catch (Exception e) {
			System.out.println("[Main] Error : cannot create Process");
			System.out.println("[Main] " + e);
			return null;
		}
	}

	/**
	 *  Generates java code for import
	 *
	 *@param  file            The writer used for printing in a file
	 *@param  externs         Description of the Parameter
	 *@exception  IOException When IO errors occur
	 */
	public void generateImport(Writer file, LinearIndexedStruct<String> externs) throws IOException {
		file.writeln("import fr.fluxmedia.transmorpher.application.CommandLineArgument;");
	}

	/**
	  *  Generates java code for class header
	 *
	 *@param  file            The writer used for printing in a file
	 *@exception  IOException When IO errors occur
	 */
	public void generateClassHeader(Writer file) throws IOException {
		file.writeln();
		file.writeln("public class " + getName() + " {");
	}

	/**
	 *  Generates java code for main header
	 *
	 *@param  file            The writer used for printing in a file
	 *@exception  IOException When IO errors occur
	 */
	public void generateMainHeader(Writer file) throws IOException {
		file.writeln(2, "public static void main(String[] argc) throws Exception {");
		file.writeln(4, "tmProcessStack = new Stack<TProcessComposite>();");
		//iFile.writeln(4,"vSR = new SystemResources();");
	}

	/**
	  *  Generates java code for command line parameters
	 *
	 *@param  file            The writer used for printing in a file
	 *@exception  IOException When IO errors occur
	 */
	public void generateReadParameters(Writer file) throws IOException {
		file.writeln(4, "CommandLineArgument arg = new CommandLineArgument();");
		file.writeln(4, "arg.argsAnalyse(argc);");
		file.writeln(4, "debug_mode = arg.getDebugMode();");
		file.writeln(4, "boolean thread_mode = arg.getThreadMode();");
		file.writeln(4, "tmParameters = arg.getParameters();");
	}

	/**
	 *  Generates java code for body
	 *
	 *@param  file            The writer used for printing in a file
	 *@param  externs          Description of the Parameter
	 *@exception  IOException When IO errors occur
	 **/
	public final void generateBody(Writer file, LinearIndexedStruct<String> externs) throws IOException {
		Parameters param = getParameters();
		file.writeln(4, "Parameters p = new Parameters();");
		for (Enumeration e = param.getNames(); e.hasMoreElements(); ) {
			String key = (String)e.nextElement();
			file.writeln(4, "p.setParameter(\"" + key + "\", \"" + (String)param.getParameter(key) + "\");");
		}
		file.writeln(4, "p.setParameters(tmParameters);");
		file.writeln(4, "tmCurrentCompositeProcess = new TMain(p);");
		for ( Call call: calls ) {
		    call.generateJavaCode(file);
		}
		// JE: warning, reuse of parameters
		file.writeln(4, "tmCurrentCompositeProcess.bindParameters(p);");
		file.writeln(4, "tmCurrentCompositeProcess.execProcess(thread_mode);");
	}//end proc

	/**
	 *  Generates java code for end
	 *
	 *@param  file            The writer used for printing in a file
	 *@exception  IOException When IO errors occur
	 */
	public void generateEnd(Writer file) throws IOException {
		file.writeln(4, "if (debug_mode>0) {");
		file.writeln(6, "end = System.currentTimeMillis();");
		file.writeln(6, "System.err.println(\">> End of execution (\"+(end-begin)+\")\");}");
		file.writeln(2, "} //end main");
		file.writeln("} //end class");
	}

}

