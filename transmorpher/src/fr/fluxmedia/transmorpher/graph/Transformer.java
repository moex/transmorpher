/*
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003, 2022.
 *
 * https://gitlab.inria.fr/moex.transmorpher - https://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.transmorpher.graph;
import fr.fluxmedia.transmorpher.engine.TMain;
import fr.fluxmedia.transmorpher.engine.TProcessComposite;

import fr.fluxmedia.transmorpher.utils.LinearIndexedStruct;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.Writer;
import java.io.IOException;
import java.util.Enumeration;

import java.util.Hashtable;
import java.util.ListIterator;

/**
 * Transmorpher graph (JAXP) transformer definitions
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */
public class Transformer extends MainProcess {

	/**
	 *Constructor for the Transformer object
	 *
	 *@param  name  Description of the Parameter
	 *@param  owner   The transmorpher which owns this instance
	 */
	public Transformer(String name, Transmorpher owner) {
		super(name, owner, 1, 1);
	}

	/**
	 *Constructor for the Transformer object
	 *
	 *@param  owner   The transmorpher which owns this instance
	 */
	public Transformer(Transmorpher owner) {
		this("transformer", owner);
	}

	/**
	 *  Description of the Method
	 *
	 *@return    Description of the Return Value
	 */
	private String XMLName() {
		return "transformer";
	}

	/**
	 *  Creates an execution component corresponding to this Servlet and starts the creation
	 *  of execution components of each calls of this Servlet
	 *
	 *@return    an execution component
	 */
	public TProcessComposite createMainProcess() {
		try {
			String[] vPortIn = inPorts().toStringList();
			String[] vPortOut = outPorts().toStringList();
			TProcessComposite vCurrent = new TMain(getParameters());
			for ( Call call: calls ) {
			    call.createProcess(vCurrent);
			}
			return vCurrent;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	  *  Generates java code for import
	 *
	 *@param  file            The writer used for printing in a file
	 *@param  externs         Description of the Parameter
	 *@exception  IOException When IO errors occur
	 */
	public void generateImport(Writer file, LinearIndexedStruct<String> externs) throws IOException {
		file.writeln("import java.util.Properties;");
		file.writeln("//import JAXP classes");
		file.writeln("import javax.xml.transform.*;");
		file.writeln("import javax.xml.transform.stream.*;");
		file.writeln();
	}

	/**
	 *  Generates java code for class header
	 *
	 *@param  file            The writer used for printing in a file
	 *@exception  IOException When IO errors occur
	 */
	public void generateClassHeader(Writer file) throws IOException {
		file.writeln("public class " + getName() + " extends javax.xml.transform.Transformer {");
	}


	/**
	 *  Generates java code for main header
	 *
	 *@param  file            The writer used for printing in a file
	 *@exception  IOException When IO errors occur
	 */
	public void generateMainHeader(Writer file) throws IOException {
		// JE: Here, this should not be main but:
		file.writeln(2, "public void transform(Source in, Result out){");
		//iFile.writeln(2,"public static void main(String[] args){");
		file.writeln(4, "tmProcessStack = new Stack<TProcessComposite>();");
		//iFile.writeln(4,"vSR = new SystemResources();");
		file.writeln();
	}

	/**
	 *  Generates java code for parameters
	 *
	 *@param  file            The writer used for printing in a file
	 *@exception  IOException When IO errors occur
	 */
	public void generateReadParameters(Writer file) throws IOException {
		// JE: Here are only considered the parameters that have been declared
		for (Enumeration e = getParameters().getNames(); e.hasMoreElements(); ) {
			Object key = e.nextElement();
			// JE: Here I must find the code corresponding to the transformer
			file.writeln(4, "if ( req.getParameter(\"" + key + "\") != null ){");
			file.writeln(6, "tmParameters.setParameter(\"" + key + "\",req.getParameter(\"" + key + "\"));");
			file.writeln(4, "};");
		}
		file.writeln();
		// iFile.writeln(4,"res.setContentType(\"text/plain\");");
		// iFile.writeln(4,"PrintWriter out = res.getWriter();");
		file.writeln();
	}

	/**
	 *  Generates java code for body
	 *
	 *@param  file            The writer used for printing in a file
	 *@param  externs          Description of the Parameter
	 *@exception  IOException When IO errors occur
	 */
	public final void generateBody(Writer file, LinearIndexedStruct<String> externs) throws IOException {
		file.writeln(4, "try{");

		Parameters param = getParameters();
		file.writeln(4, "Parameters p = new Parameters();");
		for (Enumeration e = param.getNames(); e.hasMoreElements(); ) {
			String key = (String)e.nextElement();
			file.writeln(4, "p.setParameter(\"" + key + "\", \"" + (String)param.getParameter(key) + "\");");
		}
		file.writeln(4, "tmCurrentCompositeProcess = new TMain(p);");
		for ( Call call: calls ) {
		    call.generateJavaCode(file);
		}
		// JE: warning, reuse of parameters
		file.writeln(4, "tmCurrentCompositeProcess.bindParameters(p);");
		file.writeln(6, "tmCurrentCompositeProcess.execProcess(false);");
	}//end proc

	/**
	  *  Generates java code for end
	 *
	 *@param  file            The writer used for printing in a file
	 *@exception  IOException When IO errors occur
	 */
	public void generateEnd(Writer file) throws IOException {
		file.writeln(4, "if (debug_mode>0) {");
		file.writeln(6, "end = System.currentTimeMillis();");
		file.writeln(6, "System.err.println(\">> End of execution (\"+(end-begin)+\")\");}");
		file.writeln(4, "}catch(Exception e){}");

		file.writeln(2, "} //end main");
		generateTransformerMethod(file);
		file.writeln("} //end class");

	}

	/**
	 *  Generates java code of methods required by Transformer Interface
	 *
	 *@param  file            Description of the Parameter
	 *@exception  IOException  Description of the Exception
	 */
	public void generateTransformerMethod(Writer file) throws IOException {
		file.writeln(4, "public void clearParameters(){");
		file.writeln(4, "}");

		file.writeln(4, "public ErrorListener getErrorListener(){");
		file.writeln(6, "return null;");
		file.writeln(4, "}");

		file.writeln(4, "public Properties getOutputProperties(){");
		file.writeln(6, "return null;");
		file.writeln(4, "}");

		file.writeln(4, "public String getOutputProperty(String name){");
		file.writeln(6, "return null;");
		file.writeln(4, "}");

		file.writeln(4, "public Object getParameter(String name){");
		file.writeln(6, "return null;");
		file.writeln(4, "}");

		file.writeln(4, "public URIResolver getURIResolver(){");
		file.writeln(6, "return null;");
		file.writeln(4, "}");

		file.writeln(4, "public void setErrorListener(ErrorListener listener){");
		file.writeln(4, "}");

		file.writeln(4, "public void setOutputProperties(Properties ofromat){");
		file.writeln(4, "}");

		file.writeln(4, "public void setOutputProperty(String name,String value){");
		file.writeln(4, "}");

		file.writeln(4, "public void setParameter(String name, Object value){");
		file.writeln(4, "}");

		file.writeln(4, "public void setURIResolver(URIResolver resolver){");
		file.writeln(4, "}");

	}

}

