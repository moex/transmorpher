/**
 * Transmorpher
 *
 * Copyright (C) 2001 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


package fr.fluxmedia.transmorpher.graph;

import fr.fluxmedia.transmorpher.utils.Writer;

import java.util.Vector;
import java.util.Iterator;

/**
 * Transmorpher graph list of ports/channels
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */
public class PortList implements Iterable<Port> {

	/**
	 *  Description of the Field
	 */
	protected static int DEFAULT_SIZE = 1;
	/**
	 *  Description of the Field
	 */
	protected Vector<Port> ports = null;

	/**
	 *Constructor for the PortList object
	 */
	PortList() {
		ports = new Vector<Port>();
	}

	/**
	 *Constructor for the PortList object
	 *
	 *@param  i  Description of the Parameter
	 */
	PortList(int i) {
		ports = new Vector<Port>(i);
	}

	/**
	 *  Returns the length of this PortList
	 *
	 *@return    the length of this Portlist
	 */
	public int length() {
		return ports.size();
	}

	/**
	 *  Sets the port attribute of the PortList object
	 *
	 *@param  i  The new port value
	 *@param  p  The new port value
	 */
	public void setPort(int i, Port p) {
		ports.add(i, p);
	}

	/**
	 *  Gets the port attribute of the PortList object
	 *
	 *@param  i  the index in this PortList
	 *@return    The port value
	 */
	public Port getPort(int i) {
		return ports.get(i);
	}

	/**
	 *  Adds a feature to the Port attribute of the PortList object
	 *
	 *@param  p  The feature to be added to the Port attribute
	 */
	public void addPort(Port p) {
		ports.add(p);
	}

	/**
	 *  Description of the Method
	 *
	 *@param  p  Description of the Parameter
	 */
	public void removePort(Port p) {
		for (int i = 0; i != ports.size(); i++) {
			if (ports.get(i) == p) {
				ports.removeElementAt(i);
				break;
				//i = ports.size() ;
			}
		}
	}

	/**
	 * NOT YET IMPLEMENTED *
	 *
	 *@return    Description of the Return Value
	 */
	public boolean checkPorts() {
		return true;
	}

	/**
	 *  Gets the names attribute of the PortList object
	 *
	 *@return    The names value
	 */
	public String[] getNames() {
		String[] vAns = new String[length()];
		for ( int i = 0; i < length(); i++ ) {
		    vAns[i] = ports.get(i).getName();
		}
		return vAns;
	}

    public Iterator<Port> iterator() {
	return ports.iterator();
    }

	/**
	 *  Description of the Method
	 *
	 *@return    Description of the Return Value
	 */
	public final String[] toStringList() {
		int size = length();
		String[] vPort = new String[size];
		for (int i = 0; i < size; i++) {
			vPort[i] = getPort(i).getName();
		}
		return vPort;
	}

	/**
	 * Prints the XML expression of the call
	 *NOTE that getChannel().getName() would be cleaner
	 */
	public void generateXML() {
		if (ports.size() != 0) {
			System.out.print(ports.get(0).getName());
		}
		for ( Port p: ports ) {
		    System.out.print(" " + p.getName() );
		}
	}

	/**
	 * Prints the Java code for the execution of the call
	 *
	 *@param  file  Description of the Parameter
	 */
	public void generateJavaCode(Writer file) { }
}

