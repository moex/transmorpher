/*
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003, 2022.
 *
 * https://gitlab.inria.fr/moex.transmorpher - https://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/* Parsing principles:
 * STARTELEMENT
 * - Tests are Tests
 * - Ins are Feedbacks
 * - currentTransformation is stored (it is in the structure)
 * ENDELEMENT
 * - currentTransformation is poped (can be fetch in the structure)
 */
package fr.fluxmedia.transmorpher.graph;
import fr.fluxmedia.transmorpher.engine.*;

import fr.fluxmedia.transmorpher.utils.LinearIndexedStruct;
import fr.fluxmedia.transmorpher.utils.Writer;
import java.io.IOException;
import java.util.Enumeration;

import java.util.Hashtable;
import java.util.ListIterator;

/**
 * Transmorpher graph repeat node
 *
 *@author    Jerome.Euzenat@inrialpes.fr
 *@since     jdk 1.3 / SAX 2.0
 */

public class Repeat extends CallImpl {

	/**
	 * The process body of the repeat is made of a process
	 */
	protected RepeatProcess process = null;
	/**
	 * The iterators changing the parameters
	 */
	LinearIndexedStruct<Iterator> iterators = null;

	/**
	 * The constructor. Creates a new Repeat Call.
	 *
	 *@param  name    The name of the instance to create
	 *@param  owner   The Process that owns this instance
	 *@param  nbIn   	The number of in ports
	 *@param  nbOut  	The number of out ports
	 *@param  nbBuf  	The number of buffered ports
	 */
	public Repeat(String name, Process owner, int nbIn, int nbOut, int nbBuf) {
		super(name, null, owner, nbIn, nbOut);
		// JE/ To be implemented
		iterators = new LinearIndexedStruct<Iterator>();
		process = new RepeatProcess(name, this, nbIn, nbOut, nbBuf);
	}

	/**
	 * Adds an iterator to this repeat.
	 *
	 *@param  iterator  - iterator to add
	 */
	public void addIterator(Iterator iterator) {
		iterators.add( iterator.getName(), iterator );
	}

	/**
	 * Returns the iterator corresponding to the name s.
	 *
	 *@param  s  -  name of an iterator to search for.
	 *@return    the iterator.
	 */
	public Iterator getIterator(String s) {
		return iterators.get(s);
	}

	/**
	 * Returns the repeat process linked with this call.
	 *
	 *@return    the repeat process.
	 */
	public RepeatProcess getProcessBody() {
		return process;
	}

	/**
	 * Returns the list of buffering ports.
	 *
	 *@return    the list.
	 */
	public PortList inBuf() {
		return process.bufPorts();
	}

	/**
	 *  Description of the Method
	 *
	 *@return    Description of the Return Value
	 */
	public PortList outBuf() {
		return process.outPorts();
	}

	/**
	 *  Description of the Method
	 */
	public void clearNull() {
		super.clearNull();
		process.clearNull();
	}

	/**
	 *  Description of the Method
	 */
	public void clearVisited() {
		super.clearVisited();
		process.clearVisited();
	}

	/**
	 *  Description of the Method
	 *
	 *@param  out  Description of the Parameter
	 *@param  in   Description of the Parameter
	 */
	public void retroNull(PortList out, PortList in) {
		// This should be exactly the same thing as Process
		process.retroNull(out, in);
	}

	/**
	 * Prints the XML expression of the call
	 */
	public void generateXML() {
		System.out.print("      <repeat id=\"" + id + "\" ");
		System.out.print("in=\"");
		inPorts.generateXML();
		
		System.out.print("\" out=\"");
		outPorts.generateXML();
		System.out.print("\" bin=\"");
		process.bufPorts().generateXML();
		//System.out.print("\" bout=\"");
		//process.outPorts().generateXML();
		System.out.println("\">");
		parameters.generateXMLCalls();
		// print the list of iterators
		for( Iterator it: iterators ) it.generateXML();
		// print the list of calls
		process.generateXML();
		System.out.println("      </repeat>");
	}//end proc

	/**
	 * Prints the Java code for the execution of the call.
	 *
	 *@param  file             - the file to write in
	 *@exception  IOException  Description of the Exception
	 */
	public void generateJavaCode(Writer file) throws IOException {
		String r = getAttributes().getStringParameter("ref");
		LinearIndexedStruct<Call> vCalls = process.getCalls();

		file.writeln();
		file.writeln(4, "//------------------ Generation of Repeat " + getId() + " -------------------");
		generatePorts(file);
		generateParameters(file);
		file.writeln(4, "tmCurrentProcess = new TLoop(portIn,portOut,portBuf,tmParameters,tmAttributes);");
		file.writeln(4, "tmCurrentProcess.setName(\"" + getId() + "\");");
		file.writeln();

		file.writeln(4, "tmCurrentCompositeProcess.addProcess( tmCurrentProcess );");
		file.writeln();

		file.writeln(4, "tmProcessStack.push( tmCurrentCompositeProcess);");
		file.writeln(4, "tmCurrentCompositeProcess = (TProcessComposite)tmCurrentProcess;");
		file.writeln();

		file.writeln(4, "//------------------ Generation of process body for " + getId() + " -------------------");
		file.writeln(4, "TIterator iterator = null;");
		file.writeln(4, "String name = null;");
		file.writeln(4, "String type = null;");

		for ( Iterator it : iterators ) it.generateJavaCode( file );
		for ( Call cl : vCalls ) cl.generateJavaCode( file );

		file.writeln(4, "//----------------------- End of repeat body for " + getId() + " ---------------------");
		file.writeln(4, "tmCurrentCompositeProcess = tmProcessStack.pop();");

	}

	/**
	 *  Description of the Method
	 *
	 *@param  file             Description of the Parameter
	 *@exception  IOException  Description of the Exception
	 */
	public void generatePorts(Writer file) throws IOException {
		super.generatePorts(file);
		file.writeln(4, "String[] portBuf = new String[" + (process.bufPorts()).getNames().length + "];");
		for (int i = 0; i < (process.bufPorts()).length(); i++) {
			file.writeln(4, "portBuf[" + i + "]=\"" + (process.bufPorts()).getNames()[i] + "\";");
		}
	}

	/**
	 * Creates the execution structure of a repeat and adds it to the current composite process.
	 *
	 *@param  currentProcess   - execution structure is added to this process.
	 */
	public final void createProcess(TProcessComposite currentProcess) {
		try {
			String[] vPortIn = inPorts().toStringList();
			String[] vPortOut = outPorts().toStringList();
			String[] vPortBuf = (process.bufPorts()).toStringList();

			LinearIndexedStruct<Call> vCalls = process.getCalls();
			TProcess vCurrent = new TLoop(vPortIn, vPortOut, vPortBuf,
			/*getAttributes().getStringParameter("type"), */
				getParameters(),
				getAttributes());
			//inner processes creations
			for ( Call cl : vCalls ) cl.createProcess((TProcessComposite)vCurrent);
			for ( Iterator it : iterators ) it.createIterator((TProcessComposite)vCurrent);

			currentProcess.addProcess(vCurrent);
			update(vCurrent);
		} catch (Exception e) {
			System.out.println("[Repeat] Error : cannot create Process");
			System.out.println("[Repeat] " + e);
		}
	}
}

