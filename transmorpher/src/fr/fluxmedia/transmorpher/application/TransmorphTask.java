/**
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003, 2022.
 *
 * https://gitlab.inria.fr/moex.transmorpher - https://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.application ;

import fr.fluxmedia.transmorpher.parser.ProcessParser ;
import fr.fluxmedia.transmorpher.parser.FMParser ;
import fr.fluxmedia.transmorpher.graph.Transmorpher ;
import fr.fluxmedia.transmorpher.utils.TMException;
import fr.fluxmedia.transmorpher.utils.SystemResources ;
import fr.fluxmedia.transmorpher.utils.Parameters;

import java.util.Enumeration;
import java.util.Vector;

import org.apache.tools.ant.Task;
import org.apache.tools.ant.BuildException;
//import org.apache.tools.ant.taskdefs.Param;

/**
 * A Ant task for Transmorpher
 * @author J�r�me Euzenat (Jerome.Euzenat@inria.fr)
 * @since SAX 2.0 / jdk 1.2
 * This calls for a complete reworking of all the Application directory
 */
public class TransmorphTask extends Task {

    // JE: Instead of duplicating everything, there should be a case
    // for a common TransmorphBody

    private static int debug_mode = 0;
    private String msg ="";
    private String reloc = "";
    private String mode = "run"; // compile/optimize/run/test
    private String file ="";
    /** The parameter structure required by Ant */
    private Vector<Param> paramVect;
    /** The parameter structure required by Transmorpher */
    private Parameters parameters;

    /** Helps giving Ant the parameter cell in which it will store the parameter */
        public Param createParam() {
        Param p = new Param();
        paramVect.addElement(p);
        return p;
    }
    /*
    public void addParam( Param p ) {
        paramVect.addElement(p);
    }
    */
    public void setDebug( int i ){
	debug_mode = i;
    }

    public void setReloc( String path ){
	reloc = path;
    }

    public void setSrcfile( String path ){
	file = path;
    }

    public void setMode( String m ){
        mode = m;
    }

    // The setter for the "message" attribute
    public void setMessage(String m) {
        msg = m;
    }

    /** The constructor */
    public TransmorphTask() {
       parameters = new Parameters();
       paramVect = new Vector<Param>();
    }

    // The method executing the task
    public void execute() throws BuildException {
        System.out.println(msg);
	System.setErr(System.out);
	boolean time = true ;     
	double begin = System.currentTimeMillis();
	double end = 0;
	// JE: As long as TReader uses this SystemResources.... this is useful
	SystemResources vSR = new SystemResources();
	// This is just for being able to use the methods like printVersion
	CommandLineArgument arg = new CommandLineArgument();
   
	if ( debug_mode > 0 ) arg.printVersion();

	if (debug_mode > 1){
	    System.err.println("mode = "+mode);
	    System.err.println("srcfile = "+file);
	    System.err.println("debug = "+debug_mode);
	    System.err.println("message = "+msg);
	    System.err.println("reloc = "+reloc);
	    // This should print all the arguments
	    for( Param p : paramVect ) {
		System.err.println(p.getName()+" = "+p.getSelect());
            }
	}
 
	for( Param p : paramVect ) {
	    parameters.setParameter(p.getName(), (Object)p.getSelect());
	}

	if ( file != null ){
	    try {
		FMParser iProcessParser = new ProcessParser(debug_mode);
		
		if (debug_mode > 0) System.err.println(">> Begin graph construction");
		Transmorpher graph = iProcessParser.newParse(file);
		if (debug_mode > 0) {
		    end = System.currentTimeMillis() ;
		    System.err.println(">> End of graph construction ("+(end-begin)+")");}
		if ( mode.equals("run")) {
		    if (debug_mode > 0) {
			System.err.println(">> Begining of Execution generation");
			begin = System.currentTimeMillis();}
		    graph.generateExec(reloc);
		    if (debug_mode > 0) {
			end = System.currentTimeMillis() ;
			System.err.println(">> End of Execution generation ("+(end-begin)+")");
			System.err.println(">> Begining of Execution");
			begin = System.currentTimeMillis();}
		    graph.exec(parameters);
		    if (debug_mode > 0) {
			end = System.currentTimeMillis() ;
			System.err.println(">> End of execution ("+(end-begin)+")");}
		} else if ( mode.equals("optimize") ) {
		    graph.generateXML();
		} else if ( mode.equals("test") ) {
		    //fr.fluxmedia.transmorpher.utils.CharEngine CE= new fr.fluxmedia.transmorpher.utils.CharEngine("../samples/biblio/input/bibexmo.xml");
		    //CE.ReadXML();
		} else { // compile mode
		    if (debug_mode > 0) {
			System.err.println(">> Begin of code generation");
			begin = System.currentTimeMillis();}
		    graph.generateJavaCode(reloc);
		    if (debug_mode > 0) {
			end = System.currentTimeMillis() ;
			System.err.println(">> End of code generation ("+(end-begin)+")");}
		}
	    } catch (TMException e) {
		e.printStackTrace();
	    } catch (Exception e) {
		System.err.println("transmorph : Caught error ["+ e +"]");
		e.printStackTrace();
	    } //end try
	} else {
	    throw new BuildException("Compulsory attribute for TransmophTask: file");
	} // end If
    } //end execute
    

/** The corresponding class in Ant is not usable (it is propoer to the XSLTProcess task). So we have to replicate it */

    public static class Param {
        private String name = null;
        private String value = "";

        public void setName(String n){
            name = n;
        }

        public void setSelect(String e){
            value = e;
        }

	public void addText( String s ){
            value = s;
        }

        public String getName() throws BuildException{
            if(name == null)
		throw new BuildException("Name attribute is missing.");
            return name;
        }

        public String getSelect() {
            return value;
        }
    }

} //end class
