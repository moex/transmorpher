/*
 *  $Id: CommandLineArgument.java,v 1.1 2002-11-06 14:08:21 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.application;

import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.Version;
import fr.fluxmedia.transmorpher.utils.TMException;

/**
 *  This is an utility class for the command line analysis
 *
 *@author     Laurent Tardif (laurent.tardif@inrialpes.fr)
 *@since      jdk 1.2
 */

public final class CommandLineArgument {
    /**
     *  The filename parameter
     */
    String iFilename = null;

    /**
     *  The relocation directory
     */
    String reloc = null;

    /**
     *  True if the command line is syntaxicly correct
     */
    boolean isOk = true;
    
    /**
     *  True if the debug mode is activate
     */
    int debug_mode = 0;
    boolean roundMode = false;
    boolean execMode = true;
    boolean compileMode = false;
    boolean testMode = false;
    boolean threadMode = false;

    /**
     *  The parameters to be passed to the main process
     */
    Parameters parameters;

    /**
     *  The commandLine constructor
     */
    public CommandLineArgument() {
	parameters = new Parameters();
    }

    /**
     *  Set the filename
     *
     *@param  pFilename  The new filename value
     */
    public final void setFilename(String pFilename) {
	iFilename = pFilename;
    }

    /**
     *  Get the filename
     *
     *@return    The filename value
     */
    public final String getFilename() {
	return iFilename;
    }
    
    /**
     *  Set the debug mode
     *
     *@param  mode  The new debugMode value
     */
    public final void setDebugMode(int mode) {
	debug_mode = mode;
    }
    
    /**
     *  Get the filename
     *
     *@return    The roundMode value
     */
    public final boolean getRoundMode() {
	return roundMode;
    }

    /**
     *  Gets the debugMode attribute of the CommandLineArgument object
     *
     *@return    The debugMode value
     */
    public final int getDebugMode() {
	return debug_mode;
    }
    
    /**
     *  Gets the compileMode attribute of the CommandLineArgument object
     *
     *@return    The compileMode value
     */
    public final boolean getCompileMode() {
	return compileMode;
    }
	
    /**
     *  Gets the threadMode attribute of the CommandLineArgument object
     *
     *@return    The threadMode value
     */
    public final boolean getThreadMode() {
	return threadMode;
    }
    /**
     *  Gets the execMode attribute of the CommandLineArgument object
     *
     *@return    The execMode value
     */
    public final boolean getExecMode() {
	return execMode;
    }
    
    /**
     *  Gets the testMode attribute of the CommandLineArgument object
     *
     *@return    The testMode value
     */
    public final boolean getTestMode() {
	return testMode;
    }
    
    /**
     *  Gets the relocation attribute of the CommandLineArgument object
     *
     *@return    The relocation value
     */
    public final String getRelocation() {
	return reloc;
    }

    /**
     *  Gets the parameters attribute of the CommandLineArgument object
     *
     *@return    The parameters value
     */
    public final Parameters getParameters() {
	return parameters;
    }
    
    /**
     *  return true if the command line is syntaxicly correct
     *
     *@return    true if is OK
     */
    public final boolean IsOk() {
	return isOk;
    }

    /**
     *  Set the isOk variable to false
     */
    public final void setBadCommandLineArgument() {
	isOk = false;
    }
    
    /**
     *  Prints help on the standard output.
     */
    public final static void printHelp() {
	printVersion();
	System.out.println("Usage : java [Javaoption] fr.fluxmedia.transmorpher.application.transmorph args");
	System.out.println("");
	System.out.println("Where args include :");
	System.out.println("  -v            print the version number");
	System.out.println("  -h            print this help");
	System.out.println("  -d <int>      debug level (by default 0=quiet)");
	System.out.println("                1=time; 2=variables; 3=detailled");
	System.out.println("  -reloc <dir>  relocate all generated files goes to the directory <dir>");
	System.out.println("  -D<var>=<val> will run the transformation with the variable <var> set to <val>");
	System.out.println("  -compile      compile the process given in argument");
	System.out.println("  -optimize     generate an optimized process on standard output");
	System.out.println("  -run          run the transformation (default)");
	System.out.println("  -t            run the transformation with threads");
	System.out.println("  -test         internal use");
	System.out.println("  <file>        the file which describe the process to process");
    }

    /**
     *  Prints version informations.
     */
    public final static void printVersion() {
	System.err.println(Version.NAME + " " + Version.RELEASE + " - " + Version.DATE + " - " + Version.RIGHTS);
    }

    /**
     *  Analyse the command line arguments
     *
     *@param  pArgs  Description of the Parameter
     */

    public final void argsAnalyse(String[] pArgs) throws TMException {
	int vI = 0;
	CommandLineArgument arg = this;
	boolean no_other_option = false;

	while (vI < pArgs.length) {
	    char signe = (pArgs[vI]).charAt(0);
	    if ((pArgs[vI].equals("-?") || pArgs[vI].equals("-h"))
		&& !(no_other_option)) {
		arg.setBadCommandLineArgument();
		printHelp();
		vI = pArgs.length;// all other parameters after are forbidden.
	    } else if (pArgs[vI].startsWith("-D")) {
		int ind = pArgs[vI].indexOf('=');
		parameters.setParameter(pArgs[vI].substring(2, ind), pArgs[vI].substring(ind + 1));
	    } else if (pArgs[vI].equals("-v") && !(no_other_option)) {
		printVersion();
		arg.setBadCommandLineArgument();
		vI = pArgs.length;// all other parameters after is forbidden.
	    } else if (pArgs[vI].equals("-d")) {
		if (vI++ == pArgs.length) {
		    printHelp();
		    arg.setBadCommandLineArgument();
		    vI = pArgs.length;
		} else {
		    // Should trap the eventual NumberFormatException
		    arg.setDebugMode(Integer.parseInt(pArgs[vI]));
		    no_other_option = true;
		}
	    } else if (pArgs[vI].equals("-t")) {
		threadMode=true;
	    } else if (pArgs[vI].equals("-compile")) {
		execMode = false;
		roundMode = false;
		no_other_option = true;
	    } else if (pArgs[vI].equals("-optimize")) {
		execMode = false;
		roundMode = true;
	    } else if (pArgs[vI].equals("-run")) {
		execMode = true;
		roundMode = false;
	    } else if (pArgs[vI].equals("-test")) {
		execMode = false;
		testMode = true;
		roundMode = false;
		compileMode = false;
	    } else if (pArgs[vI].equals("-reloc")) {
		if (vI + 1 == pArgs.length) {
		    throw new TMException(pArgs[vI] + " : -reloc must be followed by a path");
		} else {
		    vI = vI + 1;
		    reloc = pArgs[vI];
		}
	    } else if (signe == '-') {
		throw new TMException(pArgs[vI] + " : bad argument");
	    } else {
		arg.setFilename(pArgs[vI]);
	    }
	    vI++;
	}//while
    }
}

