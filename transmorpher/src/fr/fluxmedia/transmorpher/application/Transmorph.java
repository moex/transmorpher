/**
 * $Id: Transmorph.java,v 1.3 2004-02-26 11:49:34 jerome Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2004 INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.application;

import fr.fluxmedia.transmorpher.graph.Transmorpher;
import fr.fluxmedia.transmorpher.parser.FMParser;
import fr.fluxmedia.transmorpher.parser.ProcessParser;
import fr.fluxmedia.transmorpher.utils.TMException;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.SystemResources;

import java.util.Enumeration;

/**
 *  Parse and print and XML specification for Transmorpher.
 *
 *@author     Laurent Tardif (laurent.tardif@inrialpes.fr)
 *@author     J�r�me Euzenat (Jerome.Euzenat@inrialpes.fr)
 *@since      jdk 1.2
 */
public final class Transmorph {

    /**
     *  debug level
     */
    static int debug_mode = 0;
    
    /**
     *  Process a call to a function. This is the main function of the application.
     *
     *@param  argc  -the name of the XML file which describe the execution process
     */
    public final static void main(String argc[]) {
	//System.setErr(System.out);
	boolean time = true;
	double begin = System.currentTimeMillis();
	double end = 0;
	SystemResources vSR = new SystemResources();

	if (debug_mode > 0) {
	    System.err.println(">> Getting parameters");
	}
	CommandLineArgument arg = new CommandLineArgument();
	try { arg.argsAnalyse(argc);
	} catch (TMException e) {
	    e.printStackTrace();
	    return;
	}
	debug_mode = arg.getDebugMode();
	if (debug_mode > 0 && arg.IsOk()) {
	    arg.printVersion();
	}

	if (debug_mode > 1) {
	    for (Enumeration e = arg.getParameters().getNames(); e.hasMoreElements(); ) {
		String key = (String) e.nextElement();
		String val = (String) arg.getParameters().getParameter(key);
		System.err.println(" +++ " + key + " = " + val);
	    }//end for
	}
	
	if (arg.IsOk()) {// no continuation: error detected in command line.
	    String filename = arg.getFilename();
	    if ( filename != null ) {
		try {
		    FMParser iProcessParser = new ProcessParser(debug_mode);
		    
		    if (debug_mode > 0) {
			System.err.println(">> Begin graph construction");
		    }
		    // if 
		    Transmorpher graph = iProcessParser.newParse(filename);
		    graph.useThread(arg.getThreadMode());
		    if (debug_mode > 0) {
			end = System.currentTimeMillis();
			System.err.println(">> End of graph construction (" + (end - begin) + ")");
		    }
		    if (arg.getExecMode()) {
			if (debug_mode > 0) {
			    System.err.println(">> Begining of Execution generation");
			    begin = System.currentTimeMillis();
			}
			graph.generateExec(arg.getRelocation());
			if (debug_mode > 0) {
			    end = System.currentTimeMillis();
			    System.err.println(">> End of Execution generation (" + (end - begin) + ")");
			    System.err.println(">> Begining of Execution");
			    begin = System.currentTimeMillis();
			}
			graph.exec(arg.getParameters());
			if(arg.getThreadMode()){
				ThreadGroup group=Thread.currentThread().getThreadGroup();
				int nbThread=group.activeCount();
				Thread[] array=new Thread[nbThread];
				group.enumerate(array);
				for (int i=0;i<nbThread;i++){
					//System.out.println(array[i]);
					if (array[i]!=null)
						if (!array[i].getName().equals("main"))
							array[i].join();
				}
			}
			if (debug_mode > 0) {
			    end = System.currentTimeMillis();
			    System.err.println(">> End of execution (" + (end - begin) + ")");
			}
		    } else if (arg.getRoundMode()) {
			graph.generateXML();
		    } else if (arg.getTestMode()) {
			//fr.fluxmedia.transmorpher.utils.CharEngine CE= new fr.fluxmedia.transmorpher.utils.CharEngine("../samples/biblio/input/bibexmo.xml");
			//CE.ReadXML();
		    } else {// compile mode
			if (debug_mode > 0) {
			    System.err.println(">> Begin of code generation");
			    begin = System.currentTimeMillis();
			}
			graph.generateJavaCode(arg.getRelocation());
			if (debug_mode > 0) {
			    end = System.currentTimeMillis();
			    System.err.println(">> End of code generation (" + (end - begin) + ")");
			}
		    }
		} catch (TMException e) {
		    e.printStackTrace();
		} catch (Exception e) {
		    System.err.println("Transmorph : Caught error [" + e + "]");
		    e.printStackTrace();
		}//end try	
	    } else {
		System.err.println("Transmorph : missing transformation filename");
		arg.printHelp();
	    }// end If
	    
	}// end If
    }//end main
}

