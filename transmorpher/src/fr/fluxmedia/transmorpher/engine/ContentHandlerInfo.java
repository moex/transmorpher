/**
 * $Id: ContentHandlerInfo.java,v 1.1 2002-11-06 14:08:21 serge Exp $
 *
 * Transmorpher
 * 
 * Copyright (C) 2001 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
/**
  *
  * This clas provide a way to store content handler 
  * command in a stack
  *
  * @authors  laurent.Tardif@inrialpes.fr
  * @date 10 02 2001
  * @since jdk 1.3 && SAX 2.0
  
  ** WARNING MUST COPY THE CHAR * durring the stack, beacause some parser overide the variable
  */

package fr.fluxmedia.transmorpher.engine ;


// Imported SAX CLasses 

import org.xml.sax.ContentHandler;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl ;
import org.xml.sax.SAXException;

import org.xml.sax.Locator ;

public final class ContentHandlerInfo {

  /** String array to store String parameter */
  String iStringParameters[] = new String[4];
  
  /** int array to store int parameter */
  int iIntParameters[] = new int[2];
  
  /** Attributes to store attributes parameter */
  Attributes iAttributesParameter = null;

  /** Locator to store locator parameter */
  Locator iLocatorParameter = null;
  
  /** The CharArray to store char array paramer */
  char[] iCharParameter = null;

  /** The content handler command */
  int iCommand = START_DOCUMENT;

  /** One constructor */ 
  public ContentHandlerInfo(int pCommand) {
    iCommand = pCommand;
  }

  /** One constructor */
  public ContentHandlerInfo(int pCommand,Locator p1) {
    iLocatorParameter = p1;
    iCommand = pCommand;
  }

  /** One constructor */
  public ContentHandlerInfo(int pCommand,String p1) {
    iStringParameters[0] = p1;
    iCommand = pCommand;
  }

  /** One constructor */
  public ContentHandlerInfo(int pCommand, char[] p1, int p2, int p3) {
	// System.out.println("NEW CONTENTHANDLER INFO "+p1);
	iCommand = pCommand;
	iCharParameter  = null;
	iCharParameter = new char[p2 + p3];
	for (int i = p2; i < p2 + p3; i++) {
		iCharParameter[i] = p1[i];
	}
	//iCharParameter = p1;
	iIntParameters[0] = p2;
	iIntParameters[1] = p3;
  }
  
  /** One constructor */  
  public ContentHandlerInfo(int pCommand,String p1, String p2) {
    this(pCommand,p1);    
    iStringParameters[1] = p2;
  }
  
  /** One constructor */
  public ContentHandlerInfo(int pCommand,String p1, String p2, String p3) {
    this(pCommand,p1,p2);
    iStringParameters[2] = p3;        
  }

  /** One constructor */
  public ContentHandlerInfo(int pCommand,String p1, String p2, String p3, Attributes pAtt) {
    this(pCommand,p1,p2,p3);
    iAttributesParameter = new AttributesImpl(pAtt);
    //iAttributesParameter = pAtt;    
  }  
  
  /** Get the string parameted */
  public final String getStringParameter(int i) {
    return iStringParameters[i];
  }
  
  /** get the Inr parameter */
  public final int getIntParameter(int i) {
    return iIntParameters[i];
  }
  
  /** get the locator */
  public final Locator getLocatorParameter() {
    return iLocatorParameter;
  }
  
  /** get the attributes */
  public final Attributes getAttributes() {
    return iAttributesParameter ;
  }
  
  /** get the command */
  public final int getCommand() {
    return iCommand;
  }

  /** get the car array parameter */
  public final char[] getCharParameter() {
    return iCharParameter;
  }
  
    /** The various labels used for the events */

    static final int START_ELEMENT          = 0 ;
    static final int END_ELEMENT            = 1 ;
    static final int START_DOCUMENT         = 2 ;
    static final int END_DOCUMENT           = 3 ;
    static final int SET_DOCUMENT_LOCATOR   = 4 ;
    static final int SKIPPED_ENTITY         = 5 ;
    static final int IGNORABLE_WHITESPACE   = 6 ;
    static final int PROCESSING_INSTRUCTION = 7 ;
    static final int CHARACTERS             = 8 ;
    static final int START_PREFIX_MAPPING   = 9 ;
    static final int END_PREFIX_MAPPING     = 10 ;

    /** For converting the constants into string */
    static final String [] ContentHandlerInfo2String = {"START_ELEMENT", "END_ELEMENT", "START_DOCUMENT",
							"END_DOCUMENT", "SET_DOCUMENT_LOCATOR","SKIPPED_ENTITY",
							"IGNORABLE_WHITESPACE", "PROCESSING_INSTRUCTION", "CHARACTERS",
							"START_PREFIX_MAPPING", "END_PREFIX_MAPPING" } ;
  
    /** convert the command to string */
   public final String toString() {
	return ContentHandlerInfo2String[iCommand];
    } //end toString

    public void sendEvent(ContentHandler outputHandler)throws SAXException{
	    switch(this.getCommand() ) {
	    case ContentHandlerInfo.START_ELEMENT :
		outputHandler.startElement(this.getStringParameter(0),
				 this.getStringParameter(1),
				 this.getStringParameter(2),
				 this.getAttributes());
		break;
	    case ContentHandlerInfo.END_ELEMENT :
		outputHandler.endElement(this.getStringParameter(0),
			       this.getStringParameter(1),
			       this.getStringParameter(2));
		break ;
	    case ContentHandlerInfo.START_DOCUMENT :
		 //System.out.println("OUTPUT HANDLER "+outputHandler);
		outputHandler.startDocument();
		break ;
	    case ContentHandlerInfo.END_DOCUMENT :
		outputHandler.endDocument();
		//isDone = true;
		break ;
	    case ContentHandlerInfo.SET_DOCUMENT_LOCATOR :
		outputHandler.setDocumentLocator(this.getLocatorParameter());
		break ;
	    case ContentHandlerInfo.SKIPPED_ENTITY :
		outputHandler.skippedEntity(this.getStringParameter(0));
		break ;
	    case ContentHandlerInfo.IGNORABLE_WHITESPACE :
		outputHandler.ignorableWhitespace(this.getCharParameter(),
					this.getIntParameter(0),
					this.getIntParameter(1));
		break ;
	    case ContentHandlerInfo.PROCESSING_INSTRUCTION :
		outputHandler.processingInstruction(this.getStringParameter(0),
					  this.getStringParameter(1));
		break ;
	    case ContentHandlerInfo.CHARACTERS :
		    String toto = "";
		    /* for (int i=this.getIntParameter(0); i< this.getIntParameter(1)+ this.getIntParameter(0); i++) 
			toto+=this.getCharParameter()[i];
		System.out.println("ContentHandlerInfo . characters ; " + toto ); */
		outputHandler.characters(this.getCharParameter(),
			       this.getIntParameter(0),
			       this.getIntParameter(1));
		break ;
	    case ContentHandlerInfo.START_PREFIX_MAPPING :
		outputHandler.startPrefixMapping(this.getStringParameter(0),
				       this.getStringParameter(1));
		break ;
	    case ContentHandlerInfo.END_PREFIX_MAPPING :
		outputHandler.endPrefixMapping(this.getStringParameter(0));
		break ;
	    default :
		System.out.println("[ContentHandlerInfo]  ExecuteContentHandlerInfo : commandes inconnues");
	    } //end switch
	}
} //end class
