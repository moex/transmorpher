/*
 * Transmorpher
 *
 * Copyright (C) 2001-2002, 2022 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * https://gitlab.inria.fr/moex/transmorpher/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.transmorpher.engine;
// Imported Transmorpher classes
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
import fr.fluxmedia.transmorpher.utils.SystemResources;
import fr.fluxmedia.transmorpher.utils.TMException;
import fr.fluxmedia.transmorpher.utils.TMRuntimeException;
// Imported Java classes
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.Exception;
import java.lang.Runnable;
import java.net.*;
import java.util.Hashtable;
// Imported SAX classes
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 * This class provide a basic process which is
 * able to read an XML file
 *
 *@author    Laurent.Tardif@inrialpes.fr
 *@since     jdk 1.3 && SAX 2.0
 */

public abstract class TReader extends TProcessBasic {

	//
	// Constants
	//

	/**
	 * Namespaces feature id (http://xml.org/sax/features/namespaces).
	 */
	protected final static String NAMESPACES_FEATURE_ID = "http://xml.org/sax/features/namespaces";

	/**
	 * Namespaces feature id (http://xml.org/sax/features/namespaces).
	 */
	protected final static String NAMESPACES_PREFIXES_FEATURE_ID = "http://xml.org/sax/features/namespace-prefixes";

	/**
	 * Validation feature id (http://xml.org/sax/features/validation).
	 */
	protected final static String VALIDATION_FEATURE_ID = "http://xml.org/sax/features/validation";

	/**
	 * Schema validation feature id (http://apache.org/xml/features/validation/schema).
	 */
	protected final static String SCHEMA_VALIDATION_FEATURE_ID = "http://apache.org/xml/features/validation/schema";

	// property ids

	/**
	 * Lexical handler property id (http://xml.org/sax/properties/lexical-handler).
	 */
	protected final static String LEXICAL_HANDLER_PROPERTY_ID = "http://xml.org/sax/properties/lexical-handler";

	// default settings

	/**
	 * Default namespaces support (true).
	 */
	protected final static boolean DEFAULT_NAMESPACES = true;

	/**
	 * Default namespaces support (true).
	 */
	protected final static boolean DEFAULT_NAMESPACES_PREFIXES = true;

	/**
	 * Default validation support (false).
	 */
	protected final static boolean DEFAULT_VALIDATION = false;

	/**
	 * Default Schema validation support (false).
	 */
	protected final static boolean DEFAULT_SCHEMA_VALIDATION = false;

	/**
	 * The XMLReader
	 */
	protected XMLReader iReader = null;

	/**
	 *  Description of the Field
	 */
	public boolean isRunning = false;

	/**
	 *  An exception raised during thread processing
	 */
	private Exception raisedException = null;
	/**
	 * Name of the file to read
	 */
	protected String iFilename = null;

	/**
	 *  Description of the Field
	 */
	protected InputSource standardInput = null;

	/**
	 *  Description of the Field
	 */
	protected InputSource inSource = null;


	/**
	 *Constructor for the TReader object
	 *
	 *@param  pOut               Description of the Parameter
	 *@param  pParam             Description of the Parameter
	 *@param  pStaticAttributes  Description of the Parameter
	 *@exception  SAXException   can wrap others exceptions
	 *@exception  IOException    if an IO operation failed
	 */
	public TReader(String[] pOut, Parameters pParam, StringParameters pStaticAttributes)
		 throws SAXException, IOException {
		super(new String[0], pOut, pParam);
		param = pParam;
	}


	/**
	 * return eventualy raised exception during thread execution
	 *
	 *@return    The raisedException value
	 */
	public Exception getRaisedException() {
		return raisedException;
	}

	/**
	 * Exec the process :parse a file or an URL content
	 *
	 *@exception  IOException         if an IO operation failed
	 *@exception  SAXException        can wrap others exceptions
	 *@exception  TMRuntimeException  any error that implementations want to raise
	 */
	public void execProcess() throws IOException, SAXException, TMRuntimeException {
		try {
			//System.out.println("[test evalParameter] "+getParameters().evalParameter("file"));
			//System.out.println(getParameter("file"));
			super.bindParameters(getParameters());
			iFilename = (String)getParameter("file");
			
			if ((iFilename != null)&&(!(iFilename.equals("_stdin_")))) {
				if ( iFilename.startsWith("http://") || iFilename.startsWith("https://") ) {
					URL url = new URL(iFilename);
					URLConnection urlConnect = url.openConnection();
					InputStream inStream = urlConnect.getInputStream();
					inSource = new InputSource(inStream);
					inSource.setSystemId(iFilename);
				} else {
					File file = new File(iFilename);
					inSource = new InputSource(new FileInputStream(file));
					inSource.setSystemId(file.toURI().toURL().toString());
				}
			} else {
				inSource = new InputSource(System.in);
			}
			if (isThread) {
				procThread.start();
				getParameters().setParameters(param);

				if (raisedException != null) {
					throw new TMRuntimeException(raisedException, "Thread reading error");
				}
			} else {
					read();
					getParameters().setParameters(param);
			}
			

		} catch (IOException e) {
			// JE: There is a problem here either we report the error,
			// either we send the empty document! And if we report it,
			// this must be by raising an exception
			System.err.println("File : " + iFilename + " can't be found.");
			// Send the empty document on error!?
			iReader.getContentHandler().startDocument();
			iReader.getContentHandler().endDocument();
			getParameters().setParameters(param);

		} 
		catch (TMException e) {
			getParameters().setParameters(param);
		}
		catch (SAXException e){
			e.printStackTrace();
			getParameters().setParameters(param);
		}
	}

	/**
	 *  Begins the parse of a file. This method is called inside a repeat . At each loop, file is changed.
	 *
	 *@param  file                    the name of the file to parse
	 *@exception  TMRuntimeException  Description of the Exception
	 */
	public void execProcess(String file) throws TMRuntimeException {
		iFilename = file;
		try {
			read();
		} catch (Exception e) {
			throw new TMRuntimeException(e, "Parse error in TReader thread for " + iFilename);
		}
	}

	/**
	 *  Generates port for this component
	 */
	public void generatePort() {
		iListOut[0] = new XML_Port(nameOut[0], this, (XMLReader)iReader, 0, XML_Port.OUT);
	}

	/**
	 *  Main processing method for the TReader object
	 */
	public final void run() {
		if (!isRunning) {
			try {
				read();
				isRunning = true;
			} catch (Exception e) {
				raisedException = e;
				// JE: Maybe here it is not bad to send empty files...
				isRunning = true;
			}//end try
		}//fin if
	}//end run

	/**
	 * This is the only method a developper has to implement in order
	 * to create a new reader.
	 * Exception raised can be encapsulated withing a TMRuntimeException
	 *
	 *@exception  IOException         if an IO operations failed
	 *@exception  SAXException        can wrap others exceptions
	 *@exception  TMRuntimeException  any error that implementations want to raise
	 */
	public abstract void read() throws IOException, SAXException, TMRuntimeException;

	/**
	 *  Gets the inputSource attribute of the TReader object
	 *
	 *@return    The inputSource value
	 */
	public InputSource getInputSource() {
		return inSource;
	}

	/**
	 *  Gets the inputStream attribute of the TReader object
	 *
	 *@return    The inputStream value
	 */
	public InputStream getInputStream() {
		return inSource.getByteStream();
	}
}

