/**
 * $Id: BufferingHandler.java,v 1.2 2002-11-25 16:09:11 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.engine;

// imported fr.fluxmedia.transmorpher classes
import fr.fluxmedia.transmorpher.utils.Fifo;
// Imported org.xml.sax classes
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/**
 * Concatenate homogeneous XML file.
 * BufferingHandler does buffer all SAX events.
 * once it is called with flushBuffer(ContentHandler) it will send
 * all buffered events to the given handler
 * MAIN CRITICISM: Why not using a dom tree!
 * Since the system works assynchronously from the input, it is delicate
 * to avoid deadlock (e.g., if a process has to output to a merge, it will
 * not start until there is two consumer for the SAX events).
 * To that extent, the class above make the first handler as Current and not
 * the others. A current handler flushes the outpout to the output channel,
 * a non current one put stacks it!
 * Once a handler becomes Current (see IsFinished())    it starts by flushing its stack
 * and then process normally.
 * Of course, this should not be a stack...
 *
 *@author     laurent.tardif@inrialpes.fr
 *@author     jerome.euzenat@inrialpes.fr
 *@created    10 06 2002
 *@see        "SAX 2.0 & JDK 1.3"
 */

import org.xml.sax.helpers.DefaultHandler;

class BufferingHandler extends DefaultHandler implements TmHandler {

	/** the stack of events info */
	Fifo iStack = null;

	/** The handler to which the output will be flushed */
	ContentHandler outputHandler = null;

	/** true if the content handler is the current one */
	boolean isCurrent = false;

	/** true if the handler is finished */
	boolean isDone = false;

	/** the constructor */
	public BufferingHandler() {
		iStack = new Fifo();
	}

	/**
	 * the constructor. Sets the output handler with h
	 *
	 *@param  h  the output handler
	 */
	public BufferingHandler(ContentHandler h) {
		this();
		outputHandler = h;
	}

	// The Buffer methods

	/**
	 * Add an element in the stack
	 *
	 *@param  p  The ContentHandlerInfo to add to the stack
	 */
	public final void add(ContentHandlerInfo p) {
		iStack.push(p);
	}//end add

	/**
	 * Remove the first element in the stack
	 *
	 *@return    The ContentHandlerInfo at the top of the stack
	 */
	public final ContentHandlerInfo pop() {
		return (ContentHandlerInfo) iStack.pop();
	}//end pop

	/**
	 * return the size of the stack
	 *
	 *@return    The size of the stack
	 */
	public final int size() {
		return iStack.size();
	}//end size

	/* The Handler methods */
	/**
	 * The startElement command
	 *
	 *@param  ns                The Namespace URI.
	 *@param  localName         The local name without prefix.
	 *@param  name              The local name with prefix.
	 *@param  atts              The attributes attached to the element.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void startElement(String ns, String localName, String name, Attributes atts) throws SAXException {
		if (isCurrent) {
			outputHandler.startElement(ns, localName, name, atts);
		} else {
			// System.out.println("STORE Buffering Handler startElement "+iStack);
			add(new ContentHandlerInfo(ContentHandlerInfo.START_ELEMENT, ns, localName, name, atts));
		}
	}

	/**
	 * The endElement command
	 *
	 *@param  ns                The Namespace URI.
	 *@param  localName         The local name without prefix.
	 *@param  name              The local name with prefix.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void endElement(String ns, String localName, String name) throws SAXException {
		if (isCurrent) {
			outputHandler.endElement(ns, localName, name);
		} else {
			add(new ContentHandlerInfo(ContentHandlerInfo.END_ELEMENT, ns, localName, name));
		}
	}

	/**
	 * The startDocument command
	 *
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void startDocument() throws SAXException {
		//System.out.println("STORE START "+outputHandler);
		if (isCurrent) {
			// System.out.println("SEND BufferingHandler StartDocument "+outputHandler);
			outputHandler.startDocument();
		} else {
			// System.out.println("STORE Buffering Handler startDocument "+iStack);
			iStack = null;
			iStack=new Fifo();
			add(new ContentHandlerInfo(ContentHandlerInfo.START_DOCUMENT));
		}
	}

	/**
	 *  The setDocumentLocator command
	 *
	 *@param  locator  - An object that can return the location of any SAX document event.
	 */
	public void setDocumentLocator(Locator locator) {
		if (isCurrent) {
			outputHandler.setDocumentLocator(locator);
		} else {
			add(new ContentHandlerInfo(ContentHandlerInfo.SET_DOCUMENT_LOCATOR, locator));
		}
	}


	/**
	 * The endDocument command
	 *
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void endDocument() throws SAXException {
		// Beware of the order of the isDone assignment
		if (isCurrent) {
			isDone = true;
			outputHandler.endDocument();
		} else {
			// System.out.println("STORE Buffering Handler endDocument "+iStack);
			add(new ContentHandlerInfo(ContentHandlerInfo.END_DOCUMENT));
			isDone = true;
		}
	}

	/**
	 * The skippedEntity command
	 *
	 *@param  name              The name of the skipped entity. If it is a parameter entity, the name will begin with '%', and if it is the external DTD subset, it will be the string "[dtd]".
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void skippedEntity(java.lang.String name) throws SAXException {
		if (isCurrent) {
			outputHandler.skippedEntity(name);
		} else {
			add(new ContentHandlerInfo(ContentHandlerInfo.SKIPPED_ENTITY, name));
		}
	}

	/**
	 * The processingInstruction command
	 *
	 *@param  target            The processing instruction target.
	 *@param  data              The processing instruction data, or null if none was supplied. The data does not include any whitespace separating it from the target.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void processingInstruction(java.lang.String target, java.lang.String data) throws SAXException {
		if (isCurrent) {
			outputHandler.processingInstruction(target, data);
		} else {
			add(new ContentHandlerInfo(ContentHandlerInfo.PROCESSING_INSTRUCTION, target, data));
		}
	}


	/**
	 * The ignorableWhitespace command
	 *
	 *@param  ch                The characters from the XML document.
	 *@param  start             The start position in the array.
	 *@param  length            The number of characters to read from the array.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
		if (isCurrent) {
			outputHandler.ignorableWhitespace(ch, start, length);
		} else {
			add(new ContentHandlerInfo(ContentHandlerInfo.IGNORABLE_WHITESPACE, ch, start, length));
		}
	}

	/**
	 * The characters command
	 *
	 *@param  ch                The characters from the XML document.
	 *@param  start             The start position in the array.
	 *@param  length            The number of characters to read from the array.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void characters(char[] ch, int start, int length) throws SAXException {
		char[] tmp = new char[length + start];
		for (int i = start; i < length + start; i++) {
			tmp[i] = ch[i];
		}
		if (isCurrent) {
			outputHandler.characters(tmp, start, length);
			/*
			 *String toto = "";
			 *for (int i=start; i<length+start; i++) toto+=ch[i];
			 *System.out.println("current . characters ; " + toto );
			 */
		} else {
			add(new ContentHandlerInfo(ContentHandlerInfo.CHARACTERS, tmp, start, length));
			/* String toto = "";
			 *for (int i=start; i<length+start; i++) toto+=ch[i];
			 *System.out.println("STORE in BufferingHandler "+this+" characters ; " + toto );   */
		}
	}

	/**
	 * The startPrefixMapping command
	 *
	 *@param  prefix            The prefix that was being mapping.
	 *@param  uri               The Namespace URI the prefix is mapped to.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void startPrefixMapping(java.lang.String prefix, java.lang.String uri) throws SAXException {
		if (isCurrent) {
			outputHandler.startPrefixMapping(prefix, uri);
		} else {
			add(new ContentHandlerInfo(ContentHandlerInfo.START_PREFIX_MAPPING, prefix, uri));
		}
	}

	/**
	 * The endPrefixMapping command
	 *
	 *@param  prefix            The prefix that was being mapping.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void endPrefixMapping(java.lang.String prefix) throws SAXException {
		if (isCurrent) {
			outputHandler.endPrefixMapping(prefix);
		} else {
			add(new ContentHandlerInfo(ContentHandlerInfo.END_PREFIX_MAPPING, prefix));
		}
	}

	/**
	 * Set the done attribute
	 *
	 *@param  pDone  The new done value
	 */
	public void setDone(boolean pDone) {
		isDone = pDone;
	}

	/**
	 * Test if the handling is done
	 *
	 *@return    The done value
	 */
	public final boolean isDone() {
		return isDone;
	}

	/**
	 * test if the mixer is the current mixer
	 *
	 *@return    The current value
	 */
	public final boolean isCurrent() {
		return isCurrent;
	}

	/**
	 * set the curent attribut
	 *
	 *@param  pCurrent  The new current value
	 */
	public final void setCurrent(boolean pCurrent) {
		isCurrent = pCurrent;
	}

	/**
	 * Compute the stack of contentHandlerInfo
	 *
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void flushBuffer() throws SAXException {
		// System.out.println("FLUSH Buffering Handler "+outputHandler+" "+iStack);
		ContentHandlerInfo current = null;
		while (size() > 0) {
			current = pop();
			current.sendEvent(outputHandler);
			if (current.getCommand() == ContentHandlerInfo.END_DOCUMENT) {
				isDone = true;
			}
			/*switch(current.getCommand() ) {
			 *case ContentHandlerInfo.START_ELEMENT :
			 *outputHandler.startElement(current.getStringParameter(0),
			 *current.getStringParameter(1),
			 *current.getStringParameter(2),
			 *current.getAttributes());
			 *break;
			 *case ContentHandlerInfo.END_ELEMENT :
			 *outputHandler.endElement(current.getStringParameter(0),
			 *current.getStringParameter(1),
			 *current.getStringParameter(2));
			 *break ;
			 *case ContentHandlerInfo.START_DOCUMENT :
			 *outputHandler.startDocument();
			 *break ;
			 *case ContentHandlerInfo.END_DOCUMENT :
			 *outputHandler.endDocument();
			 *isDone = true;
			 *break ;
			 *case ContentHandlerInfo.SET_DOCUMENT_LOCATOR :
			 *outputHandler.setDocumentLocator(current.getLocatorParameter());
			 *break ;
			 *case ContentHandlerInfo.SKIPPED_ENTITY :
			 *outputHandler.skippedEntity(current.getStringParameter(0));
			 *break ;
			 *case ContentHandlerInfo.IGNORABLE_WHITESPACE :
			 *outputHandler.ignorableWhitespace(current.getCharParameter(),
			 *current.getIntParameter(0),
			 *current.getIntParameter(1));
			 *break ;
			 *case ContentHandlerInfo.PROCESSING_INSTRUCTION :
			 *outputHandler.processingInstruction(current.getStringParameter(0),
			 *current.getStringParameter(1));
			 *break ;
			 *case ContentHandlerInfo.CHARACTERS :
			 *outputHandler.characters(current.getCharParameter(),
			 *current.getIntParameter(0),
			 *current.getIntParameter(1));
			 *break ;
			 *case ContentHandlerInfo.START_PREFIX_MAPPING :
			 *outputHandler.startPrefixMapping(current.getStringParameter(0),
			 *current.getStringParameter(1));
			 *break ;
			 *case ContentHandlerInfo.END_PREFIX_MAPPING :
			 *outputHandler.endPrefixMapping(current.getStringParameter(0));
			 *break ;
			 *default :
			 *System.out.println("[bufferingHandler]  ExecuteContentHandlerInfo : commandes inconnues");
			 *} //end switch
			 */
		}//end while
	}//end flushBuffer
}

