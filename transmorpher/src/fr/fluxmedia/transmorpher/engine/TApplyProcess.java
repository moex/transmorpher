/**
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
  * This class define the element apply-process
  *
  * @author Guillaume.Chomat@inrialpes.fr
  * @date 30-07-2001
  * @since jdk 1.3 && SAX 2.0
  */

package fr.fluxmedia.transmorpher.engine ;

// Imported JAXP classes

import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;

// Imported SAX classes

import org.xml.sax.SAXException;

// Imported java.io classes

import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
import fr.fluxmedia.transmorpher.utils.TMException;
import fr.fluxmedia.transmorpher.utils.TMRuntimeException;

public final class TApplyProcess extends TProcessComposite {

    String ref = null;
    String iListLocalIn[] = null;
    String iListLocalOut[] = null;
    // used inside a Repeat
    boolean test=true;
    Vector<TProcess> listOfProcess = null;

  public TApplyProcess(String[] in,String[] out,
                      Parameters pParam,
                      StringParameters staticAttributes)
    throws TransformerException, TransformerConfigurationException,
            SAXException, IOException{
    super(in,out,pParam);
    setRef(staticAttributes.getStringParameter("ref"));
    listOfProcess = new Vector<TProcess>();
  }
   
  public String getRef(){
    return ref;
  }

  public void setRef(String ref){
    this.ref = ref ;
  }
    
    // JE: strange
    public void bindPorts(TProcess process) throws TMException, SAXException {
    process.generatePort();
    if (test)
	listOfProcess.add( process );
    for(int i=0 ; i < process.getNameOut().length ; i++) {
      if(process.getNameOut(i).equals("_null_")) {
        this.addChannel(new TChannel("_null_",process,null));
      } else {
        listOfProcessOut.put(process.getNameOut(i),process);
        TProcess isSameIn = (TProcess)listOfProcessIn.get(process.getNameOut(i));
        if(isSameIn!=null){
          this.addChannel(new TChannel(process.getNameOut(i),process,isSameIn));
          listOfProcessIn.remove(process.getNameOut(i));
          listOfProcessOut.remove(process.getNameOut(i));
        } else { // may be a composite port
        for (int i1=0; i1 < iListLocalOut.length ; i1++){
         if (process.getNameOut(i).equals(iListLocalOut[i1])) {
          iListOut[i1]= process.getOut(i);
         }
        }
      }
      }
    }
  
    for(int i=0 ; i < process.getNameIn().length ; i++) {
      //System.out.println(process.getNameIn(i).getCompositeName());
      listOfProcessIn.put(process.getNameIn(i),process);
      
      TProcess isSameOut =(TProcess)listOfProcessOut.get(process.getNameIn(i));
      
      if(isSameOut!=null){
        this.addChannel(new TChannel(process.getNameIn(i),isSameOut,process));
        listOfProcessIn.remove(process.getNameIn(i));
        listOfProcessOut.remove(process.getNameIn(i));
      } else { // may be a composite port
        for (int i1=0; i1 < iListLocalIn.length ; i1++){
         if (process.getNameIn(i).equals(iListLocalIn[i1])) {
          iListIn[i1]= process.getIn(i);
         }
        }
      }//end if
    }
}   
  public void setLocalName(String[] locIn, String[] locOut){
   iListLocalIn = locIn ;
   iListLocalOut = locOut ;

   // JE : this seems to have been added by Guillaume
   // I am not sure that this does not double with generatePort ??

   for(int i = 0; i<nameIn.length;i++){
       iListIn[i] = new XML_Port(locIn[i],this,0,XML_Port.IN);
       listOfProcessIn.put(locIn[i],this);
   }
   for(int i = 0; i<nameOut.length;i++){
       iListOut[i] = new XML_Port(locOut[i],this,0,XML_Port.OUT);
       listOfProcessOut.put(locOut[i],this);
   }
  }
    
  public void generatePort(){
      for(int i = 0; i<nameIn.length;i++){
	  iListIn[i].setName(nameIn[i]);

      }
      for(int i = 0; i<nameOut.length;i++){  
	  iListOut[i].setName(nameOut[i]);
      }
  }

    /**
     * Initializes inner processes when ApplyProcess is inside a Loop.
     */ 
    public void reset() throws TMException, SAXException {
	listOfChannel = new Hashtable<String,TChannel>();
	test = false;
	for ( TProcess process: listOfProcess ) { // JE2022: reset could be applied to any...
	    if (process instanceof TTransformation) process.reset();
	    if (process instanceof TApplyProcess) process.reset();
	    bindPorts(process);
	}
    }
}
