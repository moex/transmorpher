/**
 * Transmorpher
 *
 * Copyright (C) 2001 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
  *
  * This class allow to define and manage Basic process
  *
  * @ since jdk 1.2 and SAX 2.0
*/

package fr.fluxmedia.transmorpher.engine ;

import fr.fluxmedia.transmorpher.utils.Parameters ;
import fr.fluxmedia.transmorpher.utils.TMRuntimeException;

import java.io.IOException;

import org.xml.sax.SAXException; 

public abstract class TProcessBasic extends TProcess_Impl implements Runnable {

  /** the name of the father */
  //protected String nameFather = null ; <= move to process

  protected String type = null ;
	
	  protected Thread procThread = null ;
	 
	 protected boolean isThread = false ;
	 
	 	 protected boolean finished = true ;

	 
	/**
	 *  Description of the Field
	 */
	protected Parameters param = null;
	
  /** the empty constructor*/
  public TProcessBasic () {}
    
  /** The constructor */
  protected TProcessBasic (String[] pIn, String[] pOut) {
    super(pIn,pOut);
		//procThread=new Thread(this);
  }
   
  /** The constructor */
   protected TProcessBasic (String[] pIn, String[] pOut,Parameters pParam) {
    this(pIn,pOut);
		param=pParam;
    type = (String)pParam.getParameter("type");
    setParameters(pParam);
  }
    
    
  /** The constructor */
   protected TProcessBasic (String[] pIn, String[] pOut,String type) {
    this(pIn,pOut);
    this.type = type;
  }
    
    public void setType(String type){
	this.type = type;
    }
    
    public String getType(){
	return type;
    }
 
 public void execProcess() throws IOException, SAXException, TMRuntimeException{
	 if (isThread){
		 if (procThread!=null){
			 procThread.start();
	 	}
	 }
 }
 
	public boolean getFinished(){
	 return finished;
 }
 
 public void run(){
	 try{
		 System.out.flush();
		 while(!(getFinished())){
			 Thread.sleep(10);
		 }
	 }
	 catch (Exception e){}
 }
 
 public void setFinished(boolean finished){
	this.finished=finished;
 }
 
 	public void setThread(boolean useThread) {
		isThread = useThread;
		if ((procThread == null)&&isThread) {
	    procThread = new Thread(this);
		}
  }
	
	public void initParameters(){
	}
}
