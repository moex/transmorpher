/**
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
/**
  *
  * This class allow to build a composite process
  *
  * @author Laurent Tardif (laurent.tardif@inrialpes.fr)
  * @since jdk 1.2 / SAX 2.0
  * @see fr.fluxmedia.transmorpher.engine.XML_Port
*/

package fr.fluxmedia.transmorpher.engine ;

import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.TMRuntimeException;
import fr.fluxmedia.transmorpher.utils.TMException;

// Imported JAXP classes

import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;

// Imported SAX classes

import org.xml.sax.SAXException;

// Imported JAVA classes

import java.io.IOException;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Vector;


public abstract class TProcessComposite extends TProcess_Impl {
    
  /** The max number of process in a composite */
  public static int NB_PROCESS_RISE = 200;
    
   /** the list of the TProcessBasic (except the TApplyProcess) of
        our TProcessComposite. May contain no element.*/
  protected Vector<TProcess> listOfProcessBasic = null;
    
  /** the list of the TChannel of our TProcessComposite. May contain no element.*/
    protected Hashtable<String,TChannel> listOfChannel = null;

  /** the list of the Generator of our transmorpher. May contain no element.*/
  protected Vector<TReader> listOfGenerator = null;

  /** the list of PortProcess In */
  protected Hashtable<String,TProcess> listOfProcessIn = null;

  /** the list of PortProcess Out */
  protected Hashtable<String,TProcess> listOfProcessOut = null;
    
  /** the TApplyProcess' list */
  protected Vector<TProcess> listOfApplyProcess = null;
    
  /** The constructor : build a Composite process with pNbIn in Port, and pNbOut Out Port  */
  public TProcessComposite(String[] pIn, String[] pOut, Parameters p)
    throws TransformerException, TransformerConfigurationException,
           SAXException, IOException  {
  
    super(pIn,pOut);
    // JE: Maybe this should go even upper...
    setParameters( p );
    listOfProcessBasic = new Vector<TProcess>(NB_PROCESS_RISE*5);
    listOfChannel = new Hashtable<String,TChannel>(NB_PROCESS_RISE*5);
    listOfGenerator = new Vector<TReader>(NB_PROCESS_RISE*5);
    listOfProcessIn = new Hashtable<String,TProcess>(NB_PROCESS_RISE*5);
    listOfProcessOut = new Hashtable<String,TProcess>(NB_PROCESS_RISE*5);
    listOfApplyProcess = new Vector<TProcess>(NB_PROCESS_RISE*5);
  }

    public void addChannel( TChannel channel ){
	if ( listOfChannel.get( channel.getName()) == null ) {
	    listOfChannel.put( channel.getName(), channel );
	} else {
	    System.out.println( "Error: channel "+channel.getName()+" already Exist!!" );
	    System.exit(1);
	}
    } //END PROC
      
    public TChannel getChannel(String name){
	return listOfChannel.get(name);
    }
    
    public void addGenerator(TReader generator){
	// the generator has been added in the list of Process.
	// So it has a single id
	listOfGenerator.add(generator);
    }
    
    public Vector<TReader> getGenerator(){
	return listOfGenerator;
    }
    
  public Hashtable<String,TProcess> getListOfProcessIn(){
    return listOfProcessIn;
  }
    
  public Hashtable<String,TProcess> getListOfProcessOut(){
    return listOfProcessOut;
  }
    
  public void addApplyProcess( TProcess process ){
    listOfApplyProcess.add( process );
  }
    
  public void addProcess( TProcess process ){
      
      //System.out.println("process :" + process);
      process.setFatherName(getName());
      
      if( process instanceof TApplyProcess ) {
	  addApplyProcess( process );
      } else {
	  listOfProcessBasic.add( process );
      } //end if
      if ( process instanceof TReader ) this.addGenerator( (TReader)process );
  } //end proc
    
    public Vector<TProcess> getApplyProcess(){
	return listOfApplyProcess;
    }
    
  public Vector<TProcess> getProcessBasic(){
    return listOfProcessBasic;
  }

  public Vector<TProcess> getAllProcess(){
    Vector<TProcess> allProcess = new Vector<TProcess>( listOfProcessBasic );
    allProcess.addAll( listOfApplyProcess );
    return allProcess;
  }

    public final void bindParameters(Parameters p) throws TMRuntimeException, TMException, SAXException {
	// bind its parameters to those of the calling process
	p.bindCallerParameters( iListOfParameters );
	// bind these parameters to the processes it calls
	for ( TProcess process: listOfProcessBasic ) {
	    process.bindParameters( iListOfParameters );
	}
	for( TProcess process: listOfApplyProcess ) {
	    process.bindParameters( iListOfParameters );
	}
	bindPorts();
    }
 
    public final void bindPorts() throws TMRuntimeException, TMException, SAXException {
	for ( TProcess process: listOfProcessBasic ) {
	    bindPorts( process );
	}
	for( TProcess process: listOfApplyProcess ) {
	    bindPorts( process );
	}
    }
 
    public void bindPorts(TProcess process) throws TMRuntimeException, TMException, SAXException {
    process.generatePort() ;
   
   // System.out.println("debut for " + process);
    for( String s: process.getNameOut() ) {
	if (s.equals("_null_")) {
	    this.addChannel(new TChannel("_null_",process,null));
	} else {
	    listOfProcessOut.put( s, process );
	    TProcess isSameIn = listOfProcessIn.get( s );
	    if(isSameIn != null){
		this.addChannel(new TChannel( s, process, isSameIn ));
		listOfProcessIn.remove( s );
		listOfProcessOut.remove( s );
	    }
	}
    }
  
    for( String s: process.getNameIn() ) {
	//System.out.println(process.getNameIn(i).getCompositeName());
	listOfProcessIn.put( s, process );
        
	TProcess isSameOut = listOfProcessOut.get( s );
        
	if( isSameOut != null ){
	    this.addChannel(new TChannel( s, isSameOut, process ));
	    listOfProcessIn.remove( s );
	    listOfProcessOut.remove( s );
	}
    }
    }
 
  /** Call the execution of the process */
  public final void execProcess(boolean useThread)
      throws SAXException,IOException, TMRuntimeException {
      // JE: it is certainly time to open stdin and stdout if it is necessary
      for ( TProcess o: listOfProcessBasic ) {
	  if (!(o instanceof TLoop)){
	      TProcessBasic process = (TProcessBasic)o;
	      process.setThread(useThread);
	      if (!(process instanceof TReader))
		  process.execProcess();
	  }
      }
      for ( TReader reader: listOfGenerator ) {
	  //reader.setThread(useThread);
	  reader.execProcess();
      }
  }
    
  public void showChannel(){
    for ( Enumeration<TChannel> e = listOfChannel.elements() ; e.hasMoreElements() ;) {
      TChannel channel = e.nextElement();
      System.out.println("channel : "+ channel.getName());
      System.out.println("transmitter : "+((TProcess)channel.getIn()).getName());
      System.out.println("receiver : "+((TProcess)channel.getOut()).getName());
      System.out.println("");
    }
  }
     
 
  public void generatePort(){
    if(nameFather == null) {
      for(int i = 0; i<nameIn.length;i++){
        iListIn[i] = new XML_Port(nameIn[i],this,0,XML_Port.IN);
      }
      for(int i = 0; i<nameOut.length;i++){
        iListOut[i] = new XML_Port(nameOut[i],this,0,XML_Port.OUT);
      }
    } else {
      for(int i = 0; i<nameIn.length;i++){
        iListIn[i] = new XML_Port(nameFather+"-"+nameIn[i],this,0,XML_Port.IN);
      }
      for(int i = 0; i<nameOut.length;i++){
        iListOut[i] = new XML_Port(nameFather+"-"+nameOut[i],this,0,XML_Port.OUT);
      }
    }
  }


}
