/**
 * $Id: XML_Port.java,v 1.3 2003-04-22 14:53:57 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.engine;

// Imported TraX classes

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.sax.SAXTransformerFactory;
import  fr.fluxmedia.tmcontrib.external.Log;
// Imported SAX classes

import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import fr.fluxmedia.transmorpher.utils.TMException;

/**
 *@author    laurent.Tardif@inrialpes.fr
 *@since     SAX 2.0 && jdk 1.3
 */

public final class XML_Port {

	TransformerHandler handler;
    
  SAXTransformerFactory tfactory = null;
	
	/** The name of the port */
	String iName = null;

	/** The name of the port in his father, available only for composite element */
	String iCompositeName = null;

	/** constant for IN XML_PORT */
	public final static int IN = 0;

	/** constant for OUT XML_PORT */
	public final static int OUT = 1;

	/** constant for print IN && OUT XML_PORT */
	public final static String[] InOut = {"In", "Out"};

	/** Constant for XML READER */
	public final static int XML_READER = 0;

	/** Constant for NOT AFFECTED PORT */
	public final static int EMPTY = 1;

	/** Constant for XML SERIALIZER */
	public final static int SERIALIZER = 2;

	/** Constant for  TRANSFORMER_HANDLER_IN */
	public final static int TRANSFORMER_HANDLER_IN = 3;

	/** Constant for TRANSFORMER_HANDLER_OUT */
	public final static int TRANSFORMER_HANDLER_OUT = 4;

	/** Constant for CONTENT_DISPATCHER_IN */
	public final static int CONTENT_DISPATCHER_IN = 5;

	/** Constant for CONTENT_DISPATCHER_OUT */
	public final static int CONTENT_DISPATCHER_OUT = 6;

	/** Constant for CONTENT_HANDLER_IN */
	public final static int CONTENT_HANDLER_IN = 7;

	/** Constant for CONTENT_HANDLER_OUT */
	public final static int CONTENT_HANDLER_OUT = 8;

	/** Constant for CONNECTOR_IN */
	public final static int CONNECTOR_IN = 9;

	/** Constant for CONNECTOR_OUT */
	public final static int CONNECTOR_OUT = 10;

	/** Constant for NULL */
	public final static int NULL = 11;
	
	public final static int CONTENT_LOG_OUT = 12;

	/** Constant for printing PortType */
	public final static String[] PortType = {"XML_READER", "EMPTY", "SERIALIZER",
			"TRANSFORMER_HANDLER_IN", "TRANSFORMER_HANDLER_OUT",
			"CONTENT_DISPATCHER_IN", "CONTENT_DISPATCHER_OUT",
			"CONTENT_HANDLER_IN", "CONTENT_HANDLER_OUT",
			"CONNECTOR_IN", "CONNECTOR_OUT", "NULL","CONTENT_LOG_OUT"};

	/** The XML TProcess Owner */
	TProcess iProcessOwner = null;

	/** The port number */
	int iNumPort = -1;

	/** the Type of the port : IN or OUT */
	int iInOut = -1;

	/** The Type of the owner */
	int iTypePort = -1;

	/** The encoding type of the flow */
	String iEncodingType = "UTF-8";

	/** the dtd of the port */
	String iDTDName = "Default_dtd";

	/** The content flow of the port */
	Object iContent = null;

	/**
	 * One constructor
	 *
	 *@param  pName          The name of the port.
	 *@param  pProcessOwner  The owner of the port
	 *@param  pNumPort       the number id of the port
	 *@param  pInOut         the type of the port : 0 for IN 1 for OUT
	 */
	public XML_Port(String pName,
			TProcess pProcessOwner,
			int pNumPort, int pInOut) {
		iName = pName;
		iCompositeName = pName;//by default name = composite name,
		//for compostie you must call SET COMPOSITE NAME later :)
		iProcessOwner = pProcessOwner;
		iNumPort = pNumPort;
		iInOut = pInOut;
		iTypePort = EMPTY;
		if (pName.equals("_stdout_")){
			try{
				handler = getTransformerFactory().newTransformerHandler();
				handler.setResult(new StreamResult(System.out));
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}

	/**
	 * One constructor for XMLReader ports
	 *
	 *@param  pName          The name of the port.
	 *@param  pProcessOwner  The owner of the port
	 *@param  pXMLReader     The XMLReader.
	 *@param  pNumPort       The number id of the port
	 *@param  pInOut         The type of the port : 0 for IN 1 for OUT
	 */
	public XML_Port(String pName,
			TProcess pProcessOwner,
			XMLReader pXMLReader,
			int pNumPort, int pInOut) {
		this(pName, pProcessOwner, pNumPort, pInOut);
		iTypePort = XML_READER;
		iContent = pXMLReader;
		if (pName.equals("_stdout_")){
			((XMLReader)iContent).setContentHandler(handler);
		}
		//  System.err.println("Creation d un port de type XML_READER");
	}

	/**
	 * One constructor for TDispatcher ports
	 *
	 *@param  pName          The name of the port.
	 *@param  pProcessOwner  The owner of the port
	 *@param  pTDispatcher   The TDispatcher
	 *@param  pNumPort       The number id of the port
	 *@param  pInOut         The type of the port : 0 for IN 1 for OUT
	 */
	public XML_Port(String pName,
			TProcess pProcessOwner,
			TDispatcher pTDispatcher,
			int pNumPort, int pInOut) {

		this(pName, pProcessOwner, pNumPort, pInOut);
		iContent = pTDispatcher;
		if (pInOut == IN) {
			iTypePort = CONTENT_DISPATCHER_IN;
		} else {
			iTypePort = CONTENT_DISPATCHER_OUT;
			if (pName.equals("_stdout_")){
				((TDispatcher) iContent).setContentHandler(pNumPort,handler);

			}
		}
		//System.err.println("Creation d un port de type CONTENT_DISPATCHER :" +pNumPort);
	}
	
	public XML_Port(String pName,
			TApplyExternal pProcessOwner,
			ContentHandler apply,
			int pNumPort, int pInOut) {

		this(pName, pProcessOwner, pNumPort, pInOut);
		iContent = apply;
		
			iTypePort = CONTENT_LOG_OUT;
			if (pName.equals("_stdout_")){
				((TApplyExternal) iContent).setContentHandler(handler);

			}
		}
	

	/**
	 * One constructor for a content handler
	 *
	 *@param  pName            The name of the port.
	 *@param  pProcessOwner    The owner of the port
	 *@param  pContentHandler  The Content Handler
	 *@param  pNumPort         The number id of the port
	 *@param  pInOut           The type of the port : 0 for IN 1 for OUT
	 */
	public XML_Port(String pName,
			TProcess pProcessOwner,
			ContentHandler pContentHandler,
			int pNumPort, int pInOut) {

		this(pName, pProcessOwner, pNumPort, pInOut);
		iContent = pContentHandler;
		iTypePort = CONTENT_HANDLER_IN;
		//System.err.println("Creation d un port de type ContentHandler");
	}

	/**
	 * One constructor for TMerger ports
	 *
	 *@param  pName            The name of the port.
	 *@param  pProcessOwner    The owner of the port
	 *@param  pContentHandler  The TMerger
	 *@param  pNumPort         The number id of the port
	 *@param  pInOut           The type of the port : 0 for IN 1 for OUT
	 */
	public XML_Port(String pName,
			TProcess pProcessOwner,
			TMerger pContentHandler,
			int pNumPort, int pInOut) {

		this(pName, pProcessOwner, pNumPort, pInOut);
		iContent = pContentHandler;
		iTypePort = CONTENT_HANDLER_OUT;
		if (pName.equals("_stdout_")){
				((TMerger) iContent).setContentHandler(handler);

		}
		//System.err.println("Creation d un port de type TMerger");
	}

	/**
	 * One constructor for TLoop ports
	 *
	 *@param  pName            The name of the port.
	 *@param  pProcessOwner    The owner of the port
	 *@param  pContentHandler  The TLoop
	 *@param  pNumPort         The number id of the port
	 *@param  pInOut           The type of the port : 0 for IN 1 for OUT
	 */
	public XML_Port(String pName,
			TProcess pProcessOwner,
			TLoop pContentHandler,
			int pNumPort, int pInOut) {

		this(pName, pProcessOwner, pNumPort, pInOut);
		iContent = pContentHandler;
		iTypePort = CONTENT_HANDLER_OUT;
		// System.err.println("Creation d un port de type ContentHandler");
	}


	/**
	 * One constructor for TransformerHandler ports
	 *
	 *@param  pName                The name of the port.
	 *@param  pProcessOwner        The owner of the port
	 *@param  pTransformerHandler  The TransmorpherHandler
	 *@param  pNumPort             The number id of the port
	 *@param  pInOut               The type of the port : 0 for IN 1 for OUT
	 */
	public XML_Port(String pName,
			TProcess pProcessOwner,
			TransformerHandler pTransformerHandler,
			int pNumPort, int pInOut) {

		this(pName, pProcessOwner, pNumPort, pInOut);
		if (pInOut == IN) {
			iTypePort = TRANSFORMER_HANDLER_IN;
		} else {
			iTypePort = TRANSFORMER_HANDLER_OUT;
			if (pName.equals("_stdout_")){
				pTransformerHandler.setResult(new SAXResult(handler));
			}
		}
		iContent = pTransformerHandler;
		// System.err.println("Creation d un port de type TRANSFORMER_HANDLER");
	}

    /**
     * Get the name of the port
     *
     *@return    The name value
     */
    public final String getName() { return iName; }
    
    /**
     * Get the composite name of the port
     *
     *@return    The compositeName value
     */
    public final String getCompositeName() { return iCompositeName; }
    
    /**
     * Set the composite name of the port
     *
     *@param  pCompositeName  The new compositeName value
     */
    public final void setCompositeName(String pCompositeName) {
	iCompositeName = pCompositeName;
	iName = pCompositeName;
    }
    
    /**
     * Set the composite name of the port
     *
     *@param  pName  The name of the port.  The new name value
     */
    public final void setName(String pName) {
	iName = pName;
	iCompositeName = pName;
    }
    
    /**
     * Get the owner of the port
     *
     *@return    The owner value
     */
    public final TProcess getOwner() { return iProcessOwner; }
    
    /**
     * Get the IN/ OUT type of the port
     *
     *@return    The inOut value
     */
    public final int getInOut() { return iInOut; }
    
    /**
     * Get the owner type
     *
     *@return    The type value
     */
    public final int getType() { return iTypePort; }
    
    /**
     * Get the port number
     *
     *@return    The numPort value
     */
    public final int getNumPort() { return iNumPort; }
    
    /**
     * get the content
     *
     *@return    The content value
     */
    public final Object getContent() { return iContent; }
    
    /**
     *  Sets the content attribute of the XML_Port object
     *
     *@param  contenthandler  The new content value
     */
    public final void setContent(Object contenthandler) {
	iContent = contenthandler;
    }
    
    /**
     * Set the encoding type of the flow
     *
     *@param  pEncoding  The new encoding value
     */
    public final void setEncoding(String pEncoding) {
	iEncodingType = pEncoding;
    }
    
    /**
     * get the encoding type of the flow
     *
     *@return    The encoding value
     */
    public final String getEncoding() {	return iEncodingType; }
    
    
    /**
     * set the dtd of the port
     *
     *@param  pDTDName  The new DTD value
     */
    public final void setDTD(String pDTDName) {	iDTDName = pDTDName; }
    
    /**
     * get the dtd of the port
     *
     *@return    The dTD value
     */
    public final String getDTD() { return iDTDName; }
    
    /**
     * Link two ports
     *
     *@param  p1                An XML_Port
     *@param  p2                Another XML_Port
     *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
     */
    public final static void link(XML_Port p1, XML_Port p2) throws TMException, SAXException {
	//System.out.println("link port p1 : "+ p1 +" port p2 : "+p2);
	if (p2.getType() == EMPTY) {
	    //System.err.println("Link : p2 is EMPTY reverse the call order");
	    link(p2, p1);
	}
	//System.err.println("port p1 : "+ p1 +" port p2 : "+p2);
	switch (p1.getType()) {
	case XML_READER:
	    //System.err.println("Link un XML_READER "+p1.getContent());
	    //System.err.println("to "+p2.getContent());
	    ((XMLReader) p1.getContent()).setContentHandler((ContentHandler) p2.getContent());
	    //System.err.println("FIN XMLREADER ");
	    break;
	case EMPTY:
	    //System.err.println("Link un EMPTY");
	    TProcess p1_process = p1.getOwner();
	    if (p1.getInOut() == IN) {
		p1_process.setIn(p1.getNumPort(), p2);
	    } else {
		p1_process.setOut(p1.getNumPort(), p2);
	    }
	    break;
	case SERIALIZER:
	    //System.err.println("Link un SERIALIZER not yet implemented !");
	    break;
	case TRANSFORMER_HANDLER_IN:
	    //System.err.println("Link un TRANSFORMER_HANDLER_IN not yet implemented !");
	    break;
	case TRANSFORMER_HANDLER_OUT:
	    //System.err.println("Link un TRANSFORMER_HANDLER_OUT");
	    TransformerHandler TH = ((TransformerHandler) p1.getContent());
	    TH.setResult(new SAXResult((ContentHandler) p2.getContent()));
	    break;
	case CONTENT_DISPATCHER_IN:
	    throw new TMException("CONTENT_DISPATCHER_IN linking not yet implemented");
	case CONTENT_DISPATCHER_OUT:
	    //System.err.println("Link un CONTENT_DISPATCHER_OUT");
	    ((TDispatcher) p1.getContent()).setContentHandler(p1.getNumPort(), (ContentHandler) p2.getContent());
	    break;
	case CONTENT_HANDLER_IN:
	    throw new TMException("CONTENT_HANDLER_IN linking not yet implemented");
	    //hum
	    //((OneMixer)p1.getContent()).setContentHandler((ContentHandler)p2.getContent());
	    //break;
	case CONTENT_LOG_OUT:
		((Log) p1.getContent()).setContentHandler((ContentHandler) p2.getContent());
		break;
	case CONTENT_HANDLER_OUT:
	    //System.err.println("Link un CONTENT_HANDLER_OUT" + "P1 : " +p1 +" p2: "+p2);
	    if (p1.getContent() instanceof TMerger) {
		((TMerger) p1.getContent()).setContentHandler((ContentHandler) p2.getContent());
	    } else {
		((TLoop) p1.getContent()).setContentHandler((ContentHandler) p2.getContent(), p1.getNumPort());
	    }
	    break;
	case CONNECTOR_IN:
	    throw new TMException("CONNECTOR_IN linking not yet implemented");
	case CONNECTOR_OUT:
	    throw new TMException("CONNECTOR_OUT linking not yet implemented");
	default:
	    throw new TMException("Unknown port type for linking : "+p2.getType()+" / "+PortType[p2.getType()]+" / "+p2.getNumPort());
	}//end switch
    }
    
    /**
     * fonction to pring Port information
     *
     *@return    An string describing this XML_Port
     */
    public String toString() {
	return "<XML_Port: " + iProcessOwner + "." + InOut[iInOut] + "[" + iNumPort + "]:" + PortType[iTypePort] + ">";
    }//end toString
    
			protected SAXTransformerFactory getTransformerFactory(){
		if(tfactory == null){
			tfactory = (SAXTransformerFactory) TransformerFactory.newInstance();
		}
		return tfactory;
	}
}//End class

