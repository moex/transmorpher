/**
 * $Id: TDispatcher.java,v 1.1 2002-11-06 14:08:21 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.engine;

// Imported transmorpher classes

import fr.fluxmedia.transmorpher.utils.Parameters;

// Imported java classes

import java.util.Hashtable;
import org.xml.sax.Attributes;

// Imported SAX classes

import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/**
 * this class define the dispatcher process.
 * dispatch one data flow to different process.
 *
 * @author laurent tardif
 *@created      10 02 2001
 *@since     jdk 1.2 SAX 2.0
 */
public abstract class TDispatcher extends TProcessBasic implements ContentHandler {
	/** a list of content handler to dispatch information */
	public ContentHandler[] contentHandlers = null;

	/**
	 * the constructor
	 *
	 *@param  pIn     In port names
	 *@param  pOut    Out port names
	 *@param  pParam  Description of the Parameter
	 */
	public TDispatcher(String[] pIn, String[] pOut, Parameters pParam) {
		super(pIn, pOut, pParam);
		contentHandlers = new ContentHandler[iNbOut];
	}

	/**
	 * Sets the content handler at index i in the content handlers list.
	 *
	 *@param  i                The index value
	 *@param  pContentHandler  The new contentHandler value
	 */
	public final void setContentHandler(int i, ContentHandler pContentHandler) {
		contentHandlers[i] = pContentHandler;
	}

	/**
	 * Generates ports for this component
	 */
	public void generatePort() {
		//FT : I removed the following code for running transmorpher with compiled process. 
		// When transmorpher is started with jar, it seems not use this part. 
		/*if(nameFather != null) {
			for (int i=0; i<iNbOut; i++) {
				iListOut [i] = new XML_Port(nameFather+"-"+nameOut[i],this,this,i,XML_Port.OUT);
			} //end for
			iListIn[0]= new XML_Port(nameFather+"-"+nameIn[0],this,(ContentHandler)this,0,XML_Port.IN);
		} else { */
		for (int i = 0; i < iNbOut; i++) {
			iListOut[i] = new XML_Port(nameOut[i], this, this, i, XML_Port.OUT);
		}//end for
		iListIn[0] = new XML_Port(nameIn[0], this, (ContentHandler) this, 0, XML_Port.IN);
		//} //end f
	}//end proc
}

