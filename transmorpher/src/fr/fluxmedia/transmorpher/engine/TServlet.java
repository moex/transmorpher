/**
 * $Id: TServlet.java,v 1.1 2002-11-06 14:08:21 serge Exp $
 *
 * Transmorpher
 * 
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.engine;

/**
Author  : Chomat Guillaume
Date    : 07 08 2001
Content : Define a servlet
*/


//import JAXP classes
import javax.xml.transform.TransformerException;

//import sax classes
import org.xml.sax.SAXException;

// import java classes
import java.io.IOException;

import fr.fluxmedia.transmorpher.utils.Parameters;

public final class TServlet
    extends TMain {
   
   /** if true we must do the "retroprogation" of the null Channel*/
   Boolean optimize = new Boolean(false);
    
    /*The only constructor allow for a servlet */ 
   public TServlet(Parameters pParams) throws TransformerException,SAXException,IOException{
	   super(pParams);
   }

  public void setOptimize(Boolean optimize){
    this.optimize = optimize ;
  }
} //end class
