/**
 * $Id: LoopHandler.java,v 1.1 2002-11-06 14:08:21 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.engine;
// Imported SAX classes
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/**
 * Implements the SAX events circulation and buffering in a loop-channel
 * LoopHandler is the handler of a Loop channel.
 * It is a BufferingHandler in two respects:
 * - it takes a buffer of its output (which can be flushed either
 * to the process or to the out);
 *
 *@author    jerome.euzenat@inrialpes.fr
 *@date      14 07 2002
 *@see       "SAX 2.0 & JDK 1.3"
 */

class LoopHandler extends BufferingHandler {

	/** The regular output handler */
	ContentHandler outhdl = null;

	/** The handler of the inner process */
	ContentHandler processHandler = null;

	/** true if the handler is finished */
	boolean isDone = false;

	/** the constructor */
	public LoopHandler() {
		// Since we always, buffer, isCurrent is always false
		isCurrent = false;
	}

	/*
	 * The basic manipulation of this Handler it through:
	 * // Creation
	 * l = new LoopHandler();
	 * // Connection
	 * // input comes from process z and output goes to handler o2
	 * // the inner process p takes its input from this channel through handler o1
	 * l.setOutputHandler( o2 );
	 * l.setInnerHandler( o1 );
	 * z.setOutputHandler( o1 );
	 * p.setOutputHandler( l );
	 * ...
	 * // Within the control of the repeat
	 * l.initLoop();
	 * while ( ... ){
	 *    l.newLoop();
	 * }
	 * l.endLoop();
	 */
	/**
	 *Sets the output handler.
	 *
	 *@param  h  The new outputHandler value
	 */
	public void setOutputHandler(ContentHandler h) {
		outhdl = h;
	}

	/**
	 *  Sets the innerHandler attribute of the LoopHandler object
	 *
	 *@param  h  The new innerHandler value
	 */
	public void setInnerHandler(ContentHandler h) {
		processHandler = h;
	}

	/*
	 * Basically, everything is simple... (pattern for Loop)
	 * 1) events are stored in iStack
	 * 2) newLoop()
	 *     flush iStack to processHandler;
	 * 3) endLoop()
	 *     flush iStack to outhdl
	 */
	/** Initializes the loop. */
	public void initLoop() { }

	/**
	 * newLoop() is called by the associated LoopHandler for
	 * signifying that it has processed previous document and is ready
	 * to get a new one (unfortunately, this should be triggered by the
	 * calling process at each loop)
	 *
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void newLoop() throws SAXException {
		outputHandler = processHandler;
		//System.out.println("FLUSH LoopHandler "+outputHandler);
		flushBuffer();
	}

	/**
	 * endLoop() is called by the calling process after the last loop for
	 * flushing the channel out of the repeat process
	 *
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void endLoop() throws SAXException {
		outputHandler = outhdl;
		//System.out.println("FLUSH LoopHandler "+outputHandler);
		flushBuffer();

	}
	/*
	 * ContentHandler methods are directly inherited from
	 *  the BufferingHandler implementation.
	 * Since the previous input has already been stored,
	 * it is may be not very clever.
	 */
}

