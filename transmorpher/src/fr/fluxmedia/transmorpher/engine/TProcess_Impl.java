/**
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
  * one of the main classes of the transmorpher. This class
  * represent the main class of all TProcess. It's defines the
  * XML_PORT mechanism
  *
  *
  * @since jdk 1.3 / SAX 2.0
  */

package fr.fluxmedia.transmorpher.engine ;

import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.TMRuntimeException;
import fr.fluxmedia.transmorpher.utils.TMException;

import java.util.Hashtable;
import java.util.Enumeration;

import org.xml.sax.SAXException;

public abstract class TProcess_Impl implements TProcess {

	protected int debug=0;
	
  /** The In port */
  public XML_Port [] iListIn = null;
    
  /** the current In port */
  public int iNbIn = 0;
    
  /** The out PORT */
  public XML_Port [] iListOut = null ;
  
  /** The List of out port */
  public int iNbOut =0 ;
    
  /** The list of in port's name*/
  public String[] nameIn = null;
    
  /** The list of out port's name*/
  public String[] nameOut = null;
    
  /** The name of the process */
  String iName = null;
  
  /** the name of the father */
  protected String nameFather = null ;
   
  Parameters iListOfParameters = null ;
  
  protected String id;
       
  /** The constructor */
  protected TProcess_Impl() {
    //TODO : pour etre moins debile mettre un dispatcher a la sortie des basic
    //       des que cela est necessaire
    // Faire une methode add listener ? => PB des ports ?
  }
  

  public TProcess_Impl(String[] pIn, String[] pOut) {
    iListIn = new XML_Port[pIn.length];
    iNbIn = pIn.length;
    iListOut = new XML_Port[pOut.length];
    iNbOut = pOut.length;
    nameIn = pIn;
    nameOut = pOut;
    iListOfParameters = new Parameters();
  } //end constructor
    
  /** The constructor */

  public TProcess_Impl(String[] pIn, String[] pOut,String pId) {
    id = pId;
    iListIn=new XML_Port[pIn.length];
    iNbIn = pIn.length;
    iListOut=new XML_Port[pOut.length];
    iNbOut = pOut.length;
    nameIn = pIn;
    nameOut = pOut;
    iListOfParameters = new Parameters();
  } //end constructor
  
  public String getId(){
    return id;
  }

  public void setId(String id){
    this.id=id;
  }
  
    public void reset() throws TMRuntimeException, TMException, SAXException {};
  
    public void bindParameters( Parameters p ) throws TMRuntimeException, TMException, SAXException {
//	System.err.println("In bP@Basic: "+this);
//iListOfParameters.setParameters(p);
	p.bindParameters( iListOfParameters );
    }

    public Enumeration<String> getParameterNames(){
	return iListOfParameters.getNames();
    }

  /** pass a set of parameters to a process */
  public void setParameters(Parameters p) {
    for( Enumeration<String> e = p.getNames() ; e.hasMoreElements(); ){
      String key = e.nextElement();
      String val = (String)p.getParameter(key);
      if ( val == null ) { val = ""; } //END IF
        iListOfParameters.setParameter(key,val);
    }
  }
     
    public void setParameter( String k, Object o ){
    /* maybe use this cautious version: */
    if( iListOfParameters.getParameter(k) != null && debug > 0 )
	System.err.println("Warning : a param with the same name is already defined.");
    iListOfParameters.setParameter(k,o);
  }
     
  /** get the parameter value of a process */
  public Object getParameter(String k) {
    return iListOfParameters.getParameter(k);
  }
  public Parameters getParameters() {
    return iListOfParameters;
  }
  
  public void setFatherName(String name){
    nameFather = name;
  } //end proc
    
  public String getFatherName(){
    return nameFather;
  } //end proc
   
    
  /** get all the name of out port */
  public String[] getNameOut(){
    return nameOut;
  }
    
  /** get all the name of in port */
  public String getNameIn(int i) throws TMException {
    String name=null;
    if(i>=nameIn.length) {
      throw new TMException("Input namelist out of bound");
    } else {
      name= nameIn[i];
    } //end if
    return name;
  } //end proc
    
  
  /** get all the name of out port */
  public String getNameOut(int i) throws TMException {
    String name=null;
    if(i>=nameOut.length) {
      throw new TMException("Output namelist out of bound");
    } else {
      name= nameOut[i];
    } //end if
    return name;
  } //end proc
    
  /** get all the name of in port */
  public String[] getNameIn(){
    return nameIn;
  }


  /** Get the name of the process */
  public final String getName() {
    return iName;
  }
    
  /** Set the name of the process*/
  public final void setName(String pName) {
    iName=pName;
  }
    
  /* set an In port */
  public final void setIn(int i,XML_Port pFileIn) {
    iListIn[i]=pFileIn;
  }
    
  /** Set an Out Port */
  public final void setOut(int i,XML_Port pFileOut) {
    iListOut[i]=pFileOut;
  }

  /** Get an In port */
  public final XML_Port getIn(int i) {
    return iListIn[i];
  }
    
  /** Get an Out Port */
  public final XML_Port getOut(int i) {
    return iListOut[i];
  }
    
  /** Get an In port */
  public final XML_Port getIn(String pName) {
      for( XML_Port xp: iListIn ) {
	  if ( xp.getName().equals(pName) ) return xp;
      }
      return null;
  }
    
  /** Get an Out Port */
  public final XML_Port getOut( String pName) {
      for( XML_Port xp: iListOut ) {
	  if ( xp.getName().equals(pName) ) return xp;
      }
      return null;
  }
   
    /** Get the In ports */
    public final XML_Port[] getIn() {
	return iListIn;
    }
    
    /** Get the Out Ports */
    public final XML_Port[] getOut() {
	return iListOut;
    }

    public abstract void generatePort();
 
	 public void setDebug(int d){
		 debug=d;
	 }
}
