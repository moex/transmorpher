/**
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** define the structure for a transmorpher execution
*/

package fr.fluxmedia.transmorpher.engine;

// import JAXP classes

import javax.xml.transform.TransformerException;

//import SAX classes

import org.xml.sax.SAXException;

// import java classes

import java.io.IOException;
import java.util.Enumeration;

import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.TMException;

public class TMain extends TProcessComposite {
    
  public TMain(Parameters pParams) throws TransformerException,SAXException,IOException {
    super(new String[0],new String[0],pParams);
  }

    // I have trouble seeing the difference with what is in TProcessComposite
  public void bindPorts(TProcess process) throws TMException, SAXException {
      
      process.generatePort() ;
      //System.out.println("debut for " + process);
      for( String s: process.getNameOut() ) {
	  if( s.equals("_null_") ) {
	      this.addChannel(new TChannel("_null_",process,null));
	      //      else if(process.getNameOut(i).equals("_stdout_")) {
	  } else {
	      listOfProcessOut.put( s, process );
	      TProcess isSameIn = (TProcess)listOfProcessIn.get( s );
	      if( isSameIn != null ){
		  this.addChannel(new TChannel( s, process, isSameIn));
		  listOfProcessIn.remove( s );
		  listOfProcessOut.remove( s );
	      }
	  }
      }
      //System.out.println("fin for");
      
      for( String s: process.getNameIn() ) {
	//if(process.getNameOut(i).equals("_stdin_")) {
	//} else {
	  //System.out.println(process.getNameIn(i).getCompositeName());
	  listOfProcessIn.put( s, process );
	  TProcess isSameOut =(TProcess)listOfProcessOut.get( s );
	  if(isSameOut!=null){
	      this.addChannel(new TChannel( s, isSameOut, process ));
	      listOfProcessIn.remove( s );
	      listOfProcessOut.remove( s );
	  } 
	  //}
        
    }
    //System.out.println("fin");
}
}
