/**
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
/**
  *
  * This class allow to build the process description
  * And allow the user to start the resolution
  * @author Laurent Tardif (laurent.tardif@inrialpes.fr)
  * @since jdk 1.2 / SAX 2.0
*/
/**
 * Beware: this class really cannot be dispatched within the Graph
 * components because otherwise, the Graph would be necessary at
 * runtime...
 */

package fr.fluxmedia.transmorpher.engine ;

// Imported JAXP classes

import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;

// Imported SAX classes

import org.xml.sax.SAXException;

// Imported Java Classes

import java.io.IOException;
import java.io.InputStream;
import java.util.ListIterator;
import java.util.Vector ;
import java.util.Enumeration ;
import java.util.Hashtable ;

// Imported Transmorpher Classes

import fr.fluxmedia.transmorpher.utils.Parameters ;
import fr.fluxmedia.transmorpher.utils.StringParameters ;
import fr.fluxmedia.transmorpher.utils.SystemResources;
import fr.fluxmedia.transmorpher.utils.LinearIndexedStruct;

public final class TProcessFactory {

  static final int NB_READER_RISE = 100;
  static final int INFINITY = 100000 ;
    
  /** flag for debuging level **/
  static int debug = 0;

  /** flag when a standard output serializer has been created **/
  static boolean testSerializer = false;

   /** flag thread **/
  static boolean useThread = false;
  
  /** flag when a standard input generator has been created **/
  static boolean testReader = false;
    
    //  Vector iGeneratorList = new Vector(NB_READER_RISE,NB_READER_RISE/5);

  /** name of the class of all Transmorpher built-in (and soon other) component types **/
  static LinearIndexedStruct<String> listType = null;
   
  /** The Transformer factory */
  static TransformerFactory tFactory = null;
     
  /** the SAXTransformerFactory */
  static SAXTransformerFactory saxTFactory = null ;
    
  /** Class constructor */
  static {
      tFactory = TransformerFactory.newInstance();
      if (tFactory.getFeature(SAXSource.FEATURE) && tFactory.getFeature(SAXResult.FEATURE)) {
	  // Cast the TransformerFactory to SAXTransformerFactory.
	  saxTFactory = ((SAXTransformerFactory) tFactory);
	  
      }
  } //end static
    
    
    /** The constructor */
    public TProcessFactory (LinearIndexedStruct<String> externs, int d) {
	// Initiate a Hashtable with the list of types found in the
	// types arguments so that the system is able to generate the correct object from the type
	debug = d;
	initFactory(externs);
   }
    
    /** Return the sax factory */
    public static final SAXTransformerFactory getSAXTFactory () {
	return saxTFactory;
    }
  
    public void initFactory(LinearIndexedStruct<String> externs){
	listType = new LinearIndexedStruct<String>();
	// Default merger/dispatchers
	listType.add("broadcast", "fr.fluxmedia.transmorpher.stdlib.Broadcast");
	listType.add("concat", "fr.fluxmedia.transmorpher.stdlib.Concat");
	listType.add("wrap", "fr.fluxmedia.transmorpher.stdlib.Wrap");
	// Default reader/writers
	listType.add("readfile", "fr.fluxmedia.transmorpher.stdlib.ReadFile");
	listType.add("writefile", "fr.fluxmedia.transmorpher.stdlib.WriteFile");
	// These two should be replaced by two different classes...
	listType.add("standardInput", "fr.fluxmedia.transmorpher.engine.TReader");
	listType.add("standardOutput", "fr.fluxmedia.transmorpher.stdlib.StdOut");
	// Default transformations
	listType.add("xslt", "fr.fluxmedia.transmorpher.stdlib.XSLT");
	listType.add("tmq", "fr.fluxmedia.transmorpher.stdlib.TMQuery");
	listType.add("if", "fr.fluxmedia.transmorpher.stdlib.TestingDispatcher");
	// appends externs to listType
	for ( Enumeration<String> keys = externs.getKeys(); keys.hasMoreElements(); ) {
	    //while( keys.hasMoreElements() ) {
	    String key = keys.nextElement();
	    listType.add( key, externs.get(key) );
	}

	if ( debug > 1 ) {
	    for ( String s: listType ) {
		System.err.println( s );
	    }
	}
    }

/* ------------------------------------------------------------------------ */
  /** This method creates a TProcess object corresponding to the type.
   **/

  public static final TProcess newProcess(String type, Object[] params)  {
		TProcess process=null;
      if ( debug > 1 ) 
	  System.err.println("Creating "+type+" process with "+listType.get(type)+" class");
      //for(int i =0; i< params.length;i++)
      //  System.err.println("params["+i+"]="+params[i]);
      
      try {
	  if ( listType.get(type) != null ) type = listType.get(type);
	Class processClass =  Class.forName(type);
	  java.lang.reflect.Constructor[] processConstructors = processClass.getConstructors();
	  if ( debug > 3 ) 
	      System.err.println("Result : "+processConstructors[0].getName());
		process=(TProcess)processConstructors[0].newInstance(params);
		process.setDebug(debug);
      } catch (Exception e) {
	  System.out.println("[TprocessFactory] Error : cannot create Process");
	  System.out.println("[TprocessFactory] "+e);
      }
				  return process;

  }
  
/* ------------------------------------------------------------------------ */
  /**
       Create a new generator.
       ps : may be, the paramer must be a ParserAnswer instead of String and String[]
       
  */
  public final TProcess newGenerator(String[] pOut,
				     String type,
                                     Parameters pParam,
                                     StringParameters staticAttributes){
      // JE: Again this should have already been tested within the graph
      if ( type.equals("standardInput") ) {
          if( testReader ) {
	  System.out.println("[newGenerator] error : only one generator can be"
			     +" connected to the standard input");
	  System.exit(1);
          } else testReader = true; 
      }
      if ( type.equals("") ) type = SystemResources.getProperty("XML_READER");
      Object [] params = {(Object)pOut, (Object)pParam, (Object)staticAttributes};
      return newProcess(type, params);
  } //end proc    

  /* ------------------------------------------------------------------------ */
  public static final TProcess newSerializer(String[] pIn,
					     String type,
                                             Parameters pParam,
                                             StringParameters staticAttributes){
      // JE: Again this should have already been tested within the graph
      if ( type.equals("standardOuput") ) {
          if( testSerializer ) {
	  System.out.println("[newSerializer] error : only one serializer can be"
			     +" connected to the standard output");
	  System.exit(1);
          } else testSerializer = true; 
      }
      if ( type.equals("") ) type = SystemResources.getProperty("XML_SERIALIZER");
      Object [] params = {(Object)pIn, (Object)pParam, (Object)staticAttributes};
      return newProcess(type, params);
  } //end proc

  /* ------------------------------------------------------------------------ */
  /**
     Build a new dispatcher. 
  */
  public static final TProcess newDispatcher(String[] pIn,
                                             String[] pOut,
					     String type,
                                             Parameters pParam) {
      if ( type.equals("") ) type = SystemResources.getProperty("XML_DISPATCHER");
      Object [] params = {(Object)pIn, (Object)pOut, (Object)pParam};
      return newProcess(type, params);
  }
    

  
  /* ------------------------------------------------------------------------ */
    /**
       Build a new Connector.
    */
  public static final TProcess newConnector(String[] pIn,
                                            String[] pOut,
					    String type,
                                            Parameters pParam)  {
      if ( type.equals("") ) type = SystemResources.getProperty("XML_MERGER");
      Object [] params = {(Object)pIn, (Object)pOut, (Object)pParam};
      return newProcess(type, params);
  }

      
  /* ------------------------------------------------------------------------ */
  /**
    Create a new loop
  
  */
  // JE: There is no reason that it has a type...
  // THE SHOULD BE NO GENERIC LOOPS BUT GENERIC ITERATORS
  public static final TProcess newLoop( String[] pIn,
						String[] pOut,
						String[] pBuf,
                                               Parameters pParam,
                                                StringParameters staticAttributes){
						
  /*public final TProcess newLoop(String[] pIn,
                                String[] pOut,
				String type,
                                String pRefOn,
                                int pCount,
                                TProcessComposite pTemplateListx){*/
      //Object [] params = {(Object)pIn, (Object)pOut, (Object)pRefOn,(Object)pCount,(Object)pTemplateList};
      // JE: int cannot be converted into object so I dropped it momentaneously
      //Object [] params = {(Object)pIn, (Object)pOut, (Object)pRefOn,(Object)pTemplateList};
      //String type="repeat";
      Object [] params = {(Object)pIn,(Object)pOut,(Object)pBuf,(Object)pParam,(Object)staticAttributes};
      return newProcess("fr.fluxmedia.transmorpher.engine.TLoop", params);
  }

/* ------------------------------------------------------------------------ */
    /**
    Build a new Transformer according th the given type.
    */
  public static final TProcess newExternal( String[] pIn,
                                            String[] pOut,
					    String type,
                                            Parameters pParam,
					    StringParameters staticAttributes)
{
    if ( type.equals("") ) type = SystemResources.getProperty("XML_EXTERNAL");
    Object [] params = {(Object)pIn, (Object)pOut, (Object)pParam,(Object)staticAttributes};
    return newProcess(type, params);
  }
 
  /* ------------------------------------------------------------------------ */
 /**
    Build a new query. currently only build a internal one. In the next version more complex
    query can be developp.
 */

    public static final TProcess newApplyQuery( String[] pIn,
                                                String[] pOut,
						String type,
                                                Parameters pParam,
                                                StringParameters staticAttributes){
  if ( type.equals("") ) type = SystemResources.getProperty("XML_QUERYENGINE");
  Object [] params = {(Object)pIn, (Object)pOut, (Object)pParam, (Object)staticAttributes};
  return newProcess(type, params);
  }
  
  /* ------------------------------------------------------------------------ */
  // JE: I am pretty sure that this is a process that is never called...
  // There is also no reason that it has a type...

 public static final TProcess newApplyProcess( String[] pIn,
                                               String[] pOut,
					       String type,
                                               Parameters pParam,
                                               StringParameters staticAttributes){
     Object [] params = {(Object)pIn,(Object)pOut,(Object)pParam,(Object)staticAttributes};
     return newProcess("fr.fluxmedia.transmorpher.engine.TApplyProcess", params);
  }
 
  /* ------------------------------------------------------------------------ */
  /** Build an new ruleset process */
  // JE: There is no reason that it has a type...

  public static final TProcess newApplyRuleset(String[] pIn,
                                          String[] pOut,
					  String type,
                                          Parameters pParam,
                                          StringParameters staticAttributes){
     Object [] params = {(Object)pIn,(Object)pOut,(Object)pParam,(Object)staticAttributes};
     return newProcess("fr.fluxmedia.transmorpher.engine.TApplyRuleset", params);
  } //end proc
 
   
  /* ------------------------------------------------------------------------ */
  public static final TIterator newIterator(String name,String type,Parameters pParam){
 	try {
     		Object [] params = {(Object)name,(Object)pParam};
  		if ( listType.get(type) != null ) type = listType.get(type);
		Class processClass =  Class.forName(type);
		java.lang.reflect.Constructor[] processConstructors = processClass.getConstructors();
		if ( debug > 3 ) 
			System.err.println("Result : "+processConstructors[0].getName());
		return (TIterator)processConstructors[0].newInstance(params);
  	} catch (Exception e) {
		System.out.println("[TprocessFactory] Error : cannot create Iterator");
		System.out.println("[TprocessFactory] "+e);
		return (TIterator)null;
  	}
  }
    



  /* ------------------------------------------------------------------------ */
  /** Main process creators */
  /** Create a new Main, Servlet, Transformer
      Note: there is no type here
      - because these two things are Transformations (and not Calls)
      - because we do not expect the users to create new ones...
  */

  public static final TProcess newMain(String[] pIn, String[] pOut, Parameters p){
      try { return new TMain(p);
      } catch (Exception e) {
	  System.out.println("[TprocessFactory] Error : cannot create Process");
	  System.out.println("[TprocessFactory] "+e);
	  return (TProcess)null;
      }
  }
    
    public static final TProcess newServlet(String[] pIn, String[] pOut, Parameters p){
      try { return new TServlet(p);
      } catch (Exception e) {
	  System.out.println("[TprocessFactory] Error : cannot create Process");
	  System.out.println("[TprocessFactory] "+e);
	  return (TProcess)null;
      }
  }
   
  public static final TProcess newTransformer(String[] pIn, String[] pOut, Parameters p){
      // Not implemented
      // JE: The TTransformer should be more closely related to TMain.
      // It is actually an independent class...
      try { return new TMain(p);
      } catch (Exception e) {
	  System.out.println("[TprocessFactory] Error : cannot create Process");
	  System.out.println("[TprocessFactory] "+e);
	  return (TProcess)null;
      }
  }
}

