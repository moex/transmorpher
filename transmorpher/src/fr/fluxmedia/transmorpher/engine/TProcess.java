/**
 * $Id: TProcess.java,v 1.1 2002-11-06 14:08:21 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
  * one of the main classes of the transmorpher. This class
  * represent the main class of all TProcess. It's defines the
  * XML_PORT mechanism
  *
  * @author Laurent.Tardif@inrialpes.fr
  * @since jdk 1.3 / SAX 2.0
  */

package fr.fluxmedia.transmorpher.engine ;

import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.TMRuntimeException;
import fr.fluxmedia.transmorpher.utils.TMException;

import java.util.Hashtable;

import org.xml.sax.SAXException;

public interface TProcess {

    
    /** Get the name of the process */
    public String getName();
    
    /** Set the name of the process*/
    public void setName(String pName) ;
    
    /* set an In port */
    public void setIn(int i,XML_Port pFileIn) ;
    
    /** Set an Out Port */
    public void setOut(int i,XML_Port pFileOut) ;
  
    /** Get an In port */
    public XML_Port getIn(int i) ;
    
    /** Get an Out Port */
    public XML_Port getOut(int i) ;
 
     /** Get an In port */
    public XML_Port getIn(String pName) ;
    
    /** Get an Out Port */
    public XML_Port getOut(String pName) ;
    
    /** Get the In ports */
    public XML_Port[] getIn() ;
  
    /** Get the Out Ports */
    public XML_Port[] getOut() ;
    
    public String[] getNameOut();

    public String[] getNameIn();
    
    public String getNameOut(int i) throws TMException;

    public String getNameIn(int i) throws TMException;

    public void reset() throws TMRuntimeException, TMException, SAXException;
    
    /** pass a set of parameters to a process */
    public void setParameters(Parameters p);
    
    /** returns the parameters of a process */
    public Parameters getParameters();
    
    /** get the parameter value of a process */
    public Object getParameter(String k) ;
 
    public void setParameter(String k, Object o);

    /** bind the parameters of the process to the runtime parameters */
    public void bindParameters( Parameters p ) throws TMRuntimeException, TMException, SAXException;

    public void generatePort();

    public void setFatherName(String name);
		
    public void setDebug(int d);
		
    public String getFatherName();

}
