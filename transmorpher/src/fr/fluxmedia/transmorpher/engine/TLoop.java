/**
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.engine;

// Imported fr.fluxmedia.transmorpher classes

import fr.fluxmedia.transmorpher.engine.LoopHandler;
import fr.fluxmedia.transmorpher.engine.TIterator;

import fr.fluxmedia.transmorpher.stdlib.WriteFile;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
import fr.fluxmedia.transmorpher.utils.TMException;
import fr.fluxmedia.transmorpher.utils.TMRuntimeException;

// Imported java.io classes
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

// Imported TraX classes

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.TransformerHandler;

// Imported SAX classes

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 *  This class represents a repeat process.
 *  A Loop can accept several iterator of different types.
 *
 *@author     Laurent.Tardif@inrialpes.fr
 *@author     Fabien.Triolet@inrialpes.fr
 *@created    5 ao�t 2002
 *@since      jdk 1.3 && SAX 2.0
 */
public final class TLoop extends TProcessComposite {

	/**
	 *Controller Handler
	 */
	LoopManagerHandler managers[];

	/**
	 *In ports of this process
	 */
	String iListLocalIn[] = null;

	/**
	 *Out ports of this process
	 */
	String iListLocalOut[] = null;

	/**
	 *Buf ports of this process
	 */
	String iListLocalBuf[] = null;

	/**
	 *List of inner processes
	 */
	Vector<TProcess> listOfProcess = null;

	/**
	 *  Buf name
	 */
	String[] nameBuf = null;

	/**
	 *  Buf ports of this process
	 */
	XML_Port[] iListBuf = null;

	/**
	 *  true if it's first loop
	 */
	boolean isFirstLoop = true;

	/**
	 *  flag for iterators, if one of them is finished then its value is true
	 */
	boolean stop = false;

	/**
	 *  number of call to manage method
	 */
	int numberOfManagers = 0;

	/**
	 *  counter of loop.
	 */
	int loopCounter = 0;

	/**
	 *  List of iterator
	 */
	Vector<TIterator> iterators;

	/**
	 *  List of inner serializer
	 */
	Vector<TProcess> listOfSerializer;

	/**
	 *  the list of PortProcess buf
	 */
    protected Hashtable<String,TProcess> listOfProcessBuf = null;

	/**
	 *  The constructor
	 *
	 *@param  pIn                                    in port names
	 *@param  pOut                                   out port names
	 *@param  pBuf                                   buffer port names
	 *@param  pParam                                 Description of the Parameter
	 *@param  staticAttributes                       Description of the Parameter
	 *@exception  TransformerException               Description of the Exception
	 *@exception  TransformerConfigurationException  Description of the Exception
	 *@exception  SAXException                       Any SAX exception, possibly wrapping another exception.
	 *@exception  IOException                        Description of the Exception
	 */
	public TLoop(String[] pIn, String[] pOut, String[] pBuf, Parameters pParam, StringParameters staticAttributes)
		 throws TransformerException,
		TransformerConfigurationException,
		SAXException,
		IOException {
		super(pIn, pOut, pParam);
		// managers creation.
		managers = new LoopManagerHandler[nameIn.length];
		for ( int i = 0; i < nameIn.length; i++ ) {
		    managers[i] = new LoopManagerHandler(this);
		}
		listOfProcess = new Vector<TProcess>();
		iterators = new Vector<TIterator>();
		listOfSerializer = new Vector<TProcess>();
		listOfProcessBuf = new Hashtable<String,TProcess>();
		iListBuf = new XML_Port[pBuf.length];
		nameBuf = pBuf;
		// ports creation
		innerGeneratePort(nameIn, nameOut, nameBuf);
	}//end constructor

	/**
	 *  links ports and creates feedbacks.
	 *
	 *@param  process                 The current process to link to the others
	 *@exception  TMRuntimeException  Description of the Exception
	 *@exception  TMException         Description of the Exception
	 *@exception  SAXException        Description of the Exception
	 */
	public void bindPorts(TProcess process) throws TMRuntimeException, TMException, SAXException {
		//store process in order to reset them at the next loop.
		if (isFirstLoop) {
			listOfProcess.add(process);
		}
		process.generatePort();
		for (int i = 0; i < process.getNameOut().length; i++) {
		    String s = process.getNameOut(i);
			if ( s.equals("_null_")) {
				this.addChannel(new TChannel("_null_", process, null));
			} else {
				listOfProcessOut.put( s, process);
				TProcess isSameIn = listOfProcessIn.get( s );
				TProcess isBuf = listOfProcessBuf.get( s );
				//System.out.println("name out "+s+ isSameIn+" "+isBuf);
				if ((isSameIn != null)) {
					//System.out.println("is same in");
					this.addChannel(new TChannel( s, process, isSameIn ));
					listOfProcessIn.remove( s );
					listOfProcessOut.remove( s );
				} else {// may be a composite port or feedback
					for (int i1 = 0; i1 < iListLocalOut.length; i1++) {
						if ((s.equals(iListLocalOut[i1])) && (isBuf != null)) {
							//creates feedback
								//System.out.println("is feedback");

							int position = 0;
							for (int j = 0; j < nameBuf.length; j++) {
								if (nameBuf[j].equals( s )) {
									position = j;
								}
							}
							XML_Port port1 = new XML_Port( s, this, managers[position].buffer, position, XML_Port.IN );
							try {
							    XML_Port.link(process.getOut(i), port1);
							    listOfProcessOut.remove(i);
							} catch (Exception e) {
							    e.printStackTrace();
							}
							XML_Port port2 = new XML_Port( s, this, this, position, XML_Port.OUT );
							//System.out.println("test out "+port2.getName());
							iListOut[position] = port2;
						} //end if
						else if ( s.equals(iListLocalOut[i1]) ) {
							//composite port
							//System.out.println("is composite");
							iListOut[i1] = process.getOut(i);
						}
					}
				}
			}
		}

		for (int i = 0; i < process.getNameIn().length; i++) {
		    String s = process.getNameIn(i);
			listOfProcessIn.put( s, process);
			TProcess isSameOut = listOfProcessOut.get( s);
			TProcess isBuf = listOfProcessBuf.get( s);
			//System.out.println("name in "+ s);
			
			if ((isSameOut != null)||(isBuf != null)) {
				if (isBuf == null) {
					this.addChannel(new TChannel( s, isSameOut, process));
					listOfProcessIn.remove( s);
					listOfProcessOut.remove( s);
				//	System.out.println("is same in");

				} else {

					//creates feedback
					int position = 0;
					for (int j = 0; j < nameBuf.length; j++) {
						if (nameBuf[j].equals( s)) {
							position = j;
						}
					}
					//					System.out.println("is feedback");

					//System.out.println("Position in " + position);
					XML_Port port1 = new XML_Port( s, this, managers[position].buffer, position, XML_Port.IN);
					XML_Port port2;
					try {
						if(isSameOut!=null)
							XML_Port.link(isSameOut.getOut(i), port1);
					
						//listOfProcessBuf.remove( s);

						if (isFirstLoop) {
							port2 = new XML_Port( s, this, this, position, XML_Port.OUT);
							XML_Port.link(port2, process.getIn(i));
						//	System.out.println("test in "+port2.getName());

						}
						listOfProcessIn.remove( s);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} else {// may be a composite port
				for (int i1 = 0; i1 < iListLocalIn.length; i1++) {
					if ( s.equals(iListLocalIn[i1])) {
							//				System.out.println("is composite");

						managers[i1].loopHandler.setInnerHandler((ContentHandler)process.getIn(i).getContent());
						XML_Port port = new XML_Port( s, this, managers[i1], i1, XML_Port.IN);
						iListIn[i1] = port;
					}
				}
			}
		}
	}

	/**
	 *  To setup the Loop manager, the ports of this Loop have to be created before inner
	 *  process ports.
	 * In the case of TLoop local names and global names are the same.
	 *
	 *@param  locIn   local name of in ports
	 *@param  locOut  local name of out ports
	 *@param  locBuf  local name of buf ports
	 */
	public void innerGeneratePort(String[] locIn, String[] locOut, String[] locBuf) {
		//System.out.println("inner generate ports TLoop");
		iListLocalIn = locIn;
		iListLocalOut = locOut;
		iListLocalBuf = locBuf;
		for (int i = 0; i < nameIn.length; i++) {
			String name = locIn[i];
			iListIn[i] = new XML_Port(name, this, i, XML_Port.IN);
			listOfProcessIn.put(name, this);
		}
		for (int i = 0; i < nameOut.length; i++) {
			String name = locOut[i];
			iListOut[i] = new XML_Port(name, this, i, XML_Port.OUT);
			listOfProcessOut.put(name, this);
		}
		for (int i = 0; i < nameBuf.length; i++) {
			String name = locBuf[i];
			iListBuf[i] = new XML_Port(name, this, managers[i], i, XML_Port.IN);
			listOfProcessBuf.put(name, this);
		}

	}

	/**
	 *  Generates all the process ports.In this case, nothing to do , ports have
	 *  been generated before.
	 */
	public void generatePort() {
	}

	/**
	 *  Initializes all inner processes for a new loop. Due to a xalan bug,
	 *  TransformerHandlerImpl can be use only once. So we have to create a new one
	 *  at each loop.
	 *
	 *@exception  TMRuntimeException  Description of the Exception
	 *@exception  TMException         Description of the Exception
	 *@exception  SAXException        Description of the Exception
	 */
	public void reset() throws TMRuntimeException, TMException, SAXException {
	    listOfChannel = new Hashtable<String,TChannel>();
		isFirstLoop = false;
		for ( TProcess process: listOfProcess ) {
			process.reset();
			bindPorts(process);
		}
	}

	/**
	 *  Sets one of the output handler of this loop with the content handler of
	 *  the following process.
	 *
	 *@param  c  the next content handler.
	 *@param  i  manager position.
	 */
	public void setContentHandler(ContentHandler c, int i) {
		managers[i].loopHandler.setOutputHandler((ContentHandler)c);
	}

	/**
	 *  Adds an Iterator to the Loop. If the iterator to add is an instance of fixpointIterator, TestHandler are used in each
	 *  managers of this loop instead of LoopHandler.
	 *
	 *@param  iterator  The iterator to be added to this Loop
	 */
	public void addIterator(TIterator iterator) {
		//System.out.println("ADDED ITERATOR "+iterator);
		iterators.add(iterator);
		if (iterator.getType().equals("fixpoint")) {
		    for ( LoopManagerHandler lmh: managers ) {
				lmh.setTestHandler();
			}
		}
	}

	/**
	 *  Adds a process to the list of process of the TLoop object
	 *
	 *@param  process  The process to be added to the list of process
	 */
	public void addProcess(TProcess process) {
		super.addProcess(process);
		if (process instanceof TSerializer) {
			this.addSerializer(process);
		}
	}

	/**
	 *  Adds a process (TSerializer) to the list of serializer of the TLoop object
	 *
	 *@param  process  The process to be added to the list of serializer
	 */
	public void addSerializer(TProcess process) {
		listOfSerializer.add(process);
	}

	/**
	 *  Gets the ready attribute of the TLoop object
	 *
	 *@return    The ready value
	 */
	private boolean isReady() {
		boolean test = false;

		numberOfManagers++;
		if (numberOfManagers == managers.length) {
			test = true;
		}
		return test;
	}

	/**
	 *  Manages the loop. When all the iterators have called this methods and if
	 *  they all have a next element , a new loop is started . If one of them is
	 *  finished, loop is ended .
	 */
	public void manage() {
		String fileName = "";
		Parameters iteratorParameters = new Parameters();
		try {//maybe try should not be here.
			
			if (isReady()) {
				//all loop have called this method and are ready to start a new loop. Now ,It's time to test iterators values
			    for ( TIterator iterator: iterators ) {
				if (!(iterator.hasMoreElements())) {
				    stop = true;
				} else if (!(iterator.getType().equals("fixpoint"))) {
				    if (iterator.getType().equals("file")) {
					fileName = (String)iterator.nextElement();
				    } else {
					iteratorParameters.setParameter(iterator.getName(), iterator.nextElement());
				    }
				} else {
				    iterator.nextElement();
				}
			    }
			    // testing difference between last input and output. If no difference, stop loop.
				int counter = 0;
				for ( LoopManagerHandler lmh: managers ) {
				    if (lmh.loopHandler instanceof TestHandler) {
					if ( !(((TestHandler)lmh.loopHandler).isDiff()) ) {
					    counter++;
					}
					if (counter == managers.length) {
					    stop = true;
					}
				    }
				}
				if (!stop) {// each managers starts a new loop
					loopCounter++;
					numberOfManagers = 0;
					if (loopCounter != 1) {
						this.reset();
					}
					for ( TProcess process: listOfProcess ) {
					    process.setParameters(iListOfParameters);
					    for ( TIterator iterator: iterators ) {
						String n = fileName;
						// System.out.println("SERIALIZER :"+n);
						if (iterator.getType().equals("file")) {
						    if (!(process instanceof TReader))
							process.setParameter(iterator.getName(), n.substring(fileName.lastIndexOf('/') + 1));
						    else
							process.setParameter("file", fileName);
						} else if (!(iterator.getType().equals("fixpoint"))) {
						    process.setParameters(iteratorParameters);
						}
					    }
					    if (process instanceof TProcessBasic) {
						((TProcessBasic)process).initParameters();
					    }
					}
					for ( LoopManagerHandler lmh: managers ) {
					    //System.out.println("NEW LOOP " + loopCounter);
					    lmh.loopHandler.newLoop();
					}
					for ( TProcess proc: listOfGenerator ) {
					    //System.out.println("GENERATOR :"+fileName);
					    ((TReader)proc).execProcess();
					}
					for ( LoopManagerHandler lmh: managers ) {
					    //System.out.println("FLUSH BUFFER ");
					    lmh.buffer.flushBuffer();
					}
				} else {// end loop
					//System.out.println("END LOOP");
					for ( LoopManagerHandler lmh: managers ) {
					    lmh.loopHandler.endLoop();
					}
				}
			}
		} catch (Exception e) {
			System.out.println("[TLoop manage()] " + e);
			e.printStackTrace();
		}
	}
}// end class

/**
 *  This is the controller handler for a TLoop process
 *
 *@author     fabien.triolet@inrialpes.fr
 *@created    5 ao�t 2002
 */
final class LoopManagerHandler implements ContentHandler {

	/**
	 *  the owner
	 */
	TLoop iOwner = null;

	/**
	 *  the loop handler to manage
	 */
	LoopHandler loopHandler;
	//TestHandler loopHandler;

	/**
	 * a buffer for the output of inner process of the loop
	 */
	BufferingHandler buffer;

	/**
	 *  number of loop
	 */
	int nbLoop;

	/**
	 *  Constructs a new LoopManagerHandler and sets its owner *
	 *
	 *@param  loop  - owner
	 */
	public LoopManagerHandler(TLoop loop) {
		setOwner(loop);
		nbLoop = 0;
		loopHandler = new LoopHandler();
		buffer = new BufferingHandler(this);
	}

	/**
	 * Changes loop handler to test handler.
	 */
	void setTestHandler() {
		loopHandler = null;
		loopHandler = new TestHandler();
	}

	/**
	 *  Sets owner of this LoopManagerHandler
	 *
	 *@param  loop  - owner
	 */
	void setOwner(TLoop loop) {
		iOwner = loop;
	}

	/**
	 *  The startElement command
	 *
	 *@param  ns                The Namespace URI.
	 *@param  localName         The local name without prefix.
	 *@param  name              The local name with prefix.
	 *@param  atts              The attributes attached to the element.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public final void startElement(String ns, String localName, String name, Attributes atts) throws SAXException {
		//System.out.println("LoopManagerHandler startElement "+name);
		loopHandler.startElement(ns, localName, name, atts);
	}

	/**
	 *  The endElement command
	 *
	 *@param  ns                The Namespace URI.
	 *@param  localName         The local name without prefix.
	 *@param  name              The local name with prefix.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public final void endElement(String ns, String localName, String name) throws SAXException {
		//System.out.println(name+"/>");
		loopHandler.endElement(ns, localName, name);
	}

	/**
	 *  The startDocument command
	 *
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public final void startDocument() throws SAXException {
		//System.out.println("LoopManagerHandler startDocument");
		loopHandler.startDocument();
	}

	/**
	 *  The setDocumentLocator command
	 *
	 *@param  locator  An object that can return the location of any SAX document event.
	 */
	public final void setDocumentLocator(Locator locator) {
		loopHandler.setDocumentLocator(locator);
	}

	/**
	 *  The endDocument command
	 *
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public final void endDocument() throws SAXException {
		//System.out.println("LoopManagerHandler endDocument");
		loopHandler.endDocument();
		iOwner.manage();
	}

	/**
	 *  The skippedEntity command
	 *
	 *@param  name              The name of the skipped entity. If it is a parameter entity, the name will begin with '%', and if it is the external DTD subset, it will be the string "[dtd]".
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public final void skippedEntity(java.lang.String name) throws SAXException {
		loopHandler.skippedEntity(name);
	}

	/**
	 *  The processingInstruction command
	 *
	 *@param  target            The processing instruction target.
	 *@param  data              The processing instruction data, or null if none was supplied. The data does not include any whitespace separating it from the target.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public final void processingInstruction(java.lang.String target, java.lang.String data) throws SAXException {
		loopHandler.processingInstruction(target, data);
	}

	/**
	 *  The ignorableWhitespace command
	 *
	 *@param  ch                The characters from the XML document.
	 *@param  start             The start position in the array.
	 *@param  length            The number of characters to read from the array.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public final void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
		loopHandler.ignorableWhitespace(ch, start, length);
	}

	/**
	 *  The characters command
	 *
	 *@param  ch                The characters from the XML document.
	 *@param  start             The start position in the array.
	 *@param  length            The number of characters to read from the array.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public final void characters(char[] ch, int start, int length) throws SAXException {
		/* char[] tmp = new char[length + start];
		 *for (int i = start; i < length + start; i++) {
		 *tmp[i] = ch[i];
		 *}  */
		loopHandler.characters(ch, start, length);
	}

	/**
	 *  The startPrefixMapping command
	 *
	 *@param  prefix            The prefix that was being mapping.
	 *@param  uri               The Namespace URI the prefix is mapped to.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public final void startPrefixMapping(java.lang.String prefix, java.lang.String uri) throws SAXException {
		loopHandler.startPrefixMapping(prefix, uri);
	}

	/**
	 *  The endPrefixMapping command
	 *
	 *@param  prefix            The prefix that was being mapping.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public final void endPrefixMapping(java.lang.String prefix) throws SAXException {
		loopHandler.endPrefixMapping(prefix);
	}
}

