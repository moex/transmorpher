/*
 * $Id: TSerializer.java,v 1.6 2003-02-14 07:37:40 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.engine;

import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
import fr.fluxmedia.transmorpher.utils.TMException;
import fr.fluxmedia.transmorpher.utils.TMRuntimeException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.xml.transform.OutputKeys;

// Imported SAX classes

import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * This class offer a serializer mechanisme. A way to finish the transphormation process
 * bye writing XML output file
 *
 *@author    laurent tardif@inrialpes.fr
 *@author    triolet
 *@since     jdk 1.3 && SAX 2.0
 *@date      10 02 2001
 */

public abstract class TSerializer extends TProcessBasic {


	/**
	 *  Description of the Field
	 */
	public boolean printOut = false;

	/**
	 *  Description of the Field
	 */
	public boolean printNull = false;

	/**
	 * the name of the file to write
	 */
	public String fileName = null;

	/**
	 *  the format of the file
	 */
	public String format = null;

	/**
	 *  the output stream to write
	 */
	protected OutputStream outputStream = null;

	/**
	 *Constructor for the TSerializer object
	 *
	 *@param  pIn                in ports
	 *@param  pParam             the parameters of this component
	 *@param  pStaticAttributes  static attributes of this component
	 */
	public TSerializer(String[] pIn, Parameters pParam, StringParameters pStaticAttributes) {
		super(pIn, new String[0], pParam);
		if (pStaticAttributes.getParameter("file") != null) {
			if (pStaticAttributes.getParameter("file").equals("_null_")) {
				printNull = true;
				printOut = false;
			}
			if (pStaticAttributes.getParameter("file").equals("_stdout_")) {
				printNull = false;
				printOut = true;
			}
		}
		
	}

	/**
	 *  Sets the outputStream attribute of the TSerializer object
	 *
	 *@exception  TMRuntimeException  Description of the Exception
	 *@exception  TMException         Description of the Exception
	 *@exception  SAXException        Description of the Exception
	 */
	public void setOutputStream() throws TMRuntimeException, TMException, SAXException {
		try {
			//super.bindParameters(getParameters()); 
			fileName = (String)getParameters().getParameter("file");
			getParameters().setParameters(param);
			if ((fileName != null) && (fileName.lastIndexOf('$') == -1) && (!printOut) && (!printNull)) {// all parameters have been binded
				if (!(format == null)) {
					fileName = fileName.substring(0, fileName.lastIndexOf('.') + 1) + format;
				}
				outputStream = new FileOutputStream(new File(fileName));
			} else {
				outputStream = System.out;
			}
		} catch (Exception e) {
			throw new TMRuntimeException(e, "[TSerializer] Error setOutputStream() : cannot serialize in " + fileName);
		}
	}

	/**
	 * Binds parameters of this object
	 *
	 *@param  p                       parameters to bind
	 *@exception  TMRuntimeException  Description of the Exception
	 *@exception  TMException         Description of the Exception
	 *@exception  SAXException        Description of the Exception
	 */
	public void bindParameters(Parameters p) throws TMRuntimeException, TMException, SAXException {
		super.bindParameters(p);
		setOutputStream();
	}

	/**
	 *  Gets the contentHandler attribute of the TSerializer object
	 *
	 *@return    The contentHandler value
	 */
	public abstract ContentHandler getContentHandler();

	/**
	 *  Gets the outputStream attribute of the TSerializer object
	 *
	 *@return    The outputStream value
	 */
	public OutputStream getOutputStream() {
		return outputStream;
	}

	/**
	 *  Creates port of this component with the the corresponding handler.
	 * If PrintNull is set to true, port is create with a DefaultHandler
	 * which don't do anything.
	 */
	public void generatePort() {
		String nameI = nameIn[0];
		/*nameFather = null;
		 *if (nameFather != null) {
		 *nameI = nameFather + "-" + nameI;
		 *}  */
		try {
			if (!printNull) {
				iListIn[0] = new XML_Port(nameI, this, getContentHandler(), 0, XML_Port.IN);
			} else {
				iListIn[0] = new XML_Port(nameI, this, new DefaultHandler(), 0, XML_Port.IN);
			}
		} catch (Exception e) {
			System.out.println("[TSerializer] generatePort" + e);
		}//end try
	}

	/**
	 *  Sets printOut flag to true or false
	 *
	 *@param  printOut  true or false
	 */
	public void printStdout(boolean printOut) {
		this.printOut = printOut;
		this.printNull = !printOut;
	}

	/**
	 *  Sets printNull flag to true or false
	 *
	 *@param  printNull  true or false
	 */
	public void printNull(boolean printNull) {
		this.printNull = printNull;
	}
	
	public void initParameters() {
		try{
			super.bindParameters(getParameters());
			setOutputStream();
		}
		catch(Exception e){
		}
	}
}

