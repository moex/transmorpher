/**
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003, 2022.
 *
 * https://gitlab.inria.fr/moex.transmorpher - https://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.engine;

//import JAXP classes

import javax.xml.transform.TransformerException;

//import SAX classes

import org.xml.sax.SAXException;

//import Java classes
import java.io.IOException;
import java.util.Vector;
import java.util.Enumeration;

//import Transmorpher classes

import fr.fluxmedia.transmorpher.utils.TMException;

public class TProcessComponent extends TProcessComposite {
    
  /** the number represent how many times this process has been copied*/
  protected int number = 0;

  /** The list of composite out XML_Port */
  Vector<String> iListCompositeOut = null;

  /** The list of composite in XML_Port */
  Vector<String> iListCompositeIn = null;


   /*public TProcessComponent(){}*/

  public TProcessComponent(String[] in,String[] out)
    throws TransformerException,SAXException,IOException {
      super(in,out,null); //JE ??
    iListCompositeOut = new Vector<String>();
    iListCompositeIn = new Vector<String>();
  }

    public void bindPorts(TProcess process) throws SAXException, TMException {
    process.generatePort();
   
    for(int i=0 ; i < process.getNameOut().length ; i++) {
      //System.out.println("out : "+process.getNameOut(i)());
      listOfProcessOut.put(process.getNameOut(i),process);
      TProcess vSameIn = (TProcess)listOfProcessIn.get(process.getNameOut(i));
      if(vSameIn!=null){
        this.addChannel(new TChannel(process.getNameOut(i),vSameIn,process));
        listOfProcessIn.remove(process.getNameOut(i));
        listOfProcessOut.remove(process.getNameOut(i));
      }
    }
    
    for(int i=0 ; i < process.getNameIn().length ; i++) {
      //System.out.println("out : "+process.getNameIn(i));
      listOfProcessIn.put(process.getNameIn(i),process);
      TProcess vSameOut = (TProcess)listOfProcessOut.get(process.getNameIn(i));
      if(vSameOut!=null){
        this.addChannel(new TChannel(process.getNameIn(i),process,vSameOut));
        listOfProcessIn.remove(process.getNameIn(i));
        listOfProcessOut.remove(process.getNameIn(i));
      }
    }
  }
    
  public int getNumber(){
    return number;
  }

  public void setNumber(int number){
    this.number=number;
  }
    
  public final void InsertCompositeOutPort(String pName) {
    //System.out.println("[ProcessComposite ] "+getName()+".InsertUnlinkedOutCompositePort: name="+pName);
    iListCompositeOut.add(pName);
  }
    
  public final void InsertCompositeInPort(String pName) {
    //System.out.println("[ProcessComposite ] "+getName()+".InsertUnlinkedInCompositePort: name="+pName);
    iListCompositeIn.add(pName);
  }

    /** copy of the process. This is the complex method of the TProcess classes.
  Instead we must copy all the child and linked them correctly. This method
  is call by the copy method of the XMLProcess_impl classes
    */
  
  public TProcess internalCopy() {
    System.out.println("INTERNAL COPY " + getName() + "BUGGGGGGGGGGGGGGGGGGGGG PAS IMPLEMENTE ");
    TProcessComposite vAns = null;
    String name ;
    TProcess vDummy = null;
    String[] vPortName = new String [getOut().length];
  
    return vAns ;
  }
    
  
} //end class
