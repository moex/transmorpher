/**
 * $Id: TestHandler.java,v 1.1 2002-11-06 14:08:21 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.engine;

// Imported TRANSMORPHER CLASSES
import fr.fluxmedia.transmorpher.utils.Fifo;
// Imported SAX CLASSES
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/**
 * Implements the SAX events circulation and buffering in a loop-channel
 * LoopHandler is the handler of a Loop channel.
 * It is a BufferingHandler in two respects:
 * - it takes a buffer of its output (which can be flushed either
 * to the process or to the out);
 * - it takes a buffer of its previous input for the purpose of comparison);
 * NEED A LIFO
 *
 *@author    jerome.euzenat@inrialpes.fr
 *@date       14 07 2002
 *@see       "SAX 2.0 & JDK 1.3"
 */

class TestHandler extends LoopHandler {

	/** The witness stack */
	Fifo wStack = null;

	/** true if the wStack and iStack have been found different (Test) */
	boolean diff = true;// during init diff is true so

	/** true if the wStack and iStack have been found different (Test) */
	boolean isInit = true;

	/** the constructor */
	public TestHandler() {
		// We are in initialization phase: the first input will fill the buffer
		isInit = true;
		// We must answer yes for launching the first iteration
		diff = true;
		// Something must be done for the outhdl
		wStack = new Fifo();
		isCurrent = false;
	}

	/*
	 * The basic manipulation of this Handler it through:
	 * // Creation
	 * l = new TestHandler();
	 * // Connection
	 * // input comes from process z and output goes to handler o2
	 * // the inner process p takes its input from this channel through handler o1
	 * l.setOutputHandler( o2 );
	 * l.setInnerHandler( o1 );
	 * z.setOutputHandler( l );
	 * p.setOutputHandler( l );
	 * ...
	 * // Within the control of the repeat
	 * l.initLoop();
	 * while ( l.isDiff() && ... ){
	 *    l.newLoop();
	 * }
	 * l.endLoop();
	 *
	 */

	/*
	 * Basically, everything is simple... (pattern for TestLoop)
	 * 1) events are stored to wStack
	 * 2) endDocument()
	 *     listen to inputHandler()
	 * 3) events are compared with those in wStack and stored in iStack
	 * 3') isDiff() : tells if input is different from previous input
	 * 4) newLoop()
	 *     flush iStack to processHandler;
	 *     iStack goes to wStack
	 * 5) endLoop()
	 *     flush iStack to outhdl
	 *
	 */

	/**
	 * newLoop() is called by the associated LoopHandler for
	 * signifying that it has processed previous document and is ready
	 * to get a new one (unfortunately, this should be triggered by the
	 * calling process at each loop
	 *
	 *@exception  SAXException  Description of the Exception
	 */
	public void newLoop() throws SAXException {
		outputHandler = processHandler;
		wStack = iStack;
		flushBuffer();
		diff=false;
		iStack = new Fifo();
	}

	/**
	 * endLoop() is called by the calling process after the last loop for
	 * flushing the channel out of the repeat process
	 *
	 *@exception  SAXException  Description of the Exception
	 */
	public void endLoop() throws SAXException {
		outputHandler = outhdl;
		flushBuffer();
	}

	/**
	 * isDiff() is called by the calling process after each Loop for knowing
	 * if the input and output of the channel are different or not
	 *
	 *@return    The diff value
	 */
	public boolean isDiff() {
		return diff;
	}

	/*
	 * ContentHandler methods
	 *
	 */

	/*
	 * ContentHandler methods for TestHandler policy.
	 * There are two ways to implement this:
	 * a) as long as the events are the same:
	 *      compare them, swap them from wStack to iStack
	 * b) if events are the same (i.e., they are always compared):
	 *      swap them from wStack to iStack
	 * Here is implemented option (a) for the reason that as soon as
	 * the stuff is different, it will be different everywhere (there
	 * is no gain at comparing events). This still require empirical evidence
	 *
	 */

	/**
	 * The startElement command
	 *
	 *@param  ns                The Namespace URI.
	 *@param  localName         The local name without prefix.
	 *@param  name              The local name with prefix.
	 *@param  atts              The attributes attached to the element.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void startElement(String ns, String localName, String name, Attributes atts) throws SAXException {
		if (isInit || diff) {
			super.startElement(ns, localName, name, atts);
		} else {
			ContentHandlerInfo chi = (ContentHandlerInfo) wStack.pop();// fifo
			//// I still must compare the atts
			// chi.getAttributes()
			if (chi.getCommand() == ContentHandlerInfo.START_ELEMENT
					 /*&& ns.equals(chi.getStringParameter(0))*/
					 && localName.equals(chi.getStringParameter(1))
					 && name.equals(chi.getStringParameter(2))) {
				add(chi);
			} else {
				diff = true;
				super.startElement(ns, localName, name, atts);
			}
		}
	}

	/**
	 * The endElement command
	 *
	 *@param  ns                The Namespace URI.
	 *@param  localName         The local name without prefix.
	 *@param  name              The local name with prefix.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void endElement(String ns, String localName, String name) throws SAXException {
		if (isInit || diff) {
			super.endElement(ns, localName, name);
		} else {
			ContentHandlerInfo chi = (ContentHandlerInfo) wStack.pop();// fifo
			if (chi.getCommand() == ContentHandlerInfo.END_ELEMENT
					 /*&& ns.equals(chi.getStringParameter(0))*/
					 && localName.equals(chi.getStringParameter(1))
					 && name.equals(chi.getStringParameter(2))) {
				add(chi);
			} else {
				diff = true;
				super.endElement(ns, localName, name);
			}
		}
	}

	/**
	 * The startDocument command
	 *
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void startDocument() throws SAXException {
		if (isInit) {
			super.startDocument();
		} else {
			diff = false;// it is initialized at new here
			ContentHandlerInfo chi = (ContentHandlerInfo) wStack.pop();// fifo
			if (chi.getCommand() == ContentHandlerInfo.START_DOCUMENT) {	
				add(chi);
			} else {
				diff = true;
				super.startDocument();
			}
		}
	}

	/**
	 * The endDocument command
	 *
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void endDocument() throws SAXException {
		if (isInit) {
			super.endDocument();
			isInit = false;
		} else {
			ContentHandlerInfo chi = (ContentHandlerInfo) wStack.pop();// fifo
			if (!diff &&
					(chi.getCommand() == ContentHandlerInfo.END_DOCUMENT)) {
				add(chi);
			} else {
				diff = true;
				super.endDocument();
			}
			isDone = true;
		}
	}

	/**
	 * The setDocumentLocator command
	 *
	 *@param  locator  An object that can return the location of any SAX document event.
	 */
	public void setDocumentLocator(Locator locator) {
		if (isInit || diff) {
			super.setDocumentLocator(locator);
		} else {
			ContentHandlerInfo chi = (ContentHandlerInfo) wStack.pop();// fifo
			//// Locator equality!
			//add(new ContentHandlerInfo(ContentHandlerInfo.SET_DOCUMENT_LOCATOR,locator));
			// chi.getLocatorParameter()
			if (chi.getCommand() == ContentHandlerInfo.SET_DOCUMENT_LOCATOR) {
				add(chi);
			} else {
				diff = true;
				super.setDocumentLocator(locator);
			}
		}
	}

	/**
	 * The skippedEntity command
	 *
	 *@param  name              The name of the skipped entity. If it is a parameter entity, the name will begin with '%', and if it is the external DTD subset, it will be the string "[dtd]".
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void skippedEntity(String name) throws SAXException {
		if (isInit || diff) {
			super.skippedEntity(name);
		} else {
			ContentHandlerInfo chi = (ContentHandlerInfo) wStack.pop();// fifo
			if (chi.getCommand() == ContentHandlerInfo.SKIPPED_ENTITY
					 && name.equals(chi.getStringParameter(0))) {
				add(chi);
			} else {
				diff = true;
				super.skippedEntity(name);
			}
		}
	}

	/**
	 * The processingInstruction command
	 *
	 *@param  target            The processing instruction target.
	 *@param  data              The processing instruction data, or null if none was supplied. The data does not include any whitespace separating it from the target.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void processingInstruction(String target, String data) throws SAXException {
		if (isInit || diff) {
			super.processingInstruction(target, data);
		} else {
			ContentHandlerInfo chi = (ContentHandlerInfo) wStack.pop();// fifo
			if (chi.getCommand() == ContentHandlerInfo.PROCESSING_INSTRUCTION
					 && target.equals(chi.getStringParameter(0))
					 && data.equals(chi.getStringParameter(1))) {
				add(chi);
			} else {
				diff = true;
				super.processingInstruction(target, data);
			}
		}
	}


	/**
	 * The ignorableWhitespace command
	 *
	 *@param  ch                The characters from the XML document.
	 *@param  start             The start position in the array.
	 *@param  length            The number of characters to read from the array.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
		if (isInit || diff) {
			super.ignorableWhitespace(ch, start, length);
		} else {
			ContentHandlerInfo chi = (ContentHandlerInfo) wStack.pop();// fifo
			//// I must compare two char[]
			// chi.getCharParameter()
			if (chi.getCommand() == ContentHandlerInfo.IGNORABLE_WHITESPACE
					 && start == chi.getIntParameter(0)
					 && length == chi.getIntParameter(1)) {
				add(chi);
			} else {
				diff = true;
				super.ignorableWhitespace(ch, start, length);
			}
		}
	}

	/**
	 * The characters command
	 *
	 *@param  ch                The characters from the XML document.
	 *@param  start             The start position in the array.
	 *@param  length            The number of characters to read from the array.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void characters(char[] ch, int start, int length) throws SAXException {
		if (isInit || diff) {
			super.characters(ch, start, length);
		} else {
			ContentHandlerInfo chi = (ContentHandlerInfo) wStack.pop();// fifo
			//// I must compare two char[]
			// chi.getCharParameter()
			if (chi.getCommand() == ContentHandlerInfo.CHARACTERS
					 && start == chi.getIntParameter(0)
					 && length == chi.getIntParameter(1)) {
						 boolean isSameChar = true;
						 for (int i=start; i<length+start; i++) 
							 if (chi.getCharParameter()[i]!=(ch[i]))
								isSameChar=false;
				if (isSameChar)				
					add(chi);
				else{
					diff = true;
					super.characters(ch, start, length);
				}
			} else {
				diff = true;
				super.characters(ch, start, length);
			}
		}
	}

	/**
	 * The startPrefixMapping command
	 *
	 *@param  prefix            The prefix that was being mapping.
	 *@param  uri               The Namespace URI the prefix is mapped to.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void startPrefixMapping(String prefix, String uri) throws SAXException {
		if (isInit || diff) {
			super.startPrefixMapping(prefix, uri);
		} else {
			ContentHandlerInfo chi = (ContentHandlerInfo) wStack.pop();// fifo
			if (chi.getCommand() == ContentHandlerInfo.START_PREFIX_MAPPING
					 && prefix.equals(chi.getStringParameter(0))
					 && uri.equals(chi.getStringParameter(1))) {
				add(chi);
			} else {
				diff = true;
				super.startPrefixMapping(prefix, uri);
			}
		}
	}

	/**
	 * The endPrefixMapping command
	 *
	 *@param  prefix            The prefix that was being mapping.
	 *@exception  SAXException  Any SAX exception, possibly wrapping another exception.
	 */
	public void endPrefixMapping(String prefix) throws SAXException {
		if (isInit || diff) {
			super.endPrefixMapping(prefix);
		} else {
			ContentHandlerInfo chi = (ContentHandlerInfo) wStack.pop();// fifo
			if (chi.getCommand() == ContentHandlerInfo.END_PREFIX_MAPPING
					 && prefix.equals(chi.getStringParameter(0))) {
				add(chi);
			} else {
				diff = true;
				super.endPrefixMapping(prefix);
			}
		}
	}
	
	 /** 
	 * Compute the stack of contentHandlerInfo . Stack is not destroyed.
	 */
	public void flushBuffer() throws SAXException{
		ContentHandlerInfo current = null;
		for (int i=0;i<size();i++){
			current=(ContentHandlerInfo)iStack.get(i);
			current.sendEvent(outputHandler);
			if(current.getCommand()==ContentHandlerInfo.END_DOCUMENT)
				isDone=true;
		}
	}
}

