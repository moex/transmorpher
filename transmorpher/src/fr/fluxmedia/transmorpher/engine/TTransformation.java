/**
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) 2022 INRIA.
 *
 * https://gitlab.inria.fr/moex/transmorpher - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
/**
 * This class allow to build a Transformation from JAXP sources
 *
 *@since     jdk 1.2 / SAX 2.0
 *@author    Jerome Euzenat (Jerome.Euzenat@inrialpes.fr)
 */

package fr.fluxmedia.transmorpher.engine;
import fr.fluxmedia.transmorpher.engine.*;

// Imported transmorpher classes

import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.TMException;
import fr.fluxmedia.transmorpher.utils.TMRuntimeException;

// Imported java.io classes

import java.io.File;
import java.io.IOException;
import java.net.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

// Imported JAXP classes

import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamSource;

// Imported SAX classes
import java.util.Properties;
import org.xml.sax.InputSource;

import org.xml.sax.SAXException;

/**
 * During the parse process an xsl file was generated to handle the define external.
 *So, the external process, is only a transformer
 *
 *@author    triolet
 */
public abstract class TTransformation extends TProcessBasic {

    /**
     * port name in
     */
    String[] In = null;

    /**
     * port name out
     */
    String[] Out = null;

    /**
     * the templates
     */
    Templates templates = null;

    /**
     * The transformer
     */
    TransformerHandler iTransformerHandler = null;

    /**
     * Cache for stylesheets
     */
    private static Map<String,MapEntry> cache = new HashMap<String,MapEntry>();

    protected SAXTransformerFactory tfactory = null;

    /**
     * The file in which the transformation is stored
     */
    // It would be useful to directly feed this Transformer with Templates...
    // This can be found on p24 of current JAXP 1.1 spec
    protected String fileName = null;
    /**
     * the properties for all transformer
     */
    static java.util.Properties Op = null;

    /**
     * The instance  constructor
     *
     *@param  pIn                                    Description of the Parameter
     *@param  pOut                                   Description of the Parameter
     *@param  params                                 Description of the Parameter
     *@exception  TransformerException               Description of the Exception
     *@exception  TransformerConfigurationException  Description of the Exception
     *@exception  SAXException                       Description of the Exception
     *@exception  IOException                        Description of the Exception
     */
    public TTransformation(String[] pIn, String[] pOut, Parameters params)
	throws TransformerException, TransformerConfigurationException,
	       SAXException, IOException {
	super(pIn, pOut, params);
	// This should be used by the factory for creating the class...
	fileName = (String)params.getParameter("file");
    }

    /**
     * Returns file
     *
     *@return    The file value
     */
    public String getFile() { return fileName; }

    /**
     *create The Transformer Handler
     *
     *@return                         Description of the Return Value
     *@exception  TMRuntimeException  Description of the Exception
     */
    public final Transformer initTransformer() throws TMRuntimeException {
	String code = "";
	try {
	    long xslLastModified=0 ;
	    String systemID="";
	    if( fileName != null ){
		if( fileName.startsWith("http://") || fileName.startsWith("https://") ) {
		    URL url = new URL(fileName);
		    URLConnection urlConnect = url.openConnection();
		    xslLastModified=urlConnect.getLastModified();
		    systemID=url.toExternalForm();
		} else {
		    File xsltFile = new File(fileName);
		    xslLastModified = xsltFile.lastModified();
		    systemID=xsltFile.toURI().toURL().toExternalForm();
		}
		code = systemID+"("+fileName+")";
		MapEntry entry = cache.get(fileName);
		if (entry != null )
		    if (xslLastModified > entry.lastModified) entry = null;
		if (entry == null) {
		    InputSource IS = new InputSource(systemID);
		    SAXSource SS = new SAXSource(IS);
		    code = code+"a"+SS;
		    // Create a TransformerHandler for each stylesheet.
		    templates = getTransformerFactory().newTemplates(SS);
		    code = code+"a";
		    entry = new MapEntry(xslLastModified, templates);
		    code = code+"a";
		    cache.put(fileName, entry);
		    code = code+"a";
		}
		templates = entry.templates;
		code = code+"["+templates+"]";
		iTransformerHandler = getTransformerFactory().newTransformerHandler(templates);
		code = code+"+";
		setOutputProperties();
		code = code+"+";
	    } else { // identity
		code = "nullFilaname";
		iTransformerHandler = getTransformerFactory().newTransformerHandler();
		code = code+"+";
		setOutputProperties();
		code = code+"+";
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new TMRuntimeException(e, "TTransformation initialization failled "+code);
	}
	return getTransformer();
    }

    /**
     * Sets output properties for this Transformer
     */
    public void setOutputProperties() {
	if ( Op != null ){
	    getTransformer().setOutputProperties(Op);
	} else {
	    getTransformer().setOutputProperty(javax.xml.transform.OutputKeys.ENCODING, "UTF-8");
	    getTransformer().setOutputProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
	    getTransformer().setOutputProperty(javax.xml.transform.OutputKeys.METHOD, "xml");
	    Op = getTransformer().getOutputProperties();
	}
	/*
	setProperty(javax.xml.transform.OutputKeys.ENCODING, "UTF-8");
	if (Op == null) {
	    Op = getTransformer().getOutputProperties();
	    // Isn't it UTF-8 everywhere?
	    Op.setProperty(javax.xml.transform.OutputKeys.ENCODING, "UTF-8");
	    Op.setProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
	    Op.setProperty(javax.xml.transform.OutputKeys.METHOD, "xml");
	}//end if
	getTransformer().setOutputProperties(Op);
	*/
    }

    /*------------------------    getTransformer getTransfomerHandler ----------------- */
    /**
     * get The Transformer Handler
     *
     *@return    The transformerHandler value
     */
    public final TransformerHandler getTransformerHandler() {
	return iTransformerHandler;
    }

    /**
     * get The Transformer Handler
     *
     *@return    The transformer value
     */
    public final Transformer getTransformer() {
	return iTransformerHandler.getTransformer();
    }

    /**
     * Creates a new TransformerHandler.Used by repeat process at each loop.(Because of a xalan bug, a TransformerHandler can not be used several times.
     */
    public void reset() {
	try {
	    iTransformerHandler = null;
	    if (templates!=null)
		iTransformerHandler = getTransformerFactory().newTransformerHandler(templates);
	    else
		//identity
		iTransformerHandler = getTransformerFactory().newTransformerHandler();
	    setOutputProperties();
	} catch (Exception e) { e.printStackTrace(); }
    }
	
    protected SAXTransformerFactory getTransformerFactory() {
	if (tfactory == null) {
	    //This was for testing XSLTC
	    //String value = "org.apache.xalan.xsltc.trax.TransformerFactoryImpl";
	    //String key = "javax.xml.transform.TransformerFactory";
	    //String value = "org.apache.xalan.processor.TransformerFactoryImpl";
	    //Properties props = System.getProperties();
	    //props.put(key, value);
	    //System.setProperties(props);
	    tfactory = (SAXTransformerFactory)SAXTransformerFactory.newInstance();
	}
	return tfactory;
    }
	
    /**
     * generates ports for a TTranformer
     */
    public void generatePort() {
	//System.out.println("[TTransformation] generatePorts()");
	String nameI = nameIn[0];
	String nameO = nameOut[0];
	/*if(nameFather != null)  {
	 *nameI = nameFather+"-"+nameI;
	 *nameO = nameFather+"-"+nameO;
	 *} */
	iListIn[0] = new XML_Port(nameI, this, iTransformerHandler, 0, XML_Port.IN);
	iListOut[0] = new XML_Port(nameO, this, iTransformerHandler, 0, XML_Port.OUT);
	//System.out.println(nameI+" "+iListIn[0]+" "+nameO+" "+iListOut[0]);
    }

    /**
     *  Description of the Method
     *
     *@param  p                       Description of the Parameter
     *@exception  TMRuntimeException  Description of the Exception
     *@exception  TMException         Description of the Exception
     *@exception  SAXException        Description of the Exception
     */
    public void bindParameters(Parameters p) throws TMRuntimeException, TMException, SAXException {
	super.bindParameters(p);
	if (getParameter("file") != null) {
	    fileName = (String)getParameter("file");
	}
	initTransformer();
	// Then pass them to the transformer instance
	for (Enumeration<String> e = getParameterNames(); e.hasMoreElements(); ) {
	    String key = e.nextElement();
	    getTransformer().setParameter(key, getParameter(key));
	}//end for
    }

    /**
     *  Description of the Method
     */
    public void initParameters() {
	for (Enumeration<String> e = getParameterNames(); e.hasMoreElements(); ) {
	    String key = e.nextElement();
	    getTransformer().setParameter(key, getParameter(key));
	}
	getParameters().setParameters(param);
    }

    /**
     *  Description of the Class
     *
     *@author    triolet
     */
    static class MapEntry {
	long lastModified;
	Templates templates;

	/**
	 *Constructor for the MapEntry object
	 *
	 *@param  lastModified  Description of the Parameter
	 *@param  templates     Description of the Parameter
	 */
	MapEntry(long lastModified, Templates templates) {
	    this.lastModified = lastModified;
	    this.templates = templates;
	}
    }

}//end class

