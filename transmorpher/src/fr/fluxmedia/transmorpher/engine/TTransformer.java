/**
 * $Id: TTransformer.java,v 1.1 2002-11-06 14:08:21 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

 /** The instance  constructor
  
  @throws TransformerException
  @throws TransformerConfigurationException
  @throws SAXException
  @throws IOException
  
  */

package fr.fluxmedia.transmorpher.engine;

// Imported TraX classes
import javax.xml.transform.Transformer;
import javax.xml.transform.Source;
import javax.xml.transform.Result;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.URIResolver;
import javax.xml.transform.ErrorListener;

// Imported java classes
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

// Imported Transmorpher classes
import fr.fluxmedia.transmorpher.utils.Parameters;

abstract class TTransformer extends javax.xml.transform.Transformer {

    Parameters parameters = null;

    // Constructor
    public TTransformer(){
	parameters = new Parameters();
    }

    public void clearParameters(){
    }

    public void setParameter( String name, String value ){
  	parameters.setParameter( name, value );
    }
  
    public abstract void transform( Source in, Result out );
  
    /* This is what should be in the generated code...
  public void transform( StreamSource in, StreamResult out )
  throws TransformerException {
      try {
	  // bind standarad in and output
	  // do the usual thing
      } catch (Exception e) {
          throw new TransformerException(e);
	  // or is it
	  // errorlistener.error(new TransformerException(e));
      }
	// play the transformation
  }
    */

}
