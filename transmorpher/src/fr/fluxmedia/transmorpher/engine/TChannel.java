/**
 * $Id: TChannel.java,v 1.1 2002-11-06 14:08:21 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
   The TChannel is the link between one output and one input
*/

package fr.fluxmedia.transmorpher.engine;

import fr.fluxmedia.transmorpher.utils.TMException;
import org.xml.sax.SAXException;

public class TChannel {
    
  /* the right point of the channel */
  protected Object in = null;
    
  /* the left point of the channel */
  protected Object out = null;
    
  /* the type of the channel */
  protected int type;

  /* the name of the channel*/
  protected String name;

    public TChannel(String name,Object out,Object in) throws SAXException, TMException {
    this.name=name;
    this.in=in;
    this.out=out;
    XML_Port.link((XML_Port)((TProcess)out).getOut(name),(XML_Port)((TProcess)in).getIn(name));
  }
    
  public void setType(int type){
    this.type=type;
  }

  public void setOut(Object out){
    this.out=out;
  }
    
  public void setIn(Object out){
    this.in=in;
  }
    
  public void setName(String name){
    this.name=name;
  }

  public String getName(){
    return this.name;
  }

  public Object getIn(){
    return in;
  }
    
  public Object getOut(){
    return out;
  }

  public int getType(){
    return type;
  }

}
