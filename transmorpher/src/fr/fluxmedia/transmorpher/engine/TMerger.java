/**
 * $Id: TMerger.java,v 1.3 2003-05-28 15:53:27 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.transmorpher.engine;
// Imported transmorpher classes
import fr.fluxmedia.transmorpher.engine.XML_Port;
import fr.fluxmedia.transmorpher.utils.Parameters;
// Imported SAX classes
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

/**
 * This class based on the TProcessBasic allow to define
 * a Connector. A connector is a N -> 1 element.
 * It 's allow to merge a set of ContentHandler flow
 *
 *@author    Laurent Tardif (laurent.tardif@inrialpes.fr)
 *@since     jdk 1.2 / SAX 2.0
 */
public abstract class TMerger extends TProcessBasic {

	/** The list of SAX handlers */
	BufferingHandler[] handlerList = null;

	/** The number of the current handler */
	public int iCurrentHandler = 0;

	/** The output handler*/
	ContentHandler outputHandler = null;
	
	/** The controller handler*/
	ContentHandler managingHandler = null;

	/**
	 *Constructor for the TMerger object
	 *
	 *@param  pIn     in port names
	 *@param  pOut    out port names
	 *@param  pParam  Description of the Parameter
	 *@param  hdl      handler which have to manage the merger
	 */
	public TMerger(String[] pIn, String[] pOut, Parameters pParam, ContentHandler hdl) {
		super(pIn, pOut, pParam);
		managingHandler = hdl;
		handlerList = new BufferingHandler[iNbIn];
		for (int i = 0; i < iNbIn; i++) {
			handlerList[i] = new BufferingHandler(managingHandler);
		}
		// The first current handler is the first one
		handlerList[iCurrentHandler].setCurrent(true);
		setFinished(false);
	}

	/**
	 * Allow to set the content handler on Mixer for out port
	 *
	 *@param  pContentHandler  The new contentHandler value
	 */
	public void setContentHandler(ContentHandler pContentHandler) {
		outputHandler = pContentHandler;
	}
	
	public void reset(){
		iCurrentHandler=0;
		for (int i = 0; i < iNbIn; i++) {
			handlerList[i].setCurrent(false);
			handlerList[i].setDone(false);
		}
		handlerList[iCurrentHandler].setCurrent(true);
	}
	/**
	 * Allow to get the content handler for in port
	 *
	 *@return    The contentHandler value
	 */
	public ContentHandler getContentHandler() {
		return outputHandler;
	}

	/**
	 * Allow to get the content handler for in port
	 *
	 *@return    The managingHandler value
	 */
	public ContentHandler getManagingHandler() {
		return managingHandler;
	}

	/**
	 * return a handler
	 *
	 *@param  p  index of handler in handler list
	 *@return    The handler value
	 */
	public final BufferingHandler getHandler(int p) {
		return handlerList[p];
	}

	/**
	 * when a BufferingHandler is finished,
	 *we execute the stack of the next handler,
	 *and set it active
	 *
	 *@exception  SAXException  Encapsulate a general SAX error or warning. 
	 */
	public final void isFinished() throws SAXException {
		while (iCurrentHandler < iNbIn - 1 && handlerList[iCurrentHandler].isDone()) {
			handlerList[iCurrentHandler++].setCurrent(false);
			handlerList[iCurrentHandler].flushBuffer();
			handlerList[iCurrentHandler].setCurrent(true);
		}
		setFinished(true);
		//end while
		/*
		 *handlerList[iCurrentHandler++].setCurrent(false);
		 *if (iCurrentHandler < iNbIn) {
		 *handlerList[iCurrentHandler].flushBuffer();
		 *handlerList[iCurrentHandler].setCurrent(true);
		 *} //end if
		 */
	}//end IsFinished

	/** 
	 * Generates ports for this component.
	 */
	public void generatePort() {
		//if( nameFather==null ) {
		for (int i = 0; i < iNbIn; i++) {
			iListIn[i] = new XML_Port(nameIn[i], this, getHandler(i), i, XML_Port.IN);
		}
		iListOut[0] = new XML_Port(nameOut[0], this, this, 0, XML_Port.OUT);
		//FT : I don't know if the following code is used, but when we use transmorpher with a compiled process, it fails because
		// of this code. So I removed it.
		/*} else {
		 *for (int i=0;i<iNbIn; i++) {
		 *iListIn[i] = new XML_Port(nameFather+"-"+nameIn[i],this,getHandler(i),i,XML_Port.IN);
		 *}
		 *iListOut[0]= new XML_Port(nameFather+"-"+nameOut[0],this,this,0,XML_Port.OUT);
		 *} //end if  */
	}//end proc

}


