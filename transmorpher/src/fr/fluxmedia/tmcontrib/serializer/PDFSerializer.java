/*
 * $Id: PDFSerializer.java,v 1.15 2003-02-11 15:41:01 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.tmcontrib.serializer;

// Imported fr.fluxmedia.transmorpher

import fr.fluxmedia.transmorpher.engine.TSerializer;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
import fr.fluxmedia.transmorpher.utils.TMException;
import fr.fluxmedia.transmorpher.utils.TMRuntimeException;

// Imported java.io classes

// Imported org.apache.fop

import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.log.Hierarchy;
import org.apache.log.LogTarget;
import org.apache.log.Priority;
import org.apache.log.format.PatternFormatter;
import org.apache.log.output.io.StreamTarget;

// Imported SAX classes

import org.xml.sax.ContentHandler;

import org.xml.sax.SAXException;

/**
 * This class allows transmorpher to produce pdf or ps.
 *  To use this serializer, transmorpher has to know it.
 *  To do this, just add the following declaration of defextern in the process file :
 * <code><p>&lt defextern name="fop" class="fr.fluxmedia.transmorpher.tmcontrib.serializer.PDFSerializer/&gt</p></code>
 * <p>A fop serializer can now be declared in the process file :
 * <code><p>&lt serialize id="outputPDF" type="fop" in="out1" &gt</p>
 * <p>&lt with-param name="file"&gt ../samples/fo-sample/output/${filename}.pdf &lt /with-param &gt</p>
 *<p>&lt with-param name="format"&gt pdf &lt /with-param &gt</p>
 *<p>&lt /serialize &gt</p></code></p>
 *<p>Available formats are :
 * <ul>
 * <li>pdf</li>
 * <li>ps</li>
 * </ul></p>
 * <p>
 *  Compiling: following libs are required
 * <ul>
 * <li>fop.jar</li>
 * <li>logkit jar</li>
 * </ul>
 * Running: following libs are required
 * <ul>
 * <li>fop.jar</li>
 * <li>batik.jar</li>
 * <li>avalon-framework-4.0.jar</li>
 * <li>logkit-1.0.jar</li>
 * </ul>
 * </p>
 *
 *@author    triolet
 */
public final class PDFSerializer extends TSerializer {

	/**
	 * The object which creates pdf,ps with fo
	 */
	protected Driver driver = null;
	
	protected ContentHandler handler = null;


	/**
	 * Constructor for the PDFSerializer object
	 *
	 *@param  pIn                in ports
	 *@param  pParam             the parameters of this component
	 *@param  pStaticAttributes  static attributes of this component
	 */
	public PDFSerializer(String[] pIn, Parameters pParam, StringParameters pStaticAttributes) {
		super(pIn, pParam, pStaticAttributes);

		driver = new Driver();

		MessageHandler.setScreenLogger(new org.apache.avalon.framework.logger.NullLogger());
		driver.setLogger(new org.apache.avalon.framework.logger.NullLogger());
		if (pParam.getParameter("format") != null) {
			format = (String)pParam.getParameter("format");
		}
		driver.setRenderer(Driver.RENDER_PDF);
		if ((format.toLowerCase()).equals("pdf")) {
			driver.setRenderer(Driver.RENDER_PDF);
		}
		if ((format.toLowerCase()).equals("ps")) {
			driver.setRenderer(Driver.RENDER_PS);
		}
	}

	/**
	 *  Gets the contentHandler attribute of the PDFSerializer object
	 *
	 *@return    The contentHandler value
	 */
	public ContentHandler getContentHandler() {
		return handler;
	}

	/**
	 *  Sets the outputStream of this PDFSerializer object
	 *
	 *@exception  TMRuntimeException  Description of the Exception
	 *@exception  TMException         Description of the Exception
	 *@exception  SAXException        Description of the Exception
	 */
	public void setOutputStream() throws TMRuntimeException, TMException, SAXException {
		super.setOutputStream();
		driver.setOutputStream(getOutputStream());
		handler=(ContentHandler)driver.getContentHandler();
	}

}

