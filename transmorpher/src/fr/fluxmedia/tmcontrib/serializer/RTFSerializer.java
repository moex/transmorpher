/**
 * $Id: RTFSerializer.java,v 1.7 2002-11-07 14:31:11 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.tmcontrib.serializer;

// Imported fr.fluxmedia.transmorpher

import fr.fluxmedia.transmorpher.engine.TSerializer;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
import fr.fluxmedia.transmorpher.utils.TMException;
import fr.fluxmedia.transmorpher.utils.TMRuntimeException;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;

// Imported java.io classes

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Properties;

// Imported jfor classes

import org.jfor.jfor.converter.Converter;

// Imported SAX classes

import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * This class allows transmorpher to produce pdf or ps.
 *  To use this serializer, transmorpher has to know it.
 *  To do this, just add the following declaration of defextern in the process file :
 * <code><p>&lt defextern name="rtf" class="fr.fluxmedia.transmorpher.tmcontrib.serializer.RTFSerializer/&gt</p></code>
 * <p>A rtf serializer can now be declared in the process file :
 * <code><p>&lt serialize id="outputRTF" type="rtf" in="out1" &gt</p>
 * <p>&lt with-param name="file"&gt ../samples/fo-sample/output/${filename}.rtf &lt /with-param &gt</p>
 *<p>&lt with-param name="format"&gt pdf &lt /with-param &gt</p>
 *<p>&lt /serialize &gt</p></code></p>
 *<p>Available format is :
 * <ul>
 * <li>rtf</li></ul></p>
 * <p>
 *  Compiling: following jar is required
 * <ul><li>jfor.jar</li></ul></p>
 * <p>Running: following jar is required
 * <ul><li>jfor.jar</li></ul></p>
 *
 *@author    triolet
 */
public final class RTFSerializer extends TSerializer {
	/**
	 * the writer
	 */
	private Writer rtfWriter = null;
	/**
	 * a handler to convert fo to rtf
	 */
	private Converter handler = null;

	/**
	 * Constructor for the RTFSerializer object
	 *
	 *@param  pIn                in ports
	 *@param  pParam             the parameters of this serializer
	 *@param  pStaticAttributes  Description of the Parameter
	 *@exception  SAXException   Any SAX exception, possibly wrapping another exception.
	 *@exception  IOException    Description of the Exception
	 */
	public RTFSerializer(String[] pIn, Parameters pParam, StringParameters pStaticAttributes)
		 throws SAXException, IOException {
		super(pIn, pParam, pStaticAttributes);
		format = "rtf";
	}

	/**
	 *  Gets the contentHandler attribute of the PDFSerializer object
	 *
	 *@return    The contentHandler value
	 */
	public ContentHandler getContentHandler() {
		return (ContentHandler)handler;
	}

	/**
	 * Sets the outputStream attribute of the RTFSerializer object
	 *
	 *@exception  TMRuntimeException  Description of the Exception
	 *@exception  TMException         Description of the Exception
	 *@exception  SAXException        Description of the Exception
	 */
	public void setOutputStream() throws TMRuntimeException, TMException, SAXException {
		super.setOutputStream();
		try {
			rtfWriter = new BufferedWriter(new OutputStreamWriter(getOutputStream()));
			handler = new Converter(rtfWriter, Converter.createConverterOption(new DebugOutputStream(System.err, debug)));
		} catch (Exception e) {
			throw new TMRuntimeException(e, "[RTFSerializer] cannot serialize in " + fileName);
		}
	}

	/**
	 * An utility class to control the outputs of the jFor converter.
	 * Outputs are only visible when debug > 1
	 *
	 *@author    triolet
	 */
	class DebugOutputStream extends FilterOutputStream {
		// TO DO:debug has the same value as the command line argument debug
		int debug;

		/**
		 *Constructor for the DebugOutputStream object
		 *
		 *@param  out  Description of the Parameter
		 *@param  d    Description of the Parameter
		 */
		public DebugOutputStream(OutputStream out, int d) {
			super(out);
			debug = d;
		}

		/**
		 *  Description of the Method
		 *
		 *@param  b                Description of the Parameter
		 *@exception  IOException  Description of the Exception
		 */
		public void write(byte[] b) throws IOException {
			if (debug > 1) {
				super.write(b);
			}
		}

		/**
		 *  Description of the Method
		 *
		 *@param  b                Description of the Parameter
		 *@param  off              Description of the Parameter
		 *@param  len              Description of the Parameter
		 *@exception  IOException  Description of the Exception
		 */
		public void write(byte[] b, int off, int len) throws IOException {
			if (debug > 1) {
				super.write(b, off, len);
			}
		}

		/**
		 *  Description of the Method
		 *
		 *@param  b                Description of the Parameter
		 *@exception  IOException  Description of the Exception
		 */
		public void write(int b) throws IOException {
			if (debug > 1) {
				super.write(b);
			}
		}
	}
}

