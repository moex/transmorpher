/*
 * $Id: SVGSerializer.java,v 1.4 2007-03-05 22:52:46 euzenat Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003, 2007.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.tmcontrib.serializer;

// Imported fr.fluxmedia.transmorpher

import fr.fluxmedia.transmorpher.engine.TSerializer;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
import fr.fluxmedia.transmorpher.utils.TMException;
import fr.fluxmedia.transmorpher.utils.TMRuntimeException;

import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;

import org.apache.batik.dom.svg.SAXSVGDocumentFactory;
import org.apache.batik.dom.svg.SVGDOMImplementation;
import org.apache.batik.transcoder.Transcoder;

import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;

import org.apache.batik.transcoder.image.*;
import org.w3c.dom.*;

import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

/**
 * This class allows transmorpher to produce jpeg from svg file
 *
 *@author    triolet
 */
public final class SVGSerializer extends TSerializer {

	/**
	 * The handler that receives SAX events
	 */
	protected SVGHandler handler = null;

	/**
	 * Creates a SVGSerializer instance
	 *
	 *@param  pIn                     Description of the Parameter
	 *@param  pParam                  Description of the Parameter
	 *@param  pStaticAttributes       Description of the Parameter
	 *@exception  TMRuntimeException  Description of the Exception
	 */
	public SVGSerializer(String[] pIn, Parameters pParam, StringParameters pStaticAttributes) throws TMRuntimeException {
		super(pIn, pParam, pStaticAttributes);
		handler = new SVGHandler(this);
	}


	/**
	 *  This method is called when all SAX events have been received and turned into Document
	 * instance
	 *
	 *@param  doc               Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public void notify(Document doc) throws SAXException {
		try {
			String transcoderFormat = (String)getParameters().getParameter("transcoder");
			if (transcoderFormat == null) {
				transcoderFormat = "JPEG";
			}

			String transcoderName = "org.apache.batik.transcoder.image." + transcoderFormat + "Transcoder";

			Class transcoderClass = Class.forName(transcoderName);
			java.lang.reflect.Constructor[] transcoderConstructors = transcoderClass.getConstructors();
			Transcoder t = (Transcoder)transcoderConstructors[0].newInstance( (Object[])null );
			t.addTranscodingHint(ImageTranscoder.KEY_MEDIA, "print");

			TranscoderInput input = new TranscoderInput(doc);
			TranscoderOutput output = new TranscoderOutput(getOutputStream());

			t.transcode(input, output);
		} catch (Exception ex) {
			throw new SAXException("Exception writing image ", ex);
		}
	}

	/**
	 *  Gets the contentHandler attribute of the SVGSerializer object
	 *
	 *@return    The contentHandler value
	 */
	public ContentHandler getContentHandler() {
		return (ContentHandler)handler;
	}
}

/**
 *  Description of the Class
 *
 *@author    triolet
 */
class SVGHandler extends SAXSVGDocumentFactory {

	private final static String SAX_PARSER
		 = "org.apache.xerces.parsers.SAXParser";
	/**
	 *  Description of the Field
	 */
	protected SVGSerializer owner = null;

	/**
	 *Constructor for the SVGHandler object
	 *
	 *@param  owner  Description of the Parameter
	 */
	protected SVGHandler(SVGSerializer owner) {
		super(SAX_PARSER);
		this.owner = owner;
	}

	/**
	 *  Gets the document attribute of the SVGHandler object
	 *
	 *@return    The document value
	 */
	public Document getDocument() {
		return (this.document);
	}

	/**
	 *  Description of the Method
	 *
	 *@exception  SAXException  Description of the Exception
	 */
	public void startDocument()
		 throws SAXException {
		try {
			// Create SVG Document
			String namespaceURI = SVGDOMImplementation.SVG_NAMESPACE_URI;
			this.document = implementation.createDocument(namespaceURI, "svg", null);
			super.startDocument();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new SAXException("SVGBuilder: startDocument", ex);
		}
	}

	/**
	 *  Description of the Method
	 *
	 *@exception  SAXException  Description of the Exception
	 */
	public void endDocument()
		 throws SAXException {
		try {
			super.endDocument();

			owner.notify(this.document);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new SAXException("SVGBuilder: endDocument", ex);
		}
	}
}

