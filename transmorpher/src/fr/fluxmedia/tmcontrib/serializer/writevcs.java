/**
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA 2022.
 *
 * https://gitlab.inria.fr/moex.transmorpher - https://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.tmcontrib.serializer;

// Imported Transmorpher Classes

import fr.fluxmedia.transmorpher.engine.TSerializer;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
import fr.fluxmedia.transmorpher.utils.TMException;
import fr.fluxmedia.transmorpher.utils.TMRuntimeException;

//Imported JAVA Classes

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Vector;

//Imported javax.mail Classes

import javax.mail.MessagingException;
import javax.mail.internet.MimeUtility;

// Imported SAX Classes

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * This class allows transmorpher to produce vcard of vcal files
 *  To use this serializer, transmorpher has to know it.
 *  To do this, just add the following declaration of defextern in the process file :
 * <code><p>&lt defextern name="vcs" class="fr.fluxmedia.transmorpher.tmcontrib.serializer.writevcs/&gt</p></code>
 * <p>A vcs serializer can now be declared in the process file :
 * <code><p>&lt serialize id="outputVCS" type="vcs" in="out1" &gt</p>
 * <p>&lt with-param name="file"&gt ../samples/output/${filename}.vcs &lt /with-param &gt</p>
 *<p>&lt with-param name="type"&gt vcard &lt /with-param &gt</p>
 *<p>&lt /serialize &gt</p></code></p>
 *<p>Available types are :
 * <ul>
 * <li>vcal</li>
 * <li>vcard</li></ul></p>
 * <p>
 *  Compiling: following jar is required
 * <ul><li>mail.jar</li></ul></p>
 * <p>Running: following jar is required
 * <ul><li>mail.jar</li></ul></p>
 *
 *@author    Charre Bruno
 */
public final class writevcs extends TSerializer {

	/**
	 * a handler to write vcard or vcal
	 */
	protected vcsHandler vcs = null;

	/**
	 * True if the file is a vCard
	 */
	protected boolean card = true;


	/**
	 *Constructor for the writevcs object
	 *
	 *@param  pIn                in ports
	 *@param  pParam             the parameters of this component
	 *@param  pStaticAttributes  static attributes of this component
	 */
	public writevcs(String[] pIn, Parameters pParam, StringParameters pStaticAttributes) {
		super(pIn, pParam, pStaticAttributes);

		if (pParam.getParameter("type") != null) {
			String pCard = (String)pParam.getParameter("type");
			if (pCard.equals("vcard")) {
				card = true;
			} else {
				card = false;
			}
		}
		vcs = new vcsHandler(card);
	}

	/**
	 *  Sets the ouput  of this serializer
	 *
	 *@exception  TMRuntimeException  Description of the Exception
	 *@exception  TMException         Description of the Exception
	 *@exception  SAXException        Description of the Exception
	 */
	public void setOutputStream() throws TMRuntimeException, TMException, SAXException {
		super.setOutputStream();
		vcs.setOutputStream(getOutputStream());
	}

	/**
	 *  Gets the contentHandler attribute of the writevcs object
	 *
	 *@return    The contentHandler value
	 */
	public ContentHandler getContentHandler() {
		return (ContentHandler)vcs;
	}
}

/**
 *  Description of the Class
 *
 *@author    Charre Bruno
 */
final class vcsHandler extends DefaultHandler {

	/**
	 * The OutputStream used by this serializer.
	 */
	protected PrintStream output;

	/**
	 * All the elements which require a Begin/End
	 */
	protected Vector<String> vcsVector = new Vector<String>();

	/**
	 * All the elements which require a Begin/End
	 */
	protected Vector<String> composedVector = new Vector<String>();

	/**
	 * True if we must
	 */
	private boolean composed = false;

	/**
	 * The value of the composed value
	 */
	private String composedValue = "";

	/**
	 * True if we must
	 */
	private boolean encode = false;

	/**
	 * The value of the encoding parameter
	 */
	private String encoding = "";

	/**
	 *Constructor for the vcsHandler object
	 *
	 *@param  card  Description of the Parameter
	 */
	public vcsHandler(boolean card) {
		if (card) {
			vcsVector.add("VCARD");
			composedVector.add("N");
			composedVector.add("ADR");
			composedVector.add("ORG");
		} else {
			vcsVector.add("VCALENDAR");
			vcsVector.add("VEVENT");
			vcsVector.add("VTODO");
		}
	}

	/**
	 * Set the OutputStream where the PDI should be writted.
	 *
	 *@param  out  The new outputStream value
	 */
	public void setOutputStream(OutputStream out) {
		this.output = new PrintStream(out);
	}

	/**
	 * The startElement command
	 *
	 *@param  ns                Description of the Parameter
	 *@param  localName         Description of the Parameter
	 *@param  name              Description of the Parameter
	 *@param  atts              Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public void startElement(String ns, String localName, String name, Attributes atts) throws SAXException {
		String elementName = localName;

		if (elementName.equals("")) {
			elementName = name;
		}
		if (vcsVector.contains(elementName)) {
			output.println("BEGIN:" + elementName);
		} else if (!composed) {
			output.print(elementName);

			// gets element attributes
			if (atts != null) {
				for (int i = 0; i < atts.getLength(); i++) {
					String attName = atts.getLocalName(i);
					if (attName.equals("")) {
						attName = atts.getQName(i);
					}
					String attValue = atts.getValue(i);

					output.print(";" + attName + "=" + attValue);

					if (attName.equals("ENCODING")) {
						encode = true;
						encoding = attValue;
					}
				}
			}//end for
			output.print(":");
			if (composedVector.contains(elementName)) {
				composed = true;
			}
		}//end if
	}


	/**
	 * The endElement command
	 *
	 *@param  ns                Description of the Parameter
	 *@param  localName         Description of the Parameter
	 *@param  name              Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public void endElement(String ns, String localName, String name) throws SAXException {
		String elementName = localName;

		if (elementName.equals("")) {
			elementName = name;
		}

		if (composedVector.contains(elementName)) {
			output.println(composedValue.substring(0, composedValue.length() - 1));
			composed = false;
			composedValue = "";
		} else if (composed) {
			composedValue += ";";
		} else if (vcsVector.contains(elementName)) {
			output.println("END:" + elementName);
		}

		encode = false;
	}

	/**
	 * The characters command
	 *
	 *@param  ch                Description of the Parameter
	 *@param  start             Description of the Parameter
	 *@param  length            Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public void characters(char[] ch, int start, int length) throws SAXException {
		String s = new String(ch, start, length);

		if (encode) {
			s = encoder(s, encoding);
		}
		if (composed) {
			composedValue += s;
		} else {
			output.println(s);
		}
	}

	/**
	 *  Description of the Method
	 *
	 *@param  s         Description of the Parameter
	 *@param  encoding  Description of the Parameter
	 *@return           Description of the Return Value
	 */
	private String encoder(String s, String encoding) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		try {
			OutputStream encodedStream = MimeUtility.encode(baos, encoding.toLowerCase());
			encodedStream.write(s.getBytes());
		} catch (MessagingException me) {
			me.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return baos.toString();
	}
}

