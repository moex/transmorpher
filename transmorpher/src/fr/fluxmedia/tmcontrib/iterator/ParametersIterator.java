/*
 * $Id: ParametersIterator.java,v 1.1 2002-11-13 15:43:41 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.tmcontrib.iterator;

// Imported Transmorpher classes

import fr.fluxmedia.transmorpher.engine.TIterator;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
import java.util.Enumeration;

// Imported Java classes

import java.util.NoSuchElementException;

/**
 *  An iterator which returns a new parameter value 
 *
 *@author    triolet
 */
public final class ParametersIterator implements TIterator {

	private Parameters param;
	private String name = null;
	private String type = "string";
	private Enumeration paramNames = null;

	/**
	 *The constructor
	 *
	 *@param  n  Description of the Parameter
	 *@param  p  Description of the Parameter
	 */
	public ParametersIterator(String n, Parameters p) {
		param = p;
		name = n;
		init(p);
	}

	/**
	 *Static initialization is currently not used
	 *
	 *@param  p  Description of the Parameter
	 */
	public void staticInit(Parameters p) {
	}

	/**
	 * The initializer
	 *
	 *@param  p  Description of the Parameter
	 */
	public void init(Parameters p) {
		param = p.bindParameters(param);
		paramNames = param.getNames();
	}

	/**
	 *  Gets the name attribute of the ParametersIterator object
	 *
	 *@return    The name value
	 */
	public String getName() {
		return name;
	}

	/**
	 *Is this the last value
	 *
	 *@return    Description of the Return Value
	 */
	public final boolean hasMoreElements() {
		return (boolean)paramNames.hasMoreElements();
	}

	/**
	 * The next parameter value
	 *
	 *@return    Description of the Return Value
	 */
	public final Object nextElement() {
		return (Object)param.getParameter((String)paramNames.nextElement());
	}

	/**
	 *  Gets the type attribute of the ParametersIterator object
	 *
	 *@return    The type value
	 */
	public String getType() {
		return type;
	}
}//end class

