/*
 * $Id: readvcs.java,v 1 2002/05/24 11:02:30
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.tmcontrib.generator;

// Imported TRANSMORPHER Classes
import fr.fluxmedia.transmorpher.engine.TReader;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
//Imported JAVA classes
import java.io.IOException;
import java.net.*;
// Imported SAX Classes
import org.xml.sax.SAXException;

/**
 *  Read a vcal or vcard (specify by the type parameter) file which coordinates (local path or URL) are given in the
 * file parameter of the generate call, parse it, process it through vcsReader
 * and generate a SAX event flow to the next handler
 *
 *@author    Bruno Charre
 */
public class readvcs extends TReader {

	/**
	 * Is the file a vCard ?
	 */
	boolean card = true;


	/**
	 * Constructor for the readvcs object.
	 *
	 *@param  pOut               out port of this generator
	 *@param  pParam             parameters of this generator
	 *@param  pStaticAttributes  Description of the Parameter
	 *@exception  SAXException   can wrap others exceptions
	 *@exception  IOException    if an IO operations failed
	 */
	public readvcs(String[] pOut, Parameters pParam, StringParameters pStaticAttributes) throws SAXException, IOException {
		super(pOut, pParam, pStaticAttributes);
		if (pParam.getParameter("type") != null) {
			String pCard = (String)pParam.getParameter("type");
			if (pCard.equals("vcard")) {
				card = true;
			} else {
				card = false;
			}
		}
		iReader = new vcsReader(card);
	}

	/**
	 * Creates an input source for the parser with the name of the file and starts
	 * the parsing
	 *
	 *@exception  IOException   if an IO operations failed
	 *@exception  SAXException  can wrap others exceptions
	 */
	public void read() throws IOException, SAXException {
		try {
			iReader.parse(getInputSource());
		} catch (Exception e) {
		}
	}
}

