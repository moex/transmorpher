/*
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA 2022.
 *
 * https://gitlab.inria.fr/moex.transmorpher - https://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.tmcontrib.generator;

//Imported JAVA classes
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;
import java.util.Vector;
import javax.mail.internet.MimeUtility;

// Imported SAX Classes
import org.xml.sax.ContentHandler;
import org.xml.sax.DTDHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.AttributesImpl;

/**
 *  Description of the Class
 *
 *@author    Bruno Charre
 */

class vcsReader extends AbstractXMLReader {

	private final static String NAMESPACE = "http://co4.inrialpes.fr/xml/pimlib/vcs";

	private final static String[] NElement = {"Family", "Given", "Other", "Prefix", "Suffix"};
	private final static String[] ADRElement = {"Pobox", "Extadd", "Street", "Locality", "Region", "Pcode", "Country"};
	private final static String[] ORGElement = {"Orgname", "Orgunit"};

	/**
	 *  Description of the Field
	 */
	protected Vector<String> attributesVALUE = new Vector<String>();
	/**
	 *  Description of the Field
	 */
	protected Vector<String> attributesENCODING = new Vector<String>();

	/**
	 * The type of the file
	 */
	protected String vcsString = "";


	/**
	 *Constructor for the vcsReader object
	 *
	 *@param  card  Description of the Parameter
	 */
	public vcsReader(boolean card) {
		super();

		attributesVALUE.add("INLINE");
		attributesVALUE.add("URL");
		attributesVALUE.add("CONTENT-ID");
		attributesVALUE.add("CID");
		attributesENCODING.add("7BIT");
		attributesENCODING.add("8BIT");
		attributesENCODING.add("QUOTED-PRINTABLE");
		attributesENCODING.add("BASE64");

		if (card) {
			vcsString = "VCARD";
		} else {
			vcsString = "VCALENDAR";
		}
	}

	/**
	 *  Parse a file or an URL which is specified by an input source
	 *
	 *@param  input             the input source to parse
	 *@exception  IOException   Description of the Exception
	 *@exception  SAXException  Description of the Exception
	 */
	public void parse(InputSource input) throws IOException, SAXException {
		// throw(new SAXException("Cannot parse from InputSource"));
		InputStreamReader is = null;
		BufferedReader in = null;

		is = new InputStreamReader(new BufferedInputStream(input.getByteStream()));
		in = new BufferedReader(is);

		ContentHandler handler = getContentHandler();
		AttributesImpl atts;

		Stack<String> myStack = new Stack<String>();

		String line = "";
		String elementName = "";
		String elementValue = "";
		String attributes = "";
		String attributeName = "";
		String attributeValue = "";

		boolean decode = false;
		String encoding = "";

		while ((line = in.readLine()) != null) {

			if (!line.trim().equals("")) {
				while (line.endsWith("=")) {
					line = line.substring(0, line.length() - 1) + in.readLine();
				}

				if (line.startsWith(" ")) {
					if (decode) {
						line = decoder(line, encoding);
					}
					handler.characters(line.toCharArray(), 0, line.length());
					continue;
				}

				int index = line.indexOf(":");

				/* If there is an element  */
				if (index != -1) {
					decode = false;
					if (!elementName.equals("")) {
						handler.endElement("", elementName, elementName);
						elementName = "";
					}
					elementName = line.substring(0, index);
					elementValue = line.substring(index + 1);
					atts = new AttributesImpl();

					if (elementName.equals("BEGIN")) {
						elementName = elementValue;
						myStack.push(elementName);
						if (elementName.equals(vcsString)) {
							handler.startDocument();
						}
						handler.startElement("", elementName, elementName, atts);
						elementName = "";

					} else if (elementName.equals("END")) {
						elementName = elementValue;
						String inStack = myStack.pop();
						if (elementName.equals(inStack)) {
							handler.endElement("", elementName, elementName);
						} else {
							System.err.println("mismatch " + elementName + " expected " + inStack);
						}
						if (elementName.equals(vcsString)) {
							handler.endDocument();
						}
						elementName = "";

					} else {
						int indexE = elementName.indexOf(";");

						/* if there is some attributes  */
						if (indexE != -1) {
							attributes = elementName.substring(indexE + 1) + ";";
							elementName = elementName.substring(0, indexE);

							while ((indexE = attributes.indexOf(";")) != -1) {
								attributeValue = attributes.substring(0, indexE);
								attributes = attributes.substring(indexE + 1);

								int indexA = attributeValue.indexOf("=");
								if (indexA != -1) {
									attributeName = attributeValue.substring(0, indexA);
									attributeValue = attributeValue.substring(indexA + 1);
									if (attributeName.equals("ENCODING")) {
										decode = true;
										encoding = attributeValue;
									}
								} else if (attributesVALUE.contains(attributeValue)) {
									attributeName = "VALUE";
								} else if (attributesENCODING.contains(attributeValue)) {
									attributeName = "ENCODING";
									decode = true;
									encoding = attributeValue;
								} else {
									attributeName = "TYPE";
								}

								atts.addAttribute("", "", attributeName, "CDATA", attributeValue);
							}
						}

						handler.startElement("", elementName, elementName, atts);

						if (!elementValue.equals("")) {
							if (decode) {
								elementValue = decoder(elementValue, encoding);
							}

							if (elementName.equals("N")) {
								String text = elementValue + ";";
								int i = 0;
								while ((indexE = text.indexOf(";")) != -1) {
									atts = new AttributesImpl();
									handler.startElement("", NElement[i], NElement[i], atts);
									elementValue = text.substring(0, indexE);
									handler.characters(elementValue.toCharArray(), 0, elementValue.length());
									handler.endElement("", NElement[i], NElement[i]);

									text = text.substring(indexE + 1);
									i++;
								}
							} else if (elementName.equals("ADR")) {
								String text = elementValue + ";";
								int i = 0;
								while ((indexE = text.indexOf(";")) != -1) {
									atts = new AttributesImpl();
									handler.startElement("", ADRElement[i], ADRElement[i], atts);
									elementValue = text.substring(0, indexE);
									handler.characters(elementValue.toCharArray(), 0, elementValue.length());
									handler.endElement("", ADRElement[i], ADRElement[i]);

									text = text.substring(indexE + 1);
									i++;
								}
							} else if (elementName.equals("ORG")) {
								String text = elementValue + ";";
								int i = 0;
								while ((indexE = text.indexOf(";")) != -1) {
									atts = new AttributesImpl();
									if (i > 1) {
										i = 1;
									}
									handler.startElement("",  ORGElement[i], ORGElement[i], atts);
									elementValue = text.substring(0, indexE);
									handler.characters(elementValue.toCharArray(), 0, elementValue.length());
									handler.endElement("",  ORGElement[i], ORGElement[i]);

									text = text.substring(indexE + 1);
									i++;
								}
							} else {
								handler.characters(elementValue.toCharArray(), 0, elementValue.length());
							}
						}
					}

				} else {
					throw (new IOException("MalformedVcs"));
				}
			}
		}
		in.close();
	}

	/**
	 *  Decodes a string with specified encoding
	 *
	 *@param  elementValue  the string to decode
	 *@param  encoding      the specified encoding
	 *@return               the decoded string
	 */
	private String decoder(String elementValue, String encoding) {
		BufferedReader br;
		String line;
		String result = "";

		try {
			br = new BufferedReader(new InputStreamReader(
																				MimeUtility.decode(new ByteArrayInputStream(
																				elementValue.getBytes()), encoding.toLowerCase())));
			while ((line = br.readLine()) != null) {
				result += "\n" + line;
			}
		} catch (Exception e) {
			System.err.println("[DECODER]:" + e);
			e.printStackTrace();
		}
		return result;
	}
}

