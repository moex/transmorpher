/*
 * $Id: ReadCSV.java,v 1.6 2003-02-11 15:41:01 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.tmcontrib.generator;

// Imported TRANSMORPHER Classes
import fr.fluxmedia.transmorpher.engine.TReader;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
//Imported JAVA classes
import java.io.IOException;
// Imported SAX Classes
import org.xml.sax.SAXException;

/**
 * Read a CSV (comma separated value) file which coordinates (local path or URL) are given in the
 * file parameter of the generate call, parse it, process it through CSVXMLReader
 * and generate a SAX event flow to the next handler
 *
 *
 *@author    triolet
 */
public class ReadCSV extends TReader {

	/**
	 *Constructor for the readCSV object
	 *
	 *@param  pOut               The out port of this generator
	 *@param  pParam             The parameters of this generator
	 *@param  pStaticAttributes  The parameters of this generator
	 *@exception  SAXException   an exception that an wrap others exceptions
	 *@exception  IOException    if an I/O operation failed
	 */
	public ReadCSV(String[] pOut, Parameters pParam, StringParameters pStaticAttributes) throws SAXException, IOException {
		super(pOut, pParam, pStaticAttributes);
		iReader = new CSVXMLReader(((String)pParam.getParameter("separator")));

	}

	/**
	 * Creates an input source for the parser with the name of the file and starts
	 * the parsing
	 *
	 *@exception  IOException   if an I/O operation failed
	 *@exception  SAXException  an exception that an wrap others exceptions
	 */
	public void read() throws IOException, SAXException {
		try {
				((CSVXMLReader)iReader).setSeparator(((String)getParameter("separator")));
				iReader.parse(getInputSource());
		} catch (Exception e) {
		}
	}
}

