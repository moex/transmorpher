/*
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2022.
 *
 * https://gitlab.inria.fr/moex.transmorpher - https://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.tmcontrib.generator;
// imported java classes
import java.io.IOException;
import java.util.*;
// imported SAX2 classes
import org.xml.sax.*;

/**
 * This abstract class implements SAX2 interface XMLReader. It aims to
 * allow the sub-classes to behave like XMLReader implementations.
 *  
 * This class is defined in the following book "Java and XSLT" by Eric M. Burke (O'Reilly)
 *@author    triolet
 */
public abstract class AbstractXMLReader implements XMLReader {

    private Map<String,Boolean> featureMap = new HashMap<String,Boolean>();
    private Map<String,Object> propertyMap = new HashMap<String,Object>();
	private EntityResolver entityResolver;
	private DTDHandler dtdHandler;
	private ContentHandler contentHandler;
	private ErrorHandler errorHandler;

	/**
	 * Sub-classes have to implement this method in order to produce SAX2 events.
	 *
	 *@param  input             The InputSource to parse.
	 *@exception  IOException   if the input has not been found
	 *@exception  SAXException  an exception which can wrap others exceptions
	 */
	public abstract void parse(InputSource input) throws IOException, SAXException;

	/**
	 *  Gets the feature attribute of the AbstractXMLReader object
	 *
	 *@param  name                           The feature name
	 *@return                                The feature value
	 *@exception  SAXNotRecognizedException  if the feature is not recognized
	 *@exception  SAXNotSupportedException   if the feature is not supported
	 */
	public boolean getFeature(String name) throws SAXNotRecognizedException, SAXNotSupportedException {
		Boolean featureValue = featureMap.get(name);
		return (featureValue == null) ? false
			 : featureValue.booleanValue();
	}

	/**
	 *  Sets the feature attribute of the AbstractXMLReader object
	 *
	 *@param  name                           The new feature name
	 *@param  value                          The new feature value
	 *@exception  SAXNotRecognizedException  if the feature is not recognized
	 *@exception  SAXNotSupportedException   if the feature is not supported
	 */
	public void setFeature(String name, boolean value) throws SAXNotRecognizedException, SAXNotSupportedException {
		featureMap.put( name, new Boolean(value) );
	}

	/**
	 *  Gets the property attribute of the AbstractXMLReader object
	 *
	 *@param  name                           The property name
	 *@return                                The property value
	 *@exception  SAXNotRecognizedException  if the property is not recognized
	 *@exception  SAXNotSupportedException   if the property is not supported
	 */
	public Object getProperty(String name) throws SAXNotRecognizedException, SAXNotSupportedException {
		return propertyMap.get(name);
	}

	/**
	 *  Sets the property attribute of the AbstractXMLReader object
	 *
	 *@param  name                           The new property name
	 *@param  value                          The new property value
	 *@exception  SAXNotRecognizedException  if the property is not recognized
	 *@exception  SAXNotSupportedException   if the property is not supported
	 */
	public void setProperty(String name, Object value) throws SAXNotRecognizedException, SAXNotSupportedException {
		propertyMap.put(name, value);
	}

	/**
	 *  Sets the entityResolver attribute of the AbstractXMLReader object
	 *
	 *@param  entityResolver  The new entityResolver value
	 */
	public void setEntityResolver(EntityResolver entityResolver) {
		this.entityResolver = entityResolver;
	}

	/**
	 *  Gets the entityResolver attribute of the AbstractXMLReader object
	 *
	 *@return    The entityResolver value
	 */
	public EntityResolver getEntityResolver() {
		return entityResolver;
	}

	/**
	 *  Sets the dTDHandler attribute of the AbstractXMLReader object
	 *
	 *@param  dtdHandler  The new dTDHandler value
	 */
	public void setDTDHandler(DTDHandler dtdHandler) {
		this.dtdHandler = dtdHandler;
	}

	/**
	 *  Gets the dTDHandler attribute of the AbstractXMLReader object
	 *
	 *@return    The dTDHandler value
	 */
	public DTDHandler getDTDHandler() {
		return dtdHandler;
	}

	/**
	 *  Sets the contentHandler attribute of the AbstractXMLReader object
	 *
	 *@param  contentHandler  The new contentHandler value
	 */
	public void setContentHandler(ContentHandler contentHandler) {
		this.contentHandler = contentHandler;
	}

	/**
	 *  Gets the contentHandler attribute of the AbstractXMLReader object
	 *
	 *@return    The contentHandler value
	 */
	public ContentHandler getContentHandler() {
		return contentHandler;
	}

	/**
	 *  Sets the errorHandler attribute of the AbstractXMLReader object
	 *
	 *@param  errorHandler  The new errorHandler value
	 */
	public void setErrorHandler(ErrorHandler errorHandler) {
		this.errorHandler = errorHandler;
	}

	/**
	 *  Gets the errorHandler attribute of the AbstractXMLReader object
	 *
	 *@return    The errorHandler value
	 */
	public ErrorHandler getErrorHandler() {
		return errorHandler;
	}

	/**
	 *  Parses the file represented by the systemId
	 *
	 *@param  systemId          The systemID of the file to parse
	 *@exception  IOException   if the input has not been found
	 *@exception  SAXException  an exception which can wrap others exceptions
	 */
	public void parse(String systemId) throws IOException, SAXException {
		parse(new InputSource(systemId));
	}
}

