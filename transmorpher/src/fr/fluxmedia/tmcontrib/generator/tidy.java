/**
 * $Id: tidy.java,v 1.9 2002-11-25 16:09:11 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.tmcontrib.generator;

// Imported Transmorpher classes

import fr.fluxmedia.transmorpher.engine.TReader;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
import fr.fluxmedia.transmorpher.utils.TMRuntimeException;

// Imported Java classes

import java.io.IOException;
import java.io.PrintWriter;

// Imported JAXP classes
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXResult;

// Imported DOM classes

import org.w3c.dom.Node;

// Imported Tidy class

import org.w3c.tidy.Tidy;

// Imported SAX classes

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.TransformerFactory;


/**
 * read a HTML file which coordinates (local path or URL) is give in the
 * file parameter of the generate call, parse it, process it through TIDY
 * and generate a SAX event flow to the next handler
 *
 *@author    J�r�me.Euzenat@inrialpes.fr
 *@author    Fabien.Triolet@inrialpes.fr
 */

public final class tidy extends TReader {
	
	protected SAXTransformerFactory tfactory = null;

	/**
	 *Constructor for the tidy object
	 *
	 *@param  pOut               Description of the Parameter
	 *@param  pParam             Description of the Parameter
	 *@param  pStaticAttributes  Description of the Parameter
	 *@exception  SAXException   Description of the Exception
	 *@exception  IOException    Description of the Exception
	 */
	public tidy(String[] pOut, Parameters pParam, StringParameters pStaticAttributes) throws SAXException, IOException {

		super(pOut, pParam, pStaticAttributes);
		try {
			SAXParserFactory parserFactory = SAXParserFactory.newInstance();

			parserFactory.setFeature(NAMESPACES_FEATURE_ID, DEFAULT_NAMESPACES);
			parserFactory.setFeature(NAMESPACES_PREFIXES_FEATURE_ID, DEFAULT_NAMESPACES_PREFIXES);
			parserFactory.setFeature(VALIDATION_FEATURE_ID, DEFAULT_VALIDATION);
			parserFactory.setFeature(SCHEMA_VALIDATION_FEATURE_ID, DEFAULT_SCHEMA_VALIDATION);

			iReader = (parserFactory.newSAXParser()).getXMLReader();
		} catch (Exception e) {
			System.out.println("[tidy]Exception :" + e);
		}//end try
	}

	/**
	 *  Description of the Method
	 *
	 *@exception  IOException         I/O error in either
	 *@exception  SAXException        Parsing error
	 *@exception  TMRuntimeException  Tidy processing error
	 */
	public void read() throws IOException, SAXException, TMRuntimeException {
		Tidy cleaner = new Tidy();
		cleaner.setErrout(new PrintWriter(System.err));
		Node document = cleaner.parseDOM(getInputStream(), null);
		Transformer transformer = null;
		try {
			tfactory = (SAXTransformerFactory)TransformerFactory.newInstance();
			transformer = tfactory.newTransformer();
		} catch (TransformerConfigurationException e) {
			throw new TMRuntimeException(e, "Cannot create SAX transformer");
		}
		DOMSource source = new DOMSource(document);
		source.setSystemId(iFilename);
		ContentHandler handler = iReader.getContentHandler();
		SAXResult result = new SAXResult(handler);
		try {
			transformer.transform(source, result);
		} catch (TransformerException e) {
			throw new TMRuntimeException(e, "Cannot process Tidy on " + iFilename);
		}
	}
}

