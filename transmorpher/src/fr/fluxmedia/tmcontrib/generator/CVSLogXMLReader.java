/*
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA 2007, 2022.
 *
 * https://gitlab.inria.fr/moex.transmorpher - https://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.tmcontrib.generator;

//imported java classes
import java.io.*;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;

//imported SAX2 classes
import org.xml.sax.*;
import org.xml.sax.helpers.*;

/**
 *  This class allows to parse a cvs log file.
 *
 *@author    triolet
 */
public class CVSLogXMLReader extends AbstractXMLReader {

	private final static Attributes EMPTY_ATTR = new AttributesImpl();

	/**
	 *Constructor for the CVSLogXMLReader object
	 */
	public CVSLogXMLReader() { }

	/**
	 *  Parses the input source
	 *
	 *@param  input             input source to parse
	 *@exception  IOException   Description of the Exception
	 *@exception  SAXException  Description of the Exception
	 */
	public void parse(InputSource input) throws IOException, SAXException {
		ContentHandler ch = getContentHandler();
		if (ch == null) {
			return;
		}

		BufferedReader br = null;
		if (input.getCharacterStream() != null) {
			br = new BufferedReader(input.getCharacterStream());
		} else if (input.getByteStream() != null) {
			br = new BufferedReader(new InputStreamReader(input.getByteStream()));
		} else if (input.getSystemId() != null) {
			java.net.URL url = new URL(input.getSystemId());
			br = new BufferedReader(new InputStreamReader(url.openStream()));
		} else {
			throw new SAXException("bad InputSource Object");
		}

		ch.startDocument();
		//<cvslog>
		ch.startElement("", "cvslog", "cvslog", EMPTY_ATTR);

		String curLine = null;
		while ((curLine = br.readLine()) != null) {
			if (curLine.startsWith("RCS")) {
				parseHeader(br, ch);
			}
		}
		//</cvslog>
		ch.endElement("", "cvslog", "cvslog");
		ch.endDocument();
	}

	/**
	 *  Parses the header of the cvs log file
	 *
	 *@param  br                the buffered reader
	 *@param  ch                the content handler which receives SAX events
	 *@exception  IOException   Description of the Exception
	 *@exception  SAXException  Description of the Exception
	 */
	public void parseHeader(BufferedReader br, ContentHandler ch) throws IOException, SAXException {
		String curLine;
		AttributesImpl attrs = new AttributesImpl();
		Hashtable<String,String> symbolicNames = new Hashtable<String,String>();
		while ((curLine = br.readLine()) != null) {
			if (curLine.startsWith("----")) {
				break;
			} else {
				if (curLine.startsWith("Working")) {
					attrs.addAttribute("", "workingfile", "workingfile", "CDATA", curLine.substring(curLine.lastIndexOf(":") + 1).trim());
				}
				if (curLine.startsWith("head")) {
					attrs.addAttribute("", "head", "head", "CDATA", curLine.substring(curLine.lastIndexOf(":") + 1).trim());
				}
				if (curLine.startsWith("branch")) {
					attrs.addAttribute("", "branch", "branch", "CDATA", curLine.substring(curLine.lastIndexOf(":") + 1).trim());
				}
				if (curLine.startsWith("locks")) {
					attrs.addAttribute("", "locks", "locks", "CDATA", curLine.substring(curLine.lastIndexOf(":") + 1).trim());
				}
				if (curLine.startsWith("access")) {
					attrs.addAttribute("", "accesslist", "accesslist", "CDATA", curLine.substring(curLine.lastIndexOf(":") + 1).trim());
				}
				if (curLine.startsWith("symbolic")) {
					parseSymbolicNames(br, symbolicNames);
				}
				if (curLine.startsWith("total")) {
					parseHeaderLine(curLine, attrs);
				}
				if (curLine.startsWith("keyword")) {
					attrs.addAttribute("", "keywordsubstitution", "keywordsubstitution", "CDATA", curLine.substring(curLine.lastIndexOf(":") + 1).trim());
				}
			}
		}
		ch.startElement("", "file", "file", attrs);
		if (symbolicNames.size() != 0) {
			ch.startElement("", "symbolicnames", "symbolicnames", EMPTY_ATTR);
			Enumeration<String> e = symbolicNames.keys();
			while (e.hasMoreElements()) {
				String key = e.nextElement();
				String value = symbolicNames.get(key);
				AttributesImpl curAttrs = new AttributesImpl();
				curAttrs.addAttribute("", "name", "name", "CDATA", key);
				ch.startElement("", "sn", "sn", curAttrs);
				ch.characters(value.toCharArray(), 0, value.length());
				ch.endElement("", "sn", "sn");
				curAttrs = null;
			}
			ch.endElement("", "symbolicnames", "symbolicnames");
		}
		parseRevision(br, ch);
		ch.endElement("", "file", "file");
	}

	/**
	 *  Parses the revision part of the cvs log file
	 *
	 *@param  br                the buffered reader
	 *@param  ch                the content handler which receives SAX events
	 *@exception  IOException   Description of the Exception
	 *@exception  SAXException  Description of the Exception
	 */
	public void parseRevision(BufferedReader br, ContentHandler ch) throws IOException, SAXException {
		String curLine;
		StringBuffer accumulator = new StringBuffer();
		AttributesImpl attrs = new AttributesImpl();
		while ((curLine = br.readLine()) != null) {
			if (curLine.startsWith("====")) {
				ch.startElement("", "revision", "revision", attrs);
				ch.characters(accumulator.toString().toCharArray(), 0, accumulator.length());
				accumulator = new StringBuffer();
				ch.endElement("", "revision", "revision");
				break;
			} else {
				if (curLine.startsWith("revision")) {
					attrs.addAttribute("", "id", "id", "CDATA", curLine.substring(curLine.lastIndexOf(" ") + 1).trim());
				} else if (curLine.startsWith("date")) {
					parseRevisionLine(curLine, attrs);
				} else if (curLine.startsWith("----")) {
					ch.startElement("", "revision", "revision", attrs);
					ch.characters(accumulator.toString().toCharArray(), 0, accumulator.length());
					ch.endElement("", "revision", "revision");
					accumulator = new StringBuffer();
					attrs = new AttributesImpl();
				} else {
					accumulator.append(curLine);
				}
			}
		}
	}

	/**
	 *  Parses properties separated by ; in the revision part of the log file 
	 *
	 *@param  curLine  the string to parse
	 *@param  attrs    the attributes
	 */
	public void parseRevisionLine(String curLine, AttributesImpl attrs) {
		while (curLine.length() != 0) {
			String curString = curLine.substring(curLine.indexOf(";") + 1).trim();
			if (curLine.startsWith("state")) {
				attrs.addAttribute("", "state", "state", "CDATA", curLine.substring(curLine.indexOf(":") + 1, curLine.indexOf(";")).trim());
			}
			if (curLine.startsWith("date")) {
				attrs.addAttribute("", "date", "date", "CDATA", curLine.substring(curLine.indexOf(":") + 1, curLine.indexOf(";")).trim());
			}
			if (curLine.startsWith("author")) {
				attrs.addAttribute("", "author", "author", "CDATA", curLine.substring(curLine.indexOf(":") + 1, curLine.indexOf(";")).trim());
			}
			if (curLine.startsWith("lines")) {
				attrs.addAttribute("", "lines", "lines", "CDATA", curLine.substring(curLine.indexOf(":") + 1, curLine.length()).trim());
				break;
			}
			curLine = curString;
		}
	}

	/**
	 *  Parses properties separated by ; in the header part of the log file 
	 *
	 *@param  curLine  the line to parse
	 *@param  attrs    the attributes
	 */
	public void parseHeaderLine(String curLine, AttributesImpl attrs) {
		while (curLine.length() != 0) {
			String curString = curLine.substring(curLine.indexOf(";") + 1).trim();
			if (curLine.startsWith("total")) {
				attrs.addAttribute("", "totalrevision", "totalrevision", "CDATA", curLine.substring(curLine.indexOf(":") + 1, curLine.indexOf(";")).trim());
			}
			if (curLine.startsWith("selected")) {
				attrs.addAttribute("", "selectedlrevision", "selectedlrevision", "CDATA", curLine.substring(curLine.indexOf(":") + 1, curLine.length()).trim());
				break;
			}
			curLine = curString;
		}
	}

	/**
	 *  Description of the Method
	 *
	 *@param  br                Description of the Parameter
	 *@param  symbolicNames     Description of the Parameter
	 *@exception  IOException   Description of the Exception
	 *@exception  SAXException  Description of the Exception
	 */
    public void parseSymbolicNames(BufferedReader br, Hashtable<String,String> symbolicNames) throws IOException, SAXException {
		String curLine = "";
		while ((curLine = br.readLine()) != null) {
			if (curLine.startsWith("\t")) {
				symbolicNames.put(curLine.substring(0, curLine.indexOf(":")).trim(), curLine.substring(curLine.indexOf(":") + 1, curLine.length()).trim());
			} else {
				break;
			}
		}

	}
}

