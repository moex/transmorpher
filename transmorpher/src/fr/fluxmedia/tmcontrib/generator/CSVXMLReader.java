/*
 * $Id: CSVXMLReader.java,v 1.3 2003-02-11 15:41:01 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 * Copyright (C) INRIA Rh�ne-Alpes, 2003.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.tmcontrib.generator;
//imported java classes
import java.io.*;
import java.net.URL;
//imported SAX2 classes
import org.xml.sax.*;
import org.xml.sax.helpers.*;

/**
 *  A class for parsing CSV (comma separated value) files. File datas are converted in
 * SAX2 events.
 * A simple CSV file can look like :
 * <pre>
 * a,b,c
 * d,e,f
 * </pre>
 * Quotes can be used when values contain comma :
 * <pre>
 * a,"b,c",d
 * e,"f,g","h,i"
 * </pre>
 * Separator can be set with <code>setSeparator<code>   
 *
 * This class is defined in the following book "Java and XSLT" by Eric M. Burke (O'Reilly)
 *@author    triolet
 */
public class CSVXMLReader extends AbstractXMLReader {

	private final static Attributes EMPTY_ATTR = new AttributesImpl();

	private char separator=',';
	
	public CSVXMLReader(String separator){
		if(separator!=null)
			this.separator = separator.charAt(0);
	}
	
	/**
	 *  Analyses a CSV file. SAX2 events are sending to the ContentHandler.
	 *
	 *@param  input             The input to parse
	 *@exception  IOException   if input has not been found
	 *@exception  SAXException  an exception that can wrap others exceptions 
	 */
	public void parse(InputSource input) throws IOException, SAXException {

		ContentHandler ch = getContentHandler();
		if (ch == null) {
			return;
		}

		BufferedReader br = null;

		if (input.getCharacterStream() != null) {
			br = new BufferedReader(input.getCharacterStream());
		} else if (input.getByteStream() != null) {
			br = new BufferedReader(new InputStreamReader(input.getByteStream()));
		} else if (input.getSystemId() != null) {
			java.net.URL url = new URL(input.getSystemId());
			br = new BufferedReader(new InputStreamReader(url.openStream()));
		} else {
			throw new SAXException("bad InputSource Object");
		}

		ch.startDocument();
		//<csvfile>
		ch.startElement("", "csvfile", "csvfile", EMPTY_ATTR);

		String curLine = null;

		while ((curLine = br.readLine()) != null) {
			curLine = curLine.trim();
			if (curLine.length() > 0) {
				//<line>
				ch.startElement("", "line", "line", EMPTY_ATTR);
				parseLine(curLine, ch);
				//</line>
				ch.endElement("", "line", "line");
			}
		}
		///<csvfile>
		ch.endElement("", "csvfile", "csvfile");
		ch.endDocument();
	}

	/**
	 *  Sets the separator attribute of the CSVXMLReader object
	 *
	 *@param  separator  The new separator value
	 */
	public void setSeparator(String separator) {
		if (separator!=null)
			this.separator = separator.charAt(0);
	}

	/**
	 *  Recursive analyse of a line
	 *
	 *@param  curLine           The current line
	 *@param  ch                The contentHandler which receives SAX2 events
	 *@exception  IOException   if an I/O operation failed.
	 *@exception  SAXException  an exception that can wrap others exceptions
	 */
	private void parseLine(String curLine, ContentHandler ch) throws IOException, SAXException {

		String firstToken = null;
		String remainderOfLine = null;
		int commaIndex = locateFirstDelimiter(curLine);
		if (commaIndex > -1) {
			firstToken = curLine.substring(0, commaIndex).trim();
			remainderOfLine = curLine.substring(commaIndex + 1).trim();
		} else {
			firstToken = curLine;
		}

		firstToken = cleanupQuotes(firstToken);
		//<value>
		ch.startElement("", "value", "value", EMPTY_ATTR);
		ch.characters(firstToken.toCharArray(), 0, firstToken.length());
		//</value>
		ch.endElement("", "value", "value");

		if (remainderOfLine != null) {
			parseLine(remainderOfLine, ch);
		}
	}

	/**
	 *  Locates the first separator character in the current line
	 *
	 *@param  curLine  The current line
	 *@return          The index of the first separator character in the line, -1 if not found 
	 */
	private int locateFirstDelimiter(String curLine) {
		if (curLine.startsWith("\"")) {
			boolean inQuote = true;
			int numChars = curLine.length();
			for (int i = 1; i < numChars; i++) {
				char curChar = curLine.charAt(i);
				if (curChar == '"') {
					inQuote = !inQuote;
				} else if (curChar == separator && !inQuote) {
					return i;
				}
			}
			return -1;
		} else {
			return curLine.indexOf(separator);
		}
	}

	/**
	 *  Removes quotes around a token 
	 *
	 *@param  token  The token with quotes to remove
	 *@return        The token without quotes
	 */
	private String cleanupQuotes(String token) {
		StringBuffer buf = new StringBuffer();
		int length = token.length();
		int curIndex = 0;

		if (token.startsWith("\"") && token.endsWith("\"")) {
			curIndex = 1;
			length--;
		}

		boolean oneQuoteFound = false;
		boolean twoQuoteFound = false;

		while (curIndex < length) {
			char curChar = token.charAt(curIndex);
			if (curChar == '"') {
				twoQuoteFound = (oneQuoteFound) ? true : false;
				oneQuoteFound = true;
			} else {
				oneQuoteFound = false;
				twoQuoteFound = false;
			}

			if (twoQuoteFound) {
				oneQuoteFound = false;
				twoQuoteFound = false;

				curIndex++;
				continue;
			}
			buf.append(curChar);
			curIndex++;
		}
		return buf.toString();
	}
}

