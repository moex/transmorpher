/* $Id: Log.java,v 1.2 2003-04-22 14:53:57 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.tmcontrib.external;
import fr.fluxmedia.transmorpher.engine.*;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;
import java.io.FileWriter;
import java.io.IOException;
import java.io.IOException;
import java.util.Date;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *  The main goal of this class is debugging. A log component can be add in a SAX events flow in 
 *  order to print these events in a file.
 *
 *@author    triolet
 */
public class Log extends TProcessBasic implements TApplyExternal, ContentHandler {

	ContentHandler outHandler;
	ContentHandler inHandler;
	/**
	 *  Description of the Field
	 */
	protected String fileName = null;
	private FileWriter logfile;

	/**
	 *Constructor for the Log object
	 *
	 *@param  pIn               Description of the Parameter
	 *@param  pOut              Description of the Parameter
	 *@param  params            Description of the Parameter
	 *@param  staticParams      Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 *@exception  IOException   Description of the Exception
	 */
	public Log(String[] pIn, String[] pOut, Parameters params, StringParameters staticParams)
		 throws SAXException, IOException {
		super(pIn, pOut, params);
		// This should be used by the factory for creating the class...
		fileName = (String)params.getParameter("file");
		if (fileName != null) {
			try {
				this.logfile = new FileWriter(fileName);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		Date date = new Date();
		StringBuffer logEntry = new StringBuffer();
		logEntry.append("---------------------------- [");
		logEntry.append(date.toString());
		logEntry.append("] ----------------------------");
		this.printLog("setup", logEntry.toString());
	}

	/**
	 *  Description of the Method
	 */
	public void generatePort() {
		String nameI = nameIn[0];
		String nameO = nameOut[0];
		iListIn[0] = new XML_Port(nameI, this, (ContentHandler)this, 0, XML_Port.IN);
		iListOut[0] = new XML_Port(nameO, (TApplyExternal)this, this, 0, XML_Port.OUT);
	}

	/**
	 *  Sets the contentHandler attribute of the Log object
	 *
	 *@param  handler  The new contentHandler value
	 */
	public void setContentHandler(ContentHandler handler) {
		outHandler = handler;
	}

	/**
	 * the start element method of content handler interface
	 *
	 *@param  ns                Description of the Parameter
	 *@param  localName         Description of the Parameter
	 *@param  name              Description of the Parameter
	 *@param  atts              Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public final void startElement(String ns, String localName, String name, Attributes atts) throws SAXException {

		this.printLog("startElement", "uri=" + ns + " ,local=" + localName + " ,name=" + name);
		for (int i = 0; i < atts.getLength(); i++) {
			this.printLog("            ", new Integer(i + 1).toString()
				 + ". uri=" + atts.getURI(i)
				 + ",local=" + atts.getLocalName(i)
				 + ",qname=" + atts.getQName(i)
				 + ",type=" + atts.getType(i)
				 + ",value=" + atts.getValue(i));
		}

		outHandler.startElement(ns, localName, name, atts);
	}

	/**
	 * the endElement method of content handler interface
	 *
	 *@param  ns                Description of the Parameter
	 *@param  localName         Description of the Parameter
	 *@param  name              Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public final void endElement(String ns, String localName, String name) throws SAXException {
		this.printLog("endElement", "uri=" + ns + " ,local=" + localName + " ,name=" + name);
		outHandler.endElement(ns, localName, name);
	}

	/**
	 * the setDocumentLocator method of content handler interface
	 *
	 *@param  locator  The new documentLocator value
	 */
	public final void setDocumentLocator(Locator locator) {
		this.printLog("setDocumentLocator", "");
		outHandler.setDocumentLocator(locator);
	}

	/**
	 * the startDocument method of content handler interface
	 *
	 *@exception  SAXException  Description of the Exception
	 */
	public final void startDocument() throws SAXException {
		this.printLog("startDocument", "");
		outHandler.startDocument();
	}

	/**
	 * the endDocument method of content handler interface
	 *
	 *@exception  SAXException  Description of the Exception
	 */
	public final void endDocument() throws SAXException {
		this.printLog("endDocument", "");
		outHandler.endDocument();
	}

	/**
	 * the skippedEntity method of content handler interface
	 *
	 *@param  name              Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public final void skippedEntity(java.lang.String name) throws SAXException {
		this.printLog("skippedEntity", "name=" + name);
		outHandler.skippedEntity(name);
	}

	/**
	 * the processingInstruction method of content handler interface
	 *
	 *@param  target            Description of the Parameter
	 *@param  data              Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public final void processingInstruction(java.lang.String target, java.lang.String data) throws SAXException {
		this.printLog("processingInstruction", "target=" + target + ",data=" + data);
		outHandler.processingInstruction(target, data);
	}

	/**
	 * the ignorableWhitespace method of content handler interface
	 *
	 *@param  ch                Description of the Parameter
	 *@param  start             Description of the Parameter
	 *@param  length            Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public final void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
		this.printLog("ignorableWhitespace", new String(ch, start, length));
		outHandler.ignorableWhitespace(ch, start, length);
	}

	/**
	 * the characters method of content handler interface
	 *
	 *@param  ch                Description of the Parameter
	 *@param  start             Description of the Parameter
	 *@param  length            Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public final void characters(char[] ch, int start, int length) throws SAXException {

		this.printLog("characters", new String(ch, start, length));
		outHandler.characters(ch, start, length);
	}

	/**
	 * the startPrefixMapping method of content handler interface
	 *
	 *@param  prefix            Description of the Parameter
	 *@param  uri               Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public final void startPrefixMapping(java.lang.String prefix, java.lang.String uri) throws SAXException {
		this.printLog("startPrefixMapping", "prefix=" + prefix + ",uri=" + uri);
		outHandler.startPrefixMapping(prefix, uri);

	}

	/**
	 * the endPrefixMapping method of content handler interface
	 *
	 *@param  prefix            Description of the Parameter
	 *@exception  SAXException  Description of the Exception
	 */
	public final void endPrefixMapping(java.lang.String prefix) throws SAXException {
		this.printLog("endPrefixMapping", "prefix=" + prefix);
		outHandler.endPrefixMapping(prefix);
	}

	/**
	 *@param  method   Description of the Parameter
	 *@param  content  Description of the Parameter
	 */
	private void printLog(String method, String content) {
		final StringBuffer logEntry = new StringBuffer();
		logEntry.append("[");
		logEntry.append(method);
		logEntry.append("] ");
		logEntry.append(content);
		logEntry.append(System.getProperty("line.separator"));
		final String text = logEntry.toString();
		try {
			if (null != this.logfile) {
				this.logfile.write(text, 0, text.length());
				this.logfile.flush();
			} else {
				System.out.println(text);
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}

