/* $Id: SQLExternal.java,v 1.1 2003-04-22 14:53:57 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2001-2002 Fluxmedia and INRIA Rh�ne-Alpes.
 *
 * http://www.fluxmedia.fr - http://transmorpher.inrialpes.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.tmcontrib.external;

import fr.fluxmedia.transmorpher.engine.*;
import fr.fluxmedia.transmorpher.utils.Parameters;
import fr.fluxmedia.transmorpher.utils.StringParameters;

import org.xml.sax.*;
import org.xml.sax.helpers.*;


import java.sql.*;

public class SQLExternal extends TProcessBasic implements TApplyExternal, ContentHandler{
	
	private boolean inQuery = false;
	private ContentHandler outHandler = null;

	private String tmpQuery = "";
	private String driver="";
	private String baseName="";
	private String baseURL="";
	private String user="";
	private String password="";
	private String resultsetNode="resultset";
	private String rowNode="row";

	private Connection connection=null;
	
	private final static Attributes EMPTY_ATTR = new AttributesImpl();
	
	public SQLExternal(String[] pIn, String[] pOut,Parameters pParam,StringParameters staticParam){
		super(pIn,pOut,pParam);
		driver = (String)pParam.getParameter("driver");
		baseName = (String)pParam.getParameter("base-name");
		baseURL = (String)pParam.getParameter("base-URL");
		if (pParam.getParameter("user")!=null)
			user = (String)pParam.getParameter("user");
		if (pParam.getParameter("password")!=null)
			password = (String)pParam.getParameter("password");
		if (pParam.getParameter("resultset-node")!=null)
			resultsetNode = (String)pParam.getParameter("resultset-node");
		if (pParam.getParameter("row-node")!=null)
			rowNode = (String)pParam.getParameter("row-node");
	}
	
	public void generatePort(){
		String nameI = nameIn[0];
		String nameO = nameOut[0];
		
		iListIn[0] = new XML_Port(nameI,this,(ContentHandler)this,0,XML_Port.IN);
		iListOut[0] = new XML_Port(nameO,this,this,0,XML_Port.OUT);
		
	}
	
	public void setContentHandler(ContentHandler handler){
		this.outHandler=handler;
	}
	
	/** the start element method of content handler interface */
	public final void startElement(String ns,String localName,String name, Attributes atts) throws SAXException {
		if (localName.equals("execute-query"))
			inQuery=true;
	if (!inQuery)
		outHandler.startElement( ns, localName, name,  atts);
	}
	
	/** the endElement method of content handler interface */
	public final void endElement(String ns,String localName,String name) throws SAXException {
		if(inQuery){
			if (localName.equals("execute-query")){
				inQuery=false;
				getConnection();
			}
		}
		else
			outHandler.endElement( ns, localName, name);
	}
	
	/** the setDocumentLocator method of content handler interface */
	public final void setDocumentLocator(Locator locator) {
		outHandler.setDocumentLocator( locator);
	}
	
	/** the startDocument method of content handler interface */
	public final void startDocument() throws SAXException {
		outHandler.startDocument();
	}
	
	/** the endDocument method of content handler interface */
	public final void endDocument() throws SAXException {
		outHandler.endDocument();
	}
	
	/** the skippedEntity method of content handler interface */
	public final void skippedEntity(java.lang.String name) throws SAXException {
		outHandler.skippedEntity( name);
	}
	
	/** the processingInstruction method of content handler interface */
	public final void processingInstruction(java.lang.String target, java.lang.String data) throws SAXException {
	outHandler.processingInstruction(target, data);
	}
	
	/** the ignorableWhitespace method of content handler interface */
	public final void ignorableWhitespace(char[] ch, int start, int length) throws SAXException{
		outHandler.ignorableWhitespace( ch,  start,  length);
	}
	
	/** the characters method of content handler interface */
	public final void characters(char[] ch, int start, int length) throws SAXException{
		if (inQuery){
			tmpQuery = tmpQuery+new String(ch,start,length).trim();
		}
		else
		outHandler.characters(ch,  start,  length);
	}
	
	/** the startPrefixMapping method of content handler interface */
	public final void startPrefixMapping(java.lang.String prefix, java.lang.String uri) throws SAXException{
		outHandler.startPrefixMapping( prefix,uri);
	}
	
	/** the endPrefixMapping method of content handler interface */
	public final void endPrefixMapping(java.lang.String prefix) throws SAXException{
		outHandler.endPrefixMapping( prefix);
	}
	
	private void getConnection(){
		Statement stmt=null;
		try{
			int nbCol = 0;
			
			Class.forName(driver);
			connection = DriverManager.getConnection(baseURL+"/"+baseName,user,password);
			stmt = connection.createStatement();
			tmpQuery = processQuery(tmpQuery);
			ResultSet res = stmt.executeQuery(tmpQuery);
			ResultSetMetaData schema = res.getMetaData();

			outHandler.startElement("",resultsetNode,resultsetNode,EMPTY_ATTR);
			nbCol = schema.getColumnCount();
			while(res.next()){
				outHandler.startElement("",rowNode,rowNode,EMPTY_ATTR);
				for (int i=1;i<=nbCol;i++){
					String name = schema.getColumnName(i);
					outHandler.startElement("",name,name,EMPTY_ATTR);
					String value = res.getString(i);
					outHandler.characters(value.toCharArray(),0,value.length());
					outHandler.endElement("",name,name);
				}
				outHandler.endElement("",rowNode,rowNode);
			}
			outHandler.endElement("",resultsetNode,resultsetNode);
			
			tmpQuery="";
			stmt.close();
			connection.close();
		}
		catch(Exception e){
			e.printStackTrace();
		
		}
	}
	
	private static void close (Statement stmt,Connection connection){
		try{
			stmt.close();
			connection.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private String processQuery(String query){
		if(query.indexOf('$')==-1)
			return query;
		else{
			String before=query.substring(0,query.indexOf('$'));
			String param = query.substring(query.indexOf('$')+2,query.indexOf('}'));
			String after=query.substring(query.indexOf('}')+1,query.length());
			param = (String)getParameters().getParameter(param);
			System.out.println(before);
			System.out.println(param);
			System.out.println(after);

			return processQuery(before+param+after);
		}
	}
}
