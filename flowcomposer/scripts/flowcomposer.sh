#!/bin/sh

CPATH=../lib/xerces.jar:${CLASSPATH}
CPATH=../lib/jgraph.jar:${CPATH}
CPATH=../lib/transmo.jar:${CPATH}

java -mx64m -classpath .:../classes:../ressources:${CPATH} -Djava.compiler=NONE fr.fluxmedia.flowcomposer.FlowComposer $*
