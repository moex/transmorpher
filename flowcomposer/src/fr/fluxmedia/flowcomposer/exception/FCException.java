/**
 *
 *$Id: FCException.java,v 1.3 2002-09-13 17:53:56 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.exception;

/**
 * The <code>FCException</code> class provides the management of the exceptions
 *
 * @version 0.2
 * @author Chomat/Saint-Marcel
 */


public class FCException extends Exception {
 
    
    private Integer level;
    private String category;
    
    /**
     * Constructs an exception
     *
     * @param message the warning message
     * @param category the type of exception
     * @param level the level of exception 
     */
    public FCException(String message,String category,Integer level) {
        super(message);
        this.level=level;
        this.category=category;
    }



}
