 /**
 *
 *$Id: Version.java,v 1.6 2002-12-12 10:25:58 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.utils;

/**
 *
 * Some informations about the versionning of the software
 *
 * @version 0.2
 * @author Chomat/Saint-Marcel
 */

public interface Version {

    public static final String VERSION = "0.3";
    public static final String RELEASE = "@VERS@";
    public static final String NAME = "FlowComposer";
    public static final String DATE = "@DATE@";
    public static final String URL = "http://www.fluxmedia.fr";
    public static final String CONTACT ="flow-dev@fluxmedia.fr";
    public static final String TRANSMO_CONTACT ="transmorpher-dev@fluxmedia.fr";
    public static final String RIGHTS = "(C) FluxMedia, 2002";
}
