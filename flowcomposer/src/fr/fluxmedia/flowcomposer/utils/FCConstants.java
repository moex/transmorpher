 /**
 * $Id: FCConstants.java,v 1.6 2002-12-12 10:25:58 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.utils;

import java.util.Hashtable;
import fr.fluxmedia.transmorpher.graph.*;

/**
 * The <code>FCConstants</code> class stores all the constants used by the
 * application.   
 *
 * @version 0.2
 * @author Chomat/Saint-Marcel
 */


public class FCConstants{
    
    public static final String NMERGE        = "M"; 
    public static final String NGENERATE     = "G";
    public static final String NSERIALIZE    = "S";	 
    public static final String NDISPATCH     = "D";
    public static final String NAPPLYQUERY   = "AQ"; 
    public static final String NAPPLYPROCESS = "AP";
    public static final String NAPPLYEXTERNAL= "AE"; 
    public static final String NAPPLYRULESET = "AR";
    
    public static final int MERGE         =0;
    public static final int GENERATE      =1;
    public static final int SERIALIZE     =2;
    public static final int DISPATCH      =3;
    public static final int APPLYEXTERNAL =4;
    public static final int APPLYPROCESS  =5;
    public static final int APPLYRULESET  =6;
    public static final int APPLYQUERY    =7;
    public static final int PORTPROCESS   =8;

    public static final int PORTOUTPUT   = 0;
    public static final int PORTINPUT    = 1;

    public static final Object[] MERGETYPE        = new Object[]{"concat"};
    public static final Object[] DISPATCHTYPE     = new Object[]{"broadcast"};
    
    public static final Object[] SERIALIZETYPE    = new Object[]{"writefile","stdout"};
    public static final Object[] GENERATETYPE    = new Object[]{"readfile","stdin"};
    
    public static final Object[] APPLYQUERYTYPE   = new Object[]{"tmq"};
    public static final Object[] APPLYEXTERNALTYPE= new Object[]{"xslt"};

    public static final Object[] STRATEGYRULESET  = new Object[]{"top-down"};

    public static final int MAIN          =   1;
    public static final int SERVLET       =   2;
    public static final int TRANSFORMER   =   3;
    public static final int PROCESS       =   4;
    
    public static final int DEFAULT_MAIN_TYPE = MAIN;    
    public static final String DEFAULT_MAIN_NAME = "main";
    
    /**
     * Returns the type of the cell like Merge, Serialize ...
     */
    public static int getType(Object object){

	if(object instanceof Merge)
	    return MERGE;
	else if(object instanceof Generate)
	    return GENERATE;
	else if(object instanceof Dispatch)
	    return DISPATCH;
	else if(object instanceof Serialize)
	    return SERIALIZE;
	else if(object instanceof ApplyExternal)
	    return APPLYEXTERNAL;
	else if(object instanceof ApplyRuleset)
	    return APPLYRULESET;	
	else if(object instanceof ApplyQuery)
	    return APPLYQUERY;
	else if(object instanceof ApplyProcess)
	    return APPLYPROCESS;
	else
	    return -1;
    }


   /**
    * Returns the default name associates at the cell
    */
    public static String getCallClass(int className){
    
	String name;
	
	switch(className){

	case  MERGE : 
	    name =  NMERGE;
	    break;
	case  GENERATE :   
	    name = NGENERATE;
	    break;
	case  APPLYPROCESS :  
	    name = NAPPLYPROCESS;
	    break;     
	case  APPLYQUERY :   
	    name = NAPPLYQUERY;
	    break;	
	case  APPLYRULESET :   
	    name = NAPPLYRULESET;
	    break;	
	case  APPLYEXTERNAL :  
	    name = NAPPLYEXTERNAL;
	    break;	
	case  SERIALIZE :  
	    name = NSERIALIZE;
	    break;	
	case  DISPATCH :  
	    name = NDISPATCH;
	    break;
	default :   name = "unknow Call";
 	}
	
	return name;
    } 

    /**
     * Returns a list of default's type of a cell for Transmorpher's attributes 
     */
    public static Object[] getListType(int type){
	
	Object[] list;

	switch(type){
	    
	case  MERGE : list =  MERGETYPE;
	    break;
	case  GENERATE : list =  GENERATETYPE;
	    break;
	case  APPLYPROCESS : list =  null;
	    break;     
	case  APPLYQUERY : list =  APPLYQUERYTYPE;
	    break;	
	case  APPLYRULESET : list =  null;
	    break;	
	case  APPLYEXTERNAL : list =  APPLYEXTERNALTYPE;
	    break;	
	case  SERIALIZE : list =  SERIALIZETYPE;
	    break;	
	case  DISPATCH : list =  DISPATCHTYPE;
	    break;
	default : list =  null;
 	}
	
	return list;
    } 
    /*
    public static Hashtable defaultAttributes(int type){

	Hashtable attributes = new Hashtable();

	switch(type){
	    
	case  MERGE : 
	    attributes.put("id","Merge");
	    attributes.put("type",MERGETYPE[0]);
	    break;
	case  GENERATE : 
	    attributes.put("id","Generate");
	    attributes.put("type",GENERATETYPE[0]);
	    attributes.put("file","");
	    break;
	case  APPLYPROCESS :
	    attributes.put("id","A-P");
	    attributes.put("ref",""); 
	    break;     
	case  APPLYQUERY :  
	    attributes.put("id","A-Q");
	    attributes.put("ref","");
	    break;
	    
	case  APPLYRULESET : 
	    attributes.put("id","A-R");
	    attributes.put("ref","");
	    attributes.put("strategy",STRATEGYRULESET[0]);
 	    break;	
	case  APPLYEXTERNAL : 	    
	    attributes.put("id","A-E");
	    attributes.put("type",APPLYEXTERNALTYPE[0]);
	    attributes.put("file","");
	    break;	
	case  SERIALIZE :
	    attributes.put("id","Serialize");
	    attributes.put("type",SERIALIZETYPE[0]);
	    attributes.put("file","");
	    break;	
	case  DISPATCH : 	   
	    attributes.put("id","Dispatch");
	    attributes.put("type",DISPATCHTYPE[0]);
	    break;
	    
	default : attributes = null;
 	}

	return attributes;
    }
    */
    
    /**
     * Returns true if the object Call have a reference on a process
     */ 
    public static boolean isRef(Object call){
	
	int type = getType(call);
	return type == APPLYPROCESS || type == APPLYRULESET || type == APPLYQUERY;
	
    }

    /**
     * Returns a list of default's strategy of a cell for Transmorpher's attributes 
     */
    public static Object[] getStrategyList(int type){
	
	if (type == APPLYRULESET) {
	    return STRATEGYRULESET;
	} // end of if ()
	return null;
    }
    
   
}
