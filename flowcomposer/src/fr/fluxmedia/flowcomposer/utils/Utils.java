
/**
 *
 * Utils.java
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.utils;

import java.io.File;
import java.util.Iterator;

public class Utils {
    
    /**
     * The <code>Utils</code> class provides utility functions
     *
     * @version 0.2
     * @author Chomat/Saint-Marcel
     */
    
    /**
     * Suffix applied for the extension of xml's files
     */
    public final static String xml = "xml";
    
    /**
     * Suffix applied for the extension of xml's files
     */
    public final static String XML = "XML";
    
    /**
     * Suffix applied for the extension of xml's files
     */
    public final static String xsl = "xsl";
    
    /**
     * Suffix applied for the extension of xml's files
     */
    public final static String xslt = "xslt";
    
    /**
     * Suffix applied for the extension of xml's files
     */
    public final static String XSL = "XSL";
    
    /**
     * Suffix applied for the extension of xml's files
     */
    public final static String XSLT = "XSLT";
    
    /**
     * Suffix applied for the extension of xml's files
     */
    public final static String sav = "sav";
    
     /**
     * Suffix applied for the extension of xml's files
     */
    public final static String SAV = "SAV";
    
    

    /**
     * Get the extension of a file. (name_file.ext) --> ext
     */
    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');
        
        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }
    
    /**
     * Get the scale of graph without this extension (scale%) --> scale
     */
    public static String getScale(String st) {
        String ext = null;
        int i = st.lastIndexOf('%');
        if (i > 0) {
            ext = st.substring(0,i).toLowerCase();
        }
        return ext;
    }
    
    /**
     * Get the lenght of <code>java.awt.Iterator</code>
     */
    public static int lenght(Iterator it) {
        int compteur = 0;
        while (it.hasNext()){
            it.next();
            compteur++;
        }
        return compteur;
    }
    
    
}

















