/**
 *
 *$Id: FCOverviewPanel.java,v 1.4 2002-09-13 17:53:56 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.gui;

import javax.swing.*;
import com.jgraph.JGraph;
import com.jgraph.graph.*;
import com.jgraph.event.*;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import java.util.Observer;
import java.util.Observable;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import java.awt.*;

import fr.fluxmedia.flowcomposer.graph.ProcessGraph;

/**
 * The <code>FCOverviewPanel</code> class serves to show in window a general view of
 * the graph.
 *
 * @version 0.2 
 * @author Chomat/Saint-Marcel
 */


public class FCOverviewPanel extends JPanel implements ComponentListener,
        GraphModelListener,
        Observer
{
    /** the new instance of graph*/
    protected ProcessGraph graph;

    /** 
      * Constructs a new graph with this view.
      * 
      * @param graph it's owner graph
      */
    public FCOverviewPanel(JGraph g) {
        GraphView view = new ViewRedirector(g, g.getView());
        graph = new ProcessGraph(g.getModel(), view);
        graph.setAntiAliased(true);
        setBorder(BorderFactory.createEtchedBorder());
        graph.getModel().addGraphModelListener(this);
        graph.setEnabled(false);
        g.addComponentListener(this);
        g.getView().addObserver(this);
        setLayout(new BorderLayout());
        add(graph, BorderLayout.CENTER);
       
    }

     /** Observer implementation. This method is called whenever the object is changed*/
    public void update(Observable o, Object arg) {
        componentResized(null);
    }

    /** 
      * Implementation of GraphModelListener. This interface is used to update the view and
      * repaint the graph.
      */
    
    public void graphChanged(GraphModelEvent e) {
        componentResized(null);
    }

    /** ComponentListener implementation */
    public void componentResized(ComponentEvent e) {
        Rectangle r = AbstractCellView.getBounds(graph.getView().getRoots());
        //System.out.println(r+"rectangle");
        double scale = 0.5;
        if (r != null) {
            Dimension d = new Dimension(r.x+r.width, r.y+r.height);
            Dimension s = getSize();
            double sx = s.getWidth()*0.95/d.getWidth();
            double sy = s.getHeight()*0.95/d.getHeight();
            scale = Math.min(Math.max(Math.min(sx, sy), 0.05), 0.5);
        }
        graph.setScale(scale);
        repaint();
    }

    /** ComponentListener implementation */
    public void componentShown(ComponentEvent e) {
        componentResized(e);
    }

    public void componentHidden(ComponentEvent e) { }

    public void componentMoved(ComponentEvent e) { }

    
    
    /** The GraphView object holds the cell views.*/
    public class ViewRedirector extends GraphView {

        protected GraphView realView;

        /** 
          * Constructs a new GraphView. 
          * 
          * @param graph it's owner graph
          * @param realview the view of graph
          */
        public ViewRedirector(JGraph graph, GraphView realView) {
            super(null, graph);
            this.realView = realView;
            setModel(graph.getModel());
        }

        /** overrides the method */
        public CellView[] getRoots() {
            return realView.getRoots();
        }

        /** overrides the method*/
        public CellView getMapping(Object cell, boolean create) {
            if (realView != null)
                return realView.getMapping(cell, create);
            return null;
        }

        /** overrides the method*/
        public void putMapping(Object cell, CellView view) {
            if (realView != null)
                realView.putMapping(cell, view);
        }

    }

}
