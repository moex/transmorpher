/**
 *
 *$Id: FlowComposer.java,v 1.22 2003-01-29 15:21:42 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.gui;

// import ProcessGraph
import com.jgraph.JGraph;
import com.jgraph.graph.*;
import com.jgraph.event.*;

import java.awt.*;
import java.awt.print.Paper;
import java.beans.*;
import java.util.*;
import java.io.*;
import java.applet.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import javax.swing.plaf.metal.MetalTabbedPaneUI;
import java.awt.event.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.Hashtable;
import java.util.ArrayList;
import java.util.List;
import javax.swing.plaf.basic.*;

import javax.swing.undo.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.*;
import javax.swing.text.html.*;

import java.util.Iterator;

import fr.fluxmedia.flowcomposer.utils.*;
import fr.fluxmedia.flowcomposer.gui.guitools.*;
import fr.fluxmedia.flowcomposer.graph.*;
import fr.fluxmedia.flowcomposer.parser.*;

import fr.fluxmedia.transmorpher.parser.ProcessParser;
import fr.fluxmedia.transmorpher.utils.LinearIndexedStruct;

/**
 * FlowComposer is a RAD environment that displays a network of related objects,
 * Dispatch Merge Generate ... (for processing generic transformations on XML
 * documents) using the well-known paradigm of a graph.It bases on JGraph Component
 *
 * @version 0.2 
 * @author Chomat/Saint-Marcel
 */

public class FlowComposer extends JPanel  implements GraphModelListener,
        GraphSelectionListener,
        ComponentListener,
        PropertyChangeListener,
        ActionListener,
        Observer{

    // Properties define by user.
    protected static Properties appProperties;

    // Default properties. not be able to changed.
    protected static Properties defProperties;

    //Used only if FlowComposer is an applet
    //private boolean inAnApplet = true;

    //Process Factory allow to create new Process
    protected FCProcessFactory processFactory;

    //THE EIDTING GRAPH HANDLER
    protected FCMarqueeHandler marqueeHandler;

    //Array of all Flowcomposer instance
    protected static ArrayList instances = new ArrayList();

    
    protected Point dialogLocation=null;

    protected FlowLayout flow;
    //Version
    protected static String version = Version.VERSION ;

   
    //Application Title
    protected static String title = Version.NAME ;

    protected static String[] zoomMessage = new String[]{"50%","75%","100%","150%","200%","500%"};

    //private JPanel editor;
    protected JMenuBar menubar;

    // for represent the process notion we dacide to use a tabbed Pane
    protected JTabbedPane tabProcess ;

    //
    protected JSplitPane splitPane ;
    
    //
    protected Dimension dimConsole;
    
    // a console to display message send by transmorpher
    protected FCConsole console;
    
    // External resource
    protected static ResourceBundle resource;

    //The window listener.
    protected static FCWindowListener windowListener;

    //File
    protected File file = null;

    // temporatry file
    protected File tmpFile = null;

    protected Font font = new Font("Arial",Font.PLAIN,13);
    //a temporary filename
    String tmpFilename;

    public static Rectangle viewPortRect = new Rectangle(ProcessGraph.PREFERRED_WIDTH,ProcessGraph.PREFERRED_HEIGHT);

    // Application Icon. From resource file.
    public static ImageIcon appIcon;

    //HastTable contains IHM element
    protected Hashtable commands;
    protected Hashtable buttons;
    protected Hashtable menuItems;
    protected Hashtable specialItems;
    protected Hashtable items;

    protected FCStatusBar status;
    protected JComboBox zoom;
    protected JScrollPane sp ;
    protected JPopupMenu popupMenu;
    protected JPanel pane,panel;
    protected JMenu list;
    protected JMenuItem redrawMenuItem;
    protected JMenuItem cycleMenuItem;

    protected boolean modified = false;

    protected boolean showExecutionParameter = true ;
    protected boolean showCompilParameter = true ;

    protected int mouseRightX;
    protected int mouseRightY;
    /**Default width of the app*/
    public static final int PREFERRED_WIDTH =800;

    /**Default height of the app*/
    public static final int PREFERRED_HEIGHT =600;

    // Possible Look & Feels
    private static final String mac      =
            "com.sun.java.swing.plaf.mac.MacLookAndFeel";
    private static final String metal    =
            "javax.swing.plaf.metal.MetalLookAndFeel";
    private static final String motif    =
            "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
    private static final String windows  =
            "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

    // The current Look & Feel
    private static String currentLookAndFeel = windows;

    private Vector vectorFont;
    /**
     * Suffix applied to the key used in resource file
     * lookups for a label.
     */
    public static final String labelSuffix = "Label";


    /**
     * Suffix applied to the key used in resource file
     * lookups for a menuitem (instead of action)
     */
    public static final String accelSuffix = "Accel";

    /**
     * Suffix applied to the key used in resource file
     * lookups for a submenu
     */
    public static final String menuSuffix = "Menu";

    /**
     * Suffix applied to the key used in resource file
     * lookups for a menuitem (instead of action)
     */
    public static final String mnemonicSuffix = "Mnemonic";
    /**
     * Suffix applied to the key used in resource file
     * lookups for an action.
     */
    public static final String actionSuffix = "Action";

    /**
     * Suffix applied to the key used in resource file
     * lookups for an image.
     */
    public static final String imageSuffix = "Image";

    /**
     * Suffix applied to the key used in resource file
     * lookups for tooltip text.
     */
    public static final String tipSuffix = "Tooltip";

    static {
        try {
            resource = ResourceBundle.getBundle("resources/FlowComposer_fr",
                    Locale.getDefault());
        } catch (MissingResourceException mre) {
            System.err.println("FlowComposer_fr.properties not found");
            System.exit(1);
        }

        // load properties

	FileInputStream in ; 
	File props;
        try{
            // create and load default properties
            defProperties = new Properties();
            props = new File("defaultProperties");
            if(props.exists()){
                in = new FileInputStream("defaultProperties");
                defProperties.load(in);
                in.close();
            } // end of if (props.exists()))
            else {
                // create here the def properties
            } // end of else
	}catch(IOException e){
	    // problem when read/close the file defaultProperties
	    
	}
	try{
            // now load properties from last invocation
            appProperties = new Properties(defProperties);
            props = new File("appProperties");

            if (props.exists()) {
                in = new FileInputStream("appProperties");
                appProperties.load(in);
                in.close();
            } // end of if (props.exists())
        }
        catch(IOException e){
	    //problem when read/close the file appProperties
        }
    }

    /**
     * Constructs an empty <code>FlowComposer</code>
     */

    public FlowComposer(){
        this(null,null);
    }

    /**
     * Constructs a <code>FlowComposer</code>  and initializes it with the specified data model
     *
     * @param model its default graph model
     */
     public FlowComposer(GraphModel model) {
        this(null,model);
    }
     
     /**
     * Constructs a <code>FlowComposer</code>  and initializes it with the specified data model 
     * and a file loading
     *
     * @param file opening file
     * @param model its default graph model  
     */
     public FlowComposer(File file, GraphModel model) {

        super(true);
        setFile(file);
        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            //UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception exc) {
            System.err.println("Error loading L&F: " + exc);
        }

        GraphicsEnvironment gEnv =
                GraphicsEnvironment.getLocalGraphicsEnvironment();
        String envfonts[] = gEnv.getAvailableFontFamilyNames();
        vectorFont = new Vector();
        for ( int i = 1; i < envfonts.length; i++ ) {
            vectorFont.addElement(envfonts[i]);
        }

        marqueeHandler = new FCMarqueeHandler(this);
        
        // init the GUI.
        this.initGUI();

        // init the processFactory.
        this.initProcessFactory();           
    }

    /** 
     * Creation of the manager of process <code>FCProcessFactory</code>. there are two cases :
     * 1) if we load a file, the Transmorpher graph is create with the correspondind file xml
     * and the list of process is update in the ProcessFactory with it. After if a file RDF exists
     * we constructs the <code>ProcessGraph</code> with the parsing of the file, otherwise we applying a
     * basic algorithm layout.
     * 2) if its a new file, we create a new instance of transmorpher and instanciate the 
     * ProcessFactory with it. By default a new file is created with a process Main.
     */
    protected void initProcessFactory(){
        
	if(file != null)
	    { 
		processFactory = new FCProcessFactory(this,false);
		this.loadXML();
		File fileRDF = new File(file.getAbsolutePath().substring(0,file.getAbsolutePath().lastIndexOf('.'))+".rdf");
		
		if(fileRDF.exists())
		    {
			ParserRDF rdfParser = new ParserRDF(processFactory,0);
			rdfParser.newParse(fileRDF.getAbsolutePath());
		    }
		else 
		    {
			processFactory.layout();
		    }
	    }
        else
	    {
		tmpFilename = resource.getString("DEFAULTFILENAME")+ Integer.toString(instances.size()+1);
		processFactory = new FCProcessFactory (this,true);
		// Init the main process
		processFactory.newProcess(FCConstants.DEFAULT_MAIN_TYPE);
        }

    }

    /**
     * Creation of all graphical Components of the application : Menu, Toolbars,
     * SplitPane with the console and the DrawArea(JTabbedPane) and the statusbar.
     */
    protected void initGUI(){

        createStatusbar();

        currentDirectory = appProperties.getProperty("directory");

        //updateLookAndFeel(windows);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        this.setLayout(new BorderLayout());

        panel = new JPanel();
        panel.setPreferredSize(new Dimension(PREFERRED_WIDTH,PREFERRED_HEIGHT));
        // Use Border Layout
        panel.setLayout(new BorderLayout());

        menuItems = new Hashtable();
        items= new Hashtable();

        // install the command table
        commands = new Hashtable();
        for (int i = 0; i < actions.length; i++) {
            Action a = actions[i];
            //System.out.println("actions:"+a.getValue(Action.NAME));
            commands.put(a.getValue(Action.NAME), a);
        }

        specialItems = createSpecialItems();
        buttons = createButtons();
        //createSpcialButtons();
        // Add a ToolBar
        JPanel center = new JPanel();
        center.setLayout(new BorderLayout());
        
	// ToolBar creation.
	JToolBar toolBar = (JToolBar)createToolBar("toolbar");
        toolBar.addComponentListener(this);
	
	String borderLayout = BorderLayout.NORTH ;
	if (toolBar.getOrientation() == JToolBar.VERTICAL) {
	    borderLayout = BorderLayout.WEST ;
	}
	center.add(toolBar,borderLayout);

        JToolBar accelBar = (JToolBar)createToolBar("accelbar");
        accelBar.addComponentListener(this);
	
	borderLayout = BorderLayout.NORTH ;
	
	if (accelBar.getOrientation() == JToolBar.VERTICAL) {
	    borderLayout = BorderLayout.WEST ;
	}
	panel.add(accelBar,borderLayout);

	Border etched = BorderFactory.createRaisedBevelBorder();
	Border empty = BorderFactory.createEmptyBorder();
        
	tabProcess = new JTabbedPane(){
		 
		public void remove(int tab)
		{
		    super.remove( tab );
		    
		    int after = getSelectedIndex();
		    
		    //  The selected tab remained the same after this remove, consider
		    //  it a state changed
		    
		    if (after == tab)
			{
			    fireStateChanged();
			}
		}
	    };
	
        tabProcess.addChangeListener(new ChangeListener() {

                    public void stateChanged(ChangeEvent e) {
                        
			if(tabProcess.getSelectedIndex() != -1) {
			    
			    if ( processFactory.getCurrentProcess() != null ) {
				(processFactory.getCurrentProcess().getTouchLayout()).stop();
				redrawMenuItem.setSelected(false);
                                cycleMenuItem.setSelected(false);
			    }
			    			    
                            processFactory.setCurrentProcess(tabProcess.getTitleAt(tabProcess.getSelectedIndex()));
			    update();
			}


                    }
                }
        );

	tabProcess.setFont(font);

        splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
	Border border = BorderFactory.createEmptyBorder();
        splitPane.setBorder(empty); 
        splitPane.setBackground(Color.lightGray);
        splitPane.setContinuousLayout(true);
        splitPane.setOneTouchExpandable(false);
        splitPane.setResizeWeight(0.85);
        splitPane.setTopComponent(tabProcess);
	console = new FCConsole("Transmorpher Console:");
	splitPane.setBottomComponent(console);
	
	// Add the Graph as Center Component
        center.add(splitPane, BorderLayout.CENTER);
        panel.add(center,BorderLayout.CENTER);
        this.add(createMenubar(),BorderLayout.NORTH);
        this.add(panel,BorderLayout.CENTER);
        this.add(status,BorderLayout.SOUTH);

    }
    
    /** change the name of a tab of the JTabbedPane*/
    public void updateTab(String oldName, String newName){
        tabProcess.setTitleAt(tabProcess.indexOfTab(oldName),newName);
    }

    /**
     * Loading the transmorpher graph with the parsing of the file xml and update
     * the <code>FCProcessFactory</code>
     */
    protected void loadXML(){
			try{
				System.out.println("load XML"+file.getAbsolutePath());
        ProcessParser parser = new ProcessParser(0,new Object[]{processFactory});
        processFactory.setTransmorpher(parser.newParse(file.getAbsolutePath()));
	processFactory.createAllChannels();
        processFactory.updateCellProcess();
        tabProcess.setSelectedIndex(0);
			}
			catch (Exception e){
			}
    }

    
    /** Open an empty editor frame. */
    public static FlowComposer newEditor() {
        return newEditor((File) null);
    }

    /** Open a new editor frame with the specified file  */
    public static FlowComposer newEditor(File file) {
        return newEditor(file, null);
    }

    /** Open a new editor frame with the specified file and data model */
    public static FlowComposer newEditor(File file, GraphModel model){

        // Construct Frame
        JFrame frame = new JFrame();

        FlowComposer flowComposer = createInstance(file,model);
        instances.add(flowComposer);

        frame.setContentPane(flowComposer);
        frame.addComponentListener(flowComposer);

        frame.pack();

        flowComposer.setTitle();
        frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(flowComposer.createCloser());

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        frame.setLocation(
                screenSize.width/2 - frame.getSize().width/2 + instances.size()*10,
                screenSize.height/2 - frame.getSize().height/2+ instances.size()*10);

        frame.show();

        return flowComposer;
    }

     /** Open a new editor frame with the specified instance of <code>FlowComposer</code>*/
    public static FlowComposer newEditor(FlowComposer flowcomposer) {
        JFrame frame = new JFrame();
        /*frame.setTitle(flowcomposer.getDocumentTitle());
        frame.setBackground(Color.lightGray);
        frame.getContentPane().setLayout(new BorderLayout());
        frame.getContentPane().add("Center", flowcomposer);
        frame.addWindowListener(flowcomposer.createAppCloser());
        frame.pack();
        frame.setSize(540, 580);
        if (appIcon != null)
        frame.setIconImage(appIcon.getImage());
        frame.show();
        instances.add(flowcomposer);*/
        return flowcomposer;
    }

    /** Create a new instance of <code>FlowComposer</code>*/
    public static FlowComposer createInstance(File file, GraphModel model)
    {
        return  new FlowComposer(file, model);
    }

    
    /**
       * Get the instance of processFactory.
       * @return value of processFactory.
       */
    public FCProcessFactory getProcessFactory() {return processFactory;}
    
    /**
       * Set the instance of processFactory.
       * @param v  Value to assign to processFactory.
       */
    public void setProcessFactory(FCProcessFactory  v) {this.processFactory = v;}

    /**
     * Returns the list of reference for the type 
     */
    public Object[] getList(int type){

        if (type == FCConstants.APPLYPROCESS) {
            return processFactory.getProcessList();
        }
        else if (type == FCConstants.APPLYRULESET) {
            return processFactory.getRulesetList();
        }
	else if (type == FCConstants.APPLYQUERY) {
	    return processFactory.getQueryList();
	}	
        return null;
    }


    /**
     * Find the hosting frame, for the file-chooser dialog.
     */
    public Frame getFrame() {
        for (Container p = getParent(); p != null; p = p.getParent()) {
            if (p instanceof Frame) {
                return (Frame) p;
            }
        }
        return null;
    }

    /**
     * Get the value of file.
     * @return value of file.
     */
    public File getFile() {return file;}

    /**
     * Set the value of file.
     * @param v  Value to assign to file.
     */
    public void setFile(File  v) {
        this.file = v;
    }

    /** Set title at editor frame*/
    public void setTitle(){
        if(file != null)
            setTitle(file.getAbsolutePath());
        else 
            setTitle(tmpFilename);
    }

    /** Set title at editor frame with the specified file*/
    public void setTitle(String file){
        if(file != null)
            getFrame().setTitle(title +" "+ version + " "+ file);
    }

    /** Returns the location on screen of the DrawArea which contains the graph*/
    public Point getGraphLocationOnscreen(){

        Point result = null;

        if(tabProcess!=null)
            result =  tabProcess.getLocationOnScreen();

        return result;
    }

    /**
     * write a RDF file which is the graphical chart of all process
     */
    public void generateFileRDF(String filename) throws java.io.IOException{
        
	processFactory.generateFileRDF(filename);
         
    }
        
    /** Remove a process of JTabbedPane*/
    public void removeTabProcess(Component component){
        
	if(tabProcess != null && component != null)
	    {
		tabProcess.remove(tabProcess.indexOfTab(component.toString())) ;
	    }
    }

    /**
     * Create a new Process with an instance of <code>ProcessGraph</code>. Moreover
     * we create the corresponding <code>FCOverviewPanel</code>.
     */
    public void createProcess(Object process){

        UndoHandler undoHandler;
        final ProcessGraph currentProcess = (ProcessGraph)process;

        if(tabProcess != null ) {

            /*
            if(tabProcess.indexOfTab(process.toString())!=-1)
            tabProcess.remove(tabProcess.indexOfTab(process.toString()));
            */

            if(marqueeHandler == null)
                marqueeHandler =  new FCMarqueeHandler(this);

            currentProcess.setMarqueeHandler(marqueeHandler);
            registerListeners(currentProcess);

            /******* undo not yet implemented ***********/

            /*
            undoHandler = new UndoHandler();
            undoHandler.setProcess(currentProcess);
            currentProcess.getModel().addUndoableEditListener(undoHandler);
            undoHandler.setProcess(currentProcess);
            */

            /******** undo not yet implemented **********/

            JPanel container = new JPanel();
            pane = new JPanel();

            Border loweredbevel = BorderFactory.createLoweredBevelBorder();

            currentProcess.setPreferredSize(new Dimension(ProcessGraph.PREFERRED_WIDTH,ProcessGraph.PREFERRED_HEIGHT));
            currentProcess.setBorder(loweredbevel);
            flow = new FlowLayout(FlowLayout.LEFT,0,0);
            status.setMessageScale((String)zoomMessage[2]);
            pane.setLayout(flow);
            pane.setBackground(Color.gray);
            pane.setLayout(flow);
            pane.setBackground(Color.gray);
            pane.add(currentProcess);
            sp = new JScrollPane(pane,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
            Border empty = BorderFactory.createEmptyBorder();
            container.setLayout(new BorderLayout());
            container.add(sp,BorderLayout.CENTER);
            JButton buttonView =  new JButton("...");
            sp.setCorner(JScrollPane.LOWER_RIGHT_CORNER,buttonView);
            Component corner = sp.getCorner(JScrollPane.LOWER_RIGHT_CORNER);
            MouseListener mouseListener = new MouseListener() {
                JWindow jWindow = new JWindow();
                FCOverviewPanel overviewPanel = new FCOverviewPanel(currentProcess);
                Container fContentPane = jWindow.getContentPane();
                public void mouseClicked(MouseEvent e){}
                public void 	mousePressed(MouseEvent e){
                   if (!(jWindow.isVisible())){
                        Point corner = e.getComponent().getLocationOnScreen();
                        fContentPane.setLayout(new BorderLayout());
                        fContentPane.add(overviewPanel);
                        jWindow.setSize(new Dimension(180,180));
                        jWindow.setLocation(corner.x-180,corner.y-180);
                        jWindow.setVisible(true);
                        overviewPanel.componentResized(null);
                    }
                }
                public void 	mouseReleased(MouseEvent e){}
                public void 	mouseEntered(MouseEvent e){}
                public void 	mouseExited(MouseEvent e){jWindow.setVisible(false);}
            };
            buttonView.addMouseListener(mouseListener);
            
	    if( processFactory.isMain(currentProcess) )
                tabProcess.insertTab(currentProcess.getName(), null, container,currentProcess.getName(),0);
            else
                container = (JPanel)tabProcess.add(currentProcess.getName(),container);

            tabProcess.setSelectedComponent(container);
	}
	  
        list.removeAll();
        List listProcess = processFactory.getAllProcess();
        Iterator it = listProcess.iterator();
        while (it.hasNext()) {
            ProcessGraph graph = (ProcessGraph)it.next();
            JMenuItem mi = (JMenuItem)list.add(new JMenuItem(graph.getName()));
            mi.addActionListener(this);         
            mi.setFont(font);
        }
    
        currentProcess.resizeComponent();
    }

    /**
     * Create the menubar for the application.  By default this pulls the
     * definition of the menu from the associated resource file.
     */
    protected JMenuBar createMenubar() {
        JMenuItem mi;
        //Border empty = BorderFactory.createEmptyBorder();
        menubar = new JMenuBar();
        //menubar.setBorder(empty);

        Rectangle menu = menubar.getBounds();
        //menubar.setBorder(etched);

        String[] menuKeys = tokenize(getResourceString("menubar"));
        for (int i = 0; i < menuKeys.length; i++) {
            String[] itemKeys = tokenize(getResourceString(menuKeys[i]));
            JMenu m = createMenu(menuKeys[i], itemKeys);
           
            m.setFont(font);
            if (m != null) {
                menubar.add(m);
            }
        }
        return menubar;
    }

    /**
     * Create a menu for the application. By default this pulls the
     * definition of the menu from the associated resource file.
     */
    protected JMenu createMenu(String key, String[] itemKeys) {
        JMenu menu = new JMenu(getResourceString(key + labelSuffix));
        for (int i = 0; i < itemKeys.length; i++) {
            if (itemKeys[i].equals("-")) {
                menu.addSeparator();
            } else {
                JMenuItem mi = createMenuItem(itemKeys[i]);
                
                mi.setFont(font);
                menu.add(mi);
            }
        }
        URL url = getResource(key + imageSuffix);
        if (url != null) {
            menu.setHorizontalTextPosition(JButton.RIGHT);
            menu.setIcon(new ImageIcon(url));
        }
        return menu;
    }

    /**
     * This is the hook through which all menu items are
     * created.  It registers the result with the menuitem
     * hashtable so that it can be fetched with getMenuItem().
     * @see #getMenuItem
     * @param cmd The key in the resource file to serve as the basis
     *  of lookups.
     */
    protected JMenuItem createMenuItem(String cmd) {

        String subMenu = getResourceString(cmd + menuSuffix);
        if (subMenu != null) {
            String[] itemKeys = tokenize(subMenu);
            return createMenu(cmd, itemKeys);
        } else {
            JMenuItem mi;
            Object item = specialItems.get(cmd);
            if (item instanceof JMenuItem) {

                mi = (JMenuItem) item;
                mi.setText(getResourceString(cmd + labelSuffix));
                //mi.setEnabled(true);
                //System.out.println("item "+getResourceString(cmd + labelSuffix));
            } else
                mi = new JMenuItem(getResourceString(cmd + labelSuffix));
            //System.out.println("item "+getResourceString(cmd + labelSuffix));
            URL url = getResource(cmd + imageSuffix);
            if (url != null) {
                mi.setHorizontalTextPosition(JButton.RIGHT);
                mi.setIcon(new ImageIcon(url));
            }
            String accel = getResourceString(cmd+accelSuffix);
            if (accel != null) {
                try {
                    int mask = 0;
                    if (accel.startsWith("CTRL")) {
                        mask += ActionEvent.CTRL_MASK;
                        accel = accel.substring(5);
                    }
                    if (accel.startsWith("SHIFT")) {
                        mask += ActionEvent.SHIFT_MASK;
                        accel = accel.substring(6);
                    }
                    if (accel.startsWith("ALT")) {
                        mask += ActionEvent.ALT_MASK;
                        accel = accel.substring(4);
                    }
                    int key = KeyEvent.class.getField("VK_"+accel).getInt(null);
                    mi.setAccelerator(KeyStroke.getKeyStroke(key, mask));
                } catch (Exception e) {
                    // ignore
                }
            }

            String mnemonic = getResourceString(cmd+mnemonicSuffix);
            if (mnemonic != null && mnemonic.length() > 0)
                mi.setMnemonic(mnemonic.toCharArray()[0]);
            if (item == null) {
                String astr = getResourceString(cmd + actionSuffix);
                if (astr == null) {
                    astr = cmd;
                }

                mi.setActionCommand(astr);
                Action a = getAction(astr+actionSuffix);
                if (a != null) {
                    //System.out.println("Menu item Action "+a + " isEnabled "+ a.isEnabled());
                    mi.addActionListener(new EventRedirector(a));
                    a.addPropertyChangeListener(createActionChangeListener(mi));
                    mi.setEnabled(a.isEnabled());
                } else {
                    mi.setEnabled(false);
                }
            }
            menuItems.put(cmd, mi);
            return mi;
        }
    }

    /**
     * Create the toolbar.  By default this reads the
     * resource file for the definition of the toolbar.
     */
    public Component createToolBar(String str) {
        JToolBar toolbar = new JToolBar(str);
       
        toolbar.putClientProperty("JToolBar.isRollover", Boolean.TRUE);
	
        toolbar.setFloatable(true);
	toolbar.setBorderPainted(true);

        String[] tool = tokenize(getResourceString(str));
        for (int i = 0; i < tool.length; i++) {
            if (tool[i].equals("-")) {
                toolbar.addSeparator();
            } else {
                toolbar.add(createToolButton(tool[i]));
            }
        }
        toolbar.add(Box.createHorizontalGlue());

	String orientation = appProperties.getProperty(str +""+"Orientation");
        int orientationInt = JToolBar.HORIZONTAL;
	try{
	    orientationInt = Integer.parseInt(orientation); 
	    toolbar.setOrientation(orientationInt);
	}
        
	catch(NumberFormatException e)
	    {
		toolbar.setOrientation(orientationInt);
	    }         
	catch(IllegalArgumentException e)
	    {
		toolbar.setOrientation(JToolBar.HORIZONTAL);
	    }
	
	return toolbar;
    }

    /**
     * Create a button to go inside of the toolbar.  By default this
     * will load an image resource.
     * @param key The key in the resource file to serve as the basis
     *  of lookups.
     */


    public Component createToolButton(String key) {
        Object button;
        AbstractButton b = null;
        URL url;
        url = getResource(key + imageSuffix);
        button= buttons.get(key);
        if (button instanceof JComponent &&
                !(button instanceof AbstractButton)&&!(button==null))
            return (JComponent) button;
        else if (button instanceof AbstractButton) {
            b = (AbstractButton) button;
            b.setIcon(new ImageIcon(url));
        } else {
            b = new JButton(new ImageIcon(url)) {
                public float getAlignmentY() { return 0.5f; }
            };
        }
        b.setMargin(new Insets(1,1,1,1));
        b.setRequestFocusEnabled(false);
        if (button == null) {
            String act = getResourceString(key + actionSuffix);
            if (act == null) {
                act = key;
            }
            Action a = getAction(act+actionSuffix);
            //System.out.println("action "+ a + "  " + act);
            if (a != null) {
                b.setActionCommand(act);
                b.addActionListener(new EventRedirector(a));
                //System.out.println("tool button Action "+a + " isEnabled "+ a.isEnabled());
                b.setEnabled(a.isEnabled());
                a.addPropertyChangeListener(createActionChangeListener(b));
            } else {
                b.setEnabled(false);
            }
        }

        String tip = getResourceString(key + tipSuffix);
        if (tip != null) {
            b.setToolTipText(tip);
        }

        return b;
    }

    /**
     * Create's the right mouse button menu
     */
    public JPopupMenu createPopupMenu(){
        popupMenu = new JPopupMenu();
        String[] tool = tokenize(getResourceString("popup1"));
        for (int i = 0; i < tool.length; i++) {
            if (tool[i].equals("-")) {
                popupMenu.addSeparator();
            } else {
                popupMenu.add(createMenuItem(tool[i]));
            }
        }

        return popupMenu;
    }

    /** Invoked after the selection or the model has changed. */
    public void update() {
        //undoAction.update();
        //redoAction.update();
        portLeftAction.update();
        portRightAction.update();
        propertiesCAction.update();
        zoomInAction.update();
        zoomOutAction.update();
        marqueeHandler.update();
        removePAction.update();

        Frame f = getFrame();
        if (f != null)
            setTitle(getDocumentTitle());

    }

    /** Add this documents listeners to the specified graph. */
    protected void registerListeners(ProcessGraph graph) {
        graph.getSelectionModel().addGraphSelectionListener(this);
        graph.getModel().addGraphModelListener(this);
        graph.getView().addObserver(this);
        graph.addPropertyChangeListener(this);

    }

    /** Remove this documents listeners from the specified graph. */
    protected void unregisterListeners(ProcessGraph graph) {
        graph.getSelectionModel().removeGraphSelectionListener(this);
        graph.getModel().removeGraphModelListener(this);
        graph.removePropertyChangeListener(this);
    }

    /** Remove all the states from the graph*/
    protected void resetUndoManager() {
        processFactory.getCurrentProcess().getUndoManager().discardAllEdits();
        //undoAction.update();
        //redoAction.update();
    }

    /**
     * Stores the current L&F
     */
    public void updateLookAndFeel(String laf) {
        if(currentLookAndFeel != laf) {
            currentLookAndFeel = laf;
            try {
                UIManager.setLookAndFeel(currentLookAndFeel);

            }
            catch (Exception ex) {
                System.out.println("Failed loading L&F: " + currentLookAndFeel);
                System.out.println(ex);
            }
        }
    }
    
    /** Set the scale specified at the graph*/
    public void setScale(double scale) {
        scale = Math.max(Math.min(scale, 1024), .01);
        setScale(scale);
    }

    /**
     * Returns the instance of <code>FCMarqueeHandler</code>
     */
    public FCMarqueeHandler getMarqueeHandler(){
        return marqueeHandler;
    }

    /** Returns the Action with the specified name*/
    public Action getAction(String cmd) {
        return (Action)commands.get(cmd);
    }

    /**
     * Fetch the menu item that was created for the given
     * command.
     * @param cmd  Name of the action.
     * @returns item created for the given command or null
     *  if one wasn't created.
     */
    protected JMenuItem getMenuItem(String cmd) {
        return (JMenuItem) menuItems.get(cmd);
    }

    
    /**
     * Returns the menubar of the application
     */
    protected JMenuBar getMenubar() {
        return menubar;
    }

    /**
     * Create a status bar
     */
    protected FCStatusBar createStatusbar() {
        status = new FCStatusBar();
	runAction.addPropertyChangeListener(status);
	compileAction.addPropertyChangeListener(status);
        return status;
    }

    /**
     * return the status bar.
     */
    public FCStatusBar getStatusBar(){
	return status;
    }
    

    /**
     * Returns the flowcomposer's ressource bundle
     */
    public ResourceBundle getResource(){
        return resource;
    }

    /**
     * Take the given string and make it up into a series
     * of strings on whitespace boundries.  This is useful
     * for trying to get an array of strings out of the
     * resource file.
     */
    protected static String[] tokenize(String input) {
        Vector v = new Vector();
        StringTokenizer t = new StringTokenizer(input);
        String cmd[];

        while (t.hasMoreTokens())
            v.addElement(t.nextToken());
        cmd = new String[v.size()];
        for (int i = 0; i < cmd.length; i++)
            cmd[i] = (String) v.elementAt(i);

        return cmd;
    }

    protected String getResourceString(String nm) {
        String str;
        try {
            str = resource.getString(nm);
        } catch (MissingResourceException mre) {
            str = null;
        }
        return str;
    }


    protected URL getResource(String key) {
        String name = getResourceString(key);
        if (name != null) {
            URL url = this.getClass().getClassLoader().getResource(name);
            return url;
        }
        return null;
    }


    /** Return the title of the editor frame as a string. */
    protected String getDocumentTitle() {

        String s = (file != null) ? file.getAbsolutePath() : tmpFilename;
        if(modified)
            s = s + " *";

        return s;

    }

    /** Return the status of this document as a string. */
    protected String getDocumentStatus() {

        String s = null;

        /*int n = graph.getSelectionCount();
        if (n>0)
        s=n+" "+selectedString;
        else {
        int c = graph.getModel().getRootCount();
        if (c == 0) {
        s = emptyString;
        } else {
        s = c+" ";
        if (c > 1)
        s+=cellsString;
        else
        s+=cellString;
        c = gt.getComponentCount(graph);
        s = s + " / "+c+" ";
        if (c > 1)
        s+=componentsString;
        else
        s+=componentString;
        }
        }*/
        return s;
    }



    /***************************************************************************************************
     ************************************** Implementation of Actions************************************
     ***************************************************************************************************/

    protected UndoAction undoAction = new UndoAction();
    protected RedoAction redoAction = new RedoAction();

    protected PortRightAction portRightAction = new PortRightAction(this);
    protected PortLeftAction portLeftAction = new PortLeftAction(this);
    protected ZoomOutAction zoomOutAction =  new ZoomOutAction(this);
    protected ZoomInAction zoomInAction =  new ZoomInAction(this);
    protected PropertiesCAction propertiesCAction = new PropertiesCAction(this);
    protected RunAction runAction = new RunAction();
    protected CompileAction compileAction = new CompileAction();
    protected RemovePAction removePAction = new RemovePAction();

    protected Action[] actions = {
        undoAction,
        redoAction,
        portRightAction,
        portLeftAction,
        new LoadAction(this),
        zoomInAction,
        new ZoomAction(this),
        zoomOutAction,
        new ImportAction(),
        new ExportAction(),
        new SaveAction(),
        new SaveAsAction(),
        new ExitAction(),
        new DeleteAction(),
        new OptionAction(),
        new NewTAction(),
        runAction,
        compileAction,
        new DebugAction(),
        new ExpandTAction(),
        new PropertiesTAction(),
        new NewPAction(),
        removePAction,
	new PropertiesPAction(),
	new ExpandCAction(),
        propertiesCAction,
        new PropertiesCHAction(),
        // new RedrawAction(),
        new AdjustAction(),
        new InverseAction(),
        new LoadLAction(),
        new UnloadAction(),
        new HelpAction(),
	new AboutAction(),
	new DefexternAction(),
	new DefparametersAction()

	
    };
   
    /** Create special menu items */
    protected Hashtable createSpecialItems() {
        Hashtable items = new Hashtable();
        //cycle detection
        cycleMenuItem = new JCheckBoxMenuItem("Detection",false);
        cycleMenuItem.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
               if (cycleMenuItem.isSelected()){
                    marqueeHandler.setDetected(true);
                    marqueeHandler.detectCycle();
               }
               else
                    marqueeHandler.setDetected(false);
               
            }
        });        
        // automatic redraw
        redrawMenuItem = new JCheckBoxMenuItem("Layout",false);
        redrawMenuItem.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {

                ProcessGraph currentProcess = processFactory.getCurrentProcess();
		
		if ((currentProcess.getTouchLayout()).isRunning())
                    (currentProcess.getTouchLayout()).stop();
                else
                    (currentProcess.getTouchLayout()).start();
            }
        });
        items.put("cycle",cycleMenuItem);
        items.put("redraw", redrawMenuItem);
        // List Process Item
        list = new JMenu("List");
        //list.setEnabled(true);
        items.put("list",list);

        return items;
    }



    /** Create special buttons */
    protected void createSpecialButtons() {
        String[] pourcentage = {"1600%","800%","400%","200%","100%","50%","25%"};

	final ProcessGraph currentProcess  = processFactory.getCurrentProcess();
        zoom = new JComboBox(pourcentage);
        zoom.setMaximumSize(new Dimension(16,20));
        zoom.setSelectedIndex(4);
        zoom.setEditable(false);
        zoom.setAlignmentX(Component.LEFT_ALIGNMENT);
        zoom.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox)e.getSource();
                String chaine = (String)cb.getSelectedItem();
                String scale =Utils.getScale(chaine);

                int value = (Integer.parseInt(scale)/100);
                //getGraph().setScale( Double.valueOf(scale)/100);
                if (currentProcess.getSelectionCell() != null)
                    currentProcess.scrollCellToVisible(currentProcess.getSelectionCell());

            }
        });
        buttons.put("zoomButton",zoom);
    }

    /************************************************************************/
    /*                             Listeners                                */
    /************************************************************************/

    /** 
     * Implementation of <code>PropertyChangeListener</code> interface.
     * Listen all <code>Action</code> properties change.
     *
     * @param evt The PropertyChangeEvent
     */
    
    public void propertyChange(PropertyChangeEvent evt) {
        //System.out.println("PropertyChange " + evt);
        update();
    }

    /** 
     * Implementation of <code>GraphSelectionListener</code> interface.
     * Listen all current selection change.
     *
     * @param e The GraphSelectionEvent
     */
    public void valueChanged(GraphSelectionEvent e) {
	ProcessGraph currentProcess = processFactory.getCurrentProcess();
        if (!currentProcess.isSelectionEmpty()) {
            (currentProcess.getTouchLayout()).setDamper(0);}
        update();
    }

    /** 
     * Implementation of <code>Observer</code> interface.
     * Observe all the graph view.
     *
     * @param obs the object observed
     * @param arg an argument passed to the <code>notifyObservers</code> method
     */
    public void update(Observable obs, Object arg) {
        //System.out.println("View " + obs + "   " + arg);
	ProcessGraph currentProcess = processFactory.getCurrentProcess();
        (currentProcess.getTouchLayout()).resetDamper();
        modified = true;
        update();
    }
  
    /** 
     * Implementation of <code>GraphModelListener</code> interface.
     * Observe all change int a <code>GraphModel</code>
     *
     * @param e the <code>GraphModelEvent</code>
     */

    public void graphChanged(GraphModelEvent e) {
        //System.out.println("GraphModel" + e);
	ProcessGraph currentProcess = processFactory.getCurrentProcess();
        Object[] inserted = e.getChange().getInserted();

        if (inserted != null && inserted.length > 0)
        {
            currentProcess.setSelectionCells(inserted);
        }
        (currentProcess.getTouchLayout()).resetDamper();
        modified = true;
        update();
    }

    /** 
     * Implementation of <code>ActionListener</code> interface.
     * 
     * @param e the <code>ActionEvent</code>
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof JMenuItem) {
            JMenuItem item = (JMenuItem)e.getSource();
            tabProcess.setSelectedIndex(tabProcess.indexOfTab(item.getText()));

        }
    }

    /** 
     * Implementation of <code>ComponentListener</code> interface.
     * Called if this component is hidded. 
     *
     * @param e the <code>ComponentEvent</code>
     */
    public void componentHidden(ComponentEvent e) { }
    
     /** 
     * Implementation of <code>ComponentListener</code> interface.
     * Called if this component has moved. 
     *
     * @param e the <code>ComponentEvent</code>
     */
    public void componentMoved(ComponentEvent e) {

    }

    /** 
     * Implementation of <code>ComponentListener</code> interface.
     * Called if this component has ben resized. 
     *
     * @param e the <code>ComponentEvent</code>
     */
    public void componentResized(ComponentEvent e) {

        viewPortRect = sp.getViewport().getBounds();
        List processList = processFactory.getAllProcess();
        java.util.Iterator it = processList.iterator();
        while (it.hasNext()) {
            ProcessGraph process = (ProcessGraph)it.next();
            process.resizeComponent();
        }
        if (e.getComponent() instanceof JToolBar) {
            JToolBar toolBar = (JToolBar)e.getComponent();
	    
	    this.appProperties.setProperty(toolBar.getName()+"Orientation",
					   String.valueOf(toolBar.getOrientation()));	    
	    
	    // System.out.println(toolBar.getName());
            //System.out.println(toolBar.getOrientation());
           // System.out.println(toolBar.getComponentOrientation());
            //System.out.println(toolBar.isRollover());
            //System.out.println(toolBar.isFloatable());

        }
    }

    /** 
     * Implementation of <code>ComponentListener</code> interface.
     * Called if this component is shown. 
     *
     * @param e the <code>ComponentEvent</code>
     */
    public void componentShown(ComponentEvent e) {
        componentResized(e);
    }

    /** This will change the source of the actionevent to graph. */
    protected class EventRedirector implements ActionListener {

        protected Action action;

        public EventRedirector(Action a) {
            this.action = a;
        }

        public void actionPerformed(ActionEvent e) {
	    ProcessGraph currentProcess = processFactory.getCurrentProcess();
	    e = new ActionEvent(currentProcess,
                    e.getID(),
                    e.getActionCommand(),
                    e.getModifiers());
            action.actionPerformed(e);
        }

    }

    /** 
     * Factory method which creates the PropertyChangeListener used to 
     * update the ActionEvent source as properties change on its Action instance.
     */
    protected PropertyChangeListener createActionChangeListener(AbstractButton b) {
        return new ActionChangedListener(b);
    }

    /** A "PropertyChange" event gets fired whenever a bean changes a "bound" property*/
    protected class ActionChangedListener implements PropertyChangeListener {
        AbstractButton button;

        /**
         * Constructs an actionchangedlistener for menu's button
         * @param b the button
         */
        ActionChangedListener(AbstractButton b) {
            super();
            button = b;
        }
        
        /** This method gets called when a bound property is changed.*/
        public void propertyChange(PropertyChangeEvent e) {

            String propertyName = e.getPropertyName();
            if (e.getPropertyName().equals(Action.NAME) &&
                    button instanceof JMenuItem) {
                String text = (String) e.getNewValue();
                button.setText(text);
            } else if (propertyName.equals("enabled")) {
                Boolean enabledState = (Boolean) e.getNewValue();
                button.setEnabled(enabledState.booleanValue());
            }
        }
    }


    /** hearing about undoable operations*/
    class UndoHandler implements UndoableEditListener {
        ProcessGraph process;

        /** Constructs an empty undohandler*/
        public UndoHandler() {
        }
        /** Constructs an undohandler with the specified <code>ProcessGraph</code>*/
        public UndoHandler(ProcessGraph process){
            setProcess(process);
        }


        /**
         * Messaged when the Document has created an edit, the edit is
         * added to <code>undo</code>, an instance of UndoManager.
         */
        public void undoableEditHappened(UndoableEditEvent e) {
            ((ProcessGraph)process).getUndoManager().addEdit(e.getEdit());
            //undoAction.update();
            //redoAction.update();
        }

        /** Set the process*/
        public void setProcess(ProcessGraph process){
            this.process = process;
        }
        
        /** Returns the process*/
        public ProcessGraph getProcess(){
            return process;
        }
    }


    /** Create an array of the buttons of the toolbar*/
    protected Hashtable createButtons() {
        Hashtable buttons = new Hashtable();


        if (marqueeHandler instanceof FCMarqueeHandler) {
            ButtonGroup group = new ButtonGroup();
            FCMarqueeHandler mh = marqueeHandler;
            group.add(mh.marquee); buttons.put("Marquee", mh.marquee);
            group.add(mh.generate); buttons.put("Generate", mh.generate);
            group.add(mh.serialize); buttons.put("Serialize", mh.serialize);
            group.add(mh.merge); buttons.put("Merge", mh.merge);
            group.add(mh.dispatch); buttons.put("Dispatch", mh.dispatch);
            group.add(mh.applyExternal); buttons.put("ApplyExternal", mh.applyExternal);
            group.add(mh.applyProcess); buttons.put("ApplyProcess", mh.applyProcess);
            group.add(mh.applyRuleset); buttons.put("ApplyRuleset", mh.applyRuleset);
            group.add(mh.applyQuery); buttons.put("ApplyQuery", mh.applyQuery);
            //group.add(mh.EdgeTest); buttons.put("EdgeTest", mh.EdgeTest);
            group.add(mh.edgeNormal); buttons.put("EdgeNormal", mh.edgeNormal);
            //group.add(mh.EdgeIterator); buttons.put("EdgeIterator", mh.EdgeIterator);
            //group.add(mh.EdgeNull); buttons.put("EdgeNull", mh.EdgeNull);
            mh.marquee.doClick();
        }

        return buttons;
    }

    /** displays the right mouse button menu at the specified location*/
    public void showPopup(MouseEvent e,Point position){
	ProcessGraph currentProcess = processFactory.getCurrentProcess();
        mouseRightX = position.x;
        mouseRightY = position.y;
        if ((currentProcess.getSelectionCount()==1)&&(!(currentProcess.getSelectionCell() instanceof DefaultEdge))) {
            popupMenu = createPopupMenu();
            popupMenu.show(e.getComponent(),position.x,position.y);
        }
    }
   
    /**
    * Return true if the user really wants to close. Gives chance to save work.
    */
    public boolean close(){

        int r = JOptionPane.NO_OPTION;
        if (modified)
            r = JOptionPane.showConfirmDialog(getFrame(), getResourceString("SAVE_LAST_CHANGES"),
                    title, JOptionPane.YES_NO_CANCEL_OPTION);

        if (r == JOptionPane.YES_OPTION)
            new SaveAction().actionPerformed(null);
        if (r != JOptionPane.CANCEL_OPTION){
            instances.remove(this);
            getFrame().dispose();
            if (instances.isEmpty()) {

                try{
                    FileOutputStream out = new FileOutputStream("appProperties");
                    appProperties.store(out, "---No Comment---");
                    out.close();
                }
                catch(IOException ex){}
                System.exit(0);
            } // end of if (instances.isEmpty())

            return true;
        }

        return false;


    }


    /** Creates an adapter class for receiving window events*/
    protected WindowAdapter createCloser() {
        return new FCWindowListener();
    }

    /** class for receiving window events especially the closing*/
    protected final class FCWindowListener extends WindowAdapter{
        /** Invoked when a window is in the process of being closed.*/
        public  void windowClosing(WindowEvent e){
            new CloseAction().actionPerformed(null);
        }
    }

    /** Action Close application*/
    class CloseAction extends AbstractAction{
        CloseAction() {
            super("close"+actionSuffix);
        }

        public void actionPerformed(ActionEvent e) {
            close();
        }
    }

    /** Action Undo for undoable operations*/
    class UndoAction extends AbstractAction {


        public UndoAction() {
            super("undo"+actionSuffix);
            setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {
	    ProcessGraph currentProcess = processFactory.getCurrentProcess();
            try {

                if(currentProcess!= null & (currentProcess).getUndoManager()!=null)

                {
                    (currentProcess).getUndoManager().undo(currentProcess.getView());

                }
            } catch (CannotUndoException ex) {
                // System.out.println("Unable to undo: " + ex);
                ex.printStackTrace();
            }
            update();
            //redoAction.update();

        }

        protected void update() {

	    /*ProcessGraph currentProcess = processFactory.getCurrentProcess();
            
	    if(currentProcess!= null & (currentProcess).getUndoManager()!=null){
                if((currentProcess).getUndoManager().canUndo(currentProcess.getView())) {
                    //setEnabled(true);
                    setEnabled(false);
                    putValue(Action.NAME, (currentProcess).getUndoManager().getUndoPresentationName());
                }
                else {

                    setEnabled(false);
                    putValue(Action.NAME, "Undo");
                }
            }*/

        }
    }

    class RedoAction extends AbstractAction {
        public RedoAction() {
            super("redo"+actionSuffix);
            setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {
	    ProcessGraph currentProcess = processFactory.getCurrentProcess();
            try {

                if(currentProcess!= null & (currentProcess).getUndoManager()!=null)
                {
                    (currentProcess).getUndoManager().redo(currentProcess.getView());

                }

            } catch (CannotRedoException ex) {
                ex.printStackTrace();
            }
            update();
            //undoAction.update();

        }

        protected void update() {
	    ProcessGraph currentProcess = processFactory.getCurrentProcess();
            if(currentProcess!= null & (currentProcess).getUndoManager()!=null){
                if((currentProcess).getUndoManager().canRedo(currentProcess.getView())) {
                    setEnabled(true);
                    putValue(Action.NAME, (currentProcess).getUndoManager().getRedoPresentationName());
                }
                else {
                    setEnabled(false);
                    putValue(Action.NAME, "Redo");
                }
            }
        }
    }

    protected static String currentDirectory = "";

    /**
     * Get the value of currentDirectory.
     * @return value of currentDirectory.
     */
    public String getCurrentDirectory() {return currentDirectory;}

    /**
     * Set the value of currentDirectory.
     * @param v  Value to assign to currentDirectory.
     */
    public void setCurrentDirectory(String  v) {
        this.currentDirectory = v;
        appProperties.setProperty("directory",v);
    }


    class SaveAction extends AbstractAction {

        SaveAction(){
            super("save"+actionSuffix);
        }

        SaveAction(String msg) {
            super(msg);
        }

        public void actionPerformed(ActionEvent e){

            boolean ok =true;

            if(file == null)
            {
                File fileChoosen = GUITools.saveDialog(FlowComposer.this, currentDirectory,new XMLFilter());
                if(fileChoosen != null)
                {
                    setFile(fileChoosen);
                    String filename = file.getAbsolutePath();
                    if(filename.equals(""))
                    {
                        setFile(null);
                        return;
                    }
                }
                else
                {
                    ok = false;
                    setFile(tmpFile);
                }
                setTitle();
            }

            if(ok){
		
		/*if (modified == false) {
		    status.setMessage(resource.getString("NOTHING_TO_SAVE"));
		    return;
		}*/

		status.setMessage(resource.getString("BEGIN_SAVE"));
	

                PrintStream outTmp = System.out;
                PrintStream errTmp = System.err;

                String directory = file.getParentFile().getAbsolutePath();
                PrintStream out =null;

                try{
                    out = new PrintStream(new BufferedOutputStream(new FileOutputStream(file.getAbsolutePath())));
                }
                catch(FileNotFoundException ex)
                {
                    Alert.show(null,"file " +file.getAbsolutePath()+"is not found","WARNING",JOptionPane.WARNING_MESSAGE);
                    return;
                }

                System.setOut(out);
                System.setErr(out);
                processFactory.getTransmorpher().generateXML();
                out.close();
                System.setOut(outTmp);
                System.setErr(errTmp);

                String filename = file.getAbsolutePath();
                String name = file.getName();

                if(filename.indexOf('.') != -1)
                    name = filename.substring(filename.lastIndexOf(File.separatorChar)+1,filename.indexOf('.'));

                //generating RDF   
								try{
                generateFileRDF(directory + ""+File.separator +""+name+".rdf");
                }
								catch(Exception ioEx){
								}
                setCurrentDirectory(file.getParentFile().getAbsolutePath());
                modified = false;
		setTitle();
		status.setMessage(name +" "+resource.getString("END_SAVE"));
           }

        }


    }

    class SaveAsAction extends SaveAction {

        SaveAsAction() {
            super("saveAs"+actionSuffix);
        }

        public void actionPerformed(ActionEvent e) {
            
            tmpFile = file;
            file = null;
            super.actionPerformed(e);
        }
    }

    class LoadAction extends AbstractAction {
        private FlowComposer flow;

        LoadAction(FlowComposer flow) {
            super("load"+actionSuffix);
            this.flow=flow;
        }

        public void actionPerformed(ActionEvent e) {

            File fileSelected = GUITools.openDialog(FlowComposer.this, currentDirectory,new XMLFilter());
            
	    if(fileSelected != null)
            {
                if(fileSelected.exists())
                {
		    
                    if (processFactory.isEditing())
                    {
                        newEditor(fileSelected);
                    }
                    else
                    {
			status.setMessage(resource.getString("BEGIN_LOAD"));
			
                        setFile(fileSelected);
                        flow.loadXML();
                        
                        File fileRDF = new File(file.getAbsolutePath().substring(0,file.getAbsolutePath().lastIndexOf('.'))+".rdf");

                        if(fileRDF.exists())
                        {
                            ParserRDF rdfParser = new ParserRDF(processFactory,0);
                            rdfParser.newParse(fileRDF.getAbsolutePath());
                        }
                        else
                        {
                            processFactory.layout();
			}
			modified = false;
			setTitle();
                    }
                    setCurrentDirectory(fileSelected.getParentFile().getAbsolutePath());
		    status.setMessage(resource.getString("END_LOAD"));
                }
                else{
		    status.setMessage(resource.getString("FILE_NOT_FOUND"));
                }
            }
        }
    }


    class PortRightAction extends AbstractAction {
        private FlowComposer flow;

        PortRightAction(FlowComposer flow) {
            super("portright"+actionSuffix);
            this.flow=flow;
        }

        public void actionPerformed(ActionEvent e) {
            if(processFactory.getCurrentProcess()!=null)
                ((ProcessGraph)processFactory.getCurrentProcess()).newPort(FCConstants.PORTOUTPUT);
        }

        protected void update(){
            if (processFactory.getCurrentProcess() != null) {
                Object[] tmp = ((ProcessGraph)processFactory.getCurrentProcess()).getSelectionCells();
                if ( processFactory.isMain(processFactory.getCurrentProcess()) && (tmp.length == 0) ){
                    setEnabled(false);
                }
                else{
                    if (tmp.length!=0) {
                        int i=0;
                        boolean accept = true;
                        DefaultGraphCell cell;
                        while((i <tmp.length)&&(accept)){
                            cell = (DefaultGraphCell)tmp[i];
                            if (cell instanceof DefaultEdge){accept=false;}
                            else if (!((cell instanceof DispatchCell)||(cell instanceof ApplyProcessCell))){accept=false;}
                            i++;
                        }
                        if (accept) setEnabled (true);
                        else setEnabled(false);
                    }
                    else setEnabled(true);
                }
            }
            else setEnabled(false);
        }

    }

    class PortLeftAction extends AbstractAction {
        private FlowComposer flow;

        PortLeftAction(FlowComposer flow) {
            super("portleft"+actionSuffix);
            this.flow=flow;
        }

        public void actionPerformed(ActionEvent e) {
            if(processFactory.getCurrentProcess()!=null)
                ((ProcessGraph)processFactory.getCurrentProcess()).newPort(FCConstants.PORTINPUT);
        }

        protected void update(){
            if (processFactory.getCurrentProcess() != null) {
                Object[] tmp = ((ProcessGraph)processFactory.getCurrentProcess()).getSelectionCells();
                if ( processFactory.isMain(processFactory.getCurrentProcess()) && (tmp.length == 0) ){
                    setEnabled(false);
                }
                else{
                    if (tmp.length!=0) {
                        int i=0;
                        boolean accept = true;
                        DefaultGraphCell cell;
                        while((i<tmp.length)&&(accept)){
                            cell = (DefaultGraphCell)tmp[i];
                            if (cell instanceof DefaultEdge){accept=false;}
                            else if(!((cell instanceof MergeCell)||(cell instanceof ApplyProcessCell))){accept=false;}
                            i++;
                        }
                        if (accept) setEnabled (true);
                        else setEnabled(false);
                    }
                    else setEnabled(true);
                }
            }
            else setEnabled(false);
        }


    }

    class ZoomAction extends AbstractAction {

        private FlowComposer flow;
        ZoomAction(FlowComposer flow) {
            super("zoom"+actionSuffix);
            setEnabled(true);
            this.flow=flow;
        }

        public void actionPerformed(ActionEvent e) {
	    ProcessGraph currentProcess = processFactory.getCurrentProcess();
            double []zoomSizes = currentProcess.getZoom();
            currentProcess.setScale(1.0);
            Dimension dim = new Dimension(currentProcess.getWorkX(),currentProcess.getWorkY());
            int scale = currentProcess.getZoomCurrent();
            currentProcess.setZoomCurrent(2);

            currentProcess.setPreferredSize(new Dimension(dim.width,dim.height));
            currentProcess.resizeComponent();
            if (!(currentProcess.isMain()))
                currentProcess.updateCellProcess();

            status.setMessageScale((String)zoomMessage[2]);
        }
    }

    class ZoomInAction extends AbstractAction {
        private FlowComposer flow;
        ZoomInAction(FlowComposer flow) {
            super("zoomIn"+actionSuffix);
            setEnabled(true);
            this.flow=flow;
        }

        public void actionPerformed(ActionEvent e) {
	    ProcessGraph currentProcess = processFactory.getCurrentProcess();
            double []zoomSizes = currentProcess.getZoom();
            Dimension dim = new Dimension(currentProcess.getWorkX(),currentProcess.getWorkY());
            int scale = currentProcess.getZoomCurrent();
            currentProcess.setZoomCurrent(scale+1);
            currentProcess.setScale(zoomSizes[scale+1]);
            currentProcess.setPreferredSize(new Dimension((int)(dim.width*zoomSizes[scale+1]),(int)(dim.height*zoomSizes[scale+1])));
            currentProcess.resizeComponent();
            if (!(currentProcess.isMain()))
                currentProcess.updateCellProcess();
            status.setMessageScale((String)zoomMessage[scale+1]);
            // peut etre a mettre
            //if ((flow.getGraph()).getSelectionCell() != null)
            //  (flow.getGraph()).scrollCellToVisible((flow.getGraph()).getSelectionCell());
        }


        protected void update(){
	    ProcessGraph currentProcess = processFactory.getCurrentProcess();
            if (currentProcess != null) {
                int scale = currentProcess.getZoomCurrent();
                if (scale==5) setEnabled(false);
                else setEnabled(true);
            }
        }
    }


    class ZoomOutAction extends AbstractAction {
        private FlowComposer flow;
        ZoomOutAction(FlowComposer flow) {
            super("zoomOut"+actionSuffix);
            setEnabled(false);
            this.flow=flow;
        }

        public void actionPerformed(ActionEvent e) {
            //Dimension dim = currentProcess.getPreferredSize();
	    ProcessGraph currentProcess = processFactory.getCurrentProcess();
            double []zoomSizes = currentProcess.getZoom();
            Dimension dim = new Dimension(currentProcess.getWorkX(),currentProcess.getWorkY());
            int scale = currentProcess.getZoomCurrent();
            currentProcess.setZoomCurrent(scale-1);

            currentProcess.setScale(zoomSizes[scale-1]);
            currentProcess.setPreferredSize(new Dimension((int)(dim.width*zoomSizes[scale-1]),(int)(dim.height*zoomSizes[scale-1])));
            currentProcess.resizeComponent();
            if (!(currentProcess.isMain()))
                currentProcess.updateCellProcess();
            status.setMessageScale((String)zoomMessage[scale-1]);

        }

        protected void update() {
	    ProcessGraph currentProcess = processFactory.getCurrentProcess();
            if (currentProcess != null) {
                int scale = currentProcess.getZoomCurrent();
                if (scale==0) setEnabled(false);
                else setEnabled(true);
            }

        }

    }

    class ImportAction extends AbstractAction {

        ImportAction() {
            super("import"+actionSuffix);
            setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {
	    ProcessGraph currentProcess = processFactory.getCurrentProcess();
            
	    File filename = GUITools.saveDialog(FlowComposer.this, currentDirectory,new SAVFilter());
            
	    /*
	      if (filename != null) {
	      try {
	      XMLEncoder enc = new XMLEncoder(new BufferedOutputStream(
	      new FileOutputStream(filename)));
	      //enc.writeObject(processFactory.getArchiveableState());
	      enc.writeObject(currentProcess.getArchiveableState());
              enc.close();
	      modified = false;
	      } catch (Exception ex) {
	      GUITools.error(FlowComposer.this, ex.toString(), title);
	      } finally {
	      update();
	      invalidate();
	      }
	      }
	    */
        }
    }

    class ExportAction extends AbstractAction {
        
        ExportAction() {
            super("export"+actionSuffix);
            setEnabled(false);
        }
        
        public void actionPerformed(ActionEvent e) {
	    ProcessGraph currentProcess = processFactory.getCurrentProcess();
            
	    /*File filename = GUITools.openDialog(FlowComposer.this, currentDirectory,new SAVFilter());
            
	    if (filename != null) {
               // newEditor(filename);
                try {
                    XMLDecoder dec = new XMLDecoder(new BufferedInputStream(
                    new FileInputStream(filename)));
                    currentProcess.setArchivedState(dec.readObject());
                    dec.close();
                }
                catch (IOException eio) {
                    GUITools.error(FlowComposer.this, eio.toString(), title);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                    GUITools.error(FlowComposer.this, ex.getMessage(), title);
                }
	    
            }*/
        }
    }


    class ExitAction extends AbstractAction {

        ExitAction() {
            super("exit"+actionSuffix);
            setEnabled(true);
        }

        public void actionPerformed(ActionEvent e) {

            Object[] list = instances.toArray();
            for (int i=0;i<list.length;i++) {

                FlowComposer flow = (FlowComposer)list[i];
                flow.close();

            }// end of for (int i=0;i<list.length;i++)

            if (instances.isEmpty())
            {
                try{
                    FileOutputStream out = new FileOutputStream("appProperties");
                    appProperties.store(out, "---No Comment---");
                    out.close();
                }
                catch(IOException ex){}

                System.exit(0);
            }
        }
    }
		
		class DefexternAction extends AbstractAction {

        DefexternAction() {
            super("extern"+actionSuffix);
            setEnabled(true);
        }

        public void actionPerformed(ActionEvent e) {
					new ExternalDialog(FlowComposer.this,processFactory.getTransmorpher().getDefexterns());
				}
    }
		
		class DefparametersAction extends AbstractAction {

        DefparametersAction() {
            super("parameters"+actionSuffix);
            setEnabled(true);
        }

        public void actionPerformed(ActionEvent e) {
					new ParametersDialog(FlowComposer.this);
				}
    }
		
    class DeleteAction extends AbstractAction {

        DeleteAction() {
            super("delete"+actionSuffix);
            setEnabled(true);
        }

        public void actionPerformed(ActionEvent e) {

	    ProcessGraph currentProcess = processFactory.getCurrentProcess();
            Object[] tmp = currentProcess.getSelectionCells();
            currentProcess.delete(tmp);
        }
    }

    class OptionAction extends AbstractAction {

        OptionAction() {
            super("option"+actionSuffix);
            setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {

        }
    }

    class NewTAction extends AbstractAction {

        NewTAction() {
            super("newT"+actionSuffix);
            setEnabled(true);
        }

        public void actionPerformed(ActionEvent e) {
            newEditor();

        }
    }

    class RunAction extends AbstractAction {
	
        RunAction() {
            super("run"+actionSuffix);
        }

        public void actionPerformed(ActionEvent e) {
	    setEnabled(false);
	    
	    System.gc();
	    setCursor(new Cursor(Cursor.WAIT_CURSOR));	    
	    Thread run = new Thread(new MyRunnable());
	    run.start();
	    System.gc();
	}

	private class MyRunnable implements Runnable{
	    
	    public void run(){
		try{
		    
		    if (showExecutionParameter) {
			 
		    }			    
		    console.init();
		    console.startReader();
		    processFactory.runTransmorpher();
		    //		    console.stop();
		    FlowComposer.this.runAction.setEnabled(true);
		    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		}
		catch(Exception ex)
		    {ex.printStackTrace();}
	    }
	}
    }

    class CompileAction extends AbstractAction {

        CompileAction() {
            super("compile"+actionSuffix);
            setEnabled(true);
        }

        public void actionPerformed(ActionEvent e) {
            console.init();
            console.startReader();
	    processFactory.compileTransmorpher();
	    console.stop();
        }
    }

    class DebugAction extends AbstractAction {

        DebugAction() {
            super("debug"+actionSuffix);
            setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {

        }
    }

    class ExpandTAction extends AbstractAction {

        ExpandTAction() {
            super("expandT"+actionSuffix);
            setEnabled(false);

        }
        public void actionPerformed(ActionEvent e) {

        }
    }

    class PropertiesTAction extends AbstractAction {

        PropertiesTAction() {
            super("propertiesT"+actionSuffix);
            setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {

        }
    }

    class NewPAction extends AbstractAction {

        NewPAction() {
            super("newP"+actionSuffix);
            setEnabled(true);
        }

        public void actionPerformed(ActionEvent e) {
	               
	    processFactory.newProcess(FCConstants.PROCESS);
	    
	}
    }
    

    class RemovePAction extends AbstractAction{

	RemovePAction(){
	    super("removeP"+actionSuffix);
	    setEnabled(false);
	}
	
	public void actionPerformed(ActionEvent e){
	    
	    processFactory.removeCurrentProcess();
	}

	public void update(){
	    
	    if ( processFactory != null && processFactory.getCurrentProcess() != null )
		{
		    setEnabled(! (processFactory.getCurrentProcess().isMain()) );
		}
	}
    }
    

    class PropertiesPAction extends AbstractAction {

        PropertiesPAction() {
            super("propertiesP"+actionSuffix);
            setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {

        }
    }


    class ExpandCAction extends AbstractAction {

        ExpandCAction() {
            super("expandC"+actionSuffix);
            setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {

        }
    }

    class PropertiesCAction extends AbstractAction {

        private FlowComposer flow;

        PropertiesCAction(FlowComposer flow) {
            super("propertiesC"+actionSuffix);
            this.flow = flow;
        }

        public void actionPerformed(ActionEvent e) {
	    
	    ProcessGraph currentProcess = processFactory.getCurrentProcess();
	    Point point = new Point(mouseRightX,mouseRightY);
            if(currentProcess.getSelectionCell() != null) {
                new CallProperties(flow,((FCGraphCell)currentProcess.getSelectionCell()).getUserObject(),point);
            }
        }

        protected void update(){
	    ProcessGraph currentProcess = processFactory.getCurrentProcess();
	    
            if((currentProcess != null)&&(currentProcess.getSelectionCount()==1)&&(currentProcess.getSelectionCell() instanceof FCGraphCell))
                setEnabled(true);
            else   setEnabled(false);
        }

    }

    class PropertiesCHAction extends AbstractAction {

        PropertiesCHAction() {
            super("propertiesCH"+actionSuffix);
            setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {

        }
    }

    class AdjustAction extends AbstractAction {

        AdjustAction() {
            super("adjust"+actionSuffix);
            //setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {
            ProcessGraph currentProcess = processFactory.getCurrentProcess();
            if (currentProcess.isMain()){
            }
            else {
            }    
        }
    }

    class InverseAction extends AbstractAction {

        InverseAction() {
            super("inverse"+actionSuffix);
            setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {

        }
    }

    class LoadLAction extends AbstractAction {

        LoadLAction() {
            super("loadL"+actionSuffix);
            setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {

        }
    }

    class UnloadAction extends AbstractAction {

        UnloadAction() {
            super("unload"+actionSuffix);
            setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {

        }
    }

    class HelpAction extends AbstractAction {

        HelpAction() {
            super("help"+actionSuffix);
            setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {

        }
    }

    class AboutAction extends AbstractAction{
	
	AboutAction(){
	    super("about"+actionSuffix);
	    setEnabled(true);
	}

	public void actionPerformed(ActionEvent e){

	    JFrame aboutFrame = new JFrame("About FlowComposer");
	    aboutFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    aboutFrame.setSize(375,485);
	    aboutFrame.setResizable(false);
	   
	    Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	    Dimension us = aboutFrame.getSize();
	    int x = (screen.width - us.width)/2;
	    int y = (screen.height - us.height)/2;
	    aboutFrame.setLocation(x,y);
	    
	    URL logoFlux = getResource("logoFlux");
	    URL logoInria = getResource("logoInria");

	    Container content = aboutFrame.getContentPane();
	    String htmlText = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n"+
        "<html>\n"+
        "<head>\n"+
        "<title>About FlowComposer</title>"+
        "</head>\n"+
        "<body>"+
        "<div align=\"center\">"+
        "<h1>"+fr.fluxmedia.flowcomposer.utils.Version.NAME+"</h1>"+
        "version:"+fr.fluxmedia.flowcomposer.utils.Version.VERSION+"<br>"+
        "URL : "+fr.fluxmedia.flowcomposer.utils.Version.URL+"<br>"+
        "Contact : "+fr.fluxmedia.flowcomposer.utils.Version.CONTACT+"<br>"+
        fr.fluxmedia.flowcomposer.utils.Version.RIGHTS+"<br>"+
        "</div>"+
        "<hr>"+
        "<div align=\"center\">"+
        "<h1>"+fr.fluxmedia.transmorpher.utils.Version.NAME+"</h1>"+
        "version:"+ fr.fluxmedia.transmorpher.utils.Version.VERSION+"<br>"+
        "URL :"+fr.fluxmedia.transmorpher.utils.Version.URL+"<br>"+
        "Contact :"+fr.fluxmedia.flowcomposer.utils.Version.TRANSMO_CONTACT+"<br>"+
        "</div>"+
        "<hr>"+
        "<div align=\"center\">"+
        "<h3>Contributors</h3>"+
        "Guillaume Chomat<br>"+
        "Frederic Saint-Marcel<address<br>"+
        "</div>"+
        "<div align=\"center\">"+
        "<table cellspacing=\"2\">"+
        "<tr>"+
        "<td><img width=\"146\" height=\"36\" src=\""+logoFlux.toString()+"\"></img></td>"+
        "<td><img width=\"146\" height=\"36\" src=\""+logoInria.toString()+"\"></img></td>"+
        "</tr>"+
        "</table>"+
        "</div>"+
        "</body>"+
        "</html>";

	    JEditorPane aboutPane = new JEditorPane("text/html",htmlText); 
	    aboutPane.setEditable(false);
	    content.add(aboutPane);
	    aboutFrame.setVisible(true);
    }
  }
}
