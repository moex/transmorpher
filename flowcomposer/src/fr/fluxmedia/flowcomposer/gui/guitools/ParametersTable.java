/**
 *
 *$Id: ParametersTable.java,v 1.3 2002-12-12 10:25:58 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.gui.guitools;

import javax.swing.*;
import fr.fluxmedia.transmorpher.utils.*;
import java.util.*;
import javax.swing.table.*;
import java.lang.*;
import java.awt.event.*;
import javax.swing.event.*;
import java.awt.* ;


public class ParametersTable extends JTable{

    protected DefaultTableModel tableModel ; 
    
    public ParametersTable(Parameters parameters){	
	super();
	tableModel = new ParametersTableModel(parameters);
	setModel(tableModel);
    }


    /** Represent the data of our ParametersTable. This data is structured by the schema name/value.
     *  for this data, we've decided, for the moment, to use two arrayList in sync.
     * 
     */
    class ParametersTableData{
	
	protected ArrayList names;
	protected ArrayList values;

	public ParametersTableData(){
	    names = new ArrayList();
	    values = new ArrayList();
	}
	
	public Object getNameAt(int index){
	    return names.get(index);
	}
	
	public Object getValueAt(int index){
	    return values.get(index);
	}

	public void setValueAt(int row, Object v){
	     values.set(row,v);
	}
	
	public void setNameAt(int row, Object v){
	    names.set(row,v);
	}
	
	public void addParameter(Object name, Object value){
	    names.add(names.size(),name);
	    values.add(values.size(),value);
	}
	
	public void setParameter(Object name, Object value, int row){
	    names.set(row,name);
	    values.set(row,value);
	}
	
	public void removeParameter(int index){
	    names.remove(index);
	    values.remove(index);
	}

	public int size(){
	    return names.size();
	}
	
    }

    /** A parameters table is a table with two column and n rows */	
  
    class ParametersTableModel extends DefaultTableModel {
        
	ParametersTableData data;

	final Object[] columnNames  = new String[]{"Name","Value"};
	final Class[] columnClasses = new Class[]{String.class,Object.class}; 

	public ParametersTableModel(){
	    this(null);
	}
	
	public ParametersTableModel(Parameters parameters){
	    
	    data = new ParametersTableData();
	    if (parameters != null) {
		
		Enumeration e = parameters.getNames();
		while (e.hasMoreElements()) {
		    Object name = e.nextElement();
		    data.addParameter(name,parameters.getParameter((String)name));
		}		
	    } // end of if (parameters != null)
	}

	public boolean isCellEditable(int row,int col){
            return true;
        }	
	
	public int getRowCount(){
	    if (data != null) {
		return data.size();
	    }
	    return 0;
	}

	public Object getValueAt(int row, int column){
	    
	    if (column == 0)
		return data.getNameAt(row);
	    else {
		return data.getValueAt(row);
	    }
	}

	public String getColumnName(int col){
	   
	    if (col < columnNames.length && col >= 0)
		return columnNames[col].toString(); 
	    
	    return null;	   
	}
	
	public Class getColumnClass(int index){
	    if ( index < columnClasses.length && index >= 0 )
		return columnClasses[index];
	    return String.class;
	}
	
	public int getColumnCount() { return columnNames.length; }

	public void setValueAt(Object v, int row, int col) {
	    if (col == 0) {
		data.setNameAt(row,v);
	    }
	    else {
		data.setValueAt(row,v);
	    }
	    fireTableCellUpdated(row, col);	    
	}

	public void addRow(Object[] data){
	    if (data != null && data.length == 2) {
		this.data.addParameter(data[0],data[1]);
		fireTableRowsInserted(getRowCount(),getRowCount());
	    }
	}
	
	public void removeRow(int index){
	    data.removeParameter(index);
	    fireTableRowsDeleted(index,index);
	}
	
    }
    
    

}
