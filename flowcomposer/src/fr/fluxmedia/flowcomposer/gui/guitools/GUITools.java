/**
 *
 *$Id: GUITools.java,v 1.2 2002-09-13 17:53:56 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.gui.guitools ;


import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.SwingConstants;
import java.awt.Component;



public class GUITools implements SwingConstants{


    /** Show a file open dialog and return the filename. */
    public static File openDialog(Component component, String directory, FileFilter filter) {
        return dialog(component, JFileChooser.OPEN_DIALOG,directory,filter);
    }

    /** Show a file save dialog and return the filename. */
    public static File saveDialog(Component component, String directory,FileFilter filter) {
        return dialog(component,JFileChooser.SAVE_DIALOG,directory,filter);
    }

    /** Show a dialog with the given error message. */
    public static void error(Component component, String message, String title) {
        JOptionPane.showMessageDialog(component, message, title, JOptionPane.ERROR_MESSAGE);
    }

    /** Open a dialog and return the file. Returns null if cancelled. */
    protected static File dialog(Component component, int mode,String directory,FileFilter filter) {
	File file = null ;
        JFileChooser f = new JFileChooser(directory);
        f.addChoosableFileFilter(filter);
        f.setDialogType(mode);
        int returnVal = (mode ==JFileChooser.OPEN_DIALOG)? f.showOpenDialog(component) :f.showSaveDialog(component) ;
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            file = f.getSelectedFile();
	}
        return file;
    }
    
    
    
}
