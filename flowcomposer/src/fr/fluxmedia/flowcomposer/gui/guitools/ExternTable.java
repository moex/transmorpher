/**
 *$Id: ExternTable.java,v 1.1 2002-12-12 10:25:58 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.flowcomposer.gui.guitools;
import fr.fluxmedia.transmorpher.utils.LinearIndexedStruct;
import java.awt.*;
import java.awt.event.*;

import java.util.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;


/**
 *  Description of the Class
 *
 *@author    triolet
 */
public class ExternTable extends JTable {

	/**
	 *  Description of the Field
	 */
	protected DefaultTableModel tableModel;

	/**
	 *Constructor for the ExternTable object
	 *
	 *@param  externs  Description of the Parameter
	 */
	public ExternTable(LinearIndexedStruct externs) {
		super();
		tableModel = new ExternTableModel(externs);
		this.setModel(tableModel);
	}

	/**
	 *  Description of the Class
	 *
	 *@author    triolet
	 */
	class ExternTableData {

		/**
		 *  Description of the Field
		 */
		protected ArrayList names;
		/**
		 *  Description of the Field
		 */
		protected ArrayList values;

		/**
		 *Constructor for the ParametersTableData object
		 */
		public ExternTableData() {
			names = new ArrayList();
			values = new ArrayList();
		}

		/**
		 *  Gets the nameAt attribute of the ParametersTableData object
		 *
		 *@param  index  Description of the Parameter
		 *@return        The nameAt value
		 */
		public Object getNameAt(int index) {
			return names.get(index);
		}

		/**
		 *  Gets the valueAt attribute of the ParametersTableData object
		 *
		 *@param  index  Description of the Parameter
		 *@return        The valueAt value
		 */
		public Object getValueAt(int index) {
			return values.get(index);
		}

		/**
		 *  Sets the valueAt attribute of the ParametersTableData object
		 *
		 *@param  row  The new valueAt value
		 *@param  v    The new valueAt value
		 */
		public void setValueAt(int row, Object v) {
			values.set(row, v);
		}

		/**
		 *  Sets the nameAt attribute of the ParametersTableData object
		 *
		 *@param  row  The new nameAt value
		 *@param  v    The new nameAt value
		 */
		public void setNameAt(int row, Object v) {
			names.set(row, v);
		}

		/**
		 *  Adds a feature to the Parameter attribute of the ParametersTableData object
		 *
		 *@param  name   The feature to be added to the Parameter attribute
		 *@param  value  The feature to be added to the Parameter attribute
		 */
		public void addExtern(Object name, Object value) {
			names.add(names.size(), name);
			values.add(values.size(), value);
		}

		/**
		 *  Sets the parameter attribute of the ParametersTableData object
		 *
		 *@param  name   The new parameter value
		 *@param  value  The new parameter value
		 *@param  row    The new parameter value
		 */
		public void setExtern(Object name, Object value, int row) {
			names.set(row, name);
			values.set(row, value);
		}

		/**
		 *  Description of the Method
		 *
		 *@param  index  Description of the Parameter
		 */
		public void removeExtern(int index) {
			names.remove(index);
			values.remove(index);
		}

		/**
		 *  Description of the Method
		 *
		 *@return    Description of the Return Value
		 */
		public int size() {
			return names.size();
		}

	}

	/**
	 *  Description of the Class
	 *
	 *@author    triolet
	 */
	class ExternTableModel extends DefaultTableModel {
		final Object[] columnNames = new String[]{"Type", "Class"};
		final Class[] columnClasses = new Class[]{String.class, String.class};
		ExternTableData data;

		/**
		 *Constructor for the ExternTableModel object
		 *
		 *@param  externs  Description of the Parameter
		 */
		public ExternTableModel(LinearIndexedStruct externs) {

			data = new ExternTableData();
			Enumeration e = externs.getKeys();
			while (e.hasMoreElements()) {
				Object name = e.nextElement();
				data.addExtern(name, externs.get((String)name));
				System.out.println(name+" "+externs.get((String)name));
			}
		}

		/**
		 *  Gets the cellEditable attribute of the ExternTableModel object
		 *
		 *@param  row  Description of the Parameter
		 *@param  col  Description of the Parameter
		 *@return      The cellEditable value
		 */
		public boolean isCellEditable(int row, int col) {
			return true;
		}

		/**
		 *  Gets the rowCount attribute of the ExternTableModel object
		 *
		 *@return    The rowCount value
		 */
		public int getRowCount() {
			if (data != null) {
				return data.size();
			}
			return 0;
		}

		/**
		 *  Gets the valueAt attribute of the ExternTableModel object
		 *
		 *@param  row     Description of the Parameter
		 *@param  column  Description of the Parameter
		 *@return         The valueAt value
		 */
		public Object getValueAt(int row, int column) {

			if (column == 0) {
				return data.getNameAt(row);
			} else {
				return data.getValueAt(row);
			}
		}

		/**
		 *  Gets the columnName attribute of the ExternTableModel object
		 *
		 *@param  col  Description of the Parameter
		 *@return      The columnName value
		 */
		public String getColumnName(int col) {

			if (col < columnNames.length && col >= 0) {
				return columnNames[col].toString();
			}

			return null;
		}

		/**
		 *  Gets the columnClass attribute of the ExternTableModel object
		 *
		 *@param  index  Description of the Parameter
		 *@return        The columnClass value
		 */
		public Class getColumnClass(int index) {
			if (index < columnClasses.length && index >= 0) {
				return columnClasses[index];
			}
			return String.class;
		}

		/**
		 *  Gets the columnCount attribute of the ExternTableModel object
		 *
		 *@return    The columnCount value
		 */
		public int getColumnCount() {
			return columnNames.length;
		}

		/**
		 *  Sets the valueAt attribute of the ExternTableModel object
		 *
		 *@param  v    The new valueAt value
		 *@param  row  The new valueAt value
		 *@param  col  The new valueAt value
		 */
		public void setValueAt(Object v, int row, int col) {
			if (col == 0) {
				data.setNameAt(row, v);
			} else {
				data.setValueAt(row, v);
			}
			fireTableCellUpdated(row, col);
		}

		/**
		 *  Adds a feature to the Row attribute of the ExternTableModel object
		 *
		 *@param  data  The feature to be added to the Row attribute
		 */
		public void addRow(Object[] data) {
			if (data != null && data.length == 2) {
				this.data.addExtern(data[0], data[1]);
				fireTableRowsInserted(getRowCount(), getRowCount());
			}
		}

		/**
		 *  Description of the Method
		 *
		 *@param  index  Description of the Parameter
		 */
		public void removeRow(int index) {
			data.removeExtern(index);
			fireTableRowsDeleted(index, index);
		}

	}
}

