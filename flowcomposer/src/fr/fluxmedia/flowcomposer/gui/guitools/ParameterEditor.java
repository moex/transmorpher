/**
 *
 *$Id: ParameterEditor.java,v 1.4 2002-12-12 10:25:58 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.gui.guitools;

import javax.swing.*;
import fr.fluxmedia.transmorpher.utils.*;
import java.util.*;
import javax.swing.table.*;
import java.lang.*;
import java.awt.event.*;
import javax.swing.event.*;
import java.awt.* ;
import java.io.File;

public class ParameterEditor extends AbstractCellEditor implements TableCellEditor,ActionListener{
	
    
    JTextField textField ; 
    JButton fileChooserButton ;
    
    protected int clickCountToStart = 2 ;
    protected String file ;

    public ParameterEditor(String file){
	textField = new JTextField(30); 
	fileChooserButton = new JButton("...");
	fileChooserButton.setPreferredSize(new Dimension(16,16));
	fileChooserButton.addActionListener(this);
	this.file = file ;
    }
        
    public Component getTableCellEditorComponent(JTable table,
						 Object value,
						 boolean isSelected,
						 int row,
						 int column)
    {
	TableModel model = table.getModel();
	textField.setText(model.getValueAt(row,column).toString());
		
	if ( column == 1 && model.getValueAt(row,0).equals(file) )
	    {
		    
		JPanel pane = new JPanel();
		pane.setLayout(new BorderLayout());
		pane.add(textField,BorderLayout.CENTER);
		pane.add(fileChooserButton,BorderLayout.EAST);
		   
		return pane;
	    }
	return textField;
    }

    public boolean isCellEditable(EventObject e){
	if (e instanceof MouseEvent) {
	    return ((MouseEvent)e).getClickCount() >= clickCountToStart;
	    }
	return true;
    }
    
    
    public Object getCellEditorValue(){
	return (textField == null) ? new String("") : textField.getText();
	}  
    
    public void actionPerformed(ActionEvent event){
	
	if (event.getSource().equals(fileChooserButton)) {
	    File file = GUITools.openDialog(null,"", new XMLFilter());
	    if (file != null) {
		textField.setText(file.getAbsolutePath());
	    }
	    stopCellEditing();
	}
	
    }
}
