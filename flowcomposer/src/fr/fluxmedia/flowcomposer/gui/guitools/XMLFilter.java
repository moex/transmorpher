/**
 *
 *$Id: XMLFilter.java,v 1.2 2002-09-13 17:53:56 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.gui.guitools;

import java.io.File;
import javax.swing.*;
import javax.swing.filechooser.*;
import fr.fluxmedia.flowcomposer.utils.*;

/**
 * The <code>XMLFilter</code> class provides a filter for the extension 
 * of file when we used a JFileChooser.
 *
 * @version 0.2
 * @author Chomat/Saint-Marcel
 */


public class XMLFilter extends FileFilter {

    /** Accept all directories and xml files.*/
    public boolean accept(File f) {
	
	boolean accept = false;
	
	String extension = Utils.getExtension(f);
	
	if (f.isDirectory() || (Utils.xml).equals(extension) || (Utils.XML).equals(extension)) {
            accept = true ;
        } 
	
        return accept ;
    }

    /** The description of this filter*/
    public String getDescription() {
        return "Fichiers XML (*.xml)";
    }

}
