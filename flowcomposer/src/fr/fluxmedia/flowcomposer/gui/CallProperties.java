/**
 *
 *$Id: CallProperties.java,v 1.10 2002-12-12 10:25:58 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.gui;



// import ProcessGraph
import com.jgraph.JGraph;
import com.jgraph.graph.*;

//import Transmorpher
import fr.fluxmedia.transmorpher.graph.Process;
import fr.fluxmedia.transmorpher.graph.*;
import fr.fluxmedia.transmorpher.utils.*;
import fr.fluxmedia.transmorpher.parser.ProcessParser;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import java.util.Map;
import java.util.Hashtable;
import java.util.ArrayList;
import java.util.List;
import javax.swing.event.*;
import javax.swing.filechooser.FileFilter;

import fr.fluxmedia.flowcomposer.utils.FCConstants;
import fr.fluxmedia.flowcomposer.graph.ProcessGraph;
import fr.fluxmedia.flowcomposer.gui.guitools.*;


import javax.swing.table.TableModel;
import java.util.Iterator;
import java.util.Collections;
import java.util.ResourceBundle;
import java.util.Enumeration;

import java.awt.*;
import java.awt.event.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.BorderLayout;
import java.io.File;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellEditor;
import java.util.EventObject;
import javax.swing.AbstractCellEditor;


/**
 * A CallProperties is a class which define the panel for editing the call's properties.
 * A call is a transformation's component.
 * This panel is composed by two panels. The first, attr, allow to edit this call's attributes.
 * The second, param, is a panel which contain a table.This table is a n*2 table (row*col).
 *
 * @version 0.2
 * @author Chomat/Saint-Marcel
 */
 
public class CallProperties extends JDialog implements ActionListener{
    
    protected Container contentPane;
    protected JPanel attr,param;
    
    //JTable for show parameters informations
    protected JTable  parametersTable = null;
    
    private DefaultTableModel tableModel = null;
    
    //Parameters Table Button
    protected JButton ok,cancel,newParam,remove,removeAll;
    
    //FIELD for ID
    protected JTextField idText;
        
    protected JComboBox typeChooser,refChooser,strategyChooser;
    
    protected JFileChooser fileChooser;
    protected JComponent fileChooserComponent;
    protected JTextField fileField;

    protected JLabel idJLabel,typeJLabel,refJLabel,strategyJLabel;
    
    //The call which is editing
    protected Call call;
    protected int type;
    
    protected Point location;
    
    protected Border etched;
    protected Font fontArial;

    protected ResourceBundle resource;
    protected FlowComposer flowcomposer;
    
    protected final int LABEL_LENGTH = 70;
    
    
    /**
     * Build and show a CallProperties object
     * @param flow instance <code>of FlowComposer</code> 
     * @param call the object Transmorpher associate
     * @param location where it showing
     */
    public CallProperties(FlowComposer flow, Object call, Point location){
        
        super(flow.getFrame(),"Call Properties",true);
        
        this.flowcomposer = flow;
        this.call = (Call)call;
        this.type = FCConstants.getType(call);
        
        resource = flowcomposer.getResource();
        
        this.location = location;
        this.setResizable(false);
        
        fontArial = new Font("Arial",Font.PLAIN,13);
        
        buildPropertiesPanel();
    }
    
    /**
     * @return JPanel with component and LEFT's FlowLayout
     */
    protected JPanel makeComponent(Object[] component) {
        
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout(FlowLayout.LEFT,20,5));
        for (int i=0;i<component.length;i++) {
            ((Container)component[i]).setFont(fontArial);
            panel.add((Container)component[i]);
        }
        return panel;
    }
       
    /**
     * @return The attributes panel
     */
    protected JPanel createAttributesPanel(Object attr){
        
        JPanel pane = new JPanel();
        StringParameters attributes;
        
        if (attr instanceof StringParameters) {
            
            attributes = (StringParameters)attr;
            pane.setLayout(new GridLayout(0,1));
            
            // id construction
            idJLabel = new JLabel(resource.getString("idLabel"));
            idJLabel.setPreferredSize(new Dimension(LABEL_LENGTH,20));
            
            idText = new JTextField(call.getId());
            idText.setPreferredSize(new Dimension(165,20));
            idText.addActionListener(this);
            Object[] compId = new Object[]{idJLabel,idText};
            pane.add(makeComponent(compId));
            
            //list of type. if null no type must be defined for this call.
            Object[] list = FCConstants.getListType(type);
            
            // type construction
            if(list != null){
                typeJLabel = new JLabel(resource.getString("typeLabel"));
                typeJLabel.setPreferredSize(new Dimension(LABEL_LENGTH,20));
                typeChooser = new JComboBox(list);
                typeChooser.setPreferredSize(new Dimension(165,20));
                Object value = ((Parameters)attributes).getParameter("type");
                typeChooser.setSelectedItem(value);
                typeChooser.setEditable(true);
                
                Object[] compType = new Object[]{typeJLabel,typeChooser};
                pane.add(makeComponent(compType));
            }
            
            // add in attributes panel
            if (FCConstants.isRef(call)) {
                
                refJLabel = new JLabel(resource.getString("refLabel"));
                refJLabel.setPreferredSize(new Dimension(LABEL_LENGTH,20));
                refChooser = new JComboBox(flowcomposer.getList(type));
                refChooser.setPreferredSize(new Dimension(165,20));
                Object value = ((Parameters)attributes).getParameter("ref");
                refChooser.setSelectedItem(value);
                refChooser.setEditable(true);
                
                Object[] compRef = new Object[]{refJLabel,refChooser};
                pane.add(makeComponent(compRef));
            } // end of if (FCContants.isRef(call))
            
            Object[] strategyList = FCConstants.getStrategyList(type);
            
            if (strategyList != null) {
                
                strategyJLabel = new JLabel(resource.getString("strategyLabel"));
                strategyJLabel.setPreferredSize(new Dimension(LABEL_LENGTH,20));
                strategyChooser = new JComboBox(strategyList);
                strategyChooser.setPreferredSize(new Dimension(165,20));
                Object value = ((Parameters)attributes).getParameter("strategy");
                strategyChooser.setSelectedItem(value);
                strategyChooser.setEditable(true);
                
                Object[] compStrategy = new Object[]{strategyJLabel,strategyChooser};
                pane.add(makeComponent(compStrategy));
            } // end of if (listStrategy != null)
            
            etched = BorderFactory.createEtchedBorder();
            Border titleAttr = BorderFactory.createTitledBorder(etched,resource.getString("attrTitle"),TitledBorder.DEFAULT_JUSTIFICATION,TitledBorder.DEFAULT_POSITION,fontArial);
            
            pane.setBorder(titleAttr);
            
            
        } // end of if (attr instanceof StringParameters)
        
        return pane;
    }
    
    /**
     * @return The parameters panel
     */
    protected JPanel createParametersPanel(Object param){
        
        JPanel pane = new JPanel();
        Parameters parameters;
        
        if (param instanceof Parameters) {
            
	    String file = flowcomposer.getResource().getString("FILE");

            parameters = (Parameters)param;
	    parametersTable = new ParametersTable(parameters);
	    parametersTable.addMouseListener(new MListener());
	    parametersTable.setDefaultEditor(Object.class, new ParameterEditor(file));    
	    tableModel = (DefaultTableModel)parametersTable.getModel();
	    
	    //parametersTable.getModel.addTableModelListener(new FCTableModelListener());

	    JScrollPane  scrollTable = new JScrollPane(parametersTable);
            scrollTable.setPreferredSize(new Dimension(215,115));
             
            newParam = new JButton(resource.getString("addTitle"));
            newParam.setFont(fontArial);
            newParam.addActionListener(this);
            
	    remove = new JButton(resource.getString("removeTitle"));
            remove.setFont(fontArial);
            remove.addActionListener(this);
            remove.setEnabled(false);
	    
	    removeAll = new JButton(resource.getString("removeAllTitle"));
            if (tableModel.getRowCount() > 0)
                removeAll.setEnabled(true);
            else removeAll.setEnabled(false);
	    removeAll.setFont(fontArial);
            removeAll.addActionListener(this);
            
            JPanel panel = new JPanel();
            panel.setLayout(new GridLayout(0,1,0,5));
            
            panel.add(newParam);
            panel.add(remove);
            panel.add(removeAll);
            
            Object[] compParam = new Object[]{scrollTable,panel};
            pane.add(makeComponent(compParam));
	    Border titleParam = BorderFactory.createTitledBorder(etched,resource.getString("paramTitle"),TitledBorder.DEFAULT_JUSTIFICATION,TitledBorder.DEFAULT_POSITION,fontArial);
            pane.setFont(fontArial);
            pane.setBorder(titleParam);
            
        } // end of if (param instanceof Parameters)
        
        return pane;
    }
    
    /**
     *  build the dialog depends on type and call
     */
    protected void buildPropertiesPanel(){
        
        contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());
        
        StringParameters attributes = call.getAttributes();
        Parameters parameters = call.getParameters();
        attr =  createAttributesPanel(attributes);
        param = createParametersPanel(parameters);
        
        ok = new JButton(resource.getString("okTitle"));
        ok.addActionListener(this);
        ok.setPreferredSize(new Dimension(85,25));
        ok.setFont(fontArial);
        //ok.setEnabled(false);
        cancel = new JButton(resource.getString("cancelTitle"));
        cancel.addActionListener(this);
        cancel.setPreferredSize(new Dimension(85,25));
        cancel.setFont(fontArial);
        JPanel pane = new JPanel();
        pane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        pane.add(ok);
        pane.add(cancel);
        //Object[] compPane = new Object[]{attr,param,pane};
        contentPane.add(attr,BorderLayout.NORTH);
        contentPane.add(param,BorderLayout.CENTER);
        contentPane.add(pane,BorderLayout.SOUTH);
        //Object[] comp = new Object[]{pane,ok,pane,cancel,pane};
        this.pack();
        this.setLocation(computeLocation(location));
        //this.setSize(300,500);
        this.setTitle(resource.getString("dialogTitle"));
        this.setVisible(true);
        
    }
    
    /**
     * @return The point where the CallProperties Dialog must be showed
     */
    public Point computeLocation(Point location){
        Point graphLocation = flowcomposer.getGraphLocationOnscreen();
        Point dialogLocation =null;
        if(location != null)
            dialogLocation = new Point(graphLocation.x+location.x,graphLocation.y+location.y);
        return dialogLocation;
        
    }
  
    /**
     * implements ActionListener Interface
     */
    public void actionPerformed(ActionEvent event){
        
        String id = idText.getText();
        int typeCall = FCConstants.getType(call);
	
	ProcessGraph currentProcess = flowcomposer.getProcessFactory().getCurrentProcess();
	Point location = new Point(1,1);

        if(event.getSource() == ok ) {
          
	    //for each component we must test if the id is single.
            if(!(id.equals(""))){
                //id != ""
                if(currentProcess != null && currentProcess.isSingleId(call,id)){
                     
		    if(parametersTable.getCellEditor() != null)
			parametersTable.getCellEditor().stopCellEditing();  
                    
		    Parameters newParameters = new Parameters();
                   
		    for (int i=0;i<tableModel.getRowCount();i++) {

                        String name = (String)tableModel.getValueAt(i,0);
                        Object value = tableModel.getValueAt(i,1);
                        if (name != null && ! (name.equals("")) )
			    newParameters.setParameter(name,value);
                    }
		
		    call.setParameters(newParameters);
                    call.setId(id);
                    
                    if ((typeCall==FCConstants.APPLYPROCESS)|| (typeCall==FCConstants.APPLYRULESET)) {
                        
                        ((ApplyImpl)call).setRef(refChooser.getSelectedItem().toString());
                        
                        if (typeCall==FCConstants.APPLYRULESET)
                            ((ApplyRuleset)call).setStrategy(strategyChooser.getSelectedItem().toString());
                        
                        this.dispose();
                    }
                    else {
                        call.setType(typeChooser.getSelectedItem().toString());
                        this.dispose();
                    }
                }
                else {
                    //System.out.println("not single");
                    //traitement d'erreur
                }
                
            }
            else{
                //id = ""
                //traitement d'erreur
            }
        }
        else if(event.getSource() == cancel){
            this.dispose();
        }

        else if(event.getSource() == newParam){
	    String[] stringNull= {"",""};
	    tableModel.addRow(stringNull);
        }
        
        else if(event.getSource() == remove){
            
	    int[] index = parametersTable.getSelectedRows();
	    int nb = 0;

	         
	    if(parametersTable.getCellEditor() != null)
		parametersTable.getCellEditor().stopCellEditing(); 

            for (int i = 0; i < index.length ; i++) {
		
		tableModel.removeRow(index[i]-nb);
		nb++;
	    }
	    
            if (tableModel.getRowCount() == 0) {
		removeAll.setEnabled(false);
            }
      	    remove.setEnabled(false);
            
        }
        else if(event.getSource() == removeAll){
            
	    int nb = tableModel.getRowCount();
            	         
	    if(parametersTable.getCellEditor() != null)
		parametersTable.getCellEditor().stopCellEditing(); 

	    for (int i = 0;i < nb;i++){
                tableModel.removeRow(0);
            }
           
            remove.setEnabled(false);
            removeAll.setEnabled(false);
	}
    }

    
    class FCTableModelListener implements TableModelListener{

	public void tableChanged(TableModelEvent event){
	    
	}
    }


    class MListener implements MouseListener {
        public void 	mouseClicked(MouseEvent e){
            if ( e.getClickCount() == 1 ){
                int index = parametersTable.getSelectedRow();		
		if ( index != -1 ) {
		    remove.setEnabled(true);
                }
                else {
		    remove.setEnabled(false);
                }
            }    	    	    
        }
        public void 	mousePressed(MouseEvent e){}
        public void 	mouseReleased(MouseEvent e){}
        public void 	mouseEntered(MouseEvent e){}
        public void 	mouseExited(MouseEvent e){}
        
    }

}
