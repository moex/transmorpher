/**
 * $Id: FCMarqueeHandler.java,v 1.14 2002-12-12 10:25:58 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.gui;

import fr.fluxmedia.transmorpher.graph.Call;

import com.jgraph.JGraph;
import com.jgraph.graph.*;

import fr.fluxmedia.transmorpher.graph.Process;
import fr.fluxmedia.transmorpher.graph.Port;
import fr.fluxmedia.transmorpher.graph.Channel;
import fr.fluxmedia.transmorpher.graph.*;
import fr.fluxmedia.transmorpher.utils.*;

import javax.swing.*;
import com.jgraph.JGraph;
import com.jgraph.graph.*;
import com.jgraph.plaf.GraphUI;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;

import fr.fluxmedia.flowcomposer.utils.*;
import fr.fluxmedia.flowcomposer.graph.*;

/**
 * MarqueeHandler that can insert cells.Marquee selection is the ability to select a rectangular region by use of the
 * mouse, which is not provided by Swing.The <code>FCMarqueeHandler</code> class
 * is used to implement this type of selection.
 *
 * @version 0.2 
 * @author Chomat/Saint-Marcel
 */

public class FCMarqueeHandler extends BasicMarqueeHandler {

    protected FlowComposer flowcomposer;

    protected PortView port;
    protected PortView sourcePort;
    protected PortView targetPort;
    protected boolean cycleSelected = false; 
    
    protected Call currentCall;
    protected FCGraphTools gt;
    public transient JToggleButton marquee = new JToggleButton();
    public transient JToggleButton generate = new JToggleButton();
    public transient JToggleButton serialize = new JToggleButton();
    public transient JToggleButton merge  = new JToggleButton();
    public transient JToggleButton dispatch = new JToggleButton();
    public transient JToggleButton applyExternal = new JToggleButton();
    public transient JToggleButton applyProcess = new JToggleButton();
    public transient JToggleButton applyRuleset = new JToggleButton();
    public transient JToggleButton applyQuery = new JToggleButton();
    //public transient JToggleButton edgeTest = new JToggleButton();
    public transient JToggleButton edgeNormal = new JToggleButton();
    //public transient JToggleButton edgeIterator = new JToggleButton();
    //public transient JToggleButton edgeNull = new JToggleButton();

    protected Point start, current;
    protected Object beginCell, endCell;

    /**
     * Constructs a marquee 
     *
     * @param instance of <code>FlowComposer</code>
     */
    public FCMarqueeHandler(FlowComposer flowcomposer){
        this.flowcomposer = flowcomposer;
        gt = new FCGraphTools();
    }

    /** Returns an instance of <code>FCGraphTools</code>*/ 
    public FCGraphTools getTools() {
        return gt;
    }    
    
    /** The cycle detection : running or not*/
    public  void setDetected(boolean bool) {
        cycleSelected =bool;
    }    
    
    /**
     * Updates the button for connecting two vertices (enable/disable)
     */
    public void update() {

	if (flowcomposer != null && flowcomposer.getProcessFactory() != null) {
	    
	    ProcessGraph process = flowcomposer.getProcessFactory().getCurrentProcess();
	    
	    if (process != null && process.canConnect())
		edgeNormal.setEnabled(true);
	    else edgeNormal.setEnabled(false);
	    
	}
    }

    /**
     * Returns the current point of mouse
     */
    public Point getCurrentPoint(){
        return current;
    }

    /** Returns the start point of mouse */
    public Point getStartPoint(){
        return start;
    }

    /** Rteurns the source port for a connection */
    public PortView getSourcePort(){
        return sourcePort;
    }

    /** Returns the target port for a connection */
    public PortView getTargetPort(){
        return targetPort;
    }

    /* Return true if this handler should be preferred over other handlers. */
    public boolean isForceMarqueeEvent(MouseEvent e) {
         return !marquee.isSelected() || super.isForceMarqueeEvent(e);
    }

    /** overrides the method with management of right button mouse*/
    public void mousePressed(MouseEvent e) {
        //System.out.println("MousePressed Start...");
	flowcomposer.getStatusBar().setMessage("") ;

        ProcessGraph currentProcess = flowcomposer.getProcessFactory().getCurrentProcess();
        if (e.isPopupTrigger()){

            if (!e.isConsumed() && marquee.isSelected()) {

                flowcomposer.showPopup(e,currentProcess.snap(e.getPoint()));
                return;
            }
	}
        else{
            if(currentProcess != null){
                if (!e.isConsumed() && !marquee.isSelected()) {
                    
		    start = currentProcess.snap(e.getPoint());
		    
		    double scale = currentProcess.getScale();
		    
		    if (port != null) {
			
			Object cell = port.getCell();
			Object userObject = ((DefaultGraphCell)port.getCell()).getUserObject();
			
			if (
			    (cell instanceof PortProcessCell && ((Port)userObject).getType() == Port.OUTPUT)
			    ||
			    (!(cell instanceof PortProcessCell) && ((Port)userObject).getType() == Port.INPUT)
			    )
			    {
				targetPort = port;
			    }
			else if ( 
				 (cell instanceof PortProcessCell && ((Port)userObject).getType() == Port.INPUT)
				 ||
				 (!(cell instanceof PortProcessCell) && ((Port)userObject).getType() == Port.OUTPUT)) 
			    {
				sourcePort = port;
			    }
			
			if ((edgeNormal.isSelected())&&(sourcePort != null)) {
			    start = sourcePort.getLocation(null);
			    start = new Point((int)(start.x*scale),(int)(start.y*scale));
			}
			else if (edgeNormal.isSelected() && targetPort != null) {
			    start = targetPort.getLocation(null);
			    start = new Point((int)(start.x*scale),(int)(start.y*scale));
			}
		    }
		    
		    e.consume();
		}
		
	    }
	}
	super.mousePressed(e);
	//System.out.println("MousePressed End...");
    }
	
    /** overrides method*/
    public void mouseDragged(MouseEvent event) {
        //System.out.println("MouseDragged Start...");
        ProcessGraph currentProcess = flowcomposer.getProcessFactory().getCurrentProcess();
        
	if(currentProcess != null){
            if (!event.isConsumed() && !marquee.isSelected()) {
                
		Graphics g = currentProcess.getGraphics();
                Color   bg = currentProcess.getBackground();
                Color   fg = Color.black;
                g.setColor(fg);
                g.setXORMode(bg);
                overlay(g);
                
		current = currentProcess.snap(event.getPoint());
                
		if (edgeNormal.isSelected()){
                    double scale = currentProcess.getScale();
                    port = currentProcess.getPortViewAt((int)(event.getX()/scale),(int)(event.getY()/scale));
                    if ( port != null ) {
			current = port.getLocation(null);
                        current = new Point((int)(current.x*scale),(int)(current.y*scale));
			/*if( ((Port)((DefaultGraphCell)port.getCell()).getUserObject()).getType() == Port.INPUT ){
			    targetPort = port;
			}
			else {
			    sourcePort = port;
			}*/
			
		    }
		}
                g.setColor(bg);
                g.setXORMode(fg);
                overlay(g);
                event.consume();
            }
        }
        super.mouseDragged(event);
        // System.out.println("MouseDragged End...");
    }

    /** overrides method for inserting cells in Transmorpher graph*/
    public void mouseReleased(MouseEvent event) {
	
	ProcessGraph currentProcess = flowcomposer.getProcessFactory().getCurrentProcess();
	if (event.isPopupTrigger()){
	    if (!event.isConsumed() && marquee.isSelected()) {
		flowcomposer.showPopup(event,currentProcess.snap(event.getPoint()));
		return;
	    }
	 }
	
        Call call;
	Process currentTransformation = (Process)currentProcess.getUserObject();
        current = currentProcess.snap(event.getPoint());
        String tmpName,name;

        if(currentProcess != null){
            //endCell= currentProcess.getFirstCellForLocation(event.getX(), event.getY());

            if (event != null && !event.isConsumed() && !marquee.isSelected()) {

                if (merge.isSelected())
                {
                    tmpName = FCConstants.NMERGE;
                    name = currentProcess.getDefaultCallName(tmpName,0);
                    call = new Merge(name,(String)FCConstants.MERGETYPE[0],currentTransformation);
                    call.inPorts().setPort(0, new Port("", call, 0 ,Port.INPUT));
                    call.outPorts().setPort(0, new Port("", call, 0 ,Port.OUTPUT));
                    currentTransformation.addCall(call);
		}
                else if (generate.isSelected())
                {
                    tmpName = FCConstants.NGENERATE;
                    name = currentProcess.getDefaultCallName(tmpName,0);
                    call = new Generate(name,(String)FCConstants.GENERATETYPE[0],"",currentTransformation);
                    call.outPorts().setPort(0, new Port("", call, 0 ,Port.OUTPUT));
                    currentTransformation.addCall(call);
		}
                else if (serialize.isSelected())
                {
                    tmpName = FCConstants.NSERIALIZE;
                    name = currentProcess.getDefaultCallName(tmpName,0);

                    call = new Serialize(name,(String)FCConstants.SERIALIZETYPE[0],"",currentTransformation);
                    call.inPorts().setPort(0, new Port("", call, 0 ,Port.INPUT));
                    currentTransformation.addCall(call);
		}
                else if (dispatch.isSelected())
                {
                    tmpName = FCConstants.NDISPATCH;
                    name = currentProcess.getDefaultCallName(tmpName,0);

                    call = new Dispatch(name,(String)FCConstants.DISPATCHTYPE[0],currentTransformation);
                    call.inPorts().setPort(0, new Port("", call, 0 ,Port.INPUT));
                    call.outPorts().setPort(0, new Port("", call, 0 ,Port.OUTPUT));
                    currentTransformation.addCall(call);
		}
                else if (applyExternal.isSelected())
                {
                    tmpName = FCConstants.NAPPLYEXTERNAL;
                    name = currentProcess.getDefaultCallName(tmpName,0);

                    call = new ApplyExternal(name,(String)FCConstants.APPLYEXTERNALTYPE[0],"",currentTransformation);
		    call.inPorts().setPort(0, new Port("", call, 0 ,Port.INPUT));
		    call.outPorts().setPort(0, new Port("", call, 0 ,Port.OUTPUT));
		    currentTransformation.addCall(call);
                }
                else if (applyProcess.isSelected())
                {
                    tmpName = FCConstants.NAPPLYPROCESS;
                    name = currentProcess.getDefaultCallName(tmpName,0);

                    call = new ApplyProcess(name,"",currentTransformation,"");
                    call.inPorts().setPort(0, new Port("", call, 0 ,Port.INPUT));
                    call.outPorts().setPort(0, new Port("", call, 0 ,Port.OUTPUT));
                    currentTransformation.addCall(call);
                    
                }
                else if (applyRuleset.isSelected())
                {
                    tmpName = FCConstants.NAPPLYRULESET;
                    name = currentProcess.getDefaultCallName(tmpName,0);

                    call = new ApplyRuleset(name,"",currentTransformation,"");
                    call.inPorts().setPort(0, new Port("", call, 0 ,Port.INPUT));
                    call.outPorts().setPort(0, new Port("", call, 0 ,Port.OUTPUT));
                    currentTransformation.addCall(call);
                    
                }
                else if (applyQuery.isSelected())
                {
                    tmpName = FCConstants.NAPPLYQUERY;
                    name = currentProcess.getDefaultCallName(tmpName,0);

                    call = new ApplyQuery(name,(String)FCConstants.APPLYQUERYTYPE[0],currentTransformation,"");
                    call.inPorts().setPort(0, new Port("", call, 0 ,Port.INPUT));
                    call.outPorts().setPort(0, new Port("", call, 0 ,Port.OUTPUT));
                    currentTransformation.addCall(call);
                }
                else if (edgeNormal.isSelected())
                {
		    java.util.Iterator itSource;
		    java.util.Iterator itTarget;
			
                    if ((sourcePort!=null)&&(port!=null)&&(sourcePort!=port)) {
                        
			targetPort = port;
		    }
		    else if ((targetPort!=null)&&(port!=null)&&(targetPort!=port)) {
			
			sourcePort = port;
		    }
		    
		    if (sourcePort != null && targetPort != null) {
			
			itSource = ((DefaultPort)sourcePort.getCell()).edges();
			itTarget = ((DefaultPort)targetPort.getCell()).edges();
			if ((Utils.lenght(itSource)<1)&&(Utils.lenght(itTarget)<1)) {
			    
                            DefaultGraphCell firstCell = (DefaultGraphCell)(sourcePort.getParentView()).getCell();
                            DefaultGraphCell cell = (DefaultGraphCell)(targetPort.getParentView()).getCell();
                            int sourceType = ((PortCell)sourcePort.getCell()).getType();
                            int targetType = ((PortCell)targetPort.getCell()).getType();
                            if (!(firstCell instanceof PortProcessCell)){
                                if (!(cell instanceof PortProcessCell)){
                                    if ((sourceType!=targetType)&&(firstCell!=cell)) {doConnection(sourcePort,targetPort);}
                                }
                                else if (sourceType==targetType) {doConnection(sourcePort,targetPort);}
                            }
                            else if (!(cell instanceof PortProcessCell)){
                                if (sourceType==targetType) {doConnection(sourcePort,targetPort);}
                            }
                            else if (sourceType!=targetType) {doConnection(sourcePort,targetPort);}
                        }
		    } // end of if (sourcePort != null && targetPort != null)
		}
		event.consume();
	    }
	    
	}
	marquee.doClick();
	sourcePort = null;
	targetPort = null;
	port = null;
	start = null;
	current = null;
	super.mouseReleased(event);
	updateGraphics();
	//System.out.println("MouseReleased End...");
    }

    /** overrides method*/
    public void mouseMoved(MouseEvent event) {
        //System.out.println("currentProcess " + flowcomposer.getProcessFactory().getCurrentProcess());
        ProcessGraph currentProcess = flowcomposer.getProcessFactory().getCurrentProcess();

        if(currentProcess!=null){
            if (!marquee.isSelected() && !event.isConsumed()) {
                currentProcess.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
                event.consume();

                if (edgeNormal.isSelected()) {

		    //PortView oldPort = port;
                    double scale=currentProcess.getScale();
                    PortView newPort = currentProcess.getPortViewAt((int)(event.getX()/scale),(int)(event.getY()/scale));
                    //if (oldPort != newPort) {
		    Graphics g = currentProcess.getGraphics();
		    Color   bg = currentProcess.getBackground();
		    Color   fg = currentProcess.getMarqueeColor();
		    g.setColor(fg);
		    g.setXORMode(bg);
		    overlay(g);
		    port = newPort;
		    g.setColor(bg);
		    g.setXORMode(fg);
		    overlay(g);
		    //}
		}
	    }
	    
	}
        super.mouseMoved(event);
    }

    
    private void updateGraphics(){
        if(flowcomposer.getProcessFactory().getCurrentProcess() != null)
            flowcomposer.getProcessFactory().getCurrentProcess().getModel().insert(null,null,null,null);
    }

    /** Makes the connection of two vertices with a edge in Transmorpher graph*/
    protected void doConnection(PortView sourcePort,PortView targetPort) {

        ProcessGraph currentProcess = flowcomposer.getProcessFactory().getCurrentProcess();   
        
        DefaultGraphCell portIn = (DefaultGraphCell)sourcePort.getCell();
        DefaultGraphCell portOut = (DefaultGraphCell)targetPort.getCell();
	
	String name = targetPort.getParentView().getCell().toString();
	if (name == null || name.equals(""))
	    name = sourcePort.getParentView().getCell().toString();
	if (name == null || name.equals(""))
	    name = currentProcess.toString();

	currentProcess.newChannel(portIn.getUserObject() , portOut.getUserObject(),name);	
        if (cycleSelected) {
            detectCycle();
        }    
   }

    /** 
      * Cycle detection in graph : Selection of cells the which do not form 
      * part of possible circuit.
      */
    public void detectCycle(){
         ProcessGraph currentProcess = flowcomposer.getProcessFactory().getCurrentProcess(); 
         Object[] cells = gt.getSpanningTree(currentProcess,null);
         currentProcess.setSelectionCells(cells);
    }
    /** Draw the temporary line of the edge when we want to connect two vertices*/ 
    public void overlay(Graphics g) {
        super.overlay(g);

            previewPort(flowcomposer.getProcessFactory().getCurrentProcess().getGraphics());
            if (start != null) {
                if ((edgeNormal.isSelected()) && current != null)
                    g.drawLine(start.x, start.y, current.x, current.y);
            }

    }

    /** Hightlights (green color) the port when the connection is available*/
    protected void previewPort(Graphics g) {

	if (port != null) {
            
	    java.util.Iterator it = ((DefaultPort)port.getCell()).edges();
	   
	    PortView firstPort = null;

	    if ( sourcePort != null ) {
	        firstPort = sourcePort;
	    }
	    else if ( targetPort != null ) {
		firstPort = targetPort;
	    }
	    	      
            if ( firstPort != null &&  firstPort != port ){
		
		java.util.Iterator itFirst = ((DefaultPort)firstPort.getCell()).edges();
                if ((Utils.lenght(it)<1)&&(Utils.lenght(itFirst)<1)) {
                    
		    DefaultGraphCell firstCell = (DefaultGraphCell)(firstPort.getParentView()).getCell();
                    DefaultGraphCell cell = (DefaultGraphCell)(port.getParentView()).getCell();
                    int firstType = ((PortCell)firstPort.getCell()).getType();
                    int type = ((PortCell)port.getCell()).getType();
                    
		    if (!(firstCell instanceof PortProcessCell)){
                        if (!(cell instanceof PortProcessCell)){
                            if ((firstType!=type)&&(firstCell!=cell)) { paintPort(g);}
                        }
                        else if (firstType==type) { paintPort(g);}
                    }
                    else if (!(cell instanceof PortProcessCell)){
                        if (firstType==type) { paintPort(g);}
                    }
                    else if (firstType!=type) { paintPort(g);}
                }
	    } 
	    else{
                if ((Utils.lenght(it)<1)) {
                    paintPort(g);
                }
            }
        }
    }

    private void paintPort(Graphics g) {
        boolean offset = (GraphConstants.getOffset(port.getAttributes()) != null);
        Rectangle r = (offset) ? port.getBounds(): port.getParentView().getBounds();
        r = flowcomposer.getProcessFactory().getCurrentProcess().toScreen(new Rectangle(r));
        int s = 3;
        r.translate(-s, -s);
        r.setSize(r.width+2*s, r.height+2*s);
        flowcomposer.getProcessFactory().getCurrentProcess().getUI().paintCell(g, port, r, true);
    }

}

