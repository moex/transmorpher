/**
 *
 *$Id: FCStatusBar.java,v 1.3 2002-09-13 17:53:56 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.gui;

import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;


/** A status bar for warning message and other informations : scale*/

public class FCStatusBar extends JPanel implements PropertyChangeListener{
    
    protected JLabel message;
    protected JLabel messageScale;
    protected JLabel messageWorkArea;
    
    protected FCMemoryStatus memoryStatus ;
    
    protected JProgressBar progressBar ;
    
    Font font = new Font("Arial",Font.PLAIN,13);

    Dimension dim ;

    /** Constructs status bar*/
    public FCStatusBar() {
	super();
	
	//setBackground(Color.lightGray);
	setLayout(new BorderLayout());

	Border border = BorderFactory.createLoweredBevelBorder() ;	
	FontMetrics fm = getFontMetrics(UIManager.getFont("Label.font"));
	
	Box messageBox = new Box(BoxLayout.X_AXIS);
	messageWorkArea = new JLabel(" ");
	messageWorkArea.setBorder(border);	
	
	progressBar = new JProgressBar();
	//	progressBar.setBorder(border);
	dim = progressBar.getPreferredSize();
	dim.width += 50;
	dim.height = messageWorkArea.getPreferredSize().height;
	progressBar.setPreferredSize(dim);
	//progressBar.setIndeterminate(true);
	progressBar.setString("");
	progressBar.setStringPainted(true);
	
	messageBox.add(messageWorkArea);
	messageBox.createHorizontalStrut(3);
	messageBox.add(progressBar);
	
	Box box = new Box(BoxLayout.X_AXIS) ;
	messageScale = new JLabel(" ");
	messageScale.setFont(font);
	messageScale.setBorder(border);

	memoryStatus = new FCMemoryStatus() ;
	memoryStatus.setBorder(border) ;
	dim = memoryStatus.getPreferredSize();
	dim.width += fm.stringWidth("999Mb/999Mb");
	dim.height = messageScale.getPreferredSize().height;
	memoryStatus.setPreferredSize(dim);
	
	box.add(messageScale);
	box.createHorizontalStrut(3);
	box.add(memoryStatus) ;

	message = new JLabel(" ");
	message.setBorder(border);

	add(messageBox, BorderLayout.WEST);
	add(message, BorderLayout.CENTER);
	add(box, BorderLayout.EAST);	
    }
    
    /**
     * Get the value of message.
     * @return value of message.
     */
    public String getMessage(){
	
	return message.getText();
    }
    
    /**
     * Set the value of message.
     * @param v  Value to assign to message.
     */
    public void setMessage(String v) {
	this.message.setText(v);
    }
    
    /**
     * Get the value of messageScale.
     * @return value of messageScale.
     */
    public String getMessageScale() {
	return messageScale.getText();
    }
    
    /**
     * Set the value of messageScale.
     * @param v  Value to assign to messageScale.
	   */
    public void setMessageScale(String  v) {
	this.messageScale.setText(v);
    }
    
    /**
     * Get the value of messageWorkArea.
     * @return value of messageWorkArea.
     */
    public String getMessageWorkArea() {
	return messageWorkArea.getText();
    }
    
    /**
     * Set the value of messageWorkArea.
     * @param v  Value to assign to messageWorkArea.
     */
    public void setMessageWorkArea(String  v) {
	this.messageWorkArea.setText(v);
    }
    
    
    /** Main method*/
    public void paint(Graphics g) {
            super.paint(g);
    }
     
    //{{{ FCMemoryStatus class
    class FCMemoryStatus extends JComponent implements ActionListener
	{
	    private Timer timer;
	    
	    //MemoryStatus constructor
	    public FCMemoryStatus()
	    {
		FCMemoryStatus.this.setDoubleBuffered(true);
		FCMemoryStatus.this.setForeground(UIManager.getColor("Label.foreground"));
		FCMemoryStatus.this.setBackground(UIManager.getColor("Label.background"));
		FCMemoryStatus.this.setFont(UIManager.getFont("Label.font"));
	    }
	    
	    //addNotify() method
	    public void addNotify()
	    {
		super.addNotify();
		timer = new Timer(2000,this);
		timer.start();
		ToolTipManager.sharedInstance().registerComponent(this);
	    }
	    
	    //removeNotify() method
	    public void removeNotify()
	    {
		super.removeNotify();
		timer.stop();
		ToolTipManager.sharedInstance().unregisterComponent(this);
	    } 
	    
	    //getToolTipText() method
	    public String getToolTipText()
	    {
		Runtime runtime = Runtime.getRuntime();
		int freeMemory = (int)(runtime.freeMemory() / 1024);
		int totalMemory = (int)(runtime.totalMemory() / 1024);
		int usedMemory = (totalMemory - freeMemory);
		Integer[] args = { new Integer(usedMemory),
				   new Integer(totalMemory) };
		return args[0]+"/"+args[1];
	    } 
	    
	    //actionPerformed() method
	    public void actionPerformed(ActionEvent evt)
	    {
		FCMemoryStatus.this.repaint();
	    }
	    
	    //paintComponent() method
	    public void paintComponent(Graphics g)
	    {
		Insets insets = this.getBorder().getBorderInsets(this);
		
		Runtime runtime = Runtime.getRuntime();
		int freeMemory = (int)(runtime.freeMemory() / 1024);
		int totalMemory = (int)(runtime.totalMemory() / 1024);
		int usedMemory = (totalMemory - freeMemory);
		
		int width = this.getWidth()
		    - insets.left - insets.right;
		
		float fraction = ((float)usedMemory) / totalMemory;
		
		g.setColor(UIManager.getColor("ProgressBar.selectionBackground"));
		
		g.fillRect(insets.left,insets.top,
			   (int)(width * fraction),
			   this.getHeight()
			   - insets.top - insets.bottom);
		
		String str = (usedMemory / 1024) + "Mb/"
		    + (totalMemory / 1024) + "Mb";
		FontMetrics fm = g.getFontMetrics();
		
		Graphics g2 = g.create();
		g2.setClip(insets.left,insets.top,
			   (int)(width * fraction),
			   this.getHeight()
			   - insets.top - insets.bottom);

		g2.setColor(UIManager.getColor("ProgressBar.selectionForeground"));
		
		g2.drawString(str,
			      insets.left + (width - fm.stringWidth(str)) / 2,
			      insets.top + fm.getAscent());
		
		g2.dispose();
		
		g2 = g.create();
		
		g2.setClip(insets.left + (int)(width * fraction),
			   insets.top,this.getWidth()
			   - insets.left - (int)(width * fraction),
			   this.getHeight()
			   - insets.top - insets.bottom);
		
		g2.setColor(this.getForeground());
		
		g2.drawString(str,
			      insets.left + (width - fm.stringWidth(str)) / 2,
			      insets.top + fm.getAscent());
		
		g2.dispose();
	    }
    }

    // interface PropertyChangeListener interface

    public void propertyChange(PropertyChangeEvent evt)
    {
	if ( evt.getSource() instanceof FlowComposer.RunAction){
	    if (new String("isRunning").equals(evt.getPropertyName())) {

	    }
	}
	
	else if (evt.getSource() instanceof FlowComposer.CompileAction) {
	    System.out.println("compile " + evt.getPropertyName());
	}
    }
}
