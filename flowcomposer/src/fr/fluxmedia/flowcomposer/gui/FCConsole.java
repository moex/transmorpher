/**
 *
 *$Id: FCConsole.java,v 1.10 2002-09-13 17:53:56 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.gui;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.BadLocationException;

/**
 * The <code>FCConsole</code> class serves to displays in application the standard
 * exit System.err and System.out.
 *
 * @version 0.2
 * @author Chomat/Saint-Marcel
 */

public class FCConsole extends JPanel implements ActionListener
{ 

    public static final PrintStream systemOut = System.out;
    public static final PrintStream systemErr = System.err;
    
    protected static final int OUT = 0;
    protected static final int ERR = 1;

    //protected JTextPane message;
    protected JTextArea message;

    protected Thread readOut;
    protected Thread readErr;
    
    protected Font font = new Font("Arial",Font.PLAIN,9);
    protected  PipedInputStream pipeOut; 
    protected  PipedInputStream pipeErr; 
    private PipedOutputStream pOut;
    private PipedOutputStream pErr;
 
    //HTMLDocument htmlDocument ;
    //HTMLEditorKit htmlEditorKit ;
    
    JScrollPane scroll;
    
    protected String text = "";
    //protected String newLine = "<br>" ;
    //protected int insertPosition ;

    /**
     * Constructs a console and initialises it with a message
     *
     * @param str the message
     */ 
    public FCConsole(String str)
    {
	super();
	setLayout(new BorderLayout());	
	message = new JTextArea();
	
	// For the moment we use a simple JTextArea for whowing Transmorpher Message.
	// After, the use of JTextPane for use link or other HTML specificity must remplaced the JTextArea.

	//message.setContentType("text/html");
	//Get a referene to its HTMLEditorKit;
	//htmlEditorKit = new HTMLEditorKit();
	//message.setEditorKit(  htmlEditorKit );
	//Create e default HTML Document;
	//htmlDocument = (HTMLDocument) htmlEditorKit.createDefaultDocument();
	//message.setDocument(htmlDocument);
	message.setEditable(false);
	scroll = new JScrollPane(message,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	add(scroll,BorderLayout.CENTER);

	append(str);
    }
    
    public void init()
    {	
	try
	    {
		pipeOut = new PipedInputStream();
		pOut=new PipedOutputStream(this.pipeOut);
		System.setOut(new PrintStream(pOut,true)); 
		pipeErr = new PipedInputStream(); 
		pErr=new PipedOutputStream(this.pipeErr);
		System.setErr(new PrintStream(pErr,true));
	    } 
	catch (java.io.IOException e)
	    {
		append("<font color =\"red\">Couldn't redirect the standard output to this console\n"+e.getMessage()+"</font>");
	    }
	catch (SecurityException e)
	    {
		append("<font color =\"red\">Couldn't redirect the standard output to this console\n"+e.getMessage()+"</font>");
	    } 
	readOut = new Reader(pipeOut,OUT);
	readOut.setDaemon(true);
	readErr = new Reader(pipeErr,ERR);		
	readErr.setDaemon(true);
    }
    
    public void startReader(){
	
	if (readOut != null) {
	   readOut.start();	
	} 
	
	if (readErr != null) {
	    readErr.start();	
	} 
    }
    
    public void stop(){
		
	try{
	    if (readOut != null) {
		readOut.join(100);	
	    } 
	    
	    if (readErr != null) {
		readErr.join(100);	
	    } 
	    
	}catch(InterruptedException ie)
	    {
		append("<font color =\"red\">console interrupted badly");
		append("error "+ ie +"</font>");
	    }
	try{
	    
	    pOut.close();
	    pErr.close();
	    pipeOut.close();
	    pipeErr.close();
	}
	catch(IOException io)
	    {	
		append("<font color =\"red\">Couldn't close ouput stream\n");
		append("error "+ io+"</font>");
	    }
	System.setOut(systemOut);
	System.setErr(systemErr);
    }

    public synchronized void actionPerformed(ActionEvent evt)
    {
	clear();
    }

    public void clear(){
	text = "" ; 
	message.setText(text);
    }
    


    public synchronized void append(String newText){

		
	text = newText+"\n";
	
	Runnable updater = new Runnable(){
		
		public void run() 
		{
		    message.append(text);
		    scroll.getVerticalScrollBar().setValue(scroll.getVerticalScrollBar().getMaximum());
		}
	    };
	SwingUtilities.invokeLater(updater) ;
    }
    
    private class Reader extends Thread{
	
	protected PipedInputStream input;
	protected int type;

	public Reader(PipedInputStream input, int type)
	{
	    super() ;
	    this.input = input ;
	    this.type = type ;
	}
	
	public synchronized void run()
	{
	    try { this.wait(1000);}catch(InterruptedException ie) {}
	    try{
		while(input.available() != 0)
		    {
			String text = readLine();
			append(text);
		    }
	    }
	    catch(IOException io)
		{
		    append("Error reading standard flux\n");
		    append("Error "+io);
		}
	    try{
		input.close();
	    }
	    catch(IOException io)
	    {	
		append("<font color =\"red\">Couldn't close ouput stream\n");
		append("error "+ io+"</font>");
	    }
	    
	    if (type == OUT) {		
		try{
		    pOut.close();
		}
		catch(IOException io)
		    {	
			append("<font color =\"red\">Couldn't close ouput stream\n");
			append("error "+ io+"</font>");
		    }
		System.setOut(systemOut);
		// the thread need to be not referenced in order to be taken by the garbage collector
		readOut = null ;
	    }
	    else {
		try{
		    pErr.close();
		}
		catch(IOException io)
		    {	
			append("<font color =\"red\">Couldn't close ouput stream\n");
			append("error "+ io+"</font>");
		    }
		System.setErr(systemErr);
		// the thread need to be not referenced in order to be taken by the garbage collector
		readErr = null ;
	    }
	    
	}
	
	private synchronized String readLine() throws IOException
	{
	    String inputString="";
	    
	    do
		{
		    int available = input.available();
		    if (available == 0 ) 
			{
			    break;
			}
		    byte b[] = new byte[available] ;
		    input.read(b) ;
		    inputString = inputString + new String(b,0,b.length) ;
		    
		}
	    while( !inputString.endsWith("\n")  
		   && 
		   !inputString.endsWith("\r\n") 
		   );
	    
	    return inputString;
	}	
    }
}
