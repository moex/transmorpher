/**
 *
 *$Id: ParametersDialog.java,v 1.1 2002-12-16 08:47:23 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.gui;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import javax.swing.border.*;
import fr.fluxmedia.flowcomposer.gui.guitools.*;
import fr.fluxmedia.transmorpher.utils.LinearIndexedStruct;
import javax.swing.table.DefaultTableModel;
import fr.fluxmedia.flowcomposer.graph.*;

public class ParametersDialog extends JDialog implements ActionListener{
    
		protected Container contentPane;
		protected JButton ok;
		protected JButton cancel;
		protected JButton newParam;
		protected JButton deleteParam;
		protected DefaultTableModel tableModel;
		protected JTable parametersTable;
    protected FCProcessFactory processFactory;

		public ParametersDialog(FlowComposer flow){
			super(flow.getFrame(),"Parameters",true);
			this.setLocationRelativeTo(flow.getFrame());
			processFactory=flow.getProcessFactory();
			contentPane = getContentPane();
      contentPane.setLayout(new BorderLayout());
			ok = new JButton("OK");
			ok.addActionListener(this);

			cancel = new JButton("CANCEL");
			JPanel parametersPanel = new JPanel();
			parametersPanel.setPreferredSize(new Dimension(300,300));
			Border titleParam=BorderFactory.createEtchedBorder();

			parametersTable = new ParametersTable(processFactory.getTransmorpher().getMain().getParameters());
			tableModel = (DefaultTableModel)parametersTable.getModel();

			JScrollPane  scrollTable = new JScrollPane(parametersTable);
      scrollTable.setPreferredSize(new Dimension(250,200));
			
			newParam = new JButton("Add");
      newParam.addActionListener(this);
			
			deleteParam = new JButton("Delete");
      deleteParam.addActionListener(this);
			
			JPanel paramPanel = new JPanel();
      paramPanel.setLayout(new GridLayout(0,1,0,5));
      
      paramPanel.add(newParam);
      paramPanel.add(deleteParam);
			
			
			JPanel pane = new JPanel();
			pane.setLayout(new FlowLayout(FlowLayout.LEFT,20,5));
			pane.add(scrollTable);
			pane.add(paramPanel);
			pane.setBorder(BorderFactory.createTitledBorder(titleParam,"test"));


			JPanel buttonPanel = new JPanel();
      buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
			buttonPanel.add(ok);
      buttonPanel.add(cancel);
			contentPane.add(pane,BorderLayout.CENTER);
			contentPane.add(buttonPanel,BorderLayout.SOUTH);
			this.pack();
			this.setVisible(true);
		}
		
		public void actionPerformed(ActionEvent event){
			
			if(event.getSource() == newParam){
				String[] stringNull= {"",""};
				tableModel.addRow(stringNull);
			}
			
			if(event.getSource() == deleteParam){
				int[] index = parametersTable.getSelectedRows();
				int nb = 0;
				
				if(parametersTable.getCellEditor() != null)
					parametersTable.getCellEditor().stopCellEditing(); 
					
				for (int i = 0; i < index.length ; i++) {
					String name = (String)tableModel.getValueAt(index[i]-nb,0);
					tableModel.removeRow(index[i]-nb);
					if(processFactory.getTransmorpher().getMain().getParameters().getParameter(name)!=null)
						processFactory.getTransmorpher().getMain().getParameters().unsetParameter(name);
					nb++;
				}
			}
			
			if(event.getSource() == ok ) {

				for (int i=0;i<tableModel.getRowCount();i++) {
					String name = (String)tableModel.getValueAt(i,0);
					String value = (String)tableModel.getValueAt(i,1);
					

					if (name != null && ! (name.equals("")) )
						if(processFactory.getTransmorpher().getMain().getParameters().getParameter(name)==null)
						processFactory.getTransmorpher().getMain().getParameters().setParameter(name,value);
				}
				this.dispose();
			}
		}
	}
