/**
 *
 *$Id: FCRun.java,v 1.8 2002-09-13 17:53:56 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


/**
 * The main class for execution 
 *
 * @version 0.2
 * @author Chomat/Saint-Marcel
 */
package fr.fluxmedia.flowcomposer;

import javax.swing.JApplet;
import fr.fluxmedia.flowcomposer.gui.*;


public class FCRun extends JApplet{

    //Main method for local execution.
    public static void main(String[] args){
	
	//FCSplashScreen splashScreen = new FCSplashScreen("resources/logoF.gif",null,5000);
	FlowComposer.newEditor();
    }

    /** Applet launch for distant execution.*/
    public void start(){
	//not yet implemented
	
	//getRootPane().putClientProperty("defeatSystemEventQueueCheck",Boolean.TRUE);
    }


}
