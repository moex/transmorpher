/**
 *
 * $Id: ParserRDF.java,v 1.4 2002-09-13 17:53:56 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


package fr.fluxmedia.flowcomposer.parser;

// Imported SAX Classes
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.SAXException ;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.XMLFilterImpl;

//Imported JAVA Classes
import java.util.*;
import java.io.*;
import java.util.Stack ;
import java.lang.Integer;
import java.awt.Point;

import fr.fluxmedia.flowcomposer.graph.*;

/**
 * The RDF Parser class allows to preserve the graphical chart of the graph 
 * (coordonnees and color of the cells)
 *
 * @version 0.2
 * @author Chomat/Saint-Marcel
 */


public class ParserRDF extends DefaultHandler {

   
    protected static final String
            DEFAULT_PARSER_NAME = "org.apache.xerces.parsers.SAXParser";
    protected static final int DEFAULT=0;
    protected static final int PROCESS=1;
    protected static final int COMPONENT=2;
    protected static final int CHANNEL=3;
    protected int mode = DEFAULT;
    protected static final int COLOR=0;
    protected static final int X=1;
    protected static final int Y=2;
    protected static final int NO = -1;
    protected int parameter = NO;
    protected int x = -1;
    protected int y = -1;
    protected FCProcessFactory processFactory;
    protected ProcessGraph currentProcess;
    protected FCGraphCell currentCall;
    protected Object currentChannel ;
    protected int debug_mode;

    /**
     * Get the value of debug_mode.
     * @return value of debug_mode.
     */
    public int getDebug_mode() {return debug_mode;}

    /**
     * Set the value of debug_mode.
     * @param v  Value to assign to debug_mode.
     */
    public void setDebug_mode(int  v) {this.debug_mode = v;}

    /** XML Parser */
    javax.xml.parsers.SAXParser iParser = null;


    /*------------------------------------------ CONSTRUCTOR -----------------------------*/

    /** The constructor, build the RDF Parser but not parse the document 
     * @param factory manager of <code>ProcessGraph</code>
     * @param debug_mode level of debug
     */
    public ParserRDF(FCProcessFactory factory,int debug_mode) {

        processFactory = factory;
        this.debug_mode=debug_mode;
        try {
            javax.xml.parsers.SAXParserFactory iParserFactory = javax.xml.parsers.SAXParserFactory.newInstance();
            iParserFactory.setValidating(false);          // no DTD for RDF and annotations so far
            iParserFactory.setNamespaceAware(true);
            iParser = iParserFactory.newSAXParser();

        }  catch (javax.xml.parsers.ParserConfigurationException CNFE) {
            System.out.println("FMParser :Error [" + CNFE +"]");
        }  catch (org.xml.sax.SAXException SaxE) {
            System.out.println("FMParser :Error [" + SaxE +"]");
        }
    }

    /*------------------------------------------- END CONSTRUCTOR ------------------------*/

    /** Parse the document given in parameter */
    public void newParse(String uri) {
        try {
            iParser.parse(uri,this);
        } catch (SAXException SE) {
            System.err.println("[FMParser]Erreur SAX(" + SE+")");
            System.err.println("[FMParser]Erreur SAX(" + SE.getMessage()+")");
            System.err.println("[FMParser]Erreur SAX(" + SE.getException()+")");
            SE.printStackTrace();
        } catch (java.io.IOException IOE) {
            System.err.println("[FMParser]Erreur IO(" + IOE+")");
        }

    }

    /** Call by the XML parser at the begining of an element */
    public final void startElement(String namespaceURI, String localName, String qname, Attributes atts) {
        if(debug_mode > 0)
            System.out.println("startElement "+localName);
        if (namespaceURI.equals("http://www.w3.org/1999/02/22-rdf-syntax-ns")){
            if (localName.equals("Description")) {
                String name = atts.getValue("about");
                switch(mode){
                    case COMPONENT :
                        if(currentProcess != null)
                            currentCall = currentProcess.getCell(name);
                        break;
                    case PROCESS :
                        if(processFactory != null)
                            currentProcess = processFactory.getProcess(name);
                        break;
                    case CHANNEL :
                        if(currentProcess != null)
                            currentChannel = currentProcess.getEdge(name);
                        break;
                }
            }
        }
        else if(namespaceURI.equals("http://www.w3.org/2000/01/rdf-schema")){
            if (localName.equals("color")){
                parameter = COLOR;}
            else if (localName.equals("x")){
                parameter= X;}
            else if (localName.equals("y")){
                parameter = Y;}
        }
        else if(localName.equals("hasProcess")){
            mode = PROCESS;}
        else if(localName.equals("hasComponent")){
            mode = COMPONENT;}
        else if(localName.equals("hasChannel")){
            mode = CHANNEL;}
    }

    /** Call by the XML parser at the end of an element */
    public final void endElement(String namespaceURI,String pName, String qName ) {
        if(debug_mode > 0)
            System.out.println("endElement "+pName);
        if(pName.equals("hasProcess")){
            mode = DEFAULT;
        }
        else if(pName.equals("hasComponent")){
            if(currentProcess!=null && x!=-1 && y!=-1){
                currentProcess.updatePosition(currentCall,new Point(x,y));
            }
            mode = DEFAULT;
        }
        else if(pName.equals("hasChannel")){
            mode = DEFAULT;
        }
        else {
            parameter = NO;
        }
    }

    /** The content of the tags */
   
    public void characters(char ch[], int start, int length) {

        String content = "";
        int end = start + length;
        for( int i = start; i < end ; i++ ) {content = content + ch[i];}
        switch(parameter){
            case X:
                x = Integer.parseInt(content);
                break;
            case Y :
                y = Integer.parseInt(content);
                break;
            case COLOR:
                break;
        }
    }

    public void warning(SAXParseException exception)
            throws SAXException
    {
        System.err.println("warning");
    }


    public void error(SAXParseException ex)
    {
        System.err.print("Error!! ");
        System.err.println(ex);
        System.err.println(ex.getLineNumber());
    }


    public void fatalError(SAXParseException ex) throws SAXException
    {
        System.err.print("Fatal Error!! ");
        System.err.println(ex);
    }
}







