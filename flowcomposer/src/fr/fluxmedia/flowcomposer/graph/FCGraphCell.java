/**
 * $Id: FCGraphCell.java,v 1.5 2002-12-12 10:25:58 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.graph;


import java.util.Observer;
import java.util.Observable;
import java.util.Hashtable;
import java.util.Map;

import java.awt.Point;

import com.jgraph.graph.DefaultGraphCell;
import com.jgraph.graph.ParentMap;
import com.jgraph.graph.GraphConstants;
import com.jgraph.graph.*;

import fr.fluxmedia.transmorpher.graph.Call;
import fr.fluxmedia.transmorpher.graph.CallImpl;
import fr.fluxmedia.transmorpher.graph.Port;
import fr.fluxmedia.transmorpher.action.TmAction;

import fr.fluxmedia.flowcomposer.utils.FCConstants;

/**
 * The basic definition of a cell is represented by the class <code>FCGraphCell</code>
 * which is an extension of the DefaultGraphCell  and offers no
 * additional methods except Observer implementation and creation of ports.
 * 
 *
 * @version 0.2
 * @author Chomat/Saint-Marcel
 */

public class FCGraphCell extends DefaultGraphCell implements Observer{

    /** the owner graph of the cell*/
    protected ProcessGraph process;

    /** Constructs a <code>FCGraphCell</code>
     *
     * @param userObject the cell's data --> the reference to Transmorpher object Call
     * @param process its owner graph
     */
    public FCGraphCell(Object userObject, ProcessGraph process){
        super(userObject);
        this.process = process;
        if(userObject instanceof CallImpl){
            CallImpl call = (CallImpl)userObject;
            call.addObserver(this);
            //createPorts();
        }
    }

    /** Returns the id's Transmorpher of Call.*/
    public String getId(){
        if(userObject != null && userObject instanceof Call)
            return ((Call)userObject).getId();
        return null;
    }
    
    /** Returns the type's Transmorpher of call.*/
    public String getType(){
        if(userObject != null && userObject instanceof Call)
            return ((Call)userObject).getType();
        return null;
    }

    /** Returns the owner graph of Call*/
    public ProcessGraph getProcess(){
        return process;
    }

    /** Create a port with its type in Transmorpher graph */
    public void newPort(int type){
        if(userObject != null && userObject instanceof CallImpl) {
            CallImpl call = (CallImpl)userObject;
            Port port;
            int number;
            if(type == FCConstants.PORTOUTPUT){
                number = call.outPorts().length();
                port= new Port("null",call,number,FCConstants.PORTOUTPUT);
                call.addOut(port);
		    }
            else{
                number = call.inPorts().length();
                port= new Port("null",call,number,FCConstants.PORTINPUT);
                call.addIn(port);
	    }
	}
    }
    
    /**
     * Implementation of interface Observer for the operations on ports like
     * adding, removing and modify. 
     */
    public void update(Observable obs, Object arg){
        if(obs instanceof Call){
            if(arg instanceof TmAction){
                TmAction action = (TmAction)arg;
                switch(action.getType()){
                    case TmAction.ADD :
                        if(action.getTarget() instanceof Port){
                            if(process != null){
                                createPort(((TmAction)arg).getTarget());
                            }
                        Object source = action.getSource();
                        }
                        break;
				case TmAction.REMOVE :
                    break;
			    case TmAction.MODIFY :
                    break;
                }
		    }
		
	    }
    }
    
    /** Create all the ports of a Call.*/
    public void createPorts(){
        CallImpl call = (CallImpl)userObject;
        for(int i =0; i < call.inPorts().length();i++){
            createPort(call.inPorts().getPort(i));
	    }
        for(int j =0; j < call.outPorts().length();j++){
            createPort(call.outPorts().getPort(j));
	    }
	}
    
    /** Create a port for the cell in its owner graph <code>ProcessGraph</code>.*/
    private void createPort(Object port){
        PortCell pc = new PortCell((Port)port,((Port)port).getType());
        Map map = GraphConstants.createMap();
        int u = GraphConstants.PERCENT;
        GraphConstants.setOffset(map, new Point((int)(u/2),(int)(u/2)));
        ParentMap parent = new ParentMap();
        parent.addEntry(pc,this);
        Map viewMap = new Hashtable();
        viewMap.put(pc,map);
        process.getModel().insert(new Object[]{pc},null,parent,viewMap);
        process.updatePort(this,((Port)port).getType());

    }



}
