/**
 * $Id: GenerateCell.java,v 1.3 2002-09-13 17:53:56 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.graph;

import com.jgraph.graph.DefaultGraphCell;
import java.util.Observer;
import java.util.Observable;

   /**
     * The component Generate is represented by the <code>GenerateCell</code>
     * which is an extension of the <code>FCGraphCell</code> class, and offers no
     * additional methods except Observer implementation.It is only used to distinguish
     * Dispatch vertices from other vertices.
     *
     * @version 0.2
     * @author Chomat/Saint-Marcel
     */

public class GenerateCell extends FCGraphCell{

    /** Creates an empty <code>GenerateCell</code>.*/
    public GenerateCell() {
      this(null,null);
    }

    /**
     * Creates a <code>GenerateCell</code> and initializes it with the specified user object
     * and its graph 
     *
     * @param userObject the cell's data --> the reference to Transmorpher object Call
     * @param process it's owner graph
     *
     */
    public GenerateCell(Object userObject,ProcessGraph process) {
	super(userObject,process);

    }
    
    /** This method is called whenever the object is changed*/
    public void update(Observable o, Object arg){
	super.update(o,arg);
    }
}
