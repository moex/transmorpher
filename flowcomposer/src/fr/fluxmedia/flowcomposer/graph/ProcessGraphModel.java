/**
 * $Id: ProcessGraphModel.java,v 1.3 2002-09-13 17:53:56 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.graph;

import com.jgraph.graph.*;
import java.io.*;
import java.util.*;

import fr.fluxmedia.flowcomposer.utils.*;

/**
 * The graph model is represented by the class <code>ProcessGraphModel</code>
 * which is an extension of the DefaultGraphModel, and offers no
 * additional methods.It overrides only two methods :
 * @see ProcessGraphModel#acceptsSource.
 * @see ProcessGraphModel#acceptsTarget.
 *
 * @version 0.2
 * @author Chomat/Saint-Marcel
 */


public class ProcessGraphModel extends DefaultGraphModel{
    
    /** Constructs an empty graph model. */
    public ProcessGraphModel(){
        super();
    }
    
    /**
     * Constructs a model who's isAttributeStore returns the specified
     * value.
     *
     * @param attributeStore false if DefaultGraphModel is not an attribute store.
     */
    public ProcessGraphModel(boolean attributeStore){
        super(attributeStore);
    }
    
    /**
     *  method is called from the graph's UI before an edge is connected to,
     * or disconnected from a port source, with the edge and port as arguments.
     * By overriding these methods of DefaultGraphModel, the model can return true
     * or false for a specific edge, port pair to indicate whether the specified
     * connection is valid.
     * checking on the types of the ports (input/output)||(output/input) --> (FCGraphCell vs FCGraphCell)||(PortProcessCell vs PortProcessCell)
     * (input/input)|| (output/output) --> (FCGraphCell vs PortProcessCell) || (PortProcessCell vs FCGraphCell)
     */
    public boolean acceptsSource(Object edge,Object port) {
        // target port of edge
        Object portTarget = ((Edge)edge).getTarget();
        // target cell of edge
        Object cellTarget = getParent(portTarget);
        // source cell of edge
        Object cell = getParent(port);
        java.util.Iterator it = edges(port);
        // checking on the edges of the port
        if (Utils.lenght(it)>0) {
            //
            it = edges(port);
            while (it.hasNext()) {
                Object itEdge = it.next();
                if (!itEdge.equals(edge)) return false;
            }
            return true;
        }
        
        else {
            if (!((DefaultGraphCell)cellTarget instanceof PortProcessCell)){
                if (!((DefaultGraphCell)cell instanceof PortProcessCell))
                    return ((!cellTarget.equals(cell))&&(((PortCell)portTarget).getType()!=((PortCell)port).getType()));
                else  return ((!cellTarget.equals(cell))&&(((PortCell)portTarget).getType()==((PortCell)port).getType()));
            }
            else if (!((DefaultGraphCell)cell instanceof PortProcessCell))
                return ((!cellTarget.equals(cell))&&(((PortCell)portTarget).getType()==((PortCell)port).getType()));
            else   return ((!cellTarget.equals(cell))&&(((PortCell)portTarget).getType()!=((PortCell)port).getType()));
        }
        
    }
    
    /**
     *  method is called from the graph's UI before an edge is connected to,
     * or disconnected from a port target, with the edge and port as arguments.
     * By overriding these methods of DefaultGraphModel, the model can return true
     * or false for a specific edge, port pair to indicate whether the specified
     * connection is valid.
     *  checking on the types of the ports (input/output)||(output/input) --> (FCGraphCell vs FCGraphCell)||(PortProcessCell vs PortProcessCell)
     * (input/input)|| (output/output) --> (FCGraphCell vs PortProcessCell) || (PortProcessCell vs FCGraphCell)
     */
    public boolean acceptsTarget(Object edge,Object port) {
        
        // source port of edge
        Object portSource = ((Edge)edge).getSource();
        // source cell of edge
        Object cellSource = getParent(portSource);
        // target cell of edge
        Object cell = getParent(port);
        Iterator it = edges(port);
        // checking on the edges of the port
        if (Utils.lenght(it)>0) {
            it = edges(port);
            while (it.hasNext()) {
                Object itEdge = it.next();
                if (!itEdge.equals(edge)) return false;
            }
            return true;
        }
        
        
        else {
            if (!((DefaultGraphCell)cellSource instanceof PortProcessCell)){
                if (!((DefaultGraphCell)cell instanceof PortProcessCell))
                    return ((!cellSource.equals(cell))&&(((PortCell)portSource).getType()!=((PortCell)port).getType()));
                else  return ((!cellSource.equals(cell))&&(((PortCell)portSource).getType()==((PortCell)port).getType()));
            }
            else if (!((DefaultGraphCell)cell instanceof PortProcessCell))
                return ((!cellSource.equals(cell))&&(((PortCell)portSource).getType()==((PortCell)port).getType()));
            else   return ((!cellSource.equals(cell))&&(((PortCell)portSource).getType()!=((PortCell)port).getType()));
        }
    }
    
}
