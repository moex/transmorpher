/**
 * $Id: PortCell.java,v 1.3 2002-09-13 17:53:56 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.graph;

import com.jgraph.graph.*;

/**
 * A port that may have an attribute type which can take two values "input" or "output"
 *
 * @version 0.2
 * @author Chomat/Saint-Marcel
 */

public class PortCell extends DefaultPort{

    /** declaration of the type*/
    protected int type;

     /** Creates an empty <code>PortCell</code>.*/
    public PortCell(){
	super(null);
    }
    
    /**
     * Creates a <code>PortCell</code> and initializes it with the specified user object 
     *
     * @param userObject the cell's data --> the reference to Transmorpher object Port
     *
     */
    public PortCell(Object userObject){
	super(userObject);
    }
    
    /**
     * Creates a <code>PortCell</code> and initializes it with the specified user object
     * and its type 
     *
     * @param userObject the cell's data --> the reference to Transmorpher object Port
     * @param type input or output
     */
    public PortCell(Object userObject, int type){
	super(userObject);
	this.type = type;
    }
    
    /**
     * Creates a <code>PortCell</code> and initializes it with the specified user object
     * and its anchor 
     *
     * @param userObject the cell's data --> the reference to Transmorpher object Port
     * @param anchor a reference to a Port
     */
    public PortCell(Object userObject, Port anchor) {
	super(userObject,anchor);
    }
    
    /**
     * Creates a <code>PortCell</code> and initializes it with the specified user object
     * its type, its anchor and its type 
     *
     * @param userObject the cell's data --> the reference to Transmorpher object Port
     * @param type input or output
     * @param anchor a reference to a Port
     * @param type input or output
     */
    public PortCell(Object userObject, Port anchor, int type) {	
	super(userObject,anchor);
    	this.type = type;
    }
    
    /** Returns the type of the Port*/
    public int getType(){
	return type;
    }
    
    /** Set the type of Port*/ 
    public void setType(int type){
    	this.type = type;
    }


}
