/**
 * $Id: ProcessGraph.java,v 1.13 2002-12-12 10:25:58 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.graph;


import com.jgraph.JGraph;
import com.jgraph.graph.*;

import com.jgraph.plaf.GraphUI;
import java.awt.*;
import java.awt.print.Paper;
import java.awt.event.*;
import java.util.*;
import java.util.Iterator;
import java.util.List;
import java.io.*;
import javax.swing.*;
import javax.swing.undo.UndoManager;

import fr.fluxmedia.transmorpher.graph.*;
import fr.fluxmedia.transmorpher.graph.Process;
import fr.fluxmedia.transmorpher.graph.Call;
import fr.fluxmedia.transmorpher.graph.Merge;
import fr.fluxmedia.transmorpher.graph.Dispatch;
import fr.fluxmedia.transmorpher.graph.Serialize;
import fr.fluxmedia.transmorpher.graph.Generate;
import fr.fluxmedia.transmorpher.graph.ApplyProcess;
import fr.fluxmedia.transmorpher.graph.ApplyRuleset;
import fr.fluxmedia.transmorpher.graph.ApplyExternal;
import fr.fluxmedia.transmorpher.graph.ApplyQuery;
import fr.fluxmedia.transmorpher.graph.CallImpl;
import fr.fluxmedia.transmorpher.graph.Port;
import fr.fluxmedia.transmorpher.graph.TransformationImpl;
import fr.fluxmedia.transmorpher.graph.Channel;
import fr.fluxmedia.transmorpher.action.TmActionImpl;

import fr.fluxmedia.flowcomposer.utils.*;
import fr.fluxmedia.flowcomposer.gui.FlowComposer;
import fr.fluxmedia.flowcomposer.gui.FCMarqueeHandler;

 /** 
   * A <code>ProcessGraph</code> object doesn't actually contain your data; it simply provides
   * a view of the data.Like any non-trivial Swing component, the graph gets
   * data by querying its data model.
   * <p>
   * <code>ProcessGraph</code> displays its data by drawing individual elements.Each element
   * displayed by the graph contains exactly one item of data, which is
   * called a cell.A cell may either be a vertex or an edge.Vertices may
   * have neighbours or not like ports, and edges may have source and target vertices
   * or not, depending on whether they are connected.
   *
   * @version 0.2
   * @author Chomat/Saint-Marcel
   */



public class ProcessGraph extends JGraph implements Comparator, Observer{

    /** userObject of the process : an object Transformation of Transmorpher. */
    protected Object userObject;

    protected ApplyProcessCell[] caller;

    // Undo Manager
    protected GraphUndoManager undo;

    /** boundsCube define the dimension of a squared form composant such as ApplyQuery, Merge and Dispatch.
     * ie: this composant are included in square boxe
     */
    static Dimension boundsCube = new Dimension(60,60);

    /** boundsPort define the dimension of a port of a FCGraphCell composant
     */
    static Dimension boundsPort = new Dimension(10,10);

    /** boundsPortCell define the dimension of a port of a PortProcessCell composant
     * ie: this composant are included in square boxe
     */
    static Dimension boundsPortCell = new Dimension(30,30);

    /** boundsRect define the dimension of a  rectangle form composant such as ApplyProcess, Serialize,...
     * ie: this composant are included in rectangle boxe
     */
    static Dimension boundsRect = new Dimension(70,60);

    /**Default width of a process*/
    public static final int PREFERRED_WIDTH =640;

    /**Default height of a process*/
    public static final int PREFERRED_HEIGHT =480;

    /** all the zoom's sizes for the app*/
    protected static double[] zoomSizes = new double[]{0.5,0.75,1.0,1.5,2.0,5.0};

    /**Default width of workSurface*/
    protected int workSurfaceX = PREFERRED_WIDTH;

    /**Default height of workSurface*/
    protected int workSurfaceY = PREFERRED_HEIGHT;

    /**The defaultZoom for a process -->100% */
    protected int zoomCurrent = 2;

    /**instance of automatic layout algorithm*/
    protected Layout touchLayout;
   
    
    /**
     * Constructs an empty <code>ProcessGraph</code>
     */ 
     public ProcessGraph() {
        this(null);
    }
 
     /**
     * Constructs a <code>ProcessGraph</code>  and initializes it with the specified data model
     *
     * @param model its default graph model
     */
     public ProcessGraph(GraphModel model) {
        this(model, null);
    }

    /**
     * Constructs a <code>ProcessGraph</code>  and initializes it with the specified data model 
     * using the specified view
     *
     * @param model its default graph model
     * @param view its view
     */
    public ProcessGraph(GraphModel model, GraphView view) {
        this(model, view, null);
    }

    /**
     * Constructs a <code>ProcessGraph</code>  and initializes it with the specified data model 
     * using the specified view and its userObject. By default the portVisible property is set to 
     * true and sizeable is set to false.
     *
     * @param model its default graph model
     * @param view its view
     * @param userObject the Transmorpher's transformation
     */
    public ProcessGraph(GraphModel model, GraphView view, Object userObject) {
        super(model, view, null);
        undo = new GraphUndoManager();
        touchLayout = new Layout(this);
        setPortsVisible(true);
        setSizeable(false);
        setAntiAliased(true);
        setUI(new FCGraphUI());
        this.userObject = userObject;

    }

    /** Returns an instance of the automatic layout for this graph.*/
    public Layout getTouchLayout() {
        return touchLayout;
    }


    /**
     * set the value of the current zoom
     */
    public void setZoomCurrent(int zoomCurrent) {
        this.zoomCurrent = zoomCurrent;
    }

    /**
     * return the array of zoom's sizes
     */
    public double[] getZoom() {
        return zoomSizes;
    }

    /**
     * return the zoom current
     */
    public int getZoomCurrent() {
        return zoomCurrent;
    }

    /**
     *  set the undo manager for the process
     */
    public void setUndoManager(GraphUndoManager undo){
        this.undo = undo;
    }

    /**
     * return the undo manager
     */
    public GraphUndoManager getUndoManager(){
        return undo;
    }

    /**
     * set the userObject
     */
    public void setUserObject(Object userObject){
        this.userObject=userObject;
    }

    /**
     * return the userObject
     */
    public Object getUserObject(){
        return userObject;
    }

    /**
     * overrides the method
     */
    public String toString(){
       return getName();
    }

    /**Returns the name of Transmorpher's Transformation*/
    public String getName(){
        if(userObject  instanceof Process){
            return ((Process)userObject).getName();
        }
        return "";
    }

    /**
     * return true if its a main process
     */
    public boolean isMain(){
        return userObject instanceof MainProcess;
    }

    /** 
     * When you create a channel, you must attribute to this object a id. This id must be single (like all respectable id :-))
     * getDefaultChannelName return a default single id for a channel.
     *
     * @return a single id for a channel
     */
    private String getDefaultChannelName(String name, int count){
       
	Enumeration e = ((Process)userObject).getChannels();
        String defaultName = name+"-"+count;
	
        while(e.hasMoreElements()){
	    
	    String tmpName = ((Channel)e.nextElement()).getName();
	     
            if(tmpName.equals(defaultName))
            {
                count++;
                return getDefaultChannelName(name,count);
            }
        }
   
	return defaultName;
    }

    /** 
     * When you create a call, you must attribute to this object a id. This id must be single (like all respectable id :-))
     * getDefaultCallName return a default single id for a call.
     *
     * @return a single id for a call
     */
    public String getDefaultCallName(String name, int count){
	
        Enumeration e = ((Process)userObject).getCalls().getKeys();
        String defaultName = name+"-"+count;
	
        while(e.hasMoreElements()){

            String tmpName = (String)e.nextElement();
            if(tmpName.equals(defaultName))
            {
                count++;
                return getDefaultCallName(name,count);
            }
        }
        return defaultName;
    }

     /** 
      *  Returns a serializable data (Vector) with all the cell's graph and their attributes. 
      *  It contains a current state of graph.
      */
    public Serializable getArchiveableState() {
      Object[] cells = getView().order(getAll());
      Map viewAttributes = GraphConstants.createPropertyMap(cells, getView());
      Vector v = new Vector();
      v.add(getName());
      v.add(cells);
      v.add(viewAttributes) ;
      //v.add(this);
      return v;
    }
    
    /** Change the id of the <code>cell</code> userObject with the <code>newValue</code>.
     *  if the newValue is incorrect, changeId return false and the id is not change.
     *
     * @return false, if the  newValue is null or newValue is a empty String or if newValue is not a single id. Else, true
     *   
     */
    public boolean changeId(Object call,String newValue){
	
	boolean idChanged = false ;
	String empty = new String("") ;
	if ( call instanceof Call )
	    {
		if ( !(empty.equals(newValue)) && isSingleId((Call)call, newValue) ) {
		    idChanged = true ;
		    ((Call)call).setId(newValue) ;
		}
	    }
	
	return idChanged ;
    }
    
    /** 
      *  Create a serializable data (Vector) with all the cell's graph and their attributes. 
      *  It contains a current state of graph.
      */
    public void setArchivedState(Object s) {
      if (s instanceof Vector) {
		Vector v = (Vector) s;
                Object[] cells = (Object[]) v.get(0);
                Object st = (Object)v.get(1);
                //for (int i=0;i<cells.length;i++) 
                System.out.println(st);
                //ConnectionSet cs = (ConnectionSet) v.get(1);
                //Map attrib = (Map) v.get(2);
               // getModel().insert(cells,cs,null,attrib);
      }
    }
    
    
    
    //
    // Cell View Factory
    //

    /**
     * Constructs a view for the specified cell and associates it
     * with the specified object using the specified CellMapper.
     * This calls refresh on the created CellView to create all
     * dependent views.<p>
     * Note: The mapping needs to be available before the views
     * of child cells and ports are created.
     *
     * @param cell reference to the object in the model
     * @param map the mapping from cell to view
     */
    public CellView createView(Object cell, CellMapper map) {

        CellView view = null;
        if (cell instanceof PortCell)
            view = createPortView((PortCell) cell, map);
        else if (cell instanceof Edge)
            view = createEdgeView((Edge) cell, map);
        else
            view = createVertexView(cell, map);

        map.putMapping(cell, view);
        view.refresh(true); // Create Dependent Views
        view.update();
        return view;

    }

    /** 
     * Constructs a view for the specified port and associates it
     * with the specified object using the specified CellMapper.
     */
    protected PortView createPortView(PortCell p, CellMapper cm){
        return new FCPortView(p, this, cm);
    }

    /** 
     * Constructs a view for the specified edge and associates it
     * with the specified object using the specified CellMapper.
     */
    protected EdgeView createEdgeView(Edge v, CellMapper cm)
    {
        return new NormalEdgeView(v,this,cm);
    }

    /**
     * Creates and returns the corresponding <code>GraphView</code> of the v object.
     *
     * @return the default <code>GraphView</code>
     */
    protected VertexView createVertexView(Object v, CellMapper cm)
    {
        //A compl�ter pour tous les objets transmorpher
        if (v instanceof GenerateCell)
            return new GenerateView(v, this, cm);
        else if (v instanceof MergeCell)
            return new MergeView(v, this, cm);
        else if (v instanceof DispatchCell)
            return new DispatchView(v, this, cm);
        else if (v instanceof SerializeCell)
            return new SerializeView(v, this, cm);
        else if (v instanceof ApplyExternalCell)
            return new ApplyExternalView(v, this, cm);
        else if (v instanceof ApplyProcessCell)
            return new ApplyProcessView(v, this, cm);
        else if (v instanceof ApplyRulesetCell)
            return new ApplyRulesetView(v, this, cm);
        else if (v instanceof ApplyQueryCell)
            return new ApplyQueryView(v, this, cm);
        else if (v instanceof PortProcessCell)
            return new PortProcessView(v,this,cm);
        else
            return super.createVertexView(v, cm);
    }

    /** Implementation of interface comparator for the coordonnee y of the cell's*/
    public int compare(Object cell,Object cell2) {
        Rectangle r1 = getCellBounds(cell);
        Rectangle r2 = getCellBounds(cell2);
        return (new Integer(r1.y)).compareTo(new Integer(r2.y));
    }


    /**
     * Implementation of interface Observer for the operations on cells, channels and port
     * of process : <code>PortProcessCell</code> , like adding, removing and modify. 
     */
    public void update(Observable observable, Object arg){

        if(observable instanceof Process)
        {
            if(arg instanceof TmActionImpl)
            {
                TmActionImpl action = (TmActionImpl)arg;
                Object target = action.getTarget();

                switch(action.getType())
                {
                    case TmActionImpl.ADD :

                        if(target instanceof Call){

                            if(target instanceof Dispatch)
                                addVertex(target,FCConstants.DISPATCH);
                            else if(target instanceof Merge)
                                addVertex(target,FCConstants.MERGE);
                            else if(target instanceof ApplyProcess)
                                addVertex(target,FCConstants.APPLYPROCESS);
                            else if(target instanceof ApplyExternal)
                                addVertex(target,FCConstants.APPLYEXTERNAL);
                            else if(target instanceof Generate)
                                addVertex(target,FCConstants.GENERATE);
                            else if(target instanceof Serialize)
                                addVertex(target,FCConstants.SERIALIZE);
                            else if(target instanceof ApplyQuery)
                                addVertex(target,FCConstants.APPLYQUERY);
                            else if(target instanceof ApplyRuleset)
                                addVertex(target,FCConstants.APPLYRULESET);

                        }
                        else if(target instanceof Port)
                        {
                            Port port = (Port)target;
                            addVertex(port,FCConstants.PORTPROCESS);
                            updateCellProcess(port.getType());

                        }
                        else if(target instanceof Channel)
                        {
                            Port in = ((Channel)target).in();
                            Port out = ((Channel)target).out();

                            if(in != null && out!=null)
				{
				    DefaultEdge cell = new NormalEdge((Channel)target);
				    
				    Object[] insert = new Object[]{cell};
				    ConnectionSet cs = new ConnectionSet();

				    PortView sourcePort =null;
				    PortView targetPort =null;

				    sourcePort = getPort(in);
				    targetPort = getPort(out);
				    if(targetPort != null && sourcePort !=null)
					{
					    cs.connect(cell, sourcePort.getCell(), true);
					    cs.connect(cell, targetPort.getCell(), false);
					    getModel().insert(insert, cs, null, null);
					}
				}
                        }
                        else
			    {
				System.out.println("update " + target+" not yet implemented");
			    }
                        break;

                    case TmActionImpl.REMOVE :

                        if(target instanceof Call)
                        {
                            DefaultGraphCell cell = (DefaultGraphCell)getCall(target);

                            ArrayList deleteList = new ArrayList();
                            List portList = cell.getChildren();
                            if(portList != null){

                                Iterator it = portList.iterator();

                                while(it.hasNext()){
                                    Object port = it.next();
                                    deleteList.add(port);
                                }
                            }

                            deleteList.add(cell);
                            getModel().remove(deleteList.toArray());
                        }
                        else if(target instanceof Channel)
                        {
                            getModel().remove(new Object[]{getChannel(target)});
                        }


                        break;
                    case TmActionImpl.MODIFY:
                        break;
                }
            }
        }
    }

    private boolean inferieur(Object o1, Object o2) {
        Rectangle r1 = getCellBounds(o1);
        Rectangle r2 = getCellBounds(o2);
        return r1.y < r2.y;
    }

    private void exchange(Object[] cell,int i, int j) {
        Object o = cell[i];
        cell[i]=cell[j];
        cell[j]=o;
    }

     /** implementation insertion sort */
    public Object[] sorting(Object[] cell) {
        for (int i = 0; i < cell.length - 1; i++) {
            int max = i;
            for (int j = i + 1; j < cell.length; j++) {
                if (inferieur(cell[max],cell[j])) {
                    max = j;
                }
            }
            exchange(cell, i, max);
        }
        return cell;
    }

    //
    // Defines Semantics of the Graph
    //

    /**
     * Returns true if <code>object</code> is a vertex, that is, if it
     * is not an instance of Port or Edge, and all of its children are
     * ports, or it has no children.
     */
    public boolean isGroup(Object object) {
        if (!(object instanceof Port) && !(object instanceof Edge)) {
            for (int i = 0; i < graphModel.getChildCount(object); i++) {
                Object child = graphModel.getChild(object, i);
                if (!(child instanceof Port))
                    return true;
            }
        }
        return false;
    }

    /**
     * Returns true if <code>object</code> is a vertex, that is, if it
     * is not an instance of Port or Edge, and all of its children are
     * ports, or it has no children.
     */
    public boolean isVertex(Object object) {
        if (!(object instanceof Port) && !(object instanceof Edge)) {
            for (int i = 0; i < graphModel.getChildCount(object); i++) {
                Object child = graphModel.getChild(object, i);
                if (!(child instanceof Port))
                    return false;
            }
            return true;
        }
        return false;
    }


    /**
     * Returns true if <code>object</code> is a edge.
     *
     * @param object An object.
     */
    public boolean isEdge(Object object) {
        return (object instanceof Edge);
    }

    /**
     * Returns true if the given vertices are conntected by a single edge
     *
     * @param v1  An object.
     * @param v2  An object.
     */
    public boolean isNeighbour(Object v1, Object v2) {
        Object[] edges = getEdgesBetween(v1, v2);
        return (edges != null && edges.length > 0);
    }


    /**
     * Returns the array of all selectionned vertices.
     *
     */
    public Object[] getSelectionVertices() {
        Object[] tmp = getSelectionCells();
        Object[] all = DefaultGraphModel.getDescendants(getModel(), tmp).toArray();
        return getVertices(all);
    }

    /**
     * Returns the array of all vertices include in the cells array giving in parameter.
     * @param cells  A object's array. if cells is null getVertices return null.
     */
    public Object[] getVertices(Object[] cells) {
        if (cells != null) {
            ArrayList result = new ArrayList();
            for (int i = 0; i < cells.length; i++)
                if ((!(cells[i] instanceof PortProcessCell))&&(!(cells[i] instanceof PortCell))&&(!(cells[i] instanceof DefaultEdge)))
                // if (isVertex(cells[i]))
                    result.add(cells[i]);
            return result.toArray();
        }
        return null;
    }

    /**
     *  Returns the array of all selectionned edges.
     *
     */
    public Object[] getSelectionEdges() {
        return getEdges(getSelectionCells());
    }

    /**
     * Returns all the roots of the model
     *
     */
    public Object[] getAll() {
        return getRoots();
    }

    /** Returns the <code>FCPortView</code> for a port with the specified userObject :
     * the port Transmorpher
     */
    public PortView getPort(Object userObject){

        Object[] roots = getAll();
        for (int i = 0 ; i <roots.length ;i++)
        {
            //System.out.println("root "+roots[i]);
            java.util.List port = ((DefaultGraphCell)roots[i]).getChildren();
            if(port!=null)
            {
                Iterator it = port.iterator();
                while (it.hasNext())
                {
                    DefaultGraphCell portCell = (DefaultGraphCell)it.next();
                    //System.out.println("portCell "+portCell);
                    if(portCell.getUserObject().equals(userObject ))
                        return (PortView)getView().getMapping(portCell,false);
                }

            }
        }
        return null;
    }


    /** Returns the width of the workSurface*/
    public int getWorkX() {
        return workSurfaceX;
    }

    /** Returns the height of the workSurface*/
    public int getWorkY() {
        return workSurfaceY;
    }

    /** Update the size of workSurface : if sizeable (width+dx,height+dy) else (width-dx,height-dy)
     *  and adjust the layout of the workSurface in the drawArea
     */
    public void resizeWorkSurface(float dx,float dy,boolean sizeable) {
        int scale = getZoomCurrent();
        workSurfaceUpdate(dx,dy,sizeable);
        Dimension dim = new Dimension(getWorkX(),getWorkY());
        setPreferredSize(new Dimension((int)(dim.width*zoomSizes[scale]),(int)(dim.height*zoomSizes[scale])));
        if (!(((Container)getParent()).getLayout() instanceof java.awt.BorderLayout))
            resizeComponent();
       // if (!(isMain()))
         //   updateCellProcess();

    }

    public void resizeComponent() {
        Rectangle viewPortRect = FlowComposer.viewPortRect;
        Dimension processRect = getPreferredSize();
        FlowLayout layout = ((FlowLayout)((Container)getParent()).getLayout());
        int height = viewPortRect.height-processRect.height;
        int width = viewPortRect.width-processRect.width;
        if (processRect.height<viewPortRect.height) {
            layout.setVgap(height/2);
            ((JComponent)getParent()).revalidate();
        }
        if (processRect.width<viewPortRect.width) {
            layout.setHgap(width/2);
            ((JComponent)getParent()).revalidate();
        }
        if (height<0) {
            layout.setVgap(0);
            ((JComponent)getParent()).revalidate();
        }
        if (width<0) {
            layout.setHgap(0);
            ((JComponent)getParent()).revalidate();
        }
    }

    /** 
     * Basic layout algorithm of <code>ProcessGraph</code> if there is no chart graphical with file RDF
     * This method is used for process which have branches of the graph connects <code>PortProcessCell</code>.
     */
    public void basicProcessLayout(int dx,int dy) {
        int y=0;
        int x = dx;
        int yFinal=0;

        Vector vectorCell = new Vector();
        ArrayList cellProcess = getCellProcess(FCConstants.PORTINPUT);
        Iterator it =cellProcess.iterator();
        while (it.hasNext()) {
            PortProcessCell cell = (PortProcessCell)it.next();
            PortCell port = (PortCell)cell.getChildAt(0);
            Object edge = (port.edges()).next();
            Object target = getTargetVertex(edge);
            CellView targetView = getTargetView(edge);
            CellView sourceView = getSourceView(edge);
            Rectangle rectSource = sourceView.getBounds();
            Rectangle rectTarget = targetView.getBounds();
            if (!(vectorCell.contains(target))) {
                updatePosition(target,new Point(dx,rectSource.y-(rectTarget.height/2)));
                vectorCell.add(target);
                x+= rectTarget.width + dx;
                yFinal = rectSource.y+(rectTarget.height/2)+dy;
                if (y>rectSource.y-(rectTarget.height/2))
                    y = nextCellLayout(target,x,y,dx,dy,vectorCell);

                else {
                    y = nextCellLayout(target,x,rectSource.y-(rectTarget.height/2),dx,dy,vectorCell);
                }
                x=dx;
            }
        }
        // basicMainLayout(dx,yFinal,y,dx,dy,vectorCell);
        basicComponentLayout(dx,yFinal,y,dx,dy,vectorCell);
    }

    /** 
     * Basic layout algorithm of <code>ProcessGraph</code> if there is no chart graphical with file RDF
     * This method is used for the main process which have no <code>PortProcessCell</code> and in the other
     * process which have branches of the graph not connects to <code>PortProcessCell</code>.
     */
    public void basicComponentLayout(int x,int y,int yCurrent,int dx,int dy,Vector vect) {
        //int xCurrent = dx;
        //int yCurrent=0;
        Object[] list = getVertices(getAll());
        if (vect==null) vect = new Vector();
        for (int i = 0; i < list.length; i++) {
            ArrayList child = getPortsCell(list[i],FCConstants.PORTINPUT);
            if (child.size()==0) {
                if (!(vect.contains(list[i]))) {
                    updatePosition(list[i],new Point(x,y));
                    vect.add(list[i]);
                    CellView view = getView().getMapping(list[i], false);
                    Rectangle rectView = view.getBounds();
                    x+= rectView.width + dx;
                    if (yCurrent>y)
                        yCurrent = nextCellLayout(list[i],x,yCurrent,dx,dy,vect);
                    else {
                        yCurrent = nextCellLayout(list[i],x,y,dx,dy,vect);
                    }
                    y=rectView.y+rectView.height+dy;
                    x=dx;
                }
            }

        }
    }

    private int nextCellLayout(Object cell,int xCurrent,int yCurrent,int dx,int dy,Vector vect) {
        int yNext=0;
        int x = xCurrent;
        int y = yCurrent;
        ArrayList child = getPortsCell(cell,FCConstants.PORTOUTPUT);
        Vector vectorCell = new Vector();
        int size = child.size();
        if (child.size()!=0) {
            Iterator it =child.iterator();
            while (it.hasNext()) {
                PortCell port = (PortCell)it.next();
                Object edge = (port.edges()).next();
                Object target = getTargetVertex(edge);
                if (!(target instanceof PortProcessCell)){
                    if (!(vect.contains(target))) {
                        CellView targetView = getTargetView(edge);
                        updatePosition(target,new Point(x,y));
                        vect.add(target);
                        Rectangle rectTarget = targetView.getBounds();
                        y = rectTarget.y +rectTarget.height+dy;
                        xCurrent=rectTarget.x + rectTarget.width + dx;
                        if (yNext>yCurrent) {
                            yNext = nextCellLayout(target,xCurrent,yNext,dx,dy,vect);
                        }
                        else {
                            yNext = nextCellLayout(target,xCurrent,yCurrent,dx,dy,vect);
                            yCurrent = rectTarget.y +rectTarget.height+dy;
                        }

                    }
                }
                // else { yCurrent

            }
        }
        return y;
    }

    private void workSurfaceUpdate(float dx,float dy,boolean resize) {
        float x;
        float y;
        /*GraphView v = getView();
        ArrayList result = getAllViewCell();
        Iterator it = result.iterator();
        System.out.println(result.size()+"size");*/
        /*while (it.hasNext()) {
        CellView view = (CellView)it.next();
        System.out.println(view);
        Rectangle rect = view.getBounds();
        Map map = GraphConstants.createMap();
        Map attributes = new Hashtable();
        float factorX = dx/workSurfaceX;
        float factorY = dy/workSurfaceY;
        Dimension dim = new Dimension(rect.width,rect.height);
        if (resize){
        x = rect.x + (rect.x*factorX);
        y = rect.y + (rect.y*factorY);
        }
        else {
        x = rect.x - (rect.x*factorX);
        y = rect.y - (rect.y*factorY);
        }
        Point point = new Point();
        point.setLocation(x,y);
        System.out.println(point);
        GraphConstants.setBounds(map,new Rectangle(point,dim));
        attributes.put(view,map);
        v.edit(attributes);
        } */
        if (resize) {
            workSurfaceX += dx;
            workSurfaceY += dy;
        }
        else {
            workSurfaceX -= dx;
            workSurfaceY -= dy;
        }

    }

    /** Return the Cell whose the userObject is <code>userObject</code>
     * @param userObject An Object
     * @return The Cell 
     */
    public Object getChannel(Object userObject){

        Object[] roots = getAll();
        DefaultGraphCell cell;
        for (int i = 0 ; i <roots.length ;i++)
        {
            if (roots[i] instanceof Edge)
            {
                cell = (DefaultGraphCell)(roots[i]);
                if(cell.getUserObject() != null && cell.getUserObject().equals(userObject))
                    return cell;
            }

        }
        return null;



    }

    /** Return the Cell whose the userObject is <code>userObject</code>
     * @param userObject An Object
     * @return The Cell 
     */
    public Object getCall(Object userObject){

        Object[] roots = getAll();
        DefaultGraphCell cell;
        for (int i = 0 ; i <roots.length ;i++)
        {
            if (!(roots[i] instanceof Edge))
            {
                cell = (DefaultGraphCell)(roots[i]);
		
                if(cell.getUserObject() != null && cell.getUserObject().equals(userObject))
                    return cell;
            }

        }
        return null;
    }



    
    /**
     * Returns the edge called <code>name</code>.
     * @param name  a String.
     */
    public Object getEdge(String name){
        Object[] roots = getAll();
        for (int i=0; i < roots.length; i++){
            if (isEdge(roots[i]) && roots[i].toString().equals(name))
                return roots[i];

        }
        return null;
    }


    /**
     *  Returns the array of all edges include in the cells array giving in parameter.
     * @param cells  A object's array. if cells is null getVertices return null.
     */
    public Object[] getEdges(Object[] cells) {
        if (cells != null) {
            ArrayList result = new ArrayList();
            for (int i = 0; i < cells.length; i++)
                if (isEdge(cells[i]))
                    result.add(cells[i]);
            return result.toArray();
        }
        return null;
    }

    /**
     * Return the cell Neighboor of the vertex
     * @param edge The edge between the vertex and his neighboor
     * @param vertex A vertex.
     */

    public Object getNeighbour(Object edge, Object vertex) {
        Object source = getSourceVertex(edge);
        if (vertex == source)
            return getTargetVertex(edge);
        else
            return source;
    }

    /**
     * Return the source of the specified edge.
     * @param edge An object.
     */
    public Object getSourceVertex(Object edge) {
        Object sourcePort = graphModel.getSource(edge);
        return graphModel.getParent(sourcePort);
    }

    /**
     * Return  the source of the specified edge.
     * @param edge An object.
     *
     */
    public Object getTargetVertex(Object edge) {
        Object targetPort = graphModel.getTarget(edge);
        return graphModel.getParent(targetPort);
    }

    /**
     *  Return  the source's view of the specified edge.
     *  @param edge An object.
     */
    public CellView getSourceView(Object edge) {
        Object source = getSourceVertex(edge);
        return getView().getMapping(source, false);
    }

    /** 
     * Override the method for specialize the selection's cell : we remove
     * the <code>PortProcessCell</code> and <code>PortCell</code>.
     */
    public Object[] getSelectionCells() {
        Object[] tmp = getSelectionModel().getSelectionCells();
        ArrayList list = new ArrayList();
        for (int i = 0; i <tmp.length; i++){
            DefaultGraphCell cell = (DefaultGraphCell)tmp[i];
            if ((!(cell instanceof PortProcessCell))&&(!(cell instanceof PortCell)))
                list.add(tmp[i]);
        }
        return list.toArray();
    }

    /**
     *  Return  the target's view of the specified edge.
     *  @param edge An object.
     */
    public CellView getTargetView(Object edge) {
        Object target = getTargetVertex(edge);
        return getView().getMapping(target, false);
    }

    /**
     * Return all edges between two vertices.
     *
     */
    public Object[] getEdgesBetween(Object vertex1, Object vertex2) {
        ArrayList result = new ArrayList();
        Set edges = DefaultGraphModel.getEdges(graphModel, new Object[]{vertex1});
        Iterator it = edges.iterator();
        while (it.hasNext()) {
            Object edge = it.next();
            Object source = getSourceVertex(edge);
            Object target = getTargetVertex(edge);
            if ((source == vertex1 && target == vertex2) ||
                    (source == vertex2 && target == vertex1))
                result.add(edge);
        }
        return result.toArray();
    }

    /**
     * Return true if the specified name is not already allowing for a call
     */
    public boolean isSingleId(Object call, String id){

        Object[] roots = getAll();
        DefaultGraphCell cell;
        Object userObject;
        Call tmpCall;

        for (int i = 0; i < roots.length; i++){
            if (!(roots[i] instanceof Edge))
            {
                cell = (DefaultGraphCell)(roots[i]);
                userObject = cell.getUserObject();
                tmpCall = ((userObject instanceof Call)? (Call)userObject : null );
                if(tmpCall != null)
                {
                    if(!(tmpCall.equals(call)) && tmpCall.getId().equals(id))
                        return false;
                }
            }
        }
        return true;
    }

    /** 
     *  Returns true if in the <code>ProcessGraph</code> we have the possibility
     *  to connect two cells with the constraints of <code> ProcessGraphModel</code>
     */
    public boolean canConnect() {
        int a=0,b=0,i=0,j=0;
        boolean connect = false;
        boolean input1=false,input2 = false;
        boolean output1=false,output2 = false;
        Object[] portsInput = getPorts(getAll(),FCConstants.PORTINPUT);
        Object[] portsOutput = getPorts(getAll(),FCConstants.PORTOUTPUT);
        if (portsInput!=null) {
            while ((a<portsInput.length)&&((!input1)||(!input2))) {
                if (Utils.lenght(((DefaultPort)portsInput[a]).edges())>0) {a++;}
                else { Object ParentInput = ((DefaultPort)portsInput[a]).getParent();
                if (ParentInput instanceof PortProcessCell) input1=true;
                else input2=true;
                a++;
                }
            }
        }
        if (input1&&input2) connect=true;
        if (portsOutput!=null) {
            while ((b<portsOutput.length)&&((!output1)||(!output2))) {
                if (Utils.lenght(((DefaultPort)portsOutput[b]).edges())>0) {b++;}
                else { Object ParentOutput = ((DefaultPort)portsOutput[b]).getParent();
                if (ParentOutput instanceof PortProcessCell) output1=true;
                else output2=true;
                b++;
                }
            }
        }
        if (output1&&output2) connect=true;
        if (portsInput!=null&&portsOutput!=null) {
            while ((i<portsInput.length)&&(!connect)) {
                if (Utils.lenght(((DefaultPort)portsInput[i]).edges())>0) {i++;}
                else {  Object parentInput = ((DefaultPort)portsInput[i]).getParent();
                if (parentInput instanceof PortProcessCell) input1=true; else input2=true;
                while ((j<portsOutput.length)&&(!connect)) {
                    if (Utils.lenght(((DefaultPort)portsOutput[j]).edges())>0) {j++;}
                    else { Object parentOutput = ((DefaultPort)portsOutput[j]).getParent();
                    if (parentOutput instanceof PortProcessCell) output1=true; else output2=true;
                    if ((!parentInput.equals(parentOutput))&&((!(input1&&output2))||(!input2&&output1))) {connect=true;}
                    else {j++;}
                    }
                    
                }
                i++;
                j=0;
                }
            }
        }
        return connect;
    }

    /** Returns an array of all ports of the specified array of cells and type's port*/
    public Object[] getPorts(Object[] cells,int type) {
        if (cells != null) {
            ArrayList result = new ArrayList();
            for (int i = 0; i < cells.length; i++) {
                if (!((DefaultGraphCell)cells[i]).isLeaf()) {
                    java.util.List children = ((DefaultGraphCell)cells[i]).getChildren();
                    Iterator it = children.iterator();
                    while (it.hasNext()) {
                        Object port = it.next();
                        if (type==((PortCell)port).getType())
                            result.add(port);
                    }
                }
            }
            return result.toArray();
        }
        return null;
    }



    /**
     * Returns an array of all ports of the specified cell and type's port
     *
     */
    public ArrayList getPortsCell(Object vertex1,int type) {
        ArrayList result = new ArrayList();
        if (!((DefaultGraphCell)vertex1).isLeaf()) {
            java.util.List children = ((DefaultGraphCell)vertex1).getChildren();
            Iterator it = children.iterator();
            while (it.hasNext()) {
                Object port = it.next();
                //System.out.println(port);
                if (type==((PortCell)port).getType())
                    result.add(port);
            }
        }
        return result;

    }

    /**
     * Returns the cell with the specified name
     */
    public FCGraphCell getCell(String name){
        Object[] roots = getAll();
        for (int i=0; i < roots.length; i++){
            if (roots[i] instanceof FCGraphCell && roots[i].toString().equals(name))
                return (FCGraphCell)roots[i];
        }
        return null;
    }

    /**
     * Returns an array of all <code>PortProcessCell</code> for the specified type
     */
    public ArrayList getCellProcess(int portLocation) {
        Object[] roots = getAll();
        ArrayList result = new ArrayList();
        for (int i=0; i < roots.length; i++){
            if (roots[i] instanceof PortProcessCell && (((Port)((PortCell)((PortProcessCell)roots[i]).getChildAt(0)).getUserObject()).getType() == portLocation)){
                result.add(roots[i]);
            }
        }
        return result;
    }

    /**
     * return an array of View's cells from composant such as ApplyProces,Serialize....
     */
    public ArrayList getAllViewCell() {
        Object[] roots = getAll();
        ArrayList result = new ArrayList();
        for (int i = 0; i < roots.length; i++){
            DefaultGraphCell cell = (DefaultGraphCell)roots[i];
            if ((!(cell instanceof PortProcessCell))&&(!(cell instanceof PortCell))&&(!(cell instanceof DefaultEdge)))
                result.add(getView().getMapping(roots[i],false));

        }
        return result;
    }


    /**
     * add a Cell to the model with this default attribute's view
     * like bounds,location
     *
     */
    public DefaultGraphCell addVertex(Object userObject,int type) {

        int u = GraphConstants.PERCENT;
        boolean location = true;
        Rectangle bounds;
        double scale = getScale();
        Point position = new Point(10,10);
        if(marquee instanceof FCMarqueeHandler && ((FCMarqueeHandler)marquee).getCurrentPoint()!= null)
            position = new Point(((FCMarqueeHandler)marquee).getCurrentPoint());
        DefaultGraphCell cell = null;
        Map viewMap = new Hashtable();
        Map attributes = null;
        fromScreen(position);
        // Create Vertex
        switch (type) {

            case FCConstants.MERGE :
                cell = new MergeCell(userObject,this);
                bounds = new Rectangle(position,boundsCube);
                attributes = commonAttributres(cell,bounds);
                break;
            case  FCConstants.GENERATE :
                cell = new GenerateCell(userObject,this);
                bounds = new Rectangle(position,boundsRect);
                attributes = commonAttributres(cell,bounds);
                break;
            case  FCConstants.SERIALIZE :
                cell = new SerializeCell(userObject,this);
                bounds = new Rectangle(position,boundsRect);
                attributes = commonAttributres(cell,bounds);
                break;
            case FCConstants.DISPATCH :
                cell = new DispatchCell(userObject,this);
                bounds = new Rectangle(position,boundsCube);
                attributes = commonAttributres(cell,bounds);
                break;
            case FCConstants.APPLYEXTERNAL :
                cell = new ApplyExternalCell(userObject,this);
                bounds = new Rectangle(position,boundsRect);
                attributes = commonAttributres(cell,bounds);
                GraphConstants.setBackground(attributes,Color.lightGray);
                GraphConstants.setOpaque(attributes, true);
                break;
            case FCConstants.APPLYPROCESS :
                cell = new ApplyProcessCell(userObject,this);
                bounds = new Rectangle(position,boundsRect);
                attributes = commonAttributres(cell,bounds);
                break;
            case FCConstants.APPLYRULESET :
                cell = new ApplyRulesetCell(userObject,this);
                bounds = new Rectangle(position,boundsRect);
                attributes = commonAttributres(cell,bounds);
                break;
            case FCConstants.APPLYQUERY :
                cell = new ApplyQueryCell(userObject,this);
                bounds = new Rectangle(position,boundsCube);
                attributes = commonAttributres(cell,bounds);
                break;
            case FCConstants.PORTPROCESS :
                cell = new PortProcessCell(null);
                bounds = new Rectangle(position,boundsPort);
                attributes = commonAttributres(cell,bounds);
                if(userObject instanceof Port)
                    addPort(cell,viewMap,(Port)userObject);

                break;
            default: System.out.println("pas de vertex correspondant � la string : FlowComposerGraph+meth commonAttributres");break;

        }
        viewMap.put(cell,attributes);
        getModel().insert(new Object[]{cell}, null, null, viewMap);
        if(cell instanceof FCGraphCell)
            ((FCGraphCell)cell).createPorts();
        return cell;
    }

    /**
     * delete a cell array of Transmorpher graph with all edges connected at these cells
     * Used for delete a current cell's selection
     */
    public void delete(Object[] cells){
        if(cells != null && cells.length > 0){
            for(int i=0;i<cells.length;i++){
                delete(cells[i]);
            }
        }
    }

    /**
     * delete a cell of Transmorpher graph with all edges connected at this cell
     */
    public void delete(Object cell){
	
	// The test getModel().contains(cell) is necessary.
        if(cell instanceof DefaultGraphCell && getModel().contains(cell)){
            Object userObject = ((DefaultGraphCell)cell).getUserObject();
            Process process = (Process)this.userObject;
	   	    
            if(userObject instanceof Channel)
                process.removeChannel((Channel)userObject);
            else if(userObject instanceof Port){

            }
            else if(userObject instanceof Call){
                process.removeCall((Call)userObject);
            }
        }

    }

    /**
     * fill the default attribute's view for a cell in a map
     */
    public Map commonAttributres(DefaultGraphCell cell, Rectangle bounds) {
        Map map = GraphConstants.createMap();
        if(cell instanceof PortProcessCell){
            GraphConstants.setEditable(map,false);
            GraphConstants.setMoveable(map, false);
        }
        GraphConstants.setBounds(map, bounds);
        GraphConstants.setBorderColor(map, Color.black);
        return map;

    }

    /**
     * add a new port in Transmorpher graph for a process or current
     * selection
     */
    public void newPort(int type){
        Object[] tmp = getSelectionCells();
        DefaultGraphCell cell;
        String name ="";
        Port port ;
        if (tmp.length != 0) {
            for (int i = 0; i <tmp.length; i++){
                cell = (DefaultGraphCell)tmp[i];
                if (cell instanceof FCGraphCell){
                    ((FCGraphCell)cell).newPort(type);
                }
            }
        }
        else {
            TransformationImpl transformation = (TransformationImpl)userObject;
            int number;
            if (type == FCConstants.PORTINPUT){
                number = transformation.inPorts().length();
                port= new Port("",transformation,number,type);
                transformation.addIn(port);
            }
            else{
                number = transformation.outPorts().length();
                port= new Port("",transformation,number,type);
                transformation.addOut(port);
            }
        }
    }
    
    /**
     * Add a new Channel in Transmorpher Graph.
     *
     *
     */
    public Object newChannel(Object sourcePort, Object targetPort, String name){
	 
	Channel channel = null;
	Process currentTransformation = (Process)userObject;
         
	if (sourcePort instanceof Port && targetPort instanceof Port) {
	    
	    String defaultName = getDefaultChannelName(name,0);
	    channel = new Channel( defaultName,currentTransformation,(Port)sourcePort,(Port)targetPort);
	    
	    currentTransformation.addChannel(channel);
	    
	    return channel ;
	    
	} // end of if (sourcePort instanceof Port && targetPort instanceof Port)
	
	return channel ;
    }



    /**
     * add a port to a cell
     */
    public void addPort(DefaultGraphCell cell,Map viewMap, Port port) {
        int u = GraphConstants.PERCENT;
        Map map = GraphConstants.createMap();
        GraphConstants.setOffset(map, new Point((int)(u/2),(int) (u/2)));
       // GraphConstants.setBackground(map,Color.red);
        //GraphConstants.setOpaque(map,false);

        PortCell pc = new PortCell(port,port.getType());
        viewMap.put(pc, map);
        cell.add(pc);
    }
   

    /**
     * update the attribute's bounds view of the port's process for a type
     * gives : input or output. Called when a port is adding to a process or
     * the bound's workSurface is changed.
     */
    public void updateCellProcess(){
        updateCellProcess(FCConstants.PORTOUTPUT);
        updateCellProcess(FCConstants.PORTINPUT);
    }

    private void updateCellProcess(int portLocation){
        GraphView v = getView();
        Rectangle rect ;
        rect = new Rectangle(new Dimension(workSurfaceX,workSurfaceY));
       
        ArrayList cell = getCellProcess(portLocation);
        for (int i=0;i<cell.size();i++) {
            Map map = GraphConstants.createMap();
            Map attributes = new Hashtable();
            if (portLocation==FCConstants.PORTOUTPUT)
                GraphConstants.setBounds(map,new Rectangle(new Point(rect.width-10,(int)(rect.height*(i+1))/(cell.size()+1)),boundsPort));
            else
                GraphConstants.setBounds(map,new Rectangle(new Point(0,(int)(rect.height*(i+1))/(cell.size()+1)),boundsPort));
            CellView cellView = v.getMapping(cell.get(i),false);
            attributes.put(cellView,map);
            v.edit(attributes);
        }
    }

    /**
     * update the attribute's bounds view of a cell
     */
    public void updatePosition(Object cell,Point position){
        if(cell !=null && position != null){
            GraphView v = getView();
            Map map = GraphConstants.createMap();
            Map attributes = new Hashtable();
            CellView cellView = v.getMapping(cell,false);
            Rectangle rect = cellView.getBounds();
            GraphConstants.setBounds(map,new Rectangle(position,rect.getSize()));
            attributes.put(cellView,map);
            v.edit(attributes);
        }
    }

    /**
     * update the attribute's offset view of the port's cell for a type
     * gives : input or output. Called when a port is adding to a cell
     */

    public void updatePort(DefaultGraphCell cell,int type){

        GraphView v = getView();
        ArrayList children = getPortsCell(cell,type);
        for (int i=0;i<children.size();i++) {
            Map map = GraphConstants.createMap();
            int u = GraphConstants.PERCENT;
            Map attributes = new Hashtable();
            CellView portView = v.getMapping(children.get(i),false);
            if (type==FCConstants.PORTINPUT){
                GraphConstants.setOffset(map,new Point(0,(int)(u*(i+1))/(children.size()+1)));
            }
            else  {
                GraphConstants.setOffset(map,new Point(u,(int)(u*(i+1))/(children.size()+1)));
            }
            attributes.put(portView,map);
            v.edit(attributes);
        }
        if (children.size()>1) {
            CellView portView1 = v.getMapping(children.get(0),false);
            CellView portView2 = v.getMapping(children.get(1),false);
            Rectangle rect1 = portView1.getBounds();
            Rectangle rect2 = portView2.getBounds();


            if (rect2.y-(rect1.y+rect1.height)<5){
            //if (portView1.intersects(getGraphics(),portView2.getBounds())) {
                CellView cellView = portView1.getParentView();
                Rectangle r = cellView.getBounds();
                Map map = GraphConstants.createMap();
                Map attributes = new Hashtable();
                GraphConstants.setBounds(map,new Rectangle(r.x,r.y,r.width,r.height+10));
                attributes.put(cellView,map);
                v.edit(attributes);
                updatePort(cell,type);
            }
        }
    }

    /**
     * write a RDF file which is the graphical chart of a process
     */
    public void generateRDF(fr.fluxmedia.transmorpher.utils.Writer file) throws java.io.IOException{
        Object[] cells = getAll();
        file.writeln(6,"<hasProcess>");
        file.writeln(8,"<rdf:Description about=\""+((Process)userObject).getName()+"\">");
        for (int i = 0; i < cells.length; i++){
            if (cells[i] instanceof DefaultEdge)
                ChannelRDF(cells[i],file);
            else if (!(cells[i] instanceof PortProcessCell)&&(!(cells[i] instanceof PortCell)))
                CellRDF(cells[i],file);
        }
        file.writeln(8,"</rdf:Description>");
        file.writeln(6,"</hasProcess>");
    }

    /**
     * RDF representation of a cell
     */
    public void CellRDF(Object cell,fr.fluxmedia.transmorpher.utils.Writer file) throws java.io.IOException{
        Rectangle rect = getCellBounds(cell);
        String x = (new Integer(rect.x)).toString();
        String y = (new Integer(rect.y)).toString();
        Object call = ((DefaultGraphCell)cell).getUserObject();
        GraphView v = getView();
        CellView cellview = v.getMapping(cell,false);
        Map map = cellview.getAttributes();
        Color color = GraphConstants.getBorderColor(map);
        file.writeln(10,"<hasComponent>");
        file.writeln(12,"<rdf:Description about=\""+call.toString()+"\">");
        //file.writeln(14,"<s:color>"+color.toString()+"</s:color>");
        file.writeln(14,"<s:x>"+x+"</s:x>");
        file.writeln(14,"<s:y>"+y+"</s:y>");
        file.writeln(12,"</rdf:Description>");
        file.writeln(10,"</hasComponent>");
    }

    /**
     * RDF represenattion of a channel
     */
    public void ChannelRDF(Object cell,fr.fluxmedia.transmorpher.utils.Writer file) throws java.io.IOException{
        Object userObject = ((DefaultGraphCell)cell).getUserObject();
        GraphView v = getView();
        CellView cellview = v.getMapping(cell,false);
        Map map = cellview.getAttributes();
        file.writeln(10,"<hasChannel>");
        file.writeln(12,"<rdf:Description about=\""+userObject.toString()+"\">");
        file.writeln(14,"<s:points>");
        if(cellview instanceof EdgeView){
            int nbPoint = ((EdgeView)cellview).getPointCount();
            Point point ;
            for(int i=0;i < nbPoint;i++){
                point =((EdgeView)cellview).getPoint(i);
                file.writeln(17,"<s:point x=\""+point.x +"\" y=\""+point.y +"\"/>");
            }
            point = null;
        }
        file.writeln(14,"</s:points>");
        file.writeln(12,"</rdf:Description>");
        file.writeln(10,"</hasChannel>");
    }


   
    
}

