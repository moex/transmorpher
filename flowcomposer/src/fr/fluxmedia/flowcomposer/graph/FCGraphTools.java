/**
 * $Id: FCGraphTools.java,v 1.4 2002-09-13 17:53:56 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.graph;

import com.jgraph.JGraph;
import com.jgraph.graph.*;
import java.awt.Point;
import java.awt.*;
import java.util.*;
import javax.swing.JOptionPane;

import fr.fluxmedia.flowcomposer.utils.FCConstants;

   /**
     * Implementation of useful algorithms for the management of
     * grah --> kruskal algorithm & Union-Find.
     *
     * @version 0.2
     * @author Chomat/Saint-Marcel
     */
    
public class FCGraphTools {

     
    /**
      * Kruskal Algorithm for detecting circuits from the editing graph
      */         
    public Object[] getSpanningTree(ProcessGraph graph, CostFunction cf) {
          if (cf == null)
          cf = createDefaultCostFunction();
          Object[] all = graph.getAll();
          Map map = new Hashtable();
          Iterator edges = sort(graph, graph.getEdges(all),map);
      
          //System.out.println(edges);
          UnionFind uf = new UnionFind();
          HashSet result = new HashSet();
          Vector vector = new Vector();
          while (edges.hasNext()) {
              Object edge = edges.next();
              //System.out.println(edge);
              edges.remove();
              Object cellA, cellB;
              Object setA, setB;
              cellA = graph.getSourceVertex(edge);
              cellB = graph.getTargetVertex(edge);
              setA = uf.find(cellA);
              setB = uf.find(cellB);
              if (setA == null || setB == null || setA != setB) {
                 uf.union(setA, setB);
                 result.add(edge);
              }
              else {
                 Rectangle source= null;
                 Rectangle target= null;
                 Object portTarget = ((Edge)edge).getTarget();
                 Object portSource = ((Edge)edge).getSource();
                 CellView viewSource = graph.getView().getMapping(portSource, false);
                 CellView viewTarget = graph.getView().getMapping(portTarget, false);
                 source = viewSource.getBounds();
                 target = viewTarget.getBounds();
                 if (source.x<target.x){
                     if (((PortCell)portSource).getType()==FCConstants.PORTOUTPUT)
                         result.add(edge);
                 }
                 else { 
                     if (((PortCell)portTarget).getType()==FCConstants.PORTOUTPUT)
                         result.add(edge);
                 }
              }
          }   
          // Create set of vertices
          HashSet v = new HashSet();
          Iterator it = result.iterator();
          while (it.hasNext()) {
                Object edge = it.next();
                Object source = graph.getSourceVertex(edge);
                Object target = graph.getTargetVertex(edge);
                if (source != null)
                    v.add(source);
                if (target != null)
                    v.add(target);
          }
          Object[] cells = new Object[result.size()+v.size()];
          System.arraycopy(result.toArray(), 0, cells, 0, result.size());
          System.arraycopy(v.toArray(), 0, cells, result.size(), v.size());
          return cells;
    }

    /**
      * Sorting all the channels of graph with a Comparator
      * used to control the order of data structure : <code>DefaultCostFunction</code> 
      */

    public Iterator sort(final JGraph graph, Object[] cells,Map map) {
       Rectangle source= null;
       Rectangle target= null;
       ArrayList listEdge = new ArrayList(); 
       for (int i = 0; i < cells.length; i++){
           Object portTarget = ((Edge)cells[i]).getTarget();
           Object portSource = ((Edge)cells[i]).getSource();
           CellView viewSource = graph.getView().getMapping(portSource, false);
           CellView viewTarget = graph.getView().getMapping(portTarget, false);
           source = viewSource.getBounds();
           target = viewTarget.getBounds();
           if (source.x<target.x){
               if (((PortCell)portSource).getType()==FCConstants.PORTINPUT)
                    map.put(cells[i],target);
               else     
                    map.put(cells[i],source);
           }
           else { 
              if (((PortCell)portTarget).getType()==FCConstants.PORTINPUT)
                    map.put(cells[i],source);
              else    
                    map.put(cells[i],target);
           }       
       }    
       
       Object[] cellSorted = sorting(cells,map);
     
       for (int j=cellSorted.length-1;j>=0;--j) {
           System.out.println(cellSorted[j]); 
           listEdge.add(cellSorted[j]);
       }
       return listEdge.iterator();  
    }

    
    private boolean inferieur(Object o1, Object o2,Map map) {
        //Rectangle r1 = getCellBounds(o1);
        //Rectangle r2 = getCellBounds(o2);
        Rectangle r1 =(Rectangle)map.get(o1);
        Rectangle r2 =(Rectangle)map.get(o2);
        
        return r1.x < r2.x;
    }

    private void exchange(Object[] cell,int i, int j) {
        Object o = cell[i];
        cell[i]=cell[j];
        cell[j]=o;
    }

     /** implementation insertion sort */
    public Object[] sorting(Object[] cell,Map map) {
        for (int i = 0; i < cell.length - 1; i++) {
            int max = i;
            for (int j = i + 1; j < cell.length; j++) {
                if (inferieur(cell[max],cell[j],map)) {
                    max = j;
                }
            }
            exchange(cell, i, max);
        }
        return cell;
    }

    
    
    
    /** Creates an empty <code>DefaultCostFunction</code>. */
    public CostFunction createDefaultCostFunction() {
            return new DefaultCostFunction();
    }

    /**
     * Declaration of interface Cost Function
     */
    public interface CostFunction {
         public double getCost(JGraph graph, Object cell);
    }

    /**
     * The <code>DefaultCostFunction</code> class serves to ordering
     * the channels by their lenght  
     *
     */
    public class DefaultCostFunction implements CostFunction {
    
    
    
    public double getCost(JGraph graph, Object cell) {
            CellView view = graph.getView().getMapping(cell, false);
            return getLength(view);
    }

    }
    /**
     * Returns the lenght of a channel 
     */
     public static double getLength(CellView view) {
      double cost = 1;
      if (view instanceof EdgeView) {
	EdgeView edge = (EdgeView) view;
	Point last = null, current = null;
	for (int i = 0; i < edge.getPointCount(); i++) {
	  current = edge.getPoint(i);
	  if (last != null)
	    cost += last.distance(current);
	  last = current;
	}
      }
      return cost;
    }

   
    /**
     * Implementation of Union-Find algorithm.It makes party of family of algorithms for maintaining equivalence 
     * classes.Two of the most crucial operations that should be supported by such an algorithm are 
     * Find ("find the equivalence class of an element a") and Union ("unite two equivalence classes").Two 
     * vertices a and b occur in the same connected component of the graph if and only if a ~ b  
     */
     
    public class UnionFind {

      protected Hashtable sets = new Hashtable(),
      cells = new Hashtable();

      /* Return the number of distinct sets. */
      public int getSetCount() {
            return sets.size();
      }

      /** Return an object identifying the set that contains the given cell. */
      public Object find(Object cell) {
        Object set = null;
        if (cell != null) {
            set = cells.get(cell);
            if (set == null) {
                set = cell;
                cells.put(cell, set);
                HashSet contents = new HashSet();
                contents.add(cell);
                sets.put(set, contents);
            }
        }
        return set;
      }

      /** Union the given sets such that all elements belong to the same set. */
      public Object union(Object set1, Object set2) {
            if (set1 != null && set2 != null && set1 != set2) {
                HashSet tmp1 = (HashSet) sets.get(set1);
                HashSet tmp2 = (HashSet) sets.get(set2);
                if (tmp1 != null && tmp2 != null) {
                    if (tmp1.size() < tmp2.size()) {
                        Object tmp = tmp1; tmp1 = tmp2; tmp2 = (HashSet) tmp;
                        tmp = set1; set1 = set2; set2 = tmp;
                    }
                    tmp1.addAll(tmp2);
                    sets.remove(set2);
                    Iterator it = tmp2.iterator();
                    while (it.hasNext())
                        cells.put(it.next(), set1);
                }
            }
            return set1;
      }

    }

}
