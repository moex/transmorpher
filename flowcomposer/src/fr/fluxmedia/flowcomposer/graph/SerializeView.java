/**
 * $Id: SerializeView.java,v 1.3 2002-09-13 17:53:56 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.graph;

import com.jgraph.graph.VertexView;
import com.jgraph.graph.CellMapper;
import com.jgraph.graph.CellViewRenderer;
import com.jgraph.JGraph;
import com.jgraph.graph.VertexRenderer;
import com.jgraph.graph.GraphConstants;

import java.awt.*;

/**
 * The <code>SerializeView</code> class is needed to define the special visual
 * aspects of a <code>SerializeCell</code>.It contains an inner class which serves
 * as a renderer that provides the painting code.The method that need
 * to be overriden is getRenderer to return the correct renderer.
 *
 * @version 0.2
 * @author Chomat/Saint-Marcel
 */

public class SerializeView extends VertexView {
    
   

    /** Instanciation of the renderer. */   
   protected CellViewRenderer renderer = new SerializeRenderer();
    
    /** Constructs the view.
     * @param cell the SerializeCell Object
     * @param graph it's owner graph
     * @param cm the mapping from cell to view
     */
    public SerializeView(Object cell, JGraph graph, CellMapper cm) {
        super(cell, graph, cm);
    }
    
    /** Returns the renderer for the view. */
    public CellViewRenderer getRenderer() {
        return renderer;
    }
    
    /**
     * Change the renderer of this view.
     *
     * @param renderer a <code>CellViewRenderer</code>. if <code>renderer</code> is null, 
     * this method create a default renderer.
     */   
    public void setRenderer(CellViewRenderer renderer){
	this.renderer = renderer;
	if (this.renderer == null) {
	    this.renderer = new SerializeRenderer();
	}
    }
 
    /**
     * The renderer itself is an instance of the JComponent class, with an overriden
     * paint method that paints a cell based on its attributes
     */
    class SerializeRenderer extends VertexRenderer {

	public void paint(Graphics g) {
	    
	    int b = borderWidth;
	    Graphics2D g2 = (Graphics2D) g;
	    Dimension d = getSize();
	    boolean tmp = selected;
	    if (super.isOpaque()) {
		g.setColor(super.getBackground());
		drawSerialize(g,d,b);
            }
	    try {
		setBorder(null);
		setOpaque(false);
		selected = false;
		super.paint(g);
	    } finally {
		selected = tmp;
	    }
	    if (bordercolor != null) {
		g.setColor(bordercolor);
		g2.setStroke(new BasicStroke(b));
		drawSerialize(g,d,b);
	    }
	    if (selected) {
		g2.setStroke(GraphConstants.SELECTION_STROKE);
		g.setColor(graph.getHighlightColor());
		drawSerialize(g,d,b);
	    }
	}
	
	private void drawSerialize(Graphics g,Dimension d , int b ) {
            g.drawPolygon(new int[]{b-1,d.width/3,d.width-1,d.width-1,d.width/3},
                          new int[]{d.height/2,b-1,b-1,d.height-1,d.height-1},
                          5);
	    
	}
    }
}
