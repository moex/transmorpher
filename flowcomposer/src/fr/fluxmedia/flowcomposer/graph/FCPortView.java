/**
 *
 *$Id: FCPortView.java,v 1.5 2002-12-12 10:25:58 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.graph;

// import JGraph
import com.jgraph.JGraph;
import com.jgraph.graph.*;
import com.jgraph.event.*;
import com.jgraph.graph.PortRenderer;
import com.jgraph.graph.GraphConstants;

import java.awt.*;
import java.util.Map;

/**
     * The <code>FCPortView</code> is needed to define the special visual
     * aspects of an <code>FCPortCell</code>.It contains an inner class which serves
     * as a renderer that provides the painting code.The method that need
     * to be overriden is getRenderer to return the correct renderer.
     *
     * @version 0.2
     * @author Chomat/Saint-Marcel
     */


public class FCPortView extends PortView{

    /** Instanciation of the renderer. */  
    protected CellViewRenderer renderer = new FCPortRenderer();

    protected int glyphSize = 10;

     /** Constructs the view.
     * @param cell the ApplyRulesetCell Object
     * @param graph it's owner graph
     * @param mapper the mapping from cell to view
     */
    public FCPortView(Object cell, JGraph graph, CellMapper mapper) {
        super(cell, graph, mapper);
    }

    /**
     * Returns the bounds for the port view.
     */
    public Rectangle getBounds() {
        Rectangle bounds = new Rectangle(getLocation(null));
        bounds.x = bounds.x-glyphSize/2;
        bounds.y = bounds.y-glyphSize/2;
        bounds.width = bounds.width + glyphSize;
        bounds.height = bounds.height + glyphSize;
        return bounds;
    }

    public CellViewRenderer getRenderer() {
          return renderer;
    }
    
    /**
     * Change the renderer of this view.
     *
     * @param renderer a <code>CellViewRenderer</code>. if <code>renderer</code> is null, this method create a default renderer.
     */
    
    public void setRenderer(CellViewRenderer renderer){
	this.renderer = renderer;
	if (this.renderer == null) {
	    this.renderer = new FCPortRenderer();
	}
    }
    
    /**
	 * The renderer itself is an instance of the JComponent class, with an overriden
	 * paint method that paints a cell based on its attributes
	 */
    class FCPortRenderer extends PortRenderer {
    
	
	public void paint(Graphics g){
	    Dimension d = this.getSize();
	    g.setClip(0, 0, d.width + 1, d.height + 1);
	    if (preview)
		//The preview flag is interpreted differently in the PortRenderer than in all the other renderers.
		//The preview flag in the PortRenderer is interpreted as "highlight", and is used to  draw a special
		//border around the port if the mouse is moved over it (in special cases, namely, if you want to connect an edge).
		paintPreview(g, d);
	    else
		paintPort(g, d);
	}
	
	
	protected void paintPreview(Graphics g, Dimension d ){
	    Map attr = view.getAttributes();
	    int offset = ( GraphConstants.getOffset(attr) != null ) ? 0 : 1;
	    g.setXORMode(graph.getBackground());
	    g.setColor(Color.green);
	    g.drawRect(0, 0, d.width - offset, d.height- offset);
	    g.drawRect(1, 1, d.width - 2 - offset, d.height - 2 - offset);
	    g.drawRect(2, 2, d.width - 4 - offset, d.height - 4 - offset);
	}
	
	protected void paintPort (Graphics g,Dimension d){
	    Map attr = view.getAttributes();
	    //Color backCol = GraphConstants.getBackground(attr);
	    Color foreCol = GraphConstants.getForeground(attr);
	    Color backCol = Color.white;
	    g.setPaintMode();
	    if ( foreCol != null )
		g.setColor(foreCol);
	    else
		g.setColor(getForeground());
	    g.drawRect(0, 0, d.width, d.height);
	    if ( backCol != null ){
		g.setColor(backCol);
		g.fillRect(1, 1, d.width - 1, d.height - 1);
	    }
	}
    }
}
