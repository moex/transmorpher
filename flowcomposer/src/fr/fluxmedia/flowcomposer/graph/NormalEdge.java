/**
 * $Id: NormalEdge.java,v 1.5 2002-12-12 10:25:58 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package fr.fluxmedia.flowcomposer.graph;

import com.jgraph.graph.DefaultEdge;
import com.jgraph.graph.DefaultPort;
import fr.fluxmedia.transmorpher.graph.Channel;
import fr.fluxmedia.transmorpher.graph.Port;


   /**
     * An edge that may connect two vertices via a source and target port.
     *
     * @version 0.2
     * @author Chomat/Saint-Marcel
     */


public class NormalEdge extends DefaultEdge{

    /** Creates an empty <code>NormalEdge</code>.*/
    public NormalEdge(){
        this(null);
    }
    
    /** Constructs an edge.
      * @param userObject the edge's data --> the reference to Transmorpher object Channel
      */
    public NormalEdge(Object userObject){
        super(userObject);
    }

    /** Returns the name of the Channel */
    public String toString(){
	if(userObject instanceof Channel){
	    return ((Channel)userObject).getName();
	}
	return super.toString();
    }		
 		
    /**
     * Sets the source of the edge.
     */
    public void setSource(Object port) {
	source = port;
	if (userObject != null)
	    {
		Port tPort = source == null ? (Port)null : (Port)((DefaultPort)source).getUserObject();
		((Channel)userObject).setIn(tPort);
	    }
    }
    
    /**
     * Returns the target of <code>edge</code>.
     */
    public void setTarget(Object port) {
	target = port;
	if (userObject != null)
	    {
		Port tPort =  target == null ? (Port)null : (Port)((DefaultPort)target).getUserObject();
		((Channel)userObject).setOut(tPort);
	    }
    }

}
