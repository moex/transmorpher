/**
 * $Id: PortProcessCell.java,v 1.3 2002-09-13 17:53:56 serge Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.graph;

import com.jgraph.graph.DefaultGraphCell;

   /**
     * The component PortProcess is represented by the <code>PortProcessCell</code>
     * which is an extension of the <code>DefaultGraphCell</code> class.This class
     * is useful has to distinguish the ports from the objects Transmorpher Call represents
     * by the <code>PortCell</code> of the Ports of the objects Transmorpher Process.
     *
     * @version 0.2
     * @author Chomat/Saint-Marcel
     */

public class PortProcessCell extends DefaultGraphCell {

    /** Creates an empty <code>PortProcessCell</code>.*/
    public PortProcessCell() {
      this(null);
    }

    /**
     * Creates a <code>PortProcessCell</code> and initializes it with the specified user object 
     *
     * @param userObject the cell's data --> the reference to Transmorpher object Port
     *
     */
    public PortProcessCell(Object userObject) {
	super(userObject);
    }

}
