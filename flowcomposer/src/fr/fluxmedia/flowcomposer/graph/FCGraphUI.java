/**
 *
 *$Id: FCGraphUI.java,v 1.6 2002-12-12 10:25:58 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.graph;

import java.io.*;
import java.awt.*;
import java.beans.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.geom.Line2D;
import javax.swing.plaf.*;
import javax.swing.event.*;
import javax.swing.tree.TreeNode;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.undo.UndoableEdit;
import java.util.EventListener;
import java.util.TooManyListenersException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Set;
import java.util.HashSet;
import java.util.Observer;
import java.util.Observable;
import java.util.Collection;
import java.awt.dnd.*;
import java.awt.datatransfer.*;
import java.awt.geom.AffineTransform;
import com.jgraph.JGraph;
import com.jgraph.event.*;
import com.jgraph.graph.*;
import com.jgraph.plaf.GraphUI;
import java.io.Serializable;
import com.jgraph.plaf.basic.BasicGraphUI;

import fr.fluxmedia.transmorpher.graph.Call;

/**
 * The <code>FCGraphUI</code> constitute JGraph�s UI-delegate.Aside from the event
 * listeners to track user input, the UI-delegate is also used to paint the graph,
 * and update the display when the graph or the selection changes.When a
 * graph is painted, the UI-delegate loops through the views and paints them by
 * painting their respective renderers.
 *
 * @version 0.2
 * @author Chomat/Saint-Marcel
 */


public class FCGraphUI extends BasicGraphUI{
    
    /** Constructs an empty graph UI. */
    public FCGraphUI(){
        super();
    }
   
    /** Returns the graph UI*/
    public static ComponentUI createUI(JComponent x) {
        return new FCGraphUI();
    }   
    
    /**
     * Creates the listener responsible for calling the correct handlers
     * based on mouse events, and to select invidual cells.
     */
    protected MouseListener createMouseListener() {
        return new FCMouseHandler();
    }
        
    /**
     * override the Main painting routine.
     */
    public void paint(Graphics g, JComponent c) {
        if (graph != c)
            throw new InternalError("BasicGraphUI cannot paint "+c.toString()+
            "; "+graph+" was expected.");
        Graphics2D       g2 = (Graphics2D) g;
        Rectangle        paintBounds = new Rectangle(g.getClipBounds());
        paintBackground(g);
        // Remember current affine transform
        AffineTransform at = g2.getTransform();
        // Use anti aliasing
        if (graph.isAntiAliased())
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        // Use Swing's scaling
        double scale = graph.getScale();
        g2.scale(scale, scale);
        // Paint cells
        CellView[] views = graphView.getRoots();
        Dimension dim = new Dimension(((ProcessGraph)graph).getWorkX(),((ProcessGraph)graph).getWorkY());
        for (int i = 0; i < views.length; i++) {
            Rectangle bounds = views[i].getBounds();
            Rectangle real = graph.fromScreen(new Rectangle(paintBounds));
            if (!((views[i] instanceof PortProcessView)||(views[i] instanceof FCPortView)||(views[i] instanceof NormalEdgeView))) {

                Rectangle rectView = views[i].getBounds();
                if (rectView.x+rectView.width>dim.width)
                    ((ProcessGraph)graph).resizeWorkSurface(rectView.width,0,true);
                if (rectView.y+rectView.height>dim.height)
                    ((ProcessGraph)graph).resizeWorkSurface(0,rectView.height,true);
                
            }
            if (bounds != null && real != null && bounds.intersects(real))
                paintCell(g, views[i], bounds, false);
        }
        // Reset affine transform and antialias
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        g2.setTransform(at);
    }
    
   /**
     * Override stops the editing session.  If messageStop is true the editor
     * is messaged with stopEditing, if messageCancel is true the
     * editor is messaged with cancelEditing. If messageGraph is true
     * the graphModel is messaged with valueForCellChanged.
     */
    protected void completeEditing(boolean messageStop,
				   boolean messageCancel,
				   boolean messageGraph) {
	
        if(stopEditingInCompleteEditing && editingComponent != null) {
            Component             oldComponent = editingComponent;
            Object             oldCell = editingCell;
            GraphCellEditor       oldEditor = cellEditor;
            Object                newValue = oldEditor.getCellEditorValue();
            Rectangle             editingBounds = graph.getCellBounds(editingCell);
            boolean               requestFocus = (graph != null &&
            (graph.hasFocus() || editingComponent.hasFocus()));
            editingCell = null;
            editingComponent = null;
            if(messageStop)
                oldEditor.stopCellEditing();
            else if(messageCancel)
                oldEditor.cancelCellEditing();
            graph.remove(oldComponent);
            if(requestFocus)
                graph.requestFocus();
            if (messageGraph) {
                Map map = GraphConstants.createMap();
                // change the value of Call ID
                if(oldCell instanceof DefaultGraphCell) {
                    if(newValue instanceof String)
			{
			    boolean idChanged = ((ProcessGraph)graph).changeId(((DefaultGraphCell)oldCell).getUserObject(),(String)newValue);
			}
                }
                Map insert = new Hashtable();
                insert.put(oldCell, map);
                graphModel.edit(null, insert, null, null);
            }
            updateSize();
            // Remove Editor Listener
            if(oldEditor != null && cellEditorListener != null)
                oldEditor.removeCellEditorListener(cellEditorListener);
            cellEditor = null;
        }
    }
    
    
    
    /**
     * TreeMouseListener is responsible for updating the selection
     * based on mouse events.
     */
    public class FCMouseHandler extends BasicGraphUI.MouseHandler {
        /**
         * Invoked when a mouse button has been pressed on a component.
         */
        public void mousePressed(MouseEvent e) {
            
            if(e.isPopupTrigger()){
                if (marquee!= null)
                    marquee.mousePressed(e);
                super.mousePressed(e);
            }
            else
                super.mousePressed(e);
        }
        
        public void mouseDragged(MouseEvent e) {
            try {
                
		autoscroll(graph, e.getPoint());
                
		if (handler == marquee)
                    marquee.mouseDragged(e);
                else if (handler == null && !isEditing(graph) && focus != null) {
                    if (!graph.isCellSelected(focus.getCell())) {
                        selectCellForEvent(focus.getCell(), e);
                        cell = null;
                    }
                    if (handle != null)
                        handle.mousePressed(e);
                    handler = handle;
                }
                if (handle != null && handler == handle)
                    handle.mouseDragged(e);
            
	    } finally {
                
            }
        }
        
        /**
         * Invoked when the mouse pointer has been moved on a component
         * (with no buttons down).
         */
        public void mouseMoved(MouseEvent e) {
            if (graph != null && graph.isEnabled()) {
                if (marquee != null)
                    marquee.mouseMoved(e);
                if (handle != null)
                    handle.mouseMoved(e);
                if (!e.isConsumed())
                    graph.setCursor(Cursor.getDefaultCursor());
            }
        }
        
	

        public void mouseReleased(MouseEvent e) {
            try {
                
                //if(!(((ProcessGraph)e.getSource()).getTopLevelAncestor() instanceof FCOverviewPanel))
                if (!e.isConsumed()) {
                    
                    if(e.isPopupTrigger()) {
                        if (marquee!= null)
                            marquee.mouseReleased(e);
                        super.mouseReleased(e);
                    }
                    else {
                        
                        if (handler == marquee)
                            marquee.mouseReleased(e);
                        else if (handler == handle && handle != null)
                            handle.mouseReleased(e);
                        if (isDescendant(cell, focus) && e.getModifiers() != 0) {
                            // Do not switch to parent if Special Selection
                            cell = focus;
                        }
                        if (!e.isConsumed() && cell != null) {
                            Object tmp = cell.getCell();
                            boolean wasSelected = graph.isCellSelected(tmp);
                            selectCellForEvent(tmp, e);
                            focus = cell;
                            if (wasSelected && graph.isCellSelected(tmp)) {
                                Object root = ((DefaultMutableTreeNode) tmp).getRoot();
                                selectCellForEvent(root, e);
                                focus = graphView.getMapping(root, false);
                            }
                        }
                    }
                }
                
            } finally {
                insertionLocation = null;
                handler = null;
                cell = null;
            }
            
        }
        
        protected boolean isDescendant(CellView parent, CellView child) {
            if (parent != null && parent.getCell() instanceof DefaultMutableTreeNode &&
            child != null && child.getCell() instanceof DefaultMutableTreeNode) {
                DefaultMutableTreeNode p = (DefaultMutableTreeNode) parent.getCell();
                DefaultMutableTreeNode c = (DefaultMutableTreeNode) child.getCell();
                return p.isNodeDescendant(c);
            }
            return false;
        }
        
    } // End of BasicGraphUI.MouseHandler
    
    /**
     * Constructs the "root handle" for <code>context</code>.
     *
     * @param context reference to the context of the current selection.
     */
    public CellHandle createHandle(GraphContext context) {
        if (context != null && !context.isEmpty() && graph.isEnabled())
            return new FCRootHandle(context);
        return null;
    }
    
    /** 
     * The BasicGraphUI class maintains a reference to a RootHandle, which
     * is used to react to mouse events. We overrides it for controlling the
     * moving of cells with the workSurface . The user can't move the cells
     * with (x&lt;0||y&lt;0) and we include the management for updating the size of 
     * workSurface for other mouse events.
     */
     class FCRootHandle extends BasicGraphUI.RootHandle{
        
        /** Constructs a RootHandle
         * @param ctx reference to the context of the current selection.
         *
         */ 
         public FCRootHandle(GraphContext ctx){
            super(ctx);
        }
        
        /** Process mouse dragged event. */
        public void mouseDragged(MouseEvent event) {
            if (event != null && !event.isConsumed()) {
                if (activeHandle != null) // Paint Active Handle
                    activeHandle.mouseDragged(event); // Invoke Mouse Dragged
                else if (start != null) { // Move Cells
                    Graphics g = graph.getGraphics();
                    Point current = graph.toScreen(graph.snap(new Point(event.getPoint())));
                    int thresh = graph.getMinimumMove();
                    int dx = current.x-start.x;
                    int dy = current.y-start.y;
                    if (isMoving || Math.abs(dx) > thresh || Math.abs(dy) > thresh) {
                        isMoving = true;
                        dx = current.x-last.x;
                        dy = current.y-last.y;
                        g.setColor(graph.getBackground());
                        g.setXORMode(graph.getBackground());
                        if (!isDragging) {// Start Drag and Drop
                            startDragging(event);
                            if (graph.isDisconnectOnMove())
                                context.disconnect(graphView.getAllDescendants(views));
                        }
                        if (isConstrainedMoveEvent(event)) { // Constrained movement
                            if (Math.abs(dx) < Math.abs(dy))
                                dx = 0;
                            else
                                dy = 0;
                        }
                        //Point p = graph.fromScreen(graph.snap(new Point(dx, dy))); // Grid
                        if (dx != 0 || dy != 0) {
                            isContextVisible = !event.isControlDown() || !graph.isCloneable();
                            if (!last.equals(start) && !blockPaint)
                                overlay(g);
                            blockPaint = false;
                            Point tmp = graph.fromScreen(new Point(dx, dy));
                            //System.out.println(tmp);
                            if (cachedBounds != null){
                                //System.out.println("cachedbounds");
                                cachedBounds.translate(tmp.x, tmp.y);
                            }else {
                                graph.fromScreen(tmp);
                                boolean moved = true;
                                for (int i = 0; i < views.length; i++) {
                                    Rectangle rectView = views[i].getBounds();
                                    if ((tmp.x<0)&&(rectView.x+tmp.x<0)) moved =false;
                                    if ((tmp.y<0)&&(rectView.y+tmp.y<0)) moved =false;
                                }
                                if (moved) {
                                    //System.out.println(moved);
                                    Dimension dim = new Dimension(((ProcessGraph)graph).getWorkX(),((ProcessGraph)graph).getWorkY());
                                    for (int i = 0; i < views.length; i++) {
                                        Rectangle rectView = views[i].getBounds();
                                        int x = rectView.x;
                                        int y = rectView.y;
                                        Point p = new Point(x,y);
                                        if ((tmp.x>0)&&(p.x+rectView.width+tmp.x>dim.width))
                                            ((ProcessGraph)graph).resizeWorkSurface(tmp.x,0,true);
                                        //((ProcessGraph)graph).updateCellProcess();}
                                        if ((tmp.y>0)&&(p.y+rectView.height+tmp.y>dim.height))
                                            ((ProcessGraph)graph).resizeWorkSurface(0,tmp.y,true);
                                        //((ProcessGraph)graph).updateCellProcess();
                                    }
                                    GraphView.translateViews(views, tmp.x, tmp.y);
                                    if (views != null)
                                        for (int i = 0; i < views.length; i++)
                                            views[i].update();
                                    if (contextViews != null)
                                        for (int i = 0; i < contextViews.length; i++)
                                            contextViews[i].update();
                                    //((ProcessGraph)graph).updateCellProcess();
                                }
                            }
                            
                            
                            if (!event.getPoint().equals(start))
                                overlay(g);
                            last = current; //event.getPoint();
                            
                        }
                    }
                }
            } else if (event == null)
                graph.repaint();
        }
        
        /** Process mouse realeased event. */
        public void mouseReleased(MouseEvent event) {
            if (event != null && !event.isConsumed()) {
                if (activeHandle != null) {
                    activeHandle.mouseReleased(event);
                    activeHandle = null;
                } else if (isMoving&& !event.getPoint().equals(start)) {
                    if (cachedBounds != null) {
                        int dx = event.getX()-start.x;
                        int dy = event.getY()-start.y;
                        Point tmp = graph.fromScreen(new Point(dx, dy));
                        GraphView.translateViews(views, tmp.x, tmp.y);
                    }
                    CellView[] all = graphView.getAllDescendants(views);
                    if (event.isControlDown() && graph.isCloneable()) { // Clone Cells
                        Object[] cells = graph.getDescendants(context.getCells());
                        ConnectionSet cs = ConnectionSet.create(graphModel, cells, false);
                        cs.addConnections(all);
                        Map propertyMap = GraphConstants.createPropertyMap(all, null);
                        insertCells(context.getCells(), propertyMap, cs, true, 0, 0);
                    } else if (graph.isMoveable()){ // Move Cells
                        if (!graphModel.isAttributeStore()) {
                            Map attributeMap = GraphConstants.createAttributeMap(all, graphView);
                            GraphView.GraphViewEdit edit = graphView.createEdit(attributeMap);
                            graphModel.edit(null, null, null, new UndoableEdit[]{edit});
                            edit.execute();
                            if (!(((ProcessGraph)graph).isMain()))
                                ((ProcessGraph)graph).updateCellProcess();
                            } else {
                            Map propertyMap = GraphConstants.createPropertyMap(all, null);
                            graphModel.edit(null, propertyMap, null, null);
                        }
                    }
                    event.consume();
                }
            }
            start = null;
        }
        
    }
    
 }



