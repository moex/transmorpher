/**
 * $Id: NormalEdgeView.java,v 1.7 2002-12-12 10:25:58 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.graph;

import com.jgraph.graph.EdgeView;
import com.jgraph.graph.DefaultGraphCell;
import com.jgraph.graph.EdgeRenderer;
import com.jgraph.graph.CellMapper;
import com.jgraph.JGraph;
import com.jgraph.graph.PortView;
import com.jgraph.graph.Port;
import com.jgraph.graph.GraphContext;
import com.jgraph.graph.Edge;
import com.jgraph.graph.CellHandle;
import com.jgraph.graph.DefaultPort;
import com.jgraph.graph.CellViewRenderer;
import com.jgraph.graph.*;

import fr.fluxmedia.transmorpher.graph.Channel;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.undo.*;

   /**
     * The <code>NormalEdgeView</code> is needed to define the special visual
     * aspects of a <code>NormalEdge</code>.It contains two inner class including one 
     * serves as a renderer that provides the painting code.The method that need
     * to be overriden is getRenderer to return the correct renderer.The other serves
     * as a edge Handle <code>FCEdgeHandle</code>.  
     *
     * @version 0.2
     * @author Chomat/Saint-Marcel
     */


public class NormalEdgeView extends EdgeView{

    /** Renderer for this view. */
    protected CellViewRenderer renderer = new NormalEdgeRenderer();

    /** Constructs the view.
     * @param cell the NormalEdge Object
     * @param graph it's owner graph
     * @param mapper the mapping from cell to view
     */
    public NormalEdgeView(Object cell, JGraph graph, CellMapper mapper){
        super(cell,graph,mapper);
    }

    
     /** Returns a edge handle for the view.*/ 
    public CellHandle getHandle(GraphContext context) {
      return new FCEdgeHandle(this, context);
    }
   
    /** Returns the renderer for the view. */
    public CellViewRenderer getRenderer() {
      return renderer;
    }
  
    /**
     * Change the renderer of this view.
     *
     * @param renderer a <code>CellViewRenderer</code>. if <code>renderer</code> is null, 
     * this method create a default renderer.
     */
    
    public void setRenderer(CellViewRenderer renderer){
	this.renderer = renderer;
	if (this.renderer == null) {
	    this.renderer = new NormalEdgeRenderer();
	}
    }
    
    /**
     * Sets the <code>sourceView</code> of the edge.
     */
    public void setSource(PortView sourceView) {
	fr.fluxmedia.transmorpher.graph.Port oldPort = null;
	if(source != null)
	    {	
		DefaultGraphCell cell = (DefaultGraphCell)source.getCell();
		oldPort = (fr.fluxmedia.transmorpher.graph.Port)cell.getUserObject();
		
	    }
	else if(oldPort != null)
	    oldPort.setChannel(null);
	    
	source = sourceView;
	if (source != null)
	    points.set(0, source);
	else
	    points.set(0, getPoint(0));
    }
    
    /**
     * Sets the <code>targetView</code> of the edge.
     */
    public void setTarget(PortView targetView) {
	fr.fluxmedia.transmorpher.graph.Port oldPort = null;
	if(target != null)
	    {	
		DefaultGraphCell cell = (DefaultGraphCell)target.getCell();
		oldPort = (fr.fluxmedia.transmorpher.graph.Port)cell.getUserObject();
	    }
	else if(oldPort != null)
	    oldPort.setChannel(null);
	
	target = targetView;
	int n = points.size()-1;
	if (target != null)
	    points.set(n, target);
	else
	    points.set(n, getPoint(n));
    }
    
    /**
     * The EdgeHandle allows to connect and disconnect edges, and to add, modify or
     * remove individual points.  
     */
    public class FCEdgeHandle extends EdgeView.EdgeHandle{
	
        /** Constructs the view.
          * @param edge the view
          * @param ctx The term context is used to describe the set of cells that are indirectly
          * modified by a change, but are not themselves part of the selection.The
          * GraphContext object is used to compute and manage this set.
          */
	public FCEdgeHandle(EdgeView edge, GraphContext ctx) {
	    super(edge,ctx);
	}

	// Handle mouse released event
	public void mouseReleased(MouseEvent e) {
	    CellView[] views = new CellView[]{edge};
	    ConnectionSet cs = new ConnectionSet();
	    if (edge.getCell() instanceof Edge) {
		Edge realEdge = (Edge) edge.getCell();
		
		Port sourcePort = null;
		Port targetPort = null;
          
		if (edge.getSource() == null && edge.getTarget() == null) {
		    
		    ((ProcessGraph)graph).delete(edge.getCell());
		    e.consume();
		    return;
		}

		if (edge.getSource() != null)
		    sourcePort = (Port) edge.getSource().getCell();
		if (edge.getTarget() != null)
		    targetPort = (Port) edge.getTarget().getCell();
		if (sourcePort != getModel().getSource(realEdge))
		    cs.connect(realEdge, sourcePort, true);
		if (targetPort != getModel().getTarget(realEdge))
		    cs.connect(realEdge, targetPort, false);
		
	    }
	    // Create Compound Model Edit
	    if (!getModel().isAttributeStore()) {
		Map attributeMap = GraphConstants.createAttributeMap(views, graph.getView());
		GraphView.GraphViewEdit edit = graph.getView().createEdit(attributeMap);
		getModel().edit(cs, null, null, new UndoableEdit[]{edit});
		edit.execute();
	    } else {
		Map propertyMap = GraphConstants.createPropertyMap(views, null);
		getModel().edit(cs, propertyMap, null, null);
	    }
	    e.consume();
	}
    
        public void updateSourceChannel(Edge channel,Port port){

	if(channel instanceof DefaultGraphCell && port instanceof DefaultGraphCell)
	    {
		Channel tChannel = (Channel)((DefaultGraphCell)channel).getUserObject();
		DefaultPort  portSource = (DefaultPort)channel.getSource();
		fr.fluxmedia.transmorpher.graph.Port tPort = (fr.fluxmedia.transmorpher.graph.Port)((DefaultGraphCell)port).getUserObject();
		tChannel.setIn(tPort);
	    }
        }
    
        /**
         *
         */
        protected void updateTargetChannel(Edge channel,Port port){
            if(channel instanceof DefaultGraphCell && port instanceof DefaultGraphCell){
		Channel tChannel = (Channel)((DefaultGraphCell)channel).getUserObject();
		DefaultPort  portTarget = (DefaultPort)channel.getTarget();
		fr.fluxmedia.transmorpher.graph.Port tPort = (fr.fluxmedia.transmorpher.graph.Port)((DefaultGraphCell)port).getUserObject();
		tChannel.setOut(tPort);
	    }
	}
    }
        /**
          * the renderer itself is an instance of the JComponent class, with an overriden
          * paint method that paints a cell based on its attributes
          */
     public static class NormalEdgeRenderer extends EdgeRenderer{

            public NormalEdgeRenderer(){
                super();
            }
        }
}
