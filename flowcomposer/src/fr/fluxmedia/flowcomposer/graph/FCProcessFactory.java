/**
 * $Id: FCProcessFactory.java,v 1.10 2002-12-16 08:47:23 triolet Exp $
 *
 * Transmorpher
 *
 * Copyright (C) 2002 Fluxmedia.
 *
 * http://www.fluxmedia.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package fr.fluxmedia.flowcomposer.graph;

import java.util.Iterator;
import java.io.*;
import java.util.*;
import com.jgraph.graph.*;

//import Transmorpher
import fr.fluxmedia.transmorpher.graph.Process;
import fr.fluxmedia.transmorpher.graph.*;
import fr.fluxmedia.transmorpher.utils.*;
import fr.fluxmedia.transmorpher.parser.ProcessParser;
import fr.fluxmedia.transmorpher.action.*;

import fr.fluxmedia.flowcomposer.gui.FlowComposer;
import fr.fluxmedia.flowcomposer.utils.*;


/**
 * The implementation of a <code>ProcessGraph</code> manager that defines methods for
 * adding, removing and changing process.It implements the Observer interface
 * which listen the Transmorpher graph and the interface Serializable for saving
 * the state current of a graph. 
 *
 * @version 1.0 29/01/01
 * @author Chomat/Saint-Marcel
 */

public class FCProcessFactory implements Observer,Serializable{

    /** The list of all process create with this factory. */
    protected ArrayList listProcess ;

    /** The MainProcess*/
    protected ProcessGraph mainProcess = null;

    /** Current Process */
    protected ProcessGraph currentProcess = null;

    /** Transmorpher instance*/
    protected Transmorpher transmorpher = null;
    
    /** instance of <code>FlowComposer</code> */
    protected FlowComposer flowcomposer;
    
    // boolean for the implementation of creation
    boolean create = true;
    
    /**
     * Constructs an empty manager (with no process) with the transmorpher instance
     * associate.
     *
     * @param flowcomposer instance of <code>FlowComposer</code>
     * @param transmorpher instance of Transmorpher
     */
    public FCProcessFactory(FlowComposer flowcomposer, boolean createTransmorpher){
        this.flowcomposer = flowcomposer;
	
	if ( createTransmorpher == true ) {
	    transmorpher = new Transmorpher("transmorpher",flowcomposer.getResource().getString("TRANSMORPHER_VERSION"),3,false);
	}

        mainProcess = null;
        listProcess = new ArrayList();
	
	if (transmorpher != null) {
	    transmorpher.addObserver(this);
	}
    }
    
   /**
     * Get the value of transmorpher.
     *
     * @return value of transmorpher.
     */
    public Transmorpher getTransmorpher() {return transmorpher;}
    
   /**
     * Set the value of transmorpher.
     *
     * @param v  Value to assign to transmorpher.
     */
    public void setTransmorpher(Transmorpher  v) {this.transmorpher = v;}


     /**
       * Get the value of currentProcess.
       *
       * @return value of currentProcess.
       */
    public ProcessGraph getCurrentProcess() {return currentProcess;}
    
     /**
       * Set the value of currentProcess.
       *
       * @param v  Value to assign to currentProcess.
       */
    public void setCurrentProcess(ProcessGraph  v) {this.currentProcess = v;}
    
     /**
       * Set the value of currentProcess.
       *
       * @param name  The process Name.
       */
    public void setCurrentProcess(String  name) 
    {
	this.setCurrentProcess(this.getProcess(name));
    }
    
    /**
     * Run the transformation.
     *
     */
    
    public void runTransmorpher(){
	SystemResources tResources = new SystemResources();
	//String reloc  = "";
	if (transmorpher != null) {
	    transmorpher.setDebug(3);
	    try{
	    transmorpher.generateExec(null);
	    }
	    catch(Exception e){
		System.out.println(e);
	    }

	    Parameters p = new Parameters();
	    try{
		transmorpher.exec(p);
	    }
	    catch(Exception e){
		System.out.println(e);
	    }
	} // end of if (transmorpher != null)
    }
    
    /**
     * The compile methode generate the java code corresponding to the Transmorpher.
     *
     */

    public void compileTransmorpher(){
	if (transmorpher != null) {     		
	    transmorpher.setDebug(3);
	    try{
		transmorpher.generateJavaCode(null);
	    }
	    catch(Exception e){
		System.out.println(e);
	    }
	}
    }


    /**
     * Create a new process in Transmorpher Graph.
     * 
     * @param type - Type of this process. This type can be PROCESS, MAIN, SERVLET, TRANSFORMER.
     *
     * @return Transformation created
     */
    
    public Process newProcess(int type){

        Process process = null;
	
	if (transmorpher != null) {
	 
	    switch(type)
		{
		case FCConstants.MAIN :
		    if ( transmorpher.getMain() != null)
			{
			    // traitement erreur
			}
		    else
			{
			    process = new fr.fluxmedia.transmorpher.graph.Main(FCConstants.DEFAULT_MAIN_NAME,transmorpher);
			    transmorpher.setMain((MainProcess)process);
			    transmorpher.addTransformation(FCConstants.DEFAULT_MAIN_NAME,process);
			}
		    break;
		case FCConstants.PROCESS:
		    
		    String name = getDefaultProcessName(0);
		    process = new fr.fluxmedia.transmorpher.graph.Process(name,transmorpher);
		    transmorpher.addTransformation(name,process);
		    
		    break;
		}
	    
	} // end of if (transmorpher != null))
	
	return process;
    }

    /** Remove a process in Transmorpher */
    
    public void removeCurrentProcess(){
	if (transmorpher != null) {
	    transmorpher.removeTransformation((Process)currentProcess.getUserObject());
	} 	
    }
    
    /**
     * Return a default name for a process
     *
     */

    private String getDefaultProcessName(int count){
	
	String defaultName = "Process"+count;
	ProcessGraph process ;
	Iterator it = listProcess.iterator() ;
	
	while (it.hasNext()){

	    process = (ProcessGraph)it.next();
	    
	    if (defaultName.equals(process.getName())){
		return getDefaultProcessName(count+1);
	    }
	}
	return defaultName ;
    }

    

    /**
     * Return true if the process is a main Process.
     *
     * @return true if the process is a main Process.
     */

    public boolean isMain(Object process){
      
	if (process != null) {
	    return process.equals(mainProcess);
	}
	return false;
    } 

    
    /**
     * Adding a process to the graph
     * 
     * @param userObject the reference to Transmorpher object
     */
    public ProcessGraph createProcess(Object userObject){
        ProcessGraph  process = null;
        GraphModel model = null;
        
        if(userObject instanceof MainProcess){
	    
	    if(mainProcess != null){
			flowcomposer.updateTab(((MainProcess)mainProcess.getUserObject()).getName(),((MainProcess)userObject).getName());
			mainProcess.setUserObject(userObject);
			process = mainProcess;
			create = false;}
		else {
                        model = new ProcessGraphModel();
			process = new ProcessGraph(model,null,userObject);
			mainProcess = process;
			listProcess.add(process);}            
            }
            else{
		model = new ProcessGraphModel();
		process = new ProcessGraph(model,null,userObject);
		if(userObject instanceof Process){
		    for(int i =0; i < ((Process)userObject).inPorts().length();i++){
			process.update((Process)userObject,new TmActionImpl(TmActionImpl.ADD,userObject,((Process)userObject).inPorts().getPort(i),null,null));}
		    for(int j =0; j < ((Process)userObject).outPorts().length();j++){
			process.update((Process)userObject,new TmActionImpl(TmActionImpl.ADD,userObject,((Process)userObject).outPorts().getPort(j),null,null));}
		}
		create = true;
		listProcess.add(process);
            }
	((Process)userObject).addObserver(process);
	
	return process;
    }
    
    /**
     * Remove the ProcessGraph in the list of Process
     */

    public ProcessGraph removeProcessGraph(Object userObject){

	ProcessGraph process = getProcessGraph(userObject);
	listProcess.remove(listProcess.lastIndexOf(process));
	return process ;
    }

    /**
     * Create all the channels of transmorpher's component for all process. It makes
     * after parsing the file XML.
     */
    public void createAllChannels(){
        Iterator it = listProcess.iterator();
        while(it.hasNext())
            createChannel((Process)((ProcessGraph)it.next()).getUserObject());
    }

    /**
     * Create all the channel of transmorpher's component for a process
     */
    public void createChannel(Process transformation){
        ProcessGraph processGraph = getProcessGraph(transformation);
        Enumeration channels = transformation.getChannels();
        while(channels.hasMoreElements())
            processGraph.update(transformation,new TmActionImpl(TmActionImpl.ADD,transformation,channels.nextElement(),null,null));
    }

    /** Update the view of <code>PortProcessCell</code> for all process*/
    public void updateCellProcess(){
        Iterator it = listProcess.iterator();
        while(it.hasNext()){
            ProcessGraph process = (ProcessGraph)it.next();
            if(!(process.getUserObject() instanceof MainProcess) && (process.getUserObject() instanceof Process))
                process.updateCellProcess();
        }
   }
   /** Basic layout algorithm on all process if there is no chart graphical with file RDF*/
    public void layout() {
       Iterator it = listProcess.iterator();
       while(it.hasNext()){
           ProcessGraph process = (ProcessGraph)it.next();
           if (!(process.getUserObject() instanceof MainProcess)) {
             process.basicProcessLayout(20,20);}
           else{process.basicComponentLayout(20,20,0,20,20,null);}
       }
    }

    /** Returns the graph identify by this userObject --> Object Transmorpher Process*/
    public ProcessGraph getProcessGraph(Object userObject){
        Iterator it = listProcess.iterator();
        while(it.hasNext()){
            Object process = it.next();
            if(process instanceof ProcessGraph && ((ProcessGraph)process).getUserObject().equals(userObject))
                return (ProcessGraph)process;
        }
        return null;
    }

    
     
    /** not yet implemented*/
    public boolean validateProcess(Object process){
       return true;
    }

    /** Return true if the transmorpher has already been edited.*/
    public boolean isEditing(){
    
	return (mainProcess != null  &&  ( mainProcess.getModel().getRootCount() != 0 || listProcess.size() > 1));
    }


    /**
     * Returns the graph identify by its name : a string representation 
     */
    public ProcessGraph getProcess(String index){
        Iterator it = getAllProcess().iterator();
        String processName;
        while (it.hasNext()) {
            Object process = it.next();
            processName = ((Process)(((ProcessGraph)process).getUserObject())).getName();
            if(index.equals(processName)){
                return (ProcessGraph)process;
            }
        }
        return null;
    }

    
    /** Returns the name list of all process except the main process name and the current*/
    public Object[] getProcessList(){
        ArrayList  processList = new ArrayList();

        for (Enumeration e = transmorpher.getTransformations().getKeys() ; e.hasMoreElements() ;) {
            Object key = e.nextElement();
	    
            if (!(transmorpher.getTransformation((String)key) instanceof MainProcess) 
		&& !(transmorpher.getTransformation((String)key) instanceof Query)
		&& !(transmorpher.getTransformation((String)key) instanceof Ruleset)
		&& (!(transmorpher.getTransformation((String)key).equals(currentProcess.getUserObject())))){
		
                processList.add(key);
		
            } // end of if (transmorpher.getTransformations().get(key) instanceof MainProcess)
        }
	
        return processList.toArray();
    }

    /** return the list of all query define in the transmorpher */

    public Object[] getQueryList(){
	
	ArrayList  processList = new ArrayList();

        for (Enumeration e = transmorpher.getTransformations().getKeys() ; e.hasMoreElements() ;) {
            Object key = e.nextElement();
	    if (transmorpher.getTransformation((String)key) instanceof Query)
		processList.add(key);
	}  
	return processList.toArray();
    }
 
    /** return the list of all query define in the transmorpher */
    
    public Object[] getRulesetList(){
	
	ArrayList  processList = new ArrayList();

        for (Enumeration e = transmorpher.getTransformations().getKeys() ; e.hasMoreElements() ;) {
            Object key = e.nextElement();
	    if (transmorpher.getTransformation((String)key) instanceof Ruleset)
		processList.add(key);
	}  
	return processList.toArray();
    }
    
    /** Returns a List of all process*/
    public List getAllProcess(){
        return listProcess;
    }

    /** Generate the chart graphical file for all the process*/
    public void generateFileRDF(String filename) throws java.io.IOException{
	
	fr.fluxmedia.transmorpher.utils.Writer file = new fr.fluxmedia.transmorpher.utils.Writer(filename);

	file.writeln("<?xml version=\"1.0\" standalone=\"yes\"?>");
	file.writeln("<rdf:RDF");
	file.writeln(4,"xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns\"");
	file.writeln(4,"xmlns:s=\"http://www.w3.org/2000/01/rdf-schema\">");
	file.writeln(6,"<rdf:Description about=\""+transmorpher.getName()+"\">");
	
	Iterator it = getAllProcess().iterator();
        while (it.hasNext()) {
            Object process = it.next();
            ((ProcessGraph)process).generateRDF(file);
        }
	
	file.writeln(6,"</rdf:Description>");
	file.writeln(4,"</rdf:RDF>");
	file.close();
      
    }

    /** 
      *  Returns a serializable data (Vector) with all process and their attributes. 
      *  It contains a current state of graph.
      */
    public Serializable getArchiveableState() {
        Vector v = new Vector();
        Iterator it = getAllProcess().iterator();
        while (it.hasNext()) {
            Object process = it.next();
            v.add(((ProcessGraph)process).getArchiveableState());
        }
        return v;
    }
    
    /**
     * Implementation of interface Observer for the operations on process like
     * adding, removing and modify. 
     */
    public void update(Observable observable, Object arg){
	ProcessGraph process ;
        if(observable instanceof Transmorpher){
		if(arg instanceof TmAction){
			TmAction action = (TmAction)arg;
                        if(action.getTarget() instanceof Process){
				switch(action.getType()){
				    case TmAction.ADD :
                                        
					process = createProcess(action.getTarget());
					
					if(create)
					    flowcomposer.createProcess(process);
                                        break;
				    case TmAction.REMOVE :
				
					process = removeProcessGraph(action.getTarget());
					flowcomposer.removeTabProcess(process) ;
										
					break;
				    case TmAction.MODIFY :
					break;

				    }
			    }

		    }
	    }

    }

}
