<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->

<!-- This stylesheet defines the HTML formats for pages and reference.
     It is called by the other form-hXXXX.xsl stylesheet for adding the 
     grouping information by the main sorting key -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers"
  xmlns:date="http://www.jclark.com/xt/java/java.util.Date"
>

<xsl:template match="/">
<!-- All this stuff must be outsourced depending on the application -->

<html>
	<head>
		<title>Exmo bibliography selection</title>
		<base href="http://www.inrialpes.fr/exmo/papers/" />
	</head>
	<body bgcolor="ffffff" base="http://www.inrialpes.exmo/papers/">
	<form action="http://barbara.inrialpes.fr/servlets/BiblioServlet" method="post">
		<input type="hidden" name="file" value="bibexmo.xml" />
		<input type="hidden" name="abstrip" value="false" />
		<table width="100%">
		<tr border="0">
		<td bgcolor="#99ccff" width="25%">
			<xsl:text> in </xsl:text><select name="format">
				<option selected="true" value="html">HTML</option>
				<option value="bib">BibTeX</option>
				<option value="xml">XML</option>
			</select>
		</td>
		<td bgcolor="#00ffcc" width="50%">
			<xsl:text> sorted by </xsl:text><select name="sort">
				<option selected="true" value="ay">authors, years</option>
				<option value="ty">areas, year</option>
				<option value="ya">year, authors</option>
				<option value="cay">type, year, author</option>
			</select>
		</td>
		<td bgcolor="#cc00cc" width="25%">
			<input type="submit" value="OK" />
		</td>
		</tr>
		</table>
	</form>
		<xsl:apply-templates/>
	<hr />
	<table width="100%">
	<tr border="1"><td>
		<small>
		<a href="..">EXMO</a> :
		<a href="../research">research</a> | 
		<a href="../people">people</a> | 
		<a href="../papers">papers</a> | 
		<a href="../teaching">teaching</a> | 
		<a href="../training">training</a> | 
		<a href="../cooperation">cooperation</a> | 
		<a href="../software">software</a> | 
		<a href="../applications">applications</a> | 
		<a href="../transfert">transfert</a>
		</small>
	</td><td align="right">
		<small><a href="../notice.html" alt=" [legal notice] ">�</a> |
			<a href="../search.html" alt=" [search] ">?</a> |
			<a href="../gloss.html" alt=" [glossary] ">*</a>
		</small>
	</td></tr></table>
	<hr />
	<table width="100%"><tr><td>
		<small>http://www.inrialpes.fr/exmo/papers/</small>
	</td><td align="right">
		<small>
		<a href="../notice.html">� INRIA Rh�ne-Alpes, 1999-2000</a>
		</small>
	</td></tr></table>
	<address>Maintained by <a href="mailto:Jerome.Euzenat@inrialpes.fr">J�r�me Euzenat</a>
	<xsl:if test="function-available('date:to-string') and function-available('date:new')">
		Last generated: <xsl:value-of select="date:to-string(date:new())"/>
	</xsl:if>
 
	</address>
	</body>
</html>
</xsl:template>

<xsl:template match="reference">
	<dt>
	<br />
	<hr />
	<xsl:choose><xsl:when test="authors">
		<xsl:for-each select="authors/p">
			<xsl:apply-templates select="."/><xsl:text>, </xsl:text>
		</xsl:for-each>
	</xsl:when>
	<xsl:when test="editors"> <!-- build named templates for that -->
		<xsl:apply-templates select="editors/p[1]"/>
		<xsl:for-each select="editors/p[position()>1]">
			<xsl:text>, </xsl:text>
			<xsl:apply-templates select="."/>
		</xsl:for-each>
		<xsl:choose><xsl:when test="@language='fr'"> (�d</xsl:when>
			<xsl:when test="@language='de'"> (Hrsg</xsl:when>
			<xsl:otherwise> (ed</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="editors/p[2]">s</xsl:if>
		<xsl:text>.), </xsl:text>
	</xsl:when>
	</xsl:choose>
	<b><xsl:value-of select="title/text()"/></b><xsl:text>, </xsl:text>
	<xsl:choose>
		<xsl:when test="@type='article'">
		<i><xsl:value-of select="journal/@name"/></i>
			<xsl:if test="volume"><xsl:value-of select="volume/text()"/></xsl:if>
			<xsl:if test="number">(<xsl:value-of select="number/text()"/>)</xsl:if>
			<xsl:if test="pages">:<xsl:value-of select="pages/text()"/></xsl:if>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="year/text()"/>
		</xsl:when>
		<xsl:when test="@type='unpublished'">
		Unpublished <xsl:if test="year"><xsl:text>, </xsl:text><xsl:value-of select="year/text()"/></xsl:if>
		</xsl:when>
		<xsl:when test="@type='techreport'">
		<xsl:choose>
			<xsl:when test="type"><xsl:value-of select="type/text()"/></xsl:when>
			<xsl:otherwise>Technical report</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="number"><xsl:text> </xsl:text><xsl:value-of select="number/text()"/></xsl:if>
		<xsl:text>, </xsl:text>
			<xsl:if test="institution">
				<xsl:value-of select="institution/@name"/><xsl:text>, </xsl:text>
				<xsl:if test="institution/location">
					<xsl:apply-templates select="institution/location"/><xsl:text>, </xsl:text>
				</xsl:if>
			</xsl:if>
			<xsl:if test="month"><xsl:value-of select="month/text()"/><xsl:text> </xsl:text></xsl:if>
			<xsl:value-of select="year/text()"/>
		</xsl:when>
		<xsl:when test="@type='masterthesis'">
		<xsl:choose>
			<xsl:when test="type"><xsl:value-of select="type/text()"/></xsl:when>
			<xsl:otherwise>Master thesis</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="number"><xsl:text> </xsl:text><xsl:value-of select="number/text()"/></xsl:if>
		<xsl:text>, </xsl:text>
			<xsl:if test="school">
				<xsl:value-of select="school/@name"/><xsl:text>, </xsl:text>
				<xsl:if test="school/location">
					<xsl:apply-templates select="school/location"/><xsl:text>, </xsl:text>
				</xsl:if>
			</xsl:if>
			<xsl:if test="month"><xsl:value-of select="month/text()"/><xsl:text> </xsl:text></xsl:if>
			<xsl:value-of select="year/text()"/>
		</xsl:when>
		<xsl:when test="@type='phdthesis'">
		<xsl:choose>
			<xsl:when test="type"><xsl:value-of select="type/text()"/></xsl:when>
			<xsl:otherwise>PhD thesis</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="number"><xsl:text> </xsl:text><xsl:value-of select="number/text()"/></xsl:if>
		<xsl:text>, </xsl:text>
			<xsl:if test="school">
				<xsl:value-of select="school/@name"/><xsl:text>, </xsl:text>
				<xsl:if test="school/location">
					<xsl:apply-templates select="school/location"/><xsl:text>, </xsl:text>
				</xsl:if>
			</xsl:if>
			<xsl:if test="month"><xsl:value-of select="month/text()"/><xsl:text> </xsl:text></xsl:if>
			<xsl:value-of select="year/text()"/>
		</xsl:when>
		<xsl:when test="@type='proceedings'"> <!-- OR confernce -->
			<xsl:choose>
				<xsl:when test="publisher">
					<xsl:value-of select="publisher/@name"/><xsl:text>, </xsl:text>
					<xsl:if test="publisher/location">
						<xsl:apply-templates select="publisher/location"/><xsl:text>, </xsl:text>
					</xsl:if>
				</xsl:when>
				<xsl:when test="organization">
					<xsl:value-of select="organisation/@name"/><xsl:text>, </xsl:text>
					<xsl:if test="organisation/location">
						<xsl:apply-templates select="organisation/location"/><xsl:text>, </xsl:text>
					</xsl:if>
				</xsl:when>
			</xsl:choose>
			<xsl:value-of select="year/text()"/>
		</xsl:when>
		<xsl:when test="@type='manual'">
		<xsl:choose>
			<xsl:when test="type"><xsl:value-of select="type/text()"/></xsl:when>
			<xsl:otherwise>Manual</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="edition"><xsl:text>(</xsl:text><xsl:value-of select="edition"/><xsl:choose><xsl:when test="@language='fr'">�d</xsl:when>
			<xsl:otherwise>ed</xsl:otherwise></xsl:choose><xsl:text>.), </xsl:text></xsl:if>
		<xsl:if test="organization">
			<xsl:value-of select="organization/@name"/><xsl:text>, </xsl:text>
			<xsl:if test="organisation/location">
				<xsl:apply-templates select="organization/location"/><xsl:text>, </xsl:text>
			</xsl:if>
		</xsl:if>
		<xsl:if test="month"><xsl:value-of select="month/text()"/><xsl:text> </xsl:text></xsl:if>
		<xsl:value-of select="year/text()"/>
		</xsl:when>
		<xsl:when test="@type='inproceedings'">
			<xsl:choose>
			<xsl:when test="@language='fr'"><xsl:text>Actes </xsl:text></xsl:when>
			<xsl:otherwise><xsl:text>Proc. </xsl:text></xsl:otherwise>
			</xsl:choose>
			<xsl:if test="conference/@issue">
				<xsl:value-of select="conference/@issue"/>
				<xsl:choose>
				<xsl:when test="@language='fr'"><xsl:text>es </xsl:text></xsl:when>
				<xsl:otherwise><xsl:text>th </xsl:text></xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:if test="conference/@sponsor">
				<xsl:value-of select="conference/@sponsor"/>
				<xsl:text> </xsl:text>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="conference/@type"><xsl:value-of select="conference/@type"/></xsl:when>
				<xsl:otherwise><xsl:text>conference on</xsl:text></xsl:otherwise>
			</xsl:choose>
			<xsl:text> </xsl:text>
			<xsl:value-of select="conference/@name"/>
			<xsl:text>, </xsl:text>
			<xsl:if test="conference/location">
				<xsl:apply-templates select="conference/location"/><xsl:text>, </xsl:text>
			</xsl:if>

			<xsl:if test="pages">
				<xsl:text>pp</xsl:text>
				<xsl:value-of select="pages/text()"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<xsl:value-of select="year/text()"/>
		</xsl:when>
		<xsl:when test="@type='incollection'">
			<xsl:text>In: </xsl:text>
			<xsl:if test="book">
				<xsl:apply-templates select="book/reference/editors/p[1]"/>
				<xsl:for-each select="book/reference/editors/p[position()>1]">
					<xsl:text>, </xsl:text>
					<xsl:apply-templates select="."/>
				</xsl:for-each>
				<xsl:choose><xsl:when test="book/reference/@language='fr'"> (�d</xsl:when>
			<xsl:when test="book/reference/@language='de'"> (Hrsg</xsl:when>
			<xsl:otherwise> (ed</xsl:otherwise></xsl:choose><xsl:if test="book/reference/editors/p[2]">s</xsl:if><xsl:text>.), </xsl:text>
				<xsl:value-of select="book/reference/title/text()"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="book/reference/publisher/@name"/>
				<xsl:text>, </xsl:text>
				<xsl:if test="book/reference/publisher/location">
					<xsl:apply-templates select="book/reference/publisher/location"/><xsl:text>, </xsl:text>
				</xsl:if>
			</xsl:if>
			<xsl:if test="pages">
				<xsl:text>pp</xsl:text>
				<xsl:value-of select="pages/text()"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<xsl:value-of select="year/text()"/>
		</xsl:when>
		<xsl:when test="@type='inbook'">
			<xsl:text>In: </xsl:text>
			<xsl:if test="chapter">
				<xsl:text>Chapter </xsl:text>
				<xsl:value-of select="chapter/@number"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="chapter/text()"/>
				<xsl:text> of </xsl:text>
			</xsl:if>
			<xsl:if test="book">
				<xsl:value-of select="book/reference/title/text()"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="book/reference/publisher/@name"/>
				<xsl:text>, </xsl:text>
				<xsl:if test="book/reference/publisher/location">
					<xsl:apply-templates select="book/reference/publisher/location"/><xsl:text>, </xsl:text>
				</xsl:if>
			</xsl:if>
			<xsl:if test="pages">
				<xsl:text>pp</xsl:text>
				<xsl:value-of select="pages/text()"/>
				<xsl:text>, </xsl:text>
			</xsl:if>
			<xsl:value-of select="year/text()"/>
		</xsl:when>
		<xsl:when test="@type='booklet'">
		</xsl:when>
		<xsl:when test="@type='book'">
		</xsl:when>
		<xsl:when test="@type='misc'">
		</xsl:when>
	</xsl:choose>
	<br />
	<a href="http://barbara.inrialpes.fr/servlets/BibrefServlet?format=bib&amp;file=bibexmo.xml&amp;abstrip=false&amp;ref={@index}"><font size="-1">BibTeX</font></a><br />
	<xsl:apply-templates select="links"/>
	</dt>
	<dd>
	<xsl:if test="abstract">
		<i><xsl:value-of select="abstract/text()"/></i><br />
	</xsl:if>
	<xsl:if test="keywords">
		<font size="-1">
		<xsl:apply-templates select="keywords/li[1]"/>
		<xsl:for-each select="keywords/li[position()>1]">
			<xsl:text>, </xsl:text><xsl:apply-templates select="."/>
		</xsl:for-each>
		</font><br />
	</xsl:if>
	<xsl:if test="areas">
		<xsl:apply-templates select="areas/li[1]"/>
		<xsl:for-each select="areas/li[position()>1]">
			<xsl:text>, </xsl:text><xsl:apply-templates select="."/>
		</xsl:for-each>
	</xsl:if>
	</dd>
</xsl:template>

<xsl:template match="links">
        <font size="-1">
		<xsl:apply-templates/>
        </font>
</xsl:template>

<xsl:template match="url">
	<xsl:element name="a">
		<xsl:attribute name="href">
			<xsl:value-of select='@href'/>
		</xsl:attribute>
		<tt><xsl:value-of select='@href'/></tt>
	</xsl:element><br />
</xsl:template>

<xsl:template match="location">
	<xsl:if test="@place"><xsl:value-of select="@place"/><xsl:text>, </xsl:text></xsl:if>
	<xsl:if test="@city"><xsl:value-of select="@city"/></xsl:if>
	<xsl:if test="@country"><xsl:text> (</xsl:text>
		<xsl:if test="@state"><xsl:value-of select="@state"/><xsl:text> </xsl:text></xsl:if>
		<xsl:value-of select="@country"/>)
	</xsl:if>
</xsl:template>

<xsl:template match="p">
	<xsl:value-of select="@first"/><xsl:text> </xsl:text> <xsl:value-of select="@last"/>
</xsl:template>

<xsl:template match="li">
	<xsl:value-of select="text()"/>
</xsl:template>

<xsl:template match="*|@*|text()">
	<xsl:copy>
		<xsl:apply-templates select="*|@*|text()"/>
	</xsl:copy>
</xsl:template>


</xsl:stylesheet>
