<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version = "1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method = "html" version = "1.0" encoding = "ISO-8859-1" standalone = "no" doctype-public = "-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system = "DTD/xhtml1-strict.dtd"  indent = "yes" />
 
  <xsl:variable name="face">arial</xsl:variable>	
  <xsl:variable name="size">2</xsl:variable>	
  <xsl:variable name="titlesize">5</xsl:variable>	
  <xsl:variable name="subtitlesize">4</xsl:variable>	
  <xsl:variable name="minisize">1</xsl:variable>	
  <xsl:variable name="skillsize">3</xsl:variable>	
  
  <xsl:variable name="language">		
  <xsl:value-of select="/cv/general/language"/></xsl:variable>	
  <xsl:variable name="year">
    <xsl:choose>
      <xsl:when test="$language='french'">ans</xsl:when>	
      <xsl:otherwise>years-old</xsl:otherwise>		
    </xsl:choose>	
  </xsl:variable>	
  <xsl:variable name="professional">	
  <xsl:choose>
    <xsl:when test="$language='french'">professionel</xsl:when>	
    <xsl:otherwise>professional</xsl:otherwise>
  </xsl:choose></xsl:variable>	
  <xsl:variable name="phone">
    <xsl:choose>		
    <xsl:when test="$language='french'">T�l.</xsl:when>			
    <xsl:otherwise>phone</xsl:otherwise>
  </xsl:choose></xsl:variable>
  <xsl:variable name="mail">Mail</xsl:variable>
  <xsl:variable name="web">Web</xsl:variable>
  <xsl:variable name="experience">
    <xsl:choose>
      <xsl:when test="$language='french'">ans d'exp�rience</xsl:when>
      <xsl:otherwise>years of experience</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="clickhereurl">		
  <xsl:choose>
    <xsl:when test="$language='french'">cv-english.html</xsl:when>
    <xsl:otherwise>cv.html</xsl:otherwise>
  </xsl:choose></xsl:variable>	
  <xsl:variable name="clickhere">
    <xsl:choose>
      <xsl:when test="$language='french'">Click here for the English version</xsl:when>
      <xsl:otherwise>Click here for the French version</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="domainsection">
    <xsl:choose>
      <xsl:when test="$language='french'">Domaines de comp�tence</xsl:when>
      <xsl:otherwise>Skills domains</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="experiencesection">
    <xsl:choose>
      <xsl:when test="$language='french'">Exp�rience Professionelle</xsl:when>
      <xsl:otherwise>Professional Experience</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="degreesection">
    <xsl:choose>
      <xsl:when test="$language='french'">Dipl�mes</xsl:when>
      <xsl:otherwise>Degrees</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="personal">
    <xsl:choose>
      <xsl:when test="$language='french'">
        personel
      </xsl:when>
      <xsl:otherwise>
        personal
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="miscsection">
    <xsl:choose>
      <xsl:when test="$language='french'">Divers</xsl:when>
      <xsl:otherwise>Miscellaneous</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>	
  <xsl:variable name="since">
    <xsl:choose>
      <xsl:when test="$language='french'">Depuis</xsl:when>
      <xsl:otherwise>Since</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="month">
    <xsl:choose>
      <xsl:when test="$language='french'">mois</xsl:when>
      <xsl:otherwise>month</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="in">
    <xsl:choose>
      <xsl:when test="$language='french'">en</xsl:when>
      <xsl:otherwise>in</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="lastmodified">
    <xsl:choose>
      <xsl:when test="$language='french'">Derni�re Modification</xsl:when>
      <xsl:otherwise>Last modified</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="count">
    <xsl:choose>
      <xsl:when test="$language='french'">Compteur</xsl:when>
      <xsl:otherwise>Count</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:template match="/cv">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
      <head>
        <link href="css/cv.css" type="text/css"/>
        <title>
          <xsl:value-of select="general/title"/>
        </title>
        <script language="JavaScript">
          <xsl:text>
            <!--
            function makewin(url,width,height,windowName) {
            browser = (navigator.appName=="Netscape");
            agent = navigator.userAgent;							
            windowName = "window"; 					
            params = ""; 					
            params += "toolbar=1,"; 							
            params += "location=1,";
            params += "directories=1,";
            params += "status=1,";							
            params += "menubar=1,"; 	
            params += "scrollbars=1,"; 						
            params += "resizable=1,"; 				
            params += "width=640,"; 	
            params += "height=480"; 						
            win = window.open(url, windowName , params);
            if (!browser) {win = window.open(url, windowName , params);}
            if (!win.opener) {win.opener = window;}
            if (browser) {win.focus();}}
                 //-->
          </xsl:text>			
        </script>				
        <!--xsl:element name="META" use-attribute-sets="description"/>			
        <xsl:element name="META" use-attribute-sets="keywords"/-->
      </head>			
      <body bgcolor="#ffffff" text="#000000" link="#0000ff" vlink="#0000cc" alink="#ff0000">	
      <table width="630" border="0" cellpadding="0" cellspacing="0">	
      <tr>					
      <td>						
      <table width="100%" border="0">						
      <tr>								
      <td>									
      <table width="100%" border="0" cellspacing="0" cellpadding="0">			
      <tr>				
      <td class="head" width="70%" valign="top">	
      <font face="{$face}" size="{$size}">
        <b>
          <a name="top">
            <xsl:value-of select="personal/name/firstname"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="personal/name/lastname"/>
          </a>									
        </b>
        <br/>
        <xsl:value-of select="personal/age"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="$year"/>
        <br/>
        <xsl:value-of select="personal/adress/street"/>
        <br/>
        <xsl:value-of select="personal/adress/zipcode"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="personal/adress/city"/>
        <br/>
        <xsl:if test="$language!='french'">
          <xsl:value-of select="personal/adress/country"/>
          <br/>
        </xsl:if>
        <xsl:for-each select="personal/phones/phone">
          <xsl:value-of select="$phone"/>
          <xsl:text> </xsl:text>
        <xsl:variable name="phonetypetemp"><xsl:value-of select="@type"/></xsl:variable>
        <xsl:choose>
          <xsl:when test="$phonetypetemp='office'">
            <xsl:value-of select="$professional"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$personal"/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:text> : </xsl:text>
        <xsl:value-of select="."/>
        <br/>
      </xsl:for-each>
      <xsl:value-of select="$mail"/>
      <xsl:text> : </xsl:text>
      <xsl:variable name="href">
        <xsl:text>mailto:</xsl:text>
        <xsl:value-of select="personal/mails/mail"/>
      </xsl:variable>
      <a style="text-decoration: none" href="{$href}">
        <xsl:value-of select="personal/mails/mail"/>
      </a>
      <br/>
      <xsl:value-of select="$web"/>
      <xsl:text> : </xsl:text>
      <xsl:variable name="hrefweb">
        <xsl:value-of select="personal/web"/>
      </xsl:variable>
      <a style="text-decoration: none" href="{$hrefweb}">
        <xsl:value-of select="personal/web"/>
      </a>						
      <xsl:value-of select="br"/>
    </font>
  </td>								
  <td align="right">
    <xsl:variable name="photo">
      <xsl:value-of select="personal/photo"/>
    </xsl:variable>
    <img src="{$photo}" width="141" height="120" border="2" alt="Photo"/>
  </td>
</tr>									
</table>
</td>
</tr>
<tr><td><xsl:text> </xsl:text></td></tr>
<tr>
<td align="center">
<b>
  <font face="{$face}" size="{$titlesize}">
    <xsl:value-of select="general/heading"/>
  </font>
</b>
<!--br/>
<font face="{$face}" size="{$subtitlesize}">
  <xsl:value-of select="personal/yearofexperience"/>
  <xsl:text> </xsl:text>
  <xsl:value-of select="$experience"/>
</font-->
</td>
</tr>
<tr>
  <td align="center">
    <font face="{$face}" size="{$minisize}">
      <a style="text-decoration: none" href="{$clickhereurl}">
        <xsl:value-of select="$clickhere"/>
      </a>
    </font>
  </td>
</tr>
<tr><td><xsl:text> </xsl:text></td></tr>

<tr>
  <td>
    <xsl:text> </xsl:text></td></tr>
    <tr>
      <td class="title">
        <font face="{$face}" size="{$skillsize}">
          <b>
            <a name="experience">
              <xsl:value-of select="$experiencesection"/>
            </a>
          </b>
        </font>
      </td>
    </tr>
    <tr>
      <td>
        <xsl:text> </xsl:text>
      </td>
    </tr>
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <xsl:for-each select="experiences/experience">
            <tr>				
            <td width="10%" valign="top">
              <font face="{$face}" size="{$size}">
                <xsl:variable name="current">
                  <xsl:value-of select="@current"/>
                </xsl:variable>
                <xsl:choose>
                  <xsl:when test="$current='true'">
                    <xsl:value-of select="$since"/>
                    <br/>
                    <xsl:value-of select="@month"/>
                    <xsl:text>/</xsl:text>
                    <xsl:value-of select="@year"/>
                    <xsl:text> :</xsl:text>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="@year"/>
                    <xsl:text> :</xsl:text>
                    <br/>
                    <xsl:text>(</xsl:text>
                    <xsl:value-of select="@duration"/>     
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$month"/>
                    <xsl:text>)</xsl:text>
                  </xsl:otherwise>
                </xsl:choose>
              </font>
            </td>
            <td valign="top">
              <font face="{$face}" size="{$size}">
                <xsl:variable name="companyurl">
                  <xsl:value-of select="company/companyname/@url"/>
                </xsl:variable>
                <xsl:choose>
                  <xsl:when test="$companyurl=''">
                    <xsl:value-of select="company/companyname"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <a style="text-decoration: none" href="#top" onclick="makewin('http://{$companyurl}')">
                      <xsl:value-of select="company/companyname"/>
                    </a>	
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:text> </xsl:text>
                <xsl:value-of select="company/town"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="company/entity"/>
                <xsl:choose>
                  <xsl:when test="$language!='french'">
                    <xsl:text> (</xsl:text>
                    <xsl:value-of select="company/country"/>
                    <xsl:text>)</xsl:text>
                  </xsl:when>
                </xsl:choose>
                <xsl:text>.</xsl:text>
                <br/>
                <xsl:for-each select="experienceitems/experienceitem">
                  <xsl:apply-templates/>
                  <xsl:text>.</xsl:text>
                  <br/>
                </xsl:for-each>
              </font>
            </td>
          </tr>
          <tr><td><xsl:text> </xsl:text></td></tr>
        </xsl:for-each>
      </table>
    </td>
  </tr>
  <tr>
    <td class="title" align="center">
      <font face="{$face}" size="{$skillsize}">
        <b>
        <a name="degree">
          <xsl:value-of select="$degreesection"/>
        </a>
      </b>
    </font>
  </td>
</tr>
<tr>
  <td>
    <xsl:text> </xsl:text>
  </td>
</tr>
<tr>
  <td align="left">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <xsl:for-each select="degrees/degree">
        <tr>
          <td width="7%" valign="top">
            <font face="{$face}" size="{$size}">
              <xsl:value-of select="@year"/>
              <xsl:text> :</xsl:text>
            </font>
          </td>
          <td valign="top">
            <font face="{$face}" size="{$size}">
              <xsl:value-of select="diploma"/>
              <xsl:text> </xsl:text>
              <xsl:variable name="schoolurl">
                <xsl:value-of select="school/@url"/>
              </xsl:variable>
              <xsl:variable name="school">
                <xsl:value-of select="school"/>
              </xsl:variable>
              <xsl:text> </xsl:text>
              <xsl:value-of select="$in"/>
              <xsl:text> </xsl:text>
              <xsl:variable name="diplomatitleurl">
                <xsl:value-of select="diplomatitle/@url"/>
              </xsl:variable>
              <xsl:choose>
                <xsl:when test="$diplomatitleurl=''">
                  <xsl:value-of select="diplomatitle"/>
                </xsl:when>
                <xsl:otherwise>
                  <a style="text-decoration: none" href="#degree" onclick="makewin('http://{$diplomatitleurl}')">
                    <xsl:value-of select="diplomatitle"/>
                  </a>				
                </xsl:otherwise>
              </xsl:choose>				
              <xsl:text>, </xsl:text>
              <xsl:choose>
                <xsl:when test="$schoolurl=''">
                  <xsl:value-of select="$school"/>
                </xsl:when>
                <xsl:otherwise>
                  <a style="text-decoration: none" href="#degree" onclick="makewin('http://{$schoolurl}')">
                  <xsl:value-of select="$school"/></a>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:text> </xsl:text>
            <xsl:value-of select="schoollocation"/>
            <xsl:text>, </xsl:text>
            <xsl:choose>
              <xsl:when test="$language='french'">
                <xsl:value-of select="schoollocation/@department"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="schoollocation/@country"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:text>.</xsl:text>
          </font>										
        </td>
      </tr>
      <tr>
        <td>
          <xsl:text> </xsl:text>
        </td>
      </tr>
    </xsl:for-each>
  </table>
</td>
</tr>
<tr>
  <td class="title" align="center">
  <font face="arial" size="3">
    <b>
      <a name="domain">
        <xsl:value-of select="$domainsection"/>
      </a>
    </b>
  </font>
</td>
</tr>
<tr><td><xsl:text> </xsl:text></td>
</tr>
<tr>
  <td align="left">
    <lu>
      <font face="{$face}" size="{$skillsize}">
        <xsl:for-each select="skills/skill">
          <li>
            <xsl:value-of select="."/>
          </li>
        </xsl:for-each>
      </font>
    </lu>
  </td>
</tr>
<tr class="title">
<td align="center">
  <font face="{$face}" size="{$skillsize}">
    <b>
      <a name="misc">
        <xsl:value-of select="$miscsection"/>
      </a>
    </b>
  </font>
</td>
</tr>
<tr><td><xsl:text> </xsl:text></td></tr>
<tr>
  <td align="left">
  <font face="{$face}" size="{$size}">
    <xsl:for-each select="miscellaneous/miscitems/miscitem">
      <xsl:value-of select="."/>
      <xsl:text>.</xsl:text>
      <br/>
    </xsl:for-each>
    <br/>
    <xsl:for-each select="footnotes/footnote">
      <xsl:apply-templates/>
      <xsl:text>.</xsl:text>
      <br/><br/>
    </xsl:for-each>
  </font>
</td>
</tr>
</table>						
</td>					
</tr>					
<tr>						
<td align="center">							
<font face="{$face}" size="{$minisize}">
  <xsl:value-of select="$lastmodified"/>
  <xsl:text> : </xsl:text>
  <xsl:value-of select="footnotes/lastmodified"/>
  <xsl:text>. </xsl:text>
  <xsl:variable name="countvalue">
    <xsl:value-of select="footnotes/count/@value"/>
  </xsl:variable>
  <xsl:if test="not($countvalue='')">
    <br/>
    <xsl:value-of select ="$count"/>
      <xsl:text> : </xsl:text>
      <img src="/cgi-bin/counter?ID={$countvalue} SHOW=yes"/>
    </xsl:if>
    <br/>Ce CV a �t� cr�e � partir de mon CV en <a href="xml/">XML</a>.
    <br/>
    <a href="xml/"><img src="image/vxml10.gif" alt="XML 1.0" height="31" width="88" border="0"/>
  </a>
</font>
</td>
</tr>
</table>
</body>
</html>
</xsl:template>
<xsl:template match="a">
  <xsl:variable name="href"><xsl:value-of select="@href"/></xsl:variable>
  <a style="text-decoration: none" href="#experience" onclick="makewin('http://{$href}')">
    <xsl:value-of select="."/>
  </a>
</xsl:template>
<xsl:template match="b">
  <b>
    <xsl:value-of select="."/>
  </b>
</xsl:template>
<!--xsl:attribute-set name="description">
  <xsl:attribute name="HTTP-EQUIV">description</xsl:attribute>
  <xsl:attribute name="content">
    <xsl:value-of select="/cv/general/description"/>
  </xsl:attribute>
</xsl:attribute-set>
<xsl:attribute-set name="keywords">
  <xsl:attribute name="HTTP-EQUIV">keywords</xsl:attribute>
  <xsl:attribute name="name">keywords</xsl:attribute>
  <xsl:attribute name="content">
    <xsl:for-each select="/cv/general/additionalkeywords/additionalkeyword">
      <xsl:value-of select="."/>
      <xsl:text> </xsl:text>
    </xsl:for-each>
    <xsl:for-each select="/cv/experiences/experience/technologies/technology">
      <xsl:value-of select="."/>
      <xsl:text>
      </xsl:text>
    </xsl:for-each>
  </xsl:attribute>
</xsl:attribute-set-->
</xsl:stylesheet>