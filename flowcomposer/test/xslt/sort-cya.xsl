<?xml version="1.0" encoding="iso-8859-1"?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://exmo.inrialpes.fr/papers"
>

<xsl:output
  method="xml"
  version="1.0"
  encoding="iso-8859-1"
  omit-xml-declaration="no"
  standalone="no"
  doctype-system="http://exmo.inrialpes.fr/papers/xml/bib.dtd"
  indent="yes"/> 

<!-- toplevel -->

<xsl:template match="/">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="bibliography">
<bibliography name="{@name}" date="{@date}">
	<xsl:apply-templates select="reference">
		<!-- here is the trick provided by Oliver Becker -->
		<xsl:sort select="concat(
					substring('a', 1 div (@type='article')),
					substring('b', 1 div (@type='book')),
					substring('c', 1 div (@type='inbook' or @type='incollection')),
					substring('e', 1 div (@type='proceedings' or @type='collection')),
					substring('i', 1 div (@type='inproceedings')),
					substring('m', 1 div (@type='phdthesis')),
					substring('o', 1 div (@type='techreport')),
					substring('s', 1 div (@type='masterthesis')),
					substring('t', 1 div (@type='misc' or @type='booklet' or @type='manual')),
					substring('u', 1 div (@type='unpublished'))
				)" />
		<!-- xsl:sort select="@type"/ -->
		<xsl:sort select="year"/>
		<xsl:sort select="authors/authors/p[1]/@name"/>
	</xsl:apply-templates>
</bibliography>
</xsl:template>

<xsl:template match="*|@*|text()">
	<xsl:copy>
		<xsl:apply-templates select="*|@*|text()"/>
	</xsl:copy>
</xsl:template>

</xsl:stylesheet>
   
