<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers"
>

<xsl:import href="form-hh.xsl" />

<xsl:output
  method="html"
  encoding="iso-8859-1"
  omit-xml-declaration="no"
  standalone="no"
  doctype-public="-//IETF//DTD HTML//EN"
  indent="yes"/> 

<!-- toplevel -->

<xsl:template match="bibliography">
<h1><a href=".."><img src="../img/exmo-small.gif" alt=" [EXMO] "
          border="0" align="bottom" /></a> bibliography selection (<xsl:value-of select="@date"/>)</h1>


<dl>
	<xsl:for-each select="reference">
		<xsl:if test="not(preceding-sibling::reference) or (preceding-sibling::reference[position()=1]/authors/p[1]/@last != ./authors/p[1]/@last)">
<dt><h2><xsl:value-of select="./authors/p[1]/@first" /><xsl:text> </xsl:text><xsl:value-of select="./authors/p[1]/@last" /><i> and others</i></h2></dt><dd></dd>
		</xsl:if>
		<xsl:apply-templates select="."/>
	</xsl:for-each>
</dl>
</xsl:template>

</xsl:stylesheet>
