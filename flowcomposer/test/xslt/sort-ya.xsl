<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://exmo.inrialpes.fr/papers"
>

<xsl:output
  method="xml"
  version="1.0"
  encoding="iso-8859-1"
  omit-xml-declaration="no"
  standalone="no"
  doctype-system="http://exmo.inrialpes.fr/papers/xml/bib.dtd"
  indent="yes"/> 

<!-- toplevel -->

<xsl:template match="/">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="bibliography">
<bibliography name="{@name}" date="{@date}">
	<xsl:apply-templates select="reference">
		<xsl:sort select="year"/>
		<xsl:sort select="authors/authors/p[1]/@name"/>
	</xsl:apply-templates>
</bibliography>
</xsl:template>

<xsl:template match="*|@*|text()">
	<xsl:copy>
		<xsl:apply-templates select="*|@*|text()"/>
	</xsl:copy>
</xsl:template>

</xsl:stylesheet>
