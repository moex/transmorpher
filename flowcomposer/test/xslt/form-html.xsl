<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers"
  result-ns=""
  indent="yes">

<xsl:import href="form-hh.xsl" />

<xsl:output
  method="html"
  encoding="iso-8859-1"
  omit-xml-declaration="no"
  standalone="no"
  doctype-public="-//IETF//DTD HTML//EN"
  indent="yes"/> 

<!-- toplevel -->

<xsl:template match="bibliography">
<h1><a href=".."><img src="../img/exmo-small.gif" alt=" [EXMO] "
          border="0" align="bottom" /></a> bibliography selection (<xsl:value-of select="@date"/>)</h1>


<dl>
	<xsl:apply-templates select="*"/>
</dl>
</xsl:template>

</xsl:stylesheet>
