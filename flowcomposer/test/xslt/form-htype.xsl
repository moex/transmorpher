<?xml version="1.0" encoding="iso-8859-1" standalone="no" ?>
<!-- DOCTYPE xsl:stylesheet SYSTEM ""-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:date="http://www.jclark.com/xt/java/java.util.Date"
  xmlns:bib="http://www.inrialpes.fr/exmo/papers"
>

<xsl:output
  method="html"
  encoding="iso-8859-1"
  omit-xml-declaration="no"
  standalone="no"
  doctype-public="-//IETF//DTD HTML//EN"
  indent="yes"/> 

<!-- toplevel -->

<xsl:include href="form-hh.xsl" />


<!-- article|book|inbook|incollection article|conference|inproceedings|proceedings phdthesis|masterthesis misc|booklet|manual|techreport|unpublished -->



<xsl:template match="bibliography">
<h1><a href=".."><img src="../img/exmo-small.gif" alt=" [EXMO] "
          border="0" align="bottom" /></a> bibliography selection (<xsl:value-of select="@date"/>)</h1>


<dl>
	<xsl:for-each select="reference">
		<xsl:if test="not(preceding-sibling::reference) or (preceding-sibling::reference[position()=1]/@type != ./@type)">
			<xsl:choose>
				<xsl:when test="@type='article'">
<dt><h2>Journal articles/Articles de revues</h2></dt><dd></dd>
		                </xsl:when>
		                <xsl:when test="@type='book'">
<dt><h2>Books/Monographies</h2></dt><dd></dd>
		                </xsl:when>
		                <xsl:when test="(@type='inbook' and preceding-sibling::reference[position()=1]/@type != 'incollection') or (@type='incollection' and preceding-sibling::reference[position()=1]/@type != 'inbook')">
<dt><h2>Book chapters and collected papers/Chapitres de livres</h2></dt><dd></dd>
		                </xsl:when>
                		<xsl:when test="(@type='proceedings' and preceding-sibling::reference[position()=1]/@type != 'collection') or (@type='collection' and preceding-sibling::reference[position()=1]/@type != 'proceedings')">
<dt><h2>Conference and book editing/Coordination d'ouvrages</h2></dt><dd></dd>
		                </xsl:when>
		                <xsl:when test="@type='inproceedings'">
<dt><h2>Conference papers/Communication � des conf�rences</h2></dt><dd></dd>
		                </xsl:when>
                		<xsl:when test="@type='phdthesis'">
<dt><h2>PhD theses/Th�ses de doctorats et habilitations</h2></dt><dd></dd>
                		</xsl:when>
                		<xsl:when test="@type='techreport'">
<dt><h2>Technical reports/Rapports de recherche</h2></dt><dd></dd>
                		</xsl:when>
               			 <xsl:when test="@type='masterthesis'">
<dt><h2>Master theses/M�moires de DEA</h2></dt><dd></dd>
                		</xsl:when>
		                <xsl:when test="@type='misc' or @type='booklet' or @type='manual'">
<dt><h2>Miscellaneous, booklets and manuals/Divers documents publics</h2></dt><dd></dd>
 		               </xsl:when>
                		<xsl:when test="@type='unpublished'">
<dt><h2>Unpublished/Textes non publi�s</h2></dt><dd></dd>
                		</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:apply-templates select="."/>
	</xsl:for-each>
</dl>
</xsl:template>

</xsl:stylesheet>
